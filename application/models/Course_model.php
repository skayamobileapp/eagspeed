<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class : Login_model (Login Model)
 * Login model class to get to authenticate user credentials 
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Course_model extends CI_Model
{
	 function addtotemp($data) {
	 	$result = $this->db->insert('temp_cart', $data);

        if($result) {
            return TRUE;
        } else {
            return FALSE;
        }
	 }


	 function getRegistrationFee($idprogram) {
        $this->db->select('f.*');
        $this->db->from('fee_structure as f');

        $this->db->where('f.id_programme', $idprogram);
        $this->db->where("f.is_registration_fee='1'");
         $query = $this->db->get();
         $result = $query->result();  
        return $result;

	 }

     function getCertificateFee($idprogram){
        $this->db->select('f.*');
        $this->db->from('fee_structure as f');

        $this->db->where('f.id_programme', $idprogram);
        $this->db->where("f.is_registration_fee='0'");
         $query = $this->db->get();
         $result = $query->result();  
        return $result;
     }

	
}
