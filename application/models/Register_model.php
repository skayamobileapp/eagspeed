<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class : Login_model (Login Model)
 * Login model class to get to authenticate user credentials 
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Register_model extends CI_Model
{
	 function addNewRegistration($data) {
	 	$result = $this->db->insert('student', $data);

        if($result) {
            return TRUE;
        } else {
            return FALSE;
        }
	 }


     
     function getCms($id,$tablename)
    {
        $this->db->select('*');
        $this->db->from($tablename);
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    


     function checkvalidcode($code) {
        $currendate = date('Y-m-d');
        $this->db->select('tc.*');
        $this->db->from('discount as tc');
        $this->db->where('tc.code', $code);
        $this->db->where("(tc.start_date)<='$currendate'");
        $this->db->where("(tc.end_date)>='$currendate'");
         $query = $this->db->get();
         $result = $query->row();  
         return $result;
     }

      function addtotemp($data) {
        $result = $this->db->insert('temp_cart', $data);

        if($result) {
            return TRUE;
        } else {
            return FALSE;
        }
     }

     function addtotempdiscount($data){
        $result = $this->db->insert('temp_cart_discount', $data);

        if($result) {
            return TRUE;
        } else {
            return FALSE;
        }
     }


     function checkemployer($id) {
           $this->db->select('tc.*');
        $this->db->from('employee as tc');
        $this->db->where('tc.id_number', $id);
         $query = $this->db->get();
         $result = $query->row();  
         return $result;
     }


     function checkemail($id) {
        $this->db->select('tc.*');
     $this->db->from('student as tc');
     $this->db->where('tc.email_id', $id);
      $query = $this->db->get();
      $result = $query->row();  
      return $result;
  }

     function productTypeList(){
        $this->db->select('tc.*');
        $this->db->from('product_type as tc');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
     }


     function productActiveTypeList(){
        $this->db->select('tc.*');
        $this->db->from('product_type as tc');
        $this->db->where('tc.status', 1);        
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
     }


     function productActiveTypeListById($id){
        $this->db->select('tc.*');
        $this->db->from('product_type as tc');
        $this->db->where('tc.id', $id);        
         $query = $this->db->get();
         $result = $query->row();  
         return $result;
     }

      function checkstudentaeu($id) {
           $this->db->select('tc.*');
        $this->db->from('student_aeu as tc');
        $this->db->where('tc.id_number', $id);
         $query = $this->db->get();
         $result = $query->row();  
         return $result;
     }

     function getDiscountAmountByProgrammeStaff($idprogramme,$type) {
         $this->db->select('tc.*');
        $this->db->from('programme_discount as tc');
        $this->db->where('tc.id_programme', $idprogramme);
        $this->db->where("tc.is_staff_discount='1'");
         $query = $this->db->get();
         $result = $query->row();  
         return $result;
     }

        function getDiscountAmountByProgrammeStudent($idprogramme,$type) {
         $this->db->select('tc.*');
        $this->db->from('programme_discount as tc');
        $this->db->where('tc.id_programme', $idprogramme);
        $this->db->where("tc.is_student_discount='1'");
         $query = $this->db->get();
         $result = $query->row();  
         return $result;
     }

     function checkiddiscount($id){
         $this->db->select('tc.*');
        $this->db->from('temp_cart_discount as tc');
        $this->db->where('tc.id_session', $id);
        $this->db->where("tc.id_discount=''");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
     }

     function checkdiscountcodeadded($iddiscount,$session) {
        $this->db->select('tc.*');
        $this->db->from('temp_cart_discount as tc');
        $this->db->where('tc.id_session', $session);
        $this->db->where('tc.id_discount', $iddiscount);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
     }

     function getDataFromSession($idsession) {
         $this->db->select('tc.*');
        $this->db->from('temp_cart_discount as tc');
        $this->db->where('tc.id_session', $idsession);
         $query = $this->db->get();
         $result = $query->row();  
         return $result;
     }


     function getDiscountFromStaff($id) {
        $this->db->select('tc.*');
        $this->db->from('temp_cart_discount as tc');
        $this->db->where('tc.id_session', $id);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
     }


       function deletefromtemp($id) {
         $this->db->where('id', $id);
        $this->db->delete('temp_cart');
        return TRUE;
     }

     function deletealltempregister($id){
        $this->db->where('session_id', $id);
        $this->db->delete('gift_temp_session');
        return TRUE;
     }
     
     function deletealldiscount($id) {
         $this->db->where('id_session', $id);
        $this->db->delete('temp_cart_discount');
        return TRUE;
     }

     function getFreeCourse(){
         $this->db->select('p.*');
        $this->db->from('programme as p');
        $this->db->where('p.is_free_course', 1);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
     }

     function getMicroCredentials($id){
         $this->db->select('p.*');
        $this->db->from('programme as p');
        $this->db->where('p.id_programme_type', $id);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
     }

     function getSubjects(){
         $this->db->select('p.*');
        $this->db->from('category as p');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
     }

  function gerProgrammeFromSession($id) {
        $this->db->select('p.*,tc.id as tempid,tc.amount');
        $this->db->from('temp_cart as tc');
        $this->db->join('programme as p', 'tc.id_programme = p.id');
        $this->db->where('tc.id_session', $id);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
     }

     function checkid($data) {
        $this->db->select('tc.*');
        $this->db->from('temp_cart as tc ');
        
        $this->db->where('tc.id_session', $data['id_session']);
        $this->db->where('tc.id_programme', $data['id_programme']);

         $query = $this->db->get();
         $result = $query->row();  
         return $result;
     }


     function checkidtemp($data) {
        $this->db->select('tc.*');
        $this->db->from('temp_cart_discount as tc ');
        
        $this->db->where('tc.id_session', $data['id_session']);
         $query = $this->db->get();
         $result = $query->row();  
         return $result;
     }



	 function duplicateCheck($email) {
	 	 $this->db->select('fc.*');
        $this->db->from('student as fc');
        $this->db->where('fc.email', $email);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
	 }


     function programmeOnly(){
        $this->db->select('p.*,s.name as staffname,s.ic_no,s.image as staffimage,f.amount as amount');
        $this->db->from('programme as p');
        $this->db->join("programme_has_dean as pd", "pd.id_programme = p.id AND pd.status='1'",'left');    
        $this->db->join('fee_structure_master as f', 'f.id_programme = p.id','left');    
        $this->db->join('staff as s', 'pd.id_staff = s.id','left');    
                 $this->db->where("p.id_programme_type='2'");
                 $this->db->where("p.status='Approved and Published'");

        $this->db->order_by('rand()');
        $this->db->limit(4);             
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
     }

   
     function programmecat($id) {
         $this->db->select('p.*,f.amount as amount,c.name as cattype,sl.name as studylevel');
        $this->db->from('programme as p');
        $this->db->join("product_type as pt", "p.id_programme_type = pt.id",'left');    
        $this->db->join("category as c", "p.id_category = c.id",'left');    
        $this->db->join("study_level as sl", "p.id_study_level = sl.id",'left');    
        $this->db->join('fee_structure_master as f', 'f.id_programme = p.id','left');    
                 $this->db->where("p.status='Approved and Published'");

                 if($id!='9999999999') {
                    $this->db->where("p.id_category='$id'");
                 } else {
                    $this->db->where("p.is_free_course='1'");

                 }

        $this->db->order_by('rand()');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
     }
     function programme($id) {
         $this->db->select('p.*,f.amount as amount,c.name as cattype,sl.name as studylevel');
        $this->db->from('programme as p');
        $this->db->join("product_type as pt", "p.id_programme_type = pt.id",'left');    
        $this->db->join("category as c", "p.id_category = c.id",'left');    
        $this->db->join("study_level as sl", "p.id_study_level = sl.id",'left');    
        $this->db->join('fee_structure_master as f', 'f.id_programme = p.id','left');    
                 $this->db->where("p.status='Approved and Published'");
                 $this->db->where("p.id_programme_type='$id'");
        $this->db->order_by('rand()');
        $this->db->limit(6);             
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
     }

     function about(){
        $id = 1;
        $this->db->select('*');
        $this->db->from('about');
        $this->db->where('id', $id);
         $query = $this->db->get();
         $result = $query->row();  
         return $result;

     }

     function eduskills(){

         $this->db->select('p.*,f.amount as amount,c.name as cattype,sl.name as studylevel,sf.name as staffname,sf.image as staffimage,pt.name as producttype');
        $this->db->from('programme as p');
        $this->db->join("product_type as pt", "p.id_programme_type = pt.id",'left');    
        $this->db->join("category as c", "p.id_category = c.id",'left');    
        $this->db->join("study_level as sl", "p.id_study_level = sl.id",'left');    
        $this->db->join('fee_structure_master as f', 'f.id_programme = p.id','left');    
        $this->db->join('programme_has_staff as ps', 'ps.id_programme = p.id','left');    
        $this->db->join('staff as sf', 'sf.id = ps.id_staff','left');    
                 $this->db->where("p.status='Approved and Published'");
                 $this->db->where("p.showonhomepage='1'");
                 $this->db->group_by("p.id");
        $this->db->order_by('rand()');
        $this->db->limit(3);             
         $query = $this->db->get();
         $result = $query->result();  
         return $result;

     }

     function programmeByCategory($id) {
         $this->db->select('p.*,f.amount as amount,c.name as cattype,sl.name as studylevel,sf.name as staffname,sf.image as staffimage,pt.name as producttype');
        $this->db->from('programme as p');
        $this->db->join("product_type as pt", "p.id_programme_type = pt.id",'left');    
        $this->db->join("category as c", "p.id_category = c.id",'left');    
        $this->db->join("study_level as sl", "p.id_study_level = sl.id",'left');    
        $this->db->join('fee_structure_master as f', 'f.id_programme = p.id','left');    
        $this->db->join('programme_has_staff as ps', 'ps.id_programme = p.id','left');    
        $this->db->join('staff as sf', 'sf.id = ps.id_staff','left');    
                 $this->db->where("p.status='Approved and Published'");
                 $this->db->where("p.id_category='$id'");
                 $this->db->group_by("p.id");
        $this->db->order_by('rand()');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
     }


      function programmeByType($id) {
         $this->db->select('p.*,f.amount as amount,c.name as cattype,sl.name as studylevel,sf.name as staffname,sf.image as staffimage,pt.name as producttype');
        $this->db->from('programme as p');
        $this->db->join("product_type as pt", "p.id_programme_type = pt.id",'left');    
        $this->db->join("category as c", "p.id_category = c.id",'left');    
        $this->db->join("study_level as sl", "p.id_study_level = sl.id",'left');    
        $this->db->join('fee_structure_master as f', 'f.id_programme = p.id','left');    
        $this->db->join('programme_has_staff as ps', 'ps.id_programme = p.id','left');    
        $this->db->join('staff as sf', 'sf.id = ps.id_staff','left');    
                 $this->db->where("p.status='Approved and Published'");
                 $this->db->where("p.id_programme_type='$id'");
                 $this->db->group_by("p.id");
        $this->db->order_by('rand()');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
     }


     

     function programmefree() {
        $this->db->select('p.*,s.name as staffname,s.ic_no,s.image as staffimage,f.amount as amount,c.name as cattype,sl.name as studylevel');
        $this->db->from('programme as p');
        $this->db->join("product_type as pt", "p.id_programme_type = pt.id",'left');    
        $this->db->join("category as c", "p.id_category = c.id",'left');    
        $this->db->join("study_level as sl", "p.id_study_level = sl.id",'left');    


        $this->db->join('fee_structure_master as f', 'f.id_programme = p.id','left');    
        $this->db->join('staff as s', 'p.id_staff = s.id','left');    
                 $this->db->where("p.status='Approved and Published'");
                 $this->db->where("p.is_free_course='1'");

        $this->db->order_by('rand()');
        $this->db->limit(4);             
         $query = $this->db->get();


         $result = $query->result();  
         return $result;
     }


     function getFacultyList($id){
        $this->db->select('s.*,sal.name as salutationname');
        $this->db->from('staff as s');
        $this->db->join("programme_has_staff as p", "p.id_staff = s.id",'left');    
        $this->db->join("salutation_setup as sal", "sal.id = s.salutation",'left');    
                 $this->db->where("p.id_programme ='$id'");
         $query = $this->db->get();


         $result = $query->result();  
         return $result;
     }


     function getCertificateList($id){
        $this->db->select('s.*');
        $this->db->from('certificate_setup as s');
        $this->db->join("programme_has_certificate as p", "p.id_certificate = s.id",'left');    
                 $this->db->where("p.id_programme ='$id'");
         $query = $this->db->get();


         $result = $query->result();  
         return $result;
     }


     function programmeForCourse(){
        $this->db->select('p.*,s.name as staffname,s.ic_no,s.image as staffimage,f.amount as amount');
        $this->db->from('programme as p');
        $this->db->join("programme_has_dean as pd", "pd.id_programme = p.id AND pd.status='1'",'left');    

        $this->db->join('fee_structure_master as f', 'f.id_programme = p.id','left');    
        $this->db->join('staff as s', 'pd.id_staff = s.id','left');      
        $this->db->where("p.id_category='1'");
        $this->db->where("p.status='Approved and Published'");
        $this->db->order_by('rand()');

         $query = $this->db->get();
         $result = $query->result();  
         return $result;
     }


     function getAllByCategories($id) {
        $this->db->select('p.*,s.name as staffname,s.ic_no,s.image as staffimage,f.amount as amount,pt.name as cattype');
        $this->db->from('programme as p');
        $this->db->join("programme_has_dean as pd", "pd.id_programme = p.id AND pd.status='1'",'left');    
        $this->db->join("product_type as pt", "p.id_programme_type = pt.id",'left');    
        $this->db->join('fee_structure_master as f', 'f.id_programme = p.id','left');    
        $this->db->join('staff as s', 'pd.id_staff = s.id','left');  

        if($name) {
            $this->db->where("p.id_category",$id);
        }
                         $this->db->where("p.status='Approved and Published'");

         $query = $this->db->get();
         $result = $query->result();  
         return $result;
     }


     function programmeForCourseByFilterByName($name) {
        $this->db->select('p.*,f.amount as amount,pt.name as cattype,sl.name as studyname,sf.name as staffname,sf.image as staffimage,pt.name as producttype,d.name as categoryname');
        $this->db->from('programme as p');
        $this->db->join("programme_has_dean as pd", "pd.id_programme = p.id AND pd.status='1'",'left');    
        $this->db->join("product_type as pt", "p.id_programme_type = pt.id",'left');    
        $this->db->join("study_level as sl", "p.id_study_level = sl.id",'left');    
        $this->db->join("category as d", "p.id_category = d.id",'left');    

        $this->db->join('fee_structure_master as f', 'f.id_programme = p.id','left');    
        $this->db->join('programme_has_staff as ps', 'ps.id_programme = p.id','left');    
        $this->db->join('staff as sf', 'sf.id = ps.id_staff','left');    

        if($name) {
            $this->db->where("p.name like '%$name%'");
        }
                                            $this->db->where("p.status='Approved and Published'");

 
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
     }

     function programmeForCourseByFilterSummary($programmeType,$product_type,$studyType,$minamount,$maxamount,$minweek,$maxweek){
        $this->db->select('p.*,f.amount as amount,pt.name as cattype,sl.name as studylevel,d.name as categoryname,sf.name as staffname,sf.image as staffimage,pt.name as producttype');
        $this->db->from('programme as p');
        $this->db->join("product_type as pt", "p.id_programme_type = pt.id",'left');    
        $this->db->join("study_level as sl", "p.id_study_level = sl.id",'left');    
        $this->db->join("category as d", "p.id_category = d.id",'left');    
        $this->db->join('fee_structure_master as f', 'f.id_programme = p.id','left');    
        $this->db->join('programme_has_staff as ps', 'ps.id_programme = p.id','left');    
        $this->db->join('staff as sf', 'sf.id = ps.id_staff','left');    

        if($programmeType) {
                    $this->db->where("p.id_programme_type in ($programmeType)");
        } 

       
        if($product_type) {
                    $this->db->where("p.id_category in ($product_type)");
        }   

         if($studyType) {
                    $this->db->where("p.id_study_level in ($studyType)");
        } 

         if($minamount!=0 && $minamount!=99999) {
                    $this->db->where("f.amount >= '$minamount'");
        } 
        
         if($maxamount && $minamount!=0) {
                    $this->db->where("f.amount <= '$maxamount'");
        } 

         if($maxamount && $minamount==0) {
                    $this->db->where("(f.amount <= '$maxamount' OR p.is_free_course = '1')");
        } 


                 if($minweek!=0 && $minweek!=99999) {
                    $this->db->where("p.max_duration >= '$minweek'");
                    $this->db->where("p.duration_type = 'Weeks'");
        } 
        
         if($maxweek && $minweek!=0) {
                    $this->db->where("p.max_duration <= '$maxweek'");
                    $this->db->where("p.duration_type = 'Weeks'");
        } 

         if($maxweek && $minweek==0) {
                    $this->db->where("p.max_duration <= '$maxweek'");
                    $this->db->where("p.duration_type = 'Weeks'");
                    $this->db->or_where("p.duration_type = 'Days'");

        } 



        if($minweek==0  && $maxweek==0 ) {
                    $this->db->or_where("p.duration_type = 'Days'");
        } 





                         $this->db->where("p.status='Approved and Published'");


         $query = $this->db->get();

         $result = $query->result();  
         return $result;
     }

     function programmeForCourseByFilter($programmeType,$product_type,$studyType,$minamount,$maxamount){
        $this->db->select('p.*,f.amount as amount,pt.name as cattype,sl.name as studylevel,d.name as categoryname,sf.name as staffname,sf.image as staffimage,pt.name as producttype');
        $this->db->from('programme as p');
        $this->db->join("product_type as pt", "p.id_programme_type = pt.id",'left');    
        $this->db->join("study_level as sl", "p.id_study_level = sl.id",'left');    
        $this->db->join("category as d", "p.id_category = d.id",'left');    
        $this->db->join('fee_structure_master as f', 'f.id_programme = p.id','left');    
        $this->db->join('programme_has_staff as ps', 'ps.id_programme = p.id','left');    
        $this->db->join('staff as sf', 'sf.id = ps.id_staff','left');    
     

        if($programmeType) {
                    $this->db->where("p.id_category in ($programmeType)");
        } 

       
        if($product_type) {
                    $this->db->where("p.id_programme_type in ($product_type)");
        }   

         if($studyType) {
                    $this->db->where("p.id_study_level in ($studyType)");
        }  

         if($minamount!=0 && $minamount!=99999) {
                    $this->db->where("f.amount >= '$minamount'");
        } 
         if($maxamount && $minamount!=0) {
                    $this->db->where("f.amount <= '$maxamount'");
        } 

         if($maxamount && $minamount==0) {
                    $this->db->where("(f.amount <= '$maxamount' OR p.is_free_course = '1')");
        }  

         if($maxamount==0 && $minamount==0) {
                    $this->db->where("p.is_free_course = '1'");
        }  



                         $this->db->where("p.status='Approved and Published'");


         $query = $this->db->get();

         $result = $query->result();  
         return $result;
     }

 function programmeAwardList($id_programme)
    {
        $this->db->select('tphd.*, aw.code as award_code, aw.name as award_name, pc.code as program_condition_code, pc.name as program_condition_name');
        $this->db->from('programme_has_award as tphd');
        $this->db->join('award as aw', 'tphd.id_award = aw.id');     
        $this->db->join('programme_condition as pc', 'tphd.id_program_condition = pc.id');     
        $this->db->where('tphd.id_program', $id_programme);
        $query = $this->db->get();
        return $query->result();
    }

        function getProgramOverview($id) {
        $this->db->select('*');
        $this->db->from('programme_has_overview');
        $this->db->where('id_programme', $id);

         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getProgrammeByProductTypeId($id) {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('id_category', $id);
        $this->db->where("status='Approved and Published'");
        $this->db->order_by('name ASC');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getFreeProgrammeByProductTypeId(){
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where("is_free_course='1'");
        $this->db->where("status='Approved and Published'");        
        $this->db->order_by('name ASC');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getStudyLevel(){
        $this->db->select('*');
        $this->db->from('study_level');
        $this->db->order_by('name ASC');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }



function getCLO($id) {
        $this->db->select('*');
        $this->db->from('programme_has_clo');
        $this->db->where('id_programme', $id);

         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }



    function getProgramSyllabus($id) {
        $this->db->select('*');
        $this->db->from('programme_has_syllabus');
        $this->db->where('id_programme', $id);

         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

     function getProductType() {
        $this->db->select('ct.*');
        $this->db->from('product_type as ct');
        $this->db->join('programme as p', 'p.id_programme_type=ct.id');
        $this->db->where("ct.status='1'");
        $this->db->group_by('ct.id');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
     }

     function getProductTypeById($id){
        $this->db->select('*');
        $this->db->from('category');
        $this->db->where('id', $id);
         $query = $this->db->get();
         $result = $query->row();  
         return $result;
     }


        function getProductTypeByIdNew($id){
        $this->db->select('*');
        $this->db->from('product_type');
        $this->db->where('id', $id);
        $this->db->where("status='Approved and Published'");

         $query = $this->db->get();
         $result = $query->row();  
         return $result;
     }


    
     function getCategoryList() {
        $this->db->select('ct.*');
        $this->db->from('category as ct');
        $this->db->where("ct.status='1'");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
     }


     function getActiveCategoryList() {
        $this->db->select('ct.*');
        $this->db->from('category as ct');
        $this->db->where("ct.status='1'");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
     }

     function getCourses($id) {
        $this->db->select('tc.*,ct.name as categoryname, c.name as coursename');
        $this->db->from('temp_cart as tc');
        $this->db->join('course as c', 'tc.id_course = c.id');
        $this->db->join('category as ct', 'c.id_category = ct.id');

        $this->db->where('tc.id_session', $id);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
     }


       function getCategoryById($id) {
        $this->db->select('ct.*');
        $this->db->from('category as ct');
        $this->db->where('ct.id', $id);

         $query = $this->db->get();
         $result = $query->result();  
         return $result;
       }
       function getAllCourses($id) {
        $this->db->select('tc.*,f.amount');
        $this->db->from('course as tc');
        $this->db->join('fee_structure_main as f', 'f.id_course = tc.id');
        $this->db->where('tc.id_category', $id);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
     }




    function programmeAimDetailsById($id) {
        $this->db->select('f.*');
        $this->db->from('programme_has_aim as f');
        $this->db->where('f.id_programme', $id);
         $query = $this->db->get();
         $result = $query->result();  
        return $result;
     }

     function programmeStructureById($id) {
        $this->db->select('f.*');
        $this->db->from('programme_has_structure as f');
        $this->db->where('f.id_programme', $id);
         $query = $this->db->get();
         $result = $query->result();  
        return $result;
     }

     

      function programmeCertificateById($id) {
        $this->db->select('f.*');
        $this->db->from('programme_has_certificate as f');
        $this->db->where('f.id_programme', $id);
         $query = $this->db->get();
         $result = $query->result();  
        return $result;
     }


     function getCertificateNames($id_category){
         $this->db->select('p.*');
        $this->db->from('programme as p');
        $this->db->where("p.id_programme_type='2'");
         $query = $this->db->get();
         $result = $query->result();  
        return $result;
     }

     function programmeHasModules($id) {
        $this->db->select('p.*,pt.name as cattype,sl.name as studyname,fm.amount as amount');
        $this->db->from('programme_has_ref_program as f');
        $this->db->join('programme as p', 'p.id = f.id_child_programme'); 
        $this->db->join("product_type as pt", "p.id_programme_type = pt.id",'left');    
        $this->db->join("study_level as sl", "p.id_study_level = sl.id",'left');    
        $this->db->join('fee_structure_master as fm', 'fm.id_programme = p.id','left');    

        $this->db->where('f.id_programme', $id);
         $query = $this->db->get();
         $result = $query->result();  
        return $result;
     }

     function getCoursesById($id) {
        $this->db->select('ct.*');
        $this->db->from('programme as ct');       
        $this->db->where('ct.id', $id); 
         $query = $this->db->get();
         $result = $query->row();  
         return $result;
     }


     function getCourseAmount($id) {
         $this->db->select('f.*');
        $this->db->from('fee_structure_master as f');
        $this->db->where('f.id_programme', $id);

         $query = $this->db->get();
         $result = $query->row();  
         return $result;
     }

     function getStaffDetailsForProgramId($id) {
        $this->db->select('s.*');
        $this->db->from('programme_has_dean as pd');
        $this->db->join('staff as s', 'pd.id_staff = s.id');             

        $this->db->where('pd.id_programme', $id);
        $this->db->where("pd.status='1'");

         $query = $this->db->get();
         $result = $query->row();  
         return $result;
     }

     function getProgramById($id) {
        $this->db->select('p.*,s.first_name,s.last_name,s.degree_details,e.name as educationLevel,pc.name as pcategory,d.name as deliverymode,pt.name as progtype,l.name as learningmodename');
        $this->db->from('programme as p');
        $this->db->join('product_type as pt', 'pt.id = p.id_programme_type','left');    
        $this->db->join('category as pc', 'pc.id = p.id_category','left');    
        $this->db->join('delivery_mode as d', 'd.id = p.id_delivery_mode','left');    
        $this->db->join('learning_mode as l', 'l.id = p.id_learning_mode','left');    
        $this->db->join('education_level as e', 'e.id = p.id_education_level','left');    
        $this->db->join('staff as s', 's.id = p.id_staff','left');    
        $this->db->where('p.id', $id);
        $query = $this->db->get();
        $result = $query->row();  
        return $result;
     }

     function getTopicByProgramId($id) {
        $this->db->select('f.*');
        $this->db->from('programme_has_topic as f');
        $this->db->where('f.id_programme', $id);
         $query = $this->db->get();
         $result = $query->result();  
        return $result;
     }

     function getProgramReferences($id){
        $this->db->select('f.*');
        $this->db->from('programme_has_reference as f');
        $this->db->where('f.id_programme', $id);
         $query = $this->db->get();
         $result = $query->result();  
        return $result;
     }

     function getAssessmentByProgramId($id) {
        $this->db->select('f.*,e.name as component');
        $this->db->from('programme_has_assessment as f');
        $this->db->join('examination_components as e', 'f.id_examination_components = e.id');     

        $this->db->where('f.id_programme', $id);
         $query = $this->db->get();
         $result = $query->result();  
        return $result;
     }
       function getNames($ids) {

       
        $this->db->select('*');
        $this->db->from('programme_has_syllabus');
        $this->db->where("id ",$id);

         $query = $this->db->get();
         $result = $query->result();  
        return $result;
    }






}

