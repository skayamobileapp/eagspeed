<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Partner_university_login_model extends CI_Model
{
    
    function loginPartnerUniversity($email, $password)
    {
        $this->db->select('stu.*');
        $this->db->from('partner_university as stu');
        $this->db->where('stu.login_id', $email);
        // $this->db->where('stu.applicant_status', 'Approved');
        // $this->db->where('stu.isDeleted', 0);
        $query = $this->db->get();
        $user = $query->row();
        if($user)
        {
            if($user->password == md5($password))
            {
                return $user;
            }
            else
            {
                return array();
            }
        }
    
    }

    function partnerUniversityLastLoginInfo($id_partner_university)
    {
        $this->db->select('pll.created_dt_tm');
        $this->db->where('pll.id_partner_university', $id_partner_university);
        $this->db->order_by('pll.id', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get('partner_university_last_login as pll');

        return $query->row();
    }


    function checkPartnerUniversityUserExist($user_id)
    {
        $this->db->select('id');
        $this->db->where('login_id', $user_id);
        $this->db->where('status', 1);
        $query = $this->db->get('partner_university');

        if ($query->num_rows() > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    function resetPasswordUser($data)
    {
        $result = $this->db->insert('tbl_reset_password', $data);

        if($result)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    function getPartnerUniversityInfoByEmail($email)
    {
        $this->db->select('id, user_id, name');
        $this->db->from('partner_university');
        // $this->db->where('isDeleted', 0);
        $this->db->where('user_id', $email);
        $query = $this->db->get();

        return $query->row();
    }

    function addPartnerUniversityLastLogin($loginInfo)
    {
        $this->db->trans_start();
        $this->db->insert('partner_university_last_login', $loginInfo);
        $this->db->trans_complete();
    }

    function getSalutationByStatus($status)
    {
        $this->db->select('s.*');
        $this->db->from('salutation_setup as s');
        $this->db->where('s.status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function addNewPartnerUniversity($data)
    {
        $this->db->trans_start();
        $this->db->insert('partner_university', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function checkUserExist($data)
    {
        $this->db->select('*');
        $this->db->from('partner_university');
        // $this->db->where('isDeleted', 0);
        $this->db->where('login_id', $data['email']);
        $query = $this->db->get();

        return $query->row();
    }

    function getPartnerUniversityDuplication($data)
    {

        $this->db->select('st.*');
        $this->db->from('partner_university as st');        
        if($data['id_partner'] != '')
        {
            $this->db->where('st.id !=', $data['id_partner']);
        }
        if($data['code'] != '')
        {
            $this->db->where('st.code', $data['code']);
        }
        if($data['name'] != '')
        {
            $this->db->where('st.name', $data['name']);
        }
        if($data['email'] != '')
        {
            $this->db->where('st.email', $data['email']);
        }
        if($data['login_id'] != '')
        {
            $this->db->where('st.login_id', $data['login_id']);
        }
        $query = $this->db->get();
        $result = $query->row();

        return $result;
    }
}
?>