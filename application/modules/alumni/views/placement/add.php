<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Internship Application</h3>
        </div>
        <form id="form_internship" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Placement Form Details</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Company Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="company_name" name="company_name">
                    </div>
                </div>

              

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Company Phone No. <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="phone" name="phone">
                    </div>
                </div>   

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Email <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="email" name="email">
                    </div>
                </div>     

            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Company Address <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="company_address" name="company_address">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Country <span class='error-text'>*</span></label>
                        <select name="id_country" id="id_country" class="form-control" onchange="getStateByCountry(this.value)">
                            <option value="">Select</option>
                            <?php
                            if (!empty($countryList))
                            {
                                foreach ($countryList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>State <span class='error-text'>*</span></label>
                        <span id='view_state'></span>
                    </div>
                </div>

            </div>


             <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>City <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="city" name="city">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Zipcode <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="zipcode" name="zipcode">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Joining Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="joining_dt" name="joining_dt" autocomplete="off">
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Designation <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="designation" name="designation">
                    </div>
                </div>

            </div>

        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
        
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
          
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>

        </form>


        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>


<script>

    $('select').select2();

    function getStateByCountry(id)
    {
        $.get("/finance/bankRegistration/getStateByCountry/"+id, function(data, status){
       
            $("#view_state").html(data);
        });
    }


    $(document).ready(function() {
        $("#form_internship").validate({
            rules: {
                company_name: {
                    required: true
                },
                email: {
                    required: true
                },
                phone: {
                    required: true
                },
                company_address: {
                    required: true
                },
                 id_country: {
                    required: true
                },
                 id_state: {
                    required: true
                },
                 city: {
                    required: true
                },
                 zipcode: {
                    required: true
                },
                 joining_dt: {
                    required: true
                },
                 designation: {
                    required: true
                }
            },
            messages: {
                company_name: {
                    required: "<p class='error-text'>Company Name Reequired</p>",
                },
                email: {
                    required: "<p class='error-text'>Company Email Required</p>",
                },
                phone: {
                    required: "<p class='error-text'>Company Phone Number</p>",
                },
                company_address: {
                    required: "<p class='error-text'>Company Address Required</p>",
                },
                id_country: {
                    required: "<p class='error-text'>Select Country</p>",
                },
                id_state: {
                    required: "<p class='error-text'>Select State</p>",
                },
                city: {
                    required: "<p class='error-text'>City Required</p>",
                },
                zipcode: {
                    required: "<p class='error-text'>Zipcode Required</p>",
                },
                joining_dt: {
                    required: "<p class='error-text'>Joining Date Required</p>",
                },
                designation: {
                    required: "<p class='error-text'>Desination Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    $(function(){
        $( ".datepicker" ).datepicker();
      });

</script>