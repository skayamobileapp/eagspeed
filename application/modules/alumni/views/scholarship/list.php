<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>View Scholarship Applications</h3>
        </div>    
        <form id="form_main_invoice" action="" method="post">
            <div class="form-container">



                <h4 class="form-group-title">Alumni Details</h4>
                <div class='data-list'>
                    <div class='row'> 
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Alumni Name :</dt>
                                <dd><?php echo ucwords($studentDetails->full_name);?></dd>
                            </dl>
                            <dl>
                                <dt>Alumni NRIC :</dt>
                                <dd><?php echo $studentDetails->nric ?></dd>
                            </dl>
                            <dl>
                                <dt>Alumni Email :</dt>
                                <dd><?php echo $studentDetails->email_id; ?></dd>
                            </dl>
                             <dl>
                                <dt>Nationality :</dt>
                                <dd><?php echo $studentDetails->nationality ?></dd>
                            </dl>                            
                        </div>        
                        
                        <div class='col-sm-6'>                           
                            <dl>
                                <dt>Intake :</dt>
                                <dd><?php echo $studentDetails->intake_name ?></dd>
                            </dl>
                            <dl>
                                <dt>Program :</dt>
                                <dd><?php echo $studentDetails->programme_name; ?></dd>
                            </dl>
                            <dl>
                                <dt>Qualification Type :</dt>
                                <dd><?php echo $studentDetails->qualification_code . " - " . $studentDetails->qualification_name; ?></dd>
                            </dl>
                            <dl>
                                <dt>Scholarship :</dt>
                                <dd><?php echo ""; ?></dd>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>


        



            <!-- <h2 align="center"> Coming Soon....</h2> -->

        <br>

        <div class="page-title clearfix">

     <!--   <?php
        if($check_limit == 0)
        {         
        ?>
          <a href="add" class="btn btn-primary">+ Add Scholarship Application</a>

        <?php
         }else{

        ?>
          <h2 class="text-center"><span class='error-text'><b><?php echo $student_name; ?> You Have Exceeded The Max Application Limit.<br>(Only 1 Application Can Add Per Year)</span></b></h2>

       <?php
         }

        ?> -->
        </div>


         <div class="form-container">
            <h4 class="form-group-title">Scholarship Applications</h4>          
            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>
            <div class="clearfix">
                <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                    <li role="presentation" class="active" ><a href="#invoice" class="nav-link border rounded text-center"
                            aria-controls="invoice" aria-selected="true"
                            role="tab" data-toggle="tab">Application</a>
                    </li>
                   <!--  <li role="presentation"><a href="#receipt" class="nav-link border rounded text-center"
                            aria-controls="receipt" role="tab" data-toggle="tab">Receipt</a>
                    </li> -->
                    
                </ul>
                <br>

                <!-- <span class='error-text'>Note * :  Maximim Of 1 Applications Can Add Per Academic Year</span> -->

                
                <div class="tab-content offers-tab-content">
                    <div role="tabpanel" class="tab-pane active" id="invoice">
                        <div class="mt-4">
                            <div class="custom-table" id="printInvoice">
                                <table class="table" id="list-table">
                                    <thead>
                                    <tr>
                                        <th>Sl. No</th>
                                        <th>Application Number</th>
                                        <th>Scheme</th>
                                        <th>Father Name</th>
                                        <th>Father Occupation</th>
                                        <th>No Of Sibblings</th>
                                        <th>Year</th>
                                        <th>Max Marks</th>
                                        <th>Obtained Marks</th>
                                        <th>Percentage</th>
                                        <th class="text-center">Status</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    if (!empty($scholarshipApplicationList))
                                    {
                                        $i=1;
                                        foreach ($scholarshipApplicationList as $record) {
                                    ?>
                                        <tr>
                                            <td><?php echo $i ?></td>
                                            <td><?php echo $record->application_number ?></td>
                                            <td><?php echo $record->scholarship_code . " - " . $record->scholarship_name ?></td>
                                            <td><?php echo $record->father_name ?></td>
                                            <td><?php echo $record->father_occupation; ?></td>
                                            <td><?php echo $record->no_siblings ?></td>
                                            <td><?php echo $record->year ?></td>
                                            <td><?php echo $record->max_marks ?></td>
                                            <td><?php echo $record->obtained_marks ?></td>
                                            <td><?php echo $record->percentage ?></td>
                                            <td class="text-center"><?php 
                                            if( $record->status == '0')
                                            {
                                              echo "Pending";
                                            }
                                            elseif( $record->status == '1')
                                            {
                                              echo "Approved";
                                            }elseif( $record->status == '2')
                                            {
                                              echo "Rejected";
                                            }
                                            ?></td>
                                            <td class="text-center">
                                            <a href="<?php echo 'view/' . $record->id; ?>" title="Preview">View</a>
                                            </td>
                                        </tr>
                                    <?php
                                    $i++;
                                        }
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div> 
                    </div>

                    <!-- <div role="tabpanel" class="tab-pane" id="receipt">
                        <div class="mt-4">
                            <div class="custom-table" id="printReceipt">
                                <table class="table" id="list-table">
                                    <thead>
                                    <tr>
                                        <th>Receipt Number</th>
                                        <th>Receipt Amount</th>
                                        <th>Remarks</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    if (!empty($getReceiptByStudentId)) {
                                        foreach ($getReceiptByStudentId as $record) {
                                    ?>
                                        <tr>
                                            <td><?php echo $record->receipt_number ?></td>
                                            <td><?php if($record->receipt_amount == ""){
                                                echo "0.00";
                                            } else { echo $record->receipt_amount; } ?></td>
                                            <td><?php echo $record->remarks ?></td>
                                            <td class="">
                                            <a href="#" title="" onclick="printDiv('printReceipt')">Print</a>
                                            </td>
                                        </tr>
                                    <?php
                                        }
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>

        </div>
            
        </form>

    </div>

        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
</div>


</form>
<script type="text/javascript">
function printDiv(divName) {
    var printContents = document.getElementById(divName).innerHTML;
    var originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
}
</script>
<script>

    $('select').select2();

    function getStudentByProgramme(id)
    {

     $.get("/finance/mainInvoice/getStudentByProgrammeId/"+id, function(data, status){
   
        $("#student").html(data);
        // $("#view_programme_details").html(data);
        // $("#view_programme_details").show();
    });
 }

 function getStudentByStudentId(id)
 {

     $.get("/finance/mainInvoice/getStudentByStudentId/"+id, function(data, status){
   
        $("#view_student_details").html(data);
        $("#view_student_details").show();
    });
 }



    function opendialog()
    {
        $("#id_fee_item").val('');
        $("#amount").val('');
        $("#id").val('0');                    
        $('#myModal').modal('show');

    }
    function saveData() {


        var tempPR = {};
        tempPR['id_fee_item'] = $("#id_fee_item").val();
        tempPR['amount'] = $("#amount").val();
        tempPR['id'] = $("#id").val();
            $.ajax(
            {
               url: '/finance/mainInvoice/tempadd',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view").html(result);
                var ta = $("#inv-total-amount").val();
                $("#total_amount").val(ta);
                $('#myModal').modal('hide');
               }
            });
        
    }

    function deleteTempData(id) {
         $.ajax(
            {
               url: '/finance/mainInvoice/tempDelete/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    $("#view").html(result);
               }
            });
    }


    function getTempData(id) {
        $.ajax(
            {
               url: '/finance/mainInvoice/tempedit/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(resultfromjson)
               {
                    result = JSON.parse(resultfromjson);
                    $("#dt_fund").val(result['dt_fund']);
                    $("#dt_department").val(result['dt_department']);
                    $("#id").val(id);
                    $('#myModal').modal('show');
               }
            });

    }


    $(document).ready(function() {
        $("#form_main_invoice").validate({
            rules: {
                total_amount: {
                    required: true
                },
                id_student: {
                    required: true
                },
                date_time: {
                    required: true
                }
            },
            messages: {
                total_amount: {
                    required: "Enter Total Amount",
                },
                id_student: {
                    required: "Select Student",
                },
                date_time: {
                    required: "Select Date ",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>