<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Statement_of_account_model extends CI_Model
{

    function getStudentByStudentId($id_student)
    {
        $this->db->select('s.*, p.name as programme_name, i.name as intake_name, qs.name as qualification_name, qs.code as qualification_code');
        $this->db->from('student as s');
        $this->db->join('programme as p', 's.id_program = p.id'); 
        $this->db->join('intake as i', 's.id_intake = i.id');
        $this->db->join('qualification_setup as qs', 's.id_degree_type = qs.id');
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getInvoiceByStudentId($id_student,$id_applicant)
    {
        // $type = "Student";
        // $this->db->select('mi.*, s.full_name as student, s.email_id, s.nric');
        // $this->db->from('main_invoice as mi');
        // $this->db->join('student as s', 'mi.id_student = s.id');
        // $this->db->where('mi.id_student', $id_student);
        // $this->db->where('mi.type', $type);
        // $query = $this->db->get();
        //  $result = $query->result();

        // return$result;

        $student = "Student";
        $applicant = "Applicant";

        $this->db->select('mi.*');
        $this->db->from('main_invoice as mi');
        // $this->db->join('student as s', 'mi.id_student = s.id');
        // $this->db->where('mi.id_student', $id_student);
        // $this->db->where('mi.type', 'Student');
        $likeCriteria = "(id_student  = '" . $id_student . "' and type  ='" . $student . "') or (id_student  ='" . $id_applicant . "' and type  ='" . $applicant . "')";
        // echo "<Pre>";print_r($likeCriteria);exit();
        $this->db->where($likeCriteria);


        $query = $this->db->get();
         $result = $query->result();

        return$result;
    }

    function getReceiptByStudentId($id_student,$id_applicant)
    {
        $student = "Student";
        $applicant = "Applicant";

        // $this->db->select('r.*, s.full_name as student, s.email_id, s.nric');
        // $this->db->from('receipt as r');
        // $this->db->join('student as s', 'r.id_student = s.id');
        // $this->db->where('r.id_student', $id_student);
        // $this->db->where('r.type', $type);
        // $query = $this->db->get();
        //  $result = $query->result();

        // return$result;

        $this->db->select('mi.*');
        $this->db->from('receipt as mi');
        // $this->db->join('student as s', 'mi.id_student = s.id');
        // $this->db->where('mi.id_student', $id_student);
        // $this->db->where('mi.type', 'Student');
        $likeCriteria = "(id_student  = '" . $id_student . "' and type  ='" . $student . "') or (id_student  ='" . $id_applicant . "' and type  ='" . $applicant . "')";
        // echo "<Pre>";print_r($likeCriteria);exit();
        $this->db->where($likeCriteria);


        $query = $this->db->get();
         $result = $query->result();

        return$result;
    }

    function getStudentDiscountList($id_student,$id_applicant)
    {
        $student = "Student";
        $applicant = "Applicant";

        $this->db->select('r.*, mi.invoice_number, mi.total_amount');
        $this->db->from('main_invoice_discount_details as r');
        $this->db->join('main_invoice as mi', 'r.id_main_invoice = mi.id');
        // $this->db->where('mi.id_student', $id_student);
        // $this->db->where('mi.id_student', $id_student);
        $likeCriteria = "(mi.id_student  = '" . $id_student . "' and mi.type  ='" . $student . "') or (mi.id_student  ='" . $id_applicant . "' and mi.type  ='" . $applicant . "')";
        // echo "<Pre>";print_r($likeCriteria);exit();
        $this->db->where($likeCriteria);

        $query = $this->db->get();
         $result = $query->result();

        return$result;
    }

    function getStudentSponserList($id_student)
    {
        $this->db->select('r.*, sp.name as sponser_name, sp.mobile_number, sp.address');
        $this->db->from('sponser_has_students as r');
        $this->db->join('sponser as sp', 'r.id_sponser = sp.id');
        $this->db->where('r.id_student', $id_student);
        $query = $this->db->get();
         $result = $query->result();

        return$result;
    }

    function getMainInvoice($id)
    {
        $this->db->select('mi.*, p.name as programme_name, p.code as programme_code, i.name as intake_name');
        $this->db->from('main_invoice as mi');
        // $this->db->join('student as s', 'mi.id_student = s.id');
        $this->db->join('programme as p', 'mi.id_program = p.id');
        $this->db->join('intake as i', 'mi.id_intake = i.id');
        $this->db->where('mi.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getMainInvoiceDetails($id)
    {
        $this->db->select('mid.*, fstp.name as fee_setup');
        $this->db->from('main_invoice_details as mid');
        // $this->db->join('fee_structure as fs', 'mid.id_fee_item = fs.id');        
        $this->db->join('fee_setup as fstp', 'mid.id_fee_item = fstp.id');        
        $this->db->where('mid.id_main_invoice', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function getMainInvoiceDiscountDetails($id)
    {
        $this->db->select('*');
        $this->db->from('main_invoice_discount_details');   
        $this->db->where('id_main_invoice', $id);
        $query = $this->db->get();
        return $query->result();
    }


    function getReceipt($id)
    {
        $this->db->select('re.*, p.name as programme_name, p.code as programme_code, i.name as intake_name');
        $this->db->from('receipt as re');
        // $this->db->join('student as s', 're.id_student = s.id');
        $this->db->join('programme as p', 're.id_program = p.id');
        $this->db->join('intake as i', 're.id_intake = i.id');
        $this->db->where('re.id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    function getReceiptInvoiceDetails($id)
    {
        $this->db->select('r.*,mi.*, r.paid_amount');
        $this->db->from('receipt_details as r');
        $this->db->join('main_invoice as mi', 'mi.id = r.id_main_invoice');
        $this->db->where('r.id_receipt', $id);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }
    function getReceiptPaymentDetails($id)
    {
        $this->db->select('r.*');
        $this->db->from('receipt_paid_details as r');
        $this->db->where('r.id_receipt', $id);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getMainInvoiceApplicantData($id_applicant)
    {
        $this->db->select('app.full_name, app.nric, app.id_degree_type');
        $this->db->from('applicant as app');
        $this->db->where('app.id', $id_applicant);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getMainInvoiceStudentData($id_student)
    {
        $this->db->select('stu.full_name, stu.nric, stu.id_degree_type');
        $this->db->from('student as stu');
        $this->db->where('stu.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function qualificationList()
    {
        $this->db->select('*');
        $this->db->from('qualification_setup');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }
}

