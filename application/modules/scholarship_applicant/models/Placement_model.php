<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Placement_model extends CI_Model
{
    function getStudentByStudentId($id_student)
    {
        $this->db->select('s.*, p.name as programme_name, i.name as intake_name, qs.name as qualification_name, qs.code as qualification_code');
        $this->db->from('student as s');
        $this->db->join('programme as p', 's.id_program = p.id'); 
        $this->db->join('intake as i', 's.id_intake = i.id'); 
        $this->db->join('qualification_setup as qs', 's.id_degree_type = qs.id');
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }


	function getPlacementListByStudentId($id_student)
	{
        $this->db->select('ia.*, s.name as state, c.name as country');
        $this->db->from('student_placement as ia');
        $this->db->join('state as s', 'ia.id_state = s.id'); 
        $this->db->join('country as c', 'ia.id_country = c.id'); 
        $this->db->where('ia.id_student', $id_student);
        $query = $this->db->get();
         return $query->result();
    }

    function countryListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('country');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();      
        return $result;
    }

    function countryList()
    {
        $this->db->select('*');
        $this->db->from('country');
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();      
        return $result;
    }

    function stateListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('state');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();      
        return $result;
    }


    function stateList()
    {
        $this->db->select('*');
        $this->db->from('state');
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();      
        return $result;
    }


    


    function addPlacement($data)
    {
        $this->db->trans_start();
        $this->db->insert('student_placement', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getPlacement($id)
    {
        $this->db->select('ia.*');
        $this->db->from('student_placement as ia');
         $this->db->where('ia.id', $id);
        $query = $this->db->get();
          $result = $query->row();
         return $result; 
    }

    function checkPlacementCountByStudentId($id_student)
    {
        $check = 0;
        $this->db->select('ia.*');
        $this->db->from('student_placement as ia');
        $this->db->where('ia.id_student', $id_student);
        $this->db->where('ia.status', '0');
        $this->db->or_where('ia.status', '1');
        $this->db->order_by("ia.id", "DESC");
        $query = $this->db->get();
        $result = $query->num_rows();

        return $result;
        // echo "<Pre>";print_r($result . " - " . $max_limit);exit();
    }


    // function generatePlacementNumber()
    // {
    //     $year = date('y');
    //     $Year = date('Y');
    //         $this->db->select('*');
    //         $this->db->from('student_placement');
    //         $this->db->order_by("id", "desc");
    //         $query = $this->db->get();
    //         $result = $query->num_rows();

     
    //         $count= $result + 1;
    //         $generated_number = "INTR" .(sprintf("%'06d", $count)). "/" . $Year;
    //        // echo "<Pre>";print_r($generated_number);exit();
    //        return $generated_number;
    // }

 
    

    // function getRole($id)
    // {
    //     $this->db->select('*');
    //     $this->db->from('roles');
    //     $this->db->where('id', $id);
    //     $query = $this->db->get();
    //         // echo "<pre>";print_r($query);die;
        
    //     return $query->row();
    // }
}