<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>View Internship Application</h3>
        </div>

        <form id="form_pr_entry" action="" method="post">


         <div class="form-container">
                <h4 class="form-group-title">Student Details</h4>
                <div class='data-list'>
                    <div class='row'> 
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student Name :</dt>
                                <dd><?php echo ucwords($studentDetails->full_name);?></dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd><?php echo $studentDetails->nric ?></dd>
                            </dl>
                            <dl>
                                <dt>Student Email :</dt>
                                <dd><?php echo $studentDetails->email_id; ?></dd>
                            </dl>
                             <dl>
                                <dt>Nationality :</dt>
                                <dd><?php echo $studentDetails->nationality ?></dd>
                            </dl>                            
                        </div>        
                        
                        <div class='col-sm-6'>                           
                            <dl>
                                <dt>Intake :</dt>
                                <dd><?php echo $studentDetails->intake_name ?></dd>
                            </dl>
                            <dl>
                                <dt>Program :</dt>
                                <dd><?php echo $studentDetails->programme_name; ?></dd>
                            </dl>
                            <dl>
                                <dt>Qualification Type :</dt>
                                <dd><?php echo $studentDetails->qualification_code . " - " . $studentDetails->qualification_name; ?></dd>
                            </dl>
                            <dl>
                                <dt>Scholarship :</dt>
                                <dd><?php echo ""; ?></dd>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>
            
       

        
        <div class="form-container">
            <h4 class="form-group-title">Internship Application</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Application Number <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="application_number" name="application_number" readonly="readonly" value="<?php echo $internshipApplication->application_number;?>">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Duration (Months) <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="duration" name="duration" readonly="readonly" value="<?php echo $internshipApplication->duration;?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Description <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" readonly="readonly" value="<?php echo $internshipApplication->description;?>">
                    </div>
                </div>

                

            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Company Type <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="company_type" name="company_type" readonly="readonly" value="<?php echo $internshipApplication->company_type_code . " - " . $internshipApplication->company_type_name;?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Company <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="company" name="company" readonly="readonly" value="<?php echo $internshipApplication->registration_no . " - " . $internshipApplication->company_name;?>">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Start Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="from_dt" name="from_dt" autocomplete="off" readonly="readonly" value="<?php echo date('d-m-Y',strtotime($internshipApplication->from_dt));?>">
                    </div>
               </div>

            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Start Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="from_dt" name="from_dt" autocomplete="off" readonly="readonly" value="<?php echo date('d-m-Y',strtotime($internshipApplication->to_dt));?>">
                    </div>
               </div>

               <div class="col-sm-4">
                    <div class="form-group">
                        <label>Reporting Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="to_dt" name="to_dt" autocomplete="off" readonly="readonly" value="<?php echo date('d-m-Y');?>">
                    </div>
               </div>

               <div class="col-sm-4">
                    <div class="form-group">
                        <label>Status <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="date_time" name="date_time" value="<?php
                        if($internshipApplication->status == '0')
                        {
                            echo "Pending";
                        }
                        elseif($internshipApplication->status == '1')
                        {
                            echo "Approved";
                        }
                        elseif($internshipApplication->status == '2')
                        {
                            echo "Rejected";
                        }
                         ?>" readonly="readonly">
                    </div>
                </div>


                <?php
            if($internshipApplication->status == '2')
            {
             ?>

                <div class="col-sm-4" id="view_reject">
                    <div class="form-group">
                        <label>Reject Reason <span class='error-text'>*</span></label>
                        <input type="text" id="reason" name="reason" class="form-control" value="<?php echo $internshipApplication->reason; ?>" readonly>
                    </div>
                </div>

            <?php
            }
            ?>

            </div>


        </div>

         <?php
            if($internshipApplication->is_reporting == '0')
            {
             ?>

            <div class="form-container">
            <h4 class="form-group-title">Rporting Status</h4>


                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <p> Reporting <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                                <input type="radio" id="ed1" name="is_reporting" value="1" onclick="hideRejectField()"><span class="check-radio"></span> Yes
                            </label>
                            <label class="radio-inline">
                                <input type="radio" id="ed2" name="is_reporting" value="0" onclick="showRejectField()"><span class="check-radio"></span> No
                            </label>
                        </div>
                    </div>

                </div>
            </div>

          <?php
            }
            ?>



       

        <div class="button-block clearfix">
            <div class="bttn-group">

             <?php
            if($internshipApplication->is_reporting == '0')
            {
             ?>

                    <button type="submit" class="btn btn-primary btn-lg">Save</button>

              <?php
            }
            ?>


                <a href="../list" class="btn btn-link">Back</a>
            </div>
        </div>

        </form>

    </div>

        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
</div>


<script>
    $('select').select2();



     $(document).ready(function() {
        $("#form_pr_entry").validate({
            rules: {
                is_reporting: {
                    required: true
                }
            },
            messages: {
                is_reporting: {
                    required: "<p class='error-text'>Select Reporting Status</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

</script>