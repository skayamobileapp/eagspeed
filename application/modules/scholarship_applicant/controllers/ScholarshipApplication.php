<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ScholarshipApplication extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('scholarship_application_model');
        $this->load->model('edit_profile_model');
        $this->isScholarApplicantLoggedIn();
    }

    function schemeList()
    {       
        $id_scholar_applicant = $this->session->id_scholar_applicant;
        
        $data['studentDetails'] = $this->scholarship_application_model->getStudentDetails($id_scholar_applicant);
        // if(empty($data['studentDetails']))
        // {
        //     $msz['mesage'] = 'Profile Not Updated Update The Profile';
        //     $this->global['pageTitle'] = 'Student Portal : List Scholarship Application';
        //     $this->loadViews("scholarship/msz", $this->global, $msz, NULL);
        // }
        // if($data['studentDetails']->id_program == 0)
        // {
        //     $msz['mesage'] = 'Program Not Selected Select Program To Apply For Suitable Cohert';
        //     $this->global['pageTitle'] = 'Student Portal : List Scholarship Application';
        //     $this->loadViews("scholarship/msz", $this->global, $msz, NULL);
        // }
        
        // $data['schemeList'] = $this->scholarship_application_model->getSchemesByProgramId($data['studentDetails']->id_program);
        $data['schemeList'] = $this->scholarship_application_model->schemeListByStatus('1');


                 // echo "<Pre>";print_r($data);exit();

        $this->global['pageTitle'] = 'Scholarship Portal : List Scholarship Application';
        $this->loadViews("scholarship/scheme_list", $this->global, $data, NULL);
    }

    function view($id_scheme = NULL)
    {

        if ($id_scheme == null)
        {
            redirect('/scholarship_applicant/scholarshipApplication/schemeList');
        }

        $id_scholar_applicant = $this->session->id_scholar_applicant;
        
        if($this->input->post())
        {
            $id_application = 0;
            $form = $this->input->post();
            // echo "<Pre>"; print_r($form);exit;



            $btn_submit = $this->security->xss_clean($this->input->post('btn_submit'));

            if($btn_submit == 1)
            {
                $father_name = $this->security->xss_clean($this->input->post('father_name'));
                $mother_name = $this->security->xss_clean($this->input->post('mother_name'));
                $father_deceased = $this->security->xss_clean($this->input->post('father_deceased'));
                $father_occupation = $this->security->xss_clean($this->input->post('father_occupation'));
                $no_siblings = $this->security->xss_clean($this->input->post('no_siblings'));
                $est_fee = $this->security->xss_clean($this->input->post('est_fee'));
                $id_programme = $this->security->xss_clean($this->input->post('id_programme'));



                $generated_number = $this->scholarship_application_model->generateScholarshipApplicationNumber();


                $data = array(
                    'id_scholarship_scheme' => $id_scheme,
                    'application_number' => $generated_number,
                    'year' => date('Y'),
                    'father_name' => $father_name,
                    'mother_name' => $mother_name,
                    'father_deceased' => $father_deceased,
                    'father_occupation' => $father_occupation,
                    'no_siblings' => $no_siblings,
                    'id_program' => $id_programme,
                    'id_student' => $id_scholar_applicant,
                    'est_fee' => $est_fee,
                    'status' => 0
                );
                // echo "<Pre>"; print_r($data);exit;

                $id_application = $this->scholarship_application_model->addScholarshipApplication($data);

            }


                $salutation = $this->security->xss_clean($this->input->post('salutation'));
                $first_name = $this->security->xss_clean($this->input->post('first_name'));
                $last_name = $this->security->xss_clean($this->input->post('last_name'));
                $phone = $this->security->xss_clean($this->input->post('phone'));
                $nric = $this->security->xss_clean($this->input->post('nric'));
                $id_type = $this->security->xss_clean($this->input->post('id_type'));
                $id_number = $this->security->xss_clean($this->input->post('id_number'));
                $passport_expiry_date = $this->security->xss_clean($this->input->post('passport_expiry_date'));
                $gender = $this->security->xss_clean($this->input->post('gender'));
                $date_of_birth = $this->security->xss_clean($this->input->post('date_of_birth'));
                $martial_status = $this->security->xss_clean($this->input->post('martial_status'));
                $religion = $this->security->xss_clean($this->input->post('religion'));
                $nationality = $this->security->xss_clean($this->input->post('nationality'));
                $nationality_type = $this->security->xss_clean($this->input->post('nationality_type'));
                $race = $this->security->xss_clean($this->input->post('id_race'));
                $email_id = $this->security->xss_clean($this->input->post('email_id'));
                $mail_address1 = $this->security->xss_clean($this->input->post('mail_address1'));
                $mail_address2 = $this->security->xss_clean($this->input->post('mail_address2'));
                $permanent_address1 = $this->security->xss_clean($this->input->post('permanent_address1'));
                $permanent_address2 = $this->security->xss_clean($this->input->post('permanent_address2'));
                $mailing_zipcode = $this->security->xss_clean($this->input->post('mailing_zipcode'));
                $permanent_zipcode = $this->security->xss_clean($this->input->post('permanent_zipcode'));
                $mailing_country = $this->security->xss_clean($this->input->post('mailing_country'));
                $permanent_country = $this->security->xss_clean($this->input->post('permanent_country'));
                $mailing_state = $this->security->xss_clean($this->input->post('mailing_state'));
                $permanent_state = $this->security->xss_clean($this->input->post('permanent_state'));
                $mailing_city = $this->security->xss_clean($this->input->post('mailing_city'));
                $permanent_city = $this->security->xss_clean($this->input->post('permanent_city'));
                $passport_number = $this->security->xss_clean($this->input->post('passport_number'));
                $id_program = $this->security->xss_clean($this->input->post('id_program'));
                $is_profile_update = $this->security->xss_clean($this->input->post('is_profile_update'));
                




                $data = array(
                    'id_scholar_applicant' => $id_scholar_applicant,
                    'id_application' => $id_application,
                    'id_scholarship_scheme' => $id_scheme,
                    'year' => date('Y'),
                    'full_name' => $salutation.". ".$first_name." ".$last_name,
                    'salutation' => $salutation,
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'phone' => $phone,
                    'nric' => $nric,
                    'id_type' => $id_type,
                    'passport_number' => $passport_number,
                    'passport_expiry_date' => $passport_expiry_date,
                    'gender' => $gender,
                    'date_of_birth' => date('Y-m-d',strtotime($date_of_birth)),
                    'martial_status' => $martial_status,
                    'religion' => $religion,
                    'nationality' => $nationality,
                    'id_program' => $id_program,
                    'id_race' => $race,
                    'email_id' => $email_id,
                    'mail_address1' => $mail_address1,
                    'mail_address2' => $mail_address2,
                    'permanent_address1' => $permanent_address1,
                    'permanent_address2' => $permanent_address2,
                    'mailing_zipcode' => $mailing_zipcode,
                    'permanent_zipcode' => $permanent_zipcode,
                    'mailing_country' => $mailing_country,
                    'permanent_country' => $permanent_country,
                    'mailing_state' => $mailing_state,
                    'permanent_state' => $permanent_state,
                    'mailing_city' => $mailing_city,
                    'permanent_city' => $permanent_city
                );

                if($is_profile_update > 0)
                {
                    $updated_student = $this->scholarship_application_model->updatePersonalData($data,$is_profile_update);
                }
                else
                {
                    $updated_student = $this->scholarship_application_model->addPersonalData($data);
                }
                if($btn_submit == 1)
                {
                    $update_data = array(
                    'id_scholar_applicant' => $id_scholar_applicant,
                    'id_application' => $id_application,
                    'id_scholarship_scheme' => $id_scheme,
                    'year' => date('Y'),
                    'previous_application' => 0
                    );

                    $result = $this->scholarship_application_model->updateApplicationIdToPersonalDetails($update_data);
                }

                // echo "<Pre>";print_r($formData);exit();



                $qualification_level = $this->security->xss_clean($this->input->post('qualification_level'));
                $degree_awarded = $this->security->xss_clean($this->input->post('degree_awarded'));
                $specialization = $this->security->xss_clean($this->input->post('specialization'));
                $class_degree = $this->security->xss_clean($this->input->post('class_degree'));
                $result = $this->security->xss_clean($this->input->post('result'));
                $year = $this->security->xss_clean($this->input->post('year'));
                $medium = $this->security->xss_clean($this->input->post('medium'));
                $college_country = $this->security->xss_clean($this->input->post('college_country'));
                $college_name = $this->security->xss_clean($this->input->post('college_name'));
                $certificate = $this->security->xss_clean($this->input->post('certificate'));
                $transcript = $this->security->xss_clean($this->input->post('transcript'));


                $data = array(
                    'id_scholar_applicant' => $id_scholar_applicant,
                    'id_application' => $id_application,
                    'id_scholarship_scheme' => $id_scheme,
                    'qualification_level' => $qualification_level,
                    'degree_awarded' => $degree_awarded,
                    'specialization' => $specialization,
                    'class_degree' => $class_degree,
                    'result' => $result,
                    'year' => date('Y'),
                    'medium' => $medium,
                    'college_country' => $college_country,
                    'college_name' => $college_name,
                    'certificate' => $certificate,
                    'transcript' => $transcript
                );
                if ($qualification_level != "")
                {

                  

                  $certificate_name = $_FILES['certificate']['name'];
                  
                  $file_name = $_FILES['certificate']['name'];
                  $file_size =$_FILES['certificate']['size'];
                  $file_tmp =$_FILES['certificate']['tmp_name'];
                  $file_type=$_FILES['certificate']['type'];
                  // $file_ext=strtolower(end(explode('.',$_FILES['image']['name'])));
                  $file_ext=explode('.',$file_name);
                  $file_ext=end($file_ext);
                  $file_ext=strtolower($file_ext);


                  $transcript_name = $_FILES['transcript']['name'];
                  $transcript_size = $_FILES['transcript']['size'];
                  $transcript_tmp =$_FILES['transcript']['tmp_name'];

                  $transcript_ext=explode('.',$transcript_name);
                  $transcript_ext=end($transcript_ext);
                  $transcript_ext=strtolower($transcript_ext);
                  

                  // For file validation from 36 to 44 , file size validation

                  $this->fileFormatNSizeValidation($file_ext,$file_size,'Certificate');
                  $this->fileFormatNSizeValidation($transcript_ext,$transcript_size,'Transcript');

                  $data['certificate'] = $this->uploadFile($file_name,$file_tmp,'certificate');
                  $data['transcript'] = $this->uploadFile($transcript_name,$transcript_tmp,'transcript');
                  


                  
                  // if(empty($errors)==true)
                  // {
                        // $date = date('dmY_his');
                        // $upload_path = '/var/www/html/college-management-system/assets/images/';

                        // $fileinfo = pathinfo($name);

                        // $extension = $fileinfo['extension'];
                        // $file_name = $fileinfo['filename'];

                        // $file_path = $upload_path . $date . '.'. $extension;
                        // $uploaded_file_name = $date . '.'. $extension;

                        
                        // echo "<Pre>";print_r($data);exit();


                     // move_uploaded_file($file_tmp,$file_path);
                     // echo "Success";exit();
                  // }else
                  // {
                  //    print_r($errors);exit();
                  // }


                    $result = $this->scholarship_application_model->addExamDetails($data);
                    // echo "<Pre>";print_r($data);exit();
                }
                if($btn_submit == 1)
                {
                    $update_data = array(
                    'id_scholar_applicant' => $id_scholar_applicant,
                    'id_application' => $id_application,
                    'id_scholarship_scheme' => $id_scheme,
                    'year' => date('Y'),
                    'previous_application' => 0
                    );

                    $result = $this->scholarship_application_model->updateApplicationIdToQualificationDetails($update_data);
                }

                $father_name = $this->security->xss_clean($this->input->post('father_name'));
                $mother_name = $this->security->xss_clean($this->input->post('mother_name'));
                $no_siblings = $this->security->xss_clean($this->input->post('no_siblings'));
                $father_deceased = $this->security->xss_clean($this->input->post('father_deceased'));
                $father_occupation = $this->security->xss_clean($this->input->post('father_occupation'));
                $est_fee = $this->security->xss_clean($this->input->post('est_fee'));
                $family_annual_income = $this->security->xss_clean($this->input->post('family_annual_income'));
                $is_family_update = $this->security->xss_clean($this->input->post('is_family_update'));

                $data = array(
                    'id_scholar_applicant' => $id_scholar_applicant,
                    'id_application' => $id_application,
                    'id_scholarship_scheme' => $id_scheme,
                    'father_name' => $father_name,
                    'mother_name' => $mother_name,
                    'year' => date('Y'),
                    'no_siblings' => $no_siblings,
                    'father_deceased' => $father_deceased,
                    'father_occupation' => $father_occupation,
                    'family_annual_income' => $family_annual_income,
                    'est_fee' => $est_fee
                );
                if ($father_name != "")
                {
                    if($is_family_update > 0)
                    {
                        $result = $this->scholarship_application_model->updateFamilyDetails($data,$is_family_update);
                    }else
                    {
                        $result = $this->scholarship_application_model->addFamilyDetails($data);
                    }
                }
                if($btn_submit == 1)
                {
                    $update_data = array(
                    'id_scholar_applicant' => $id_scholar_applicant,
                    'id_application' => $id_application,
                    'id_scholarship_scheme' => $id_scheme,
                    'year' => date('Y'),
                    'previous_application' => 0
                    );

                    $result = $this->scholarship_application_model->updateApplicationIdToFamilyDetails($update_data);
                }

               $company_name = $this->security->xss_clean($this->input->post('company_name'));
                $company_address = $this->security->xss_clean($this->input->post('company_address'));
                $telephone = $this->security->xss_clean($this->input->post('telephone'));
                $fax_num = $this->security->xss_clean($this->input->post('fax_num'));
                $designation = $this->security->xss_clean($this->input->post('designation'));
                $position = $this->security->xss_clean($this->input->post('position'));
                $service_year = $this->security->xss_clean($this->input->post('service_year'));
                $industry = $this->security->xss_clean($this->input->post('industry'));
                $job_desc = $this->security->xss_clean($this->input->post('job_desc'));
                $employment_letter = $this->security->xss_clean($this->input->post('employment_letter'));

                $data = array(
                    'id_scholar_applicant' => $id_scholar_applicant,
                    'id_application' => $id_application,
                    'id_scholarship_scheme' => $id_scheme,
                    'company_name' => $company_name,
                    'company_address' => $company_address,
                    'year' => date('Y'),
                    'telephone' => $telephone,
                    'fax_num' => $fax_num,
                    'designation' => $designation,
                    'position' => $position,
                    'service_year' => $service_year,
                    'industry' => $industry,
                    'job_desc' => $job_desc,
                    'employment_letter' => $employment_letter
                );
                
                if ($company_name != "")
                {


                  $employment_letter_name = $_FILES['employment_letter']['name'];
                  $employment_letter_size = $_FILES['employment_letter']['size'];
                  $employment_letter_tmp =$_FILES['employment_letter']['tmp_name'];

                  $employment_letter_ext=explode('.',$employment_letter_name);
                  $employment_letter_ext=end($employment_letter_ext);
                  $employment_letter_ext=strtolower($employment_letter_ext);
                  

                  // For file validation from 36 to 44 , file size validation
                  $this->fileFormatNSizeValidation($employment_letter_ext,$employment_letter_size,'Employment Letter');

                  $data['employment_letter'] = $this->uploadFile($employment_letter_name,$employment_letter_tmp,'Employment Letter');



                    $result = $this->edit_profile_model->addEmploymentDetails($data);
                }
                if($btn_submit == 1)
                {
                    $update_data = array(
                    'id_scholar_applicant' => $id_scholar_applicant,
                    'id_application' => $id_application,
                    'id_scholarship_scheme' => $id_scheme,
                    'year' => date('Y'),
                    'previous_application' => 0
                    );

                    $result = $this->scholarship_application_model->updateApplicationIdToEmploymentDetails($update_data);
                }

            if($btn_submit == 1)
            {
                redirect('/scholarship_applicant/scholarshipApplication/schemeList');
            }
            else
            {
                redirect($_SERVER['HTTP_REFERER']);
            }
        }

        $check_applied = $this->scholarship_application_model->checkSchemeAppliedByStudent($id_scheme,$id_scholar_applicant);
        if($check_applied)
        {
            $msz['message'] = 'Already Application Submitted To This Scheme REF. No : ' . $check_applied->application_number;
            $this->viewMsz($msz);

            // $this->global['pageTitle'] = 'Scholarship Portal : Status';
            // $this->loadViews("scholarship/msz", $this->global, $msz, NULL);
        // echo "<Pre>"; print_r($check_applied);exit;
        }


        // echo "<Pre>"; print_r($data['studentApplicatoinDetails']);exit;

        $data['studentApplicatoinDetails'] = $this->scholarship_application_model->getScholarshipApplicationPersonalDetails($id_scholar_applicant,$id_scheme);
        $data['studentDetails'] = $this->scholarship_application_model->getStudentDetails($id_scholar_applicant);
        $data['profileDetails'] = $this->edit_profile_model->getStudentDetails($id_scholar_applicant);
        


        $data['countryList'] = $this->edit_profile_model->countryList();
        $data['stateList'] = $this->edit_profile_model->stateList();
        $data['raceList'] = $this->edit_profile_model->raceListByStatus('1');
        $data['religionList'] = $this->edit_profile_model->religionListByStatus('1');
        $data['programList'] = $this->edit_profile_model->programListByStatus('1');

        $data['educationDetails'] = $this->scholarship_application_model->getExamDetails($id_scholar_applicant,$id_scheme);
        $data['familyDetails'] = $this->scholarship_application_model->getFamilyDetails($id_scholar_applicant,$id_scheme);
        $data['employmentDetails'] = $this->scholarship_application_model->getEmploymentDetails($id_scholar_applicant,$id_scheme);
        $data['scheme'] = $this->scholarship_application_model->getSchemeById($id_scheme);
        $data['programList'] = $this->scholarship_application_model->getProgramBySchemeId($id_scheme);

        // $root = $_SERVER['DOCUMENT_ROOT'];
        // echo $root;exit;

            // echo "<Pre>"; print_r($data['programList']);exit;
        // echo microtime();exit;


        $this->global['pageTitle'] = 'Student Portal : View Scholarship Application';
        $this->loadViews("scholarship/view", $this->global, $data, NULL);
    }


    function viewMsz($msz)
    {
            // $msz['mesage'] = 'Profile Not Updated Update The Profile';
        // echo "<Pre>"; print_r($msz['message']);exit;

            $this->global['pageTitle'] = 'Scholarship Portal : Status';
            $this->loadViews("scholarship/msz", $this->global, $msz, NULL);
    }


    function list()
    {       
        $id_scholar_applicant = $this->session->id_scholar_applicant;
        
        $data['studentDetails'] = $this->scholarship_application_model->getStudentDetails($id_scholar_applicant);

        $data['scholarshipApplicationList'] = $this->scholarship_application_model->getScholarshipApplicationListByStudentId($id_scholar_applicant);


        // $data['maxLimit'] = $this->scholarship_application_model->getMaxLimit();
        // $data['check_limit'] = $this->scholarship_application_model->checkStudentScholarshipApplication($check);


                 // echo "<Pre>";print_r($data);exit();

        $this->global['pageTitle'] = 'Student Portal : List Scholarship Application';
        $this->loadViews("scholarship/list", $this->global, $data, NULL);
    }

    function viewApplication($id_application,$id_scheme)
    {

        $id_scholar_applicant = $this->session->id_scholar_applicant;

        if ($id_application == null)
        {
            redirect('/scholarship_applicant/scholarshipApplication/list');
        }
        if($this->input->post())
        {
            redirect('/scholarship_applicant/scholarshipApplication/list');
        }

        $data['studentDetails'] = $this->scholarship_application_model->getStudentDetails($id_scholar_applicant);
        // if(empty($data['studentDetails']))
        // {
        //     $msz['message'] = 'Profile Not Updated Update The Profile';
        //     $this->viewMsz($msz);
        // }
        // if($data['studentDetails']->id_program == 0)
        // {
        //     $msz['message'] = 'Program Not Selected Select Program To Apply For Suitable Cohert';
        //     $this->viewMsz($msz);
        // }


        $data['countryList'] = $this->edit_profile_model->countryList();
        $data['stateList'] = $this->edit_profile_model->stateList();
        $data['raceList'] = $this->edit_profile_model->raceListByStatus('1');
        $data['religionList'] = $this->edit_profile_model->religionListByStatus('1');
        $data['programList'] = $this->edit_profile_model->programListByStatus('1');

        $data['profileDetails'] = $this->scholarship_application_model->getApplicationPersonalDetailsByApplicationId($id_scholar_applicant,$id_scheme,$id_application);

        $data['educationDetails'] = $this->scholarship_application_model->getExamDetailsByApplicationId($id_scholar_applicant,$id_scheme,$id_application);
        $data['familyDetails'] = $this->scholarship_application_model->getFamilyDetailsByApplicationId($id_scholar_applicant,$id_scheme,$id_application);
        $data['employmentDetails'] = $this->scholarship_application_model->getEmploymentDetailsByApplicationId($id_scholar_applicant,$id_scheme,$id_application);
        $data['scheme'] = $this->scholarship_application_model->getSchemeById($id_scheme);
        $data['scholarship'] = $this->scholarship_application_model->getScholarship($id_application);

        // echo "<Pre>"; print_r($data);exit;

        $this->global['pageTitle'] = 'Student Portal : View Scholarship Application';
        $this->loadViews("scholarship/view_application", $this->global, $data, NULL);


    }
    
    function add()
    {

        $id_student = $this->session->id_student;
        $id_intake = $this->session->id_intake;
        $id_program = $this->session->id_program;
        $id_qualification = $this->session->id_qualification;

        if($this->input->post())
        {
            $form = $this->input->post();


            $id_scholarship_scheme = $this->security->xss_clean($this->input->post('id_scholarship_scheme'));

            $father_name = $this->security->xss_clean($this->input->post('father_name'));
            $mother_name = $this->security->xss_clean($this->input->post('mother_name'));
            $father_deceased = $this->security->xss_clean($this->input->post('father_deceased'));
            $father_occupation = $this->security->xss_clean($this->input->post('father_occupation'));
            $no_siblings = $this->security->xss_clean($this->input->post('no_siblings'));
            $year = $this->security->xss_clean($this->input->post('year'));
            $result_item = $this->security->xss_clean($this->input->post('result_item'));
            $max_marks = $this->security->xss_clean($this->input->post('max_marks'));
            $obtained_marks = $this->security->xss_clean($this->input->post('obtained_marks'));
            $percentage = $this->security->xss_clean($this->input->post('percentage'));
            $est_fee = $this->security->xss_clean($this->input->post('est_fee'));


            $generated_number = $this->scholarship_application_model->generateScholarshipApplicationNumber();


            $data = array(
                'id_scholarship_scheme' => $id_scholarship_scheme,
                'application_number' => $generated_number,
                'father_name' => $father_name,
                'mother_name' => $mother_name,
                'father_deceased' => $father_deceased,
                'father_occupation' => $father_occupation,
                'no_siblings' => $no_siblings,
                'year' => $year,
                'result_item' => $result_item,
                'max_marks' => $max_marks,
                'obtained_marks' => $obtained_marks,
                'percentage' => $percentage,
                'id_intake' => $id_intake,
                'id_program' => $id_program,
                'id_student' => $id_student,
                'id_qualification' => $id_qualification,
                'est_fee' => $est_fee,
                'status' => 0
            );

            // $check_limit = $this->scholarship_application_model->checkStudentScholarshipApplication($data);
            // if($check_limit == 1)
            // {
            //      echo "<Pre>";print_r("Max Application Limit Reached For Active & Pending Scholarship Application");exit();
            // }

            // echo "<Pre>";print_r($data);exit();


            $insert_id = $this->scholarship_application_model->addScholarshipApplication($data);
            redirect('/student/scholarshipApplication/list');
        }

        
        $check['id_student'] = $this->session->id_student;
        $check['id_intake'] = $this->session->id_intake;
        $check['id_program'] = $this->session->id_program;


        $data['studentDetails'] = $this->scholarship_application_model->getStudentByStudentId($id_student);
        $data['schemeList'] = $this->scholarship_application_model->getSchemesByProgramId($id_program);
        
        // echo "<Pre>";print_r($data);exit();

        $this->global['pageTitle'] = 'Student Portal : Add Scholarship Application';
        $this->loadViews("scholarship/add", $this->global, $data, NULL);
    }


    





    function getPercentage()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
            // echo "<Pre>"; print_r($tempData);exit;

        $type = $tempData['type'];
        $max_marks = $tempData['max_marks'];
        $obtained_marks = $tempData['obtained_marks'];
       
       switch ($type)
       {
            case 'Band':

                $one_percent = $max_marks/100;
                $percentage = $obtained_marks/$one_percent;
                $percentage = round($percentage,2);
               break;

            case 'CGPA':

                $one_percent = $max_marks/10;
                $percentage = $obtained_marks/$one_percent;
               break;

            // case 'Grade':

            //     $one_percent = $max_marks/100;
            //     $percentage = $obtained_marks/$one_percent;

            //     $percentage = getGrade($percentage);

               
            //    break;

            // case 'Marks':

                

            //    # code...
            //    break;
           
           default:
                $one_percent = $max_marks/100;
                $percentage = $obtained_marks/$one_percent;
                $percentage = round($percentage,2);
               break;
       }
        echo $percentage;
            exit;
    }

    function getRequirementsByIdProgram($id_scholarship_program)
    {
        $program = $this->scholarship_application_model->getProgram($id_scholarship_program);
        $isPersonel = $this->scholarship_application_model->getIsPersonelDetails($id_scholarship_program);
        $isQualification = $this->scholarship_application_model->getIsQualificationDetails($id_scholarship_program);
        $isFamily = $this->scholarship_application_model->getIsFamilyDetails($id_scholarship_program);
        $isEmployment = $this->scholarship_application_model->getIsEmploymentDetails($id_scholarship_program);




        $is_personel_details = $program->is_personel_details; 
        $is_education_details = $program->is_education_details; 
        $is_family_details = $program->is_family_details; 
        $is_financial_recoverable_details = $program->is_financial_recoverable_details; 
        $is_file_upload_details = $program->is_file_upload_details; 
        $is_employment_details = $program->is_employment_details; 





        $is_salutation = $isPersonel->is_salutation; 
        $is_first_name = $isPersonel->is_first_name; 
        $is_last_name = $isPersonel->is_last_name; 
        $is_nric = $isPersonel->is_nric; 
        $is_passport = $isPersonel->is_passport; 
        $is_phone = $isPersonel->is_phone; 
        $is_email_id = $isPersonel->is_email_id; 
        $is_passport_expiry_date = $isPersonel->is_passport_expiry_date;
        $is_gender = $isPersonel->is_gender; 
        $is_date_of_birth = $isPersonel->is_date_of_birth; 
        $is_martial_status = $isPersonel->is_martial_status; 
        $is_religion = $isPersonel->is_religion; 
        $is_nationality = $isPersonel->is_nationality; 
        $is_id_race = $isPersonel->is_id_race; 
        $is_mail_address1 = $isPersonel->is_mail_address1; 
        $is_mail_address2 = $isPersonel->is_mail_address2; 
        $is_mailing_country = $isPersonel->is_mailing_country; 
        $is_mailing_state = $isPersonel->is_mailing_state; 
        $is_mailing_city = $isPersonel->is_mailing_city; 
        $is_mailing_zipcode = $isPersonel->is_mailing_zipcode; 
        $is_permanent_address1 = $isPersonel->is_permanent_address1; 
        $is_permanent_address2 = $isPersonel->is_permanent_address2; 
        $is_permanent_country = $isPersonel->is_permanent_country; 
        $is_permanent_state = $isPersonel->is_permanent_state; 
        $is_permanent_city = $isPersonel->is_permanent_city; 
        $is_permanent_zipcode = $isPersonel->is_permanent_zipcode;
        $is_id_type = $isPersonel->is_id_type;
        $is_passport_number = $isPersonel->is_passport_number;





        $is_qualification_level = $isQualification->is_qualification_level; 
        $is_degree_awarded = $isQualification->is_degree_awarded; 
        $is_specialization = $isQualification->is_specialization; 
        $is_class_degree = $isQualification->is_class_degree; 
        $is_result = $isQualification->is_result; 
        $is_year = $isQualification->is_year; 
        $is_medium = $isQualification->is_medium; 
        $is_college_country = $isQualification->is_college_country; 
        $is_college_name = $isQualification->is_college_name; 
        $is_certificate = $isQualification->is_certificate; 
        $is_transcript = $isQualification->is_transcript; 




        $is_father_name = $isFamily->is_father_name; 
        $is_mother_name = $isFamily->is_mother_name; 
        $is_father_deceased = $isFamily->is_father_deceased; 
        $is_father_occupation = $isFamily->is_father_occupation; 
        $is_no_siblings = $isFamily->is_no_siblings; 
        $is_est_fee = $isFamily->is_est_fee; 
        $is_family_annual_income = $isFamily->is_family_annual_income; 





        $is_company_name = $isEmployment->is_company_name; 
        $is_company_address = $isEmployment->is_company_address; 
        $is_telephone = $isEmployment->is_telephone; 
        $is_fax_num = $isEmployment->is_fax_num; 
        $is_designation = $isEmployment->is_designation; 
        $is_position = $isEmployment->is_position; 
        $is_service_year = $isEmployment->is_service_year; 
        $is_industry = $isEmployment->is_industry; 
        $is_job_desc = $isEmployment->is_job_desc; 
        $is_employment_letter = $isEmployment->is_employment_letter; 
        

        $table = "
            <input type='text' name='personel_details' id='personel_details' value='$is_personel_details' />
            <input type='text' name='education_details' id='education_details' value='$is_education_details' />
            <input type='text' name='family_details' id='family_details' value='$is_family_details' />
            <input type='text' name='financial_recoverable_details' id='financial_recoverable_details' value='$is_financial_recoverable_details' />
            <input type='text' name='file_upload_details' id='file_upload_details' value='$is_file_upload_details' />
            <input type='text' name='employment_details' id='employment_details' value='$is_employment_details' />

            <input type='text' name='is_salutation' id='is_salutation' value='$is_salutation' />
            <input type='text' name='is_first_name' id='is_first_name' value='$is_first_name' />
            <input type='text' name='is_last_name' id='is_last_name' value='$is_last_name' />
            <input type='text' name='is_nric' id='is_nric' value='$is_nric' />
            <input type='text' name='is_passport' id='is_passport' value='$is_passport' />
            <input type='text' name='is_phone' id='is_phone' value='$is_phone' />
            <input type='text' name='is_email_id' id='is_email_id' value='$is_email_id' />
            <input type='text' name='is_passport_expiry_date' id='is_passport_expiry_date' value='$is_passport_expiry_date' />
            <input type='text' name='is_gender' id='is_gender' value='$is_gender' />
            <input type='text' name='is_date_of_birth' id='is_date_of_birth' value='$is_date_of_birth' />
            <input type='text' name='is_martial_status' id='is_martial_status' value='$is_martial_status' />
            <input type='text' name='is_religion' id='is_religion' value='$is_religion' />
            <input type='text' name='is_nationality' id='is_nationality' value='$is_nationality' />
            <input type='text' name='is_id_race' id='is_id_race' value='$is_id_race' />
            <input type='text' name='is_mail_address1' id='is_mail_address1' value='$is_mail_address1' />
            <input type='text' name='is_mail_address2' id='is_mail_address2' value='$is_mail_address2' />
            <input type='text' name='is_mailing_country' id='is_mailing_country' value='$is_mailing_country' />
            <input type='text' name='is_mailing_state' id='is_mailing_state' value='$is_mailing_state' />
            <input type='text' name='is_mailing_city' id='is_mailing_city' value='$is_mailing_city' />
            <input type='text' name='is_mailing_zipcode' id='is_mailing_zipcode' value='$is_mailing_zipcode' />
            <input type='text' name='is_permanent_address1' id='is_permanent_address1' value='$is_permanent_address1' />
            <input type='text' name='is_permanent_address2' id='is_permanent_address2' value='$is_permanent_address2' />
            <input type='text' name='is_permanent_country' id='is_permanent_country' value='$is_permanent_country' />
            <input type='text' name='is_permanent_state' id='is_permanent_state' value='$is_permanent_state' />
            <input type='text' name='is_permanent_city' id='is_permanent_city' value='$is_permanent_city' />
            <input type='text' name='is_permanent_zipcode' id='is_permanent_zipcode' value='$is_permanent_zipcode' />
            <input type='text' name='is_id_type' id='is_id_type' value='$is_id_type' />
            <input type='text' name='is_passport_number' id='is_passport_number' value='$is_passport_number' />

            <input type='text' name='is_qualification_level' id='is_qualification_level' value='$is_qualification_level' />
            <input type='text' name='is_degree_awarded' id='is_degree_awarded' value='$is_degree_awarded' />
            <input type='text' name='is_specialization' id='is_specialization' value='$is_specialization' />
            <input type='text' name='is_class_degree' id='is_class_degree' value='$is_class_degree' />
            <input type='text' name='is_result' id='is_result' value='$is_result' />
            <input type='text' name='is_year' id='is_year' value='$is_year' />
            <input type='text' name='is_medium' id='is_medium' value='$is_medium' />
            <input type='text' name='is_college_country' id='is_college_country' value='$is_college_country' />
            <input type='text' name='is_college_name' id='is_college_name' value='$is_college_name' />
            <input type='text' name='is_certificate' id='is_certificate' value='$is_certificate' />
            <input type='text' name='is_transcript' id='is_transcript' value='$is_transcript' />

            <input type='text' name='is_father_name' id='is_father_name' value='$is_father_name' />
            <input type='text' name='is_mother_name' id='is_mother_name' value='$is_mother_name' />
            <input type='text' name='is_father_deceased' id='is_father_deceased' value='$is_father_deceased' />
            <input type='text' name='is_father_occupation' id='is_father_occupation' value='$is_father_occupation' />
            <input type='text' name='is_no_siblings' id='is_no_siblings' value='$is_no_siblings' />
            <input type='text' name='is_est_fee' id='is_est_fee' value='$is_est_fee' />
            <input type='text' name='is_family_annual_income' id='is_family_annual_income' value='$is_family_annual_income' />

            <input type='text' name='is_company_name' id='is_company_name' value='$is_company_name' />
            <input type='text' name='is_company_address' id='is_company_address' value='$is_company_address' />
            <input type='text' name='is_telephone' id='is_telephone' value='$is_telephone' />
            <input type='text' name='is_fax_num' id='is_fax_num' value='$is_fax_num' />
            <input type='text' name='is_designation' id='is_designation' value='$is_designation' />
            <input type='text' name='is_position' id='is_position' value='$is_position' />
            <input type='text' name='is_service_year' id='is_service_year' value='$is_service_year' />
            <input type='text' name='is_industry' id='is_industry' value='$is_industry' />
            <input type='text' name='is_job_desc' id='is_job_desc' value='$is_job_desc' />
            <input type='text' name='is_employment_letter' id='is_employment_letter' value='$is_employment_letter' />
            ";



        echo "<Pre>";print_r($table);exit();

    }






















    function diffDates()
    {
        // // Declare two dates 
        // $start_date = strtotime("2018-06-08"); 
        // $end_date = strtotime("2018-09-01"); 
          
        // // Get the difference and divide into  
        // // total no. seconds 60/60/24 to get  
        // // number of days 
        // echo "Difference between two dates: "
        //     . ($end_date - $start_date)/60/60/24;exit();
    }
}

