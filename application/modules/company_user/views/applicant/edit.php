<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Employee</h3>
        </div>

        
        <form id="form_applicant" action="" method="post">



          <div class="form-container">
              <h4 class="form-group-title">Profile Details</h4>      


              <div class="row">
                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Salutation <span class='error-text'>*</span></label>
                          <select name="salutation" id="salutation" class="form-control">
                              <?php
                              if (!empty($salutationList)) {
                                  foreach ($salutationList as $record) {
                              ?>
                                      <option value="<?php echo $record->id;  ?>"
                                          <?php if($getApplicantDetails->salutation==$record->id)
                                          {
                                              echo "selected=selected";
                                          }
                                          ?>
                                          >
                                          <?php echo $record->name;  ?>        
                                      </option>
                              <?php
                                  }
                              }
                              ?>
                          </select>
                      </div>
                  </div>
                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>First Name <span class='error-text'>*</span></label>
                          <input type="text" class="form-control" id="first_name" name="first_name" value="<?php echo $getApplicantDetails->first_name ?>">
                      </div>
                  </div>
                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Last Name <span class='error-text'>*</span></label>
                          <input type="text" class="form-control" id="last_name" name="last_name" value="<?php echo $getApplicantDetails->last_name ?>">
                      </div>
                  </div>
              
              </div>


              <div class="row">

                  <div class="col-sm-4">
                     <div class="form-group">
                        <label>Nationality <span class='error-text'>*</span></label>
                        <select name="nationality" id="nationality" class="form-control">
                           <option value="">Select</option>
                           <?php
                              if(!empty($nationalityList))
                              {
                                foreach ($nationalityList as $record)
                                {
                              ?>
                           <option value="<?php echo $record->id;  ?>"
                              <?php if($getApplicantDetails->nationality==$record->id)
                                 {
                                     echo "selected=selected";
                                 }
                                 ?>
                              >
                              <?php echo $record->name;  ?>        
                           </option>
                            <?php
                                }
                              }
                              ?>
                        </select>
                     </div>
                  </div>

                  <div class="col-sm-4">
                    <div class="form-group">
                      <label>ID Type <span class='error-text'>*</span></label>
                       <select name="id_type" id="id_type" class="form-control" required onchange="getlabel()">
                        <option value="">Select</option>
                          <option value="NRIC"
                          <?php if($getApplicantDetails->id_type == 'NRIC')
                           {
                               echo "selected=selected";
                           }
                           ?>
                          >NRIC</option>
                          <option value="PASSPORT"
                          <?php if($getApplicantDetails->id_type == 'PASSPORT')
                           {
                               echo "selected=selected";
                           }
                           ?>>PASSPORT</option>
                      </select>
                    </div>
                  </div>

                  <div class="col-sm-4">
                      <div class="form-group">
                          <label><span id='labelspanid'></span> <span class='error-text'>*</span></label>
                          <input type="text" class="form-control" id="nric" name="nric" value="<?php echo $getApplicantDetails->nric ?>" onblur="getNRICDuplication()">
                      </div>
                  </div>

              </div>


              <div class="row">

                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Date Of Birth <span class='error-text'>*</span></label>
                          <input type="text" class="form-control datepicker" id="date_of_birth" name="date_of_birth" autocomplete="off" value="<?php echo $getApplicantDetails->date_of_birth ?>">
                      </div>
                  </div>


                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Email <span class='error-text'>*</span></label>
                          <input type="text" class="form-control" id="email_id" name="email_id" value="<?php echo $getApplicantDetails->email_id ?>" onblur="getEmailIdDuplication()">
                      </div>
                  </div>


                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Password <span class='error-text'>*</span></label>
                          <input type="password" class="form-control" id="password" name="password" value="<?php echo $getApplicantDetails->password ?>" readonly>
                      </div>
                  </div>

              </div>


              <div class="row">

                  <div class="col-sm-4">
                      <label>Phone Number <span class='error-text'>*</span></label>
                        <div class="row">
                           <div class="col-sm-4">

                             <select name="country_code" id="country_code" class="form-control" required>
                              <option value="">Select</option>                    
                              <?php
                                  if (!empty($countryList))
                                  {
                                    foreach ($countryList as $record)
                                    {
                                      if($record->phone_code != '')
                                      {

                                  ?>
                               <option value="<?php echo $record->phone_code;  ?>"
                                  <?php
                                  if($getApplicantDetails->country_code == $record->phone_code)
                                  {
                                      echo 'selected';
                                  }
                                  ?>
                                  >
                                  <?php echo $record->phone_code . " - " . $record->name;  ?>
                               </option>
                                 <?php
                                      }
                                  }
                                }
                              ?>
                            </select>
                      </div>
                      <div class="col-sm-8">
                        <input type="number" class="form-control" id="phone" name="phone" value="<?php echo $getApplicantDetails->phone; ?>">
                      </div>
                      </div>
                  </div>


                  
                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Gender <span class='error-text'>*</span></label>
                          <select class="form-control" id="gender" name="gender">
                              <option value="">SELECT</option>
                              <option value="Male" <?php if($getApplicantDetails->gender=='Male'){ echo "selected"; } ?>>MALE</option>
                              <option value="Female" <?php if($getApplicantDetails->gender=='Female'){ echo "selected"; } ?>>FEMALE</option>
                              <!-- <option value="Others"<?php if($getApplicantDetails->gender=='Others'){ echo "selected"; } ?> >OTHERS</option> -->
                          </select>
                      </div>
                  </div>

              </div>


          </div>



          <div class="form-container">
               <h4 class="form-group-title">Other Details</h4>
               <div class="row">
                  <div class="col-sm-4">
                    <div class="form-group">
                        <label>Whatsapp Number <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="whatsapp_number" name="whatsapp_number" value="<?php echo $getApplicantDetails->whatsapp_number; ?>">
                     </div>
                  </div>

                   <div class="col-sm-4">
                    <div class="form-group">
                        <label>LinkedIn ID/Link: <span class='error-text'></span></label>
                        <input type="text" class="form-control" id="linked_in" name="linked_in" value="<?php echo $getApplicantDetails->linked_in; ?>">
                     </div>
                  </div>

                   <div class="col-sm-4">
                    <div class="form-group">
                        <label>Facebook ID/ Link <span class='error-text'></span></label>
                        <input type="text" class="form-control" id="facebook_id" name="facebook_id" value="<?php echo $getApplicantDetails->facebook_id; ?>">
                     </div>
                  </div>

                   <div class="col-sm-4">
                    <div class="form-group">
                        <label>Twitter ID/Link: <span class='error-text'></span></label>
                        <input type="text" class="form-control" id="twitter_id" name="twitter_id" value="<?php echo $getApplicantDetails->twitter_id; ?>">
                     </div>
                  </div>

                   <div class="col-sm-4">
                    <div class="form-group">
                        <label>Instagram ID/ Link <span class='error-text'></span></label>
                        <input type="text" class="form-control" id="ig_id" name="ig_id" value="<?php echo $getApplicantDetails->ig_id; ?>">
                     </div>
                  </div>

                </div>
              
          </div>


          <div class="form-container">
              <h4 class="form-group-title">Application Details</h4>

              <div class="row">
               
               <div class="col-sm-4">
                    <div class="form-group">
                        <label>Applicant Status <span class='error-text'>*</span></label>
                        <input type="text" id="user_name" name="user_name" class="form-control" value="<?php echo $getApplicantDetails->applicant_status ?>" readonly>
                    </div>
                </div>

              </div>     
          </div>  


          <div class="button-block clearfix">
              <div class="bttn-group">
                  <button type="submit" class="btn btn-primary btn-lg">Save</button>
                  <a href="../list" class="btn btn-link">Back</a>
              </div>
          </div>






            

        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    $('select').select2();

    $(function()
    {
      $( ".datepicker" ).datepicker({
          changeYear: true,
          changeMonth: true,
          yearRange: "1960:2001"
      });
    });
  

    function getlabel()
    {
      var labelnric = $("#id_type").val();
      $("#labelspanid").html(labelnric);
    }


    function getEmailIdDuplication()
    {
      var email_id = $("#email_id").val()

      if(email_id != '')
      {

        var tempPR = {};
        tempPR['id_employee'] = <?php echo $getApplicantDetails->id ?>;
        tempPR['email_id'] = email_id;
        tempPR['nric'] = '';
        
        // alert(tempPR['email_id']);

        $.ajax(
        {
           url: '/company_user/applicant/getEmailIdDuplication',
            type: 'POST',
           data:
           {
            tempData: tempPR
           },
           error: function()
           {
            alert('Something is wrong');
           },
           success: function(result)
           {
              // alert(result);
              if(result == '0')
              {
                  alert('Duplicate E-Mail Id Not Allowed, User Already Registered With The Given Email Id : '+ tempPR['email_id'] );
                  $("#email_id").val('');
              }
           }
        });
      }
    }


    function getNRICDuplication()
    {
        var labelnric = $("#id_type").val();
        var nric = $("#nric").val();


        if(nric != '')
        {

        if(labelnric != '')
        {
            var tempPR = {};
            tempPR['id_employee'] = <?php echo $getApplicantDetails->id ?>;;
            tempPR['email_id'] = '';
            tempPR['nric'] = nric;
            
            // alert(tempPR['email_id']);

            $.ajax(
            {
               url: '/company_user/applicant/getNRICDuplication',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                  // alert(result);
                  if(result == '0')
                  {
                      alert('Duplicate '+ labelnric +' Not Allowed, User Already Registered With The '+ labelnric+' : '+ tempPR['nric'] );
                      $("#nric").val('');
                  }
               }
            });
            
          }else
          {
            alert('Select The Id Type');
            $("#nric").val('');
          }
        }
    }


   
   $(document).ready(function()
   {
        getlabel();
        $("#form_applicant").validate({
            rules: {
                salutation: {
                    required: true
                },
                 first_name: {
                    required: true
                },
                 last_name: {
                    required: true
                },
                 phone: {
                    required: true
                },
                 email_id: {
                    required: true
                },
                 password: {
                    required: true
                },
                 nric: {
                    required: true
                },
                 gender: {
                    required: true
                },
                 id_program: {
                    required: true
                },
                 whatsapp_number: {
                    required: true
                },
                employee_discount :{
                    required : true
                },
                sibbling_discount :{
                    required : true
                },
                 sibbling_name: {
                    required: true
                },
                 sibbling_nric: {
                    required: true
                },
                 employee_name: {
                    required: true
                },
                 employee_nric: {
                    required: true
                },
                 employee_designation: {
                    required: true
                },
                 date_of_birth: {
                    required: true
                },
                 martial_status: {
                    required: true
                },
                 religion: {
                    required: true
                },
                 nationality: {
                    required: true
                },
                 id_race: {
                    required: true
                },
                 mail_address1: {
                    required: true
                },
                 mailing_city: {
                    required: true
                },
                 mailing_country: {
                    required: true
                },
                 mailing_state: {
                    required: true
                },
                 mailing_zipcode: {
                    required: true
                },
                 permanent_address1: {
                    required: true
                },
                 permanent_city: {
                    required: true
                },
                 permanent_country: {
                    required: true
                },
                 permanent_state: {
                    required: true
                },
                 permanent_zipcode: {
                    required: true
                },
                is_hostel: {
                    required: true
                },
                id_degree_type: {
                    required: true
                }
            },
            messages: {
                salutation: {
                    required: "<p class='error-text'>Salutation required</p>",
                },
                first_name: {
                    required: "<p class='error-text'>First Name required</p>",
                },
                last_name: {
                    required: "<p class='error-text'>Last Name required</p>",
                },
                email_id: {
                    required: "<p class='error-text'>Email required</p>",
                },
                phone: {
                    required: "<p class='error-text'>Phone required</p>",
                },
                gender: {
                    required: "<p class='error-text'>Gender required</p>",
                },
                nric: {
                    required: "<p class='error-text'>NRIC required</p>",
                },
                password: {
                    required: "<p class='error-text'>Password required</p>",
                },
                id_program: {
                    required: "<p class='error-text'>Select Program</p>",
                },
                whatsapp_number: {
                    required: "<p class='error-text'>Whatsapp No. Required</p>",
                },
                employee_discount: {
                    required: "<p class='error-text'>Employee Discount required</p>",
                },
                sibbling_discount: {
                    required: "<p class='error-text'>Sibbling Discount required</p>",
                },
                sibbling_name: {
                    required: "<p class='error-text'>Sibling Name required</p>",
                },
                sibbling_nric: {
                    required: "<p class='error-text'>Sibling NRIC required</p>",
                },
                employee_name: {
                    required: "<p class='error-text'>Employee Name required</p>",
                },
                employee_nric: {
                    required: "<p class='error-text'>Employee NRIC required</p>",
                },
                employee_designation: {
                    required: "<p class='error-text'>Employee Designation required</p>",
                },
                date_of_birth: {
                    required: "<p class='error-text'>Select Date Of Birth</p>",
                },
                martial_status: {
                    required: "<p class='error-text'>Select Maritual Status</p>",
                },
                religion: {
                    required: "<p class='error-text'>Select Religion</p>",
                },
                nationality: {
                    required: "<p class='error-text'>Select Type Of Nationality</p>",
                },
                id_race: {
                    required: "<p class='error-text'>Select Race</p>",
                },
                mail_address1: {
                    required: "<p class='error-text'>Enter Mailing Address 1</p>",
                },
                mailing_city: {
                    required: "<p class='error-text'>Enter Mailimg City</p>",
                },
                mailing_country: {
                    required: "<p class='error-text'>Select Mailing Country</p>",
                },
                mailing_state: {
                    required: "<p class='error-text'>Select Mailing State</p>",
                },
                mailing_zipcode: {
                    required: "<p class='error-text'>Enter Mailing Zipcode</p>",
                },
                permanent_address1: {
                    required: "<p class='error-text'>Enter Permanent Address 1</p>",
                },
                permanent_city: {
                    required: "<p class='error-text'>Enter Permanent City</p>",
                },
                permanent_country: {
                    required: "<p class='error-text'>Select Permanent Country</p>",
                },
                permanent_state: {
                    required: "<p class='error-text'>Select Permanent State</p>",
                },
                permanent_zipcode: {
                    required: "<p class='error-text'>Enter Permanent Zipcode</p>",
                },
                is_hostel: {
                    required: "<p class='error-text'>Select Accomodation Required</p>",
                },
                id_degree_type: {
                    required: "<p class='error-text'>Select Degree Type</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

</script>