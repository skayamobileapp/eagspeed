<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Profile extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('profile_model');
        $this->isCompanyUserLoggedIn();
    }


    public function index()
    {
        $this->list();
    }


    function demoView()
    {
        $this->load->view('vie');
    }


    function pageNotFound()
    {
        $this->global['pageTitle'] = 'Corporate User Portal : 404 - Page Not Found';
        $this->loadViews("404", $this->global, NULL, NULL);
    }


    function list()
    {
        $id_company_user = $this->session->id_company_user;
        $id_company = $this->session->id_company;

        $data['companyUser'] = $this->profile_model->getCompanyUserProfile($id_company_user);
        $data['company'] = $this->profile_model->getCompany($id_company);

        $this->global['pageTitle'] = 'Corporate User Portal : Profile';
        $this->loadViews("profile/list", $this->global, $data, NULL);
    }
    

    function add()
    {
        if($this->input->post())
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $code = $this->security->xss_clean($this->input->post('code'));
            $status = $this->security->xss_clean($this->input->post('status'));
                        
            $data = array(
                'name' => $name,
                'code' => $code,
                'status' => $status
            );

            $result = $this->profile_model->addNewPaymentType($data);
            redirect('/student/profile/list');
        }
        //print_r($data['stateList']);exit;
        $this->global['pageTitle'] = 'Corporate User Portal : Add Sponser';
        $this->loadViews("profile/add", $this->global, NULL, NULL);
    }


    function edit($id = NULL)
    {
        if ($id == null)
        {
            redirect('/student/profile/list');
        }
        if($this->input->post())
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $code = $this->security->xss_clean($this->input->post('code'));
            $status = $this->security->xss_clean($this->input->post('status'));
                        
            $data = array(
                'name' => $name,
                'code' => $code,
                'status' => $status
            );
            
            $result = $this->profile_model->editPaymentType($data,$id);
            redirect('/student/profile/list');
        }

        $data['profileDetails'] = $this->profile_model->getPaymentType($id);
        
        $this->global['pageTitle'] = 'Corporate User Portal : Edit Sponser';
        $this->loadViews("profile/edit", $this->global, $data, NULL);
    }

    function logout()
    {
        $isCompanyUserLoggedIn = $this->session->isCompanyUserLoggedIn;

        if($isCompanyUserLoggedIn == TRUE)
        {

        // echo $isStudentAdminLoggedIn;exit();

            $sessionArray = array(
                        'id_company_user'=>'',
                        'company_user_full_name'=>'',
                        'company_user_name'=>'',
                        'company_user_email'=>'',
                        'company_user_phone'=>'',
                        'company_user_designation'=>'',
                        'company_user_role'=>'',
                        'id_company_user_role'=>'',
                        'company_user_last_login'=> '',
                        'company_image'=> '',
                        'company_user_session_id' => '',
                        'isCompanyUserLoggedIn' => FALSE
                    );
            $this->session->set_userdata($sessionArray);
        } 
        
        $this->isCompanyUserLoggedIn();
    }
}