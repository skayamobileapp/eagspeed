<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class GraduationList extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('graduation_list_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('graduation.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        { 
            
            // $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
            // $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            // $formData['id_program'] = $this->security->xss_clean($this->input->post('id_program'));
            // $formData['id_qualification'] = $this->security->xss_clean($this->input->post('id_qualification'));

            $formData['id_convocation'] = $this->security->xss_clean($this->input->post('id_convocation'));
            $formData['status'] = '';
            $data['searchParam'] = $formData;


            $data['graduationList'] = $this->graduation_list_model->graduationListSearch($formData);
            $data['convocationList'] = $this->graduation_list_model->convocationListByStatus('1');
            

            // echo "<Pre>";print_r($data['graduationList']);exit();

            $this->global['pageTitle'] = 'Campus Management System : Graduation List';
            $this->loadViews("graduation_list/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('graduation.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                $formData = $this->input->post();

                // echo "<Pre>"; print_r($formData);exit;

                
               
                $id_convocation = $this->security->xss_clean($this->input->post('id_convocation'));
                $message = $this->security->xss_clean($this->input->post('message'));
                $date_tm = $this->security->xss_clean($this->input->post('date_tm'));
            
                $data = array(
                   'id_convocation' => $id_convocation,
                    'message' => $message,
                    'date_tm' => date('Y-m-d',strtotime($date_tm)),
                    'status' => 0,
                    'created_by' => $id_user
                );
                $inserted_id = $this->graduation_list_model->addNewGraduation($data);


                

                for($i=0;$i<count($formData['id_student']);$i++)
                 {
                    $id_student = $formData['id_student'][$i];

                    if($id_student > 0)
                    {
                        if($inserted_id)
                        {
                            $detailsData = array(
                                'id_graduation'=>$inserted_id,
                                'id_student'=>$id_student,
                                'created_by'=>$id_user
                            );
                        // echo "<Pre>"; print_r($detailsData);exit;
                            $inserted_detail_id = $this->graduation_list_model->addNewGraduationDetail($detailsData);
                            if($inserted_detail_id)
                            {
                                $student_data = array(
                                'applicant_status'=> 'Graduation Pending'
                                );

                                $update_student_status = $this->graduation_list_model->updateStudent($student_data,$id_student);

                            }
                       
                        }
                    }
                }


                redirect('/graduation/graduationList/list');
            }
            $data['convocationList'] = $this->graduation_list_model->convocationListByStatus('1');
            $data['programList'] = $this->graduation_list_model->programListByStatus('1');
            $data['intakeList'] = $this->graduation_list_model->intakeListByStatus('1');
            $data['qualificationList'] = $this->graduation_list_model->qualificationListByStatus('1');

            $this->global['pageTitle'] = 'Campus Management System : Add Graduation Students';
            $this->loadViews("graduation_list/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('graduation.view') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/graduation/graduationList/list');
            }
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;
                
                // $id_convocation = $this->security->xss_clean($this->input->post('id_convocation'));
                // $message = $this->security->xss_clean($this->input->post('message'));
            
                // $data = array(
                //    'id_convocation' => $id_convocation,
                //     'message' => $message,
                //     'status' => $status,
                //     'created_by' => $id_user
                // );
                // // echo "<Pre>"; print_r($id);
                // // exit;

                // $result = $this->graduation_list_model->editGraduationList($data,$id);
                redirect('/graduation/graduationList/list');
            }
            $data['graduation'] = $this->graduation_list_model->getGraduationList($id);
            $data['graduationDetails'] = $this->graduation_list_model->getGraduationDetailsByMasterId($id);
            $data['organisation'] = $this->graduation_list_model->getOrganisation();
        // echo "<Pre>";print_r($data);exit();
            
            $this->global['pageTitle'] = 'Campus Management System : View GraduationList';
            $this->loadViews("graduation_list/edit", $this->global, $data, NULL);
        }
    }

    function addStudent($id)
    {
        if ($this->checkAccess('graduation.add_student') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/graduation/graduationList/list');
            }
            if($this->input->post())
            {
                $formData = $this->input->post();


                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                // echo "<Pre>"; print_r($formData);exit;

                
                for($i=0;$i<count($formData['id_student']);$i++)
                 {
                    $id_student = $formData['id_student'][$i];

                    if($id_student > 0)
                    {
                        if($id)
                        {
                            $detailsData = array(
                                'id_graduation'=>$id,
                                'id_student'=>$id_student,
                                'created_by'=>$id_user
                            );
                        // echo "<Pre>"; print_r($detailsData);exit;
                            $inserted_detail_id = $this->graduation_list_model->addNewGraduationDetail($detailsData);
                            if($inserted_detail_id)
                            {
                                $student_data = array(
                                'applicant_status'=> 'Graduation Pending'
                                );

                                $update_student_status = $this->graduation_list_model->updateStudent($student_data,$id_student);

                            }
                       
                        }
                    }
                }


                redirect('/graduation/graduationList/addStudent/'.$id);
            }

            $data['programList'] = $this->graduation_list_model->programListByStatus('1');
            $data['intakeList'] = $this->graduation_list_model->intakeListByStatus('1');
            $data['qualificationList'] = $this->graduation_list_model->qualificationListByStatus('1');
            $data['organisation'] = $this->graduation_list_model->getOrganisation();

            $data['graduation'] = $this->graduation_list_model->getGraduationList($id);
            $data['graduationDetails'] = $this->graduation_list_model->getGraduationDetailsByMasterId($id);
        // echo "<Pre>";print_r($data['graduationDetails']);exit();
            
            $this->global['pageTitle'] = 'Campus Management System : View GraduationList';
            $this->loadViews("graduation_list/add_student", $this->global, $data, NULL);
        }

    }

    function approvalList()
    {
        if ($this->checkAccess('graduation.approval_list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        { 
            
            // $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
            // $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            // $formData['id_program'] = $this->security->xss_clean($this->input->post('id_program'));
            // $formData['id_qualification'] = $this->security->xss_clean($this->input->post('id_qualification'));

            $formData['id_convocation'] = $this->security->xss_clean($this->input->post('id_convocation'));
            $formData['status'] = '0';
            $data['searchParam'] = $formData;


            $data['graduationList'] = $this->graduation_list_model->graduationListSearch($formData);
            $data['convocationList'] = $this->graduation_list_model->convocationListByStatus('1');
            

            // echo "<Pre>";print_r($data['graduationList']);exit();

            $this->global['pageTitle'] = 'Campus Management System : Graduation List';
            $this->loadViews("graduation_list/approval_list", $this->global, $data, NULL);
        }
    }

    function view($id = NULL)
    {
        if ($this->checkAccess('graduation.approve') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/graduationList/list');
            }
            if($this->input->post())
            {
                $postData = $this->input->post();

                $btn_submit = $postData['btn_submit'];

                // echo "<Pre>";print_r($postData);exit();

                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;
                
                // $status = $this->security->xss_clean($this->input->post('status'));
                // $reason = $this->security->xss_clean($this->input->post('reason'));


                // $data = array(
                //     'status' => $status,
                //     'reason' => $reason
                // );

                //  $result = $this->graduation_list_model->updateGraduation($data,$id);
                //  $result = $this->graduation_list_model->updateGraduationDetailsByMasterId($data,$id);
                //  if ($status == '1')
                //  {
                //     $graduationDetails = $this->graduation_list_model->getGraduationDetailsByMasterId($id);
                //     foreach ($graduationDetails as $graduationDetail)
                //     {
                //             $student_data = array(
                //                 'applicant_status'=> 'Graduated'
                //                 );

                //             $update_student_status = $this->graduation_list_model->updateStudent($student_data,$graduationDetail->id_student);
                //     }
                     
                //  }elseif ($status == '2')
                //  {
                //     $graduationDetails = $this->graduation_list_model->getGraduationDetailsByMasterId($id);
                //     foreach ($graduationDetails as $graduationDetail)
                //     {
                //         $student_data = array(
                //                 'applicant_status'=> 'Approved'
                //                 );

                //             $update_student_status = $this->graduation_list_model->updateStudent($student_data,$graduationDetail->id_student);
                //     }

                //  }



                $id_graduation_detail = $this->security->xss_clean($this->input->post('id_graduation_detail'));
                // echo "<Pre>";print_r($id_graduation_detail);exit();


                for($i=0;$i<count($id_graduation_detail);$i++)
                {
                // echo "<Pre>";print_r($id_graduation_detail[$i]);exit();
                    
                    $id_detail = $id_graduation_detail[$i];

                    $data['status'] = $btn_submit;

                    $updated = $this->graduation_list_model->updateGraduationDetails($data,$id_detail);

                    if($updated)
                    {
                        $graduationDetail = $this->graduation_list_model->getGraduationDetails($id_detail);
                        if($btn_submit == 1)
                        {
                            $student_data = array(
                                'applicant_status'=> 'Graduated'
                                );

                        }elseif($btn_submit == 2)
                        {
                            $student_data = array(
                                'applicant_status'=> 'Approved'
                                );
                        }

                        $update_student_status = $this->graduation_list_model->updateStudent($student_data,$graduationDetail->id_student);


                        $is_pending = $this->graduation_list_model->getPendingGraduationDetailsByMasterId($id);

                        if(!$is_pending)
                        {
                            $graduation_data['status'] = 1;

                        $update_graduation_list = $this->graduation_list_model->updateGraduation($graduation_data,$id);

                        }
                    }

                }
                redirect('/graduation/graduationList/approvalList');
            }
            $data['graduation'] = $this->graduation_list_model->getGraduationList($id);
            $data['graduationDetails'] = $this->graduation_list_model->getGraduationDetailsByMasterId($id);
            $data['organisation'] = $this->graduation_list_model->getOrganisation();
        // echo "<Pre>";print_r($data);exit();
            
            $this->global['pageTitle'] = 'Campus Management System : Approve Graduation List';
            $this->loadViews("graduation_list/view", $this->global, $data, NULL);
        }
    }


    function searchStudent()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $student_data = $this->graduation_list_model->studentListSearchForGraduation($tempData);
        // echo "<Pre>";print_r($student_data);exit();

        if(!empty($student_data))
        {

        // echo "<Pre>";print_r($student_data);exit();

         $table = "<table  class='table' id='list-table'>
                  <tr>
                    <th>Sl. No</th>
                    <th>Student</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Program</th>
                    <th>Intake</th>
                    <th>Qualification Type</th>
                    <th>Program Scheme</th>
                    <th>Branch</th>
                    <th>Total Credit Hours</th>
                    <th>Completed Credit Hours</th>
                    <th>Financial Status</th>
                    <th style='text-align: center;'><input type='checkbox' id='checkAll' name='checkAll'> Check All</th>
                </tr>";

            for($i=0;$i<count($student_data);$i++)
            {
                $id = $student_data[$i]->id;
                $full_name = $student_data[$i]->full_name;
                $course_credit_hours = $student_data[$i]->course_credit_hours;
                $cleared_credit_hours = $student_data[$i]->cleared_credit_hours;
                $is_invoice_pending = $student_data[$i]->is_invoice_pending;
                $nric = $student_data[$i]->nric;
                $email_id = $student_data[$i]->email_id;
                $program_scheme = $student_data[$i]->program_scheme;
                $id_branch = $student_data[$i]->id_branch;
                $branch_code = $student_data[$i]->branch_code;
                $branch_name = $student_data[$i]->branch_name;
                $intake_year = $student_data[$i]->intake_year;

                if($id_branch == 1)
                {
                    $organisation = $this->graduation_list_model->getOrganisation();

                    $branch_code = $organisation->short_name;
                    $branch_name = $organisation->name;
                }



                $program = $student_data[$i]->program_code . " - " . $student_data[$i]->program_name;
                $intake = $student_data[$i]->intake_year . " - " . $student_data[$i]->intake_name;
                $qualification = $student_data[$i]->qualification_code . " - " . $student_data[$i]->qualification_name;


                

                
                $phone = $student_data[$i]->phone;
                // $depriciation_value = $student_data[$i]->depriciation_value;
                $j = $i+1;
                $table .= "
                <tr>
                    <td>$j</td>
                    <td>$nric - $full_name</td>
                    <td>
                        $email_id
                    </td>                           
                    <td>$phone</td>                    
                    <td>$program</td>                           
                    <td>$intake_year</td>                           
                    <td>$qualification</td>                      
                    <td>$program_scheme</td>                    
                    <td>$branch_code - $branch_name</td>                    
                    <td>$course_credit_hours</td>                    
                    <td>$cleared_credit_hours </td>
                    <td>$is_invoice_pending</td>                    
                    
                    <td class='text-center'>
                        <input type='checkbox' id='id_student[]' name='id_student[]' class='check' value='".$id."'>
                    </td>
               
                </tr>";
            }

         $table.= "</table>";
        }
        else
        {
            $table= "<h4> No Students Available For Entered Data</h4>";
        }
        echo $table;
    }

    function deleteGraduationTaging($id)
    {

        $student_data = array(
        'applicant_status'=> 'Approved'
        );

        $graduation_details = $this->graduation_list_model->getGraduationDetails($id);


        $update_student_status = $this->graduation_list_model->updateStudent($student_data,$graduation_details->id_student);

        if($update_student_status)
        {
            $this->graduation_list_model->deleteGraduationTaging($id);
        }
        echo "success";exit;
    }
}
