<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Convocation</h3>
        </div>
        <form id="form_convocation" action="" method="post">
            <div class="form-container">
                <h4 class="form-group-title">Convocation Details</h4>  
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Convocation Session <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="convocation_session" name="convocation_session" value="<?php echo $convocation->convocation_session; ?>">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Session Capacity <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="session_capacity" name="session_capacity" value="<?php echo $convocation->session_capacity; ?>">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Convocation Year <span class='error-text'>*</span></label>
                            <select name="year" id="year" class="form-control">
                                <option value="">Select</option>
                                <?php
                                for ($i=date('Y'); $i < date('Y') + 5; $i++)
                                { 
                                    ?>
                                    <option value="<?php echo $i;?>"
                                        <?php if($i== $convocation->year)
                                        {
                                            echo 'selected';
                                        }?>
                                        ><?php echo  $i;?>
                                    </option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>


                </div>

                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Number Of Guests <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="number_of_guest" name="number_of_guest" value="<?php echo $convocation->number_of_guest; ?>">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Weblink <span class='error-text'>*</span></label>
                            <input type="weblink" class="form-control" id="weblink" name="weblink" value="<?php echo $convocation->weblink; ?>">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Start Date <span class='error-text'>*</span></label>
                            <input type="text" class="form-control datepicker" id="start_date" name="start_date" value="<?php echo date('d-m-Y',strtotime($convocation->start_date)); ?>" autocomplete="off">
                        </div>
                    </div>

                
                </div>


                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>End Date <span class='error-text'>*</span></label>
                            <input type="text" class="form-control datepicker" id="end_date" name="end_date" value="<?php echo date('d-m-Y',strtotime($convocation->end_date)); ?>" autocomplete="off">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Attendence End Date <span class='error-text'>*</span></label>
                            <input type="text" class="form-control datepicker" id="confirm_attendence_end_date" name="confirm_attendence_end_date" value="<?php echo date('d-m-Y',strtotime($convocation->confirm_attendence_end_date)); ?>" autocomplete="off">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Record Verification End Date <span class='error-text'>*</span></label>
                            <input type="text" class="form-control datepicker" id="record_verification_end_date" name="record_verification_end_date" value="<?php echo date('d-m-Y',strtotime($convocation->record_verification_end_date)); ?>" autocomplete="off">
                        </div>
                    </div>

                
                </div>


                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Guest Application End Date <span class='error-text'>*</span></label>
                            <input type="text" class="form-control datepicker" id="graduation_guest_application_end_date" name="graduation_guest_application_end_date" value="<?php echo date('d-m-Y',strtotime($convocation->graduation_guest_application_end_date)); ?>" autocomplete="off">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Student Portal End Date <span class='error-text'>*</span></label>
                            <input type="text" class="form-control datepicker" id="student_portal_end_date" name="student_portal_end_date" value="<?php echo date('d-m-Y',strtotime($convocation->student_portal_end_date)); ?>" autocomplete="off">
                        </div>
                    </div>

                    <div class="col-sm-4">
                            <div class="form-group">
                                <p>Status <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                <input type="radio" name="status" id="status" value="1" <?php if($convocation->status=='1') {
                                    echo "checked=checked";
                                };?>><span class="check-radio"></span> Active
                                </label>
                                <label class="radio-inline">
                                <input type="radio" name="status" id="status" value="0" <?php if($convocation->status=='0') {
                                    echo "checked=checked";
                                };?>>
                                <span class="check-radio"></span> In-Active
                                </label>                              
                            </div>                         
                    </div>          
                
                </div>
            </div>

            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="../list" class="btn btn-link">Back</a>
                </div>
            </div>
        </form>



        <form id="form_detail" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Convocation Fee Details</h4>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Fee Item <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" name="fee_item" id="fee_item" autocomplete="off">
                        <!-- <span id='view_centers'></span> -->
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Fee Amount <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" name="fee_amount" id="fee_amount" autocomplete="off" value="">
                        <!-- <span id='view_centers'></span> -->
                    </div>
                </div>
            </div>

            <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="button" class="btn btn-primary btn-lg" onclick="saveDetailData()">Add</button>
                <!-- <a href="list" class="btn btn-link">Cancel</a> -->
            </div>
        </div>

        </div>

    </form>



        <?php
        if(!empty($convocationDetails))
        {
            ?>
            <br>

            <div class="form-container">
                    <h4 class="form-group-title">Convocation Fee Details</h4>

                

                  <div class="custom-table">
                    <table class="table">
                        <thead>
                            <tr>
                            <th>Sl. No</th>
                            <th>Fee Item</th>
                            <th>Fee Amount</th>
                            <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                             <?php
                         $total = 0;
                          for($i=0;$i<count($convocationDetails);$i++)
                         { ?>
                            <tr>
                            <td><?php echo $i+1;?></td>
                            <td><?php echo $convocationDetails[$i]->fee_item; ?></td>
                            <td><?php echo $convocationDetails[$i]->fee_amount; ?></td>
                            <td class="text-center">
                                <a onclick="deleteDetailData(<?php echo $convocationDetails[$i]->id; ?>)">Delete</a>
                            </td>

                             </tr>
                          <?php 
                      } 
                      ?>
                        </tbody>
                    </table>
                  </div>

                </div>

        <?php
        
        }
         ?>





        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

     function saveDetailData()
    {
        if($('#form_detail').valid())
        {

        var tempPR = {};

        tempPR['fee_item'] = $("#fee_item").val();
        tempPR['fee_amount'] = $("#fee_amount").val();
        tempPR['id_convocation'] = <?php echo $convocation->id; ?>;

            $.ajax(
            {
               url: '/graduation/convocation/saveDetailData',
                type: 'POST',
                // type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {

                // alert(result);
                // $('#myModal').modal('show');
                // $("#view_requirement_data").html(result);
                // $("#view_temp_details").html(result);

                // location.reload();
                window.location.reload();

               }
            });
        }
    }

    function deleteDetailData(id)
    {
        // alert(id);
         $.ajax(
            {
               url: '/graduation/convocation/deleteDetailData/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view_temp_details").html(result);
                    window.location.reload();
                    // alert(result);
                    // window.location.reload();
               }
            });
    }


    $(document).ready(function() {
        $("#form_detail").validate({
            rules: {
                fee_item: {
                    required: true
                },
                fee_amount: {
                    required: true
                }
            },
            messages: {
                fee_item: {
                    required: "<p class='error-text'>Fee Item Required</p>",
                },
                fee_amount: {
                    required: "<p class='error-text'>Fee Amount Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    $('select').select2();

    $(document).ready(function() {
        $("#form_convocation").validate({
            rules: {
                convocation_session: {
                    required: true
                },
                session_capacity: {
                    required: true
                },
                number_of_guest: {
                    required: true
                },
                weblink: {
                    required: true
                },
                start_date: {
                    required: true
                },
                end_date: {
                    required: true
                },
                confirm_attendence_end_date: {
                    required: true
                },
                record_verification_end_date: {
                    required: true
                },
                graduation_guest_application_end_date: {
                    required: true
                },
                student_portal_end_date: {
                    required: true
                }
            },
            messages: {
                convocation_session: {
                    required: "<p class='error-text'>Convocation Session Name Required</p>",
                },
                session_capacity: {
                    required: "<p class='error-text'>Capacity Required</p>",
                },
                number_of_guest: {
                    required: "<p class='error-text'>No. Of Guests Required</p>",
                },
                weblink: {
                    required: "<p class='error-text'>Weblink Required</p>",
                },
                start_date: {
                    required: "<p class='error-text'>Start Date Required</p>",
                },
                end_date: {
                    required: "<p class='error-text'>End Date Required</p>",
                },
                confirm_attendence_end_date: {
                    required: "<p class='error-text'>Attendence End Date Required</p>",
                },
                record_verification_end_date: {
                    required: "<p class='error-text'>Record Verification End Date Required</p>",
                },
                graduation_guest_application_end_date: {
                    required: "<p class='error-text'>Guest Application End Date Required</p>",
                },
                student_portal_end_date: {
                    required: "<p class='error-text'>Student Portal End Date Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script>
  $( function() {
    $( ".datepicker" ).datepicker();
  } );
</script>