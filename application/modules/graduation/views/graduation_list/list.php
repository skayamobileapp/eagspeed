<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>Graduation List</h3>
      <a href="add" class="btn btn-primary">+ Add Graduation Students</a>
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">



                <div class="row">
                <!-- <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Project Name</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="name" value="<?php echo $searchParam['name']; ?>">
                    </div>
                  </div>
                </div> -->

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Convocation</label>
                    <div class="col-sm-8">
                      <select name="id_convocation" id="id_convocation" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($convocationList)) {
                          foreach ($convocationList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['id_convocation']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo $record->convocation_session;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>

              </div>


              <!-- <div class="row">

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Intake</label>
                    <div class="col-sm-8">
                      <select name="id_intake" id="id_intake" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($intakeList)) {
                          foreach ($intakeList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['id_intake']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->year ." - " . $record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Qualification Level</label>
                    <div class="col-sm-8">
                      <select name="id_qualification" id="id_qualification" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($qualificationList)) {
                          foreach ($qualificationList as $record)
                          {
                            $selected = '';
                            if ($record->code == $searchParam['id_qualification']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->code;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->code ."-".$record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>

              </div> -->


                

              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href="list" class="btn btn-link">Clear All Fields</a>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

  <form id="form_award" action="" method="post">
    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. NO</th>
            <th>Convocation Session</th>
            <th>Convocation Date</th>
            <th style="text-align: center;">Status</th>
            <th style="text-align: center;">Action</th>
            <!-- <th style="text-align: center;"><input type="checkbox" id="checkAll" name="checkAll"> Check All</th> -->
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($graduationList)) {
              $i=0;
            foreach ($graduationList as $record) {
          ?>
              <tr>
                <td><?php echo $i+1;?></td>
                <td><?php echo $record->convocation_session ?></td>
                <td><?php echo date('d-m-Y',strtotime($record->date_tm)) ?></td>
                <td class="text-center"><?php if( $record->status == '1')
                {
                  echo "Approved";
                }
                else if( $record->status == '0')
                {
                   echo "Pending";
                }
                else if( $record->status == '2')
                {
                  echo "Rejected";
                } 
                ?></td>

                 <td class="text-center">
                  <a style="text-align: center;" href="<?php echo 'edit/' . $record->id; ?>" title="View">View</a>

                  <?php 
                  if( $record->status == '0')
                  {
                    ?>
                    |

                    <a style="text-align: center;" href="<?php echo 'addStudent/' . $record->id; ?>" title="Add | Remove Student">Edit</a>

                    <?php
                  }
                  ?>
                </td>
               <!--  <td class="text-center">
                  <input type="checkbox" name="checkvalue[]" class="check" value="<?php echo $record->id ?>">
                </td> -->
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
          <!-- <div class="app-btn-group">
            <button type="submit" class="btn btn-primary">Submit</button>
          </div> -->
    </div>
  </form>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script>
    $(function () {
        $("#checkAll").click(function () {
            if ($("#checkAll").is(':checked')) {
                $(".check").prop("checked", true);
            } else {
                $(".check").prop("checked", false);
            }
        });
    });

  $('select').select2();
  
  function clearSearchForm()
      {
        window.location.reload();
      }
</script>