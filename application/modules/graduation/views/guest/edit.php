<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Guest</h3>
        </div>
        <form id="form_guest" action="" method="post">
            <div class="form-container">
                <h4 class="form-group-title">Guest Details</h4> 
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Name <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="name" name="name" value="<?php echo $guest->name; ?>">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Email <span class='error-text'>*</span></label>
                            <input type="email" class="form-control" id="email" name="email" value="<?php echo $guest->email; ?>">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Description <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="description" name="description" value="<?php echo $guest->description; ?>">
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>NRIC <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="nric" name="nric" value="<?php echo $guest->nric; ?>">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Mobile <span class='error-text'>*</span></label>
                            <input type="mobile" class="form-control" id="mobile" name="mobile" value="<?php echo $guest->mobile; ?>">
                        </div>
                    </div>



                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Convocation <span class='error-text'>*</span></label>
                            <select name="id_convocation" id="id_convocation" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($convocationList))
                                {
                                    foreach ($convocationList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;  ?>"
                                            <?php 
                                            if($record->id == $guest->id_convocation)
                                            {
                                                echo "selected=selected";
                                            } ?>>
                                            <?php echo $record->convocation_session;  ?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>              
                </div>

                <div class="row">
                    
                    <div class="col-sm-4">
                            <div class="form-group">
                                <p>Status <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                <input type="radio" name="status" id="status" value="1" <?php if($guest->status=='1') {
                                    echo "checked=checked";
                                };?>><span class="check-radio"></span> Active
                                </label>
                                <label class="radio-inline">
                                <input type="radio" name="status" id="status" value="0" <?php if($guest->status=='0') {
                                    echo "checked=checked";
                                };?>>
                                <span class="check-radio"></span> In-Active
                                </label>                              
                            </div>                         
                    </div>        
                
                </div>
            </div>

            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="../list" class="btn btn-link">Cancel</a>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>
    $(document).ready(function() {
        $("#form_guest").validate({
            rules: {
                name: {
                    required: true
                },
                email: {
                    required: true
                },
                description: {
                    required: true
                },
                nric: {
                    required: true
                },
                mobile: {
                    required: true
                },
                id_convocation: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Award Name required</p>",
                },
                email: {
                    required: "<p class='error-text'>Email required</p>",
                },
                description: {
                    required: "<p class='error-text'>Description For Guest required</p>",
                },
                nric: {
                    required: "<p class='error-text'>NRIC required</p>",
                },
                mobile: {
                    required: "<p class='error-text'>Mobile Number required</p>",
                },
                id_convocation: {
                    required: "<p class='error-text'>Select Convocation</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script type="text/javascript">
    $('select').select2();
</script>