<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Award_model extends CI_Model
{
    function awardList()
    {
        $this->db->select('a.*, p.name as programme');
        $this->db->from('award_level as a');
        $this->db->join('programme as p', 'a.id_programme = p.id');
        $this->db->order_by("id", "DESC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function awardListSearch($formData)
    {
        $this->db->select('a.*, p.name as programme');
        $this->db->from('award_level as a');
        $this->db->join('programme as p', 'a.id_programme = p.id');
        if (!empty($formData['name']))
        {
            $likeCriteria = "(a.name  LIKE '%" . $formData['name'] . "%' or a.description  LIKE '%" . $formData['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if (!empty($formData['id_programme']))
        {
            $likeCriteria = "(a.id_programme  LIKE '%" . $formData['id_programme'] . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("id", "DESC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getAward($id)
    {
        $this->db->select('*');
        $this->db->from('award_level');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewAward($data)
    {
        $this->db->trans_start();
        $this->db->insert('award_level', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editAward($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('award_level', $data);
        return TRUE;
    }

    function programmeList()
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }
    function intakeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('intake');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }
    
}

