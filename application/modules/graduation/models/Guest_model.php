<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Guest_model extends CI_Model
{
    function guestList()
    {
        $this->db->select('*');
        $this->db->from('guest');
        $this->db->order_by("id", "DESC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function guestListSearch($search)
    {
        $this->db->select('g.*, c.convocation_session as convocation');
        $this->db->from('guest as g');
         $this->db->join('convocation as c', 'g.id_convocation = c.id');
        if (!empty($search))
        {
            $likeCriteria = "(name  LIKE '%" . $search . "%' or mobile  LIKE '%" . $search . "%' or nric  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("g.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getGuest($id)
    {
        $this->db->select('*');
        $this->db->from('guest');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewGuest($data)
    {
        $this->db->trans_start();
        $this->db->insert('guest', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editGuest($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('guest', $data);
        return TRUE;
    }
}

