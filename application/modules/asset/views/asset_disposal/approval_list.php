<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>Approve Asset Disposal</h3>
      <!-- <a href="add" class="btn btn-primary">+ Add Convocation</a> -->
    </div>
    <form action="" method="post" id="searchForm">
    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">
                

              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Reason</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="name" value="<?php echo $searchParam['name']; ?>">
                    </div>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Department</label>
                    <div class="col-sm-8">
                      <select name="id_department" id="id_department" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($departmentList)) {
                          foreach ($departmentList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['id_department']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->code ."-".$record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>

              </div>


              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Disposal Type</label>
                    <div class="col-sm-8">
                      <select name="id_disposal_type" id="id_disposal_type" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($disposalTypeList)) {
                          foreach ($disposalTypeList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['id_disposal_type']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->code ."-".$record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Staff</label>
                    <div class="col-sm-8">
                      <select name="id_staff" id="id_staff" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($staffList)) {
                          foreach ($staffList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['id_staff']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->ic_no ."-".$record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>
                
              </div>

              </div>
              <div class="app-btn-group">
                <button type="button" class="btn btn-primary">Search</button>
                <button type="reset" class="btn btn-link" onclick="clearSearchForm()">Clear All Fields</button>
              </div>
            </div>
          </div>
      </div>
    </div>

    
    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Disposal Type</th>
            <th>Date</th>
            <th>Reason</th>
            <th>No Of Assets</th>
            <th>Status</th>
            <th style="text-align:center; ">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($assetDisposalList))
          {
            $i=0;
            foreach ($assetDisposalList as $record) {
          ?>
              <tr>
                 <td><?php echo $i+1;?></td>
                <td><?php echo $record->disposal_type_code . " - " . $record->disposal_type_name ?></td>
                <td><?php echo $record->date_time ?></td>
                <td><?php echo $record->reason ?></td>
                <td><?php echo $record->no_of_assets ?></td>
                <td><?php if( $record->status == '1')
                {
                  echo "Approved";
                }
                else
                {
                  echo "Pending";
                } 
                ?></td>
                <td class="text-center">

                  <a href="<?php echo 'view/' . $record->id; ?>" title="View">Approve</a>
                  <!--  -->
                </td>
              </tr>
          <?php
            }
          }
          ?>
        </tbody>
      </table>
    </div>

  </form>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script>
    $(function () {
        $("#checkAll").click(function () {
            if ($("#checkAll").is(':checked')) {
                $(".check").prop("checked", true);
            } else {
                $(".check").prop("checked", false);
            }
        });
    });
    
    $('select').select2();

  function clearSearchForm()
      {
        window.location.reload();
      }
</script>