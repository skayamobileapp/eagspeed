<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>Approve Asset Pre-Registration</h3>
    </div>
    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>

      <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">
                

              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Name</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="name" value="<?php echo $searchParam['name']; ?>">
                    </div>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Company Name</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="company" value="<?php echo $searchParam['company']; ?>">
                    </div>
                  </div>
                </div>

              </div>

              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Brand Name</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="brand" value="<?php echo $searchParam['brand']; ?>">
                    </div>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Depreciation Code</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="depriciation_code" value="<?php echo $searchParam['depriciation_code']; ?>">
                    </div>
                  </div>
                </div>

              </div>


              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Asset Order / GRN Number</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="asset_order_number" value="<?php echo $searchParam['asset_order_number']; ?>">
                    </div>
                  </div>
                </div>

              </div>


              </div>
              <div class="app-btn-group">
                <button type="submit" name="button" value="search" class="btn btn-primary">Search</button>
                <button type="reset" class="btn btn-link" onclick="clearSearchForm()">Clear All Fields</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
           <th>Sl No.</th>
           <th>Asset Order Number</th>
           <th>GRN Number</th>
           <th>Name</th>
            <th>Brand</th>
            <th>Company</th>
            <th>Depreciation Code</th>
            <th>Depreciation Value</th>
            <th>Item </th>
            <th>Price</th>
            <th>Tax</th>
            <th>Total</th>
            <th>Status </th>
            <th style="text-align: center;">Action</th>
            <!-- <th style="text-align: center;"><input type="checkbox" id="checkAll" name="checkAll"> Check All</th> -->
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($assetPreRegistrationList)) {
              $i=0;
            foreach ($assetPreRegistrationList as $record)
            {
          ?>
              <tr>
                <td><?php echo $i+1;?></td>
                <td><?php echo $record->asset_order_number ?></td>
                <td><?php echo $record->reference_number ?></td>
                <td><?php echo $record->name ?></td>
                <td><?php echo $record->brand ?></td>
                <td><?php echo $record->company ?></td>
                <td><?php echo $record->depriciation_code ?></td>
                <td><?php echo $record->depriciation_value ?></td>
                <td><?php echo $record->item_code . " - " . $record->item_name ?></td>
                <td><?php echo $record->price ?></td>
                <td><?php echo $record->tax ?></td>
                <td><?php echo $record->total_amount ?></td>
                <td><?php if( $record->status == '1')
                {
                  echo "Approved";
                }
                else if( $record->status == '0')
                {
                   echo "Pending";
                }
                else if( $record->status == '2')
                {
                  echo "Rejected";
                } 
                ?></td>

                <td>
                  <a href="<?php echo 'view/' . $record->id; ?>" title="View">Approve</a>
                </td>

                <?php if($record->status=="1")
                {
                  echo "<td class='text-center'>Approved</td>";
                }
                else
                {
                ?>
                <!-- <td class="text-center">
                  <input type="checkbox" name="checkvalue[]" class="check" value="<?php echo $record->id ?>">
                </td> -->
                <?php
                }
                ?>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
      <div class="app-btn-group">
          <!-- <button type="submit" name="button" value="approve" class="btn btn-primary">Approve</button> -->
      </div>
    </div>
  </form>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script>
    $(function () {
        $("#checkAll").click(function () {
            if ($("#checkAll").is(':checked')) {
                $(".check").prop("checked", true);
            } else {
                $(".check").prop("checked", false);
            }
        });
    });
</script>
<script type="text/javascript">
    $('select').select2();
    
  function clearSearchForm()
      {
        window.location.reload();
      }
</script>