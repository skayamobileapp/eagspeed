<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ScholarshipApplication extends BaseController
{
    public function __construct()
    {
        // $test = new BaseController();
        // $test->isStudentLoggedIn();
        parent::__construct();
        $this->load->model('scholarship_application_model');
        $this->isScholarLoggedIn();
    }

    function list()
    {
        if ($this->checkScholarAccess('scholarship_application.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            // if($this->input->post())
            // {
                $formData['application_number'] = $this->security->xss_clean($this->input->post('application_number'));
                $formData['name'] = $this->security->xss_clean($this->input->post('name'));
                $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
                $formData['id_scholarship_scheme'] = $this->security->xss_clean($this->input->post('id_scholarship_scheme'));
                $formData['id_program'] = $this->security->xss_clean($this->input->post('id_program'));
                $formData['status'] = '';
     
                $data['searchParam'] = $formData;
            
            $data['scholarshipApplicationList'] = $this->scholarship_application_model->getScholarshipApplicationListSearch($formData);
            
            
            $data['schemeList'] = $this->scholarship_application_model->schemeListByStatus('1');
            $data['programList'] = $this->scholarship_application_model->programListByStatus('1');



            // $data['scholarshipApplicationList'] = $this->scholarship_application_model->getScholarshipApplicationListByStudentId($id);
            // echo "<Pre>";print_r($data);exit();

            $this->global['pageTitle'] = 'Campus Management System : List Scholarship Application';
            $this->loadViews("scholarship_application/list", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkScholarAccess('scholarship_application.view') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {


        if ($id == null)
        {
            redirect('/scholarship/scholarshipApplication/list');
        }

        $data['scholarship'] = $this->scholarship_application_model->getScholarshipApplication($id);
        $id_scholar_applicant = $data['scholarship']->id_student;
        $id_scheme = $data['scholarship']->id_scholarship_scheme;

        $data['countryList'] = $this->scholarship_application_model->countryList();
        $data['stateList'] = $this->scholarship_application_model->stateList();
        $data['raceList'] = $this->scholarship_application_model->raceListByStatus('1');
        $data['religionList'] = $this->scholarship_application_model->religionListByStatus('1');
        $data['programList'] = $this->scholarship_application_model->programListByStatus('1');


        $data['profileDetails'] = $this->scholarship_application_model->getApplicationPersonalDetailsByApplicationId($id_scholar_applicant,$id_scheme,$id);

        $data['educationDetails'] = $this->scholarship_application_model->getExamDetailsByApplicationId($id_scholar_applicant,$id_scheme,$id);
        $data['familyDetails'] = $this->scholarship_application_model->getFamilyDetailsByApplicationId($id_scholar_applicant,$id_scheme,$id);
        $data['employmentDetails'] = $this->scholarship_application_model->getEmploymentDetailsByApplicationId($id_scholar_applicant,$id_scheme,$id);
        $data['scheme'] = $this->scholarship_application_model->getSchemeById($id_scheme);



       
        

        $data['studentDetails'] = $this->scholarship_application_model->getStudentDetails($id_scholar_applicant);
            // echo "<Pre>"; print_r($data['scholarshipApplication']->id_student);exit;
        

        $this->global['pageTitle'] = 'Campus Management System : View Scholarship Application';
        $this->loadViews("scholarship_application/edit", $this->global, $data, NULL);
        }
    }
    
    function approvalList()
    {
        if ($this->checkScholarAccess('scholarship_application.approval_list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['application_number'] = $this->security->xss_clean($this->input->post('application_number'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
            $formData['id_scholarship_scheme'] = $this->security->xss_clean($this->input->post('id_scholarship_scheme'));
            $formData['id_program'] = $this->security->xss_clean($this->input->post('id_program'));
            $formData['status'] = '0';
 
            $data['searchParam'] = $formData;
            
            $data['scholarshipApplicationList'] = $this->scholarship_application_model->getScholarshipApplicationListSearch($formData);
        
            $data['schemeList'] = $this->scholarship_application_model->schemeListByStatus('1');
            $data['programList'] = $this->scholarship_application_model->programListByStatus('1');



            // $data['scholarshipApplicationList'] = $this->scholarship_application_model->getScholarshipApplicationListByStudentId($id);
                     // echo "<Pre>";print_r($data);exit();

            $this->global['pageTitle'] = 'Campus Management System : List Scholarship Application';
            $this->loadViews("scholarship_application/approval_list", $this->global, $data, NULL);
        }
    }


    function view($id = NULL)
    {
        if ($this->checkScholarAccess('scholarship_application.approve') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {


        if ($id == null)
        {
            redirect('/scholarship/scholarshipApplication/list');
        }
        if($this->input->post())
        {

            // echo "<Pre>"; print_r($this->input->post());exit;

                $status = $this->security->xss_clean($this->input->post('status'));
                $reason = $this->security->xss_clean($this->input->post('reason'));


                $data = array(
                    'status' => $status,
                    'reason' => $reason
                );

                 $result = $this->scholarship_application_model->editScholarshipApplication($data,$id);
                redirect('/scholarship/scholarshipApplication/approvalList');

        }
        // $data['studentDetails'] = $this->scholarship_application_model->getStudentByStudentId($id);
        // $data['scholarshipApplication'] = $this->scholarship_application_model->getScholarshipApplication($id);
        $data['scholarship'] = $this->scholarship_application_model->getScholarshipApplication($id);
        $id_scholar_applicant = $data['scholarship']->id_student;
        $id_scheme = $data['scholarship']->id_scholarship_scheme;

        $data['countryList'] = $this->scholarship_application_model->countryList();
        $data['stateList'] = $this->scholarship_application_model->stateList();
        $data['raceList'] = $this->scholarship_application_model->raceListByStatus('1');
        $data['religionList'] = $this->scholarship_application_model->religionListByStatus('1');
        $data['programList'] = $this->scholarship_application_model->programListByStatus('1');

        $data['profileDetails'] = $this->scholarship_application_model->getApplicationPersonalDetailsByApplicationId($id_scholar_applicant,$id_scheme,$id);

        $data['educationDetails'] = $this->scholarship_application_model->getExamDetailsByApplicationId($id_scholar_applicant,$id_scheme,$id);
        $data['familyDetails'] = $this->scholarship_application_model->getFamilyDetailsByApplicationId($id_scholar_applicant,$id_scheme,$id);
        $data['employmentDetails'] = $this->scholarship_application_model->getEmploymentDetailsByApplicationId($id_scholar_applicant,$id_scheme,$id);
        $data['scheme'] = $this->scholarship_application_model->getSchemeById($id_scheme);
        

        $data['studentDetails'] = $this->scholarship_application_model->getStudentDetails($id_scholar_applicant);

            // echo "<Pre>"; print_r($data);exit;

        $this->global['pageTitle'] = 'Campus Management System : Approve Scholarship Application';
        $this->loadViews("scholarship_application/view", $this->global, $data, NULL);
        }
    }


    function diffDates()
    {
        // // Declare two dates 
        // $start_date = strtotime("2018-06-08"); 
        // $end_date = strtotime("2018-09-01"); 
          
        // // Get the difference and divide into  
        // // total no. seconds 60/60/24 to get  
        // // number of days 
        // echo "Difference between two dates: "
        //     . ($end_date - $start_date)/60/60/24;exit();
    }
}

