<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ProgramAccreditation extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('program_accreditation_model');
        $this->isScholarLoggedIn();
    }

    function list()
    {
        if ($this->checkScholarAccess('program_accreditation.list') == 1)
        // if ($this->checkScholarAccess('program_accreditation.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['id_accreditation_category'] = $this->security->xss_clean($this->input->post('id_accreditation_category'));
            $formData['id_accreditation_type'] = $this->security->xss_clean($this->input->post('id_accreditation_type'));

            $data['searchParam'] = $formData;

            $data['programAccreditationList'] = $this->program_accreditation_model->programAccreditationList($formData);

            $data['accreditationCategoryList'] = $this->program_accreditation_model->accreditationCategoryByStatus('1');
            $data['accreditationTypeList'] = $this->program_accreditation_model->accreditationTypeByStatus('1');


            $this->global['pageTitle'] = 'Scholarship Management System : Program Accreditation List';
            $this->loadViews("program_accreditation/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkScholarAccess('program_accreditation.add') == 1)
        // if ($this->checkScholarAccess('program_accreditation.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {
                $formData = $this->input->post();

               // echo "<Pre>"; print_r($formData);exit;
            
                $id_accreditation_category = $this->security->xss_clean($this->input->post('id_accreditation_category'));
                $id_accreditation_type = $this->security->xss_clean($this->input->post('id_accreditation_type'));
                $date_time = $this->security->xss_clean($this->input->post('date_time'));
                $accreditation_number = $this->security->xss_clean($this->input->post('accreditation_number'));
                $valid_from = $this->security->xss_clean($this->input->post('valid_from'));
                $valid_to = $this->security->xss_clean($this->input->post('valid_to'));
                $approval_date = $this->security->xss_clean($this->input->post('approval_date'));
                $reference_number = $this->security->xss_clean($this->input->post('reference_number'));


                $data = array(
                    'id_accreditation_category' => $id_accreditation_category,
                    'id_accreditation_type' => $id_accreditation_type,
                    'date_time' => date('Y-m-d', strtotime($date_time)),
                    'accreditation_number' => $accreditation_number,
                    'valid_from' => date('Y-m-d', strtotime($valid_from)),
                    'valid_to' => date('Y-m-d', strtotime($valid_to)),
                    'approval_date' => date('Y-m-d', strtotime($approval_date)),
                    'reference_number' => $reference_number,
                    'status' => 1,
					'created_by' => $user_id
                );

                $inserted_id = $this->program_accreditation_model->addNewProgramAccreditation($data);
                redirect('/scholarship/programAccreditation/list');
            }

            $data['accreditationCategoryList'] = $this->program_accreditation_model->accreditationCategoryByStatus('1');
            $data['accreditationTypeList'] = $this->program_accreditation_model->accreditationTypeByStatus('1');
               // echo "<Pre>"; print_r($data);exit;

            
            $this->global['pageTitle'] = 'Scholarship Management System : Add Program Accreditation';
            $this->loadViews("program_accreditation/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkScholarAccess('program_accreditation.edit') == 1)
        // if ($this->checkScholarAccess('program_accreditation.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/scholarship/programAccreditation/list');
            }
            if($this->input->post())
            {
                $formData = $this->input->post();

               // echo "<Pre>"; print_r($formData);exit;

	            $id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;

                $id_accreditation_category = $this->security->xss_clean($this->input->post('id_accreditation_category'));
                $id_accreditation_type = $this->security->xss_clean($this->input->post('id_accreditation_type'));
                $date_time = $this->security->xss_clean($this->input->post('date_time'));
                $accreditation_number = $this->security->xss_clean($this->input->post('accreditation_number'));
                $valid_from = $this->security->xss_clean($this->input->post('valid_from'));
                $valid_to = $this->security->xss_clean($this->input->post('valid_to'));
                $approval_date = $this->security->xss_clean($this->input->post('approval_date'));
                $reference_number = $this->security->xss_clean($this->input->post('reference_number'));



                $data = array(
                    'id_accreditation_category' => $id_accreditation_category,
                    'id_accreditation_type' => $id_accreditation_type,
                    'date_time' => date('Y-m-d', strtotime($date_time)),
                    'accreditation_number' => $accreditation_number,
                    'valid_from' => date('Y-m-d', strtotime($valid_from)),
                    'valid_to' => date('Y-m-d', strtotime($valid_to)),
                    'approval_date' => date('Y-m-d', strtotime($approval_date)),
                    'reference_number' => $reference_number,
                    'status' => 1,
                    'updated_by' => $user_id
                );

                //print_r($data);exit;
                $result = $this->program_accreditation_model->editProgramAccreditation($data,$id);
                redirect('/scholarship/programAccreditation/list');
            }
            // $data['studentList'] = $this->program_accreditation_model->studentList();
            $data['programAccreditation'] = $this->program_accreditation_model->getProgramAccreditation($id);
            $data['accreditationCategoryList'] = $this->program_accreditation_model->accreditationCategoryByStatus('1');
            $data['accreditationTypeList'] = $this->program_accreditation_model->accreditationTypeByStatus('1');
               // echo "<Pre>"; print_r($data);exit;

            $this->global['pageTitle'] = 'Scholarship Management System : Edit Program Accreditation';
            $this->loadViews("program_accreditation/edit", $this->global, $data, NULL);
        }
    }

    function deleteProgramAccreditation($id)
    {
        $deleted = $this->program_accreditation_model->deleteProgramAccreditation($id);   
        echo "Success"; 
    }
}
