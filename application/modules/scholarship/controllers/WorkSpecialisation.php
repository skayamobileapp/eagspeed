<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class WorkSpecialisation extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('work_specialisation_model');
        $this->isScholarLoggedIn();
    }

    function list()
    {
        if ($this->checkScholarAccess('work_specialisation.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;
            $data['workSpecialisationList'] = $this->work_specialisation_model->workSpecialisationListSearch($name);
            $this->global['pageTitle'] = 'Scholarship Management System : Work Specialisation List';
            $this->loadViews("work_specialisation/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkScholarAccess('work_specialisation.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'status' => $status
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->work_specialisation_model->addNewWorkSpecialisation($data);
                redirect('/scholarship/workSpecialisation/list');
            }
            $this->global['pageTitle'] = 'Scholarship Management System : Add Work Specialisation';
            $this->loadViews("work_specialisation/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkScholarAccess('work_specialisation.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/scholarship/workSpecialisation/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'status' => $status
                );

                $result = $this->work_specialisation_model->editWorkSpecialisation($data,$id);
                redirect('/scholarship/workSpecialisation/list');
            }
            $data['workSpecialisation'] = $this->work_specialisation_model->getWorkSpecialisation($id);
            $this->global['pageTitle'] = 'Scholarship Management System : Edit Work Specialisation';
            $this->loadViews("work_specialisation/edit", $this->global, $data, NULL);
        }
    }
}
