<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

require APPPATH . '/libraries/BaseController.php';

class FrequencyMode extends BaseController {
	public function __construct() {
		parent::__construct();
		$this->load->model('frequency_mode_model');
		$this->isScholarLoggedIn();
	}

	function list() {
		if ($this->checkScholarAccess('frequency_mode.list') == 1) {
			$this->loadAccessRestricted();
		} else {
			$data['frequencyModeList'] = $this->frequency_mode_model->frequencyModeList();
			$this->global['pageTitle'] = 'Campus Management System : Frequency Mode List';
			$this->loadViews("frequency_mode/list", $this->global, $data, NULL);
		}
	}

	function add() {
		if ($this->checkScholarAccess('frequency_mode.add') == 1) {
			$this->loadAccessRestricted();
		} else {
			if ($this->input->post()) {
				$code = $this->security->xss_clean($this->input->post('code'));
				$name = $this->security->xss_clean($this->input->post('name'));
				$name_optional_language = $this->security->xss_clean($this->input->post('name_optional_language'));
				$status = $this->security->xss_clean($this->input->post('status'));

				$data = array(
					'code' => $code,
					'name' => $name,
					'name_optional_language' => $name_optional_language,
					'status' => $status,
				);
				$inserted_id = $this->frequency_mode_model->addNewFrequencyMode($data);
				redirect('/scholarship/frequencyMode/list');
			}

			$this->global['pageTitle'] = 'Campus Management System : Add Frequency Mode';
			$this->loadViews("frequency_mode/add", $this->global, NULL, NULL);
		}
	}

	function edit($id = NULL) {
		if ($this->checkScholarAccess('frequency_mode.edit') == 1) {
			$this->loadAccessRestricted();
		} else {
			if ($id == null) {
				redirect('/scholarship/frequencyMode/list');
			}
			if ($this->input->post()) {
				$code = $this->security->xss_clean($this->input->post('code'));
				$name = $this->security->xss_clean($this->input->post('name'));
				$name_optional_language = $this->security->xss_clean($this->input->post('name_optional_language'));
				$status = $this->security->xss_clean($this->input->post('status'));

				$data = array(
					'code' => $code,
					'name' => $name,
					'name_optional_language' => $name_optional_language,
					'status' => $status,
				);
				$result = $this->frequency_mode_model->editFrequencyMode($data, $id);
				redirect('/scholarship/frequencyMode/list');
			}
			// $data['studentList'] = $this->frequency_mode_model->studentList();
			$data['frequencyMode'] = $this->frequency_mode_model->getFrequencyMode($id);
			$this->global['pageTitle'] = 'Campus Management System : Edit Frequency Mode';
			$this->loadViews("frequency_mode/edit", $this->global, $data, NULL);
		}
	}
}
