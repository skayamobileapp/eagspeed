<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Department extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('department_model');
        $this->isScholarLoggedIn();
    }

    function list()
    {
        if ($this->checkScholarAccess('department.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;
            $data['departmentDetails'] = $this->department_model->departmentListSearch($name);
            $this->global['pageTitle'] = 'Scholarship Management System : Department List';
            //print_r($subjectDetails);exit;
            $this->loadViews("department/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkScholarAccess('department.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'description' => $description,
                    'status'=>$status
                );
                $result = $this->department_model->addNewDepartment($data);
                redirect('/scholarship/department/edit/'.$result);
            }
            //print_r($data['stateList']);exit;
            //$this->load->model('department_model');
            $this->global['pageTitle'] = 'Scholarship Management System : Add Department';
            $this->loadViews("department/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkScholarAccess('department.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/scholarship/department/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'description' => $description,
                    'status'=>$status
                );
                
                $result = $this->department_model->editDepartment($data,$id);
                redirect('/scholarship/department/list');
            }
            $data['departmentDetails'] = $this->department_model->getDepartment($id);
            $data['departmentHasStaffList'] = $this->department_model->getDepartmentHasStaffList($id);
            $data['staffList'] = $this->department_model->staffListByStatus('1');
            $this->global['pageTitle'] = 'Scholarship Management System : Edit Department';
            $this->loadViews("department/edit", $this->global, $data, NULL);
        }
    }


    function addNewStaffToDepartment()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $tempData['effective_date'] = date('Y-m-d', strtotime($tempData['effective_date']));
        $inserted_id = $this->department_model->addNewStaffToDepartment($tempData);
            echo "<Pre>"; print_r($inserted_id);exit();

        if($inserted_id)
        {
            echo "success";
        }

    }

    function deleteDepartmentHasStaff($id)
    {
            // echo "<Pre>"; print_r($id);exit();
        $inserted_id = $this->department_model->deleteDepartmentHasStaff($id);
        echo "Success";
    }
}
