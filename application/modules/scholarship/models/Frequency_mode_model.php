<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Frequency_mode_model extends CI_Model
{
    function frequencyModeList()
    {
        $this->db->select('fm.*');
        $this->db->from('scholarship_frequency_mode as fm');
        // $this->db->join('country as c', 'sp.id_country = c.id');
        $this->db->order_by("fm.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getFrequencyMode($id)
    {
        $this->db->select('fm.*');
        $this->db->from('scholarship_frequency_mode as fm');
        $this->db->where('fm.id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewFrequencyMode($data)
    {
        $this->db->trans_start();
        $this->db->insert('scholarship_frequency_mode', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function editFrequencyMode($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('scholarship_frequency_mode', $data);
        return TRUE;
    }

    function frequencyModeListByStatus($status)
    {
        $this->db->select('fm.*');
        $this->db->from('scholarship_frequency_mode as fm');
        $this->db->where('fm.status', $status);
        $this->db->order_by("fm.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }
}

