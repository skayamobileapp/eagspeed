<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Sub_thrust_model extends CI_Model
{
    function moduleTypeList()
    {
        $this->db->select('*');
        $this->db->from('scholarship_sub_thrust');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function subThrustListSearch($search)
    {
        $this->db->select('sst.*, c.code as thrust_code, c.name as thrust_name');
        $this->db->from('scholarship_sub_thrust as sst');
        $this->db->join('scholarship_thrust as c','sst.id_thrust = c.id');
        if ($search['name'] != '')
        {
            $likeCriteria = "(sst.scholarship_name  LIKE '%" . $search['name'] . "%' or sst.scholarship_code  LIKE '%" . $search['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($search['id_thrust'] != '')
        {
            $this->db->where('sst.id_thrust', $search['id_thrust']);
        }
        $this->db->order_by("sst.scholarship_name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getSubThrust($id)
    {
        $this->db->select('*');
        $this->db->from('scholarship_sub_thrust');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewSubThrust($data)
    {
        $this->db->trans_start();
        $this->db->insert('scholarship_sub_thrust', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editSubThrust($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('scholarship_sub_thrust', $data);
        return TRUE;
    }

    function raceListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('scholarship_race_setup');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function thrustListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('scholarship_thrust');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function programListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('scholarship_programme');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function addSubThrustRequirement($data)
    {
        $this->db->trans_start();
        $this->db->insert('scholarship_sub_thrust_has_requirement', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function AddAgeRequirement($data)
    {
        $this->db->trans_start();
        $this->db->insert('scholarship_sub_thrust_requirement_has_age', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function AddEducationRequirement($data)
    {
        $this->db->trans_start();
        $this->db->insert('scholarship_sub_thrust_requirement_has_education', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function AddWorkExperienceRequirement($data)
    {
        $this->db->trans_start();
        $this->db->insert('scholarship_sub_thrust_requirement_has_work_experience', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function AddOtherRequirement($data)
    {
        $this->db->trans_start();
        $this->db->insert('scholarship_sub_thrust_requirement_has_other', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getSubThrustRequirementsBySubThrustId($id_sub_thrust)
    {
        $this->db->select('DISTINCT(sst.id_program) as id, sp.code as program_code, sp.name as program_name');
        $this->db->from('scholarship_sub_thrust_has_requirement as sst');
        $this->db->join('scholarship_programme as sp','sst.id_program = sp.id');
        $this->db->where('sst.id_sub_thrust', $id_sub_thrust);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getAgeRequirementsBySubThrustIdNProgramId($id_sub_thrust,$id_program)
    {
        $this->db->select('sst.*');
        $this->db->from('scholarship_sub_thrust_requirement_has_age as sst');
        $this->db->where('sst.id_sub_thrust', $id_sub_thrust);
        $this->db->where('sst.id_program', $id_program);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getEducationRequirementsBySubThrustIdNProgramId($id_sub_thrust,$id_program)
    {
        $this->db->select('sst.*, sel.short_name as education_level_short_name, sel.name as education_level_name');
        $this->db->from('scholarship_sub_thrust_requirement_has_education as sst');
        $this->db->join('education_level as sel','sst.id_qualification = sel.id');
        $this->db->where('sst.id_sub_thrust', $id_sub_thrust);
        $this->db->where('sst.id_program', $id_program);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getWorkExperienceRequirementsBySubThrustIdNProgramId($id_sub_thrust,$id_program)
    {
        $this->db->select('sst.*, sel.code as work_code, sel.name as work_name');
        $this->db->from('scholarship_sub_thrust_requirement_has_work_experience as sst');
        $this->db->join('scholarship_work_specialisation as sel','sst.id_specialisation = sel.id');
        $this->db->where('sst.id_sub_thrust', $id_sub_thrust);
        $this->db->where('sst.id_program', $id_program);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getOtherRequirementsBySubThrustIdNProgramId($id_sub_thrust,$id_program)
    {
        $this->db->select('sst.*');
        $this->db->from('scholarship_sub_thrust_requirement_has_other as sst');
        $this->db->where('sst.id_sub_thrust', $id_sub_thrust);
        $this->db->where('sst.id_program', $id_program);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function deleteAgeRequirement($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('scholarship_sub_thrust_requirement_has_age');
        return TRUE;
    }

    function deleteEducationRequirement($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('scholarship_sub_thrust_requirement_has_education');
        return TRUE;
    }

    function deleteWorkExperienceRequirement($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('scholarship_sub_thrust_requirement_has_work_experience');
        return TRUE;
    }

    function deleteOtherRequirement($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('scholarship_sub_thrust_requirement_has_other');
        return TRUE;
    }

    function qualificationListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('education_level');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function workSpecialisationListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('scholarship_work_specialisation');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getPartnerProgram($id_program)
    {
        $this->db->select('DISTINCT(sst.id_partner_university) as id, sel.code as partner_code, sel.name as partner_name');
        $this->db->from('scholarship_partner_university_has_program as sst');
        $this->db->join('scholarship_partner_university as sel','sst.id_partner_university = sel.id');
        $this->db->where('sst.id_program', $id_program);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function addScholarshipProgram($data)
    {
        $this->db->trans_start();
        $this->db->insert('scholarship_sub_thrust_has_program', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getScholarshipProgramBySubThrustId($id_sub_thrust)
    {
        $this->db->select('sst.*, sel.code as partner_code, sel.name as partner_name, sp.code as program_code, sp.name as program_name');
        $this->db->from('scholarship_sub_thrust_has_program as sst');
        $this->db->join('scholarship_partner_university as sel','sst.id_partner_university = sel.id');
        $this->db->join('scholarship_programme as sp','sst.id_program = sp.id');
        $this->db->where('sst.id_sub_thrust', $id_sub_thrust);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function deleteScholarshipProgram($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('scholarship_sub_thrust_has_program');
        return TRUE;
    }






    function addProgramEntryRequirement($data)
    {
        $this->db->trans_start();
        $this->db->insert('scholarship_sub_thrust_has_entry_requirement', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getScholarshipSubThrustEntryRequirement($id_sub_thrust)
    {
        $this->db->select('sst.*, sel.code as race_code, sel.name as race_name, qs.code as qualification_code, qs.name as qualification_name,');
        $this->db->from('scholarship_sub_thrust_has_entry_requirement as sst');
        $this->db->join('education_level as qs','sst.id_education_qualification = qs.id','left');
        $this->db->join('scholarship_race_setup as sel','sst.id_race = sel.id');
        $this->db->where('sst.id_sub_thrust', $id_sub_thrust);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function deleteScholarshipSubThrustEntryRequirement($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('scholarship_sub_thrust_has_entry_requirement');
        return TRUE;
    }
}