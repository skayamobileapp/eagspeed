<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Fee_structure_model extends CI_Model
{
    function programmeListSearch($search)
    {
        $this->db->select('p.*');
        $this->db->from('programme as p');
        if (!empty($search))
        {
            $likeCriteria = "(p.name  LIKE '%" . $search . "%' or p.code  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function intakeListByProgrammeId($id, $search)
    {
       $this->db->select('i.*, p.name as programme_name, p.code as programme_code');
        $this->db->from('intake as i');
        $this->db->join('intake_has_programme as ihp', 'ihp.id_intake = i.id');
        $this->db->join('programme as p', 'ihp.id_programme = p.id');
        if (!empty($search))
        {
            $likeCriteria = "(p.name  LIKE '%" . $search . "%' or i.name  LIKE '%" . $search . "%' or p.code  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('ihp.id_programme', $id);
        $this->db->order_by("i.name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getProgrammeById($id)
    {
       $this->db->select('p.*');
        $this->db->from('programme as p');
        $this->db->where('p.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getFeeStructure($id_programme,$id_intake)
    {
       $this->db->select('p.*');
        $this->db->from('fee_structure as p');
        $this->db->where('p.id_programme', $id_programme);
        $this->db->where('p.id_intake', $id_intake);
        $query = $this->db->get();
        return $query->row();
    }

    function getFeeStructureDetails($id_fee_structure)
    {
        $this->db->select('tfsd.*, fs.name as fee_structure, fm.name as frequency_mode');
        $this->db->from('fee_structure_details as tfsd');
        $this->db->join('fee_setup as fs', 'tfsd.id_fee_item = fs.id');   
        $this->db->join('frequency_mode as fm', 'tfsd.id_frequency_mode = fm.id');   
        $this->db->where('tfsd.id_fee_structure', $id_fee_structure);
        $query = $this->db->get();
        return $query->result();
    }

    

    function getIntakeById($id)
    {
       $this->db->select('p.*');
        $this->db->from('intake as p');
        $this->db->where('p.id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addTempDetails($data)
    {
        // echo "<Pre>";  print_r($data);exit;

        $this->db->trans_start();
        $this->db->insert('temp_fee_structure_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addNewFeeStructure($data)
    {
        // echo "<Pre>";  print_r($data);exit;

        $this->db->trans_start();
        $this->db->insert('fee_structure', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }


    function addNewFeeStructureDetails($data)
    {
        // echo "<Pre>";  print_r($data);exit;

        $this->db->trans_start();
        $this->db->insert('fee_structure_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    

    

    function updateTempDetails($data,$id) {
        $this->db->where('id', $id);
        $this->db->update('temp_fee_structure_details', $data);
        return TRUE;
    }

    function getTempFeeStructureDetails($id_session)
    {
        $this->db->select('tfsd.*, fs.name as fee_structure, fm.name as frequency_mode');
        $this->db->from('temp_fee_structure_details as tfsd');
        $this->db->join('fee_setup as fs', 'tfsd.id_fee_item = fs.id');   
        $this->db->join('frequency_mode as fm', 'tfsd.id_frequency_mode = fm.id');   
        $this->db->where('tfsd.id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }

    function deleteTempDataBySession($id_session)
    {
        $this->db->where('id_session', $id_session);
       $this->db->delete('temp_fee_structure_details');
    }


    function deleteTempData($id)
    {
        $this->db->where('id', $id);
       $this->db->delete('temp_fee_structure_details');
    }

    function getFeeStructureByIdProgrammeNIdIntake($id_programme,$id_intake)
    {
       $this->db->select('p.*, fs.name as fee_structure,fm.code as frequency_code, fm.name as frequency_mode');
        $this->db->from('fee_structure as p');
        $this->db->join('fee_setup as fs', 'p.id_fee_item = fs.id');   
        $this->db->join('frequency_mode as fm', 'p.id_frequency_mode = fm.id');
        $this->db->where('p.id_programme', $id_programme);
        $this->db->where('p.id_intake', $id_intake);
        $this->db->order_by("fs.name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function deleteFeeStructureById($id)
    {
        $this->db->where('id', $id);
       $this->db->delete('fee_structure');
    }

    
}

