<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Program Landscape</h3>
            </div>
        
        <form id="form_programme_landscape" action="" method="post">

             <br>
        <div class="form-container">
            <h4 class="form-group-title">Programme Details</h4>

            

          <div class="row">

            <div class="col-sm-4">
                <div class="form-group">
                    <label>Program Name <span class='error-text'>*</span></label>
                    <input type="text" class="form-control" id="name1" name="name1" value="<?php echo $programme->name; ?>" readonly="readonly">
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group">
                    <label>Program Code <span class='error-text'>*</span></label>
                    <input type="text" class="form-control" id="code" name="code" value="<?php echo $programme->code; ?>" readonly="readonly">
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group">
                    <label>Program Name In Malay <span class='error-text'>*</span></label>
                    <input type="text" class="form-control" id="name_optional_language" name="name_optional_language" value="<?php echo $programme->name_optional_language; ?>" readonly="readonly">
                </div>
            </div>
            
          </div>

        </div>

        <br>

        <div class="form-container">
        <h4 class="form-group-title">Program Landscape Details</h4>



            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Program Landscape Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name">
                    </div>
                </div>

                <!-- <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Program <span class='error-text'>*</span></label>
                        <select name="id_programme" id="id_programme" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($programmeList))
                            {
                                foreach ($programmeList as $record)
                                {?>
                                    <option value="<?php echo $record->id;?>"
                                    ><?php echo $record->name;?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div> -->

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Intake <span class='error-text'>*</span></label>
                        <select name="id_intake" id="id_intake" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($intakeList))
                            {
                                foreach ($intakeList as $record)
                                {?>
                                    <option value="<?php echo $record->id;?>"
                                    ><?php echo $record->name;?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


                <div class="col-sm-4">
                        <div class="form-group">
                            <label>Min Pass Subject <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="min_pass_subject" name="min_pass_subject">
                        </div>
                </div>
                
            </div>


            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Min. Total Cr. Hrs <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="min_total_cr_hrs" name="min_total_cr_hrs">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Min. Repeat Hrs <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="min_repeat_course" name="min_repeat_course">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Max. Repeat Exams <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="max_repeat_exams" name="max_repeat_exams">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Total Semester <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="total_semester" name="total_semester">
                    </div>
                </div>
                <div class="col-sm-4">
                        <div class="form-group">
                            <label>Total Blocks <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="total_block" name="total_block">
                        </div>
                </div>

                <div class="col-sm-4">
                        <div class="form-group">
                            <label>Total Levels <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="total_level" name="total_level">
                        </div>
                 </div>
            </div>

            <div class="row">

                <div class="col-sm-4">
                        <div class="form-group">
                            <label>Minimum Total Score <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="min_total_score" name="min_total_score">
                        </div>
                </div>

                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                            </label>                              
                        </div>                         
                </div>

            </div>

        </div>

            

            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="<?php echo '../programmeLandscapeList/' . $programme->id; ?>" class="btn btn-link">Cancel</a>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>
    $(document).ready(function() {
        $("#form_programme_landscape").validate({
            rules: {
                name:
                {
                    required: true
                },
                id_programme:
                {
                    required: true
                },
                id_intake:
                {
                    required: true
                },
                min_total_cr_hrs:
                {
                    required: true
                },
                min_repeat_course:
                {
                    required: true
                },
                max_repeat_exams:
                {
                    required: true
                },
                total_semester:
                {
                    required: true
                },
                total_block:
                {
                    required: true
                },
                total_level:
                {
                    required: true
                },
                min_total_score:
                {
                    required: true
                },
                min_pass_subject:
                {
                    required: true
                }
            },
            messages:
            {
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                id_programme: {
                    required: "<p class='error-text'>Select Program</p>",
                },
                id_intake: {
                    required: "<p class='error-text'>Select Intake</p>",
                },
                min_total_cr_hrs: {
                    required: "<p class='error-text'>Enter Min Total Cr. Hours</p>",
                },
                min_repeat_course: {
                    required: "<p class='error-text'>Enter Repeat Course</p>",
                },
                max_repeat_exams: {
                    required: "<p class='error-text'>Enter Repeat Exams</p>",
                },
                total_semester: {
                    required: "<p class='error-text'>Enter Total Semester</p>",
                },
                total_block: {
                    required: "<p class='error-text'>Enter Total Block</p>",
                },
                total_level: {
                    required: "<p class='error-text'>Enter Total Level</p>",
                },
                min_total_score: {
                    required: "<p class='error-text'>Enter Min Total Score</p>",
                },
                min_pass_subject: {
                    required: "<p class='error-text'>Enter Minimum Pass Subject</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script type="text/javascript">
    $('select').select2();
</script>