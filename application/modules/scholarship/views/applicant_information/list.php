<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List Program Landscape</h3>
      <a href="<?php echo '../add/' . $id_programme; ?>" class="btn btn-primary">+ Add Program Landscape</a>
    </div>

    <div class="page-title clearfix">
      <h4>Program Details</h4>
    </div>


      <br>
      <div class="row">

        <div class="col-sm-4">
            <div class="form-group">
                <label>Name</label>
                <input type="text" class="form-control" id="name" name="name" value="<?php echo $programme->name; ?>" readonly="readonly">
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group">
                <label>Code</label>
                <input type="text" class="form-control" id="code" name="code" value="<?php echo $programme->code; ?>" readonly="readonly">
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group">
                <label>Name Optional Language</label>
                <input type="text" class="form-control" id="name_optional_language" name="name_optional_language" value="<?php echo $programme->name_optional_language; ?>" readonly="readonly">
            </div>
        </div>
      <!-- </div> -->
        <br>
        <br>
        <br>

      </div>

      <div class="button-block clearfix">
          <div class="bttn-group">
              <a href="<?php echo '../list' ?>" class="btn btn-link">Back</a>
          </div>
      </div>



    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Landscape Name</th>
            <th>Program Name</th>
            <th>Intake</th>
            <th>Min Total Cr. Hrs</th>
            <th>Max Repeat Course</th>
            <th>Max Repeat Exams</th>
            <th>Total Semester</th>
            <th>Total BLock</th>
            <th>Total Level</th>
            <th>Min Total Score</th>
            <th>Min Pass Subjects</th>
            <th>Action</th>
            <th>Courses</th>
          </tr>
        </thead>
        <tbody>
          <?php
            if (!empty($programmeLandscapeList)) {
              $i=1;
            	foreach ($programmeLandscapeList as $record) {
            		?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->name ?></td>
                <td><?php echo $record->programme_code . " - " . $record->programme ?></td>
                <td><?php echo $record->intake_year . " - " . $record->intake ?></td>
                <td><?php echo $record->min_total_cr_hrs ?></td>
                <td><?php echo $record->min_repeat_course ?></td>
                <td><?php echo $record->max_repeat_exams ?></td>
                <td><?php echo $record->total_semester ?></td>
                <td><?php echo $record->total_block ?></td>
                <td><?php echo $record->total_level ?></td>
                <td><?php echo $record->min_total_score ?></td>
                <td><?php echo $record->min_pass_subject ?></td>
                <!-- <td><?php echo date("d-m-Y", strtotime($record->created_dt_tm)) ?></td> -->
                <td class="text-center">
                  <a href="<?php echo '../edit/' . $record->id . '/' . $id_programme; ?>" title="Edit">Edit</a>
                </td>
                <td class="text-center">
                  <a href="<?php echo '../addcourse/' . $record->id . '/' . $programme->id . '/' . $record->id_intake; ?>" title="Add Courses">Add Courses</a>
                </td>
              </tr>
          <?php
          $i++;
              }
            }
            ?>
        </tbody>
      </table>
    </div>

    

  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script type="text/javascript">
    $('select').select2();
    
      function clearSearchForm()
      {
        window.location.reload();
      }
</script>