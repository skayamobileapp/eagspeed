<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Course To Program Landscape</h3>
        </div>

        <form id="form_programme_landscape" action="" method="post">


        <div class="form-container">
            <h4 class="form-group-title">Programme Landscape Details</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Program Landscape Name *</label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $programmeLandscapeDetails->name; ?>" readonly>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Program *</label>
                        <select name="id_programme" id="id_programme" class="form-control" disabled>
                            <option value="">Select</option>
                            <?php
                            if (!empty($programmeList))
                            {
                                foreach ($programmeList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>"
                                        <?php 
                                        if($record->id == $programmeLandscapeDetails->id_programme)
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Intake *</label>
                        <select name="id_intake" id="id_intake" disabled="disabled" class="form-control">

                            <option value="">Select</option>
                            <?php
                            if (!empty($intakeList))
                            {
                                foreach ($intakeList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>"
                                        <?php 
                                        if($record->id == $id_intake)
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->year . " - " . $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

            </div>

        </div>

        <div class="form-container">
        <h4 class="form-group-title">Course Details</h4>

            <div class="row">

                <!-- <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Semester *</label>
                        <select name="id_semester" id="id_semester" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($semesterList))
                            {
                                foreach ($semesterList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>"
                                        >
                                        <?php echo $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div> -->

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Course *</label>
                        <select name="id_course" id="id_course" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($courseList))
                            {
                                foreach ($courseList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>"
                                        >
                                        <?php echo $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Course Type*</label>
                        <select name="course_type" id="course_type" class="form-control">
                            <option value="">Select</option>
                            <option value="Compulsory">Compulsory</option>
                            <option value="Major">Major</option>
                            <option value="Minor">Minor</option>
                            <option value="Not-Compulsory">Not Compulsory</option>

                        </select>
                    </div>
                </div>
                
            </div>

            <div class="row">
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <p>Pre Requisite *</p>
                        <label class="radio-inline">
                          <input type="radio" name="pre_requisite" id="pre_requisite" value="yes"><span class="check-radio"></span> Yes
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="pre_requisite" id="pre_requisite" value="No" >
                          <span class="check-radio"></span> No
                        </label>                              
                    </div>                       
                </div>
            </div>

        </div>

        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="<?php echo '../../../programmeLandscapeList/' . $id_programme; ?>" class="btn btn-link">Cancel</a>
            </div>
        </div>

    

        <form id="form_profile" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Course Details</h4>
  
   <!--  <div class="form-container">
        <h4 class="form-group-title">Profile Details</h4>   -->
    
        <div class="m-auto text-center">
           <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
         </div>
        <div class="clearfix">
        <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
            <li role="presentation" class="active"><a href="#education" class="nav-link border rounded text-center"
                    aria-controls="education" aria-selected="true"
                    role="tab" data-toggle="tab">Compulsary Course</a>
            </li>
            <li role="presentation"><a href="#proficiency" class="nav-link border rounded text-center"
                    aria-controls="proficiency" role="tab" data-toggle="tab">Major Course </a>
            </li>
            <li role="presentation"><a href="#employment" class="nav-link border rounded text-center"
                    aria-controls="employment" role="tab" data-toggle="tab">Minor Course</a>
            </li>
            <li role="presentation"><a href="#profile" class="nav-link border rounded text-center"
                    aria-controls="profile" role="tab" data-toggle="tab">Not Compulsary</a>
            </li>
        </ul>



        <div class="tab-content offers-tab-content">
            <div role="tabpanel" class="tab-pane active" id="education">
            <div class="col-12 mt-4">
                <br>


            <div class="form-container">
            <h4 class="form-group-title">Compulsary Course Details</h4>

                <div class="custom-table">
                  <table class="table" id="list-table">
                    <thead>
                      <tr>
                        <th>Landscape Name</th>
                        <!-- <th>Semester Name</th> -->
                        <th>Course Name</th>
                        <th>Intake</th>
                        <th>Course Type Name</th>
                        <th>Pre Requisite</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      if (!empty($getCompulsoryCourse)) {
                        foreach ($getCompulsoryCourse as $record) {
                      ?>
                          <tr>
                            <td><?php echo $record->programName ?></td>
                            <!-- <td><?php echo $record->semesterName ?></td> -->
                            <td><?php echo $record->coursename ?></td>
                            <td><?php echo $record->intake ?></td>
                            <td><?php echo $record->course_type ?></td>
                            <td><?php echo $record->pre_requisite ?></td>
                            <td class="text-center">
                              <?php echo anchor('setup/programmeLandscape/delete_course_program?id='.$record->id, 'DELETE', 'id="$record->id"'); ?>
                            </td>
                          </tr>
                      <?php
                        }
                      }
                      ?>
                    </tbody>
                  </table>
                </div>
            </div>
             
             </div> <!-- END col-12 -->  
            </div>

            <div role="tabpanel" class="tab-pane" id="proficiency">
                <div class="col-12 mt-4">
                    <br>


                <div class="form-container">
                <h4 class="form-group-title">Major Course Details</h4>  

                     <div class="custom-table">
                      <table class="table" id="list-table">
                        <thead>
                          <tr>
                            <th>Landscape Name</th>
                            <!-- <th>Semester Name</th> -->
                            <th>Course Name</th>
                            <th>Intake</th>
                            <th>Course Type Name</th>
                            <th>Pre Requisite</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                          if (!empty($getMajorCourse)) {
                            foreach ($getMajorCourse as $record) {
                          ?>
                              <tr>
                                <td><?php echo $record->programName ?></td>
                                <!-- <td><?php echo $record->semesterName ?></td> -->
                                <td><?php echo $record->coursename ?></td>
                                <td><?php echo $record->intake ?></td>
                                <td><?php echo $record->course_type ?></td>
                                <td><?php echo $record->pre_requisite ?></td>
                                <td class="text-center">
                                  <?php echo anchor('setup/programmeLandscape/delete_course_program?id='.$record->id, 'DELETE', 'id="$record->id"'); ?>
                                </td>
                              </tr>
                          <?php
                            }
                          }
                          ?>
                        </tbody>
                      </table>
                    </div>

                </div>
                             
                </div> <!-- END col-12 -->  
            </div>


            <div role="tabpanel" class="tab-pane" id="employment">
                <div class="col-12 mt-4">
                    <br>

            <div class="form-container">
                <h4 class="form-group-title">Minor Course Details</h4>  

                <div class="custom-table">
                  <table class="table" id="list-table">
                    <thead>
                      <tr>
                        <th>Landscape Name</th>
                        <!-- <th>Semester Name</th> -->
                        <th>Course Name</th>
                        <th>Intake</th>
                        <th>Course Type Name</th>
                        <th>Pre Requisite</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      if (!empty($getMinorCourse)) {
                        foreach ($getMinorCourse as $record) {
                      ?>
                          <tr>
                            <td><?php echo $record->programName ?></td>
                            <!-- <td><?php echo $record->semesterName ?></td> -->
                            <td><?php echo $record->coursename ?></td>
                            <td><?php echo $record->intake ?></td>
                            <td><?php echo $record->course_type ?></td>
                            <td><?php echo $record->pre_requisite ?></td>
                            <td class="text-center">
                              <?php echo anchor('setup/programmeLandscape/delete_course_program?id='.$record->id, 'DELETE', 'id="$record->id"'); ?>
                            </td>
                          </tr>
                      <?php
                        }
                      }
                      ?>
                    </tbody>
                  </table>
                </div>

            </div>
                         
             </div> <!-- END col-12 -->  
            </div>

        
            <div role="tabpanel" class="tab-pane" id="profile">
                <div class="col-12 mt-4">
                    <br>




                <div class="form-container">
                <h4 class="form-group-title">Other Documents Details</h4>  

                 <div class="custom-table">
                  <table class="table" id="list-table">
                    <thead>
                      <tr>
                        <th>Landscape Name</th>
                        <th>Semester Name</th>
                        <th>Course Name</th>
                        <th>Intake</th>
                        <th>Course Type Name</th>
                        <th>Pre Requisite</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      if (!empty($getNotCompulsoryCourse)) {
                        foreach ($getNotCompulsoryCourse as $record) {
                      ?>
                          <tr>
                            <td><?php echo $record->programName ?></td>
                            <td><?php echo $record->semesterName ?></td>
                            <td><?php echo $record->coursename ?></td>
                            <td><?php echo $record->intake ?></td>
                            <td><?php echo $record->course_type ?></td>
                            <td><?php echo $record->pre_requisite ?></td>
                            <td class="text-center">
                              <?php echo anchor('setup/programmeLandscape/delete_course_program?id='.$record->id, 'DELETE', 'id="$record->id"'); ?>
                            </td>
                          </tr>
                      <?php
                        }
                      }
                      ?>
                    </tbody>
                  </table>
                </div>

            </div>

       
                 
             </div> <!-- END col-12 -->  
            </div>

          </div>
        </div>

       </div> <!-- END row-->
    </div>
    </form>



   </div> <!-- END row-->
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script type="text/javascript">
    $('select').select2();
</script>
<script>
    $(document).ready(function() {
        $("#form_programme_landscape").validate({
            rules: {
                name:
                {
                    required: true
                },
                id_programme:
                {
                    required: true
                },
                id_semester:
                {
                    required: true
                },
                id_course:
                {
                    required: true
                },
                course_type:
                {
                    required: true
                },
                pre_requisite:
                {
                    required: true
                },
                total_semester:
                {
                    required: true
                },
                total_block:
                {
                    required: true
                },
                total_level:
                {
                    required: true
                },
                min_total_score:
                {
                    required: true
                },
                min_pass_subject:
                {
                    required: true
                }
            },
            messages:
            {
                name: {
                    required: "<p class='error-text'>Program Landscape Name Required</p>",
                },
                id_programme: {
                    required: "<p class='error-text'>Select Program</p>",
                },
                id_semester: {
                    required: "<p class='error-text'>Select Semester</p>",
                },
                id_course: {
                    required: "<p class='error-text'>Select Course</p>",
                },
                course_type: {
                    required: "<p class='error-text'>Select Course Type</p>",
                },
                pre_requisite: {
                    required: "<p class='error-text'>Select Pre Requisite</p>",
                },
                total_semester: {
                    required: "<p class='error-text'>Enter Total Semester</p>",
                },
                total_block: {
                    required: "<p class='error-text'>Enter Total Block</p>",
                },
                total_level: {
                    required: "<p class='error-text'>Enter Total Level</p>",
                },
                min_total_score: {
                    required: "<p class='error-text'>Enter Min Total Score</p>",
                },
                min_pass_subject: {
                    required: "<p class='error-text'>Enter Minimum Pass Subject</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
