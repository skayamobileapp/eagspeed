<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List Scholarship Application</h3>
      <!-- <a href="add" class="btn btn-primary">+ Add Scholarship Application</a> -->
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">
                
              <div class="row">

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Applicant Name</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="name" value="<?php echo $searchParam['name']; ?>">
                    </div>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Applicant NRIC</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="nric" value="<?php echo $searchParam['nric']; ?>">
                    </div>
                  </div>
                </div>

              </div>

              
              <div class="row">

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Application Number</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="application_number" value="<?php echo $searchParam['application_number']; ?>">
                    </div>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Select Program</label>
                    <div class="col-sm-8">
                      <select name="id_program" id="id_program" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($programList)) {
                          foreach ($programList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['id_program']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->code ."-".$record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>


              </div>

              <div class="row">

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Cohert</label>
                    <div class="col-sm-8">
                      <select name="id_scholarship_scheme" id="id_scholarship_scheme" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($schemeList)) {
                          foreach ($schemeList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['id_scholarship_scheme']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo $record->code ."-".$record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>


              </div>
              

              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <button type="reset" class="btn btn-link" onclick="clearSearchForm()">Clear All Fields</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Application Number</th>
            <th>Student</th>
            <th>Program</th>
            <th>Cohert</th>
            <th>Father Name</th>
            <th>Application Date</th>
            <!-- <th>Father Occupation</th>
            <th>No Of Sibblings</th>
            <th>Max Marks</th>
            <th>Obtained Marks</th> -->
            <!-- <th>Percentage</th> -->
            <th class="text-center">Status</th>
            <th class="text-center">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($scholarshipApplicationList))
          {
            $i=0;
            foreach ($scholarshipApplicationList as $record) {
          ?>
              <tr>
                <td><?php echo $i+1;?></td>
                <td><?php echo $record->application_number ?></td>
                <td><?php echo $record->nric . " - " . $record->student_name ?></td>
                <td><?php echo $record->program_code . " - " . $record->program_name ?></td>
                <!-- <td><?php echo $record->intake_year . " - " . $record->intake_name ?></td> -->
                <td><?php echo $record->scholarship_code . " - " . $record->scholarship_name ?></td>
                <td><?php echo $record->father_name ?></td>
                <td><?php echo date('d-m-Y', strtotime($record->created_dt_tm)) ?></td>
                <!-- <td><?php echo $record->father_occupation; ?></td> -->
                <!-- <td><?php echo $record->no_siblings ?></td> -->
                <!-- <td><?php echo $record->max_marks ?></td> -->
                <!-- <td><?php echo $record->obtained_marks ?></td> -->
                <!-- <td><?php echo $record->percentage ?></td> -->
                <td class="text-center"><?php 
                if( $record->status == '0')
                {
                  echo "Pending";
                }
                elseif( $record->status == '1')
                {
                  echo "Approved";
                }elseif( $record->status == '2')
                {
                  echo "Rejected";
                }
                ?></td>
                <td class="text-center">
                <a href="<?php echo 'edit/' . $record->id; ?>" title="Preview">View</a>
                </td>
              </tr>
          <?php
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script type="text/javascript">
    
    $('select').select2();

    function clearSearchForm()
      {
        window.location.reload();
      }
</script>