<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Sub Thrust</h3>
        </div>
        <form id="form_award" action="" method="post" enctype="multipart/form-data">

         <div class="form-container">
            <h4 class="form-group-title">Sub Thrust Details</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Thrust <span class='error-text'>*</span></label>
                        <select name="id_thrust" id="id_thrust" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($thrustList))
                            {
                                foreach ($thrustList as $record)
                                {?>
                            <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                            </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Scholarship Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="scholarship_code" name="scholarship_code">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Scholarship Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="scholarship_name" name="scholarship_name">
                    </div>
                </div>


            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Scholarship Short Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="scholarship_short_name" name="scholarship_short_name">
                    </div>
                </div>

                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                            </label>                              
                        </div>                         
                </div>  

            </div>

        </div>

        
        

        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>


        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>



    $(document).ready(function() {
        $("#form_award").validate({
            rules: {
                id_thrust: {
                    required: true
                },
                scholarship_code: {
                    required: true
                },
                scholarship_name: {
                    required: true
                },
                scholarship_short_name : {
                    required : true
                },
                status: {
                    required: true
                }
            },
            messages: {
                id_thrust: {
                    required: "<p class='error-text'>Select Thrust</p>",
                },
                scholarship_code: {
                    required: "<p class='error-text'>Code Required</p>",
                },
                scholarship_name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                scholarship_short_name: {
                    required: "<p class='error-text'>Short Name Required</p>",
                },
                status: {
                    required: "<p class='error-text'>Select Status</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

</script>

<script type="text/javascript">
    $('select').select2();
    
  $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
  } );
</script>