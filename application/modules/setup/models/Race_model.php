<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Race_model extends CI_Model
{
    function raceList()
    {
        $this->db->select('*');
        $this->db->from('race_setup');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function raceListSearch($search)
    {
        $this->db->select('rs.*, cre.name as creater_name, upd.name as updater_name');
        $this->db->from('race_setup as rs');
        $this->db->join('users as cre','rs.created_by = cre.id','left');
        $this->db->join('users as upd','rs.updated_by = upd.id','left');
        if (!empty($search))
        {
            $likeCriteria = "(rs.name  LIKE '%" . $search . "%' or rs.code  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("rs.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getRace($id)
    {
        $this->db->select('*');
        $this->db->from('race_setup');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewRace($data)
    {
        $this->db->trans_start();
        $this->db->insert('race_setup', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editRace($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('race_setup', $data);
        return TRUE;
    }
}

