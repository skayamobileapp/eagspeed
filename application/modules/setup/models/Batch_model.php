<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Batch_model extends CI_Model
{
    function awardList()
    {
        $this->db->select('a.*');
        $this->db->from('award as a');
        $this->db->order_by("id", "DESC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function batchListSearch($formData)
    {
        $this->db->select('a.*');
        $this->db->from('batch as a');
        if (!empty($formData['name']))
        {
            $likeCriteria = "(a.name  LIKE '%" . $formData['name'] . "%' or a.code  LIKE '%" . $formData['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        // if (!empty($formData['id_programme']))
        // {
        //     $likeCriteria = "(a.id_programme  LIKE '%" . $formData['id_programme'] . "%')";
        //     $this->db->where($likeCriteria);
        // }
        $this->db->order_by("a.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getBatch($id)
    {
        $this->db->select('*');
        $this->db->from('batch');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewBatch($data)
    {
        $this->db->trans_start();
        $this->db->insert('batch', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editBatch($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('batch', $data);
        return TRUE;
    }

    function getBatchHasProgramme($id)
    {
        $this->db->select('bhp.*, p.code as programme_code, p.name as programme_name');
        $this->db->from('batch_has_programme as bhp');
        $this->db->join('programme as p', 'bhp.id_programme = p.id');
        $this->db->where('bhp.id_batch', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function getProrammeListByStatus($status)
    {
        $this->db->select('p.*');
        $this->db->from('programme as p');
        $this->db->where('p.status', $status);
        $query = $this->db->get();
        return $query->result();
    }

    function getProgammeList()
    {
        $this->db->select('p.*');
        $this->db->from('programme as p');
        $query = $this->db->get();
        return $query->result();
    }

    function addBatchhasProgramme($data)
    {
        $this->db->trans_start();
        $this->db->insert('batch_has_programme', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function deleteBatchHasProgramme($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('batch_has_programme');
        return TRUE;
    }

    function getBatchProgramme($id)
    {
        $this->db->select('bhp.*');
        $this->db->from('batch_has_programme as bhp');
        $this->db->where('bhp.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function editBatchhasProgramme($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('batch_has_programme', $data);
        return TRUE;        
    }
}