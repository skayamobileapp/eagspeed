<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Cms_model extends CI_Model
{
   

     function getCms($id,$tablename)
    {
        $this->db->select('*');
        $this->db->from($tablename);
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
  

    function editCms($data, $id,$tablename)
    {
        $this->db->where('id', $id);
        $this->db->update($tablename, $data);
        return TRUE;
    }




}

