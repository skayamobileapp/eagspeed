<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Field_model extends CI_Model
{
    function fieldList()
    {
        $this->db->select('*');
        $this->db->from('field_setup');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function fieldListSearch($search)
    {
        $this->db->select('ss.*, cre.name as creater_name, upd.name as updater_name');
        $this->db->from('field_setup as ss');
        $this->db->join('users as cre','ss.created_by = cre.id','left');
        $this->db->join('users as upd','ss.updated_by = upd.id','left');
        if (!empty($search))
        {
            $likeCriteria = "(ss.name  LIKE '%" . $search . "%' or ss.sequence  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("ss.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getfield($id)
    {
        $this->db->select('*');
        $this->db->from('field_setup');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewfield($data)
    {
        $this->db->trans_start();
        $this->db->insert('field_setup', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editfield($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('field_setup', $data);
        return TRUE;
    }
}

