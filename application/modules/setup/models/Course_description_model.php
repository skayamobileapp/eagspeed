<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Course_description_model extends CI_Model
{
    function courseDescriptionList()
    {
        $this->db->select('*');
        $this->db->from('course_description_setup');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function courseDescriptionListSearch($search)
    {
        $this->db->select('*');
        $this->db->from('course_description_setup');
        if (!empty($search))
        {
            $likeCriteria = "(name  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getCourseDescription($id)
    {
        $this->db->select('*');
        $this->db->from('course_description_setup');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewCourseDescription($data)
    {
        $this->db->trans_start();
        $this->db->insert('course_description_setup', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editCourseDescription($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('course_description_setup', $data);
        return TRUE;
    }
}

