
<?php
        $this->load->model('role_model');
$roleModules = $this->role_model->getAllModules();

?>

<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Role</h3>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Role Edit</li>
                </ol>
            </div>
        </div>


        <form id="form_scheme" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Role Details</h4>

            <div class="row">

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="fname">Role</label>
                        <input type="text" class="form-control" id="fname" placeholder="Role Name" name="role" value="<?php echo $roleInfo->role; ?>" maxlength="128">
                        <input type="hidden" id="roleId" name="roleId" value="<?php echo $roleInfo->roleId;?>">
                        
                    </div>

                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <p>Status *</p>
                        <label class="radio-inline">
                          <input type="radio" name="status" id="status" value="1" <?php if($roleInfo->status=='1') {
                             echo "checked=checked";
                          };?>><span class="check-radio"></span> Active
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="status" id="status" value="0" <?php if($roleInfo->status=='0') {
                             echo "checked=checked";
                          };?>>
                          <span class="check-radio"></span> In-Active
                        </label>
                    </div>
                </div>

            </div>


        <div class="form-container">


            <div class="clearfix">



              <?php for($i=0;$i<count($roleModules);$i++) {

                 $moduleid = "Module".$roleModules[$i]->id;
                 $module_name = $roleModules[$i]->module_name; ?> 
                  
                <ul class="menu-tree">
                                        
                      <li>
                          <span class="icon folder collapsed" data-toggle="collapse" href="#menu<?php echo $moduleid;?>" aria-expanded="false" aria-controls="menu<?php echo $moduleid;?>"></span>
                          <div class="checkbox" >
                              <label>
                                <input type="checkbox"  id="chkparent<?php echo $roleModules[$i]->id;?>" onclick="checkallparent(<?php echo $roleModules[$i]->id;?>)">
                                <span class="check-radio"></span>
                                <?php echo $module_name;?>
                              </label>
                          </div> 
                            
                          <div class="collapse" id="menu<?php echo $moduleid;?>">
                            <?php

                               $moduleid = $roleModules[$i]->id;

                               $roleModulesMenuList = $this->role_model->getAllMenuByModuleId($moduleid);
                               ?>



                              <ul>

                                  <?php for($j=0;$j<count($roleModulesMenuList);$j++) { ?>


                                    <li>
                                      <span class="icon folder collapsed" data-toggle="collapse" href="#menu<?php echo $roleModulesMenuList[$j]->id;?>" aria-expanded="false" aria-controls="menu<?php echo $moduleid;?>"></span>
                                      <div class="checkbox">
                                          <label>
                                            <input type="checkbox"  id="chk<?php echo $roleModulesMenuList[$j]->id;?>" onclick="checkallchild(<?php echo $roleModulesMenuList[$j]->id;?>)">
                                            <span class="check-radio"></span>
                                            <?php echo $roleModulesMenuList[$j]->menu_name;?>                                            
                                        </label>
                                      </div> 
                                        
                                      <div class="collapse" id="menu<?php echo $roleModulesMenuList[$j]->id;?>">
                                          <ul>
                                          <?php 
                                          $getrolemenu = $this->role_model->getUsermenu($roleModulesMenuList[$j]->id); 
                                         ?>

                                         <?php for($m=0;$m<count($getrolemenu);$m++)
                                         {
                                            $permissionGranted = $this->role_model->checkPermission($roleInfo->roleId,$getrolemenu[$m]->id);
                                               $checked = "";
                                            if($permissionGranted)
                                            {
                                               $checked="checked=checked";
                                            }?>
                                              <li>
                                                <span class="icon page"></span>
                                                  <div class="checkbox">
                                                      <label>
                                                        <input type="checkbox" name="checkrole[]" value="<?php echo $getrolemenu[$m]->id;?>" id="<?php echo $roleModulesMenuList[$j]->id;?>-<?php echo $getrolemenu[$m]->id;?>" <?php echo $checked;?>>
                                                        <span class="check-radio"></span>
                                                        <?php echo $getrolemenu[$m]->description;?>
                                                      </label>
                                                  </div>                                         
                                              </li>
                                            <?php
                                          }
                                          ?> 
                                             
                                          </ul>
                                      </div>                              
                                  </li>

                                <?php }  ?> 
                                  
                                                              
                              </ul>
                          </div>                              
                      </li>
                  </ul>

                <?php } ?> 
              
            </div>


        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="../list" class="btn btn-link">Back</a>
            </div>
        </div>

        
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>
    $(document).ready(function() {
        $("#form_scheme").validate({
            rules: {
                description: {
                    required: true
                },
                code: {
                    required: true
                }
            },
            messages: {
                description: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                code: {
                    required: "<p class='error-text'>Code Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    function checkallchild(id) {



      var checkstatus = $("#chk"+id).prop("checked");
      if(checkstatus==true) {
        $( "[id^="+id+"-]" ).prop("checked",true); 
      }
      if(checkstatus==false){
        $( "[id^="+id+"-]" ).prop("checked",false); 
      }
    }


    function checkallparent(id) {
      
    }
</script>