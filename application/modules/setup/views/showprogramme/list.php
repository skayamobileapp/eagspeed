<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>Show coures in Whats new in myeduskills section</h3>
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">


                <div class="row">

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Product</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="name" value="<?php echo $searchParam['name']; ?>">
                      </div>
                    </div>
                  </div>

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Category Type</label>
                      <div class="col-sm-8">
                        <select name="id_category" id="id_category" class="form-control selitemIcon">
                            <option value="">Select</option>
                            <?php
                            if (!empty($categoryList))
                            {
                                foreach ($categoryList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                              <?php
                              if($record->id == $searchParam['id_category'])
                              {
                                echo "selected";
                              }
                              ?>
                              >
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                      </div>
                    </div>

                  </div>

                </div>



                <div class="row">

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Product Type</label>
                      <div class="col-sm-8">
                        <select name="id_programme_type" id="id_programme_type" class="form-control selitemIcon">
                            <option value="">Select</option>
                            <?php
                            if (!empty($productTypeSetupList))
                            {
                                foreach ($productTypeSetupList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                              <?php
                              if($record->id == $searchParam['id_programme_type'])
                              {
                                echo "selected";
                              }
                              ?>
                              >
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                      </div>
                    </div>

                  </div>

                </div>




              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href='list' class="btn btn-link">Clear All Fields</a>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Course Provider</th>
            <th>Course type</th>
            <th>Course Category</th>
            <th>Course Title</th>
            <th>Course Code</th>
            <th>Duration</th>
            <th>Course Image</th>
            <th>Status</th>
            <th style="text-align: center;">show in Whatsmyeduskills</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($programmeList)) {
            $i=1;
            foreach ($programmeList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->partneruniversityname ?></td>
                <td><?php echo $record->coursetype ?></td>
                <td><?php echo $record->categoryname ?></td>

                <td><?php echo $record->name ?></td>
                <td><?php echo $record->code ?></td>
                <td><?php echo $record->max_duration.' ';?> <?php echo $record->duration_type ?></td>

                <td>
                  <?php if($record->course_image!='') {?>

                                <a href="/assets/images/<?php echo $record->course_image;?>" target="_blank">View Image</a>
                              <?php } else { 
                                 echo "No image";
                               }?>  
                </td>
                <td><?php echo $record->status ?></td>
               
               
                                <td style="text-align: center;">
                                     <?php 
                                     $checked = '';
                                      if($record->showonhomepage=='1') {
                                        $checked = "checked=checked";
                                     } ?>
                                  <input type='checkbox' name="show<?php echo $record->id;?>" id="show<?php echo $record->id;?>" <?php echo $checked;?>  onchange="updateprogramme(<?php echo $record->id;?>)"/> </td>

              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script>

  $('select').select2();

  function clearSearchForm()
  {
    window.location.reload();
  }

  function updateprogramme(id) {
    var chb = $("#show"+id).is(':checked');
    console.log(chb);
    if(chb==true) {
      status = '1'
    } else {
      status = '0';
    }

     $.ajax(
            {
               url: '/prdtm/programme/updateshow',
                type: 'POST',
               data:
               {
                id: id,
                status : status
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                 
               }
            });



  }

 

</script>