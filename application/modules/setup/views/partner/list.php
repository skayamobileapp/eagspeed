<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>Partner</h3>
    </div>

 
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">
                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group">
                      <div class="col-sm-12">
                         <textarea class="form-control z-depth-1" rows="3" placeholder="Short Descripton..." name="name" id="name"><?php echo $awardDetails->name; ?></textarea>

                      </div>
                    </div>
                  </div>

                </div>
              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Save</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

  
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script src="<?php echo BASE_PATH; ?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript">




CKEDITOR.replace('name',{
  width: "100%",
  height: "200px"

}); 

</script>
<script>

      function clearSearchForm()
      {
        window.location.reload();
      }
</script>
