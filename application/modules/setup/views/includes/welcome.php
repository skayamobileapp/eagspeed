<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <!-- <div class="page-title clearfix">
            <h3>Welcome : Module SETUP</h3>
        </div> -->
    <?php

    if($id_role == 1)
    {
        ?>

        <div class="row">            
            <div class="col-sm-3 col-lg-2">
                <a href="/setup/welcome" class="dashboard-menu"><span class="icon"></span>System Setup</a>
            </div>
            <div class="col-sm-3 col-lg-2">
                <a href="/prdtm/welcome" class="dashboard-menu curriculm-management"><span class="icon"></span>Course Management</a>
            </div>   
            <div class="col-sm-3 col-lg-2">
                <a href="/pm/welcome" class="dashboard-menu partners-management"><span class="icon"></span>Partners Management</a>
            </div>

              <div class="col-sm-3 col-lg-2">
                <a href="/af/welcome" class="dashboard-menu academic-facilitator"><span class="icon"></span>Academic Facilitator Management</a>
            </div>  
  <div class="col-sm-3 col-lg-2">
                <a href="/examination/welcome" class="dashboard-menu exam-timetable"><span class="icon"></span>Assessment</a>
            </div>

            <div class="col-sm-3 col-lg-2">
                <a href="/records/welcome" class="dashboard-menu records"><span class="icon"></span>Records</a>
            </div>  



             <div class="col-sm-3 col-lg-2">
                <a href="/chatboat/welcome" class="dashboard-menu chatbot"><span class="icon"></span>Smart Chatbot</a>
            </div> 
            <div class="col-sm-3 col-lg-2">
                <a href="/finance/welcome" class="dashboard-menu student-finance"><span class="icon"></span>Finance</a>
            </div>  
           
            <div class="col-sm-3 col-lg-2">
                <a href="/communication/welcome" class="dashboard-menu communication-management"><span class="icon"></span>Communication Management</a>
            </div>
          
           
            <div class="col-sm-3 col-lg-2">
                <a href="/reports/welcome" class="dashboard-menu reporting"><span class="icon"></span>Reporting</a>
            </div>
            <!-- <div class="col-sm-3 col-lg-2">
                <a href="/cm/welcome" class="dashboard-menu class-timetable"><span class="icon"></span>Class Timetable &amp; Attendance</a>
            </div> 
            <div class="col-sm-3 col-lg-2">
                <a href="/examination/welcome" class="dashboard-menu examination-management"><span class="icon"></span>Examination Management</a>
            </div>
            <div class="col-sm-3 col-lg-2">
                <a href="/internship/welcome" class="dashboard-menu internship"><span class="icon"></span>Internship</a>
            </div>
            <div class="col-sm-3 col-lg-2">
                <a href="/graduation/welcome" class="dashboard-menu graduation"><span class="icon"></span>Graduation</a>
            </div>   
            <div class="col-sm-3 col-lg-2">
                <a href="/hostel/welcome" class="dashboard-menu hostel"><span class="icon"></span>Hostel</a>
            </div>
            <div class="col-sm-3 col-lg-2">
                <a href="#" class="dashboard-menu sponsorship"><span class="icon"></span>Sponsorship</a>
            </div>
            <div class="col-sm-3 col-lg-2">
                <a href="#" class="dashboard-menu alumni"><span class="icon"></span>Alumni</a>
            </div> -->
        </div>



        <?php
    }
    else
    {
        ?>

        <div class="welcome-container">
            <img src="<?php echo BASE_PATH; ?>assets/img/system_setup_icon.svg" alt="Partner Management Module">
            <h3>Welcome to <br/><strong>Setup Module</strong></h3>
            <p>Text........................</p>
        </div>


        <?php
    }
    ?>

        <footer class="footer-wrapper">
            <p>&copy; 2020 All rights, reserved</p>
        </footer>

    </div>
</div>