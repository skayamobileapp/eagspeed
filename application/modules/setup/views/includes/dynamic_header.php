<?php
$roleModel = new Role_Model();

$urlarray = explode ('/',$_SERVER['REQUEST_URI']);

// echo "<Pre>";print_r($urlarray);exit();

$urlmodule = $urlarray['1'];
$urlcontroller = $urlarray['2'];

// echo "<Pre>";print_r($roleList);exit();

$id_role = $this->session->role;

?>   

<!DOCTYPE html>
<html lang="en"> 
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $pageTitle; ?></title>
    <link rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/css/datatable.min.css">
    <link rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/css/bootstrap-multiselect.css">
        <link rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/css/main.css">

</head>
<body>
    <header class="navbar navbar-default navbar-fixed-top main-header">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand main-logo" href="/">Asia e University</a>
            </div>


        <?php
            if($id_role == 1)
            {
                ?>


            
            <nav class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="active"><a href="/setup/welcome">Setup</a></li>
                    <li><a href="/prdtm/welcome">Product Management</a></li>
                    <li><a href="/pm/welcome">Partner Management</a></li>
                    <li><a href="/records/welcome">Records</a></li>
                    <li><a href="/finance/welcome">Finance</a></li>
                    <li><a href="/af/welcome">Facilitator Management</a></li>
                    <li>
                        <button class="dropdown-toggle more-link" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">More
                        </button>

                        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                            <li><a href="/communication/welcome">Communication</a></li>
                            <li><a href="/mrktngm/welcome">Marketing Management</a></li>
                             
                            <li><a href="/examination/welcome">Assessment</a></li>
                             
                            <li><a href="/chatboat/welcome">Smart Chatboat</a></li>
                            <li><a href="/reports/welcome">Reports</a></li>
                            <li><a href="/setup/user/logout">Logout</a></li>
                        </ul>
                    </li>
                </ul>
            </nav>



                <?php
            }
            else
            {

                $moduleList  = $roleModel->getModuleListByIdRole();

                ?>
            <nav class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">

                    <?php

                echo "<Pre>";print_r($moduleList);exit();
                $i = 1;
                foreach ($moduleList as $menu)
                {
                    $module_name = $menu->module_name;

                    if($i < 7)
                    {

                   ?>
                    <li 
                    <?php
                    if($urlmodule == $module_name)
                    {
                        echo 'class="active"';
                    }
                    ?>>
                    <a href="<?php echo '/'. $module_name .'/welcome' ?>"><?php echo $module_name ?></a></li>


                   <?php
                    }else
                    {
                        ?>

                        <li>
                            <button class="dropdown-toggle more-link" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">More
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">

                            </ul>
                        </li>


                        <?php
                    }
                   $i++;
                }
                ?>


                    <li ><a href="/setup/welcome">Setup</a></li>
                    <li><a href="/pm/welcome">Partner Management</a></li>
                    <li><a href="/records/welcome">Records</a></li>
                    <li><a href="/finance/welcome">Finance</a></li>
                    <li><a href="/af/welcome">Facilitator Management</a></li>
                    <li>
                        <button class="dropdown-toggle more-link" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">More
                        </button>

                        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                            <li><a href="/communication/welcome">Communication</a></li>
                            <li><a href="/mrktngm/welcome">Marketing Management</a></li>
                             
                            <li><a href="/examination/welcome">Assessment</a></li>
                             
                            <li><a href="/chatboat/welcome">Smart Chatboat</a></li>
                            <li><a href="/reports/welcome">Reports</a></li>
                            <li><a href="/setup/user/logout">Logout</a></li>
                        </ul>
                    </li>
                </ul>
            </nav>



                <?php
            }
        ?>

            
        </div>
    </header>
</body>

     
