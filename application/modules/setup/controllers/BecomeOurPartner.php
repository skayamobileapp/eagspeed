<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class BecomeOurPartner extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('cms_model');
        $this->isLoggedIn();
    }

    function list()
    {
            $id = 1;
        if ($this->checkAccess('cms.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
             if($this->input->post())
            {
                $id_user = $this->session->userId;

                $name = $this->input->post('name');
            
                $data = array(
                    'name' => $name,
                    'updated_by' => $id_user,
                    'updated_at' => date('Y-m-d H:i:s')

                );


                $result = $this->cms_model->editCms($data,$id,'become_our_partner');
                redirect('/setup/becomeOurPartner/list');
            }

            $data['awardDetails'] = $this->cms_model->getCms($id,'become_our_partner');

            $this->global['pageTitle'] = 'Campus Management System : Award List';
            $this->loadViews("become_our_partner/list", $this->global, $data, NULL);
        }
    }
   
   
}
