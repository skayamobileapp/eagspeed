<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Field extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('field_model');
                $this->load->model('role_model');
        
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('field.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;
            $data['fieldList'] = $this->field_model->fieldListSearch($name);
            $this->global['pageTitle'] = 'Scholarship Management System : field List';
            $this->loadViews("field/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('field.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $id_user = $this->session->userId;
            $id_session = $this->session->my_session_id;

            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $sequence = $this->security->xss_clean($this->input->post('sequence'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'sequence' => $sequence,
                    'status' => $status,
                    'created_by' => $id_user
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->field_model->addNewfield($data);
                redirect('/setup/field/list');
            }
            $this->global['pageTitle'] = 'Scholarship Management System : Add field';
            $this->loadViews("field/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('field.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $id_user = $this->session->userId;
            $id_session = $this->session->my_session_id;

            if ($id == null)
            {
                redirect('/setup/field/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $sequence = $this->security->xss_clean($this->input->post('sequence'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'sequence' => $sequence,
                    'status' => $status,
                    'updated_by' => $id_user,
                    'updated_dt_tm' => date('Y-m-d H:i:s')
                );

                $result = $this->field_model->editfield($data,$id);
                redirect('/setup/field/list');
            }
            $data['field'] = $this->field_model->getfield($id);
            $this->global['pageTitle'] = 'Scholarship Management System : Edit field';
            $this->loadViews("field/edit", $this->global, $data, NULL);
        }
    }
}
