<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Religion extends BaseController
{
    public function __construct()
    {
        parent::__construct();
                $this->load->model('role_model');

        $this->load->model('religion_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('religion.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;
            $data['religionList'] = $this->religion_model->religionListSearch($name);
            $this->global['pageTitle'] = 'Campus Management System : Religion List';
            $this->loadViews("religion/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('religion.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $id_user = $this->session->userId;
            $id_session = $this->session->my_session_id;

            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'status' => $status,
                    'created_by' => $id_user
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->religion_model->addNewReligion($data);
                redirect('/setup/religion/list');
            }
            $this->global['pageTitle'] = 'Campus Management System : Add Religion';
            $this->loadViews("religion/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('religion.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $id_user = $this->session->userId;
            $id_session = $this->session->my_session_id;
            
            if ($id == null)
            {
                redirect('/setup/religion/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'status' => $status,
                    'updated_by' => $id_user,
                    'updated_dt_tm' => date('Y-m-d H:i:s')
                );

                $result = $this->religion_model->editReligion($data,$id);
                redirect('/setup/religion/list');
            }
            $data['religion'] = $this->religion_model->getReligion($id);
            $this->global['pageTitle'] = 'Campus Management System : Edit Religion';
            $this->loadViews("religion/edit", $this->global, $data, NULL);
        }
    }
}
