<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Industry extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('industry_model');
                $this->load->model('role_model');
        
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('industry.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;
            $data['industryList'] = $this->industry_model->industryListSearch($name);
            $this->global['pageTitle'] = 'Scholarship Management System : industry List';
            $this->loadViews("industry/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('industry.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $id_user = $this->session->userId;
            $id_session = $this->session->my_session_id;

            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $sequence = $this->security->xss_clean($this->input->post('sequence'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'sequence' => $sequence,
                    'status' => $status,
                    'created_by' => $id_user
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->industry_model->addNewindustry($data);
                redirect('/setup/industry/list');
            }
            $this->global['pageTitle'] = 'Scholarship Management System : Add industry';
            $this->loadViews("industry/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('industry.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $id_user = $this->session->userId;
            $id_session = $this->session->my_session_id;

            if ($id == null)
            {
                redirect('/setup/industry/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $sequence = $this->security->xss_clean($this->input->post('sequence'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'sequence' => $sequence,
                    'status' => $status,
                    'updated_by' => $id_user,
                    'updated_dt_tm' => date('Y-m-d H:i:s')
                );

                $result = $this->industry_model->editindustry($data,$id);
                redirect('/setup/industry/list');
            }
            $data['industry'] = $this->industry_model->getindustry($id);
            $this->global['pageTitle'] = 'Scholarship Management System : Edit industry';
            $this->loadViews("industry/edit", $this->global, $data, NULL);
        }
    }
}
