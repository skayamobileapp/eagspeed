<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Batch extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('batch_model');
        $this->isLoggedIn();
        error_reporting(0);
    }

    function list()
    {
        if ($this->checkAccess('batch.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $formData['name'] = $this->security->xss_clean($this->input->post('name'));

            $data['searchParam'] = $formData;

            $data['batchList'] = $this->batch_model->batchListSearch($formData);

            $this->global['pageTitle'] = 'Campus Management System : Batch List';
            $this->loadViews("batch/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('batch.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {


            if($this->input->post())
            {
                // echo "<Pre>";print_r($this->input->post());exit();

                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'code' => $code,
                    'name' => $name,
                    'description' => $description,
                    'start_date' => date('Y-m-d', strtotime($start_date)),
                    'end_date' => date('Y-m-d', strtotime($end_date)),
                    'status' => $status,
                    'created_by' => $id_user
                );

                $result = $this->batch_model->addNewBatch($data);

                // echo "<Pre>";// print_r($data);exit();


                redirect('/setup/batch/list');
            }

            // $data['programmeList'] = $this->batch_model->programmeList();


            $this->global['pageTitle'] = 'Campus Management System : Add Award';
            $this->loadViews("batch/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('batch.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/batch/list');
            }
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'code' => $code,
                    'name' => $name,
                    'description' => $description,
                    'start_date' => date('Y-m-d', strtotime($start_date)),
                    'end_date' => date('Y-m-d', strtotime($end_date)),
                    'status' => $status,
                    'created_by' => $id_user
                );

                $result = $this->batch_model->editBatch($data,$id);
                
                redirect('/setup/batch/list');
            }

            $data['id_batch'] = $id;
            $data['batch'] = $this->batch_model->getBatch($id);

            // echo "<Pre>";print_r($data['batch']);exit();

            
            $this->global['pageTitle'] = 'Campus Management System : Edit Batch';
            $this->loadViews("batch/edit", $this->global, $data, NULL);
        }
    }


    function programme($id = NULL,$id_batch_proramme = NULL)
    {
        if ($this->checkAccess('batch.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/batch/list');
            }
            if($this->input->post())
            {
                // echo "<Pre>";print_r($_POST);exit();

                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;


                $id_programme = $this->security->xss_clean($this->input->post('id_programme'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                // echo "<Pre>";print_r($start_date);exit();

                if($start_date)
                {
                    $start_date = date('Y-m-d', strtotime($start_date));
                    // echo "<Pre>";print_r($start_date);exit();
                }
            
                $data = array(
                    'id_batch' => $id,
                    'id_programme' => $id_programme,
                    'start_date' => $start_date,
                    'status' => 1
                );

                // echo "<Pre>";print_r($data);exit();

                if($id_batch_proramme > 0)
                {
                    $data['updated_by'] = $id_user;
                    $data['updated_dt_tm'] = date('Y-m-d H:i:s');
                    $result = $this->batch_model->editBatchhasProgramme($data,$id_batch_proramme);
                }
                else
                {
                    $data['created_by'] = $id_user;
                    $result = $this->batch_model->addBatchhasProgramme($data);
                }
                redirect('/setup/batch/programme/'.$id);
                // redirect($_SERVER['HTTP_REFERER']);
            }

            $data['id_batch'] = $id;
            $data['id_batch_proramme'] = $id_batch_proramme;
            $data['batch'] = $this->batch_model->getBatch($id);
            $data['batchHasProgramme'] = $this->batch_model->getBatchHasProgramme($id);
            $data['batchProgramme'] = $this->batch_model->getBatchProgramme($id_batch_proramme);
            $data['programmeList'] = $this->batch_model->getProgammeList();

            // echo "<Pre>";print_r($data['batchHasProgramme']);exit();

            
            $this->global['pageTitle'] = 'Campus Management System : Edit Batch';
            $this->loadViews("batch/programme", $this->global, $data, NULL);
        }
    }

    function deleteBatchHasProgramme($id)
    {
        $deleted = $this->batch_model->deleteBatchHasProgramme($id);
        return TRUE;
    }
}
