<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Degree extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('degree_model');
                $this->load->model('role_model');
        
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('degree.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;
            $data['degreeList'] = $this->degree_model->degreeListSearch($name);
            $this->global['pageTitle'] = 'Scholarship Management System : degree List';
            $this->loadViews("degree/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('degree.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $id_user = $this->session->userId;
            $id_session = $this->session->my_session_id;

            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $sequence = $this->security->xss_clean($this->input->post('sequence'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'sequence' => $sequence,
                    'status' => $status,
                    'created_by' => $id_user
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->degree_model->addNewdegree($data);
                redirect('/setup/degree/list');
            }
            $this->global['pageTitle'] = 'Scholarship Management System : Add degree';
            $this->loadViews("degree/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('degree.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $id_user = $this->session->userId;
            $id_session = $this->session->my_session_id;

            if ($id == null)
            {
                redirect('/setup/degree/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $sequence = $this->security->xss_clean($this->input->post('sequence'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'sequence' => $sequence,
                    'status' => $status,
                    'updated_by' => $id_user,
                    'updated_dt_tm' => date('Y-m-d H:i:s')
                );

                $result = $this->degree_model->editdegree($data,$id);
                redirect('/setup/degree/list');
            }
            $data['degree'] = $this->degree_model->getdegree($id);
            $this->global['pageTitle'] = 'Scholarship Management System : Edit degree';
            $this->loadViews("degree/edit", $this->global, $data, NULL);
        }
    }
}
