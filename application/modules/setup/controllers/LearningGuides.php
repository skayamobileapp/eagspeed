<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class LearningGuides extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('cms_model');
        $this->isLoggedIn();
    }

    function list()
    {
            $id = 1;
        if ($this->checkAccess('cms.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
             if($this->input->post())
            {
                $id_user = $this->session->userId;

                $name = $this->security->xss_clean($this->input->post('name'));
            
                $data = array(
                    'name' => $name,
                    'updated_by' => $id_user,
                    'updated_at' => date('Y-m-d H:i:s')

                );


                $result = $this->cms_model->editCms($data,$id,'learning_guides');
                redirect('/setup/LearningGuides/list');
            }

            $data['awardDetails'] = $this->cms_model->getCms($id,'learning_guides');

            $this->global['pageTitle'] = 'Campus Management System : Award List';
            $this->loadViews("learning_guides/list", $this->global, $data, NULL);
        }
    }
   
   
}

