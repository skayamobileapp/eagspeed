<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Faq extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('cms_model');
        $this->isLoggedIn();
    }

    function list()
    {
            $id = 1;
        if ($this->checkAccess('cms.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
             if($this->input->post())
            {
                $id_user = $this->session->userId;

                $name = $this->input->post('name');
            
                $data = array(
                    'name' => $name,
                    'updated_by' => $id_user,
                    'updated_at' => date('Y-m-d H:i:s')

                );


                $result = $this->cms_model->editCms($data,$id,'faq');
                redirect('/setup/faq/list');
            }

            $data['awardDetails'] = $this->cms_model->getCms($id,'faq');

            $this->global['pageTitle'] = 'Campus Management System : Award List';
            $this->loadViews("faq/list", $this->global, $data, NULL);
        }
    }
   
   
}

