<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Refund extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('cms_model');
        $this->isLoggedIn();
    }

    function list()
    {
            $id = 1;
        if ($this->checkAccess('cms.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
             if($this->input->post())
            {

                $name = $this->input->post('name');
            
                $data = array(
                    'name' => $name
                );


                $result = $this->cms_model->editCms($data,$id,'refund');
                redirect('/setup/refund/list');
            }

            $data['awardDetails'] = $this->cms_model->getCms($id,'refund');

            $this->global['pageTitle'] = 'Campus Management System : Award List';
            $this->loadViews("refund/list", $this->global, $data, NULL);
        }
    }
   
   
}
