<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Showprogramme extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('prdtm/programme_model');
        $this->isLoggedIn();
        error_reporting(0);
    }

    function list()
    {
        if ($this->checkAccess('programme.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            

            $data['programmeList'] = $this->programme_model->programmeListSearchApproved($formData);



            $this->global['pageTitle'] = 'Campus Management System : Program List';
            $this->loadViews("showprogramme/list", $this->global, $data, NULL);
        }
    }
  
}