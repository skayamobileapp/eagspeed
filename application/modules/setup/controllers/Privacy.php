<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Privacy extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('cms_model');
        $this->isLoggedIn();
    }

    function list()
    {
            $id = 1;
        if ($this->checkAccess('cms.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
             if($this->input->post())
            {

                $name = $this->input->post('name');
            
                $data = array(
                    'name' => $name
                );


                $result = $this->cms_model->editCms($data,$id,'privacy');
                redirect('/setup/privacy/list');
            }

            $data['awardDetails'] = $this->cms_model->getCms($id,'privacy');

            $this->global['pageTitle'] = 'Campus Management System : Award List';
            $this->loadViews("privacy/list", $this->global, $data, NULL);
        }
    }
   
   
}
