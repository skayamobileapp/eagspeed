<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class TermsAndCondition extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('cms_model');
        $this->isLoggedIn();
    }

    function list()
    {
            $id = 1;
        if ($this->checkAccess('cms.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
             if($this->input->post())
            {

                $name = $this->input->post('name');
            
                $data = array(
                    'name' => $name
                );


                $result = $this->cms_model->editCms($data,$id,'terms_and_condition');
                redirect('/setup/TermsAndCondition/list');
            }

            $data['awardDetails'] = $this->cms_model->getCms($id,'terms_and_condition');

            $this->global['pageTitle'] = 'Campus Management System : Award List';
            $this->loadViews("terms_and_condition/list", $this->global, $data, NULL);
        }
    }
   
   
}
