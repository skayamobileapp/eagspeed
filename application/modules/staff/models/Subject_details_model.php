<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Subject_details_model extends CI_Model
{
    function subjectDetailsList()
    {
        $this->db->select('sd.*');
        $this->db->from('subject_details as sd');
         $query = $this->db->get();
         $result = $query->result();        
         return $result;
    }
    
    function addNewSubjectDetails($countryInfo)
    {
        $this->db->trans_start();
        $this->db->insert('subject_details', $countryInfo);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getSubjectDetails($subjectId)
    {
        $this->db->select('*');
        $this->db->from('subject_details');
        $this->db->where('id', $subjectId);
        $query = $this->db->get();
        return $query->row();
    }

    function editSubjectDetails($subjectDetails, $subjectDetailsId)
    {
        $this->db->where('id', $subjectDetailsId);
        $this->db->update('subject_details', $subjectDetails);
        return TRUE;
    }
    
    function deleteSubjectDetails($countryId, $countryInfo)
    {
        $this->db->where('countryId', $countryId);
        $this->db->update('subject_details', $countryInfo);
        return $this->db->affected_rows();
    }
}

  