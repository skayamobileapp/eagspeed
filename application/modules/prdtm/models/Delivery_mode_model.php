<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Delivery_mode_model extends CI_Model
{
    function DeliveryModeList()
    {
        $this->db->select('*');
        $this->db->from('delivery_mode');
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }


    function getDeliveryModeDetails($id)
    {
        $this->db->select('*');
        $this->db->from('delivery_mode');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewDeliveryMode($data)
    {
        $this->db->trans_start();
        $this->db->insert('delivery_mode', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editDeliveryModeDetails($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('delivery_mode', $data);
        return TRUE;
    }

}

