<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Learning_mode_model extends CI_Model
{
    function LearningModeList()
    {
        $this->db->select('*');
        $this->db->from('learning_mode');
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }


    function getLearningModeDetails($id)
    {
        $this->db->select('*');
        $this->db->from('learning_mode');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewLearningMode($data)
    {
        $this->db->trans_start();
        $this->db->insert('learning_mode', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editLearningModeDetails($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('learning_mode', $data);
        return TRUE;
    }

}

