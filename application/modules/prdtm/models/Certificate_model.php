<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Certificate_model extends CI_Model
{
    function CertificateList()
    {
        $this->db->select('*');
        $this->db->from('certificate_setup');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function CertificateListSearch($search)
    {
        $this->db->select('el.*, cre.name as creater_name, upd.name as updater_name');
        $this->db->from('certificate_setup as el');
        $this->db->join('users as cre','el.created_by = cre.id','left');
        $this->db->join('users as upd','el.updated_by = upd.id','left');
        if (!empty($search))
        {
            $likeCriteria = "(el.name  LIKE '%" . $search . "%' or el.short_name  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("el.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


  function addAwardCondition($data)
    {
        $this->db->trans_start();
        $this->db->insert('certificate_condition', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;        
    }

   


    function getCertificate($id)
    {
        $this->db->select('*');
        $this->db->from('certificate_setup');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

       function getAward($id)
    {
        $this->db->select('*');
        $this->db->from('award');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    
    function addNewCertificate($data)
    {
        $this->db->trans_start();
        $this->db->insert('certificate_setup', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editCertificate($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('certificate_setup', $data);
        return TRUE;
    }


     function editAwardCondition($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('certificate_condition', $data);
        return TRUE;
    }

    function getConditionListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme_condition');
        $this->db->where('status', $status);
        $query = $this->db->get();
        return $query->result();        
    }

    function getAwardConditionList($id_award)
    {
        $this->db->select('ac.*, c.name as condition_name');
        $this->db->from('certificate_condition as ac');
        $this->db->join('programme_condition as c', 'ac.id_condition = c.id');
        $this->db->where('ac.status', 1);
        $this->db->where('ac.id_certificate', $id_award);
        $query = $this->db->get();
        return $query->result();        
    }

    function getAwardCondition($id)
    {
        $this->db->select('ac.*');
        $this->db->from('certificate_condition as ac');
        $this->db->where('ac.id', $id);
        $query = $this->db->get();
        return $query->row();        
    }

    function deleteAwardConditionDetails($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('certificate_condition');
        return TRUE;
    }

}