<?php
$programme_model = new Programme_Model();

$urlarray = explode ('/',$_SERVER['REQUEST_URI']);

$urlmodule = $urlarray['1'];
$urlcontroller = $urlarray['2'];
$urlaction = $urlarray['3'];

$id_product_type = $programmeDetails->id_programme_type;

$programme_tabs  = $programme_model->getProductTabsByProductId($id_product_type);

// echo "<Pre>";print_r($programme_tabs);exit();
?>
<div class="container-fluid page-wrapper">
   <div class="main-container clearfix">
       <ul class="page-nav-links">
               <li ><a href="/prdtm/programme/edit/<?php echo $id_programme;?>">Course Information</a></li>

 <li><a href="/prdtm/programme/faculty/<?php echo $id_programme;?>">Course Instructor</a></li>


            <li class="active"><a href="/prdtm/programme/newassessment/<?php echo $id_programme;?>">Course Assessment</a></li>

           

            <li><a href="/prdtm/programme/syllabus/<?php echo $id_programme;?>">Course Content</a></li>
            <li><a href="/prdtm/programme/fee/<?php echo $id_programme;?>">Course Fees</a></li>
            <li><a href="/prdtm/programme/award/<?php echo $id_programme;?>">Course Certificate</a></li>
            
                    <li><a href="/prdtm/programme/skill/<?php echo $id_programme;?>">Other Settings</a></li>

            <li><a href="/prdtm/programme/aim/<?php echo $id_programme;?>">Status of Course Details</a></li>

        </ul>

      <form id="form_programme" action="" method="post">
         <div class="form-container">
            <h4 class="form-group-title">Programme Assessment Details</h4>

             
                <div class="row">

                  <div class="col-sm-4">
                    <div class="form-group">
                       <label>Assesment <span class='error-text'>*</span></label>
                       <select name="id_assesment" id="id_assesment" class="form-control" required>
                          <option value="">Select</option>
                          <?php
                             if (!empty($assesmentList))
                             {
                                 foreach ($assesmentList as $record)
                                 { ?>
                          <option value="<?php echo $record->id; ?>"
                              <?php
                              if($record->id == $programAssesment->id_assesment)
                              {
                                echo 'selected';
                              }
                              ?>
                              ><?php echo $record->name; ?>
                          </option>
                          <?php
                             }
                             }
                             ?>
                       </select>
                    </div>
                  </div>
                     <div class="col-sm-4">
                      <div class="form-group">
                         <label>Enter weightage <span class='error-text'>*</span></label>
                         <input type="number" class="form-control" id="weightage" name="weightage" max="100" value="<?php echo $markDistribution->weightage; ?>">
                      </div>
                    </div>


                    <div class="col-sm-4">
                      <div class="form-group">
                         <label>Maximum Mark <span class='error-text'>*</span></label>
                         <input type="number" class="form-control" id="maximum_marks" name="maximum_marks" min="1" value="<?php echo $markDistribution->maximum_marks; ?>">
                      </div>
                    </div>

                </div>

                <div class="row">
                       <div class="col-sm-4">
                      <div class="form-group">
                         <label>Pass Mark <span class='error-text'>*</span></label>
                         <input type="number" class="form-control" id="pass_marks" name="pass_marks" min="1" value="<?php echo $markDistribution->pass_marks; ?>">
                      </div>
                    </div>

                      <div class="col-sm-4">
                        <div class="form-group">
                            <p>Is Pass Compulsary <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="is_pass" id="is_pass" value="1" <?php if($markDistribution->is_pass=='1') {
                                 echo "checked=checked";
                              };?>><span class="check-radio"></span> Yes
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="is_pass" id="is_pass" value="0" <?php if($markDistribution->is_pass=='0') {
                                 echo "checked=checked";
                              };?>>
                              <span class="check-radio"></span> No
                            </label>                              
                        </div>                         
                    </div>
                </div>




                 <div class="button-block clearfix">
                  <div class="form-group">

                      <button type="button" onclick="getProgrammeAssesmentDuplication()" class="btn btn-primary btn-lg" value="Modules" name="save">Save</button>
                     
                      <a href="<?php echo '../../newassessment/'. $id_programme ?>" class="btn btn-link">Cancel</a>
                    
                   </div>
                 </div>

         </div>
     
      </form>


      <?php

        if(!empty($assessmentDetailsList))
        {
            ?>

            <div class="form-container">
                    <h4 class="form-group-title">Programme Assessment Details</h4>

                

                  <div class="custom-table">
                    <table class="table">
                        <thead>
                            <tr>
                            <th>Sl. No</th>
                             <th>Assessment</th>
                             <th>Weightage</th>
                             <th>Maximum Marks</th>
                             <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                             <?php
                         $total = 0;
                          for($i=0;$i<count($assessmentDetailsList);$i++)
                         { ?>
                            <tr>
                            <td><?php echo $i+1;?></td>
                            <td><?php echo $assessmentDetailsList[$i]->assessment_name;?></td>
                            <td><?php echo $assessmentDetailsList[$i]->weightage;?></td>
                            <td><?php echo $assessmentDetailsList[$i]->maximum_marks;?></td>
                            <td class="text-center">
                                <a href='/prdtm/programme/newassessment/<?php echo $id_programme;?>/<?php echo $assessmentDetailsList[$i]->id;?>'>Edit</a> | 
                                <a onclick="deleteAssesmentData(<?php echo $assessmentDetailsList[$i]->id; ?>)">Delete</a>
                            </td>

                             </tr>
                          <?php 
                      } 
                      ?>
                        </tbody>
                    </table>
                  </div>

                </div>



      <?php
      }
      ?>


   </div>
</div>
<footer class="footer-wrapper">
   <p>&copy; 2019 All rights, reserved</p>
</footer>

<script type="text/javascript">

    $('select').select2();


	CKEDITOR.replace('overview',{
	  width: "100%",
	  height: "300px"
	});

  function getProgrammeAssesmentDuplication()
  {
    if($('#form_programme').valid())
    {
      var id_assesment = $("#id_assesment").val();

      if(id_assesment != '')
      {
        var tempPR = {};
        tempPR['id_assesment'] = id_assesment;
        tempPR['id_programme'] = "<?php echo $id_programme; ?>";
        tempPR['id'] = "<?php echo $id_programme_resources; ?>";
        

        $.ajax(
        {
           url: '/prdtm/programme/getProgrammeAssesmentDuplication',
            type: 'POST',
           data:
           {
            tempData: tempPR
           },
           error: function()
           {
            alert('Something is wrong');
           },
           success: function(result)
           {
              // alert(result);
              if(result == '0')
              {
                  alert('Duplicate Programme Assesment Not Allowed' );
                  $("#id_assesment").val('');
              }
              else
              if(result == '1')
              {
                $("#form_programme").submit();
              }
           }
        });
      }
    }
  }

   function deleteAssesmentData(id)
  	{
    	var cnf= confirm('Do you really want to delete?');
    	if(cnf==true)
    	{

	    $.ajax(
	        {
	           url: '/prdtm/programme/deleteAssesmentData/'+id,
	           type: 'GET',
	           error: function()
	           {
	            alert('Something is wrong');
	           },
	           success: function(result)
	           {
	              window.location.reload();
	           }
	        });
	    }
  }


</script>
<script src="<?php echo BASE_PATH; ?>assets/ckeditor/ckeditor.js"></script>


<script type="text/javascript">




</script>