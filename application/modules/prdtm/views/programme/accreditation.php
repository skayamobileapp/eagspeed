<?php
$programme_model = new Programme_Model();

$urlarray = explode ('/',$_SERVER['REQUEST_URI']);

$urlmodule = $urlarray['1'];
$urlcontroller = $urlarray['2'];
$urlaction = $urlarray['3'];

$id_product_type = $programmeDetails->id_programme_type;

$programme_tabs  = $programme_model->getProductTabsByProductId($id_product_type);

// echo "<Pre>";print_r($programme_tabs);exit();
?>

<div class="container-fluid page-wrapper">
   <div class="main-container clearfix">
      <ul class="page-nav-links">
            <li><a href="/prdtm/programme/edit/<?php echo $id_programme;?>">Product Details</a></li>

            <?php
            foreach ($programme_tabs as $individual_tab)
            {
              $tab = $individual_tab->tab;
              $title = $individual_tab->title;
            ?>
              <li 
              <?php
              if($tab == $urlaction)
              {
                echo 'class="active"';
              }
              ?>
              ><a href="/prdtm/programme/<?php echo $tab;?>/<?php echo $id_programme;?>"><?php echo $title ?></a></li>
            <?php
            }
            ?>

            <li><a href="/prdtm/programme/fee/<?php echo $id_programme;?>">Fee Structure</a></li>
            <li><a href="/prdtm/programme/markDistribution/<?php echo $id_programme;?>">Mark Distribution</a></li>

            <li><a href="/prdtm/programme/approval/<?php echo $id_programme;?>">Approval Details</a></li>

            <!-- <li><a href="/prdtm/programme/skill/<?php echo $id_programme;?>">Skills</a></li>

            <?php
          if ($programmeDetails->id_programme_type == '1')
          {
          ?>
            <li><a href="/prdtm/programme/overview/<?php echo $id_programme;?>">Description</a></li>
            <li><a href="/prdtm/programme/syllabus/<?php echo $id_programme;?>">Learning Outcomes</a></li>
            <li><a href="/prdtm/programme/topic/<?php echo $id_programme;?>">Topic</a></li>
            <li><a href="/prdtm/programme/faculty/<?php echo $id_programme;?>">Facilitator</a></li>
            <li><a href="/prdtm/programme/assessment/<?php echo $id_programme;?>">Assessment</a></li>
            <li class="active"><a href="/prdtm/programme/accreditation/<?php echo $id_programme;?>">Accreditation</a></li>
            <li><a href="/prdtm/programme/award/<?php echo $id_programme;?>">Award</a></li>
            <li><a href="/prdtm/programme/discount/<?php echo $id_programme;?>">Discounts</a></li>

          <?php
          }
          elseif ($programmeDetails->id_programme_type == '2')
          {
          ?>

            <li><a href="/prdtm/programme/structure/<?php echo $id_programme;?>">Programme Structure</a></li>
            <li><a href="/prdtm/programme/aim/<?php echo $id_programme;?>">Aim Of The Program</a></li>
            <li><a href="/prdtm/programme/modules/<?php echo $id_programme;?>">Modules to Courses</a></li>

          <?php
          }
          ?> -->

        </ul>

       


          <form id="form_acceredation" action="" method="post">
              <div class="form-container">
                  <h4 class="form-group-title"> Acceredation Details</h4>

                  <div class="row">

                      <div class="col-sm-4">
                          <div class="form-group">
                              <label>Acceredation Type <span class='error-text'>*</span></label>
                              <select name="acceredation_type" id="acceredation_type" class="form-control">
                                  <option value="">Select</option>
                                  <option value="Local"
                                  <?php
                                  if($programmeAcceredation->type == 'Local')
                                  {
                                    echo 'selected';
                                  }
                                  ?>
                                  >Local</option>
                                  <option value="International"
                                  <?php
                                  if($programmeAcceredation->type == 'International')
                                  {
                                    echo 'selected';
                                  }
                                  ?>
                                  >International</option>
                              </select>
                          </div>
                      </div>

                      <div class="col-sm-4">
                          <div class="form-group">
                              <label>Accreditation Body <span class='error-text'>*</span></label>
                              <select name="acceredation_category" id="acceredation_category" class="form-control">
                                  <option value="">Select</option>
                                  <option value="Board"
                                  <?php
                                  if($programmeAcceredation->category == 'Board')
                                  {
                                    echo 'selected';
                                  }
                                  ?>
                                  >Board</option>
                                  <option value="Kementarian"
                                  <?php
                                  if($programmeAcceredation->category == 'Kementarian')
                                  {
                                    echo 'selected';
                                  }
                                  ?>
                                  >Kementarian</option>
                                  <option value="MQA"
                                  <?php
                                  if($programmeAcceredation->category == 'MQA')
                                  {
                                    echo 'selected';
                                  }
                                  ?>
                                  >MQA</option>
                              </select>
                          </div>
                      </div>

                      <div class="col-sm-4">
                          <div class="form-group">
                              <label>Acceredation Date <span class='error-text'>*</span></label>
                              <input type="text" class="form-control datepicker" id="acceredation_dt" name="acceredation_dt" autocomplete="off" value="<?php if($programmeAcceredation->acceredation_dt){ echo date('d-m-Y', strtotime($programmeAcceredation->acceredation_dt)); } ?>">
                          </div>
                      </div>

                  </div>


                  <div class="row">

                      <div class="col-sm-4">
                          <div class="form-group">
                              <label>Acceredation Number <span class='error-text'>*</span></label>
                              <input type="text" class="form-control" id="acceredation_number" name="acceredation_number"value="<?php if($programmeAcceredation->acceredation_number){ echo $programmeAcceredation->acceredation_number; } ?>">
                          </div>
                      </div>


                      <div class="col-sm-4">
                          <div class="form-group">
                              <label>Validity From <span class='error-text'>*</span></label>
                              <input type="text" class="form-control datepicker" id="valid_from" name="valid_from" autocomplete="off" value="<?php if($programmeAcceredation->valid_from){ echo date('d-m-Y', strtotime($programmeAcceredation->valid_from)); } ?>">
                          </div>
                      </div>


                      <div class="col-sm-4">
                          <div class="form-group">
                              <label>Validity To <span class='error-text'>*</span></label>
                              <input type="text" class="form-control datepicker" id="valid_to" name="valid_to" autocomplete="off" value="<?php if($programmeAcceredation->valid_to){ echo date('d-m-Y', strtotime($programmeAcceredation->valid_to)); } ?>">
                          </div>
                      </div>

                  </div>

                  <div class="row">

                      <div class="col-sm-4">
                          <div class="form-group">
                              <label>Approval Date <span class='error-text'>*</span></label>
                              <input type="text" class="form-control datepicker" id="approval_date" name="approval_date" autocomplete="off" value="<?php if($programmeAcceredation->approval_dt){ echo date('d-m-Y', strtotime($programmeAcceredation->approval_dt)); } ?>">
                          </div>
                      </div>

                      <div class="col-sm-4">
                          <div class="form-group">
                              <label>Acceredation Reference <span class='error-text'>*</span></label>
                              <input type="text" class="form-control" id="acceredation_reference" name="acceredation_reference"value="<?php if($programmeAcceredation->acceredation_reference){ echo $programmeAcceredation->acceredation_reference; } ?>">
                          </div>
                      </div>


                  </div>

              </div>


              <div class="button-block clearfix">
                  <div class="bttn-group">
                      <button type="button" class="btn btn-primary btn-lg" onclick="saveAcceredationData()">Save</button>
                       <?php
                    if($id_accreditation != NULL)
                    {
                      ?>
                      <a href="<?php echo '../../accreditation/'. $id_programme ?>" class="btn btn-link">Cancel</a>
                      <?php
                    }
                    ?>
                      <!-- <a href="../list" class="btn btn-link">Back</a> -->
                  </div>
              </div>

          </form>




              <?php

              if(!empty($programmeAcceredationList))
              {
                  ?>
                  <br>

                  <div class="form-container">
                          <h4 class="form-group-title">Accrediation Details</h4>

                      

                        <div class="custom-table">
                          <table class="table">
                              <thead>
                                  <tr>
                                  <th>Sl. No</th>
                                   <th>Type</th>
                                   <th>Category</th>
                                   <th>Reference</th>
                                   <th>Date</th>
                                   <th>Number</th>
                                   <th>Valid From</th>
                                   <th>Valid To</th>
                                   <th>Approved Date</th>
                                   <th>Action</th>
                                  </tr>
                              </thead>
                              <tbody>
                                   <?php
                               $total = 0;
                                for($i=0;$i<count($programmeAcceredationList);$i++)
                               { ?>
                                  <tr>
                                  <td><?php echo $i+1;?></td>
                                  <td><?php echo $programmeAcceredationList[$i]->type;?></td>
                                  <td><?php echo $programmeAcceredationList[$i]->category;?></td>
                                  <td><?php echo $programmeAcceredationList[$i]->acceredation_reference;?></td>
                                  <td><?php echo date('d-m-Y', strtotime($programmeAcceredationList[$i]->acceredation_dt));?></td>
                                  <td><?php echo $programmeAcceredationList[$i]->acceredation_number;?></td>
                                  <td><?php echo date('d-m-Y', strtotime($programmeAcceredationList[$i]->valid_from));?></td>
                                  <td><?php echo date('d-m-Y', strtotime($programmeAcceredationList[$i]->valid_to));?></td>
                                  <td><?php echo date('d-m-Y', strtotime($programmeAcceredationList[$i]->approval_dt));?></td>
                                  <td>
                                  <a href='/prdtm/programme/accreditation/<?php echo $id_programme;?>/<?php echo $programmeAcceredationList[$i]->id;?>'>Edit</a> | 
                                  <a onclick="deleteAcceredationDetails(<?php echo $programmeAcceredationList[$i]->id; ?>)">Delete</a>
                                  </td>

                                   </tr>
                                <?php 
                            } 
                            ?>
                              </tbody>
                          </table>
                        </div>

                      </div>




              <?php
              
              }
               ?>







        



         



   </div>
</div>
<footer class="footer-wrapper">
   <p>&copy; 2019 All rights, reserved</p>
</footer>

<script type="text/javascript">

  $('select').select2();

  $(function()
  {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
  });

  function saveAcceredationData()
    {
        if($('#form_acceredation').valid())
        {
          $('#form_acceredation').submit();
          

        // var tempPR = {};
        // tempPR['category'] = $("#acceredation_category").val();
        // tempPR['type'] = $("#acceredation_type").val();
        // tempPR['acceredation_dt'] = $("#acceredation_dt").val();
        // tempPR['acceredation_number'] = $("#acceredation_number").val();
        // tempPR['valid_from'] = $("#valid_from").val();
        // tempPR['valid_to'] = $("#valid_to").val();
        // tempPR['approval_dt'] = $("#approval_date").val();
        // tempPR['acceredation_reference'] = $("#acceredation_reference").val();
        // tempPR['id_program'] = <?php echo $id_programme;?>;
        //     $.ajax(
        //     {
        //        url: '/prdtm/programme/saveAcceredationData',
        //         type: 'POST',
        //        data:
        //        {
        //         tempData: tempPR
        //        },
        //        error: function()
        //        {
        //         alert('Something is wrong');
        //        },
        //        success: function(result)
        //        {
        //         location.reload();
        //         // $("#view_scheme").html(result);
        //         // $('#myModal').modal('hide');
        //        }
        //     });
        }
    }



    function deleteAcceredationDetails(id)
    {
      var cnf= confirm('Do you really want to delete?');
      if(cnf==true)
      {
        $.ajax(
            {
               url: '/prdtm/programme/deleteAcceredationDetails/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    location.reload();
               }
            });
      }
    }



    $(document).ready(function()
     {
        $("#form_acceredation").validate({
            rules: {
                acceredation_category: {
                    required: true
                },
                acceredation_type: {
                    required: true
                },
                acceredation_dt: {
                    required: true
                },
                acceredation_number: {
                    required: true
                },
                valid_from: {
                    required: true
                },
                valid_to: {
                    required: true
                },
                approval_date: {
                    required: true
                },
                acceredation_reference: {
                    required: true
                }
            },
            messages: {
                acceredation_category: {
                    required: "<p class='error-text'>Select Category</p>",
                },
                acceredation_type: {
                    required: "<p class='error-text'>Select Acceredation Type</p>",
                },
                acceredation_dt: {
                    required: "<p class='error-text'>Select Acceredation Date</p>",
                },
                acceredation_number: {
                    required: "<p class='error-text'>Acceredation No. Required</p>",
                },
                valid_from: {
                    required: "<p class='error-text'>Select Validity Start Date</p>",
                },
                valid_to: {
                    required: "<p class='error-text'>Select Validity End Date</p>",
                },
                approval_date: {
                    required: "<p class='error-text'>Select Approval Date</p>",
                },
                acceredation_reference: {
                    required: "<p class='error-text'>Acceredation Reference Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


</script>