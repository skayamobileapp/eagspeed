<?php
$programme_model = new Programme_Model();

$urlarray = explode ('/',$_SERVER['REQUEST_URI']);

$urlmodule = $urlarray['1'];
$urlcontroller = $urlarray['2'];
$urlaction = $urlarray['3'];

$id_product_type = $programmeDetails->id_programme_type;

$programme_tabs  = $programme_model->getProductTabsByProductId($id_product_type);

// echo "<Pre>";print_r($programme_tabs);exit();
?>
<div class="container-fluid page-wrapper">
   <div class="main-container clearfix">
       <ul class="page-nav-links">
          <li ><a href="/prdtm/programme/edit/<?php echo $id_programme;?>">Course Information</a></li>

 <li><a href="/prdtm/programme/faculty/<?php echo $id_programme;?>">Course Instructor</a></li>


            <li><a href="/prdtm/programme/newassessment/<?php echo $id_programme;?>">Course Assessment</a></li>

           

            <li><a href="/prdtm/programme/syllabus/<?php echo $id_programme;?>">Course Content</a></li>
            <li><a href="/prdtm/programme/fee/<?php echo $id_programme;?>">Course Fees</a></li>
            <li><a href="/prdtm/programme/award/<?php echo $id_programme;?>">Course Certificate</a></li>
            
                    <li><a href="/prdtm/programme/skill/<?php echo $id_programme;?>">Other Settings</a></li>

            <li class="active"><a href="/prdtm/programme/aim/<?php echo $id_programme;?>">Status of Course Details</a></li>
        </ul>

      <form id="form_programme" action="" method="post">
         <div class="form-container">
            <h4 class="form-group-title">Status of the Course Tabs</h4>

             <div class="row">
              <div class="col-sm-12">
                   <table class="table" >
            <thead>
              <tr>
                <th>Menu</th>
                <th>Any Pending Item</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>Course Information</td>
                <td> No </td>
                <td> Completed</td>
              </tr>
              
                <tr>
                <td>Course Instructor</td>
                <td style="color:red;"> Yes <br/>
                    <ul><li> Add course Instructor</li>
                    </ul> </td>
                <td> In-Complete <br/>
                    <a href="/prdtm/programme/faculty/<?php echo $id_programme;?>">Click to Update</a></td>
              </tr>

               <tr>
                <td>Course Content</td>
                <td style="color:red;"> Yes <br/>
                    <ul><li> Add course Description</li>
                    </ul> </td>
                <td> In-Complete <br/>
                    <a href="/prdtm/programme/syllabus/<?php echo $id_programme;?>">Click to Update</a></td>
              </tr>

              <tr>
                <td>Course fee</td>
                <td> No  </td>
                <td> Complete <td>
              </tr>
              
            </tbody>
          </table>
               </div>
                </div>
               
         </div>
     
      </form>
   </div>
</div>
<footer class="footer-wrapper">
   <p>&copy; 2019 All rights, reserved</p>
</footer>

<script type="text/javascript">
      $('select').select2();


</script>
<script src="<?php echo BASE_PATH; ?>assets/ckeditor/ckeditor.js"></script>


<script type="text/javascript">



CKEDITOR.replace('aim',{
  width: "100%",
  height: "300px"

}); 

</script>