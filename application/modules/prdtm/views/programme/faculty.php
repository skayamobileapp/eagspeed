<?php
$programme_model = new Programme_Model();

$urlarray = explode ('/',$_SERVER['REQUEST_URI']);

$urlmodule = $urlarray['1'];
$urlcontroller = $urlarray['2'];
$urlaction = $urlarray['3'];

$id_product_type = $programmeDetails->id_programme_type;

$programme_tabs  = $programme_model->getProductTabsByProductId($id_product_type);

// echo "<Pre>";print_r($programme_tabs);exit();
?>
<div class="container-fluid page-wrapper">
   <div class="main-container clearfix">

       <ul class="page-nav-links">
             <li ><a href="/prdtm/programme/edit/<?php echo $id_programme;?>">Course Information</a></li>

 <li class="active"><a href="/prdtm/programme/faculty/<?php echo $id_programme;?>">Course Instructor</a></li>


            <li><a href="/prdtm/programme/newassessment/<?php echo $id_programme;?>">Course Assessment</a></li>

           

            <li><a href="/prdtm/programme/syllabus/<?php echo $id_programme;?>">Course Content</a></li>
            <li><a href="/prdtm/programme/fee/<?php echo $id_programme;?>">Course Fees</a></li>
            <li><a href="/prdtm/programme/award/<?php echo $id_programme;?>">Course Certificate</a></li>
            
                    <li><a href="/prdtm/programme/skill/<?php echo $id_programme;?>">Other Settings</a></li>

            <li><a href="/prdtm/programme/aim/<?php echo $id_programme;?>">Status of Course Details</a></li>


        </ul>
      <form id="form_programme" action="" method="post">
         <div class="form-container">
            <h4 class="form-group-title">Faculty Details</h4>
            <div class="row">
              
               <div class="col-sm-4">
                  <div class="form-group">
                     <label>Faculty NRIC / Name <span class='error-text'>*</span></label>
                     <input type="text" class="form-control" id="nric" name="nric" value="" required>
                  </div>
               </div>

               
               <div class="col-sm-4 pt-10">
                <div class="form-group">

                    <button type="submit" class="btn btn-primary btn-lg" value="Search" name="save">Search</button>
                 </div>
               </div>
              </div>

         </div>

<?php if($facultySearchList){ ?>
          <div class="custom-table">
          <table class="table" >
            <thead>
              <tr>
                <th>Sl. No</th>
                <th>Name</th>
                <th>NRIC</th>
                <th>Active Since</th>
                <th>Total Students</th>
                <th>Rating</th>
                <th>Total Course</th>
              </tr>
            </thead>
            <tbody>
              <?php
              
              $i=1;
                foreach ($facultySearchList as $record) {

              ?>
                  <tr>
                    <td><?php echo $i ?><input type='checkbox' name='faculty[]' value="<?php echo $record->id;?>"/></td>
                    <td><?php echo ucfirst($record->name) ?></td>
                    <td><?php echo $record->ic_no;?></td>
                    <td><?php echo date('d-m-Y',strtotime($record->created_dt_tm));?></td>
                    <td>2,341</td>
                    <td>4.8</td>
                                      <td>2</td>

                  </tr>
              <?php
              $i++;
                }
              
              ?>
            </tbody>
          </table>
       </div>
       <div class="row">

               <div class="col-sm-4 pt-10">
                <div class="form-group">

                    <button type="submit" class="btn btn-primary btn-lg" value="Add" name="save">Add</button>
                 </div>
        </div>
      </div>
<?php } ?>

     
      </form>
      <hr/>

       <div class="custom-table">
 <table class="table" >
         <thead>
            <tr>
              <th>Sl. No</th>
              <th>Name</th>
              <th>NRIC</th>
              <th>Active Since</th>
              <th>Total Students</th>
              <th>Rating</th>
              <th>Total Course</th>
              <th>Show Facilitator in e-commerce page</th>
            </tr>
          </thead>
          <tbody>
            <?php
            
            $i=1;
              foreach ($facultyList as $record) {

            ?>
                <tr>
                  <td><?php echo $i ?></td>
                  <td><?php echo ucfirst($record->staff) ?></td>
                  <td><?php echo $record->ic_no;?></td>
                  <td><?php echo date('d-m-Y',strtotime($record->created_dt_tm));?></td>
                  <td>2,341</td>
                  <td>4.8</td>
                                    <td>2</td>

                  <td><input type='radio' name='profileshow' value="<?php echo $record->id;?>" onclick="updateshowprofile(<?php echo $record->id;?>)"
                    <?php if ($record->status=='1') { echo "checked=checked";} ?>/>

                </tr>
            <?php
            $i++;
              }
            
            ?>
          </tbody>
        </table>
       </div>


   </div>
</div>
<footer class="footer-wrapper">
   <p>&copy; 2019 All rights, reserved</p>
</footer>


<script>
    $('select').select2();

function updateshowprofile(id) {
 var id_programme = <?php echo $id_programme;?>;;
 if(id!='') {

  $.get("/prdtm/programme/updatefacilitator/"+id+"/"+id_programme, function(data, status)
            {

            });

   
   }
}
</script>
