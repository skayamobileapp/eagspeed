<?php
$programme_model = new Programme_Model();

$urlarray = explode ('/',$_SERVER['REQUEST_URI']);

$urlmodule = $urlarray['1'];
$urlcontroller = $urlarray['2'];
$urlaction = $urlarray['3'];

$id_product_type = $programmeDetails->id_programme_type;

$programme_tabs  = $programme_model->getProductTabsByProductId($id_product_type);

?>

<div class="container-fluid page-wrapper">
   <div class="main-container clearfix">
      <form id="form_programme" action="" method="post" enctype="multipart/form-data">

        <div class="form-container">
            <h4 class="form-group-title">Product Details</h4>
            
            <div class="row">
               <div class="col-sm-12">
               <div class="custom-table">
                 <table class="table table-striped table-bordered">
                   <tbody>
                      <tr>
                       <td width="300" class="text-right">Course Provider
                        <input type='hidden' name='id_programme' id='id_programme' value="<?php echo $id_programme;?>"/></td>
                       <td>
                         <div class="row">
                           <div class="col-sm-6">
                             <div class="form-group">
                               <select name="id_partner_university" id="id_partner_university" class="form-control">
                                  <option value="">Select</option>
                                  <?php
                                     if (!empty($partnerList))
                                     {
                                         foreach ($partnerList as $record)
                                         { ?>
                                  <option value="<?php echo $record->id; ?>"
                                     <?php
                                        if ($record->id == $programmeDetails->id_partner_university)
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                     <?php echo $record->code . " - " . $record->name; ?>
                                  </option>
                                  <?php
                                     }
                                     }
                                     ?>
                               </select>
                                       </div>
                                     </div>
                                   </div>
                                 </td>
                               </tr>

                    

                       

                      <tr>
                       <td width="300" class="text-right">Course Type</td>
                       <td>
                         <div class="row">
                           <div class="col-sm-6">
                             <div class="form-group">
                               <select name="id_programme_type" id="id_programme_type" class="form-control">
                        <option value="">Select</option>
                        <?php
                           if (!empty($productTypeSetupList))
                           {
                               foreach ($productTypeSetupList as $record)
                               { ?>
                        <option value="<?php echo $record->id; ?>"
                           <?php
                              if ($programmeDetails->id_programme_type == $record->id)
                              {
                                  echo 'selected';
                              }
                              ?>
                           ><?php echo $record->name; ?>
                        </option>
                        <?php
                           }
                           }
                           ?>
                     </select>
                             </div>
                           </div>
                         </div>
                       </td>
                     </tr>


                         <tr>
                       <td width="300" class="text-right">Accredited By</td>
                       <td>
                         <div class="row">
                           <div class="col-sm-6">
                             <div class="form-group">
                               <select name="course_sub_type" id="course_sub_type" class="form-control">
                        <option value="">Select</option>
                        <option value="MQA" <?php if($programmeDetails->course_sub_type=='MQA'){ echo "selected=selected";} ?> >MQA</option>
                        <option value="HRDF" <?php if($programmeDetails->course_sub_type=='HRDF'){ echo "selected=selected"; } ?> >HRDF</option>
                       
                     </select>
                             </div>
                           </div>
                         </div>
                       </td>
                     </tr>


                      <tr>
                       <td width="300" class="text-right">Course Category </td>
                       <td>
                         <div class="row">
                           <div class="col-sm-6">
                             <div class="form-group">
                               <select name="id_category" id="id_category" class="form-control">
                                  <option value="">Select</option>
                                  <?php
                                     if (!empty($categoryList))
                                     {
                                         foreach ($categoryList as $record)
                                         { ?>
                                  <option value="<?php echo $record->id; ?>"
                                     <?php
                                        if ($programmeDetails->id_category == $record->id)
                                        {
                                            echo 'selected';
                                        }
                                        ?>
                                     ><?php echo $record->name; ?>
                                  </option>
                                  <?php
                                     }
                                     }
                                     ?>
                               </select>
                             </div>
                           </div>
                         </div>
                       </td>
                     </tr>


                      <tr>
                       <td width="300" class="text-right">Course Title </td>
                       <td>
                         <div class="row">
                           <div class="col-sm-6">
                             <div class="form-group">
                               <input type="text" type="text" class="form-control" id="name" name="name" value="<?php echo $programmeDetails->name; ?>" onblur="getProgrammeNameDuplication()">
                             </div>
                           </div>
                         </div>
                       </td>
                     </tr>

                     <tr>
                       <td width="300" class="text-right">Course Code </td>
                       <td>
                         <div class="row">
                           <div class="col-sm-6">
                             <div class="form-group">
                               <input type="text" type="text" class="form-control" id="code" name="code" value="<?php echo $programmeDetails->code; ?>" >
                             </div>
                           </div>
                         </div>
                       </td>
                     </tr>

                      <tr>
                       <td width="300" class="text-right">Language of Delivery</td>
                       <td>
                         <div class="row">
                           <div class="col-sm-6">
                             <div class="form-group">
                               <select name="language_of_delivery" id="language_of_delivery" class="form-control">
                        <option value="">Select</option>
                        <option value="English" <?php if($programmeDetails->language_of_delivery=='English'){ echo "selected=selected";} ?> >English</option>
                        <option value="Bahasa Melayu" <?php if($programmeDetails->language_of_delivery=='Bahasa Melayu'){ echo "selected=selected"; } ?> >Bahasa Melayu</option>
                       
                     </select>
                             </div>
                           </div>
                         </div>
                       </td>
                     </tr>


                       <tr>
                       <td width="300" class="text-right">Course Image</td>
                       <td>
                         <div class="row">
                           <div class="col-sm-6">
                              <input type="file" name="image" id="image">
                              <?php if($programmeDetails->course_image!='') {?>

                                <a href="/assets/images/<?php echo $programmeDetails->course_image;?>" target="_blank">View Image</a>
                              <?php } ?> 
                           </div>
                         </div>
                       </td>
                     </tr>



                     <tr>
                       <td width="300" class="text-right">Duration </td>
                       <td>
                         <div class="row">
                           <div class="col-sm-2">
                              <input type="number" class="form-control" id="max_duration" name="max_duration" min='1' value="<?php echo $programmeDetails->max_duration; ?>">
                           </div>
                           <div class="col-sm-4">
                             <div class="form-group">
                                                       <select name="duration_type" id="duration_type" class="form-control" required>
                           <option value="">Select</option>

                            <option value="Hours"
                              <?php
                                 if ($programmeDetails->duration_type == 'Hours')
                                 {
                                     echo 'selected';
                                 }
                                 ?>
                              >Hours</option>


                           <option value="Days"
                              <?php
                                 if ($programmeDetails->duration_type == 'Days')
                                 {
                                     echo 'selected';

                                 }
                                 ?>
                              >Days</option>
                           <option value="Weeks"
                              <?php
                                 if ($programmeDetails->duration_type == 'Weeks')
                                 {
                                     echo 'selected';
                                 }
                                 ?>
                              >Weeks</option>
                           <option value="Months"
                              <?php
                                 if ($programmeDetails->duration_type == 'Months')
                                 {
                                     echo 'selected';
                                 }
                                 ?>
                              >Months</option>
                           <option value="Years"
                              <?php
                                 if ($programmeDetails->duration_type == 'Years')
                                 {
                                     echo 'selected';
                                 }
                                 ?>
                              >Years</option>
                              </select>
                             </div>
                           </div>
                         </div>
                       </td>
                     </tr>

                       <tr>
                       <td width="300" class="text-right">Course Fee (RM)</td>
                       <td>
                         <div class="row">
                           <div class="col-sm-6">
                             <div class="form-group">
                              <?php 
                              $feeamount = 0;
                               if($programmeFees->amount>0) {
                                $feeamount = $programmeFees->amount;
                              } ?>
                               <input type="text" type="text" class="form-control" id="course_fee" name="course_fee" value="<?php echo $feeamount; ?>"  >
                                                           </div>
                           </div>
                         </div>
                       </td>
                     </tr>


                     <tr>
                       <td width="300" class="text-right">Course Credit </td>
                       <td>
                         <div class="row">
                           <div class="col-sm-6">
                             <div class="form-group">
                               <input type="text" type="text" class="form-control" id="credit" name="credit" value="<?php echo $programmeDetails->credit; ?>" >
                             </div>
                           </div>
                         </div>
                       </td>
                     </tr>




                       <tr>
                       <td width="300" class="text-right">Pre-requisite</td>
                       <td>
                         <div class="row">
                           <div class="col-sm-6">
                             <div class="form-group">
                            <input type="text" type="text" class="form-control" id="pre_requisite" name="pre_requisite" value="<?php echo $programmeDetails->pre_requisite; ?>" >
                          </div>
                        </div>
                         </div>
                       </td>
                     </tr>

                       <tr>
                       <td width="300" class="text-right">Competency Level </td>
                       <td>
                         <div class="row">
                           <div class="col-sm-6">
                             <div class="form-group">
                               <select name="id_study_level" id="id_study_level" class="form-control">
                                 <option value="">Select</option>
                                 <?php
                                    if (!empty($studyLevelList))
                                    {
                                        foreach ($studyLevelList as $record)
                                        { ?>
                                 <option value="<?php echo $record->id; ?>"
                                   <?php
                                       if ($programmeDetails->id_study_level == $record->id)
                                       {
                                           echo 'selected';
                                       }
                                       ?>

                                    ><?php echo $record->name; ?>
                                 </option>
                                 <?php
                                    }
                                    }
                                    ?>
                              </select>
                             </div>
                           </div>
                         </div>
                       </td>
                     </tr>

                     
                      <tr>
                       <td width="300" class="text-right">Education Level </td>
                       <td>
                         <div class="row">
                           <div class="col-sm-6">
                             <div class="form-group">
                               <select name="id_education_level" id="id_education_level" class="form-control">
                                 <option value="">Select</option>
                                 <?php
                                    if (!empty($educationLevelList))
                                    {
                                        foreach ($educationLevelList as $record)
                                        { ?>
                                 <option value="<?php echo $record->id; ?>"
                                    <?php
                                       if ($programmeDetails->id_education_level == $record->id)
                                       {
                                           echo 'selected';
                                       }
                                       ?>
                                    ><?php echo $record->name; ?>
                                 </option>
                                 <?php
                                    }
                                    }
                                    ?>
                              </select>
                             </div>
                           </div>
                         </div>
                       </td>
                     </tr>


                       <tr>
                       <td width="300" class="text-right">Delivery Modality </td>
                       <td>
                         <div class="row">
                           <div class="col-sm-6">
                             <div class="form-group">
                               <select name="id_delivery_mode" id="id_delivery_mode" class="form-control">
                                 <option value="">Select</option>
                                 <?php
                                    if (!empty($deliveryModeList))
                                    {
                                        foreach ($deliveryModeList as $record)
                                        { ?>
                                 <option value="<?php echo $record->id; ?>"
                                    <?php
                                       if ($programmeDetails->id_delivery_mode == $record->id)
                                       {
                                           echo 'selected';
                                       }
                                       ?>
                                    ><?php echo $record->name; ?>
                                 </option>
                                 <?php
                                    }
                                    }
                                    ?>
                              </select>
                             </div>
                           </div>
                         </div>
                       </td>
                     </tr>
                        <tr>
                       <td width="300" class="text-right">Learning Mode </td>
                       <td>
                         <div class="row">
                           <div class="col-sm-6">
                             <div class="form-group">
                               <select name="id_learning_mode" id="id_learning_mode" class="form-control">
                                 <option value="">Select</option>
                                 <?php
                                    if (!empty($learningModeList))
                                    {
                                        foreach ($learningModeList as $record)
                                        { ?>
                                 <option value="<?php echo $record->id; ?>" <?php
                                       if ($programmeDetails->id_learning_mode == $record->id)
                                       {
                                           echo 'selected';
                                       }
                                       ?>><?php echo $record->name; ?>
                                 </option>
                                 <?php
                                    }
                                    }
                                    ?>
                              </select>
                             </div>
                           </div>
                         </div>
                       </td>
                     </tr>




                    
                   

                    <tr>
                       <td width="300" class="text-right">Entry Qualification  </td>
                       <td>
                         <div class="row">
                           <div class="col-sm-12">
                             <div class="form-group">
                                 <textarea class="form-control z-depth-1" rows="3" placeholder="Short Descripton..." name="entry_qualification" id="entry_qualification"><?php echo $programmeDetails->entry_qualification; ?></textarea>
                             </div>
                           </div>
                         </div>
                       </td>
                     </tr>


                     
                      <tr>
                       <td width="300" class="text-right">Facilitator</td>
                       <td>
                         <div class="row">
                           <div class="col-sm-6">
                             <div class="form-group">
                               <select name="id_staff" id="id_staff" class="form-control">
                                  <option value="">Select</option>
                                  <?php
                                     if (!empty($staffList))
                                     {
                                         foreach ($staffList as $record)
                                         { ?>
                                  <option value="<?php echo $record->id; ?>"
                                     <?php
                                        if ($programmeDetails->id_staff == $record->id)
                                        {
                                            echo 'selected';
                                        }
                                        ?>
                                     ><?php echo $record->name; ?>
                                  </option>
                                  <?php
                                     }
                                     }
                                     ?>
                               </select>


                             </div>
                           </div>
                           <div class="col-sm-6">
                               <button type="button" onclick="addstaffmain()" class="btn btn-primary"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>                               
                           </div>
                         </div>
                       </td>
                     </tr>


                      <tr>
                       <td width="300" class="text-right">Staff List </td>
                       <td>
                         <div id="divstaff"></div>
                        </td>
                     </tr>





                      <tr>
                       <td width="300" class="text-right">Course Synopsis </td>
                       <td>
                         <div class="row">
                           <div class="col-sm-12">
                             <div class="form-group">
                                 <textarea class="form-control z-depth-1" rows="3" placeholder="Short Descripton..." name="synopsys" id="synopsys"><?php echo $programmeDetails->synopsys; ?></textarea>
                             </div>
                           </div>
                         </div>
                       </td>
                     </tr>
                      <tr>
                       <td width="300" class="text-right">CLO </td>
                       <td>
                         <div class="row">
                           <div class="col-sm-6">
                              <input type="text" class="form-control" id="clo" name="clo" >
                           </div>
                           <div class="col-sm-6">
                               <button type="button" onclick="addclomain()" class="btn btn-primary"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
                           </div>
                          </div>
                        </td>
                     </tr>
                     <tr>
                       <td width="300" class="text-right">CLO List </td>
                       <input type="hidden" name="id_clo" id="id_clo">

                       <td>
                         <div id="divclo"></div>
                        </td>
                     </tr>

                      <tr>
                       <td width="300" class="text-right">Topic </td>

                          <td>
                         <div class="row">
                              <div class="col-sm-11">
                                 <div class="row" style="padding-bottom: 9px;">
                                   <div class="col-sm-2" style="text-align: right;">
                                    <label>Name</label>
                                  </div>
                                  <div class="col-sm-10">
                                      <input type="text" class="form-control" id="topic" name="topic" >
                                   </div>
                                 </div>
                                    <div class="row" style="padding-bottom: 9px;">
                                   <div class="col-sm-2" style="text-align: right;">
                                    <label>Overview</label>
                                  </div>
                                  <div class="col-sm-10">

                                      <input type="text" class="form-control" id="topic_overview" name="topic_overview" >
                                   </div>
                                 </div>
                                     
                                 <div class="row">
                                  <div class="col-sm-2" style="text-align: right;">
                                    <label>Outcome</label>
                                  </div>
                                   <div class="col-sm-10">
                                       <textarea class="form-control z-depth-1" rows="3" placeholder="Short Descripton..." 
                                       name="topic_outcome" id="topic_outcome"></textarea>
                                   </div>
                                  </div>                                 
                             </div>
                             <div class="col-sm-1">
                               <button type="button" onclick="addtopicmain()" class="btn btn-primary"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>                                   
                             </div>
                         </div>  

                        </td>
                     </tr>
                     <tr>
                       <td width="300" class="text-right">Topic List </td>
                       <td><input type="hidden" name="id_topic" id="id_topic">
                         <div id="divtopic"></div>
                        </td>
                     </tr>
                      <tr>
                       <td width="300" class="text-right">Overall Assessment Information </td>

                       <td>
                           <div class="form-horizontal">
                             <?php for($i=0;$i<count($assessmentList);$i++) { 
                              $marks=0;
                                for($j=0;$j<count($getprogrammeAssignment);$j++) {

                                    if($getprogrammeAssignment[$j]->id_assignment==$assessmentList[$i]->id) {

                                        $marks = $getprogrammeAssignment[$j]->marks;
                                    }
                                }
                            ?>
                             
                              <div class="form-group">
                                <label for="inputEmail3" class="col-sm-5 control-label"><?php echo $assessmentList[$i]->name;?></label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="overallassissment[]" name="overallassissment[<?php echo $assessmentList[$i]->id;?>]"  value="<?php echo $marks;?>">
    
                                     <input type="hidden" class="form-control" id="overallassissmentids[]" name="overallassissmentids[]" value="<?php echo $assessmentList[$i]->id;?>">
                                </div>
                              </div>                             
                           <?php } ?>                                
                           </div>
                       </td>


                     </tr>
                     <tr>
                       <td width="300" class="text-right">CERTIFICATE </td>
                       <td>
                         <?php for($i=0;$i<count($certificateList);$i++) { 

                          $checked="";
                              for($j=0;$j<count($certificateProgrammeList);$j++) {
                                   
                                  if($certificateList[$i]->id==$certificateProgrammeList[$j]->id_certificate) {
                                      $checked="Checked=checked";
                                  }
                              }


                          ?>
                         <div class="row">
                           <div class="col-sm-12">
                            <label class="checkbox-inline">
                              <input type="checkbox" name='overallcertificate[<?php echo $certificateList[$i]->id;?>]' value="<?php echo $certificateList[$i]->id;?>" <?php echo $checked;?> >
                              <span class="check-radio"></span><?php echo $certificateList[$i]->name;?>
<input type="hidden" class="form-control" id="overallcertificateids[]" name="overallcertificateids[]" value="<?php echo $certificateList[$i]->id;?>">                              
                            </label>                               

                           
                           </div>
                         </div>
                       <?php } ?> 
                       </td>
                     </tr>
                      <tr>
                       <td width="300" class="text-right">Status</td>
                      <td>  <div class="row">
                           <div class="col-sm-4">
                             <div class="form-group"><select name="status" id="status" class="form-control">
              <option value="">Select </option>
              <option value="Pending Modification" <?php if($programmeDetails->status=='Pending Modification') {echo "selected=selected";} ?>>Pending Modification</option>
              <option value="Pending Approval" <?php if($programmeDetails->status=='Pending Approval') {echo "selected=selected";} ?>>Pending Approval</option>
              <option value="Approved Pending Content Update" <?php if($programmeDetails->status=='Approved Pending Content Update') {echo "selected=selected";} ?>>Approved Pending Content Update</option>
              <option value="Approved and Published" <?php if($programmeDetails->status=='Approved and Published') {echo "selected=selected";} ?>>Approved and Published</option>
            </select></div></div></div></td>
                    </tr>
                  </tbody>
               </table>
            </div>
            </div>
         </div>


               




      <div class="button-block clearfix">
         <div class="bttn-group">
            <button type="submit" class="btn btn-primary btn-lg" >Save</button>
            <a href="../list" class="btn btn-link">Back</a>
         </div>
      </div>


      <div id="view_fields" style="display: none;">
      </div>
   
   </div>


         </form>

</div>
<footer class="footer-wrapper">
   <p>&copy; 2019 All rights, reserved</p>
</footer>
</div>
</div>


<script src="<?php echo BASE_PATH; ?>assets/ckeditor/ckeditor.js"></script>

<script type="text/javascript">




CKEDITOR.replace('entry_qualification',{
  width: "100%",
  height: "100px"

}); 

CKEDITOR.replace('synopsys',{
  width: "100%",
  height: "100px"

}); 

// CKEDITOR.replace('assessment_details',{
//   width: "100%",
//   height: "100px"

// }); 


CKEDITOR.replace('topic_outcome',{
  width: "100%",
  height: "100px"

}); 




</script>

</script>

<style type="text/css">
   .shadow-textarea textarea.form-control::placeholder {
   font-weight: 300;
   }
   .shadow-textarea textarea.form-control {
   padding-left: 0.8rem;
   }
</style>
<script>


        function deleteclomaindata(id) {
    var cnf = confirm("Do you really want to delete?");
    if(cnf==true) {
        var tempPR = {};
        tempPR['clo_id'] = id;
    $.ajax(
            {
               url: '/prdtm/programme/deleteclomain',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                editclodata();
               }
            });
    }
  }


    function edittopicdataById(id) {

       $.ajax(
            {
               url: '/prdtm/programme/edittopic',
                type: 'POST',
               data:
               {
                id: id
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                 var topicarray = JSON.parse(result);
                 $("#topic").val(topicarray['topic_name']);
                 $("#topic_overview").val(topicarray['topic_overview']);
                 $("#id_topic").val(id);
                 CKEDITOR.instances['topic_outcome'].setData(topicarray['topic_outcome']);
               }
            });

    }



    function editclo(id){
       $.ajax(
            {
               url: '/prdtm/programme/editclo',
                type: 'POST',
               data:
               {
                id: id
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                 var topicarray = JSON.parse(result);

                                 $("#clo").val(topicarray['clo_name']);

                 $("#id_clo").val(id);
               }
            });
    }


        function deletetopicmaindata(id) {
    var cnf = confirm("Do you really want to delete?");
    if(cnf==true) {
        var tempPR = {};
        tempPR['topic_id'] = id;
    $.ajax(
            {
               url: '/prdtm/programme/deletetopicmain',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                edittopicdata();
               }
            });
    }
  }


   function deletestaffmaindata(id) {
    var cnf = confirm("Do you really want to delete?");
    if(cnf==true) {
        var tempPR = {};
        tempPR['staff_id'] = id;
    $.ajax(
            {
               url: '/prdtm/programme/deletestaffmain',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                editstaffdata();
               }
            });
    }
  }

  $('select').select2();

  // getProductFieldsByProductType();

  $(function()
  {
      editclodata();
      editstaffdata();
      edittopicdata();
  });

   showRejectField();

   function showRejectField()
    {
      var status = $("#status").val();

      // alert(status);

      if(status == '5')
      {
      // alert(status+ "per");
        $("#view_percentage").show();
        $("#view_reason").hide();
      }
      else if(status != '')
      {
      // alert(status);
        $("#view_percentage").hide();
        $("#view_reason").show();
      }
    }



    function addclomain()
  {
    var clo = $("#clo").val();
    var tempPR = {};
        tempPR['clo'] = clo;
        tempPR['id_programme'] = $("#id_programme").val();
        tempPR['id_clo'] = $("#id_clo").val();
    $.ajax(
            {
               url: '/prdtm/programme/addclomain',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#clo").val("");
                                $("#id_clo").val("0");

                editclodata();
               }
            });
  }

  function editclodata()
    {
          var idprogramme = $("#id_programme").val();
      
         $.ajax(
            {
               url: '/prdtm/programme/editclodata/'+idprogramme,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                 console.log(result);
                    $("#divclo").html(result);
               }
            });
       
    }



  function edittopicdata()
    {
          var idprogramme = $("#id_programme").val();
      
         $.ajax(
            {
               url: '/prdtm/programme/edittopicdata/'+idprogramme,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                 console.log(result);
                    $("#divtopic").html(result);
               }
            });
       
    }

     function addstaffmain()
  {
    var id_staff = $("#id_staff").val();
    var tempPR = {};
        tempPR['id_staff'] = id_staff;
        tempPR['id_programme'] = $("#id_programme").val();
    $.ajax(
            {
               url: '/prdtm/programme/addstaffmain',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                editstaffdata();
               }
            });
  }

  function editstaffdata()
    {
          var idprogramme = $("#id_programme").val();
      
         $.ajax(
            {
               url: '/prdtm/programme/editstaffdata/'+idprogramme,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                 console.log(result);
                    $("#divstaff").html(result);
               }
            });
       
    }


function addtopicmain()
  {
     var topic = $("#topic").val();
     var id_topic = $("#id_topic").val();
    var topic_overview = $("#topic_overview").val();
    // var topic_outcome = $("#topic_outcome").val();
   var topic_outcome =  CKEDITOR.instances['topic_outcome'].getData();

    var tempPR = {};
        tempPR['topic'] = topic;
        tempPR['topic_outcome'] = topic_outcome;
        tempPR['topic_overview'] = topic_overview;
        tempPR['id_topic'] = id_topic;
        
        tempPR['id_programme'] = $("#id_programme").val();
    $.ajax(
            {
               url: '/prdtm/programme/addtopicmain',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#topic").val("");
                $("#topic_overview").val("");
                CKEDITOR.instances['topic_outcome'].setData("");
                $("#id_topic").val("0");



                edittopicdata();
               }
            });
  }

   
   
   function showPartner(){
       var value = $("#internal_external").val();
       if(value=='Internal') {
            $("#partnerdropdown").hide();
   
       } else if(value=='External') {
            $("#partnerdropdown").show();
   
       }
   }


  function getProductFieldsByProductType()
  {
    var id_programme_type = $("#id_programme_type").val();
    // alert(id_programme_type);
    $.ajax(
        {
           url: '/prdtm/programme/getProductFieldsByProductType/'+id_programme_type,
           type: 'GET',
           error: function()
           {
            alert('Something is wrong');
           },
           success: function(result)
           {

              $("#view_fields").html(result);
              
              // window.location.reload();
           }
        });
  }



   function getProgrammeCodeDuplication()
   {
      var code = $("#code").val()

      if(code != '')
      {

        var tempPR = {};
        tempPR['code'] = code;
        tempPR['name'] = '';
        tempPR['id_programme'] = <?php echo $programmeDetails->id ?>;
        

        $.ajax(
        {
           url: '/prdtm/programme/getProgrammeDuplication',
            type: 'POST',
           data:
           {
            tempData: tempPR
           },
           error: function()
           {
            alert('Something is wrong');
           },
           success: function(result)
           {
              // alert(result);
              if(result == '0')
              {
                  alert('Duplicate Programme Code Not Allowed, Programme Already Registered With The Given Code : '+ code );
                  $("#code").val('');
              }
           }
        });
      }
   }


   function getProgrammeNameDuplication()
   {
      var name = $("#name").val()

      if(name != '')
      {

        var tempPR = {};
        tempPR['code'] = '';
        tempPR['name'] = name;
        tempPR['id_programme'] = <?php echo $programmeDetails->id ?>;
        

        $.ajax(
        {
           url: '/prdtm/programme/getProgrammeDuplication',
            type: 'POST',
           data:
           {
            tempData: tempPR
           },
           error: function()
           {
            alert('Something is wrong');
           },
           success: function(result)
           {
              // alert(result);
              if(result == '0')
              {
                  alert('Duplicate Programme Name Not Allowed, Programme Already Registered With The Given Name : '+ name );
                  $("#name").val('');
              }
           }
        });
      }
   }
   
  

   $(document).ready(function()
   {
        $("#form_programme").validate({
            rules: {
                name: {
                    required: true
                },
                code: {
                    required: true
                },
                internal_external: {
                    required: true
                },
                id_partner_university: {
                    required: true
                },
                id_category: {
                    required: true
                },
                id_category_setup: {
                    required: true
                },
                id_programme_type: {
                    required: true
                },
                max_duration: {
                    required: true
                },
                duration_type: {
                    required: true
                },
                trending: {
                    required: true
                },
                target_audience: {
                    required: true
                },
                student_learning_hours: {
                    required: true
                },
                short_description: {
                    required: true
                },
                id_delivery_mode: {
                    required: true
                },
                id_study_level: {
                    required: true
                },
                sold_separately: {
                    required: true
                },
                status: {
                    required: true
                },
                language: {
                    required: true
                },
                is_free_course: {
                    required: true
                },
                percentage: {
                    required: true
                },
                reason: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                code: {
                    required: "<p class='error-text'>Code Required</p>",
                },
                internal_external: {
                    required: "<p class='error-text'>Select Type</p>",
                },
                id_partner_university: {
                    required: "<p class='error-text'>Select Partner University</p>",
                },
                id_category: {
                    required: "<p class='error-text'>Select Product Type</p>",
                },
                id_category_setup: {
                    required: "<p class='error-text'>Select Category</p>",
                },
                id_programme_type: {
                    required: "<p class='error-text'>Select Category Type</p>",
                },
                max_duration: {
                    required: "<p class='error-text'>Max. Duration Required</p>",
                },
                duration_type: {
                    required: "<p class='error-text'>Select Duration Type</p>",
                },
                trending: {
                    required: "<p class='error-text'>Select Status Type</p>",
                },
                target_audience: {
                    required: "<p class='error-text'>Tagset Audience Reuired</p>",
                },
                student_learning_hours: {
                    required: "<p class='error-text'>Learning Hours Required</p>",
                },
                short_description: {
                    required: "<p class='error-text'>Short Description Required</p>",
                },
                id_delivery_mode: {
                    required: "<p class='error-text'>Select Delicery Mode</p>",
                },
                id_study_level: {
                    required: "<p class='error-text'>Select Competency Level</p>",
                },
                sold_separately: {
                    required: "<p class='error-text'>Select Sold Seperately</p>",
                },
                status: {
                    required: "<p class='error-text'>Select Status</p>",
                },
                language: {
                    required: "<p class='error-text'>Select Language</p>",
                },
                is_free_course: {
                    required: "<p class='error-text'>Select Is Free Course</p>",
                },
                percentage: {
                    required: "<p class='error-text'>Percentage Required</p>",
                },
                reason: {
                    required: "<p class='error-text'>Reason Reason Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
   
   
   
</script>

<script src="<?php echo BASE_PATH; ?>assets/ckeditor/ckeditor.js"></script>

