<?php
$programme_model = new Programme_Model();

$urlarray = explode ('/',$_SERVER['REQUEST_URI']);

$urlmodule = $urlarray['1'];
$urlcontroller = $urlarray['2'];
$urlaction = $urlarray['3'];

$id_product_type = $programmeDetails->id_programme_type;

$programme_tabs  = $programme_model->getProductTabsByProductId($id_product_type);

?>
<div class="container-fluid page-wrapper">
   <div class="main-container clearfix">
       <ul class="page-nav-links">
             <li ><a href="/prdtm/programme/edit/<?php echo $id_programme;?>">Course Information</a></li>

 <li><a href="/prdtm/programme/faculty/<?php echo $id_programme;?>">Course Instructor</a></li>


            <li><a href="/prdtm/programme/newassessment/<?php echo $id_programme;?>">Course Assessment</a></li>

           

            <li class="active"><a href="/prdtm/programme/syllabus/<?php echo $id_programme;?>">Course Content</a></li>
            <li><a href="/prdtm/programme/fee/<?php echo $id_programme;?>">Course Fees</a></li>
            <li><a href="/prdtm/programme/award/<?php echo $id_programme;?>">Course Certificate</a></li>
            
                    <li><a href="/prdtm/programme/skill/<?php echo $id_programme;?>">Other Settings</a></li>

            <li><a href="/prdtm/programme/aim/<?php echo $id_programme;?>">Status of Course Details</a></li>
        </ul>

      <form id="form_programme" action="" method="post">
         <div class="form-container">
            <h4 class="form-group-title">Program Learning Outcomes Details</h4>
            <div class="row">
              
               <div class="col-sm-4">
                  <div class="form-group">
                     <label>Learning Outcome <span class='error-text'>*</span></label>
                     <input type="text" class="form-control" id="learning_objective" name="learning_objective" required value="<?php echo $programmeSyllabus->learning_objective; ?>">
                  </div>
               </div>

            </div>

            <div class="button-block clearfix">
                  <div class="form-group">

                      <button type="button" onclick="getProgrammeSyllabusDuplication()" class="btn btn-primary btn-lg" value="Objective" name="save">Save</button>
                      <?php
                    if($id_syllabus != NULL)
                    {
                      ?>
                      <a href="<?php echo '../../syllabus/'. $id_programme ?>" class="btn btn-link">Cancel</a>
                      <?php
                    }
                    ?>
                   </div>
                 </div>

                  <div class="custom-table">
                      <table class="table">
                        <thead>
                          <tr>
                            <th>Sl. No</th>
                            <th>Learning Outcome</th>
                            <th class="text-center">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                          
                          $i=1;
                            foreach ($syllabus as $record) {
                          ?>
                              <tr>
                                <td><?php echo $i ?></td>
                                <td><?php echo ucfirst($record->learning_objective) ?></td>
                                </td>
                                <td class="text-center">
                                  <a href='/prdtm/programme/syllabus/<?php echo $id_programme;?>/<?php echo $record->id;?>'>Edit</a> | 
                                  <a href="javascript:deleteSyllabusData(<?php echo $record->id; ?>)">Delete</a>
                                </td>
                              </tr>
                          <?php
                          $i++;
                            }
                          
                          ?>
                        </tbody>
                      </table>
                     </div>

         </div>

          <div class="form-container">
            <h4 class="form-group-title">Topic Details</h4>
            <div class="row">
              
               <div class="col-sm-4">
                  <div class="form-group">
                     <label>Topic Name <span class='error-text'>*</span></label>
                     <input type="text" class="form-control" id="topic" name="topic" value="<?php echo $topicDetails[0]->topic;?>" required>
                  </div>
               </div>

             </div>
              <div class="custom-table">
        <table class="table" id="list-table">
          <thead>
            <tr>
              <th>Sl. No</th>
              <th>Topic</th>
              <th class="text-center">Action</th>
            </tr>
          </thead>
          <tbody>
            <?php
            
            $i=1;
              foreach ($topic as $record) {

              $this->load->model('programme_model');

              $namesArray = array();

             
            ?>
                <tr>
                  <td><?php echo $i ?></td>
                  <td><?php echo ucfirst($record->topic) ?></td>
                  </td>
                  <td>
                    <a href='/prdtm/programme/topic/<?php echo $id_programme;?>/<?php echo $record->id;?>'>Edit</a>|
                    <a onclick="deleteTopicData(<?php echo $record->id; ?>)">Delete</a>
                  </td>
                </tr>
            <?php
            $i++;
              }
            
            ?>
          </tbody>
        </table>
       </div>
           
         </div>

          <div class="form-container">
            <h4 class="form-group-title">Program Description</h4>

             <div class="row">
              <div class="col-sm-12">
                  <div class="form-group">
                     <textarea type="text" class="form-control" id="overview" name="overview"><?php echo $overview[0]->overview; ?></textarea>
                  </div>
               </div>
                </div>
                <div class="row">
               
               <div class="col-sm-2 pt-10">
                <div class="form-group">

                    <button type="submit" class="btn btn-primary btn-lg" value="Objective" name="save">Save</button>
                 </div>
               </div>
            </div>
         </div>


     
      </form>
      


   </div>
</div>
<footer class="footer-wrapper">
   <p>&copy; 2019 All rights, reserved</p>
</footer>

<script>
  
  $('select').select2();

  function getProgrammeSyllabusDuplication()
  {
    if($('#form_programme').valid())
    {
      var learning_objective = $("#learning_objective").val();

      if(learning_objective != '')
      {
        var tempPR = {};
        tempPR['learning_objective'] = learning_objective;
        tempPR['id_programme'] = "<?php echo $id_programme; ?>";
        tempPR['id_syllabus'] = "<?php echo $id_syllabus; ?>";
        

        $.ajax(
        {
           url: '/prdtm/programme/getProgrammeSyllabusDuplication',
            type: 'POST',
           data:
           {
            tempData: tempPR
           },
           error: function()
           {
            alert('Something is wrong');
           },
           success: function(result)
           {
              // alert(result);
              if(result == '0')
              {
                  alert('Duplicate Programme Learning Objective Not Allowed: '+ learning_objective );
                  $("#learning_objective").val('');
              }
              else
              if(result == '1')
              {
                $("#form_programme").submit();
              }
           }
        });
      }
    }
  }


  
  function deleteSyllabusData(id)
  {

    var cnf= confirm('Do you really want to delete?');
    if(cnf==true)
    {
    
    $.ajax(
        {
           url: '/prdtm/programme/deleteSyllabusData/'+id,
           type: 'GET',
           error: function()
           {
            alert('Something is wrong');
           },
           success: function(result)
           {
              window.location.reload();
           }
        });
    }
  }



</script>

<script src="<?php echo BASE_PATH; ?>assets/ckeditor/ckeditor.js"></script>


<script type="text/javascript">



CKEDITOR.replace('overview',{
  width: "100%",
  height: "300px"

}); 

</script>