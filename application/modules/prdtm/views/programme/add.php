<?php
$programme_model = new Programme_Model();

$urlarray = explode ('/',$_SERVER['REQUEST_URI']);

$urlmodule = $urlarray['1'];
$urlcontroller = $urlarray['2'];
$urlaction = $urlarray['3'];

$id_product_type = $programmeDetails->id_programme_type;

$programme_tabs  = $programme_model->getProductTabsByProductId($id_product_type);

?>

<div class="container-fluid page-wrapper">
   <div class="main-container clearfix">
      <form id="form_programme" action="" method="post" enctype="multipart/form-data">

        <div class="form-container">
            <h4 class="form-group-title">Product Details</h4>
            
            <div class="row">
               <div class="col-sm-12">
               <div class="custom-table">
                 <table class="table table-striped table-bordered">
                   <tbody>
                     <tr>
                       <td width="300" class="text-right">Course Provider</td>
                       <td>
                         <div class="row">
                           <div class="col-sm-6">
                             <div class="form-group">
                               <select name="id_partner_university" id="id_partner_university" class="form-control">
                                  <option value="">Select</option>
                                  <?php
                                     if (!empty($partnerList))
                                     {
                                         foreach ($partnerList as $record)
                                         { ?>
                                  <option value="<?php echo $record->id; ?>"
                                     <?php
                                        if ($record->id == $programmeDetails->id_partner_university)
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                     <?php echo $record->code . " - " . $record->name; ?>
                                  </option>
                                  <?php
                                     }
                                     }
                                     ?>
                               </select>
                                       </div>
                                     </div>
                                   </div>
                                 </td>
                               </tr>

                    

                       

                      <tr>
                       <td width="300" class="text-right">Course Type</td>
                       <td>
                         <div class="row">
                           <div class="col-sm-6">
                             <div class="form-group">
                               <select name="id_programme_type" id="id_programme_type" class="form-control">
                        <option value="">Select</option>
                        <?php
                           if (!empty($productTypeSetupList))
                           {
                               foreach ($productTypeSetupList as $record)
                               { ?>
                        <option value="<?php echo $record->id; ?>"
                           <?php
                              if ($programmeDetails->id_programme_type == $record->id)
                              {
                                  echo 'selected';
                              }
                              ?>
                           ><?php echo $record->name; ?>
                        </option>
                        <?php
                           }
                           }
                           ?>
                     </select>
                             </div>
                           </div>
                         </div>
                       </td>
                     </tr>


                       <tr>
                       <td width="300" class="text-right">Accredited By</td>
                       <td>
                         <div class="row">
                           <div class="col-sm-6">
                             <div class="form-group">
                               <select name="course_sub_type" id="course_sub_type" class="form-control">
                        <option value="">Select</option>
                        <option value="MQA">MQA</option>
                        <option value="HRDF">HRDF</option>
                       
                     </select>
                             </div>
                           </div>
                         </div>
                       </td>
                     </tr>



                      <tr>
                       <td width="300" class="text-right">Course Category </td>
                       <td>
                         <div class="row">
                           <div class="col-sm-6">
                             <div class="form-group">
                               <select name="id_category" id="id_category" class="form-control">
                                  <option value="">Select</option>
                                  <?php
                                     if (!empty($categoryList))
                                     {
                                         foreach ($categoryList as $record)
                                         { ?>
                                  <option value="<?php echo $record->id; ?>"
                                     <?php
                                        if ($programmeDetails->id_category == $record->id)
                                        {
                                            echo 'selected';
                                        }
                                        ?>
                                     ><?php echo $record->name; ?>
                                  </option>
                                  <?php
                                     }
                                     }
                                     ?>
                               </select>
                             </div>
                           </div>
                         </div>
                       </td>
                     </tr>

                       <tr>
                       <td width="300" class="text-right">Course Title </td>
                       <td>
                         <div class="row">
                           <div class="col-sm-6">
                             <div class="form-group">
                               <input type="text" type="text" class="form-control" id="name" name="name" value="">
                             </div>
                           </div>
                         </div>
                       </td>
                     </tr>

                     <tr>
                       <td width="300" class="text-right">Course Code </td>
                       <td>
                         <div class="row">
                           <div class="col-sm-6">
                             <div class="form-group">
                               <input type="text" type="text" class="form-control" id="code" name="code" value="" >
                             </div>
                           </div>
                         </div>
                       </td>
                     </tr>

                       <tr>
                       <td width="300" class="text-right">Language of Delivery</td>
                       <td>
                         <div class="row">
                           <div class="col-sm-6">
                             <div class="form-group">
                               <select name="language_of_delivery" id="language_of_delivery" class="form-control">
                        <option value="">Select</option>
                        <option value="English">English</option>
                        <option value="Bahasa Melayu">Bahasa Melayu</option>
                       
                     </select>
                             </div>
                           </div>
                         </div>
                       </td>
                     </tr>


                       <tr>
                       <td width="300" class="text-right">Course Image </td>
                       <td>
                         <div class="row">
                           <div class="col-sm-6">
                                         <input type="file" name="image" id="image">

                           </div>
                         </div>
                       </td>
                     </tr>



                     <tr>
                       <td width="300" class="text-right">Duration </td>
                       <td>
                         <div class="row">
                           <div class="col-sm-2">
                              <input type="number" class="form-control" id="max_duration" name="max_duration" min='1' value="">
                           </div>
                           <div class="col-sm-4">
                             <div class="form-group">
                                                       <select name="duration_type" id="duration_type" class="form-control" required>
                           <option value="">Select</option>
                           <option value="Hours">Hours</option>
                           <option value="Days">Days</option>
                           <option value="Weeks">Weeks</option>
                           <option value="Months">Months</option>
                           <option value="Years">Years</option>
                              </select>
                             </div>
                           </div>
                         </div>
                       </td>
                     </tr>



                       <tr>
                       <td width="300" class="text-right">Course Fee (RM)</td>
                       <td>
                         <div class="row">
                           <div class="col-sm-6">
                             <div class="form-group">
                               <input type="text" type="text" class="form-control" id="course_fee" name="course_fee" value="" >
                             </div>
                           </div>
                         </div>
                       </td>
                     </tr>



                     <tr>
                       <td width="300" class="text-right">Course Credit </td>
                       <td>
                         <div class="row">
                           <div class="col-sm-6">
                             <div class="form-group">
                               <input type="text" type="text" class="form-control" id="credit" name="credit" value="" >
                             </div>
                           </div>
                         </div>
                       </td>
                     </tr>


                       <tr>
                       <td width="300" class="text-right">Pre-requisite</td>
                       <td>
                         <div class="row">
                           <div class="col-sm-6">
                             <div class="form-group">
                            <input type="text" type="text" class="form-control" id="pre_requisite" name="pre_requisite" value="" >
                          </div>
                        </div>
                         </div>
                       </td>
                     </tr>



                     <tr>
                       <td width="300" class="text-right">Competency Level </td>
                       <td>
                         <div class="row">
                           <div class="col-sm-6">
                             <div class="form-group">
                               <select name="id_study_level" id="id_study_level" class="form-control">
                                 <option value="">Select</option>
                                 <?php
                                    if (!empty($studyLevelList))
                                    {
                                        foreach ($studyLevelList as $record)
                                        { ?>
                                 <option value="<?php echo $record->id; ?>"
                                    ><?php echo $record->name; ?>
                                 </option>
                                 <?php
                                    }
                                    }
                                    ?>
                              </select>
                             </div>
                           </div>
                         </div>
                       </td>
                     </tr>

                       <tr>
                       <td width="300" class="text-right">Education Level </td>
                       <td>
                         <div class="row">
                           <div class="col-sm-6">
                             <div class="form-group">
                               <select name="id_education_level" id="id_education_level" class="form-control">
                                 <option value="">Select</option>
                                 <?php
                                    if (!empty($educationLevelList))
                                    {
                                        foreach ($educationLevelList as $record)
                                        { ?>
                                 <option value="<?php echo $record->id; ?>"
                                    <?php
                                       if ($programmeDetails->id_education_level == $record->id)
                                       {
                                           echo 'selected';
                                       }
                                       ?>
                                    ><?php echo $record->name; ?>
                                 </option>
                                 <?php
                                    }
                                    }
                                    ?>
                              </select>
                             </div>
                           </div>
                         </div>
                       </td>
                     </tr>

                      <tr>
                       <td width="300" class="text-right">Delivery Modality </td>
                       <td>
                         <div class="row">
                           <div class="col-sm-6">
                             <div class="form-group">
                               <select name="id_delivery_mode" id="id_delivery_mode" class="form-control">
                                 <option value="">Select</option>
                                 <?php
                                    if (!empty($deliveryModeList))
                                    {
                                        foreach ($deliveryModeList as $record)
                                        { ?>
                                 <option value="<?php echo $record->id; ?>" ><?php echo $record->name; ?>
                                 </option>
                                 <?php
                                    }
                                    }
                                    ?>
                              </select>
                             </div>
                           </div>
                         </div>
                       </td>
                     </tr>
                      <tr>
                       <td width="300" class="text-right">Learning Mode </td>
                       <td>
                         <div class="row">
                           <div class="col-sm-6">
                             <div class="form-group">
                               <select name="id_learning_mode" id="id_learning_mode" class="form-control">
                                 <option value="">Select</option>
                                 <?php
                                    if (!empty($learningModeList))
                                    {
                                        foreach ($learningModeList as $record)
                                        { ?>
                                 <option value="<?php echo $record->id; ?>" ><?php echo $record->name; ?>
                                 </option>
                                 <?php
                                    }
                                    }
                                    ?>
                              </select>
                             </div>
                           </div>
                         </div>
                       </td>
                     </tr>

 <tr>
                       <td width="300" class="text-right">Entry Qualification  </td>
                       <td>
                         <div class="row">
                           <div class="col-sm-12">
                             <div class="form-group">
                                 <textarea class="form-control z-depth-1" rows="3" placeholder="Short Descripton..." name="entry_qualification" id="entry_qualification"></textarea>
                             </div>
                           </div>
                         </div>
                       </td>
                     </tr>



                      <tr>
                       <td width="300" class="text-right">Facilitator</td>
                       <td>
                         <div class="row">
                           <div class="col-sm-6">
                             <div class="form-group">
                               <select name="id_staff" id="id_staff" class="form-control">
                                  <option value="">Select</option>
                                  <?php
                                     if (!empty($staffList))
                                     {
                                         foreach ($staffList as $record)
                                         { ?>
                                  <option value="<?php echo $record->id; ?>"
                                     <?php
                                        if ($programmeDetails->id_staff == $record->id)
                                        {
                                            echo 'selected';
                                        }
                                        ?>
                                     ><?php echo $record->name; ?>
                                  </option>
                                  <?php
                                     }
                                     }
                                     ?>
                               </select>


                             </div>
                           </div>
                           <div class="col-sm-6">
                               <button type="button" onclick="addstaff()" class="btn btn-primary"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>                               
                           </div>
                         </div>
                       </td>
                     </tr>

                      <tr>
                       <td width="300" class="text-right">Staff List </td>
                       <td>
                         <div id="divstaff"></div>
                        </td>
                     </tr>

                      <tr>
                       <td width="300" class="text-right">Course Synopsis </td>
                       <td>
                         <div class="row">
                           <div class="col-sm-12">
                             <div class="form-group">
                                 <textarea class="form-control z-depth-1" rows="3" placeholder="Short Descripton..." name="synopsys" id="synopsys"></textarea>
                             </div>
                           </div>
                         </div>
                       </td>
                     </tr>

                      <tr style="display: none;">
                       <td width="300" class="text-right">How it works </td>
                       <td>
                         <div class="row">
                           <div class="col-sm-12">
                             <div class="form-group">
                                 <textarea class="form-control z-depth-1" rows="3" placeholder="Short Descripton..." name="works" id="works"></textarea>
                             </div>
                           </div>
                         </div>
                       </td>
                     </tr>

                      <tr style="display: none;">
                       <td width="300" class="text-right">How to pass </td>
                       <td>
                         <div class="row">
                           <div class="col-sm-12">
                             <div class="form-group">
                                 <textarea class="form-control z-depth-1" rows="3" placeholder="Short Descripton..." name="pass" id="pass"></textarea>
                             </div>
                           </div>
                         </div>
                       </td>
                     </tr>

                     <tr style="display: none;">
                       <td width="300" class="text-right">Assessment Type </td>
                       <td>
                         <div class="row">
                           <div class="col-sm-6">
                             <div class="form-group">
                              <select name="assessment_type" id="assessment_type" class="form-control" required>
                           <option value="">Select</option>
                           <option value="Self-Reflection">Self-Reflection</option>
                           <option value="Self-Assessment">Self-Assessment</option>
                           
                              </select>
                             </div>
                           </div>
                         </div>
                       </td>
                     </tr>
                     <tr style="display: none;">
                       <td width="300" class="text-right">Assesment Description </td>
                       <td>
                         <div class="row">
                           <div class="col-sm-12">
                             <div class="form-group">
                                 <textarea class="form-control z-depth-1" rows="3" placeholder="Short Descripton..." name="assessment_details" id="assessment_details"></textarea>
                             </div>
                           </div>
                         </div>
                       </td>
                     </tr>
                      <tr>
                       <td width="300" class="text-right">CLO </td>
                       <td>
                         <div class="row">
                           <div class="col-sm-6">
                              <input type="text" class="form-control" id="clo" name="clo" >
                           </div>
                           <div class="col-sm-6">
                               <button type="button" onclick="addclo()" class="btn btn-primary"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
                           </div>
                          </div>
                        </td>
                     </tr>
                     <tr>
                       <td width="300" class="text-right">CLO List </td>
                       <td>
                         <div id="divclo"></div>
                        </td>
                     </tr>
                     <tr>
                       <td width="300" class="text-right">Topic </td>
                       <td>
                         <div class="row">

                             <div class="col-sm-11">
                                 <div class="row" style="padding-bottom: 9px;">
                                   <div class="col-sm-2" style="text-align: right;">
                                    <label>Name</label>
                                  </div>
                                  <div class="col-sm-10">
                                      <input type="text" class="form-control" id="topic" name="topic" >
                                   </div>
                                 </div>
                                    <div class="row" style="padding-bottom: 9px;">
                                   <div class="col-sm-2" style="text-align: right;">
                                    <label>Overview</label>
                                  </div>
                                  <div class="col-sm-10">

                                      <input type="text" class="form-control" id="topic_overview" name="topic_overview" >
                                   </div>
                                 </div>
                                     <div class="row" style="padding-bottom: 9px;">
                                   <div class="col-sm-2" style="text-align: right;">
                                    <label>Outcome</label>
                                  </div>
                                  <div class="col-sm-10">
                                      <input type="text" class="form-control" id="topic_outcome" name="topic_outcome" >
                                   </div>
                                 </div>
                                <!--  <div class="row">
                                   <div class="col-sm-10">
                                       <textarea class="form-control z-depth-1" rows="3" placeholder="Short Descripton..." 
                                       name="topic_outcome" id="topic_outcome"></textarea>
                                   </div>
                                  </div>    -->                              
                             </div>
                             <div class="col-sm-1">
                               <button type="button" onclick="addtopic()" class="btn btn-primary"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>                                   
                             </div>
                         </div>  

                        </td>
                     </tr>
                     <tr>
                       <td width="300" class="text-right">Topic List </td>
                       <td>
                         <div id="divtopic"></div>
                        </td>
                     </tr>

                      <tr>
                       <td width="300" class="text-right">Overall Assessment Information </td>
                       <td>
                           <div class="form-horizontal">
                             <?php for($i=0;$i<count($assessmentList);$i++) { ?>
                             
                              <div class="form-group">
                                <label for="inputEmail3" class="col-sm-5 control-label"><?php echo $assessmentList[$i]->name;?></label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="overallassissment[]" name="overallassissment[<?php echo $assessmentList[$i]->id;?>]" >
    
                                     <input type="hidden" class="form-control" id="overallassissmentids[]" name="overallassissmentids[]" value="<?php echo $assessmentList[$i]->id;?>">
                                </div>
                              </div>                             
                           <?php } ?>                                
                           </div>
                       </td>
                     </tr>
                     <tr>
                       <td width="300" class="text-right">CERTIFICATE </td>
                       <td>
                         <?php for($i=0;$i<count($certificateList);$i++) { ?>
                         <div class="row">
                           <div class="col-sm-12">
                            <label class="checkbox-inline">
                              <input type="checkbox" name='overallcertificate[<?php echo $certificateList[$i]->id;?>]' value="<?php echo $certificateList[$i]->id;?>">
                              <span class="check-radio"></span><?php echo $certificateList[$i]->name;?>
<input type="hidden" class="form-control" id="overallcertificateids[]" name="overallcertificateids[]" value="<?php echo $certificateList[$i]->id;?>">                              
                            </label>                               

                           
                           </div>
                         </div>
                       <?php } ?> 
                       </td>
                     </tr>
                     <tr>
                                              <td width="300" class="text-right">Status</td>

                      <td><div class="row">
                           <div class="col-sm-6">
                             <div class="form-group"><select name="status" id="status" class="form-control">
              <option value="">Select </option>
              <option value="Pending Modification">Pending Modification</option>
              <option value="Pending Approval">Pending Approval</option>
              <option value="Approved Pending Content Update">Approved Pending Content Update</option>
              <option value="Approved and Published">Approved and Published</option>
            </select></div></div></div></td>
                    </tr>
                  </tbody>
               </table>

            </div>
               
            </div>
         </div>


               




      <div class="button-block clearfix">
         <div class="bttn-group">
            <button type="submit" class="btn btn-primary btn-lg" >Save</button>
            <a href="../list" class="btn btn-link">Back</a>
         </div>
      </div>


      <div id="view_fields" style="display: none;">
      </div>
   
   </div>


         </form>

</div>
<footer class="footer-wrapper">
   <p>&copy; 2019 All rights, reserved</p>
</footer>
</div>
</div>


<script src="<?php echo BASE_PATH; ?>assets/ckeditor/ckeditor.js"></script>

<script type="text/javascript">




CKEDITOR.replace('entry_qualification',{
  width: "100%",
  height: "100px"

}); 

CKEDITOR.replace('synopsys',{
  width: "100%",
  height: "100px"

}); 
CKEDITOR.replace('works',{
  width: "100%",
  height: "100px"

}); 

CKEDITOR.replace('assessment_details',{
  width: "100%",
  height: "100px"

}); 


CKEDITOR.replace('pass',{
  width: "100%",
  height: "100px"

}); 


// CKEDITOR.replace('topic_outcome',{
//   width: "100%",
//   height: "100px"

// }); 






</script>

<style type="text/css">
   .shadow-textarea textarea.form-control::placeholder {
   font-weight: 300;
   }
   .shadow-textarea textarea.form-control {
   padding-left: 0.8rem;
   }
</style>
<script>

  $('select').select2();

  // getProductFieldsByProductType();



function addstaff()
  {
    var id_staff = $("#id_staff").val();
    var tempPR = {};
        tempPR['id_staff'] = id_staff;
    $.ajax(
            {
               url: '/prdtm/programme/addstaff',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                stafflist();
               }
            });
  }

  function stafflist()
    {
      
         $.ajax(
            {
               url: '/prdtm/programme/stafftext',
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                 console.log(result);
                    $("#divstaff").html(result);
               }
            });
       
    }

  function addclo()
  {
    var clo = $("#clo").val();
    var tempPR = {};
        tempPR['clo'] = clo;
    $.ajax(
            {
               url: '/prdtm/programme/addclo',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#clo").val("");
                clolist();
               }
            });
  }

  function clolist()
    {
      
         $.ajax(
            {
               url: '/prdtm/programme/clotext',
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                 console.log(result);
                    $("#divclo").html(result);
               }
            });
       
    }


   function addtopic()
  {
    var topic = $("#topic").val();
    var topic_overview = $("#topic_overview").val();
    // var topic_outcome =  CKEDITOR.instances['topic_outcome'].getData();
   var topic_outcome =  $("#topic_outcome").val();
    var tempPR = {};
        tempPR['topic'] = topic;
        tempPR['topic_outcome'] = topic_outcome;
        tempPR['topic_overview'] = topic_overview;
    $.ajax(
            {
               url: '/prdtm/programme/addtopic',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#topic").val("");
                $("#topic_overview").val("");
                $("#topic_outcome").val("");
                // CKEDITOR.instances['topic_outcome'].setData('');
                topiclist();
               }
            });
  }

  function topiclist()
    {
      
         $.ajax(
            {
               url: '/prdtm/programme/topictext',
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                 console.log(result);
                    $("#divtopic").html(result);
               }
            });
       
    }


    function deletecloData(id) {
    var cnf = confirm("Do you really want to delete?");
    if(cnf==true) {
        var tempPR = {};
        tempPR['clo_id'] = id;
    $.ajax(
            {
               url: '/prdtm/programme/deleteclo',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                clolist();
               }
            });
    }
  }


  function deletetopicData(id) {
    var cnf = confirm("Do you really want to delete?");
    if(cnf==true) {
        var tempPR = {};
        tempPR['topic_id'] = id;
    $.ajax(
            {
               url: '/prdtm/programme/deletetopic',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                topiclist();
               }
            });
    }
  }

   
  

  function deletestaffData(id) {
    var cnf = confirm("Do you really want to delete?");
    if(cnf==true) {
        var tempPR = {};
        tempPR['staff_id'] = id;
    $.ajax(
            {
               url: '/prdtm/programme/deletestaff',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                stafflist();
               }
            });
    }
  }

  

   $(document).ready(function()
   {
        $("#form_programme").validate({
            rules: {
                name: {
                    required: true
                },
                code: {
                    required: true
                },
                internal_external: {
                    required: true
                },
                id_partner_university: {
                    required: true
                },
                id_category: {
                    required: true
                },
                id_category_setup: {
                    required: true
                },
                id_programme_type: {
                    required: true
                },
                max_duration: {
                    required: true
                },
                duration_type: {
                    required: true
                },
                trending: {
                    required: true
                },
                target_audience: {
                    required: true
                },
                student_learning_hours: {
                    required: true
                },
                short_description: {
                    required: true
                },
                id_delivery_mode: {
                    required: true
                },
                id_study_level: {
                    required: true
                },
                sold_separately: {
                    required: true
                },
                status: {
                    required: true
                },
                language: {
                    required: true
                },
                is_free_course: {
                    required: true
                },
                percentage: {
                    required: true
                },
                reason: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                code: {
                    required: "<p class='error-text'>Code Required</p>",
                },
                internal_external: {
                    required: "<p class='error-text'>Select Type</p>",
                },
                id_partner_university: {
                    required: "<p class='error-text'>Select Partner University</p>",
                },
                id_category: {
                    required: "<p class='error-text'>Select Product Type</p>",
                },
                id_category_setup: {
                    required: "<p class='error-text'>Select Category</p>",
                },
                id_programme_type: {
                    required: "<p class='error-text'>Select Category Type</p>",
                },
                max_duration: {
                    required: "<p class='error-text'>Max. Duration Required</p>",
                },
                duration_type: {
                    required: "<p class='error-text'>Select Duration Type</p>",
                },
                trending: {
                    required: "<p class='error-text'>Select Status Type</p>",
                },
                target_audience: {
                    required: "<p class='error-text'>Tagset Audience Reuired</p>",
                },
                student_learning_hours: {
                    required: "<p class='error-text'>Learning Hours Required</p>",
                },
                short_description: {
                    required: "<p class='error-text'>Short Description Required</p>",
                },
                id_delivery_mode: {
                    required: "<p class='error-text'>Select Delicery Mode</p>",
                },
                id_study_level: {
                    required: "<p class='error-text'>Select Competency Level</p>",
                },
                sold_separately: {
                    required: "<p class='error-text'>Select Sold Seperately</p>",
                },
                status: {
                    required: "<p class='error-text'>Select Status</p>",
                },
                language: {
                    required: "<p class='error-text'>Select Language</p>",
                },
                is_free_course: {
                    required: "<p class='error-text'>Select Is Free Course</p>",
                },
                percentage: {
                    required: "<p class='error-text'>Percentage Required</p>",
                },
                reason: {
                    required: "<p class='error-text'>Reason Reason Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
   
   
   
</script>

<script src="<?php echo BASE_PATH; ?>assets/ckeditor/ckeditor.js"></script>
