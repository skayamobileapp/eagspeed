<?php
$programme_model = new Programme_Model();

$urlarray = explode ('/',$_SERVER['REQUEST_URI']);

$urlmodule = $urlarray['1'];
$urlcontroller = $urlarray['2'];
$urlaction = $urlarray['3'];

$id_product_type = $programmeDetails->id_programme_type;

$programme_tabs  = $programme_model->getProductTabsByProductId($id_product_type);

// echo "<Pre>";print_r($id_product_type);exit();
?>
<div class="container-fluid page-wrapper">
   <div class="main-container clearfix">
       <ul class="page-nav-links">
            

            <li><a href="/prdtm/programme/edit/<?php echo $id_programme;?>">Product Details</a></li>

            <?php
            foreach ($programme_tabs as $individual_tab)
            {
              $tab = $individual_tab->tab;
              $title = $individual_tab->title;
            ?>
              <li 
              <?php
              if($tab == $urlaction)
              {
                echo 'class="active"';
              }
              ?>
              ><a href="/prdtm/programme/<?php echo $tab;?>/<?php echo $id_programme;?>"><?php echo $title ?></a></li>
            <?php
            }
            ?>

            <li><a href="/prdtm/programme/fee/<?php echo $id_programme;?>">Fee Structure</a></li>
            <li class="active"><a href="/prdtm/programme/markDistribution/<?php echo $id_programme;?>">Mark Distribution</a></li>
            <li><a href="/prdtm/programme/approval/<?php echo $id_programme;?>">Approval Details</a></li>

            
        </ul>

        <div class="page-title clearfix">
            <h3>Add Marks Distribution</h3>
            <!-- <?php
            if($id_marks_distribution == NULL)
            {
              ?>
                <a href="../list" class="btn btn-link"> < Back</a>
              <?php
            }
            ?> -->

        </div>

        


      <div class="form-container">
          <h4 class="form-group-title">Programme Details</h4>

              <div class='data-list'>
                  <div class='row'>
  
                      <div class='col-sm-6'>
                          <dl>
                              <dt>Programme Name :</dt>
                              <dd><?php echo ucwords($programme->name); ?></dd>
                          </dl>
                          <!-- <dl>
                              <dt>Optional Name :</dt>
                              <dd><?php echo $programme->name_optional_language ?></dd>
                          </dl> -->                          
                          <dl>
                              <dt>Max. Duration :</dt>
                              <dd><?php echo $programme->max_duration . " - " . $programme->duration_type; ?></dd>
                          </dl>
                      </div>        
                      
                      <div class='col-sm-6'>
                          <dl>
                              <dt>Programme Code :</dt>
                              <dd><?php echo $programme->code ?></dd>
                          </dl>         
                          <dl>
                              <dt>Type :</dt>
                              <dd><?php echo $programme->internal_external; ?></dd>
                          </dl>
                          <!-- <dl>
                              <dt>Trending In </dt>
                              <dd><?php echo $programme->trending; ?></dd>
                          </dl> -->
                          
                      </div>
  
                  </div>
              </div>


       </div>





    <form id="form_main" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Marks Distribution Details</h4>


            <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Exam Conponents <span class='error-text'>*</span></label>
                            <select name="id_exam_component" id="id_exam_component" class="form-control selitemIcon">
                                <option value="">Select</option>
                                <?php
                                if (!empty($examComponentsList))
                                {
                                    foreach ($examComponentsList as $record)
                                    {?>
                                <option value="<?php echo $record->id;  ?>"
                                    <?php
                                    if($record->id == $markDistribution->id_exam_component)
                                    {
                                        echo 'selected';
                                    } 
                                    ?>>
                                    <?php echo $record->name;?>
                                </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>


                    <div class="col-sm-4">
                      <div class="form-group">
                         <label>Max. Marks <span class='error-text'>*</span></label>
                         <input type="number" class="form-control" id="max" name="max" max="100" value="<?php echo $markDistribution->max; ?>">
                      </div>
                    </div>


                    <div class="col-sm-4">
                      <div class="form-group">
                         <label>Pass Marks <span class='error-text'>*</span></label>
                         <input type="number" class="form-control" id="pass_marks" name="pass_marks" min="1" value="<?php echo $markDistribution->pass_marks; ?>">
                      </div>
                    </div>
                    


                </div>


                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <p>Attendance Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="is_attendance" id="is_attendance" value="1" <?php if($markDistribution->is_attendance=='1') {
                                 echo "checked=checked";
                              };?>><span class="check-radio"></span> Yes
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="is_attendance" id="is_attendance" value="0" <?php if($markDistribution->is_attendance=='0') {
                                 echo "checked=checked";
                              };?>>
                              <span class="check-radio"></span> No
                            </label>                              
                        </div>                         
                    </div>



                    <div class="col-sm-4">
                        <div class="form-group">
                            <p>Is Pass Compulsary <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="is_pass" id="is_pass" value="1" <?php if($markDistribution->is_pass=='1') {
                                 echo "checked=checked";
                              };?>><span class="check-radio"></span> Yes
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="is_pass" id="is_pass" value="0" <?php if($markDistribution->is_pass=='0') {
                                 echo "checked=checked";
                              };?>>
                              <span class="check-radio"></span> No
                            </label>                              
                        </div>                         
                    </div>



                </div>


                    


        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="button" class="btn btn-primary btn-lg" onclick="getProgrammeMarksDistributionDuplication()">Save</button>
                <?php
                if($id_marks_distribution != NULL)
                {
                  ?>
                  <a href="<?php echo '../'. $id_programme ?>" class="btn btn-link">Cancel</a>
                  <?php
                }
                ?>
            </div>
        </div>


    </form>



      <?php

      if(!empty($markDistributionByProgramme))
      {
      ?>
          <br>

          <div class="form-container">
                  <h4 class="form-group-title">Marks Distribution Details</h4>

              

                <div class="custom-table">
                  <table class="table">
                      <thead>
                          <tr>
                          <th>Sl. No</th>
                           <th>Exam Components</th>
                           <th>Max. Marks</th>
                           <th>Pass Marks</th>
                           <th>Is Attendance</th>
                           <th>Is Pass Compulsary</th>
                           <th style="text-align: center;">Action</th>
                          </tr>
                      </thead>
                      <tbody>
                           <?php
                       $total = 0;
                        for($i=0;$i<count($markDistributionByProgramme);$i++)
                       { ?>
                          <tr>
                          <td><?php echo $i+1;?></td>
                          <td><?php echo $markDistributionByProgramme[$i]->exam_component ;?></td>
                          <td><?php echo $markDistributionByProgramme[$i]->max ;?></td>
                          <td><?php echo $markDistributionByProgramme[$i]->pass_marks;?></td>
                          <td>
                          <?php if($markDistributionByProgramme[$i]->is_attendance == 1)
                          {
                            echo 'Yes';
                          }
                          elseif ($markDistributionByProgramme[$i]->is_attendance == 0)
                          {
                            echo 'No';
                          } ?>  
                          </td>
                          <td>
                          <?php if($markDistributionByProgramme[$i]->is_pass == 1)
                          {
                            echo 'Yes';
                          }
                          elseif ($markDistributionByProgramme[$i]->is_pass == 0)
                          {
                            echo 'No';
                          } ?>  
                          </td>
                          <td style="text-align: center;">
                          <a href='/prdtm/programme/markDistribution/<?php echo $id_programme;?>/<?php echo $markDistributionByProgramme[$i]->id;?>'>Edit</a> |
                           <a onclick="deleteMarksDistribution(<?php echo $markDistributionByProgramme[$i]->id; ?>)">Delete</a>
                          </td>

                          </tr>
                        <?php 
                    } 
                    ?>
                      </tbody>
                  </table>
                </div>

              </div>




      <?php
       
      }
      ?>








   </div>
</div>
<footer class="footer-wrapper">
   <p>&copy; 2019 All rights, reserved</p>
</footer>

<script src="<?php echo BASE_PATH; ?>assets/ckeditor/ckeditor.js"></script>

<script type="text/javascript">

    $('select').select2();

    $(function(){
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
        yearRange: "1960:2001"
    });
    });

    function deleteProgramHasResources(id)
    {
      var cnf= confirm('Do you really want to delete?');
      if(cnf==true)
      {
         $.ajax(
            {
               url: '/programme_model/programme/deleteProgramHasResources/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    window.location.reload();
               }
            });
       }
    }

    function getProgrammeMarksDistributionDuplication()
    {
      if($('#form_main').valid())
      {
        var id_exam_component = $("#id_exam_component").val();

        if(id_exam_component != '')
        {
          var tempPR = {};
          tempPR['id_exam_component'] = id_exam_component;
          tempPR['id_programme'] = "<?php echo $id_programme; ?>";
          tempPR['id'] = "<?php echo $id_marks_distribution; ?>";

          $.ajax(
          {
             url: '/prdtm/programme/getProgrammeMarksDistributionDuplication',
              type: 'POST',
             data:
             {
              tempData: tempPR
             },
             error: function()
             {
              alert('Something is wrong');
             },
             success: function(result)
             {
                // alert(result);
                if(result == '0')
                {
                    alert('Duplicate Exam Components Not Allowed');
                    // $("#one_id_fee_item").val('');
                }
                else
                if(result == '1')
                {
                  $("#form_main").submit();
                }
             }
          });
        }
      }
    } 

    function saveMarksDistribution()
    {
        if($('#form_main').valid())
        {
            $('#form_main').submit();
        // var tempPR = {};

        // tempPR['id_programme'] = <?php echo $programme->id ?>;
        // tempPR['min'] = $("#min").val();
        // tempPR['max'] = $("#max").val();
        // tempPR['id_grade'] = $("#id_grade").val();
        // tempPR['is_fail'] = $("#is_fail").val();

        //     $.ajax(
        //     {
        //        url: '/examination/markDistribution/saveMarksDistribution',
        //         type: 'POST',
        //         // type: 'POST',
        //        data:
        //        {
        //         tempData: tempPR
        //        },
        //        error: function()
        //        {
        //         alert('Something is wrong');
        //        },
        //        success: function(result)
        //        {
        //         window.location.reload();
        //        }
        //     });
        }
    }

    function deleteMarksDistribution(id)
    {
        var cnf= confirm('Do you really want to delete?');
        if(cnf==true)
        {

         $.ajax(
            {
               url: '/prdtm/programme/deleteMarksDistribution/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    window.location.reload();
               }
            });
        }
    }



    $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                pass_marks: {
                    required: true
                },
                max: {
                    required: true
                },
                id_exam_component: {
                    required: true
                },
                is_pass: {
                    required: true
                },
                is_attendance: {
                    required: true
                }
            },
            messages: {
                pass_marks: {
                    required: "<p class='error-text'>Pass Marks Required</p>",
                },
                max: {
                    required: "<p class='error-text'>Max Marks Required</p>",
                },
                id_exam_component: {
                    required: "<p class='error-text'>Select Exam Component</p>",
                },
                is_pass: {
                    required: "<p class='error-text'>Select Pass Compulsary</p>",
                },
                is_attendance: {
                    required: "<p class='error-text'>Select Attendance Compulsary</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


</script>