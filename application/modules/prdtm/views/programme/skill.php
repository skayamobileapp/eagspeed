<?php
$programme_model = new Programme_Model();

$urlarray = explode ('/',$_SERVER['REQUEST_URI']);

$urlmodule = $urlarray['1'];
$urlcontroller = $urlarray['2'];
$urlaction = $urlarray['3'];

$id_product_type = $programmeDetails->id_programme_type;

$programme_tabs  = $programme_model->getProductTabsByProductId($id_product_type);

// echo "<Pre>";print_r($programme_tabs);exit();
?>
<div class="container-fluid page-wrapper">
   <div class="main-container clearfix">
       <ul class="page-nav-links">
             <li ><a href="/prdtm/programme/edit/<?php echo $id_programme;?>">Course Information</a></li>

 <li><a href="/prdtm/programme/faculty/<?php echo $id_programme;?>">Course Instructor</a></li>


            <li><a href="/prdtm/programme/newassessment/<?php echo $id_programme;?>">Course Assessment</a></li>

           

            <li><a href="/prdtm/programme/syllabus/<?php echo $id_programme;?>">Course Content</a></li>
            <li><a href="/prdtm/programme/fee/<?php echo $id_programme;?>">Course Fees</a></li>
            <li><a href="/prdtm/programme/award/<?php echo $id_programme;?>">Course Certificate</a></li>
            
                    <li class="active"><a href="/prdtm/programme/skill/<?php echo $id_programme;?>">Other Settings</a></li>

            <li><a href="/prdtm/programme/aim/<?php echo $id_programme;?>">Status of Course Details</a></li>
        </ul>

 <div class="form-container">
            <h4 class="form-group-title">Language</h4>

             
                <div class="row">

                    <div class="col-sm-4">
                  <div class="form-group">
                     <label>Language <span class='error-text'>*</span></label>
                     <select name="language" id="language" class="form-control" onchange="showPartner(this.value)">
                        <option value="" >Select</option>
                        <option value="1" <?php if ($programmeDetails->language == '1')
                           {
                               echo "selected";
                           } ?>>English</option>
                        <option value="2" <?php if ($programmeDetails->language == '2')
                           {
                               echo "selected";
                           } ?>>Bahasa Melayu</option>
                     </select>
                  </div>
               </div>
                 <div class="col-sm-4">
                  <div class="form-group">
                     <br/>
                     <button type="button" class="btn btn-primary btn-lg" value="Modules" name="save">Save</button>
                  </div>
               </div>

                 

                </div>

         </div>

 <div class="form-container">
            <h4 class="form-group-title">Keyword / Meta Search</h4>

             
                <div class="row">

                    <div class="col-sm-4">
                  <div class="form-group">
                     <label>Keyword / Meta Search <span class='error-text'>*</span></label>
                     <input type='text' class="form-control" name='keyword' id='keyword'>
                  </div>
               </div>
                 <div class="col-sm-4">
                  <div class="form-group">
                     <br/>
                     <button type="button" class="btn btn-primary btn-lg" value="Modules" name="save">Save</button>
                  </div>
               </div>

                 

                </div>

         </div>


      <form id="form_programme" action="" method="post">
         <div class="form-container">
            <h4 class="form-group-title">Skills & Competencies Details</h4>

             
                <div class="row">

                  <div class="col-sm-4">
                    <div class="form-group">
                       <label>Skills <span class='error-text'>*</span></label>
                       <select name="id_skill" id="id_skill" class="form-control" required>
                          <option value="">Select</option>
                          <?php
                             if (!empty($skillList))
                             {
                                 foreach ($skillList as $record)
                                 { ?>
                          <option value="<?php echo $record->id; ?>"
                              <?php
                              if($record->id == $programSkill->id_skill)
                              {
                                echo 'selected';
                              }
                              ?>
                              ><?php echo $record->name; ?>
                          </option>
                          <?php
                             }
                             }
                             ?>
                       </select>
                    </div>
                  </div>

                  <div class="col-sm-4">
                    <div class="form-group">
                       <label>Competencies <span class='error-text'>*</span></label>
                       <select name="id_competency" id="id_competency" class="form-control" required>
                          <option value="">Select</option>
                          <?php
                             if (!empty($competencyList))
                             {
                                 foreach ($competencyList as $record)
                                 { ?>
                          <option value="<?php echo $record->id; ?>"
                              <?php
                              if($record->id == $programSkill->id_competency)
                              {
                                echo 'selected';
                              }
                              ?>
                              ><?php echo $record->name; ?>
                          </option>
                          <?php
                             }
                             }
                             ?>
                       </select>
                    </div>
                  </div>

                   <div class="col-sm-4">
                    <div class="form-group">
                        <br/>
                         <button type="button" onclick="getProgrammeSkillDuplication()" class="btn btn-primary btn-lg" value="Modules" name="save">Save</button>
                    </div>
                </div>

                </div>
         </div>
     
      </form>

   

      <?php

        if(!empty($programHasSkills))
        {
            ?>

            <div class="form-container">
                    <h4 class="form-group-title">Skill & Competencies Details</h4>

                

                  <div class="custom-table">
                    <table class="table">
                        <thead>
                            <tr>
                            <th>Sl. No</th>
                             <th>Skill</th>
                             <th>Competency</th>
                             <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                             <?php
                         $total = 0;
                          for($i=0;$i<count($programHasSkills);$i++)
                         { ?>
                            <tr>
                            <td><?php echo $i+1;?></td>
                            <td><?php echo $programHasSkills[$i]->skill_name;?></td>
                            <td><?php echo $programHasSkills[$i]->competency_name;?></td>
                            <td class="text-center">
                                <a href='/prdtm/programme/skill/<?php echo $id_programme;?>/<?php echo $programHasSkills[$i]->id;?>'>Edit</a> | 
                                <a onclick="deleteProgramHasSkills(<?php echo $programHasSkills[$i]->id; ?>)">Delete</a>
                            </td>

                             </tr>
                          <?php 
                      } 
                      ?>
                        </tbody>
                    </table>
                  </div>

                </div>

        <?php
        
        }
         ?>


   </div>
</div>
<footer class="footer-wrapper">
   <p>&copy; 2019 All rights, reserved</p>
</footer>

<script type="text/javascript">

    $('select').select2();

    function getProgrammeSkillDuplication()
    {
      if($('#form_programme').valid())
      {
        var id_skill = $("#id_skill").val();
        var id_competency = $("#id_competency").val();

        if(id_skill != '')
        {
          var tempPR = {};
          tempPR['id_skill'] = id_skill;
          tempPR['id_competency'] = id_competency;
          tempPR['id_programme'] = "<?php echo $id_programme; ?>";
          tempPR['id'] = "<?php echo $id_programme_skill; ?>";
          

          $.ajax(
          {
             url: '/prdtm/programme/getProgrammeSkillDuplication',
              type: 'POST',
             data:
             {
              tempData: tempPR
             },
             error: function()
             {
              alert('Something is wrong');
             },
             success: function(result)
             {
                // alert(result);
                if(result == '0')
                {
                    alert('Duplicate Programme Skill & Competency Details Not Allowed' );
                    $("#id_skill").val('');
                    $("#id_competency").val('');
                }
                else
                if(result == '1')
                {
                  $("#form_programme").submit();
                }
             }
          });
        }
      }
    }


    function deleteProgramHasSkills(id)
    {
      var cnf= confirm('Do you really want to delete?');
      if(cnf==true)
      {
         $.ajax(
            {
               url: '/prdtm/programme/deleteProgramHasSkills/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    window.location.reload();
               }
            });
       }
    }


</script>
<script src="<?php echo BASE_PATH; ?>assets/ckeditor/ckeditor.js"></script>


<script type="text/javascript">



CKEDITOR.replace('overview',{
  width: "100%",
  height: "300px"

}); 

</script>