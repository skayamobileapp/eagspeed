<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Course type</h3>
        </div>
        <form id="form_main" action="" method="post">

 

              <div class="form-container">
            <h4 class="form-group-title">Course type Details</h4>



             <div class="row">
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Course type Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $productType->name;?>">
                    </div>
                </div>
          
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Course type Number <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="code" name="code" value="<?php echo $productType->code;?>">
                    </div>
                </div>
                 <div class="col-sm-4">
                    <div class="form-group">
                        <p>Status <span class='error-text'>*</span></p>
                        <label class="radio-inline">
                          <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                        </label>                              
                    </div>                         
                </div>
            </div>
            <div class="row">

                        <div class="col-sm-12">
                        <div class="form-group">
                            <label>Description </label>
                            <textarea class="form-control" id="description" name="description" style="height: 100px;"><?php echo $productType->description;?></textarea>
                        </div>
                    </div>

          

               
            </div>
        </div>
         <div class="form-container">
            <h4 class="form-group-title">Step Details</h4>


             <div class="row">
                  <div class="col-sm-4">
                    <div class="form-group">
                        <label>step one title <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="step_one_title" name="step_one_title" value="<?php echo $productType->step_one_title;?>">
                    </div>
                </div>
          

                        <div class="col-sm-8">
                        <div class="form-group">
                            <label>Step one Description </label>
                            <textarea class="form-control" id="step_one_description" name="step_one_description" style="height: 100px;"><?php echo $productType->step_one_description;?></textarea>
                        </div>
                    </div>

          

               
            </div>

             <div class="row">
                  <div class="col-sm-4">
                    <div class="form-group">
                        <label>step two title <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="step_two_title" name="step_two_title" value="<?php echo $productType->step_two_title;?>">
                    </div>
                </div>
          

                        <div class="col-sm-8">
                        <div class="form-group">
                            <label>Step two Description </label>
                            <textarea class="form-control" id="step_two_description" name="step_two_description" style="height: 100px;"><?php echo $productType->step_two_description;?></textarea>
                        </div>
                    </div>

          

               
            </div>

             <div class="row">
                  <div class="col-sm-4">
                    <div class="form-group">
                        <label>step three title <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="step_three_title" name="step_three_title" value="<?php echo $productType->step_three_title;?>">
                    </div>
                </div>
          

                        <div class="col-sm-8">
                        <div class="form-group">
                            <label>Step three Description </label>
                            <textarea class="form-control" id="step_three_description" name="step_three_description" style="height: 100px;"><?php echo $productType->step_three_description;?></textarea>
                        </div>
                    </div>

          

               
            </div>

             <div class="row">
                  <div class="col-sm-4">
                    <div class="form-group">
                        <label>step four title <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="step_four_title" name="step_four_title" value="<?php echo $productType->step_four_title;?>">
                    </div>
                </div>
          

                        <div class="col-sm-8">
                        <div class="form-group">
                            <label>Step four Description </label>
                            <textarea class="form-control" id="step_four_description" name="step_four_description" style="height: 100px;"><?php echo $productType->step_four_description;?></textarea>
                        </div>
                    </div>

          

               
            </div>


          

        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="../list" class="btn btn-link">Cancel</a>
            </div>
        </div>


        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

   $(document).ready(function() {
        $('select').select2();

        showchildCategory(<?php echo $productType->id_parent_product;?>);

        

        $("#form_main").validate({
            rules: {
                code: {
                    required: true
                },
                name: {
                    required: true
                }
            },
            messages: {
                code: {
                    required: "<p class='error-text'>Code Required</p>",
                },
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


   function showchildCategory(id) {
      if(id=='99999') {
         $("#showChildDiv").hide();
      } else {

        var childid = <?php echo $productType->id_child_product;?>;
         $.get("/prdtm/productType/getChild/"+id+"/"+childid, function(data, status)
        {
             $("#optionDiv").html(data);

        });

        $("#showChildDiv").show();
      }

   }

</script>