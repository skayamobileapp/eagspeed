<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Product Type Tabs</h3>
            <?php
            if($id_product_tab == NULL)
            {
            ?>
                <a href="../list" class="btn btn-link"> < Back</a>
            <?php
            }
            ?>
        </div>

 

            <div class="form-container">
                <h4 class="form-group-title">Product Type Details</h4>

                <div class="row">
                    
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Product Name <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="name" name="name" value="<?php echo $productType->name;?>" readonly>
                        </div>
                    </div>
              
                    
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Product Number <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="code" name="code" value="<?php echo $productType->code;?>" readonly>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Description </label>
                            <input type="text" class="form-control" id="description" name="description" value="<?php echo $productType->description;?>" readonly>
                        </div>
                    </div>

                </div>



                <div class="row">


                    <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                            </label>                              
                        </div>                         
                    </div>

                </div>

            </div>


        <form id="form_main" action="" method="post">

            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <?php
                    if($id_product_tab != NULL)
                    {
                    ?>
                        <a href="../../tabs/<?php echo $id_product_type; ?>" class="btn btn-link">Cancel</a>
                    <?php
                    }
                    ?>
                </div>
            </div>





            <?php

              if(!empty($tabList))
              {
                        $this->load->model('product_type_model');

                  ?>
                  <br>

                  <div class="form-container">
                          <h4 class="form-group-title">Product Type Tab Details</h4>

                      

                        <div class="custom-table">
                          <table class="table">
                              <thead>
                                  <tr>
                                    <th>Select</th>
                                  <th>Sl. No</th>
                                   <th>Tab Title</th>
                                   <th>Description</th>
                                  </tr>
                              </thead>
                              <tbody>
                            <?php
                               $total = 0;
                                for($i=0;$i<count($tabList);$i++)
                               { 


                                  $subFields = $this->product_type_model->getAllFieldsById($tabList[$i]->id);

$checked='';
                                  for($ta=0;$ta<count($assignedTabs);$ta++) {
                                    if($tabList[$i]->title==$assignedTabs[$ta]->name) {
                                      $checked="Checked=checked";
                                    }
                                  }

                                ?>
                                  <tr>
                                  <td style="width:5%"><input type='checkbox'  name='tablist[]' value='<?php echo $tabList[$i]->id;?>' <?php echo $checked;?>/></td>

                                  <td style="width:5%"><?php echo $i+1;?></td>
                                  <!-- <td><?php echo $tabList[$i]->name;?></td> -->
                                  <td style="width:25%;font-weight: bold;
    font-size: 15px;
    color: #c22026;"><?php echo $tabList[$i]->title;?></td>
                                  <td><?php echo $tabList[$i]->description;?></td>



                                  </tr>

                                   <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                      <td colspan="4">
                                        <table class="table">

                                           <tr style='    background-color: #f3f3f3;'>
                                            <td>Field</td>
                                            <td>Compulsory</td>
                                            <td>Optional</td>
                                            <td>Not Applicable</td>
                                          </tr>


                                  <?php for($m=0;$m<count($subFields);$m++) {
                                    $values = $this->product_type_model->getValueNumber($tabList[$i]->id,$subFields[$m]->id);
$checkedone = "";
$checkedtwo = "";
$checkedthree = "";
                                    if($values[0]->value_no=='1') {
                                      $checkedone = "Checked=checked";
                                    } else if($values[0]->value_no=='2') {
                                      $checkedtwo = "Checked=checked";
                                    } else if($values[0]->value_no=='3') {
                                      $checkedthree = "Checked=checked";
                                    }

                                    if($checkedone=='' && $checkedtwo=='' && $checkedthree=='') {
                                      $checkedone = "Checked=checked";
                                    }





                                    ?>

                                   
                                          <tr>
                                            <td style="width:25%"><?php echo $subFields[$m]->field_name;?></td>
                                            <td style="width:25%"><input type='radio' name='fieldnames[<?php echo $subFields[$m]->id;?>][]' id='fieldnames' value='1' <?php echo $checkedone;?>/></td>
                                            <td style="width:25%"><input type='radio' name='fieldnames[<?php echo $subFields[$m]->id;?>][]' id='fieldnames' value='2' <?php echo $checkedtwo;?>/></td>
                                            <td style="width:25%"><input type='radio' name='fieldnames[<?php echo $subFields[$m]->id;?>][]' id='fieldnames' value='3' <?php echo $checkedthree;?>/></td>
                                          </tr>
                                    

                                  <?php } ?> 
                                    </table></td>
                                    </tr>
                                <?php 
                            } 
                            ?>
                              </tbody>
                          </table>
                        </div>

                      </div>




              <?php
              
              }

              ?>


        </form>








        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

</div>
<script>
    
    $('select').select2();

    function deleteProductHasTabs(id)
    {
      var cnf= confirm('Do you really want to delete?');
      if(cnf==true)
      {
        $.ajax(
            {
               url: '/prdtm/productType/deleteProductHasTabs/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    location.reload();
               }
            });
      }
    }

    function checkAllCheckBoxes()
    {
      var statuscheck = $("#checkAll").is(':checked');
      if(statuscheck==true)
      {
          $('input:checkbox').prop('checked', true);
      }

      if(statuscheck==false)
      {
          $('input:checkbox').prop('checked', false);
      }    
    }


    $(document).ready(function()
    {
        // showchildCategory(<?php echo $productType->id_parent_product;?>);

        $("#form_main").validate({
            rules: {
                id_tab: {
                    required: true
                }
            },
            messages: {
                id_tab: {
                    required: "<p class='error-text'>Select Tab</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


  

</script>