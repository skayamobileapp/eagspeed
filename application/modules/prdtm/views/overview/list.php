<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Guidelines and Best Practices
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" style="font-size:14px;">
            <div class="panel-body">
              <div class="form-horizontal">

                <div class="row">

                  <div class="col-sm-12">
                   <ul>
                    <li><b>What is an Online Course </b><Br/>An online course is the delivery of learning materials and assessment via the Internet which can be accessed anytime and anywhere using a desktop, laptop, tablet or a smartphone. 
                      
                    </li>
                   </ul>
                  </div>

                </div>

                <div class="row">

                  <div class="col-sm-12">
                   <ul>
                    <li><b>Types of Online Courses </b> An online course can be: <br/>
                      a) Instructor or Teacher-Led courses which incorporates <ul>
                        <li>Only Synchronous (live) interaction – Live lectures available online or Face-to-Face lectures (both may be recorded available for students)</li>
                        <li>Only Asynchronous (delayed) interaction – Interaction with students facilitated through social media (such as whatsapp, telegram) and discussion forum</li>
                        <li>Combination of Synchronous (live) and Asynchronous (delayed) interaction</li>
                      </ul>
                      b) Self-Paced Course which DOES NOT include the involvement of the instructor or teacher synchronously or asynchronously.

                    </li>
                   </ul>
                  </div>

                </div>



                  <div class="row">

                  <div class="col-sm-12">
                   <ul>
                    <li><b>Meeting the Needs of the Target Audience </b><br/>Ensure that the online course has taken into consideration audience needs and the course helps the learner solve a problem. Focus on:<ul>
                        <li>Age of Learners</li>
                        <li>Prior experiences of Learners</li>
                        <li>Digital competencies of Learners</li>
                        <li>Reason for taking the course such as fulfilling programme requirements, enrichment, upskilling and reskilling. </li>
                      </ul>
                    
                    </li>
                   </ul>
                  </div>

                </div>

                   <div class="row">

                  <div class="col-sm-12">
                   <ul>
                    <li><b>Course Description</b><br/>
                      Short description of the content or subject matter emphasising application in the real world. It should be clear, concise, and easy to understand – not more than 80 words. Detail the significant learning experiences and benefits students can expect.                    
                    </li>
                   </ul>
                  </div>

                </div>


                    <div class="row">

                  <div class="col-sm-12">
                   <ul>
                    <li><b>Course Learning Outcomes</b><br/>
                      Learning Outcomes are descriptions of the specific knowledge, skills, or expertise that the learner will acquire from the course. <br/>
Learning Outcomes should include verbs that describes an observable action
<ul>
                    <li>what the learner will be able to do </li>
<li>The conditions they will be able to do it,</li>
<li>The performance level they should be able to reach<br/></li>
</ul>
Learning Outcomes are measurable and should be aligned to the assessment methods employed<br/>
Learning Outcomes should focus on how the learner will be able to apply their new knowledge in a real-world context.                  
                    </li>
                   </ul>
                  </div>

                </div>



                   <div class="row">

                  <div class="col-sm-12">
                   <ul>
                    <li><b>Duration of the Course</b><br/>
                     Duration of the course should be clearly specified (days or weeks) and the total number of hours learners are required to spend on the course.                 
                    </li>
                   </ul>
                  </div>

                </div>



                   <div class="row">

                  <div class="col-sm-12">
                   <ul>
                    <li><b>Credits [if applicable]</b><br/>
                      If applicable, the number of credits a course carries should be specified which will facilitate transfer of credits to accredited programmes or otherwise. You are advised to use the credit equivalency of 1 credit = 40 student learning hours (according to MQA).                 
                    </li>
                   </ul>
                  </div>

                </div>

                 <div class="row">

                  <div class="col-sm-12">
                   <ul>
                    <li><b>Assessment</b><br/>
                      The Assessment method for course should be clearly specified. 
                       <ul>
                                          <li>Align assessment to the course learning outcomes. </li>
                      <li>State Grading policy.</li>
                      </ul>
                      Example:
                       <ul>
                                          <li>
                      Final Examination – type</li>
                      <li>Assignments – provide details on what learners are required to do</li>
                      <li>Quiz – the number of attempts allowable</li>
                      Also specify, the conditions for the award of a Certificate of Completion or a Certificate of Achievement.                
                    </li>
                   </ul>
                  </div>

                </div>






              </div>
            </div>
          </div>
        </form>
      </div>
    </div>       
 </div>
</div>

<script>

    $('select').select2();
  
    function clearSearchForm()
    {
      window.location.reload();
    }
</script>