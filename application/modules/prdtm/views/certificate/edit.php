<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Certificate</h3>
        </div>
         <ul class="page-nav-links">
            <li class="active"><a href="/prdtm/certificate/edit/<?php echo $id_certificate;?>">Certificate Details</a></li>
            <li><a href="/prdtm/certificate/condition/<?php echo $id_certificate;?>">Condition Details</a></li>
            <!-- <li><a href="/examination/award/assesment/<?php echo $id_award;?>">Assesment Details</a></li>
            <li><a href="/examination/award/activities/<?php echo $id_award;?>">Activities</a></li> -->


        </ul>


        <form id="form_educationLevel" action="" method="post">

         <div class="form-container">
            <h4 class="form-group-title">Certificate Details</h4>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $educationLevel->name; ?>">
                    </div>
                </div>
             </div>
         <div class="row">


                <div class="col-sm-12">
                    <div class="form-group">
                        <label>Description <span class='error-text'>*</span></label>

                         <textarea class="form-control z-depth-1" rows="3" placeholder="Short Descripton..." name="description" id="description"><?php echo $educationLevel->description; ?></textarea>


                       
                    </div>
                </div>

                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" <?php if($educationLevel->status=='1') {
                                 echo "checked=checked";
                              };?>><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0" <?php if($educationLevel->status=='0') {
                                 echo "checked=checked";
                              };?>>
                              <span class="check-radio"></span> In-Active
                            </label>                              
                        </div>                         
                </div>
            </div>

        </div>
            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="../list" class="btn btn-link">Cancel</a>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>
    $(document).ready(function() {
        $("#form_educationLevel").validate({
            rules: {
                name: {
                    required: true
                },
                short_name: {
                    required: true
                },
                status: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name required</p>",
                },
                short_name: {
                    required: "<p class='error-text'>Short Name required</p>",
                },
                status: {
                    required: "<p class='error-text'>Select Status</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>


<script src="<?php echo BASE_PATH; ?>assets/ckeditor/ckeditor.js"></script>

<script type="text/javascript">




CKEDITOR.replace('description',{
  width: "100%",
  height: "100px"

}); 

</script>

