<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class DeliveryMode extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('delivery_mode_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('delivery_mode.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));

            $data['searchParam'] = $formData;
            
            $data['deliveryModeList'] = $this->delivery_mode_model->deliveryModeList();

            // print_r($subjectDetails);exit;
            
            $this->global['pageTitle'] = 'Campus Management System : Category Type';
            $this->loadViews("delivery_mode/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('delivery_mode.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            
            // print_r(expression)

            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $data = array(
                    'name' => $name,
                    'status' => $status
                );
            
                $result = $this->delivery_mode_model->addNewdeliveryMode($data);
                redirect('/prdtm/deliveryMode/list');
            }

            $this->global['pageTitle'] = 'Campus Management System : Add Category Type';
            $this->loadViews("delivery_mode/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('delivery_mode.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/prdtm/deliveryMode/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $data = array(
                    'name' => $name,
                    'status' => $status
                );
                
                $result = $this->delivery_mode_model->editdeliveryModeDetails($data,$id);
                redirect('/prdtm/deliveryMode/list');
            }
            $data['deliveryModeDetails'] = $this->delivery_mode_model->getdeliveryModeDetails($id);
            $this->global['pageTitle'] = 'Campus Management System : Edit Category Type';
            $this->loadViews("delivery_mode/edit", $this->global, $data, NULL);
        }
    }
}
