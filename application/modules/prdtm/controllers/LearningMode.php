<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class LearningMode extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('learning_mode_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('learning_mode_model.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));

            $data['searchParam'] = $formData;
            
            $data['learningModeList'] = $this->learning_mode_model->learningModeList();

            // print_r($subjectDetails);exit;
            
            $this->global['pageTitle'] = 'Campus Management System : Category Type';
            $this->loadViews("learning_mode/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('learning_mode.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            
            // print_r(expression)

            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $data = array(
                    'name' => $name,
                    'status' => $status
                );
            
                $result = $this->learning_mode_model->addNewlearningMode($data);
                redirect('/prdtm/learningMode/list');
            }

            $this->global['pageTitle'] = 'Campus Management System : Add Category Type';
            $this->loadViews("learning_mode/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('learning_mode.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/prdtm/learningMode/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $data = array(
                    'name' => $name,
                    'status' => $status
                );
                
                $result = $this->learning_mode_model->editlearningModeDetails($data,$id);
                redirect('/prdtm/learningMode/list');
            }
            $data['learningModeDetails'] = $this->learning_mode_model->getlearningModeDetails($id);
            $this->global['pageTitle'] = 'Campus Management System : Edit Category Type';
            $this->loadViews("learning_mode/edit", $this->global, $data, NULL);
        }
    }
}
