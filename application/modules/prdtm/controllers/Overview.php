<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Overview extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->isLoggedIn();
        error_reporting(0);
    }

    function list()
    {
         $data='';
       
            $this->global['pageTitle'] = 'Campus Management System : Product Type';
            $this->loadViews("overview/list", $this->global, $data, NULL);
        
    }
}
?>