<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ProductType extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('product_type_model');
        $this->isLoggedIn();
        error_reporting(0);
    }

    function list()
    {
        if ($this->checkAccess('product_type.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));

            $data['searchParam'] = $formData;
            
            $data['productTypeList'] = $this->product_type_model->productTypeListSearch($formData);

            // print_r($subjectDetails);exit;
            
            $this->global['pageTitle'] = 'Campus Management System : Product Type';
            $this->loadViews("product_type/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('product_type.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            
            $id_user = $this->session->userId;
            $id_session = $this->session->my_session_id;
            // print_r(expression)

            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));
                $id_parent_product = $this->security->xss_clean($this->input->post('id_parent_product'));
                $id_child_product = $this->security->xss_clean($this->input->post('id_child_product'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $description = $this->security->xss_clean($this->input->post('description'));

            
                $data = array(
                    'code'=>$code,
                    'name' => $name,
                    'description' =>$description,
                    'id_parent_product' =>$id_parent_product,
                    'id_child_product' =>$id_child_product,
                    'status' => $status,
                    'created_by' => $id_user

                );
            
                $id_producttype = $this->product_type_model->addNewProductType($data);



                $functionName = 'core_course_create_categories';
                $rand = rand(00000000,999999999);

                $user1 = new stdClass();


                $user1->name = $name;
                $user1->description = $description;


                $users = array($user1);
                $params = array('categories' => $users);

                /// REST CALL
                $restformat = "json";
                $serverurl = 'https://lmssystem.tech/webservice/rest/server.php?wstoken=f49e9fcee28e547c612eeeee6cae3cb9&wsfunction=' . $functionName. '&moodlewsrestformat=' . $restformat;
                require_once ('curl.php');
                $curl = new curl();


                $resp = $curl->post($serverurl, $params);


                ///echo '</br>************************** Server Response    createUser()**************************</br></br>';
                ///echo $serverurl . '</br></br>';

                $responseArray = json_decode($resp);
                ///print_R($responseArray);exit;
                //print_R($responseArray[0]->id);
                $studentdata = array();
                $studentdata['moodle_id'] = $responseArray[0]->id;
                if($responseArray[0]->id) {
                      $this->product_type_model->editProductTypeDetails($studentdata,$id_producttype);
                }

                redirect('/prdtm/productType/list');
            }

            $data['getParentProduct'] = $this->product_type_model->getParentProduct();
            $this->global['pageTitle'] = 'Campus Management System : Add Product Type';
            $this->loadViews("product_type/add", $this->global, $data, NULL);
        }
    }

    function edit($id = NULL)
    {
        if ($this->checkAccess('product_type.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $id_user = $this->session->userId;
            $id_session = $this->session->my_session_id;

            if ($id == null)
            {
                redirect('/prdtm/productType/list');
            }
            if($this->input->post())
            {

                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));
                $id_parent_product = $this->security->xss_clean($this->input->post('id_parent_product'));
                $id_child_product = $this->security->xss_clean($this->input->post('id_child_product'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $description = $this->security->xss_clean($this->input->post('description'));

            
                $step_one_title = $this->security->xss_clean($this->input->post('step_one_title'));
                $step_one_description = $this->security->xss_clean($this->input->post('step_one_description'));

                $step_two_title = $this->security->xss_clean($this->input->post('step_two_title'));
                $step_two_description = $this->security->xss_clean($this->input->post('step_two_description'));


                $step_three_title = $this->security->xss_clean($this->input->post('step_three_title'));
                $step_three_description = $this->security->xss_clean($this->input->post('step_three_description'));


                $step_four_title = $this->security->xss_clean($this->input->post('step_four_title'));
                $step_four_description = $this->security->xss_clean($this->input->post('step_four_description'));


                $data = array(
                    'code'=>$code,
                    'name' => $name,
                    'description' =>$description,
                    'id_parent_product' =>$id_parent_product,
                    'id_child_product' =>$id_child_product,
                    'status' => $status,
                    'updated_by' => $id_user,
                    'updated_dt_tm' => date('Y-m-d H:i:s'),
                    'step_one_title' =>$step_one_title,
                    'step_one_description' =>$step_one_description,
                    'step_two_title' =>$step_two_title,
                    'step_two_description' =>$step_two_description,
                    'step_three_title' =>$step_three_title,
                    'step_three_description' =>$step_three_description,
                    'step_four_title' =>$step_four_title,
                    'step_four_description' =>$step_four_description
                );

                $result = $this->product_type_model->editProductTypeDetails($data,$id);
                redirect('/prdtm/productType/list');
            }
            $data['productType'] = $this->product_type_model->getProductTypeDetails($id);
                        $data['getParentProduct'] = $this->product_type_model->getParentProduct();

            $this->global['pageTitle'] = 'Campus Management System : Edit Product Type';
            $this->loadViews("product_type/edit", $this->global, $data, NULL);
        }
    }

    function tabs($id,$id_product_tab = NULL)
    {
        if ($this->checkAccess('product_type.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $id_user = $this->session->userId;
            $id_session = $this->session->my_session_id;

            if ($id == null)
            {
                redirect('/prdtm/productType/list');
            }

            if($this->input->post())
            {

            print_r($_POST);exit;

                for($i=0;$i<count($_POST['fieldnames']);$i++) 
                {
                    $data = array(
                        'id_product' => $id,
                        'id_product_tab_has_fields' => $_POST['fieldnames'][0],
                        'id_product_tab_has_fields' => $_POST['fieldnames'][0]


                    );


                    if($id_product_tab > 0)
                    {
                        $data['updated_by'] = $id_user;
                        $data['updated_dt_tm'] = date('Y-m-d H:i:s');
                        
                        $result = $this->product_type_model->editProductTypeTab($data,$id_product_tab);
                    }
                    else
                    {
                        $data['created_by'] = $id_user;
                        
                        $result = $this->product_type_model->addProductTypeFields($data);
                        $result = $this->product_type_model->addProductTypeTab($data);
                    }
                
                }

                // redirect($_SERVER['HTTP_REFERER']);
                redirect('/prdtm/productType/tabs/'.$id);
            }

            $data['id_product_type'] = $id;
            $data['id_product_tab'] = $id_product_tab;
            $data['productType'] = $this->product_type_model->getProductTypeDetails($id);
            $data['productTabsList'] = $this->product_type_model->getProductTabsByProductId($id);
            $data['productTab'] = $this->product_type_model->getProductTab($id_product_tab);
            $data['getParentProduct'] = $this->product_type_model->getParentProduct();
            $data['tabList'] = $this->product_type_model->getTabList();

            if($id_product_tab != NULL)
            {                
                $data['order_no'] = $data['productTab']->order_no;
            }
            else
            {
                $data['order_no'] = count($data['productTabsList']) + 1;
            }

            // echo "<Pre>"; print_r($data);exit;


            $this->global['pageTitle'] = 'Campus Management System : Edit Product Type';
            $this->loadViews("product_type/tabs", $this->global, $data, NULL);
        }
    }

    function productTabs($id,$id_product_tab = NULL)
    {
        if ($this->checkAccess('product_type.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/prdtm/productType/list');
            }

            if($this->input->post())
            {

                $id_tab = $this->security->xss_clean($this->input->post('id_tab'));
                $order_no = $this->security->xss_clean($this->input->post('order_no'));
                $tab = $this->product_type_model->getTab($id_tab);

                $sql = $this->product_type_model->deleteallFields($id);
                $sql = $this->product_type_model->deletealltabs($id);




                for($m=0;$m<count($_POST['tablist']);$m++) {
                    $tabname = $this->product_type_model->getTab($_POST['tablist'][$m]);


            
                    $datas = array(
                        'id_product' => $id,
                        'tab' => $tabname->name,
                        'name' => $tabname->title,
                        'order_no' => $m+1,
                        'status' =>1
                    );


                        $result = $this->product_type_model->addproductTabs($datas);
                }
                //  $datas = array(
                //         'id_product' => $id,
                //         'tab' => 'overview',
                //         'name' => 'Description',
                //         'order_no' => $m+1,
                //         'status' =>1
                //     );
                // $result = $this->product_type_model->addproductTabs($datas);

                //   $datas = array(
                //         'id_product' => $id,
                //         'tab' => 'topic',
                //         'name' => "Topic's",
                //         'order_no' => $m+2,
                //         'status' =>1
                //     );
                // $result = $this->product_type_model->addproductTabs($datas);

                for($i=0;$i<count($_POST['fieldnames']);$i++) 
                {
                    $data = array(
                        'id_product' => $id,
                        'id_product_tab_has_fields' => $i,
                        'value_no' => $_POST['fieldnames'][$i][0]
                    );


                        $result = $this->product_type_model->addProductTypeFields($data);
                
                }


                // redirect($_SERVER['HTTP_REFERER']);
                redirect('/prdtm/productType/list');
            }

            $data['id_product_type'] = $id;
            $data['id_product_tab'] = $id_product_tab;
            $data['productType'] = $this->product_type_model->getProductTypeDetails($id);
            $data['productTabsList'] = $this->product_type_model->getProductTabsByProductId($id);
            $data['productTab'] = $this->product_type_model->getProductTab($id_product_tab);
            $data['getParentProduct'] = $this->product_type_model->getParentProduct();
            // $data['tabList'] = $this->product_type_model->getTabList();
            $data['tabList'] = $this->product_type_model->getProductTabListByProductTypeId($id);

            $data['assignedTabs'] = $this->product_type_model->assignedProductTabsByProductId($id);

            if($id_product_tab != NULL)
            {                
                $data['order_no'] = $data['productTab']->order_no;
            }
            else
            {
                $data['order_no'] = count($data['productTabsList']) + 1;
            }

            // echo "<Pre>"; print_r($data['tabList']);exit;


            $this->global['pageTitle'] = 'Campus Management System : Edit Product Type';
            $this->loadViews("product_type/product_tabs", $this->global, $data, NULL);
        }        
    }

    function getChild($id= NULL, $childid=NULL)
    {
         $results = $this->product_type_model->getChildForParent($id);

            // echo "<Pre>"; print_r($programme_data);exit;

          $table="   
                <script type='text/javascript'>
                     $('select').select2();
                 </script>
         ";

            $table.="
            <select name='id_child_product' id='id_child_product' class='form-control'>
                <option value=''>Select</option>
                ";


            $table.="<option value='99999'>Top</option>";
            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $selected = "";

            if($id==$childid) {
                $selected = "selected=selected";
            }
            $table.="<option value=".$id." ".$selected.">".$name.
                    "</option>";

            }
            $table.="</select>";
        
            echo $table;
            exit;
    }

    function deleteProductHasTabs($id)
    {
        $inserted_id = $this->product_type_model->deleteProductHasTabs($id);
        echo "Success"; 
    }

}
