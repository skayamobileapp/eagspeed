<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Certificate extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('certificate_model');
        $this->isLoggedIn();
        error_reporting(1);
    }

    function list()
    {
        if ($this->checkAccess('certificate.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;
            $data['educationLevelList'] = $this->certificate_model->certificateListSearch($name);
            $this->global['pageTitle'] = 'School Management System : Education Level List';
            $this->loadViews("certificate/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('certificate.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $id_user = $this->session->userId;
            $id_session = $this->session->my_session_id;


            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'description' => $description,
                    'status' => $status,
                    'created_by' => $id_user
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->certificate_model->addNewCertificate($data);
                redirect('/prdtm/certificate/list');
            }
            $this->global['pageTitle'] = 'School Management System : Add Education Level';
            $this->loadViews("certificate/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('certificate.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $id_user = $this->session->userId;
            $id_session = $this->session->my_session_id;
            
            if ($id == null)
            {
                redirect('/prdtm/certificate/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'description' => $description,
                    'status' => $status,
                    'updated_by' => $id_user,
                    'updated_dt_tm' => date('Y-m-d H:i:s')
                );

                $result = $this->certificate_model->editCertificate($data,$id);
                redirect('/prdtm/certificate/list');
            }
            $data['educationLevel'] = $this->certificate_model->getCertificate($id);
            $data['id_certificate'] = $id;
            $this->global['pageTitle'] = 'School Management System : Edit Education Level';
            $this->loadViews("certificate/edit", $this->global, $data, NULL);
        }
    }

      function condition($id,$id_award_condition = NULL)
    {
        if ($this->checkAccess('award.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/examination/award/list');
            }
            if($this->input->post())
            {
                // echo '<Pre>';print_r($this->input->post());exit();

                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                $id_condition = $this->security->xss_clean($this->input->post('id_condition'));
            
                $data = array(
                    'id_certificate' => $id,
                    'id_condition' => $id_condition,
                    'status' => 1,
                    'created_by' => $id_user
                );

                if($id_award_condition > 0)
                {
                    $result = $this->certificate_model->editAwardCondition($data,$id_award_condition);
                }else
                {
                    $result = $this->certificate_model->addAwardCondition($data);
                }

                redirect('/prdtm/certificate/condition/'.$id);
            }
            
            $data['id_certificate'] = $id;
            $data['id_award_condition'] = $id_award_condition;
            $data['awardDetails'] = $this->certificate_model->getAward($id);
            $data['awardConditionList'] = $this->certificate_model->getAwardConditionList($id);
            $data['awardCondition'] = $this->certificate_model->getAwardCondition($id_award_condition);
            $data['conditionList'] = $this->certificate_model->getConditionListByStatus('1');

            // echo '<Pre>';print_r($awardCondition);exit();

            $this->global['pageTitle'] = 'Campus Management System : Edit Award';
            $this->loadViews("certificate/condition", $this->global, $data, NULL);
        }
    }

     function deleteAwardConditionDetails($id)
    {
        $edit_data['status'] = 0;
        $deleted = $this->certificate_model->editAwardCondition($edit_data,$id);
        echo $deleted;exit;
    }



}
