<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Programme extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('programme_model');
        $this->isLoggedIn();
        error_reporting(0);
    }

    function list()
    {
        if ($this->checkAccess('programme.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_category'] = $this->security->xss_clean($this->input->post('id_category'));
            $formData['id_category_setup'] = $this->security->xss_clean($this->input->post('id_category_setup'));
            $formData['id_programme_type'] = $this->security->xss_clean($this->input->post('id_programme_type'));
            $formData['id_partner_university'] = $this->security->xss_clean($this->input->post('id_partner_university'));

            $data['searchParam'] = $formData;

            $data['categoryList'] = $this->programme_model->categoryListByStatus('1');
            $data['productTypeSetupList'] = $this->programme_model->productTypeSetupListByStatus('1');
            $data['organisationDetails'] = $this->programme_model->getOrganisation();

            $data['programmeList'] = $this->programme_model->programmeListSearch($formData);



            $this->global['pageTitle'] = 'Campus Management System : Program List';
            $this->loadViews("programme/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('programme.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $id_user = $this->session->userId;

            if($this->input->post())
            {                

                // For file validation from 36 to 44 , file size validationss
                if($_FILES['image'])
                {

                    $certificate_name = $_FILES['image']['name'];
                    $certificate_size = $_FILES['image']['size'];
                    $certificate_tmp =$_FILES['image']['tmp_name'];
                    
                    // echo "<Pre>"; print_r($certificate_tmp);exit();

                    $certificate_ext=explode('.',$certificate_name);
                    $certificate_ext=end($certificate_ext);
                    $certificate_ext=strtolower($certificate_ext);


                    $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'Image File');

                    $image_file = $this->uploadFile($certificate_name,$certificate_tmp,'Image File');
                }


                $id_session = $this->session->my_session_id;
                $id_user = $this->session->userId;

                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $credit = $this->security->xss_clean($this->input->post('credit'));
                $max_duration = $this->security->xss_clean($this->input->post('max_duration'));
                $duration_type = $this->security->xss_clean($this->input->post('duration_type'));
                $id_study_level = $this->security->xss_clean($this->input->post('id_study_level'));
                $id_delivery_mode = $this->security->xss_clean($this->input->post('id_delivery_mode'));
                $id_learning_mode = $this->security->xss_clean($this->input->post('id_learning_mode'));
                $entry_qualification = $this->security->xss_clean($this->input->post('entry_qualification'));
                $short_description = $this->security->xss_clean($this->input->post('short_description'));
                $introduction = $this->security->xss_clean($this->input->post('introduction'));
                $synopsys    = $this->security->xss_clean($this->input->post('synopsys'));
                $works = $this->security->xss_clean($this->input->post('works'));
                $pass = $this->security->xss_clean($this->input->post('pass'));
                $assessment_type = $this->security->xss_clean($this->input->post('assessment_type'));
                $assessment_details = $this->security->xss_clean($this->input->post('assessment_details'));
$id_staff = $this->security->xss_clean($this->input->post('id_staff'));               
$id_partner_university = $this->security->xss_clean($this->input->post('id_partner_university'));               
$id_programme_type = $this->security->xss_clean($this->input->post('id_programme_type'));               
$id_category = $this->security->xss_clean($this->input->post('id_category'));               
$id_education_level = $this->security->xss_clean($this->input->post('id_education_level'));      
$course_fee = $this->security->xss_clean($this->input->post('course_fee'));      
$pre_requisite = $this->security->xss_clean($this->input->post('pre_requisite'));      
$status = $this->security->xss_clean($this->input->post('status'));      
$course_sub_type = $this->security->xss_clean($this->input->post('course_sub_type'));      
$language_of_delivery = $this->security->xss_clean($this->input->post('language_of_delivery'));      





    $is_free_course = 1;
   if($course_fee>0){
    $is_free_course = 0;
   }


                // $id_staff = $this->security->xss_clean($this->input->post('id_staff'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'credit' => $credit,
                    'max_duration' => $max_duration,
                    'duration_type' => $duration_type,
                    'id_study_level' => $id_study_level,
                    'id_delivery_mode' => $id_delivery_mode,
                    'id_study_level' => $id_study_level,
                    'entry_qualification' => $entry_qualification,
                    'short_description' => $short_description,
                    'introduction' => $introduction,
                    'synopsys' => $synopsys,
                    'works' => $works,
                    'pass' => $pass,
                    'assessment_type'=>$assessment_type,
                    'assessment_details' => $assessment_details,
                    'created_by' => $id_user,
                    'id_staff'=>$id_staff,
                    'id_partner_university'=>$id_partner_university,
                    'id_programme_type'=>$id_programme_type,
                    'id_category'=>$id_category,
                    'id_education_level'=>$id_education_level,
                    'pre_requisite'=>$pre_requisite,
                    'id_learning_mode'=>$id_learning_mode,
                    'is_free_course'=>$is_free_course,
                    'status'=>$status,
                    'course_sub_type'=>$course_sub_type,
                    'language_of_delivery'=>$language_of_delivery
                );

               
                 


                // echo "<Pre>"; print_r($image_file);exit;

                if($image_file)
                {
                    $data['course_image'] = $image_file;
                }

                $inserted_id = $this->programme_model->addNewProgrammeDetails($data);



               $cloResultFromTemp = $this->programme_model->getCloTemp($id_session);
               for($i=0;$i<count($cloResultFromTemp);$i++) {
                    $newclo = array();

                    $id = $cloResultFromTemp[$i]->id;
                    $cloname = $cloResultFromTemp[$i]->clo_name;

                    $dataclo['clo_name'] = $cloname;
                    $dataclo['id_programme'] = $inserted_id;
                    $this->programme_model->insertintoMainCLO($dataclo);
               }


                $topicResultFromTemp = $this->programme_model->getTopicTemp($id_session);
               for($i=0;$i<count($topicResultFromTemp);$i++) {
                    $newclo = array();

                    $id = $topicResultFromTemp[$i]->id;
                    $topic_name = $topicResultFromTemp[$i]->topic_name;
                    $topic_overview = $topicResultFromTemp[$i]->topic_overview;
                    $topic_outcome = $topicResultFromTemp[$i]->topic_outcome;

                    $datatopic['topic_name'] = $topic_name;
                    $datatopic['topic_overview'] = $topic_overview;
                    $datatopic['topic_outcome'] = $topic_outcome;
                    $datatopic['id_programme'] = $inserted_id;
                    $this->programme_model->insertintoMainTopic($datatopic);
               }

                $staffResultFromTemp = $this->programme_model->getStaffTemp($id_session);
               for($i=0;$i<count($staffResultFromTemp);$i++) {
                    $datatopicstaff = array();
                    $id = $staffResultFromTemp[$i]->id;
                    $id_staff = $staffResultFromTemp[$i]->id_staff;

                    $datatopicstaff['id_staff'] = $id_staff;
                    $datatopicstaff['id_programme'] = $inserted_id;
                    $this->programme_model->insertintoMainStaff($datatopicstaff);
               }


               for($i=0;$i<count($_POST['overallassissmentids']);$i++) {

                 $idassignment = $_POST['overallassissmentids'][$i];
                    $datatopicstaff = array();

                    $datatopicstaff['id_assignment'] = $idassignment;
                    $datatopicstaff['marks'] = $_POST['overallassissment'][$idassignment];
                    $datatopicstaff['id_programme'] = $inserted_id;
                    $this->programme_model->insertintoMainAssignment($datatopicstaff);
               }

                for($i=0;$i<count($_POST['overallcertificateids']);$i++) {

                 $idassignment = $_POST['overallcertificateids'][$i];
                    $datatopicstaff = array();

                    $datatopicstaff['id_certificate'] = $_POST['overallcertificate'][$idassignment];
                    $datatopicstaff['id_programme'] = $inserted_id;
                    $this->programme_model->insertintoMainCertificate($datatopicstaff);
               }

                    
                

                if($inserted_id)
                {
                    $fee_structure_data = array(
                        'name' => "Fee Structure for - ".$name,
                        'name_optional_language' => $name,
                        'code' => $code,
                        'amount'=>$course_fee,
                        'id_programme' => $inserted_id,
                        'id_currency' => 1,
                        'status' => 1,
                        'created_by' => $id_user
                    );
                    
                    $id_fee_structure_master = $this->programme_model->addNewFeeStructureMaster($fee_structure_data);

                    $fee_structure_details = array(
                        'id_fee_structure' => $id_fee_structure_master,
                        'id_fee_item' => 2,
                        'amount' => $course_fee,
                        'status' => 1,
                        'created_by' => $id_user
                    );
                    $this->programme_model->addNewFeeStructureMasterDetails($fee_structure_details);

                }

                //delete all temp table
                $this->programme_model->deleteTempCloData($id_session);
                $this->programme_model->deleteTempStaffData($id_session);
                $this->programme_model->deleteTempTopicData($id_session);

                //
                // $functionName = 'core_course_create_courses';
                // $rand = rand(00000000,999999999);

                // $user1 = new stdClass();
                // $user1->fullname = $name;
                // $user1->shortname = $code;
                // $user1->categoryid = $id_category;


                // $users = array($user1);
                // $params = array('courses' => $users);

                // /// REST CALL
                // $restformat = "json";
                // $serverurl = 'https://lmssystem.tech/webservice/rest/server.php?wstoken=f49e9fcee28e547c612eeeee6cae3cb9&wsfunction=' . $functionName. '&moodlewsrestformat=' . $restformat;
                // require_once ('curl.php');
                // $curl = new curl();


                // $resp = $curl->post($serverurl, $params);


                redirect('/prdtm/programme/edit/'. $inserted_id);
            }
            else
            {
                $this->programme_model->deleteTempProgHasDeanDataBySession($id_session);
            }

            $data['staffList'] = $this->programme_model->staffListByStatus('1');
            $data['awardList'] = $this->programme_model->awardLevelListByStatus('1');
            $data['schemeList'] = $this->programme_model->schemeListByStatus('1');

            $data['categoryList'] = $this->programme_model->categoryListByStatus('1');
            $data['categoryTypeList'] = $this->programme_model->categoryTypeListByStatus('1');
            $data['productTypeSetupList'] = $this->programme_model->productTypeSetupListByStatus('1');
            $data['statusList'] = $this->programme_model->statusListByType('Programme');
            $data['studyLevelList'] = $this->programme_model->getStudyLevel();
            $data['educationLevelList'] = $this->programme_model->educationLevelListByStatus('1');
            

            $data['partnerList'] = $this->programme_model->partnerUniversityList();

            $data['deliveryModeList'] = $this->programme_model->getDeliveryMode();

            $data['learningModeList'] = $this->programme_model->getLearningMode();


            $data['assessmentList'] = $this->programme_model->getAssessmentList();
            $data['certificateList'] = $this->programme_model->getCertificateList();

            // $data['educationLevelList'] = $this->programme_model->educationLevelListByStatus('1');



            // echo "<Pre>";print_r($data['categoryList']);exit();

            $this->global['pageTitle'] = 'Campus Management System : Add Programme';
            $this->loadViews("programme/add", $this->global, $data, NULL);
        }
    }

    function sso($id){

          $params = [
            'user' => [
                'email' => 'ace@aeu.edu.my'
            ]
        ];
        $serverurl = "https://lms.myeduskills.com/webservice/rest/server.php?wstoken=721f6638fb484690528dad6d71d72fcd&wsfunction=auth_userkey_request_login_url&moodlewsrestformat=json";
        require_once ('curl.php');
        $curl = new curl();
        $resp = $curl->post($serverurl, $params);
        $responseArray = json_decode($resp);
        $url = $responseArray->loginurl.'&wantsurl=https://lms.myeduskills.com/course/view.php?id='.$id;
        $data['url'] = $url;
        $this->loadViews("programme/sso", $this->global, $data, NULL);

    }

    function edit($id = NULL)
    {
        if ($this->checkAccess('programme.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/prdtm/programme/list');
            }
            if($this->input->post())
            {

                // echo "<Pre>"; print_r($this->input->post());exit();

                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                 // For file validation from 36 to 44 , file size validationss


                if($_FILES['image'])
                {

                    $certificate_name = $_FILES['image']['name'];
                    $certificate_size = $_FILES['image']['size'];
                    $certificate_tmp =$_FILES['image']['tmp_name'];
                    
                    // echo "<Pre>"; print_r($certificate_tmp);exit();

                    $certificate_ext=explode('.',$certificate_name);
                    $certificate_ext=end($certificate_ext);
                    $certificate_ext=strtolower($certificate_ext);

                    $image_file = $this->uploadFile($certificate_name,$certificate_tmp,'Image File');
                }


                $id_session = $this->session->my_session_id;
                $id_user = $this->session->userId;

                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $credit = $this->security->xss_clean($this->input->post('credit'));
                $max_duration = $this->security->xss_clean($this->input->post('max_duration'));
                $duration_type = $this->security->xss_clean($this->input->post('duration_type'));
                $id_study_level = $this->security->xss_clean($this->input->post('id_study_level'));
                $id_delivery_mode = $this->security->xss_clean($this->input->post('id_delivery_mode'));
                $id_learning_mode = $this->security->xss_clean($this->input->post('id_learning_mode'));
                $entry_qualification = $this->security->xss_clean($this->input->post('entry_qualification'));
                $short_description = $this->security->xss_clean($this->input->post('short_description'));
                $introduction = $this->security->xss_clean($this->input->post('introduction'));
                $synopsys    = $this->security->xss_clean($this->input->post('synopsys'));
                $works = $this->security->xss_clean($this->input->post('works'));
                $pass = $this->security->xss_clean($this->input->post('pass'));
                $assessment_type = $this->security->xss_clean($this->input->post('assessment_type'));
                $assessment_details = $this->security->xss_clean($this->input->post('assessment_details'));
$id_staff = $this->security->xss_clean($this->input->post('id_staff'));               
$id_partner_university = $this->security->xss_clean($this->input->post('id_partner_university'));               
$id_programme_type = $this->security->xss_clean($this->input->post('id_programme_type'));               
$id_category = $this->security->xss_clean($this->input->post('id_category'));               
$id_education_level = $this->security->xss_clean($this->input->post('id_education_level'));      
$course_fee = $this->security->xss_clean($this->input->post('course_fee'));      
$pre_requisite = $this->security->xss_clean($this->input->post('pre_requisite'));      

$status = $this->security->xss_clean($this->input->post('status'));      
$course_sub_type = $this->security->xss_clean($this->input->post('course_sub_type'));      
$language_of_delivery = $this->security->xss_clean($this->input->post('language_of_delivery'));      




    $is_free_course = 1;
   if($course_fee>0){
    $is_free_course = 0;
   }


                // $id_staff = $this->security->xss_clean($this->input->post('id_staff'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'credit' => $credit,
                    'max_duration' => $max_duration,
                    'duration_type' => $duration_type,
                    'id_study_level' => $id_study_level,
                    'id_delivery_mode' => $id_delivery_mode,
                    'id_study_level' => $id_study_level,
                    'entry_qualification' => $entry_qualification,
                    'short_description' => $short_description,
                    'introduction' => $introduction,
                    'synopsys' => $synopsys,
                    'works' => $works,
                    'pass' => $pass,
                    'assessment_type'=>$assessment_type,
                    'assessment_details' => $assessment_details,
                    'created_by' => $id_user,
                    'id_staff'=>$id_staff,
                    'id_partner_university'=>$id_partner_university,
                    'id_programme_type'=>$id_programme_type,
                    'id_category'=>$id_category,
                    'id_education_level'=>$id_education_level,
                    'pre_requisite'=>$pre_requisite,
                    'id_learning_mode'=>$id_learning_mode,
                    'is_free_course'=>$is_free_course,
                    'status'=>$status,
                    'course_sub_type'=>$course_sub_type,
                    'language_of_delivery'=>$language_of_delivery
                );

                $this->programme_model->deleteprogrammebyCert($id);
                $this->programme_model->deleteprogrammeassissment($id);


                $dataupdate['amount']=$course_fee;
                $updateFeeAmount = $this->programme_model->updateFee($id,$dataupdate);




                  for($i=0;$i<count($_POST['overallcertificateids']);$i++) {

                 $idassignment = $_POST['overallcertificateids'][$i];
                    $datatopicstaff = array();

                    $datatopicstaff['id_certificate'] = $_POST['overallcertificate'][$idassignment];
                    $datatopicstaff['id_programme'] = $id;
                    $this->programme_model->insertintoMainCertificate($datatopicstaff);
               }


                 for($i=0;$i<count($_POST['overallassissmentids']);$i++) {

                 $idassignment = $_POST['overallassissmentids'][$i];
                    $datatopicstaffs = array();

                    $datatopicstaffs['id_assignment'] = $idassignment;
                    $datatopicstaffs['marks'] = $_POST['overallassissment'][$idassignment];
                    $datatopicstaffs['id_programme'] = $id;

                    $this->programme_model->insertintoMainAssignment($datatopicstaffs);
               }


                    

               
                 


                // echo "<Pre>"; print_r($image_file);exit;

                if($image_file)
                {
                    $data['course_image'] = $image_file;
                }
                $result = $this->programme_model->editProgrammeDetails($data,$id);

               

                redirect('/prdtm/programme/list');
            }
            
            $data['id_programme'] = $id;
            $data['staffList'] = $this->programme_model->staffListByStatus('1');
            $data['programmeHasDeanList'] = $this->programme_model->getProgrammeHasDean($id);
            $data['programmeHasCourseList'] = $this->programme_model->getProgrammeHasCourse($id);
            $data['certificateProgrammeList'] = $this->programme_model->getProgrammeCertificate($id);
            $data['awardList'] = $this->programme_model->awardLevelListByStatus('1');
            $data['schemeList'] = $this->programme_model->schemeListByStatus('1');
            $data['programTypeList'] = $this->programme_model->programTypeListByStatus('1');
            $data['educationLevelList'] = $this->programme_model->educationLevelListByStatus('1');
            // $data['schemeList'] = $this->programme_model->schemeListByStatus('1');

            $data['categoryList'] = $this->programme_model->categoryListByStatus('1');
            $data['categoryTypeList'] = $this->programme_model->categoryTypeListByStatus('1');
            $data['productTypeSetupList'] = $this->programme_model->productTypeSetupListByStatus('1');

            $data['deliveryModeList'] = $this->programme_model->getDeliveryMode();

            $data['studyLevelList'] = $this->programme_model->getStudyLevel();

            $data['partnerList'] = $this->programme_model->partnerUniversityList();
            $data['statusList'] = $this->programme_model->statusListByType('Programme');

                        $data['learningModeList'] = $this->programme_model->getLearningMode();

         $data['getprogrammeAssignment'] = $this->programme_model->getprogrammeAssignment($id);
         $data['programmeFees'] = $this->programme_model->getProgrammeFees($id);
            // $data['programmeObjective'] = $this->programme_model->programmeObjective($id);
            // $data['programmeCourseOverview'] = $this->programme_model->programmeCourseOverview($id);
            // $data['programmeDeliveryMode'] = $this->programme_model->programmeDeliveryMode($id);
            // $data['programmeAssesment'] = $this->programme_model->programmeAssesment($id);
            // $data['programmeSyllabus'] = $this->programme_model->programmeSyllabus($id);

            $data['programmeDetails'] = $this->programme_model->getProgrammeDetails($id);


            $data['partnerList'] = $this->programme_model->partnerUniversityList();

            $data['deliveryModeList'] = $this->programme_model->getDeliveryMode();

            $data['learningModeList'] = $this->programme_model->getLearningMode();


            $data['assessmentList'] = $this->programme_model->getAssessmentList();
            $data['certificateList'] = $this->programme_model->getCertificateList();



            $tab_fields = $this->programme_model->getProductFieldsByProductType($data['programmeDetails']->id_programme_type);

            $id_fields = array();
            foreach ($tab_fields as $value)
            {
                $id_field = $value->if_field;
                array_push($id_fields, $id_field);
            }


            $this->global['pageTitle'] = 'Campus Management System : Edit Programme';
            $this->loadViews("programme/edit", $this->global, $data, NULL);
        }
    }


     function addstaff(){
        $id_user = $this->session->userId;
        $clodata['id_staff'] = $_POST['tempData']['id_staff'];
        $clodata['session_id'] = $this->session->my_session_id;         
        $result = $this->programme_model->addStaffTemp($clodata);
    }

    function addstaffmain(){
         $id_user = $this->session->userId;
        $clodata['id_staff'] = $_POST['tempData']['id_staff'];
        $clodata['id_programme'] = $_POST['tempData']['id_programme'];
        $result = $this->programme_model->addStaffMain($clodata);
    }


    function editclodata($id){
                 $result = $this->programme_model->getClobyProgrammeId($id);
            if(!empty($result))
        {

        $table.="<div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>CLO NAME</th>
                    <th class='text-center'>Action</th>
                </tr>
                </thead>";
                    for($i=0;$i<count($result);$i++)
                    {
                    $id = $result[$i]->id;
                    $staff = $result[$i]->clo_name;
                    $j = $i+1;
                        $table.= "
                         <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$staff</td>                         
                            <td class='text-center'>
                                <a class='btn btn-sm btn-edit' onclick='deleteclomaindata($id)'>Delete</a>

                                 |  
                                <a class='btn btn-sm btn-edit' onclick='editclo($id)'>Edit</a>

                            </td>
                        </tr>";
                    }
        $table.= "</tbody>
         </table></div>";
        }
        
        echo  $table;
        exit;

    }


     function editstaffdata($id){
                 $result = $this->programme_model->getStaffbyProgrammeId($id);
            if(!empty($result))
        {

        $table.="<div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>CLO NAME</th>
                    <th class='text-center'>Action</th>
                </tr>
                </thead>";
                    for($i=0;$i<count($result);$i++)
                    {
                    $id = $result[$i]->id;
                    $staff = $result[$i]->name;
                    $j = $i+1;
                        $table.= "
                         <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$staff</td>                         
                            <td class='text-center'>
                                <a class='btn btn-sm btn-edit' onclick='deletestaffmaindata($id)'>Delete</a>
                            <td>
                        </tr>";
                    }
        $table.= "</tbody>
         </table></div>";
        }
        
        echo  $table;
        exit;

    }

    function edittopic(){
        $id = $_POST['id'];
        $result = $this->programme_model->getTopicbyTopicId($id);
        echo json_encode($result);
        exit;

    }


    function updateshow() {
        $id = $_POST['id'];
        $data['showonhomepage'] = $_POST['status'];
        $result = $this->programme_model->editProgrammeDetails($data, $id);
        return 1;

    }

    function editclo(){
        $id = $_POST['id'];
        $result = $this->programme_model->getCloId($id);
        echo json_encode($result);
        exit;
    }

    function edittopicdata($id){
                 $result = $this->programme_model->getTopicbyProgrammeId($id);
            if(!empty($result))
        {

        $table.="<div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Topic </th>
                    <th>Topic Overview</th>
                    <th>Topic Outcome</th>
                    <th class='text-center'>Action</th>
                </tr>
                </thead>";
                    for($i=0;$i<count($result);$i++)
                    {
                    $id = $result[$i]->id;
                    $staff = $result[$i]->topic_name;
                    $topic_overview = $result[$i]->topic_overview;
                    $topic_outcome = $result[$i]->topic_outcome;
                    $j = $i+1;
                        $table.= "
                         <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$staff</td>                         
                            <td>$topic_overview</td>                         
                            <td>$topic_outcome</td>                         
                            <td class='text-center'>
                                <a class='btn btn-sm btn-edit' onclick='deletetopicmaindata($id)'>Delete</a> |     <a class='btn btn-sm btn-edit' onclick='edittopicdataById($id)'>Edit</a>
                                                         </td>
                        </tr>";
                    }
        $table.= "</tbody>
         </table></div>";
        }
        
        echo  $table;
        exit;

    }


    function stafftext(){
        $result = $this->programme_model->getStaffTemp($this->session->my_session_id);

        if(!empty($result))
        {

        $table.="<div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Staff NAME</th>
                    <th class='text-center'>Action</th>
                </tr>
                </thead>";
                    for($i=0;$i<count($result);$i++)
                    {
                    $id = $result[$i]->id;
                    $staff = $result[$i]->first_name;
                    $j = $i+1;
                        $table.= "
                         <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$staff</td>                         
                            <td class='text-center'>
                                <a class='btn btn-sm btn-edit' onclick='deletestaffData($id)'>Delete</a>
                            <td>
                        </tr>";
                    }
        $table.= "</tbody>
         </table></div>";
        }
        
        echo  $table;
        exit;

    }


    function addclo(){
        $id_user = $this->session->userId;
        $clodata['clo_name'] = $_POST['tempData']['clo'];
        $clodata['session_id'] = $this->session->my_session_id;         
        $result = $this->programme_model->addclo($clodata);
    }

    function addclomain(){
                $id_user = $this->session->userId;
        $clodata['clo_name'] = $_POST['tempData']['clo'];
        $clodata['id_programme'] = $_POST['tempData']['id_programme'];
                if($_POST['tempData']['id_clo']>0) {
          $this->programme_model->updateMainClo($clodata,$_POST['tempData']['id_clo']);
        } else {
        $result = $this->programme_model->addMainclo($clodata);
        }

    }

     function addtopicmain(){
                $id_user = $this->session->userId;
        $clodata['topic_name'] = $_POST['tempData']['topic'];
        $clodata['topic_outcome'] = $_POST['tempData']['topic_outcome'];
        $clodata['topic_overview'] = $_POST['tempData']['topic_overview'];        
        $clodata['id_programme'] = $_POST['tempData']['id_programme'];
        if($_POST['tempData']['id_topic']>0) {
          $this->programme_model->updateMainTopic($clodata,$_POST['tempData']['id_topic']);
        } else {
        $result = $this->programme_model->addMainTopic($clodata);
        }

    }

    function clotext(){
        $result = $this->programme_model->getCloTemp($this->session->my_session_id);

        if(!empty($result))
        {

        $table.="<div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>CLO NAME</th>
                    <th class='text-center'>Action</th>
                </tr>
                </thead>";
                    for($i=0;$i<count($result);$i++)
                    {
                    $id = $result[$i]->id;
                    $staff = $result[$i]->clo_name;
                    $j = $i+1;
                        $table.= "
                         <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$staff</td>                         
                            <td class='text-center'>
                                <a class='btn btn-sm btn-edit' onclick='deletecloData($id)'>Delete</a>  |  
                                <a class='btn btn-sm btn-edit' onclick='editcloData($id)'>Edit</a>
                            </td>
                        </tr>";
                    }
        $table.= "</tbody>
         </table></div>";
        }
        
        echo  $table;
        exit;

    }


     function addtopic(){
        $id_user = $this->session->userId;
        $clodata['topic_name'] = $_POST['tempData']['topic'];
        $clodata['topic_outcome'] = $_POST['tempData']['topic_outcome'];
        $clodata['topic_overview'] = $_POST['tempData']['topic_overview'];
        $clodata['session_id'] = $this->session->my_session_id;         
        $result = $this->programme_model->addtemptopic($clodata);
    }

     function deletetopic(){
        if($_POST){

            $id = $_POST['tempData']['topic_id'];
            $this->programme_model->deletetemptopic($id);
        }
     }

       function deleteclo(){
        if($_POST){

            $id = $_POST['tempData']['clo_id'];
            $this->programme_model->deletetempclo($id);
        }
     }


     function deleteclomain(){
        if($_POST){

            $id = $_POST['tempData']['clo_id'];
            $this->programme_model->deletemainclo($id);
        }
     }

     function deletestaffmain(){
        if($_POST){

            $id = $_POST['tempData']['staff_id'];
            $this->programme_model->deletemainstaff($id);
        }
     }

    function  deletetopicmain(){
        if($_POST){

            $id = $_POST['tempData']['topic_id'];
            $this->programme_model->deletemaintopic($id);
        }
     }



     function deletestaff(){
        if($_POST){

            $id = $_POST['tempData']['staff_id'];
            $this->programme_model->deletetempstaff($id);
        }
     }

    function topictext(){
        $result = $this->programme_model->getTopicTemp($this->session->my_session_id);

        if(!empty($result))
        {

          $table.="<div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Topic </th>
                    <th>Topic Overview</th>
                    <th>Topic Outcome</th>
                    <th class='text-center'>Action</th>
                </tr>
                </thead>";
                    for($i=0;$i<count($result);$i++)
                    {
                    $id = $result[$i]->id;
                    $staff = $result[$i]->topic_name;
                    $topic_overview = $result[$i]->topic_overview;
                    $topic_outcome = $result[$i]->topic_outcome;
                    $j = $i+1;
                        $table.= "
                         <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$staff</td>                         
                            <td>$topic_overview</td>                         
                            <td>$topic_outcome</td>                         
                            <td class='text-center'>
                                <a class='btn btn-sm btn-edit' onclick='deletetopicData($id)'>Delete</a>
                            <td>
                        </tr>";
                    }
        $table.= "</tbody>
         </table></div>";

        }
        
        echo  $table;
        exit;

    }


    function syllabus($id,$id_syllabus=NULL)
    {

        $id_user = $this->session->userId;
        $id_session = $this->session->my_session_id;


        if($this->input->post())
        {
            // echo "<Pre>"; print_r($id_programme_skill);exit();

            $learning_objective = $this->security->xss_clean($this->input->post('learning_objective'));
                         
            $sylabuss_data = array(
                'learning_objective' => $learning_objective,
                'id_programme' => $id
            );            

            if($id_syllabus > 0)
            {
                $sylabuss_data['updated_by'] = $id_user;
                $sylabuss_data['updated_dt_tm'] = date('Y-m-d H:i:s');

                $result = $this->programme_model->updateProgrammeSyllabus($sylabuss_data,$id_syllabus);
            }
            else
            {
                $sylabuss_data['created_by'] = $id_user;
                
                $result = $this->programme_model->addsyllabus($sylabuss_data);
            }
                      
            redirect("/prdtm/programme/syllabus/".$id);
        }

  $data['topic'] = $this->programme_model->getprogrammeTopic($id);
        $data['topicDetails'] = $this->programme_model->gettopicById($topicid);
        
        $data['id_programme'] = $id;
        $data['id_syllabus'] = $id_syllabus;
        $data['programmeDetails'] = $this->programme_model->getProgrammeDetails($id);
        $data['syllabus'] = $this->programme_model->getProgrammeSyllabus($id);
        $data['programmeSyllabus'] = $this->programme_model->getSyllabus($id_syllabus);

        // echo "<Pre>"; print_r($data['programmeSyllabus']);exit();


        $this->global['pageTitle'] = 'Speed Management System : Edit Programme';
        $this->loadViews("programme/syllabus", $this->global, $data, NULL);
    }


    function overview($id)
    {
        $data = array();

        if($this->input->post())
        {
            $result = $this->programme_model->deleteprogramoverview($id);
            $overview = $this->security->xss_clean($this->input->post('overview'));
                         
            $data = array();
            $data['overview'] = $overview;
            $data['id_programme'] = $id;

            $result = $this->programme_model->addProgramOverview($data);
            
            redirect("/prdtm/programme/overview/".$id);
        }

        $data['id_programme'] = $id;
        $data['programmeDetails'] = $this->programme_model->getProgrammeDetails($id);
        $data['overview'] = $this->programme_model->getProgramOverview($id);

        $this->global['pageTitle'] = 'Speed Management System : Edit Programme';
        $this->loadViews("programme/overview", $this->global, $data, NULL);
    }

    function howToPass($id)
    {
        $data = array();

        if($this->input->post())
        {
            // echo "<Pre>"; print_r($_POST);exit;

            $description = $this->security->xss_clean($this->input->post('description'));
            $id_how_to_pass = $this->security->xss_clean($this->input->post('id'));
                         
            $data = array();
            $data['description'] = $description;
            $data['id_programme'] = $id;

            // echo "<Pre>"; print_r($data);exit;

            if($id_how_to_pass > 0)
            {
                $result = $this->programme_model->editProgrammeHowToPass($data,$id_how_to_pass);
            }
            else
            {
                $result = $this->programme_model->addProgrammeHowToPass($data);
            }
            redirect("/prdtm/programme/howToPass/".$id);
        }

        $data['id_programme'] = $id;
        $data['programmeDetails'] = $this->programme_model->getProgrammeDetails($id);
        $data['howToPass'] = $this->programme_model->getProgrammeHowToPassByProgrammeId($id);
        
        // echo "<Pre>"; print_r($data['howToPass']);exit;

        $this->global['pageTitle'] = 'Speed Management System : Edit Programme';
        $this->loadViews("programme/how_to_pass", $this->global, $data, NULL);
    }

    function assessment($id,$id_assesment=NULL)
    {

        $id_user = $this->session->userId;
        $id_session = $this->session->my_session_id;

        $data = array();

        if($this->input->post())
        {
            // echo "<Pre>"; print_r($_POST);exit;


            $total_weightage_percentage = $this->security->xss_clean($this->input->post('total_weightage_percentage'));
            $passing_mark_percentage = $this->security->xss_clean($this->input->post('passing_mark_percentage'));
                            
            $datas = array();

            if($passing_mark_percentage>0 && $total_weightage_percentage>0)
            {

                $result = $this->programme_model->deleteassessmentmain($id);


                $datas['total_weightage_percentage'] = $total_weightage_percentage;
                $datas['passing_mark_percentage'] = $passing_mark_percentage;
                $datas['id_programme'] = $id;


                $result = $this->programme_model->addassessmentmain($datas);
            }

            $id_examination_components = $this->security->xss_clean($this->input->post('id_examination_components'));
            $weightage = $this->security->xss_clean($this->input->post('weightage'));
            $total_mark = $this->security->xss_clean($this->input->post('total_mark'));
            $passing_mark = $this->security->xss_clean($this->input->post('passing_mark')); 

            if($weightage>0 && $total_mark>0)
            {                   
                $data = array();
                $data['weightage'] = $weightage;
                $data['total_mark'] = $total_mark;
                $data['passing_mark'] = $passing_mark;
                $data['id_programme'] = $id;
                $data['id_examination_components'] = $id_examination_components;

                if($id_assesment > 0)
                {

                    $data['updated_by'] = $id_user;
                    $data['updated_dt_tm'] = date('Y-m-d H:i:s');

                    $result = $this->programme_model->editProgrammeAssessment($data,$id_assesment);
                }
                else
                {
                    $data['created_by'] = $id_user;
                
                    $result = $this->programme_model->addProgrammeAssessment($data);
                }
            }
            redirect("/prdtm/programme/assessment/".$id);
        }


        $data['id_programme'] = $id;
        $data['id_assesment'] = $id_assesment;
        $data['programmeDetails'] = $this->programme_model->getProgrammeDetails($id);
        $data['programmeAssesment'] = $this->programme_model->getProgrammeAssesment($id_assesment);
        $data['assessmentDetailsList'] = $this->programme_model->getAssessmentDetails($id);
        $data['assessmentDetailsListMain'] = $this->programme_model->getAssessmentDetailsMain($id);
        $data['examinationComponent'] = $this->programme_model->getExaminationComponent();

        $this->global['pageTitle'] = 'Speed Management System : Edit Programme';
        $this->loadViews("programme/assessment", $this->global, $data, NULL);
    }

    function faculty($id)
    {
        $data = array();

         if($this->input->post())
        {



            if($_POST['save']=='Search') {
            $data['facultySearchList'] = $this->programme_model->fetFacultySearch();


            }

            if($_POST['save']=='Add') {

                for($l=0;$l<count($_POST['faculty']);$l++) {

                    $id_staff = $_POST['faculty'][$l];
                    $dataprogram['id_programme'] = $id;
                    $dataprogram['id_staff'] = $id_staff;
                    
                    $result = $this->programme_model->addProgrammetoStaff($dataprogram);

                }


            }

           
        }


        $data['id_programme'] = $id;
        $data['programmeDetails'] = $this->programme_model->getProgrammeDetails($id);
        $data['facultyList'] = $this->programme_model->getProgrammeHasDean($id);


        $this->global['pageTitle'] = 'Speed Management System : Edit Programme';
        $this->loadViews("programme/faculty", $this->global, $data, NULL);
    }


   function topic($id,$topicid = NULL)
   {

        $id_user = $this->session->userId;
        $id_session = $this->session->my_session_id;

        $data = array();

         if($this->input->post())
        {

            $id_programme_has_syllabus = 0;

            // echo "<Pre>"; print_r($_POST);exit;

   
            for($j=0;$j<count($_POST['id_programme_has_syllabus']);$j++)
            {
                if($j > 0)
                {
                    $id_programme_has_syllabus = $id_programme_has_syllabus.','.$_POST['id_programme_has_syllabus'][$j];
                }else
                {
                    $id_programme_has_syllabus = $_POST['id_programme_has_syllabus'][$j];
                }
            }


            $topic = $this->security->xss_clean($this->input->post('topic'));
            $message= $this->security->xss_clean($this->input->post('message'));

            $data = array();
            $data['topic'] = $topic;
            $data['id_programme'] = $id;
            $data['id_programme_has_syllabus'] = $id_programme_has_syllabus;
            $data['message'] = $message;

            // echo "<Pre>"; print_r($id_programme_has_syllabus);exit;


            // print_r($)
            if($topicid > 0)
            {

                $data['updated_by'] = $id_user;
                $data['updated_dt_tm'] = date('Y-m-d H:i:s');

                $result = $this->programme_model->updatetopic($data,$topicid);
            }
            else
            {
                $data['created_by'] = $id_user;
                $result = $this->programme_model->addtopic($data);
            }
            redirect("/prdtm/programme/topic/".$id);
        }


        $data['syllabus'] = $this->programme_model->getProgrammeSyllabus($id);


        $data['id_programme'] = $id;
        $data['id_topic'] = $topicid;
        $data['topic'] = $this->programme_model->getprogrammeTopic($id);
        $data['topicDetails'] = $this->programme_model->gettopicById($topicid);
        $data['programmeDetails'] = $this->programme_model->getProgrammeDetails($id);

        $this->global['pageTitle'] = 'Speed Management System : Edit Programme';
        $this->loadViews("programme/topic", $this->global, $data, NULL);
    }


    function updatefacilitator($id,$idprogramme)
    {
        $this->programme_model->updateAllFacilitatorToInactive($idprogramme);
        $this->programme_model->updateFacilitatorToActive($id);
    }


    function accreditation($id,$id_accreditation=NULL)
    {

        $id_user = $this->session->userId;
        $id_session = $this->session->my_session_id;

        if($this->input->post())
        {
            $category = $this->security->xss_clean($this->input->post('acceredation_category'));
            $type = $this->security->xss_clean($this->input->post('acceredation_type'));
            $acceredation_dt = $this->security->xss_clean($this->input->post('acceredation_dt'));
            $acceredation_number = $this->security->xss_clean($this->input->post('acceredation_number'));
            $valid_from = $this->security->xss_clean($this->input->post('valid_from'));
            $valid_to = $this->security->xss_clean($this->input->post('valid_to'));
            $approval_dt = $this->security->xss_clean($this->input->post('approval_date'));
            $acceredation_reference = $this->security->xss_clean($this->input->post('acceredation_reference'));
                         
            $accreditation_data = array(
                'id_program' => $id,
                'category' => $category,
                'type' => $type,
                'acceredation_dt' =>  date('Y-m-d', strtotime($acceredation_dt)),
                'acceredation_number' => $acceredation_number,
                'valid_from' =>  date('Y-m-d', strtotime($valid_from)),
                'valid_to' =>  date('Y-m-d', strtotime($valid_to)),
                'approval_dt' =>  date('Y-m-d', strtotime($approval_dt)),
                'acceredation_reference' => $acceredation_reference
            );            

            if($id_accreditation > 0)
            {
                $accreditation_data['updated_by'] = $id_user;
                $accreditation_data['updated_dt_tm'] = date('Y-m-d H:i:s');

                $result = $this->programme_model->editProgrammeHasAcceredation($accreditation_data,$id_accreditation);
            }
            else
            {
                $accreditation_data['created_by'] = $id_user;

                $result = $this->programme_model->addNewProgrammeHasAcceredation($accreditation_data);
            }

            redirect("/prdtm/programme/accreditation/".$id);
        }

        $data['id_programme'] = $id;
        $data['id_accreditation'] = $id_accreditation;
        $data['programmeDetails'] = $this->programme_model->getProgrammeDetails($id);
        $data['programmeAcceredationList'] = $this->programme_model->programmeAcceredationList($id);
        $data['programmeAcceredation'] = $this->programme_model->programmeAcceredation($id_accreditation);

        // echo "<Pre>"; print_r($data['programmeAcceredation']);exit;


        $this->global['pageTitle'] = 'Speed Management System : Edit Programme';
        $this->loadViews("programme/accreditation", $this->global, $data, NULL);
    }

    function award($id, $id_programme_award = NULL)
    {

        $id_user = $this->session->userId;
        $id_session = $this->session->my_session_id;
        // echo "<Pre>";print_r($id_award);exit;

        if($this->input->post())
        {
            $award_name = $this->security->xss_clean($this->input->post('award_name'));
            $id_award = $this->security->xss_clean($this->input->post('id_award'));
            $id_program_condition = $this->security->xss_clean($this->input->post('id_program_condition'));
                         
            $award_data = array(
                'name' => $award_name,
                'id_award' => $id_award,
                'id_program_condition' => $id_program_condition,
                'id_program' => $id
            );

            if($id_programme_award > 0)
            {
                $award_data['updated_by'] = $id_user;
                $award_data['updated_dt_tm'] = date('Y-m-d H:i:s');

                $result = $this->programme_model->updateAwardData($award_data,$id_programme_award);
            }
            else
            {
                $award_data['created_by'] = $id_user;
                $result = $this->programme_model->saveAwardData($award_data);
            }
            
            redirect("/prdtm/programme/award/".$id);
        }

        $data['id_programme'] = $id;
        $data['id_programme_award'] = $id_programme_award;
        $data['awardList'] = $this->programme_model->awardListByStatus('1');
        $data['programmeAwardList'] = $this->programme_model->programmeAwardList($id);
        $data['programmeAward'] = $this->programme_model->programmeAward($id_programme_award);
        $data['programmeDetails'] = $this->programme_model->getProgrammeDetails($id);
        // $data['programmeConditionList'] = $this->programme_model->programmeConditionListByStatus('1');

        // echo "<Pre>";print_r($data['programmeAwardList']);exit;

        $this->global['pageTitle'] = 'Speed Management System : Edit Programme';
        $this->loadViews("programme/award", $this->global, $data, NULL);
    }

    function discount($id)
    {

        $id_user = $this->session->userId;
        $id_session = $this->session->my_session_id;

        if($this->input->post())
        {
            // echo "<Pre>";print_r($this->input->post());exit;

            $is_staff_discount = $this->security->xss_clean($this->input->post('is_staff_discount'));
            $staff_discount_type = $this->security->xss_clean($this->input->post('staff_discount_type'));
            $staff_value = $this->security->xss_clean($this->input->post('staff_value'));
            $is_student_discount = $this->security->xss_clean($this->input->post('is_student_discount'));
            $student_discount_type = $this->security->xss_clean($this->input->post('student_discount_type'));
            $student_value = $this->security->xss_clean($this->input->post('student_value'));
            $id_programme_discount = $this->security->xss_clean($this->input->post('id_programme_discount'));
                         
            $discount_data = array(
                'id_programme' => $id,
                'is_staff_discount' => $is_staff_discount,
                'staff_discount_type' => $staff_discount_type,
                'staff_value' => $staff_value,
                'is_student_discount' => $is_student_discount,
                'student_discount_type' => $student_discount_type,
                'student_value' => $student_value,
                'status' => 1
            );

            if($id_programme_discount > 0)
            {
                $discount_data['updated_by'] = $id_user;
                $discount_data['updated_dt_tm'] = date('Y-m-d H:i:s');

                $result = $this->programme_model->updateDiscountData($discount_data,$id_programme_discount);
            }
            else
            {
                $discount_data['created_by'] = $id_user;
                $result = $this->programme_model->saveDiscountData($discount_data);
            }
            
            redirect("/prdtm/programme/discount/".$id);
        }

        $data['id_programme'] = $id;
        $data['id_programme_discount'] = $id_programme_discount;
        $data['programmeDetails'] = $this->programme_model->getProgrammeDetails($id);
        $data['programmeDiscount'] = $this->programme_model->getProgrammeDiscount($id);

        // echo "<Pre>";print_r($data['programmeDiscount']);exit;

        $this->global['pageTitle'] = 'Speed Management System : Edit Programme';
        $this->loadViews("programme/discount", $this->global, $data, NULL);
    }

    function structure($id)
    {
        $data = array();

        if($this->input->post())
        {
            $result = $this->programme_model->deleteProgramStructure($id);
            $structure = $this->security->xss_clean($this->input->post('structure'));
                         
            $data = array();
            $data['structure'] = $structure;
            $data['id_programme'] = $id;

            $result = $this->programme_model->addProgramStructure($data);
            
            redirect("/prdtm/programme/structure/".$id);
        }

        $data['id_programme'] = $id;
        $data['programmeDetails'] = $this->programme_model->getProgrammeDetails($id);
        $data['structure'] = $this->programme_model->getProgramStructure($id);

        $this->global['pageTitle'] = 'Speed Management System : Edit Programme';
        $this->loadViews("programme/structure", $this->global, $data, NULL);

    }

    function aim($id)
    {

        $id_user = $this->session->userId;
        $id_session = $this->session->my_session_id;

        $data = array();

        if($this->input->post())
        {
            $result = $this->programme_model->deleteProgramAim($id);
            $aim = $this->security->xss_clean($this->input->post('aim'));
                         
            $data = array();
            $data['aim'] = $aim;
            $data['id_programme'] = $id;

            $result = $this->programme_model->addProgramAim($data);
            
            redirect("/prdtm/programme/aim/".$id);
        }

        $data['id_programme'] = $id;
        $data['programmeDetails'] = $this->programme_model->getProgrammeDetails($id);
        $data['aim'] = $this->programme_model->getProgramAim($id);

        $this->global['pageTitle'] = 'Speed Management System : Edit Programme';
        $this->loadViews("programme/aim", $this->global, $data, NULL);
    }

    function modules($id,$id_module=NULL)
    {

        $id_user = $this->session->userId;
        $id_session = $this->session->my_session_id;

        $data = array();

        if($this->input->post())
        {
            // echo "<Pre>"; print_r($_POST);exit();

            $id_child_programme = $this->security->xss_clean($this->input->post('id_child_programme'));
                         
            $modules_data = array(
                'id_child_programme' => $id_child_programme,
                'id_programme' => $id
            );

            if($id_module > 0)
            {

                $modules_data['updated_by'] = $id_user;
                $modules_data['updated_dt_tm'] = date('Y-m-d H:i:s');

                $result = $this->programme_model->updateProgramModules($modules_data,$id_module);
            }
            else
            {
                $modules_data['updated_by'] = $id_user;

                $result = $this->programme_model->addProgramModules($modules_data);
            }            
            redirect("/prdtm/programme/modules/".$id);
        }

        $data['id_programme'] = $id;
        $data['id_module'] = $id_module;
        $data['programmeDetails'] = $this->programme_model->getProgrammeDetails($id);
        $data['programmeList'] = $this->programme_model->getProgrammeListByIdCategory('1');
        $data['programHasModules'] = $this->programme_model->getProgramHasModules($id);
        $data['programModule'] = $this->programme_model->getProgramModule($id_module);

        // echo "<Pre>"; print_r($data['programModule']);exit();


        $this->global['pageTitle'] = 'Speed Management System : Edit Programme';
        $this->loadViews("programme/modules", $this->global, $data, NULL);
    }


    function skill($id,$id_programme_skill=NULL)
    {

        $id_user = $this->session->userId;
        $id_session = $this->session->my_session_id;

        if($this->input->post())
        {
            // echo "<Pre>"; print_r($id_programme_skill);exit();

            $id_skill = $this->security->xss_clean($this->input->post('id_skill'));
            $id_competency = $this->security->xss_clean($this->input->post('id_competency'));
                         
            $skill_data = array(
                'id_skill' => $id_skill,
                'id_competency' => $id_competency,
                'id_programme' => $id
            );            

            if($id_programme_skill > 0)
            {
                $skill_data['updated_by'] = $id_user;
                $skill_data['updated_dt_tm'] = date('Y-m-d H:i:s');

                $result = $this->programme_model->updateProgrammeHasSkills($skill_data,$id_programme_skill);
            }
            else
            {
                $skill_data['created_by'] = $id_user;

                $result = $this->programme_model->addProgrammeHasSkills($skill_data);
            }
            
            redirect("/prdtm/programme/skill/".$id);
        }

        $data['id_programme'] = $id;
        $data['id_programme_skill'] = $id_programme_skill;
        $data['programmeDetails'] = $this->programme_model->getProgrammeDetails($id);
        $data['skillList'] = $this->programme_model->getSkillListByStatus('1');
        $data['competencyList'] = $this->programme_model->getCompetencyListByStatus('1');
        $data['programHasSkills'] = $this->programme_model->getProgramHasSkills($id);
        $data['programSkill'] = $this->programme_model->getProgramSkill($id_programme_skill);
        
        // echo "<Pre>"; print_r($data['programSkill']);exit();

        $this->global['pageTitle'] = 'Speed Management System : Edit Programme';
        $this->loadViews("programme/skill", $this->global, $data, NULL);
    }

    function resources($id,$id_programme_resources=NULL)
    {
        $id_user = $this->session->userId;
        $id_session = $this->session->my_session_id;

        if($this->input->post())
        {
            // echo "<Pre>"; print_r($id_programme_resources);exit();

            $id_resource = $this->security->xss_clean($this->input->post('id_resource'));
                         
            $skill_data = array(
                'id_resource' => $id_resource,
                'id_programme' => $id
            );            

            if($id_programme_resources > 0)
            {
                $skill_data['updated_by'] = $id_user;
                $skill_data['updated_dt_tm'] = date('Y-m-d H:i:s');

                $result = $this->programme_model->updateProgrammeHasResources($skill_data,$id_programme_resources);
            }
            else
            {
                $skill_data['created_by'] = $id_user;
                $result = $this->programme_model->addProgrammeHasResources($skill_data);
            }
            
            redirect("/prdtm/programme/resources/".$id);
        }

        $data['id_programme'] = $id;
        $data['id_programme_resources'] = $id_programme_resources;
        $data['programmeDetails'] = $this->programme_model->getProgrammeDetails($id);
        $data['resourceList'] = $this->programme_model->getResourcesListByStatus('1');
        $data['programHasResources'] = $this->programme_model->getProgramHasResources($id);
        $data['programResource'] = $this->programme_model->getProgramResourse($id_programme_resources);
        
        // echo "<Pre>"; print_r($data['programResource']);exit();

        $this->global['pageTitle'] = 'Speed Management System : Edit Programme';
        $this->loadViews("programme/resources", $this->global, $data, NULL);
    }

    function newassessment($id,$id_programme_assesment=NULL)
    {
        $id_user = $this->session->userId;
        $id_session = $this->session->my_session_id;

        if($this->input->post())
        {

            $id_assesment = $this->security->xss_clean($this->input->post('id_assesment'));
            $weightage = $this->security->xss_clean($this->input->post('weightage'));
            $pass_marks = $this->security->xss_clean($this->input->post('pass_marks'));
            $maximum_marks = $this->security->xss_clean($this->input->post('maximum_marks'));
            $is_pass = $this->security->xss_clean($this->input->post('is_pass'));
                         
            $skill_data = array(
                'id_assesment' => $id_assesment,
                'weightage' => $weightage,
                'passing_mark' => $pass_marks,
                'total_mark' => $maximum_marks,
                'is_pass' => $is_pass,
                'id_programme' => $id
            );            

            if($id_programme_assesment > 0)
            {
                $skill_data['updated_by'] = $id_user;
                $skill_data['updated_dt_tm'] = date('Y-m-d H:i:s');

                $result = $this->programme_model->editProgrammeAssessment($skill_data,$id_programme_assesment);
            }
            else
            {
                $skill_data['created_by'] = $id_user;
                $result = $this->programme_model->addProgrammeAssessment($skill_data);
            }

            redirect("/prdtm/programme/newassessment/".$id);
        }

        $data['id_programme'] = $id;
        $data['id_programme_assesment'] = $id_programme_assesment;
        $data['programmeDetails'] = $this->programme_model->getProgrammeDetails($id);
        $data['assesmentList'] = $this->programme_model->assesmentListByStatus('1');
        $data['assessmentDetailsList'] = $this->programme_model->getAssessmentDetailsByProgrammeId($id);
        $data['programAssesment'] = $this->programme_model->getProgrammeAssesment($id_programme_assesment);
        
        // echo "<Pre>"; print_r($data['assesmentList']);exit();

        $this->global['pageTitle'] = 'Speed Management System : Edit Programme';
        $this->loadViews("programme/new_assesment", $this->global, $data, NULL);
    }

    function approval($id = NULL)
    {
        if ($id == null)
        {
            redirect('/prdtm/programme/list');
        }
        if($this->input->post())
        {
            // echo "<Pre>"; print_r($this->input->post());exit();

            $id_session = $this->session->my_session_id;
            $id_user = $this->session->userId;


            $status = $this->security->xss_clean($this->input->post('status'));
            $reason = $this->security->xss_clean($this->input->post('reason'));
            $percentage = $this->security->xss_clean($this->input->post('percentage'));

            // $id_staff = $this->security->xss_clean($this->input->post('id_staff'));
        
            $data = array(
                'reason' => $reason,
                'status' => $status,
                'percentage' => $percentage,
                'updated_by' => $id_user,
                'updated_dt_tm' => date('Y-m-d H:i:s')
            );

            if($status != '5')
            {
                $data['send_for_approval'] = 0;
            }

            // echo "<Pre>";print_r($data);exit;

            $result = $this->programme_model->editProgrammeDetails($data,$id);

            if($status)
            {
                $status_data = array(
                'id_programme' => $id,
                'reason' => $reason,
                'percentage' => $percentage,
                'status' => $status,
                'created_by' => $id_user
                );

                $result = $this->programme_model->addProgrammeStatusChange($status_data);

            }

            redirect('/prdtm/programmeApproval/list');
        }

        $data['programmeDetails'] = $this->programme_model->getProgrammeDetails($id);
        $data['programmeStatusChange'] = $this->programme_model->getProgrammeStatusChangeByIdProgramme($id);
        // $data['statusList'] = $this->programme_model->statusListByType('Programme');
        $data['id_programme'] = $id;
        
        // echo "<Pre>";print_r($data['programmeStatusChange']);exit;
    
        $this->global['pageTitle'] = 'Campus Management System : Edit Programme';
        $this->loadViews("programme/approval", $this->global, $data, NULL);
    }

    function fee($id,$id_fee_details=NULL)
    {
        $data = array();
        $id_programme_fee_structure = 0;
        $programme_fee_structure = $this->programme_model->getFeeStructureMasterByProgrammeId($id);

        if($programme_fee_structure)
        {
            $id_programme_fee_structure = $programme_fee_structure->id;
        }


        if($this->input->post())
        {
            // echo "<Pre>";print_r($_POST);exit;

            $id_fee_item = $this->security->xss_clean($this->input->post('one_id_fee_item'));
            $is_registration_fee = $this->security->xss_clean($this->input->post('is_registration_fee'));
            $amount = $this->security->xss_clean($this->input->post('one_amount'));
            $id_programme = $this->security->xss_clean($this->input->post('id_programme'));


            $fee_detail_data = array(
                'id_programme' => $id_programme, 
                'id_program_landscape' => $id_programme_fee_structure, 
                'id_training_center' => 1, 
                'is_registration_fee' => $is_registration_fee, 
                'id_fee_item' => $id_fee_item, 
                'currency' => 'MYR', 
                'amount' => $amount, 
                'status' => 1 
                );

            if($id_fee_details > 0)
            {
                $inserted_id = $this->programme_model->updateFeeStructureDetail($fee_detail_data,$id_fee_details);
            }
            else
            {
                $inserted_id = $this->programme_model->addNewFeeStructure($fee_detail_data);
            }            
            redirect("/prdtm/programme/fee/".$id);
        }

        $data['id_programme'] = $id;
        $data['getProgrammeLandscapeLocal'] = $this->programme_model->getFeeStructureMaster($id_programme_fee_structure);
        $data['feeStructureLocalList'] = $this->programme_model->getFeeStructureByIdProgrammeNIdIntake($id_programme_fee_structure,'MYR');
        $data['feeSetupList'] = $this->programme_model->feeSetupListByStatus('1');

        $data['programmeDetails'] = $this->programme_model->getProgrammeDetails($id);
        $data['currencyList'] = $this->programme_model->currencyListByStatus('1');
        $data['feeStructureDetail'] = $this->programme_model->getFeeStructureDetail($id_fee_details);
        $data['id_program_scheme'] = $data['getProgrammeLandscapeLocal']->id_program_scheme;
        $data['id_program_landscape'] = $id_programme_fee_structure;
        $data['id_fee_details'] = $id_fee_details;

        // echo "<Pre>"; print_r($data['programmeDetails']);exit();

        $this->global['pageTitle'] = 'Speed Management System : Edit Programme';
        $this->loadViews("programme/fee", $this->global, $data, NULL);
    }

    function deleteFeeStructure($id)
    {
        $deleted = $this->programme_model->deleteFeeStructure($id);
        echo $deleted;exit();   
    }

    function markDistribution($id_programme,$id_marks_distribution=NULL)
    {
        $id_session = $this->session->my_session_id;
        $user_id = $this->session->userId;

        if($this->input->post())
        {
            // echo "<Pre>";print_r($_POST);exit();

            $id_exam_component = $this->security->xss_clean($this->input->post('id_exam_component'));
            $max = $this->security->xss_clean($this->input->post('max'));
            $pass_marks = $this->security->xss_clean($this->input->post('pass_marks'));
            $is_pass = $this->security->xss_clean($this->input->post('is_pass'));
            $is_attendance = $this->security->xss_clean($this->input->post('is_attendance'));

            
            $data = array(
               'id_programme' => $id_programme,
               'id_exam_component' => $id_exam_component,
               'max' => $max,
               'pass_marks' => $pass_marks,
               'is_attendance' => $is_attendance,
               'is_pass' => $is_pass,
               'status' => 1
            );

            // echo "<Pre>";print_r($id_marks_distribution);exit();
            if($id_marks_distribution > 0)
            {
                $data['updated_by'] = $user_id;
                $data['updated_dt_tm'] = date('Y-m-d H:i:s');
                $result = $this->programme_model->editMarkDistribution($data,$id_marks_distribution);
            }
            else
            {
                $data['created_by'] = $user_id;
                $result = $this->programme_model->addMarkDistribution($data);
            }
            redirect('/prdtm/programme/markDistribution/'.$id_programme);
        }

        $data['id_programme'] = $id_programme;
        $data['id_marks_distribution'] = $id_marks_distribution;
        $data['programme'] = $this->programme_model->getProgrammeDetails($id_programme);
        $data['programmeDetails'] = $data['programme'];
        $data['markDistribution'] = $this->programme_model->getMarkDistribution($id_marks_distribution);
        $data['markDistributionByProgramme'] = $this->programme_model->getMarkDistributionByProgramme($id_programme);
        $data['examComponentsList'] = $this->programme_model->examComponentsListByStatus('1');

            // echo "<Pre>";print_r($data['programme']);exit();

        $this->global['pageTitle'] = 'Speed Management System : Add Mark Distribution';
        $this->loadViews("programme/mark_distribution", $this->global, $data, NULL);
    }

    function deleteMarksDistribution($id)
    {
        $deleted = $this->programme_model->deleteMarksDistribution($id);
        echo $deleted;exit();
    }


    function saveAcceredationData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $tempData['acceredation_dt'] = date('Y-m-d', strtotime($tempData['acceredation_dt']));
        $tempData['valid_from'] = date('Y-m-d', strtotime($tempData['valid_from']));
        $tempData['valid_to'] = date('Y-m-d', strtotime($tempData['valid_to']));
        $tempData['approval_dt'] = date('Y-m-d', strtotime($tempData['approval_dt']));
            // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->programme_model->addNewProgrammeHasAcceredation($tempData);

        if($inserted_id)
        {
            echo "success";
        }
    }


    function deleteAcceredationDetails($id_details)
    {
        $inserted_id = $this->programme_model->deleteAcceredationDetails($id_details);
        
        echo "Success"; 
    }




    function saveAwardData()
    {

        $id_user = $this->session->userId;
        $id_session = $this->session->my_session_id;
        
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $id_award = $tempData['id'];
        $id_programme = $tempData['id_programme'];

        if($id_award > 0)
        {
            $tempData['updated_by'] = $id_user;
            $tempData['updated_dt_tm'] = date('Y-m-d H:i:s');

            $inserted_id = $this->programme_model->updateAwardData($tempData,$id_award);
        }
        else
        {
            $tempData['created_by'] = $id_user;

            $inserted_id = $this->programme_model->saveAwardData($tempData);
        }

        if($inserted_id)
        {
            redirect("/prdtm/programme/award/".$id_programme);
            // echo "success";
        }
    }


    function deleteAwardData($id_details)
    {
        $inserted_id = $this->programme_model->deleteAwardData($id_details);
        echo "Success"; 
    }


    function deleteAssesmentData($id_details)
    {
        $inserted_id = $this->programme_model->deleteAssesmentData($id_details);
        echo "Success"; 
    }


    function deleteTopicData($id_details)
    {
        $inserted_id = $this->programme_model->deleteTopicData($id_details);
        echo "Success"; 
    }

    function deleteSyllabusData($id_details)
    {
        $inserted_id = $this->programme_model->deleteSyllabusData($id_details);
        echo "Success"; 
    }

    
    function tempadd()
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        // echo "<Pre>";print_r($tempData);exit;
        $tempData['id_session'] = $id_session;
        $tempData['effective_start_date'] = date('Y-m-d',strtotime($tempData['effective_start_date']));
        
            $inserted_id = $this->programme_model->addTempDetails($tempData);
        // }
        $data = $this->displaytempdata();
        
        echo $data;
    }

    function tempaddcourse()
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $tempData['id_session'] = $id_session;
        // echo "<Pre>";print_r($tempData);exit;
        if($tempData['id'] && $tempData['id']>0)
        {
            $id =  $tempData['id'];
            unset($tempData['id']);
            $inserted_id = $this->programme_model->updateTempCourseDetails($tempData,$id);
        }
        else
        {

            unset($tempData['id']);
            $inserted_id = $this->programme_model->addTempCourseDetails($tempData);
        }
        $data = $this->displaytempCoursedata();
        
        echo $data;        
    }

    function displaytempdata()
    {
        $id_session = $this->session->my_session_id;
        
        $temp_details = $this->programme_model->getTempProgrammeHasDean($id_session); 

        if(!empty($temp_details))
        {

        // echo "<Pre>";print_r($details);exit;
        $table = "
        <div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Staff Name</th>
                    <th>Effective Date</th>
                    <th class='text-center'>Action</th>
                </tr>
                </thead>";
                    for($i=0;$i<count($temp_details);$i++)
                    {
                    $id = $temp_details[$i]->id;
                    $staff = $temp_details[$i]->salutation . " - " . $temp_details[$i]->staff;
                    $effective_start_date = $temp_details[$i]->effective_start_date;
                    $effective_start_date = date('d-m-Y',strtotime($effective_start_date));
                    $j = $i+1;
                        $table .= "
                         <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$staff</td>
                            <td>$effective_start_date</td>                            
                            <td class='text-center'>
                                <a class='btn btn-sm btn-edit' onclick='deleteTempData($id)'>Delete</a>
                            <td>
                        </tr>";
                    }
        $table.= "
         </tbody>
         </table>";
        }
        else
        {
            $table="";
        }
        return $table;
    }

    function displaytempCoursedata()
    {
        $id_session = $this->session->my_session_id;
        
        $temp_details = $this->programme_model->getTempProgrammeHasCourse($id_session); 
        // echo "<Pre>";print_r($details);exit;
        $table = "<table  class='table' id='list-table'>
                  <tr>
                    <th>Sl. No</th>
                    <th>Course Name</th>
                    <th class='text-centre'>Action</th>
                </tr>";
                    for($i=0;$i<count($temp_details);$i++)
                    {
                    $id = $temp_details[$i]->id;
                    $name = $temp_details[$i]->courseName;
                    $j = $i+1;
                        $table .= "
                        <tr>
                            <td>$j</td>
                            <td>$name</td>            
                            <td>
                                <a class='btn btn-sm btn-edit' onclick='deleteTempCourseData($id)'>Delete</a>
                            <td>
                        </tr>";
                    }
        $table.= "</table>";
        return $table;
    }

    function tempDetailsDataAdd()
    {
        
        $id_session = $this->session->my_session_id;

        $id_staff = $this->security->xss_clean($this->input->post('id_staff'));
        $effective_start_date = $this->security->xss_clean($this->input->post('effective_start_date'));
             
        // echo "<Pre>";  print_r($effective_start_date);exit;
        $data = array(
            'id_session' => $id_session,
            'id_staff' => $id_staff,
            'effective_start_date' => $effective_start_date
        );   
        $inserted_id = $this->programme_model->addNewTempProgrammeHasDean($data);

        $temp_details = array(
                'id' => $inserted_id,
                'id_staff' => $id_staff,
                'effective_start_date' => date('Y-m-d',strtotime($effective_start_date))
            );

        $temp_details = $this->programme_model->getTempProgrammeHasDean($id_session);
        // echo "<Pre>";  print_r($temp_details);exit;

        if(!empty($temp_details))
        {  
            $table = "
            <table  class='table' id='list-table'>
                <tr>
                    <th>Sl. No</th>
                    <th>Staff Name</th>
                    <th>Effective Date</th>
                    <th>Delete</th>
                </tr>";
                for($i=0;$i<count($temp_details);$i++)
                {
                    $id = $temp_details[$i]->id;
                    $staff = $temp_details[$i]->salutation . " - " . $temp_details[$i]->staff;
                    $effective_start_date = $temp_details[$i]->effective_start_date;

                    $table .= "
                <tr>
                    <td>$i+1</td>
                    <td>$staff</td>
                    <td>$effective_start_date</td>
                    <td>
                        <span onclick='deleteid($id)'>Delete</a>
                    <td>
                </tr>";
                }
                        
            $table .= "
            </table>";
            echo $table;
        }
    }

    function add_new_dean_on_edit()
    {
        // echo "<Pre>";print_r("jk");exit;
        $id_programme= $_POST['id_programme'];
        $id_staff = $this->security->xss_clean($this->input->post('id_staff'));
        $effective_start_date = $this->security->xss_clean($this->input->post('effective_start_date'));

         $detailsData = array(
            'id_programme' => $id_programme,
            'id_staff' => $id_staff,
            // 'id_programme' => $id_programme,
            'effective_start_date' => date('Y-m-d',strtotime($effective_start_date)),
        );
        // echo "<Pre>";print_r($detailsData);exit;
        $result = $this->programme_model->addNewProgrammeHasDean($detailsData);
        // $data['programmeHasDeanList'] = $this->programme_has_dean_model->getProgrammeHasDeanByProgrammeId($id);
        redirect($_SERVER['HTTP_REFERER']);

    }

    function delete_programme_has_dean()
    {
        $id = $this->input->get('id');
        // echo "<Pre>";print_r($id);exit;

       $this->programme_model->deleteProgrammeHasDean($id);

       redirect($_SERVER['HTTP_REFERER']);
    }

    function delete_programme_has_course()
    {
        $id = $this->input->get('id');
        // echo "<Pre>";print_r($id);exit;

       $this->programme_model->deleteProgrammeHasCourse($id);

       redirect($_SERVER['HTTP_REFERER']);
    }

    function tempedit($id){
        $data = $this->programme_model->getTempDetails($id);
        echo json_encode($data);

    }

    function tempDelete($id)
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $inserted_id = $this->programme_model->deleteTempData($id);
        $data = $this->displaytempdata();
        echo $data; 
    }

    function tempCourseDelete($id)
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $inserted_id = $this->programme_model->deleteTempCourseData($id);
        $data = $this->displaytempCoursedata();
        echo $data; 
    }

    function directadd()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $data['id_staff'] =  $tempData['id_staff'];
        $data['effective_start_date'] =  date('Y-m-d',strtotime($tempData['effective_start_date']));
        $data['id_programme'] =  $tempData['id'];
        $inserted_id = $this->programme_model->addNewProgrammeHasDean($data);

        if($inserted_id)
        {
            echo "success";exit;
        }
        
        //  $temp_details = $this->programme_model->getProgrammeHasDean($tempData['id']);
        // if(!empty($temp_details))
        // {

        // // echo "<Pre>";print_r($temp_details);exit;
        // $table = "<table  class='table' id='list-table'>
        //           <tr>
        //             <th>Sl. No</th>
        //             <th>Staff Name</th>
        //             <th>Effective Start Date</th>
        //             <th>Action</th>
        //         </tr>";
        //             for($i=0;$i<count($temp_details);$i++)
        //             {
        //             $id = $temp_details[$i]->id;
        //             $staff_name = $temp_details[$i]->salutation . " - " .$temp_details[$i]->staff;
        //             $effective_start_date = $temp_details[$i]->effective_start_date;
        //             $effective_start_date = date('d-m-Y',strtotime($effective_start_date));
        //             $j = $i+1;
        //                 $table .= "
        //                 <tr>
        //                     <td>$j</td>
        //                     <td>$staff_name</td>                         
        //                     <td>$effective_start_date</td>                         
        //                     <td>
        //                         <span onclick='deleteStaffDetailData($id)'>Delete</a>
        //                     <td>
        //                 </tr>";
        //             }
        // $table.= "</table>";

        // }
        // else
        // {
        //     $table="";
        // }

        // echo $table;           
    }

    function deleteStaffDetailData($id_details)
    {
        $inserted_id = $this->programme_model->deleteProgrammeHasDean($id_details);
        
        echo "Success"; 
    }

    function tempSchemeAdd()
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        // echo "<Pre>";print_r($tempData);exit;
        $tempData['id_session'] = $id_session;
        
        $inserted_id = $this->programme_model->addTempProgramHasScheme($tempData);
        
        $data = $this->displayTempSchemeData();
        
        echo $data;        
    }


    function displayTempSchemeData()
    {
        $id_session = $this->session->my_session_id;
        
        $temp_details = $this->programme_model->getTempProgrammeHasScheme($id_session); 

        if(!empty($temp_details))
        {

        // echo "<Pre>";print_r($details);exit;
        $table = "
        <div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Scheme</th>
                    <th class='text-center'>Action</th>
                </tr>
                </thead>";
                    for($i=0;$i<count($temp_details);$i++)
                    {
                    $id = $temp_details[$i]->id;
                    $scheme = $temp_details[$i]->scheme_code . " - " . $temp_details[$i]->scheme_name;
                    $j = $i+1;
                        $table .= "
                         <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$scheme</td>                           
                            <td class='text-center'>
                                <a class='btn btn-sm btn-edit' onclick='deleteTempProgramHasScheme($id)'>Delete</a>
                            <td>
                        </tr>";
                    }
        $table.= "
         </tbody>
         </table>";
        }
        else
        {
            $table="";
        }
        return $table;
    }

    function deleteTempProgramHasScheme($id)
    {
        $inserted_id = $this->programme_model->deleteTempProgramHasScheme($id);
        $data = $this->displayTempSchemeData();
        echo $data; 
    }

    function deleteProgramHasModules($id)
    {
        $inserted_id = $this->programme_model->deleteProgramHasModules($id);
        echo $inserted_id; 
    }

    function saveProgramObjectiveData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
            // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->programme_model->saveProgramObjectiveData($tempData);

        if($inserted_id)
        {
            echo "success";
        }

    }


    function saveCourseOverviewData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
            // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->programme_model->saveCourseOverviewData($tempData);

        if($inserted_id)
        {
            echo "success";
        }

    }


    function saveDeliveryModeData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
            // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->programme_model->saveDeliveryModeData($tempData);

        if($inserted_id)
        {
            echo "success";
        }

    }


    function saveAssesmentData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
            // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->programme_model->saveAssesmentData($tempData);

        if($inserted_id)
        {
            echo "success";
        }

    }


    function saveSyllabusData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
            // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->programme_model->saveSyllabusData($tempData);

        if($inserted_id)
        {
            echo "success";
        }

    }

    function deleteProgramObjectiveDetails($id_details)
    {
        $inserted_id = $this->programme_model->deleteProgramObjectiveDetails($id_details);
        
        echo "Success"; 
    }

    function deleteCourseOverviewDetails($id_details)
    {
        $inserted_id = $this->programme_model->deleteCourseOverviewDetails($id_details);
        
        echo "Success"; 
    }

    function deleteDeliveryModeDetails($id_details)
    {
        $inserted_id = $this->programme_model->deleteDeliveryModeDetails($id_details);
        
        echo "Success"; 
    }

    function deleteAssesmentDetails($id_details)
    {
        $inserted_id = $this->programme_model->deleteAssesmentDetails($id_details);
        
        echo "Success"; 
    }

    function deleteSyllabusDetails($id_details)
    {
        $inserted_id = $this->programme_model->deleteSyllabusDetails($id_details);
        echo "Success";
    }

    function deleteProgramHasSkills($id)
    {
        $inserted_id = $this->programme_model->deleteProgramHasSkills($id);
        echo "Success";
    }



    function updateProgramObjectiveData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $id = $tempData['id'];
        unset($tempData['id']);

        // echo "<Pre>"; print_r($tempData);exit();
        
        $inserted_id = $this->programme_model->updateProgramObjectiveData($tempData,$id);

        // echo "<Pre>"; print_r($inserted_id);exit();
        if($inserted_id)
        {
            echo "success";
        }

    }


    function updateCourseOverviewData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $id = $tempData['id'];
        unset($tempData['id']);
            // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->programme_model->updateCourseOverviewData($tempData,$id);

        if($inserted_id)
        {
            echo "success";
        }

    }


    function updateDeliveryModeData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $id = $tempData['id'];
        unset($tempData['id']);
            // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->programme_model->updateDeliveryModeData($tempData,$id);

        if($inserted_id)
        {
            echo "success";
        }

    }


    function updateAssesmentData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $id = $tempData['id'];
        unset($tempData['id']);
            // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->programme_model->updateAssesmentData($tempData,$id);

        if($inserted_id)
        {
            echo "success";
        }

    }


    function updateSyllabusData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $id = $tempData['id'];
        unset($tempData['id']);
            // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->programme_model->updateSyllabusData($tempData,$id);

        if($inserted_id)
        {
            echo "success";
        }
    }

    function getProgrammeDuplication()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $result = $this->programme_model->getProgrammeDuplication($tempData);

        if($result)
        {
            print_r('0');exit;
        }
        else
        {
            print_r('1');exit;
        }       
    }

    function getProductFieldsByProductType($id_product_type)
    {
        $product_tabs = $this->programme_model->getProductFieldsByProductType($id_product_type);
        echo "<Pre>";print_r($product_tabs);exit;
    }

    function getConditionsByAwardId($id_award)
    { 
        $awardConditionList = $this->programme_model->getAwardConditionList($id_award);

        if(!empty($awardConditionList))
        {

        // echo "<Pre>";print_r($awardConditionList);exit;
        $table = "
        <div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Condition</th>
                </tr>
                </thead>";
                    for($i=0;$i<count($awardConditionList);$i++)
                    {
                    $id = $awardConditionList[$i]->id;
                    $condition_name = $awardConditionList[$i]->condition_name;
                    $j = $i+1;
                        $table .= "
                         <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$condition_name</td>
                        </tr>";
                    }
        $table.= "
         </tbody>
         </table>";
        }
        else
        {
            $table="";
        }
        echo $table;exit;
    }

    function deleteProgramHasResources($id_details)
    {
        $inserted_id = $this->programme_model->deleteProgramHasResources($id_details);
        
        echo "Success"; 
    }

    function getProgrammeSyllabusDuplication()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $result = $this->programme_model->getProgrammeSyllabusDuplication($tempData);

        if($result)
        {
            print_r('0');exit;
        }
        else
        {
            print_r('1');exit;
        }
    }

    function getProgrammeResourceDuplication()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $result = $this->programme_model->getProgrammeResourceDuplication($tempData);
        // print_r($result);exit;

        if($result)
        {
            print_r('0');exit;
        }
        else
        {
            print_r('1');exit;
        }
    }

    function getProgrammeSkillDuplication()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $result = $this->programme_model->getProgrammeSkillDuplication($tempData);
        // print_r($result);exit;

        if($result)
        {
            print_r('0');exit;
        }
        else
        {
            print_r('1');exit;
        }
    }

    function getProgrammeAssesmentDuplication()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $result = $this->programme_model->getProgrammeAssesmentDuplication($tempData);
        // print_r($result);exit;

        if($result)
        {
            print_r('0');exit;
        }
        else
        {
            print_r('1');exit;
        }
    }

    function getProgrammeAwardDuplication()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $result = $this->programme_model->getProgrammeAwardDuplication($tempData);
        // print_r($result);exit;

        if($result)
        {
            print_r('0');exit;
        }
        else
        {
            print_r('1');exit;
        }
    }

    function getProgrammeTopicDuplication()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $result = $this->programme_model->getProgrammeTopicDuplication($tempData);
        // print_r($result);exit;

        if($result)
        {
            print_r('0');exit;
        }
        else
        {
            print_r('1');exit;
        }
    }

    function getProgrammeFeeStructureDuplication()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $result = $this->programme_model->getProgrammeFeeStructureDuplication($tempData);
        // print_r($result);exit;

        if($result)
        {
            print_r('0');exit;
        }
        else
        {
            print_r('1');exit;
        }
    }

    function getProgrammeMarksDistributionDuplication()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $result = $this->programme_model->getProgrammeMarksDistributionDuplication($tempData);
        // print_r($result);exit;

        if($result)
        {
            print_r('0');exit;
        }
        else
        {
            print_r('1');exit;
        }
    }
}