<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <!-- <h3>Edit Student</h3> -->
        </div>

        <form id="form_programgrade" action="" method="post">
  
   <div class="row">
     <div class="m-auto text-center">
       <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
     </div>
    <div class="container" id="tabs">
        <ul class="nav nav-tabs offers-tab text-center" role="tablist" >
            <li role="nav-item" class="active"><a href="#personal" class="nav-link active border rounded text-center"
                    aria-controls="personal" aria-selected="true"
                    role="tab" data-toggle="tab">Personal Details</a>
            </li>
            <li role="nav-item"><a href="#company" class="nav-link company border rounded text-center" aria-selected="false"
                    aria-controls="company" role="tab" data-toggle="tab">Company Details</a>
            </li>
            <li role="nav-item"><a href="#bank" class="nav-link bank border rounded text-center" aria-selected="false"
                    aria-controls="bank" role="tab" data-toggle="tab">Bank Details</a>
            </li>
            <li role="presentation"><a href="#billing" class="nav-link border rounded text-center"
                    aria-controls="billing" role="tab" data-toggle="tab">Billing Person Details</a>
            </li>
        </ul>
        <div class="tab-content offers-tab-content">
          <div role="tabpanel" class="tab-pane active" id="personal">
            <div class="col-12 mt-4">
                <br>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Vendor Name <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="vname" name="vname">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Email <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="vemail" name="vemail" >
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Phone Number <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="vphone" name="vphone">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <p>Gender <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="gender" id="gender" value="Male" checked="checked"><span class="check-radio"></span> Male
                             <span class='error-text'>*</span></label>
                            <label class="radio-inline">
                              <input type="radio" name="gender" id="gender" value="Female"><span class="check-radio"></span> Female
                             <span class='error-text'>*</span></label> 
                            <label class="radio-inline">
                              <input type="radio" name="gender" id="gender" value="Others"><span class="check-radio"></span> Others
                             <span class='error-text'>*</span></label>                              
                        </div>                         
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>NRIC <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="vnric" name="vnric" >
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Mobile Number <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="vmobile" name="vmobile" >
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Address One <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="vaddress_one" name="vaddress_one" >
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Address Two <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="vaddress_two" name="vaddress_two"  >
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Country <span class='error-text'>*</span></label>
                            <select name="vcountry" id="vcountry" class="form-control" onchange="getStateByCountry(this.value)" style="width: 360px">
                                <option value="">Select</option>
                                <?php
                                if (!empty($countryList))
                                {
                                    foreach ($countryList as $record)
                                    {?>
                                 <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->name;?>
                                 </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>State <span class='error-text'>*</span></label>
                            <span id='view_state'></span>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>zipcode / Pincode <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="vzipcode" name="vzipcode" >
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Date Of Birth <span class='error-text'>*</span></label>
                            <input type="text" class="form-control datepicker" id="date_of_birth" name="date_of_birth" >
                        </div>
                    </div>
                </div>

            <div class="button-block clearfix">
                <div class="bttn-group pull-right">
                    <button type="button" class="btn btn-primary btn-lg btnNext" >Next</button>
                </div>
            </div>
             
         </div> <!-- END col-12 -->  
        </div>

        <div role="tabpanel" class="tab-pane" id="company">
            <div class="col-12 mt-4">
                <br>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Company Name <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="cname" name="cname">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Email <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="cemail" name="cemail" >
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Phone Number <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="cphone" name="cphone">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Tax Number <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="tax_no" name="tax_no"  >
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Address One <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="caddress_one" name="caddress_one"  >
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Address Two <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="caddress_two" name="caddress_two"  >
                        </div>
                    </div>
                </div>
                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Country <span class='error-text'>*</span></label>
                            <select name="ccountry" id="ccountry" class="form-control" onchange="getStateByCountryCompany(this.value)" style="width: 360px">
                                <option value="">Select</option>
                                <?php
                                if (!empty($countryList))
                                {
                                    foreach ($countryList as $record)
                                    {?>
                                 <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->name;?>
                                 </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>State <span class='error-text'>*</span></label>
                            <span id='view_company_state'></span>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>zipcode / Pincode <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="czipcode" name="czipcode" >
                        </div>
                    </div>
                </div>
                <div class="button-block clearfix">
                    <div class="bttn-group pull-right">
                        <button type="button" class="btn btn-primary btn-lg btnNext2">Next</button>
                    </div>
                </div>  
         </div> <!-- END col-12 -->  
        </div>


        <div role="tabpanel" class="tab-pane" id="bank">
            <div class="col-12 mt-4">
                <br>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Bank Id <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="id_bank" name="id_bank">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Address <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="address" name="address" >
                        </div>
                    </div>
                    
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Country <span class='error-text'>*</span></label>
                            <select name="country" id="country" class="form-control" onchange="getStateByCountryBank(this.value)" style="width: 360px">
                                <option value="">Select</option>
                                <?php
                                if (!empty($countryList))
                                {
                                    foreach ($countryList as $record)
                                    {?>
                                 <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->name;?>
                                 </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>State <span class='error-text'>*</span></label>
                            <span id='view_bank_state'></span>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>City <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="city" name="city" >
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Zipcode/Pincode <span class='error-text'>*</span></label>
                            <input type="text" name="zipcode1" id="zipcode1" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Branch Number <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="branch_no" name="branch_no">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Account Number <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="acc_no" name="acc_no" >
                        </div>
                    </div>
                </div>
                <div class="button-block clearfix">
                    <div class="bttn-group pull-right">
                        <button type="button" id="addBankData" class="btn btn-success btn-lg" >Add</button>
                        <button type="button" class="btn btn-primary btn-lg btnNext3" >Next</button>
                    </div>
                </div>

            <div class="custom-table">
                <div id="view"></div>
            </div>
             
         </div> <!-- END col-12 -->  
        </div>

        <div role="tabpanel" class="tab-pane" id="billing">
            <div class="col-12 mt-4">
                <br>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Name <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="name1" name="name1">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>NRIC <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="nric1" name="nric1" >
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Email <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="email1" name="email1">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Phone Number <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="phone1" name="phone1">
                        </div>
                    </div>
                </div>
                <div class="button-block clearfix">
                    <div class="bttn-group pull-right">
                        <button type="button" id="addBillingData" class="btn btn-success btn-lg" >Add</button>
                        <button type="button" class="btn btn-primary btn-lg btnNext3" >Next</button>
                    </div>
                </div>

            <div class="custom-table">
                <div id="view1"></div>
            </div>
             
         </div> <!-- END col-12 -->  
        </div>

      </div>
    </div>

   </div> <!-- END row-->
   

            <div class="button-block clearfix">
                <div class="bttn-group pull-right pull-right">
                    <button type="submit" class="btn btn-primary btn-lg" >Save</button>
                    <a href="../list" class="btn btn-link">Cancel</a>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script type="text/javascript">

    function getStateByCountry(id)
    {
        $.get("/procurement/vendor/getStateByCountry/"+id, function(data, status){
       
            $("#view_state").html(data);
        });
    }

    function getStateByCountryCompany(id)
    {
        $.get("/procurement/vendor/getStateByCountryCompany/"+id, function(data, status){
       
            $("#view_company_state").html(data);
        });

    }

    function getStateByCountryBank(id)
    {
        $.get("/procurement/vendor/getStateByCountryBank/"+id, function(data, status){
       
            $("#view_bank_state").html(data);
        });
    }


$(document).ready(function() {
  $('.btnNext').click(function() {
    $('.nav-tabs .active').parent().next('li').find('a').trigger('click');
  });
  $('.btnNext2').click(function() {
    $('.nav-tabs .company').parent().next('li').find('a').trigger('click');
  });
  $('.btnNext3').click(function() {
    $('.nav-tabs .bank').parent().next('li').find('a').trigger('click');
  });
});

</script>
<script type="text/javascript">
    $("#addBankData").click(function()
    {
       var id_bank = $("input[name='id_bank']").val();
       var address = $("input[name='address']").val();
       var country = $("input[name='country']").val();
       var state = $("input[name='state']").val();
       var city = $("input[name='city']").val();
       var zipcode = $("input[name='zipcode1']").val();
       var branch_no = $("input[name='branch_no']").val();
       var acc_no = $("input[name='acc_no']").val();

        $.ajax(
        {
           url: '/procurement/vendor/tempAddBank',
           type: 'POST',
           data:
           {
            id_bank: id_bank,
            address: address,
            country: country,
            state: state,
            city: city,
            zipcode: zipcode,
            branch_no: branch_no,
            acc_no: acc_no
           },
           error: function()
           {
            alert(data);
            alert('Something is wrong');
           },
           success: function(result)
           {
            $("#view").html(result);
            $("input[name='id_bank']").val('');
            $("input[name='address']").val('');
            $("input[name='country']").val('');
            $("input[name='state']").val('');
            $("input[name='city']").val('');
            $("input[name='zipcode1']").val('');
            $("input[name='branch_no']").val('');
            $("input[name='acc_no']").val('');

            //alert("Record added successfully");  
           }
        });
    });
</script>
<script type="text/javascript">
    $("#addBillingData").click(function()
    {
       var name = $("input[name='name1']").val();
       var nric = $("input[name='nric1']").val();
       var email = $("input[name='email1']").val();
       var phone = $("input[name='phone1']").val();

        $.ajax(
        {
           url: '/procurement/vendor/tempAddBilling',
           type: 'POST',
           data:
           {
            name: name,
            nric: nric,
            email: email,
            phone: phone
           },
           error: function()
           {
            alert(data);
            alert('Something is wrong');
           },
           success: function(result)
           {
            $("#view1").html(result);
            $("input[name='name1']").val('');
            $("input[name='nric1']").val('');
            $("input[name='email1']").val('');
            $("input[name='phone1']").val('');

            //alert("Record added successfully");  
           }
        });
    });
</script>
<script type="text/javascript">
    $('select').select2();
</script>
<script>
  $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
  } );
</script>