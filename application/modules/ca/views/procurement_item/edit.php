<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Procurement Item</h3>
        </div>
        <form id="form_grade" action="" method="post">
            <div class="row">
               
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Description <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $procurementItem->description; ?>">
                    </div>
                </div>

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="code" name="code" value="<?php echo $procurementItem->code; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Category <span class='error-text'>*</span></label>
                        <select name="pr_category_code" id="pr_category_code" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($procurementCategoryList))
                            {
                                foreach ($procurementCategoryList as $record)
                                {?>
                                    <option value="<?php echo $record->code;  ?>"
                                        <?php 
                                        if($record->code == $procurementItem->pr_category_code)
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->code." - ".$record->description;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Sub-Category <span class='error-text'>*</span></label>
                        <select name="pr_sub_category_code" id="pr_sub_category_code" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($procurementSubCategoryList))
                            {
                                foreach ($procurementSubCategoryList as $record)
                                {?>
                                    <option value="<?php echo $record->code;  ?>"
                                        <?php 
                                        if($record->code == $procurementItem->pr_sub_category_code)
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->code." - ".$record->description;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
                
                 <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" <?php if($procurementItem->status=='1') {
                                 echo "checked=checked";
                              };?>><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0" <?php if($procurementItem->status=='0') {
                                 echo "checked=checked";
                              };?>>
                              <span class="check-radio"></span> In-Active
                            </label>                              
                        </div>                         
                </div>               
                
            </div>
            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="../list" class="btn btn-link">Cancel</a>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>
    $(document).ready(function() {
        $("#form_grade").validate({
            rules: {
                code: {
                    required: true
                },
                 description: {
                    required: true
                },
                 id_procurement_category: {
                    required: true
                },
                 id_procurement_sub_category: {
                    required: true
                },
                 status: {
                    required: true
                }
            },
            messages: {
                code: {
                    required: "<p class='error-text'>Procurement Sub Category Code required</p>",
                },
                description: {
                    required: "<p class='error-text'>Description required</p>",
                },
                id_procurement_category: {
                    required: "<p class='error-text'>Select Category Code</p>",
                },
                id_procurement_sub_category: {
                    required: "<p class='error-text'>Select Sub-Category Code</p>",
                },
                status: {
                    required: "<p class='error-text'>Status required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script type="text/javascript">
    $('select').select2();
</script>