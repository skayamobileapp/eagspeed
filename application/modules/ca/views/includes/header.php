<!DOCTYPE html>
<html lang="en"> 
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $pageTitle; ?></title>
    <link rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/css/main.css">
    <link rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/css/datatable.min.css">
    <link rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/select2/css/select2.css">
    <link rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/select2/css/select2-bootstrap.min.css"
    >

</head>
<body>
    <header class="navbar navbar-default navbar-fixed-top main-header">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Financial Management</a>
            </div>

            <nav class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    
                    <li class="active"><a href="/procurement/procurementCategory/list">Procurement</a></li>
                    <li><a href="/asset/assetCategory/list">Asset</a></li>
                    <li><a href="/ca/assetCategory/list">Cash Advance</a></li>
                    <li><a href="/glsetup/financialYear/list">GL Setup</a></li>
                    <li><a href="/budget/budgetYear/list">Budget</a></li>
                    <li><a href="/investment/investmentType/list">Investment</a></li>
                    <!-- <li><a href="/ap/billRegistration/list">AP</a></li> -->
                    <li><a href="/setup/user/logout">Logout</a></li>
                </ul>
                
            </nav>
        </div>
    </header>
</body>

     