<?php $this->load->helper("form"); ?>
<form id="form_pr_entry" action="" method="post">
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>View Tender Quotation Awarded</h3>
            </div>
            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>PR Number <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $tenderQuotationMaster->pr_number;?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>PR Description <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $tenderQuotationMaster->pr_description;?>" readonly="readonly">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>PR Entry Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="pr_entry_date" name="pr_entry_date" value="<?php echo date('d-m-Y',strtotime($tenderQuotationMaster->pr_entry_date));?>" readonly="readonly">
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Quotation Number <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $tenderQuotationMaster->quotation_number;?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Tender Description <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $tenderQuotationMaster->description;?>" readonly="readonly">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Tender Entry Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="pr_entry_date" name="pr_entry_date" value="<?php echo date('d-m-Y',strtotime($tenderQuotationMaster->created_dt_tm));?>" readonly="readonly">
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Financial Year <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $tenderQuotationMaster->financial_year;?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Vendor <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $tenderQuotationMaster->vendor_code . ' - ' . $tenderQuotationMaster->vendor_name; ?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Department <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $tenderQuotationMaster->department_code . ' - ' . $tenderQuotationMaster->department_name;?>" readonly="readonly">
                    </div>
                </div>

            </div>

            <div class="row">

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Type <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $tenderQuotationMaster->type;?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Total Amount <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $tenderQuotationMaster->amount;?>" readonly="readonly">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Tender Start Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="start_date" name="start_date" value="<?php echo date('d-m-Y',strtotime($tenderQuotationMaster->start_date));?>" readonly="readonly">
                    </div>
                </div>

            </div>

            <div class="row">


                 <div class="col-sm-4">
                        <div class="form-group">
                            <label>Tender Close Date <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="end_date" name="end_date" value="<?php echo date('d-m-Y',strtotime($tenderQuotationMaster->end_date));?>" readonly="readonly">
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Tender Opening Date <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="opening_date" name="opening_date" value="<?php echo date('d-m-Y',strtotime($tenderQuotationMaster->opening_date));?>" readonly="readonly">
                        </div>
                    </div>

                </div>

          <hr>

        <h3>Tender Specification Details</h3>

        <div class="custom-table">
          <table class="table">
            <thead>
                 <tr>
                     <th>Sl. No</th>
                     <th>Visit Location</th>
                     <th>Visit Date</th>
                     <th>Period</th>
                     <th>Period Type</th>
                      <th>CREDIT GL CODE</th>
                     <th>DEBIT GL CODE</th>
                     <th>Category</th>
                     <th>Sub Category</th>
                     <th>Item</th>
                     <th>Tax</th>
                     <th>Qty</th>
                     <th>Price</th>
                     <th>Tax Amount</th>
                     <th>Final Total</th>
                 </tr>
            </thead>
            <tbody>
                 <?php 
                  $total = 0;
                 for($i=0;$i<count($tenderQuotationDetails);$i++)
                    { 
                    // echo "<Pre>";print_r($tenderQuotationDetails[$i]);exit();

                        ?>
                    <tr>
                    <td><?php echo $i+1;?></td>
                    <td><?php echo $tenderQuotationDetails[$i]->visit_location;?></td>
                    <td><?php echo $tenderQuotationDetails[$i]->visit_date_time;?></td>
                    <td><?php echo $tenderQuotationDetails[$i]->period;?></td>
                    <td><?php echo $tenderQuotationDetails[$i]->period_type;?></td>
                    <td><?php echo $tenderQuotationDetails[$i]->cr_account . " - " . $tenderQuotationDetails[$i]->cr_activity . " - " . $tenderQuotationDetails[$i]->cr_department . " - " . $tenderQuotationDetails[$i]->cr_fund;?></td>
                    <td><?php echo $tenderQuotationDetails[$i]->dt_account . " - " . $tenderQuotationDetails[$i]->dt_activity . " - " . $tenderQuotationDetails[$i]->dt_department . " - " . $tenderQuotationDetails[$i]->dt_fund;?></td>
                    <td><?php echo $tenderQuotationDetails[$i]->category_code . " - " . $tenderQuotationDetails[$i]->category_name; ?></td>
                    <td><?php echo $tenderQuotationDetails[$i]->sub_category_code . " - " . $tenderQuotationDetails[$i]->sub_category_name;?></td>
                    <td><?php echo $tenderQuotationDetails[$i]->item_code . " - " . $tenderQuotationDetails[$i]->item_name;?></td>
                    <td><?php echo $tenderQuotationDetails[$i]->tax_code . " - " . $tenderQuotationDetails[$i]->tax_name;?></td>
                    <td><?php echo $tenderQuotationDetails[$i]->quantity;?></td>
                    <td><?php echo $tenderQuotationDetails[$i]->price;?></td>
                    <td><?php echo $tenderQuotationDetails[$i]->tax_price;?></td>
                    <td><?php echo $tenderQuotationDetails[$i]->total_final;?></td>

                     </tr>
                  <?php 
                  $total = $total + $tenderQuotationDetails[$i]->total_final;
                }
                $total = number_format($total, 2, '.', ',');
                ?>

                <tr>
                    <td bgcolor="" colspan="13"></td>
                    <td bgcolor=""><b> Total : </b></td>
                    <td bgcolor=""><b><?php echo $total; ?></b></td>
                </tr>

            </tbody>
        </table>
      </div>







        <br>
        <h3>Tender Comitee Details</h3>
        
        <div class="custom-table">
          <table class="table">
            <thead>
                 <tr>
                     <th>Sl. No</th>
                     <th>Staff Name</th>
                     <th>Description</th>
                 </tr>
            </thead>
            <tbody>
                 <?php 
                 for($i=0;$i<count($tenderComiteeDetails);$i++)
                    { 
                    // echo "<Pre>";print_r($tenderQuotationDetails[$i]);exit();

                        ?>
                    <tr>
                    <td><?php echo $i+1;?></td>
                    <td><?php echo $tenderComiteeDetails[$i]->ic_no . " - " . $tenderComiteeDetails[$i]->salutation . " - " . $tenderComiteeDetails[$i]->staff_name; ?></td>
                    <td><?php echo $tenderComiteeDetails[$i]->comitee_description;?></td>

                     </tr>
                  <?php 
                }
                ?>

            </tbody>
        </table>
      </div>



        <br>
        <h3>Tender Remarks Details</h3>

        <div class="custom-table">
          <table class="table">
            <thead>
                 <tr>
                     <th>Sl. No</th>
                     <th>Remarks</th>
                 </tr>
            </thead>
            <tbody>
                 <?php 
                 for($i=0;$i<count($tenderRemarksDetails);$i++)
                    { 
                    // echo "<Pre>";print_r($tenderQuotationDetails[$i]);exit();

                        ?>
                    <tr>
                    <td><?php echo $i+1;?></td>
                    <td><?php echo $tenderRemarksDetails[$i]->remarks;?></td>

                     </tr>
                  <?php
                }
                ?>

            </tbody>
        </table>



        <br>
        <h3>Vendor Awarded For Tender</h3>
        

        <div class="custom-table">
          <table class="table">
            <thead>
                 <tr>
                     <th>Sl. No</th>
                     <th>Vendor Name</th>
                     <th>Description</th>
                 </tr>
            </thead>
            <tbody>
                 <?php 
                 for($i=0;$i<count($tenderSubmissionForTendor);$i++)
                    { 
                    // echo "<Pre>";print_r($tenderQuotationDetails[$i]);exit();

                        ?>
                    <tr>
                    <td><?php echo $i+1;?></td>
                    <td><?php echo $tenderSubmissionForTendor[$i]->vendor_code . " - " . $tenderSubmissionForTendor[$i]->vendor_name;?></td>
                    <td><?php echo $tenderSubmissionForTendor[$i]->description;?></td>

                     </tr>
                  <?php
                }
                ?>

            </tbody>
        </table>


            <div class="button-block clearfix">
                <div class="bttn-group">
                    <a href="../list" class="btn btn-link">Back</a>
                </div>
            </div>
        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>