<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class StudentRecord extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('student_record_model');
        $this->isPartnerUniversityLoggedIn();
    }

    function list()
    {
        $id_partner_university = $this->session->id_partner_university;

        $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
        $formData['name'] = $this->security->xss_clean($this->input->post('name'));
        $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));
        $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
        $formData['id_partner_university'] = $id_partner_university;
        $formData['status'] = '';

        $data['searchParam'] = $formData;

        $data['mainInvoiceList'] = $this->student_record_model->applicantList($formData);
        // echo "<Pre>";print_r($data['mainInvoiceList']);exit;
        // print_r($data['mainInvoiceList']);exit();
        $data['programmeList']= $this->student_record_model->programmeListByStatus('');
        $data['intakeList']= $this->student_record_model->intakeListByStatus('1');
        $this->global['pageTitle'] = 'Campus Management System : List Students';
        $this->loadViews("student_record/list", $this->global, $data, NULL);

    }
    
    
    function view($id)
    {
        $data['getStudentData'] = $this->student_record_model->getStudentByStudentId($id);
        $data['studentDetails'] = $data['getStudentData'];

        $data['barrReleaseByStudentId'] = $this->student_record_model->barrReleaseByStudentId($id);
        $data['courseWithdrawByStudentId'] = $this->student_record_model->getCourseWithdrawByStudentId($id);
        $data['creditTransferByStudentId'] = $this->student_record_model->getCreditTransferByStudentId($id);

        $data['studentHasProgramme'] = $this->student_record_model->getStudentHasProgrammeByIdStudent($id);
        
        $data['courseRegisteredLandscape'] = $this->student_record_model->getCourseRegisteredLandscapeByLandscapeId($data['studentDetails']->id_program_landscape,$id);
        $data['otherDocuments'] = $this->student_record_model->getOtherDocuments($id);

        // $data['courseCompletedDetails'] = $this->student_record_model->courseCompletedDetails($data['studentDetails']->id_program,$data['studentDetails']->id_intake,$id);


        $data['organisationDetails'] = $this->student_record_model->getOrganisation();


        $data['courseRegisteredByStudentId'] = $this->student_record_model->getCourseRegisteredByStudentId($data['studentDetails']->id_program,$data['studentDetails']->id_intake,$id);


        $data['landscapeCount'] = $this->student_record_model->getLandscapeCount($data['studentDetails']->id_program,$data['studentDetails']->id_intake);
        $data['landscapeRegisteredCount'] = $this->student_record_model->getLandscapeRegisteredCount($data['studentDetails']->id_program,$data['studentDetails']->id_intake,$id);

        $data['studentStatus'] = $this->student_record_model->studentStatus($id);
        $data['applyChangeStatusListByStudentId'] = $this->student_record_model->applyChangeStatusListByStudentId($id);
        $data['courseRegisteredList'] = $this->student_record_model->courseRegisteredList($id);

        // echo "<Pre>";print_r($data['studentHasProgramme']);exit;

        $this->global['pageTitle'] = 'Campus Management System : View Student Account Statements';
        $this->loadViews("student_record/view", $this->global, $data, NULL);
    }

    function addStudentNote()
    {
        $user_id = $this->session->userId; 

        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        // echo "<Pre>";print_r($tempData);exit;

        if($tempData['note'] != '')
        {
            $tempData['created_by'] = $user_id;
            $inserted_id = $this->student_record_model->addStudentNote($tempData);
            $data = $this->displayStudentNote($tempData['id_student']);
        }else
        {
        // echo "<Pre>";print_r($tempData);exit;
            $data = $this->displayStudentNote($tempData['id_student']);
        }
        echo $data;exit();       
    }

    function displayStudentNote($id_student)
    {        
        $temp_details = $this->student_record_model->getStudentNote($id_student); 
        // echo "<Pre>";print_r($id_student);exit;
        if(!empty($temp_details))
        {

        $table = "
        <div class='row'>
            <div class='col-sm-2'>
                <div class='form-group'>
                </div>
            </div>
        <div class='col-sm-8'>";




                           
    $table.= "

    <div class='form-container'>
        <h4 class='form-group-title'>Student Note Details</h4>

        <table  class='table' id='list-table'>
                    <tr>
                    <th>Sl. No</th>
                    <th class='text-center'>Note</th>
                    <th class='text-center'>Action</th>
                    </tr>";
                $total_amount = 0;
                    for($i=0;$i<count($temp_details);$i++)
                    {
                    $id = $temp_details[$i]->id;
                    $note = $temp_details[$i]->note;
                    $user = $temp_details[$i]->user;
                    $created_dt_tm = date('h:i:s A ( d-m-Y )', strtotime($temp_details[$i]->created_dt_tm));

                    $j = $i+1;
                    
                        $table .= "
                        <tr>
                            <td>$j</td>
                            <td>$note
                            <br>
                            <br>
                            <p style='text-align: left;'>
                            Created By - <b> $user </b>, Created On - <b> $created_dt_tm </b></p>
                            </td>                          
                            <td style='text-align: center;'>
                                <a onclick='deleteStudentNote($id)'>Delete</a>
                            </td>
                        </tr>";
                    }

                    $table .= "";

        $table.= "</table>
            </div>";

         $table.= "   
                </div>
                <div class='col-sm-2'>
                    <div class='form-group'>
                    </div>
                </div>
            </div>";


        }
        else
        {
            $table= "";
        }
        return $table;
    }

    function deleteStudentNote()
    {
        // echo "<Pre>";  print_r($id);exit;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $id = $tempData['id'];
        $id_student = $tempData['id_student'];

        $inserted_id = $this->student_record_model->deleteStudentNote($id);
        $data = $this->displayStudentNote($id_student);

        echo $data;exit(); 
    }

    
    function generateDeliverableReport($id_student)
    {
        // To Get Mpdf Library
        $this->getMpdfLibrary();

        // print_r($id_student);exit;
            
        $mpdf=new \Mpdf\Mpdf(); 

        $currentDate = date('d-m-Y');
        $currentTime = date('H:i:s');

        $organisationDetails = $this->student_record_model->getOrganisation();

        // echo "<Pre>";print_r($organisationDetails);exit;
        

        $signature = $_SERVER['DOCUMENT_ROOT']."/assets/images/logo.png";

        if($organisationDetails->image != '')
        {
            $signature = $_SERVER['DOCUMENT_ROOT']."/assets/images/" . $organisationDetails->image;
        }




        $student_data = $this->student_record_model->getStudentByStudentId($id_student);



        // echo "<Pre>";print_r($student_data);exit;


        $student_name = $student_data->full_name;
        $nric = $student_data->nric;
        $learning_mode = $student_data->program_scheme;
        $scheme_name = $student_data->scheme_name;
        $scheme_code = $student_data->scheme_code;
        $branch_name = $student_data->branch_name;
        $branch_code = $student_data->branch_code;
        $partner_university_code = $student_data->partner_university_code;
        $partner_university_name = $student_data->partner_university_name;
        $race = $student_data->race;
        $qualification = $student_data->qualification;
        $programme_code = $student_data->programme_code;
        $programme_name = $student_data->programme_name;
        $intake_name = $student_data->intake_name;

        // if($date_time)
        // {
        //     $date_time = date('d-m-Y', strtotime($date_time));
        // }


            $file_data = "";


            $file_data.="<table align='center' width='100%'>
                <tr>
                          <td style='text-align: left' width='20%' ><b>DELIVERABLES</b></td>
                          <td style='text-align: center' width='30%' ></td>

                  <td style='text-align: right' width='40%' ><img src='$signature' width='120px' /></td>
                  
                </tr>
               
                
                <tr>
                  <td style='text-align: center' width='100%'  colspan='3'></td>
                </tr>
            </table>";

            $file_data = $file_data ."

            <table width='100%'>
                <tr>
                 <td>Student Name : $student_name </td>
                 <td></td>
                 <td>NRIC: $nric</td>
                 <td></td>
                </tr>
                <tr>
                 <td>Intake:  $intake_name</td>
                 <td></td>
                 <td>Programme: $programme_name</td>
                 <td></td>
                </tr>
             </table>


             <h4><p>Deliverables of the 4-Year PhD Journey to be Reported at the End of Every 6-Month by the Candidate in Consultation with the Supervisor.</p></h4>





             <table width='100%' border='1' style='margin-top:20px;border-collapse: collapse;'>
              <tr>
               <td><b></b></td>
               <td><b>Deliverables</b></td>
               <td><b>My Targeted Deadline</b></td>
               <td><b>My Progress This Far</b></td>
               <td><b>Supervisor’s Comments</b></td>
              </tr>";

        $deliverables = $this->student_record_model->getDeliverablesDurationsByIdStudent($id_student);

        $deliverables_first = $this->student_record_model->getDeliverablesStartingByIdStudent($id_student);


        // echo "<Pre>";print_r($deliverables_first);exit;


        $i=1;
        foreach ($deliverables as $value)
        {

            $chapter = $value->chapter;
            $topics = $value->topics;

            // $amount = number_format($amount, 2, '.', ',');

            // $acqDate   = date("d/m/Y", strtotime($acqDate));

            if($deliverables_first)
            {
                $first_deliverable = $deliverables_first->new_deliverable_term;
            }else
            {
                $first_deliverable = '-';
            }

            if($first_deliverable)
            {
                $month = substr($first_deliverable, 0,3);
                $year = substr($first_deliverable, -4);

            // echo "<Pre>";print_r($year);exit;
            
                // if($first_deliverable )


                $one_year = $year+1;
                $two_year = $year+2;
                $three_year = $year+3;
                $four_year = $year+4;



                if($month == 'Dec')
                {
                    $second_deliverable = 'Jun'. '-' . $one_year;
                    // echo "<Pre>";print_r($second_deliverable);exit;
                    $third_deliverable = 'Dec' . $one_year;
                    $fourth_deliverable = 'Jun' . $two_year;
                    $fifth_deliverable = 'Dec' . $two_year;
                    $sixth_deliverable = 'Jun' . $three_year;
                    $seventh_deliverable = 'Dec' . $three_year;
                    $eighth_deliverable = 'Jun' . $four_year;
                }
                elseif($month == 'Jun')
                {
                    $second_deliverable = 'Dec'. '-' . $year;
                    $third_deliverable = 'Jun' . $one_year;
                    $fourth_deliverable = 'Dec' . $one_year;
                    $fifth_deliverable = 'Jun' . $two_year;
                    $sixth_deliverable = 'Dec' . $two_year;
                    $seventh_deliverable = 'Jun' . $three_year;
                    $eighth_deliverable = 'Dec' . $three_year;
                }
            
            }

            switch($i)
            {
                case 1:
                $file_data = $file_data ."
               <tr>
                   <td><b>Starting</b><br>( $first_deliverable )</td>
                   <td><b>Intent of Research</b></td>
                   <td></td>
                   <td></td>
                   <td></td>
               </tr>
               ";
                break;
            }

            
            $file_data = $file_data ."
               <tr>
                   <td valign='top'> ";

                   switch ($i)
                   {
                       case 1:
                        $file_data = $file_data ."First 
                        ";   
                        break;

                       case 2:
                        $file_data = $file_data ."Second 
                        ";   
                        break;

                       case 3:
                        $file_data = $file_data ."Third 
                        ";   
                        break;


                       case 4:
                        $file_data = $file_data ."Fourth 
                        ";   
                        break;


                       case 5:
                        $file_data = $file_data ."Fifth 
                        ";   
                        break;


                       case 6:
                        $file_data = $file_data ."Sixth 
                        ";   
                        break;


                       case 7:
                        $file_data = $file_data ."Seventh 
                        ";   
                        break;

                       case 8:
                        $file_data = $file_data ."Eighth 
                        ";   
                        break;
                       
                       default:
                           # code...
                           break;
                   }

                   $file_data = $file_data ."
                    6 months
                       </td>
                       <td>$chapter
                    ";
                

               
               if(!empty($topics))
               {

                foreach ($topics as $topic_value)
                {
                    $topic = $topic_value->topic;

                // echo "<Pre>";print_r($topic);exit;
                    $file_data = $file_data ."
                    <br>
                     &nbsp;&nbsp;&nbsp;&nbsp; &#8729; $topic
                    ";
                }
               
               }

                $file_data = $file_data ."
                        </td>
                   <td valign='top' style='text-align: center'>";


                switch ($i)
                   {
                       case 1:
                        $file_data = $file_data .  " " . $first_deliverable;
                        break;

                       case 2:
                        $file_data = $file_data . " " . $second_deliverable;;   
                        break;

                       case 3:
                        $file_data = $file_data . " " . $third_deliverable;   
                        break;


                       case 4:
                        $file_data = $file_data . " " . $fourth_deliverable;   
                        break;


                       case 5:
                        $file_data = $file_data . " " . $fifth_deliverable;   
                        break;


                       case 6:
                        $file_data = $file_data . " " . $sixth_deliverable;   
                        break;


                       case 7:
                        $file_data = $file_data . " " . $seventh_deliverable;   
                        break;

                       case 8:
                        $file_data = $file_data . " " . $eighth_deliverable;   
                        break;
                       
                       default:
                           # code...
                           break;
                   }




                $file_data = $file_data ."

                   </td>
                   <td></td>
                   <td style='text-align:right;'></td>
               </tr>";
          $i++;

        }


    //     $amount_c = $invoice_total;

    // $invoice_total = number_format($invoice_total, 2, '.', ',');
    // $amount_word = $this->getAmountWordings($amount_c);

    // $amount_word = ucwords($amount_word);


    // $file_data = $file_data ."

    //     <tr>
    //        <td></td>
    //        <td style='text-align:center;'><b>TOTAL</b></td>
    //        <td></td>
    //        <td style='text-align:right;'><b>$invoice_total</b></td>
    //     </tr>


    //     <tr>
    //        <td colspan='3' style='text-align:center;'><b>GRAND TOTAL</b></td>
    //        <td style='text-align:right;'><b>$invoice_total</b></td>
    //     </tr>

    //     <tr>
    //        <td colspan='4' style='text-align:center;'><b>$currency : $amount_word</b></td>
    //     </tr>";

            $file_data = $file_data ."
               </table>";



        // $bankDetails = $this->student_record_model->getBankRegistration();


        // if($bankDetails && $organisationDetails)
        // {
        //     $bank_name = $bankDetails->name;
        //     $bank_code = $bankDetails->code;
        //     $account_no = $bankDetails->account_no;
        //     $state = $bankDetails->state;
        //     $country = $bankDetails->country;
        //     $address = $bankDetails->address;
        //     $city = $bankDetails->city;
        //     $zipcode = $bankDetails->zipcode;
            

        //     $organisation_name = $organisationDetails->name;




             $file_data = $file_data ."
    <p> 1. This is auto generated Document. No signature is required : </p>
      ";


        // }

      $file_data = $file_data ."
    <pagebreak>";

    
        // echo "<Pre>";print_r($file_data);exit;


            // $mpdf->SetFooter('<div>Campus Management System</div>');
            // echo $file_data;exit;

            $mpdf->WriteHTML($file_data);
            $mpdf->Output($nric . '_DELIVERABLES_'.$currentDate . '_' . $currentTime.'.pdf', 'D');
            exit;
    }
}