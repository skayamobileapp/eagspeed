<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Program Landscape</h3>
        </div>


        <br>


        <div class="topnav">
          <a href="<?php echo '/cm/programmeLandscape/edit/' . $programmeLandscapeDetails->id .'/' . $programmeLandscapeDetails->id_programme; ?>" style="background: #aaff00" >Landscape Info</a> |
          <a href="<?php echo '/cm/programmeLandscape/programObjective/' . $programmeLandscapeDetails->id .'/' . $programmeLandscapeDetails->id_programme.'/' . $programmeLandscapeDetails->id_intake; ?>">Program Objective</a> |
          <a href="<?php echo '/cm/programmeLandscape/programLearningMode/' . $programmeLandscapeDetails->id .'/' . $programmeLandscapeDetails->id_programme .'/' . $programmeLandscapeDetails->id_intake; ?>" >Programme Learning Mode</a> | 
          <a href="<?php echo '/cm/programmeLandscape/subjectRegistration/' . $programmeLandscapeDetails->id .'/' . $programmeLandscapeDetails->id_programme; ?>">Subject Registration</a> 

          <!-- <a href="<?php echo '../../editProgramRequirementTab/' . $programmeLandscapeDetails->id .'/' . $programmeLandscapeDetails->id_programme; ?>">Program Requirement</a> | 
          <a href="<?php echo '../../editCourseTab/' . $programmeLandscapeDetails->id .'/' . $programmeLandscapeDetails->id_programme .'/' . $programmeLandscapeDetails->id_intake; ?>">Course</a>

          <?php

            if($programme->mode == 0)
            {

            ?>

          |
          <a href="<?php echo '../../addLearningMode/' . $programmeLandscapeDetails->id .'/' . $programmeLandscapeDetails->id_programme .'/' . $programmeLandscapeDetails->id_intake; ?>">Learning Mode</a>


          <?php

            }
            
            ?> -->


        </div>


        <br>


        <div class="form-container">
            <h4 class="form-group-title">Program Details</h4>

            

          <div class="row">

            <div class="col-sm-4">
                <div class="form-group">
                    <label>Name <span class='error-text'>*</span></label>
                    <input type="text" class="form-control" id="name1" name="name1" value="<?php echo $programme->name; ?>" readonly="readonly">
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group">
                    <label>Code <span class='error-text'>*</span></label>
                    <input type="text" class="form-control" id="code" name="code" value="<?php echo $programme->code; ?>" readonly="readonly">
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group">
                    <label>Name In Other Language <span class='error-text'>*</span></label>
                    <input type="text" class="form-control" id="name_optional_language" name="name_optional_language" value="<?php echo $programme->name_optional_language; ?>" readonly="readonly">
                </div>
            </div>
            
          </div>



          <div class="row">

            <div class="col-sm-4">
                <div class="form-group">
                    <label>Educatoin Level <span class='error-text'>*</span></label>
                    <input type="text" class="form-control" id="name1" name="name1" value="<?php echo $programme->education_level_name; ?>" readonly="readonly">
                </div>
            </div>


          </div>

        </div>

        <br>





        <br>

        <form id="form_programme_landscape" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Programme Landscape Details</h4>

            <div class="row">

                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Program Landscape Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $programmeLandscapeDetails->name; ?>">
                    </div>
                </div>




                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Program Landscape Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="landscape_code" name="landscape_code" value="<?php echo $programmeLandscapeDetails->code; ?>">
                    </div>
                </div>



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select From Intake <span class='error-text'>*</span></label>
                        <select name="id_intake" disabled="disabled" id="id_intake" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($intakeList))
                            {
                                foreach ($intakeList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>"
                                        <?php 
                                        if($record->id == $programmeLandscapeDetails->id_intake)
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->year . " - " . $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select To Intake </label>
                        <select name="id_intake_to" id="id_intake_to" class="form-control"
                        <?php

                        if($programmeLandscapeDetails->intake_year != 0 && $programmeLandscapeDetails->intake_year <= date('Y'))
                        {
                            echo 'disabled';
                        }
                        ?>
                        >
                            <option value="">Select</option>
                            <?php
                            if (!empty($intakeList))
                            {
                                foreach ($intakeList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>"
                                        <?php 
                                        if($record->id == $programmeLandscapeDetails->id_intake_to)
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->year . " - " . $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


               <!--  <?php
                if($programme->mode == 1)
                {
                ?>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Program Learning Mode <span class='error-text'>*</span></label>
                        <select name="learning_mode" id="learning_mode" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($programmelearningMode))
                            {
                                foreach ($programmelearningMode as $record)
                                {?>
                                    <option value="<?php echo $record->id;?>"
                                    <?php if($programmeLandscapeDetails->learning_mode==$record->id){ echo "selected"; } ?>

                                    ><?php echo $record->mode_of_program . " - " . $record->mode_of_study;?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


                <?php
                }
                ?>
 -->

            

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Program Landscape Type <span class='error-text'>*</span></label>
                        <select name="program_landscape_type" id="program_landscape_type" class="form-control" onchange="getProgramLandscapeType(this.value)">
                            <option value="">Select</option>
                            <option value="Block"
                            <?php
                            if ($programmeLandscapeDetails->program_landscape_type == 'Block')
                            {
                                echo 'selected';
                            }
                            ?>
                            >Block</option>
                            <option value="Level"
                            <?php
                            if ($programmeLandscapeDetails->program_landscape_type == 'Level')
                            {
                                echo 'selected';
                            }
                            ?>
                            >Level</option>
                            <option value="Semester"
                            <?php
                            if ($programmeLandscapeDetails->program_landscape_type == 'Semester')
                            {
                                echo 'selected';
                            }
                            ?>
                            >Semester</option>
                        </select>
                    </div>
                </div>



            <!-- </div>

            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Program Scheme <span class='error-text'>*</span></label>
                        <select name="program_scheme" id="program_scheme" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($programmeSchemeList))
                            {
                                foreach ($programmeSchemeList as $record)
                                {?>
                                    <option value="<?php echo $record->id;?>"
                                    <?php if($programmeLandscapeDetails->program_scheme==$record->id){ echo "selected"; } ?>

                                    ><?php echo $record->description;?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div> -->



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Total Cr. Hrs <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="min_total_cr_hrs" name="min_total_cr_hrs" value="<?php echo $programmeLandscapeDetails->min_total_cr_hrs;?>">
                    </div>
                </div>


                


            <!--     <div class="col-sm-4">
                    <div class="form-group">
                        <label>Min. Repeat Hrs <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="min_repeat_course" name="min_repeat_course"  value="<?php echo $programmeLandscapeDetails->min_repeat_course;?>">
                    </div>
                </div>


            </div>

            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Max. Repeat Exams <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="max_repeat_exams" name="max_repeat_exams" value="<?php echo $programmeLandscapeDetails->max_repeat_exams;?>">
                    </div>
                </div>


               
            

 
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Total Blocks <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="total_block" name="total_block" value="<?php echo $programmeLandscapeDetails->total_block;?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Total Level <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="total_level" name="total_level" value="<?php echo $programmeLandscapeDetails->total_level;?>">
                    </div>
                </div> -->

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Total <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="total_semester" name="total_semester" value="<?php echo $programmeLandscapeDetails->total_semester;?>">
                    </div>
                </div>


            <!-- </div>

            <div class="row">




                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Minimum Total Score <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="min_total_score" name="min_total_score" value="<?php echo $programmeLandscapeDetails->min_total_score;?>">
                    </div>
                </div>
                
                


           

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Min Pass Subject <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="min_pass_subject" name="min_pass_subject" value="<?php echo $programmeLandscapeDetails->min_pass_subject;?>">
                    </div>
                </div>
            

            </div>

            
            <div class="row"> -->

                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" <?php if($programmeLandscapeDetails->status=='1') {
                                 echo "checked=checked";
                              };?>><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0" <?php if($programmeLandscapeDetails->status=='0') {
                                 echo "checked=checked";
                              };?>>
                              <span class="check-radio"></span> In-Active
                            </label>                              
                        </div>                         
                </div>

            </div>

        </div>

         



        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="<?php echo '../../programmeLandscapeList/' . $programmeLandscapeDetails->id_programme; ?>" class="btn btn-link">Cancel</a>
            </div>
        </div>


    </form>


    <form id="form_details" action="" method="post">
        <div class="form-container" style="display: none;">
        <h4 class="form-group-title">Semester Course Registration Info</h4>

            <div class="row">

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Semester Type <span class='error-text'>*</span></label>
                        <select name="semester_type" id="semester_type" class="form-control">
                            <option value="">Select</option>
                            <option value="Short Semester">Short Semester</option>
                            <option value="Long Semester">Long Semester</option>
                        </select>
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Registration Rule <span class='error-text'>*</span></label>
                        <select name="registration_rule" id="registration_rule" class="form-control">
                            <option value="">Select</option>
                            <option value="No Of Courses">No Of Courses</option>
                            <option value="No Of Credit Hours">No Of Credit Hours</option>
                        </select>
                    </div>
                </div>

                <div class="col-sm-3">
                        <div class="form-group">
                            <label>Minimum <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="minimum" name="minimum">
                        </div>
                </div>

                 <div class="col-sm-3">
                        <div class="form-group">
                            <label>Maximum <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="maximum" name="maximum">
                        </div>
                </div>



            </div>


            <div class="row">
              
                <div class="col-sm-4">
                    <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="saveData()">Add</button>
                </div>
            </div>


            <?php

            if(!empty($programLandscapeHasSemesterList))
            {
                ?>

                <div class="form-container">
                        <h4 class="form-group-title">Semester Info. Details</h4>

                    

                      <div class="custom-table">
                        <table class="table">
                            <thead>
                                <tr>
                                <th>Sl. No</th>
                                 <th>Semester Type</th>
                                 <th>Registration Rule</th>
                                 <th class="text-center">Min | Max</th>
                                 <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                 <?php
                             $total = 0;
                              for($i=0;$i<count($programLandscapeHasSemesterList);$i++)
                             { ?>
                                <tr>
                                <td><?php echo $i+1;?></td>
                                <td><?php echo $programLandscapeHasSemesterList[$i]->semester_type;?></td>
                                <td><?php echo $programLandscapeHasSemesterList[$i]->registration_rule;?></td>
                                <td class="text-center"><?php echo $programLandscapeHasSemesterList[$i]->minimum . " | " . $programLandscapeHasSemesterList[$i]->maximum;?></td>
                                <td class="text-center">

                                <a onclick="deleteSemesterInfo(<?php echo $programLandscapeHasSemesterList[$i]->id; ?>)">Delete</a>
                                </td>

                                 </tr>
                              <?php 
                          } 
                          ?>
                            </tbody>
                        </table>
                      </div>

                    </div>




            <?php
            
            }
             ?>

        </div>
    </form>




    <form id="form_credit_hr_details" action="" method="post">
        <div class="form-container">
        <h4 class="form-group-title">Course Credit Hour Info</h4>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Course Type <span class='error-text'>*</span></label>
                        <select name="id_landscape_course_type" id="id_landscape_course_type" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($landscapeCourseTypeList))
                            {
                                foreach ($landscapeCourseTypeList as $record)
                                {?>
                                    <option value="<?php echo $record->id;?>">
                                        <?php echo $record->code . " - " .  $record->name;?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


                 <div class="col-sm-3">
                        <div class="form-group">
                            <label>Hours <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="hours" name="hours">
                        </div>
                </div>



            </div>


            <div class="row">
              
                <div class="col-sm-4">
                    <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="addCourseCreditHoursDetails()">Add</button>
                </div>
            </div>



            <?php

            if(!empty($getCreditHourDetailsByProgramLandscapeId))
            {
                ?>

                <div class="form-container">
                        <h4 class="form-group-title">Course Credit Hour Details</h4>

                    

                      <div class="custom-table">
                        <table class="table">
                            <thead>
                                <tr>
                                <th>Sl. No</th>
                                 <th>Course Type</th>
                                 <th>Hours</th>
                                 <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                 <?php
                             $total = 0;
                              for($i=0;$i<count($getCreditHourDetailsByProgramLandscapeId);$i++)
                             { ?>
                                <tr>
                                <td><?php echo $i+1;?></td>
                                <td><?php echo $getCreditHourDetailsByProgramLandscapeId[$i]->course_type_code . " - " . $getCreditHourDetailsByProgramLandscapeId[$i]->course_type_name;?></td>
                                <td><?php echo $getCreditHourDetailsByProgramLandscapeId[$i]->hours;?></td>
                                <td class="text-center">

                                <a onclick="deleteCourseCreditHour(<?php echo $getCreditHourDetailsByProgramLandscapeId[$i]->id; ?>)">Delete</a>
                                </td>

                                 </tr>
                              <?php 
                          } 
                          ?>
                            </tbody>
                        </table>
                      </div>

                    </div>




            <?php
            
            }
             ?>


        </div>
    </form>







        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    $('select').select2();


    function getProgramLandscapeType(type)
    {
        // alert(type);
        // $("#show_type").val(type);
    }

    function saveData()
    {
        if($('#form_details').valid())
        {

        var tempPR = {};
        tempPR['semester_type'] = $("#semester_type").val();
        tempPR['registration_rule'] = $("#registration_rule").val();
        tempPR['minimum'] = $("#minimum").val();
        tempPR['maximum'] = $("#maximum").val();
        tempPR['id_program_landscape'] = <?php echo $id_program_landscape; ?>;
            $.ajax(
            {
               url: '/setup/programmeLandscape/semesterInfoAdd',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // alert(result);
                    location.reload();
               }
            });
        }
    }


    function deleteSemesterInfo(id) {
        // alert(id);
         $.ajax(
            {
               url: '/setup/programmeLandscape/deleteSemesterInfo/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    alert('Deleted Successfully');
                    location.reload();
               }
            });
    }


    function addCourseCreditHoursDetails()
    {
        if($('#form_credit_hr_details').valid())
        {

        var tempPR = {};
        tempPR['id_landscape_course_type'] = $("#id_landscape_course_type").val();
        tempPR['hours'] = $("#hours").val();
        tempPR['id_program_landscape'] = <?php echo $id_program_landscape; ?>;
        tempPR['status'] = <?php echo 1; ?>;
            $.ajax(
            {
               url: '/setup/programmeLandscape/addCourseCreditHoursDetails',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    location.reload();
               }
            });
        }
    }



    function deleteCourseCreditHour(id)
    {
        // alert(id);
         $.ajax(
            {
               url: '/setup/programmeLandscape/deleteCourseCreditHour/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    alert('Deleted Successfully');
                    location.reload();
               }
            });
    }


    $(document).ready(function() {
        $("#form_details").validate({
            rules: {
                semester_type: {
                    required: true
                },
                registration_rule: {
                    required: true
                },
                minimum: {
                    required: true
                },
                maximum: {
                    required: true
                }
            },
            messages: {
                semester_type: {
                    required: "<p class='error-text'>Select Semester Type</p>",
                },
                registration_rule: {
                    required: "<p class='error-text'>Select Registration Rule</p>",
                },
                minimum: {
                    required: "<p class='error-text'>Minimum Required</p>",
                },
                maximum: {
                    required: "<p class='error-text'>Maximum Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    $(document).ready(function() {
        $("#form_programme_landscape").validate({
            rules: {
                name:
                {
                    required: true
                },
                landscape_code: {
                    required:true
                },
                id_programme:
                {
                    required: true
                },
                id_intake:
                {
                    required: true
                },
                min_total_cr_hrs:
                {
                    required: true
                },
                min_repeat_course:
                {
                    required: true
                },
                max_repeat_exams:
                {
                    required: true
                },
                total_semester:
                {
                    required: true
                },
                total_block:
                {
                    required: true
                },
                total_level:
                {
                    required: true
                },
                min_total_score:
                {
                    required: true
                },
                min_pass_subject:
                {
                    required: true
                },
                program_scheme:
                {
                    required: true
                },
                program_landscape_type:
                {
                    required: true
                },
                learning_mode:
                {
                    required: true
                }
            },
            messages:
            {
                name: {
                    required: "<p class='error-text'>Program Landscape Name Required</p>",
                },
                landscape_code : {
                    required: "<p class='error-text'>Code Required</p>",
                },
                id_programme: {
                    required: "<p class='error-text'>Select Program</p>",
                },
                id_intake: {
                    required: "<p class='error-text'>Select Intake</p>",
                },
                min_total_cr_hrs: {
                    required: "<p class='error-text'>Enter Min Total Cr. Hours</p>",
                },
                min_repeat_course: {
                    required: "<p class='error-text'>Enter Repeat Course</p>",
                },
                max_repeat_exams: {
                    required: "<p class='error-text'>Enter Repeat Exams</p>",
                },
                total_semester: {
                    required: "<p class='error-text'>Enter Total Semester</p>",
                },
                total_block: {
                    required: "<p class='error-text'>Enter Total Block</p>",
                },
                total_level: {
                    required: "<p class='error-text'>Enter Total Level</p>",
                },
                min_total_score: {
                    required: "<p class='error-text'>Enter Min Total Score</p>",
                },
                min_pass_subject: {
                    required: "<p class='error-text'>Enter Minimum Pass Subject</p>",
                },
                program_scheme: {
                    required: "<p class='error-text'>Select Program Scheme</p>",
                },
                program_landscape_type: {
                    required: "<p class='error-text'>Select Program Landscape Type</p>",
                },
                learning_mode: {
                    required: "<p class='error-text'>Select Learning Mode</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });




    $(document).ready(function() {
        $("#form_credit_hr_details").validate({
            rules: {
                id_landscape_course_type: {
                    required: true
                },
                hours: {
                    required: true
                }
            },
            messages: {
                id_landscape_course_type: {
                    required: "<p class='error-text'>Select Course Type</p>",
                },
                hours: {
                    required: "<p class='error-text'>Hours Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });



</script>