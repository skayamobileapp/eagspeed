<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Program Has Fee Structure</h3>
        </div>
        <form id="form_programme_has_course" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Program Has Fee Structure Details</h4>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Scheme</label>
                        <input type="text" class="form-control" id="scheme" name="scheme" value="<?php echo $programmeHasFeeStructureDetails->scheme;?>">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Program *</label>
                        <select name="id_programme" id="id_programme" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($programmeList))
                            {
                                foreach ($programmeList as $record)
                                {?>
                                   <option value="<?php echo $record->id;  ?>"
                                        <?php 
                                        if($record->id == $programmeHasFeeStructureDetails->id_programme)
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Code</label>
                        <input type="text" class="form-control" id="code" name="code" value="<?php echo $programmeHasFeeStructureDetails->code;?>">
                    </div>
                </div>
            
            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Description</label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $programmeHasFeeStructureDetails->description;?>">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Intake *</label>
                        <select name="id_intake" id="id_intake" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($intakeList))
                            {
                                foreach ($intakeList as $record)
                                {?>
                                   <option value="<?php echo $record->id;  ?>"
                                        <?php 
                                        if($record->id == $programmeHasFeeStructureDetails->id_intake)
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Semester *</label>
                        <select name="id_semester" id="id_semester" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($semesterList))
                            {
                                foreach ($semesterList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>"
                                        <?php 
                                        if($record->id == $programmeHasFeeStructureDetails->id_semester)
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
            
            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Currency Type *</label>
                        <select name="currency" id="currency" class="form-control">
                             <option value="">Select</option>
                            <option value="<?php echo $programmeHasFeeStructureDetails->currency;?>"
                                <?php 
                                if ($programmeHasFeeStructureDetails->currency == 'RM')
                                {
                                    echo "selected=selected";
                                } ?>>
                                        <?php echo "RM";  ?>
                            </option>

                            <option value="<?php echo $programmeHasFeeStructureDetails->currency;?>"
                                <?php 
                                if ($programmeHasFeeStructureDetails->currency == 'Other')
                                {
                                    echo "selected=selected";
                                } ?>>
                                        <?php echo "Other";  ?>
                            </option>
                        </select>
                    </div>
                </div>



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Student Category *</label>
                        <select name="student_category" id="student_category" class="form-control">
                             <option value="">Select</option>
                            <option value="<?php echo $programmeHasFeeStructureDetails->student_category;?>"
                                <?php 
                                if ($programmeHasFeeStructureDetails->student_category == 'Bhumiputra')
                                {
                                    echo "selected=selected";
                                } ?>>
                                        <?php echo "Bhumiputra";  ?>
                            </option>

                            <option value="<?php echo $programmeHasFeeStructureDetails->student_category;?>"
                                <?php 
                                if ($programmeHasFeeStructureDetails->student_category == 'International')
                                {
                                    echo "selected=selected";
                                } ?>>
                                        <?php echo "International";  ?>
                            </option>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Effective Date *</label>
                        <input type="text" class="form-control" id="effective_date" name="effective_date" value="<?php echo date("d-m-Y", strtotime($programmeHasFeeStructureDetails->effective_date));?>">
                    </div>
                </div>   

            </div>

        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="../list" class="btn btn-link">Cancel</a>
            </div>
        </div>
            
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>
    $(document).ready(function() {
        $("#form_programme_has_course").validate({
            id_programme: {
                    required: true
                },
                id_programme_landscape: {
                    required: true
                },
                id_semester: {
                    required: true
                },
                id_course: {
                    required: true
                },
                type: {
                    required: true
                },
                effective_date: {
                    required: true
                }
            },
            messages: {
                id_programme: {
                    required: "Select Programme",
                },
                id_programme_landscape: {
                    required: "Select Program Landscape",
                },
                id_semester: {
                    required: "Select Semester",
                },
                id_course: {
                    required: "Select Course",
                },
                type: {
                    required: "Select Type",
                },
                effective_date: {
                    required: "Select Effective Date",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
