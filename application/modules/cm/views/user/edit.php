<?php
$userId = $userInfo->id;
$name = $userInfo->name;
$email = $userInfo->email;
$mobile = $userInfo->mobile;
$roleId = $userInfo->role_id;
?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit User</h3>
        </div>
        <form id="form" action="" method="post">
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Full Name</label>
                        <input type="text" class="form-control" id="fname" name="fname" value="<?php echo $name; ?>">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" class="form-control" id="email" name="email" value="<?php echo $email; ?>" readonly>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Mobile Number</label>
                        <input type="text" class="form-control" id="mobile" name="mobile" value="<?php echo $userInfo->mobile; ?>">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Role</label>
                        <select name="role" id="role" class="form-control" disabled>
                            <option value="">Select</option>
                            <?php
                            if (!empty($roles)) {
                                foreach ($roles as $record) {
                            ?>
                                    <option value="<?php echo $record->id;  ?>" 
                                        <?php if($record->id == $userInfo->role_id)
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->role;  ?></option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" class="form-control" id="password" name="password" value="<?php echo $userInfo->password; ?>" readonly>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Confirm Password</label>
                        <input type="password" class="form-control" id="cpassword" name="cpassword" value="<?php echo $userInfo->password; ?>" readonly>
                    </div>
                </div>
            </div>
            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="../list" class="btn btn-link">Cancel</a>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>
    $(document).ready(function() {
        $("#form").validate({
            rules: {
                fname: {
                    required: true
                },
                email: {
                    required: true,
                    email: true
                },
                mobile: {
                    required: true,
                    number: true
                },
                role: {
                    required: true,
                },
                password: {
                    required: true,
                },
                cpassword: {
                    required: true,
                    equalTo : "#password"
                }
            },
            messages: {
                fname: {
                    required: "<p class='error-text'>Full Name required</p>",
                },
                email: {
                    required: "<p class='error-text'>Email required</p>",
                },
                mobile: {
                    required: "<p class='error-text'>Mobile required</p>",
                    number: "<p class='error-text'>Please enter a valid phone number without +91 </p>"
                },
                role: {
                    required: "<p class='error-text'>Role required</p>",
                },
                password: {
                    required: "<p class='error-text'>Password required</p>",
                },
                cpassword: {
                    required: "<p class='error-text'>Confirm Password required</p>",
                    equalTo: "<p class='error-text'>Password Mismatch</p>"
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>