<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List Student Records</h3>
      <!-- <a href="add" class="btn btn-primary">+ Add Main Invoice</a> -->
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">
                

              <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Student</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="first_name" value="<?php echo $searchParam['first_name']; ?>">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-4 control-label">Email</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="email_id" value="<?php echo $searchParam['email_id']; ?>">
                      </div>
                    </div>
                     <div class="form-group">
                      <label class="col-sm-4 control-label">NRIC</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="nric" value="<?php echo $searchParam['nric']; ?>">
                      </div>
                    </div>

                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Intake</label>
                      <div class="col-sm-8">
                        <select name="id_intake" id="id_intake" class="form-control selitemIcon">
                            <option value="">Select</option>
                            <?php
                            if (!empty($intakeList))
                            {
                                foreach ($intakeList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                              <?php
                              if($record->id ==$searchParam['id_intake'])
                              {
                                echo 'selected';
                              }
                              ?>
                              >
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                      </div>
                    </div>

                     <div class="form-group">
                      <label class="col-sm-4 control-label">Program</label>
                      <div class="col-sm-8">
                        <select name="id_program" id="id_program" class="form-control selitemIcon">
                            <option value="">Select</option>
                            <?php
                            if (!empty($programList))
                            {
                                foreach ($programList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                              <?php
                              if($record->id ==$searchParam['id_program'])
                              {
                                echo 'selected';
                              }
                              ?>
                              >
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                  </div>
                    
                     <div class="form-group">
                      <label class="col-sm-4 control-label">Applicant Status</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="applicant_status" value="<?php echo $searchParam['applicant_status']; ?>" readonly>
                      </div>
                    </div>

                  </div>
                </div>



              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href="list" type="reset" class="btn btn-link">Clear All Fields</a>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
            <th>Sl. No</th>
            <th>Student</th>
            <th>NRIC</th>
            <th>Programme</th>
            <th>Intake</th>
            <th>Email</th>
            <th>Registered By</th>
            <th>Status</th>
            <th>Action</th>
            <!-- <th>Login</th> -->
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($studentList)) {
            $i=1;
            foreach ($studentList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->full_name ?></td>
                <td><?php echo $record->nric ?></td>
                <td><?php echo $record->programme_code . " - " . $record->programme_name ?></td>
                <td><?php echo $record->intake_name ?></td>
                <td><?php echo $record->email_id ?></td>
                <td><?php
                if($record->added_by_partner_university > 0)
                {
                  echo $partner_university_code . " - " . $partner_university_name;
                }
                else
                {
                  echo 'Administrator';
                }
                 ?></td>

                <td><?php echo $record->applicant_status ?></td>
                <td class="">
                  <a href="<?php echo 'view/' . $record->id; ?>" title="View">View</a>
                </td>
                <!--  <td class="">
                  <a href="<?php echo '/adminstudentLogin/' . $record->id; ?>" title="Edit" target="_blank">Login</a>
                </td> -->
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script type="text/javascript">
    
  $('select').select2();

  function clearSearchForm()
      {
        window.location.reload();
      }
</script>