<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>View Partner University Invoice</h3>
            <a href="../list" class="btn btn-link btn-back">‹ Back</a>
        </div>

        


        <form id="form_performa_invoice" action="" method="post">

            <div class="form-container">
                <h4 class="form-group-title">Invoice Details</h4>             
                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Invoice Number <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="invoice_number" name="invoice_number" value="<?php echo $mainInvoice->invoice_number;?>" readonly="readonly">
                        </div>
                    </div>

                     <div class="col-sm-4">
                        <div class="form-group">
                            <label>Invoice Amount <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="invoice_number" name="invoice_number" value="<?php echo $mainInvoice->amount;?>" readonly="readonly">
                        </div>
                    </div>

                    


                </div>

            

                
            </div>




                 <div class="form-container">
               
                <h4 class="form-group-title">Student Details</h4>
                <div class='data-list'>
                    <div class='row'> 
                         <table class="table" width="100%">
                            <tr>
                                <th>Student Name</th>
                                <th>Course Name</th>
                                <th>Purchased On</th>
                                <th>Amount</th>
                            </tr>
                    <?php for($m=0;$m<count($mainInvoiceDetailsList);$m++) {?>
                            <tr>
                                <td><?php echo $mainInvoiceDetailsList[$m]->full_name;?></td>
                                <td><?php echo $mainInvoiceDetailsList[$m]->name;?></td>
                                <td><?php echo date('d-m-Y',strtotime($mainInvoiceDetailsList[$m]->created_dt_tm));?></td>
                                <td><?php echo $mainInvoiceDetailsList[$m]->partner_share;?></td>
                            </tr>

                     <?php } ?> 

                         </table>
                        
                    </div>
                </div>
            </div>


                    </div>
            

            </div>







            



        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>
    $(document).ready(function() {
        $("#form_performa_invoice").validate({
            rules: {
                type_of_invoice: {
                    required: true
                },
                invoice_number: {
                    required: true
                },
                total_amount: {
                    required: true
                },
                id_student: {
                    required: true
                },
                id_application: {
                    required: true
                },
                remarks: {
                    required: true
                },
                status: {
                    required: true
                }
            },
            messages: {
                type_of_invoice: {
                    required: "Select Type Of Invoice",
                },
                invoice_number: {
                    required: "Enter Main Invoice Number",
                },
                total_amount: {
                    required: "Enter Total Amount",
                },
                id_student: {
                    required: "Select Student",
                },
                id_application: {
                    required: "Select Application",
                },
                remarks: {
                    required: "Enter Remarks",
                },
                status: {
                    required: "Status required",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
