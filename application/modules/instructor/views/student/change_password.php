<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Change Password</h3>
        </div>    
        <form action="" method="post">
            <div class="form-container">
                <h4 class="form-group-title">Instructor Details</h4>
                <div class='data-list'>
                    <div class='row'> 
                        <div class='col-sm-6'>
                               <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Password <span class='error-text'>*</span></label>
                                            <input type='password' name='password' id='password' value='' required/>
                                        </div>
                                </div>
                        </div>
                    </div>
                     <div class='row'> 
                        <div class='col-sm-6'>
                               <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Change Password <span class='error-text'>*</span></label>
                                           <input type='password' name='changepassword' id='changepassword' value='' required/>
                                        </div>
                                </div>
                        </div>
                    </div>

                    
                        </div>
                        <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Save</button>
              </div>
                    </div>
                </div>
            </div>
        </form>

    </div>

        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
</div>


</form>
