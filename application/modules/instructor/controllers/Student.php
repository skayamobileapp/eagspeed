<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Student extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('student_record_model');
        $this->load->model('student_model');
        $this->id_instructor = $_SESSION['id_instructor'];
                $this->load->model('staff_model');

    }

    
    function list()
    {
       
            $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['partner_university_id'] = $this->security->xss_clean($this->input->post('partner_university_id'));
            $formData['email_id'] = $this->security->xss_clean($this->input->post('email_id'));
            $formData['end_date'] = $this->security->xss_clean($this->input->post('end_date'));
            $formData['start_date'] = $this->security->xss_clean($this->input->post('start_date'));
            $data['searchParam'] = $formData;

            if($this->input->post())
            {           
             
                $data['mainInvoiceList'] = $this->student_model->applicantList($formData,$this->id_instructor);
            }
            else
            {
                $data['mainInvoiceList'] = array();
            }




            $data['programmeTypeList']= $this->student_model->programmeType();
            $data['organisationList']= $this->student_model->organisation();



            $data['programmeList']= $this->student_record_model->programmeListByStatus($this->id_instructor);
            $data['intakeList']= $this->student_record_model->intakeListByStatus('1');
            $this->global['pageTitle'] = 'Campus Management System : List Students';
            $this->loadViews("student/list", $this->global, $data, NULL);
       
    }

    public function changePassword() {

        if($_POST) {
                $password = $this->security->xss_clean($this->input->post('password'));
                $changepassword = $this->security->xss_clean($this->input->post('changepassword'));

           if($password!=$changepassword) {
             echo "<script>alert('Both the password and confirm password should be same');</script>";
             echo "<script>parent.location='/instructor/student/changepassword'</script>";
             exit;
           }

           if($password==$changepassword) {

                 $staff_data['password'] = md5($password);
                $updated_student = $this->staff_model->editStaff($staff_data,$this->id_instructor);
                 echo "<script>alert('Password has been updated');</script>";
             echo "<script>parent.location='/instructor/programme/list'</script>";
             exit;
           }
        }
        $data['id']='';
                    $this->loadViews("student/change_password", $this->global, $data, NULL);

    }


  
}
