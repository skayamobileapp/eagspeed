<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class StudentRecord extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('student_record_model');
    }

     function list()
    {
        if ($this->checkAccess('student.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['partner_university_id'] = $this->security->xss_clean($this->input->post('partner_university_id'));
            $formData['email_id'] = $this->security->xss_clean($this->input->post('email_id'));
            $formData['end_date'] = $this->security->xss_clean($this->input->post('end_date'));
            $formData['start_date'] = $this->security->xss_clean($this->input->post('start_date'));
            $data['searchParam'] = $formData;

            if($this->input->post())
            {           
             
                $data['mainInvoiceList'] = $this->student_model->applicantList($formData);
            }
            else
            {
                $data['mainInvoiceList'] = array();
            }




            $data['programmeTypeList']= $this->student_model->programmeType();
            $data['organisationList']= $this->student_model->organisation();



            $data['programmeList']= $this->student_record_model->programmeListByStatus('');
            $data['intakeList']= $this->student_record_model->intakeListByStatus('1');
            $this->global['pageTitle'] = 'Campus Management System : List Students';
            $this->loadViews("student/list", $this->global, $data, NULL);
        }
    }
    
   
}