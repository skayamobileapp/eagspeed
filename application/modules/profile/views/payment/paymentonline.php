
    <div class="pt-5">
      <div class="container">
        <div class="row">
          <div class="col-lg-10 offset-lg-1 mb-5">
            <div class="card mb-3">
              <div class="card-body text-center">
                <span class="loader-icon">
                  <i class="fe fe-loader"></i>
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    
<body>
<form action="/profile/payment/paymentdrdo" method="post" accept-charset="UTF-8" id="frm1" style="display: none;">
<input type="hidden" name="Title" value = "PHP VPC 3 Party Transacion">

<!-- get user input -->
<table width="80%" align="center" border="0" cellpadding='0' cellspacing='0'>

<tr>
    <td align="right"><strong><em>Virtual Payment Client URL:&nbsp;</em></strong></td>
    <td><input name="virtualPaymentClientURL" size="65" value="https://migs.mastercard.com.au/vpcpay" maxlength="250"/></td>
</tr>
<tr><td colspan="2">&nbsp;<hr width="75%">&nbsp;</td></tr>
<tr class="title">
    <td colspan="2" height="25"><p><strong>&nbsp;Basic 3-Party Transaction Fields</strong></p></td>
</tr>
<tr>
    <td align="right"><strong><em> VPC Version: </em></strong></td>
    <td><input name="vpc_Version" value="<?php echo $vpc_Version;?>" size="20" maxlength="8"/></td>
</tr>
<tr>
    <td align="right"><strong><em>Command Type: </em></strong></td>
    <td><input name="vpc_Command" value="<?php echo $vpc_Command;?>" size="20" maxlength="16"/></td>
</tr>
<tr>
    <td align="right"><strong><em>Merchant AccessCode: </em></strong></td>
    <td><input name="vpc_AccessCode" value="<?php echo $vpc_AccessCode;?>" size="20" maxlength="8"/></td>
</tr>
<tr>
    <td align="right"><strong><em>Merchant Transaction Reference: </em></strong></td>
    <td><input name="vpc_MerchTxnRef" value="<?php echo $vpc_MerchTxnRef;?>"  size="20" maxlength="40"/></td>
</tr>
<tr>
    <td align="right"><strong><em>MerchantID: </em></strong></td>
    <td><input name="vpc_Merchant" value="<?php echo $vpc_Merchant;?>" size="20" maxlength="16"/></td>
</tr>
<tr>
    <td align="right"><strong><em>Transaction OrderInfo: </em></strong></td>
    <td><input name="vpc_OrderInfo" value="<?php echo $vpc_OrderInfo;?>" size="20" maxlength="34"/></td>
</tr>
<tr>
    <td align="right"><strong><em>Purchase Amount: </em></strong></td>
    <td><input name="vpc_Amount" value="<?php echo $vpc_Amount;?>" maxlength="10"/></td>
</tr>
<tr>
    <td align="right"><strong><em>Receipt ReturnURL: </em></strong></td>
    <td><input name="vpc_ReturnURL" size="65" value="<?php echo $vpc_ReturnURL;?>" maxlength="250"/></td>
</tr>


<tr>
    <td align="right"><strong><em>VPC Locale: </em></strong></td>
    <td><input name="vpc_Locale" size="65" value="<?php echo $vpc_Locale;?>" maxlength="250"/></td>
</tr>
<tr>
    <td align="right"><strong><em>VPC Currency: </em></strong></td>
    <td><input name="vpc_Currency" size="65" value="<?php echo $vpc_Currency;?>" maxlength="250"/></td>
</tr>



<tr>    <td colspan="2">&nbsp;</td></tr>
<tr>
    <td>&nbsp;</td>
    <td><input type="submit" NAME="SubButL" value="Pay Now!"></td>
</tr>
<tr><td colspan="2">&nbsp;<hr width="75%">&nbsp;</td></tr>



<tr>
    <td width="40%">&nbsp;</td>
    <td width="60%">&nbsp;</td>
</tr>

</table>

</form>
</body>

<head>
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="pragma" content="no-cache" />
    <meta http-equiv="expires" content="0" />
</head>
<script type="text/javascript">
    $(document).ready(function(){
     $("#frm1").submit();
});

</script>

</html>
