
<div class="course-lists">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-xl-12 col-lg-12 col-md-12 col-12">
            <div>
              <h1 class="mb-0 text-white display-4">Thank You</h1>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!--PAGE HEADER ENDS HERE-->

    <!--CHECKOUT PAGE  STARTS HERE-->

    <div class="pt-5">
      <div class="container">
        <div class="row">
          <div class="col-lg-10 offset-lg-1 mb-5">
            <div class="card mb-3">
              <div class="card-body text-center mb-3">
                <h3 class="pb-5">Thank You!</h3>
                <h4>Your Payment is successfull</h4>
                  <a href="/profile/dashboard/index" class="btn btn-outline-primary my-3">
                  Dashboard
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
