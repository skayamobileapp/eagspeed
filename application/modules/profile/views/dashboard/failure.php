
<div class="course-lists">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-xl-12 col-lg-12 col-md-12 col-12">
            <div>
              <h1 class="mb-0 text-white display-4">Thank You</h1>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!--PAGE HEADER ENDS HERE-->

    <!--CHECKOUT PAGE  STARTS HERE-->

    <div class="pt-5">
      <div class="container">
        <div class="row">
          <div class="col-lg-10 offset-lg-1 mb-5">
            <div class="card mb-3">
              <div class="card-body text-center mb-3">
                <img src="img/fireworks.svg" class="img-4by3-lg mt-5 mb-3" />
                <h3 class="pb-5">Sorry</h3>
                <h4>Sorry Your Payment was un-successfull</h4>
                <a href="/profile/dashboard/index" class="btn btn-outline-primary my-3">
                  Dashboard
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
