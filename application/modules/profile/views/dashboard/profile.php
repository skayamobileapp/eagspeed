
    <!-- MAIN CONTAINER BLOCK STARTS HERE -->
    <div class="mt-3 container-fluid">
      <div class="row">
        <!-- SIDEBAR NAV STARTS HERE -->
        <div class="col-xl-2 col-md-3">
          <nav
            class="navbar navbar-expand-md navbar-light shadow-sm mb-4 mb-lg-0 small-sidenav"
          >
            <!--MENU-->
            <a
              href="#"
              class="d-xl-none d-lg-none d-md-none text-inherit font-weight-bold"
              >Menu</a
            >
            <button
              class="navbar-toggler d-md-none icon-shape icon-sm rounded bg-primary text-light"
              type="button"
              data-toggle="collapse"
              data-target="#smallSidenav"
              aria-controls="smallSidenav"
              aria-expanded="true"
              aria-label="Toggle navigation"
            >
              <span class="fe fe-menu"></span>
            </button>
            <div class="collapse navbar-collapse" id="smallSidenav">
              <div class="navbar-nav flex-column w-100">
                <div class="d-flex mb-3 align-items-center">
                  <div class="avatar avatar-md avatar-indicators avatar-online">

                 <?php if($studentDetails->image=='') { ?> 

                    <img
                      alt="avatar"
                      src="/website/img/blank.png"
                      class="rounded-circle"
                    />
                  <?php } else { ?> 

                    <img
                      alt="avatar"
                      src="/assets/images/<?php echo $studentDetails->profile_pic;?>"
                      class="rounded-circle"
                    />

                  <?php } ?> 


                  </div>
                  <div class="ml-3 lh-1">
                    <h5 class="mb-1" style="text-transform:uppercase;"><?php echo $studentDetails->full_name;?></h5>
                    <p class="mb-0 text-muted"><?php echo $studentDetails->nric;?></p>
                  </div>
                </div>
                <ul class="list-unstyled mb-0">
                  <li class="list-unstyled nav-item">
                    <a href="/profile/dashboard/index" class="nav-link"
                      ><i class="fe fe-home nav-icon"></i> Dashboard</a
                    >
                  </li>
                   <li class="list-unstyled nav-item active">
                    <a href="/profile/dashboard/profile" class="nav-link"
                      ><i class="fe fe-user nav-icon"></i> Profile</a
                    >
                  </li>
                 
                   <li class="list-unstyled nav-item">
                    <a href="/profile/dashboard/soa" class="nav-link"
                      ><i class="fe fe-clipboard nav-icon"></i> Invoice & Receipt</a
                    >
                  </li>
                 
                  <li class="list-unstyled nav-item">
                    <a href="/profile/dashboard/password" class="nav-link"
                      ><i class="fe fe-clock nav-icon"></i> Change Password</a
                    >
                  </li>
                
                </ul>
              </div>
            </div>
          </nav>
        </div>



        <div class="col-xl-10 col-md-6">
           <div class="row" style="padding-bottom:10px;">

                               <div class="col-sm-1" style="text-align: right;padding-top:5px;">I'm  a 
           </div>
           <div class="col-sm-2"> <input type='text' class="form-control" value="<?php echo $studentDetails->current_profile;?>" id="current_profile" onblur='getcurrentprofie()'>
           </div>

           <div class="col-sm-2" style="text-align: right;padding-top:5px;">I want to become
           </div>
           <div class="col-sm-2"> <input type='text' class="form-control" value="<?php echo $studentDetails->next_profile;?>" id="next_profile" onblur='getnextprofie()'>
           </div>
            <div class="col-sm-2" style="text-align: right;padding-top:5px;">Set your learning goals
           </div>
           <div class="col-sm-2"> <select name='learning_goals' id='learning_goals' class="form-control" onchange="getnextlearning()"> 
            <option value="Career Change" <?php if($studentDetails->learning_goals=='Career Change') { echo "selected=selected";} ?>>Career Change</option>
            <option value="Upgrade Skill" <?php if($studentDetails->learning_goals=='Upgrade Skill') { echo "selected=selected";} ?>>Upgrade Skill</option>
            <option value="Reskilling" <?php if($studentDetails->learning_goals=='Reskilling') { echo "selected=selected";} ?>>Reskilling</option>
            <option value="Lifelong Learning" <?php if($studentDetails->learning_goals=='Lifelong Learning') { echo "selected=selected";} ?>>Lifelong Learning</option>
           </select>
           </div>

        </div>
        <div class="row">
          <div class='col-sm-1'></div>
          <div class='col-sm-10'>
                       <p style="font-size:12px;"> Eg: I'm a <b>teacher</b>  I want to be <b>Artist</b>  Set your learning goal as <b>Reskilling</b></p>

          </div>
        </div>


          <div class="card mb-3">
            <div class="card-header bg-light">
              <h4 class="mb-0">Profile Details</h4>
            </div>
            <div class="card-body">
              <form class="form-row" action="" method="post" enctype="multipart/form-data">
                <!-- First name -->

                 <div class="form-group col-12 col-md-4">
                  <label class="form-label" for="fname">Salutation</label>

                  <select name='salutation' id='salutation' class="form-control">
                    <?php for($s=0;$s<count($salutationList);$s++) { ?> 
                      <option value="<?php echo $salutationList[$s]->id;?>"
                         <?php if($salutationList[$s]->id==$studentDetails->salutation) {
                           echo "selected=selected"; 
                         } ?>><?php echo $salutationList[$s]->name;?></option>

                    <?php } ?> 
                  </select>
 
                </div>


                <div class="form-group col-12 col-md-4">
                  <label class="form-label" for="fname">First Name</label>
                  <input
                    type="text"
                    id="fname"
                    class="form-control"
                    name="full_name"
                    
                    value="<?php echo $studentDetails->full_name;?>"
                  />
                </div>
                <!-- Last name -->
                <div class="form-group col-12 col-md-4">
                  <label class="form-label" for="lname">Last Name</label>
                  <input
                    type="text"
                    id="lname"
                    class="form-control"
                    name="last_name"
                    
                    value="<?php echo $studentDetails->last_name;?>"                    
                  />
                </div>
                <!-- Phone -->
                <div class="form-group col-12 col-md-4">
                  <label class="form-label" for="phone">Email</label>
                  <input
                    type="text"
                    id="phone"
                    class="form-control"
                    name="email_id"
                    
                    value="<?php echo $studentDetails->email_id;?>"                    

                  />
                </div>
                  <div class="form-group col-12 col-md-4">
                  <label class="form-label" for="phone">Phone Number</label>
                  <input
                    type="text"
                    id="phone"
                    class="form-control"
                    name="phone"
                    
                    value="<?php echo $studentDetails->phone;?>"                    

                  />
                </div>

                <!-- Birthday -->
                 <div class="form-group col-12 col-md-4">
                  <label class="form-label" for="phone">Whatsapp Number</label>
                  <input
                    type="text"
                    id="whatsapp_number"
                    class="form-control"
                    name="whatsapp_number"
                    
                    value="<?php echo $studentDetails->whatsapp_number;?>"                    

                  />
                </div>
            </div>

            <div class="card-header bg-light">
              <h4 class="mb-0">Work Experience</h4>
            </div>
            <div class="card-body form-row">
                <!-- First name -->
                <div class="form-group col-12 col-md-4">
                  <label class="form-label" for="occupation">Occupation</label>
                  <input
                    type="text"
                    id="occupation"
                    class="form-control"
                    name="occupation"
                    
                    value="<?php echo $studentDetails->occupation;?>"
                  />
                </div>
                <!-- Last name -->
                <div class="form-group col-12 col-md-4">
                  <label class="form-label" for="experience_level">Experience Level</label>
                  <input
                    type="text"
                    id="experience_level"
                    class="form-control"
                    name="experience_level"
                    
                    value="<?php echo $studentDetails->experience_level;?>"                    
                  />
                </div>
                <!-- Phone -->
                <div class="form-group col-12 col-md-4">
                  <label class="form-label" for="employer">Employer</label>
                  <input
                    type="text"
                    id="employer"
                    class="form-control"
                    name="employer"
                    
                    value="<?php echo $studentDetails->employer;?>"                    

                  />
                </div>
                 
            </div>

              <div class="card-header bg-light">
              <h4 class="mb-0">Education</h4>
            </div>
            <div class="card-body form-row">
                <!-- First name -->
                <div class="form-group col-12 col-md-4">
                  <label class="form-label" for="degree">Degree</label>
                  <input
                    type="text"
                    id="degree"
                    class="form-control"
                    name="degree"
                    
                    value="<?php echo $studentDetails->degree;?>"
                  />
                </div>
                <!-- Last name -->
                <div class="form-group col-12 col-md-4">
                  <label class="form-label" for="university">University</label>
                  <input
                    type="text"
                    id="university"
                    class="form-control"
                    name="university"
                    
                    value="<?php echo $studentDetails->university;?>"                    
                  />
                </div>
                <!-- Phone -->
                <div class="form-group col-12 col-md-4">
                  <label class="form-label" for="field_major">Field or Major</label>
                  <input
                    type="text"
                    id="field_major"
                    class="form-control"
                    name="field_major"
                    
                    value="<?php echo $studentDetails->field_major;?>"                    

                  />
                </div>
                 
            </div>

              <div class="card-header bg-light">
              <h4 class="mb-0">Career Goal</h4>
            </div>
            <div class="card-body form-row">
                <!-- First name -->
                <div class="form-group col-12 col-md-4">
                  <label class="form-label" for="fname">Occupation</label>
                  <input
                    type="text"
                    id="career_occupation"
                    class="form-control"
                    name="career_occupation"
                    
                    value="<?php echo $studentDetails->career_occupation;?>"
                  />
                </div>
                <!-- Last name -->
                <div class="form-group col-12 col-md-4">
                  <label class="form-label" for="industry">Industry</label>
                  <input
                    type="text"
                    id="industry"
                    class="form-control"
                    name="industry"
                    
                    value="<?php echo $studentDetails->industry;?>"                    
                  />
                </div>
               
                 
            </div>

            
              
                <div class="col-12">
                  <!-- Button -->
                  <button class="btn btn-primary" type="submit">
                    Update Profile
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>


        <!-- SIDEBAR NAV ENDS HERE -->
      </div>
    </div>
    <!-- MAIN CONTAINER BLOCK ENDS HERE -->

    <div class="footer">
      <div class="container">
        <div class="row align-items-center no-gutters border-top py-2">
          <!-- Desc -->
          <div class="col-md-6 col-12">
            <span>&copy; 2021 Speed. All Rights Reserved.</span>
          </div>
          <!-- Links -->
          <div class="col-12 col-md-6">
            <nav class="nav justify-content-center justify-content-md-end">
              <a class="nav-link active pl-0" href="#!">Privacy</a>
              <a class="nav-link" href="#!">Terms </a>
              <a class="nav-link" href="#!">Feedback</a>
              <a class="nav-link" href="#!">Support</a>
            </nav>
          </div>
        </div>
      </div>
    </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>



    <script type="text/javascript">

  

   function getcurrentprofie(){

        $.post("/profile/dashboard/saveprofile",
        {
          current_profile: $("#current_profile").val(),
        },
        function(data, status){
          parent.location='/profile/dashboard/profile';
        });

   }

    function getnextprofie(){

        $.post("/profile/dashboard/saveprofile",
        {
          next_profile: $("#next_profile").val(),
        },
        function(data, status){
          parent.location='/profile/dashboard/profile';
        });

   }

    function getnextlearning(){

        $.post("/profile/dashboard/saveprofile",
        {
          learning_goals: $("#learning_goals").val(),
        },
        function(data, status){
          // parent.location='/profile/dashboard/index';
        });

   }


   
  </script>

  </body>
</html>
