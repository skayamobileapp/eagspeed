 <div class="mt-3 container-fluid">


      <div class="row">
        <!-- SIDEBAR NAV STARTS HERE -->
        <div class="col-xl-2 col-md-3">
          <nav
            class="navbar navbar-expand-md navbar-light shadow-sm mb-4 mb-lg-0 small-sidenav"
          >
            <!--MENU-->
            <a
              href="#"
              class="d-xl-none d-lg-none d-md-none text-inherit font-weight-bold"
              >Menu</a
            >
            <button
              class="navbar-toggler d-md-none icon-shape icon-sm rounded bg-primary text-light"
              type="button"
              data-toggle="collapse"
              data-target="#smallSidenav"
              aria-controls="smallSidenav"
              aria-expanded="true"
              aria-label="Toggle navigation"
            >
              <span class="fe fe-menu"></span>
            </button>
            <div class="collapse navbar-collapse" id="smallSidenav">
              <div class="navbar-nav flex-column w-100">
                <div class="d-flex mb-3 align-items-center">
                  <div class="avatar avatar-md avatar-indicators avatar-online">

                 <?php if($studentDetails->image=='') { ?> 

                    <img
                      alt="avatar"
                      src="/website/img/blank.png"
                      class="rounded-circle"
                    />
                  <?php } else { ?> 

                    <img
                      alt="avatar"
                      src="/assets/images/<?php echo $studentDetails->profile_pic;?>"
                      class="rounded-circle"
                    />

                  <?php } ?> 


                  </div>
                  <div class="ml-3 lh-1">
                    <h5 class="mb-1" style="text-transform:uppercase;"><?php echo $studentDetails->full_name;?></h5>
                    <p class="mb-0 text-muted"><?php echo $studentDetails->nric;?></p>
                  </div>
                </div>
                <ul class="list-unstyled mb-0">
                  <li class="list-unstyled nav-item active">
                    <a href="/profile/dashboard/index" class="nav-link"
                      ><i class="fe fe-home nav-icon"></i> Dashboard</a
                    >
                  </li>
                   <li class="list-unstyled nav-item ">
                    <a href="/profile/dashboard/profile" class="nav-link"
                      ><i class="fe fe-user nav-icon"></i> Profile</a
                    >
                  </li>
               
                  <li class="list-unstyled nav-item">
                    <a href="/profile/dashboard/soa" class="nav-link"
                      ><i class="fe fe-clipboard nav-icon"></i> Invoice & Receipt</a
                    >
                  </li>
                 <!--  <li class="list-unstyled nav-item">
                    <a href="/profile/dashboard/soa" class="nav-link"
                      ><i class="fe fe-home nav-icon"></i> SOA</a
                    >
                  </li> -->
                 
                  <li class="list-unstyled nav-item">
                    <a href="/profile/dashboard/password" class="nav-link"
                      ><i class="fe fe-clock nav-icon"></i> Change Password</a
                    >
                  </li>
                 
                </ul>
              </div>
            </div>
          </nav>
        </div>


        <div class="col-xl-10 col-md-6">
        <div class="col-md-12">
                  <div class="row" style="padding-bottom:10px;">

                               <div class="col-sm-1" style="text-align: right;padding-top:5px;">I'm  a 
           </div>
           <div class="col-sm-2"> <input type='text' class="form-control" value="<?php echo $studentDetails->current_profile;?>" id="current_profile" onblur='getcurrentprofie()'>
           </div>

           <div class="col-sm-2" style="text-align: right;padding-top:5px;">I want to become
           </div>
           <div class="col-sm-2"> <input type='text' class="form-control" value="<?php echo $studentDetails->next_profile;?>" id="next_profile" onblur='getnextprofie()'>
           </div>
            <div class="col-sm-2" style="text-align: right;padding-top:5px;">Set your learning goals
           </div>
           <div class="col-sm-2"> <select name='learning_goals' id='learning_goals' class="form-control" onchange="getnextlearning()"> 
            <option value="Career Change" <?php if($studentDetails->learning_goals=='Career Change') { echo "selected=selected";} ?>>Career Change</option>
            <option value="Upgrade Skill" <?php if($studentDetails->learning_goals=='Upgrade Skill') { echo "selected=selected";} ?>>Upgrade Skill</option>
            <option value="Reskilling" <?php if($studentDetails->learning_goals=='Reskilling') { echo "selected=selected";} ?>>Reskilling</option>
            <option value="Lifelong Learning" <?php if($studentDetails->learning_goals=='Lifelong Learning') { echo "selected=selected";} ?>>Lifelong Learning</option>
           </select>
           </div>

        </div>
        <div class="row">
          <div class='col-sm-1'></div>
          <div class='col-sm-10'>
                       <p style="font-size:12px;"> Eg: I'm a <b>teacher</b>  I want to be <b>Artist</b>  Set your learning goal as <b>Reskilling</b></p>

          </div>
        </div>


          <div class="card-header border-bottom-0 p-0 bg-light">
            <div>
              <!-- Nav -->
              <ul class="nav nav-lb-tab" id="tab" role="tablist">
                <li class="nav-item">
                  <a
                    class="nav-link active"
                    id="inProgressCourses-tab"
                    data-toggle="pill"
                    href="#inProgressCourses"
                    role="tab"
                    aria-controls="table"
                    aria-selected="true"
                    >In Progress </a
                  >
                </li>
                <li class="nav-item">
                  <a
                    class="nav-link"
                    id="completedCourses-tab"
                    data-toggle="pill"
                    href="#completedCourses"
                    role="tab"
                    aria-controls="description"
                    aria-selected="false"
                    >Completed</a
                  >
                </li>
              </ul>
            </div>
          </div>
          <!-- Card Body -->
          <div class="card-body bg-light">
            <div class="tab-content" id="tabContent">
              <div
                class="tab-pane fade show active"
                id="inProgressCourses"
                role="tabpanel"
                aria-labelledby="inProgressCourses-tab"
              >
                <div class="course-card pt-0">
                  <div class="row">
                      <div class="table-responsive">
                <table class="courses-table table border-0" style="text-align: center;">
                <thead>
                  <tr>
                    <th scope="col" width="30%">Course</th>
                    <th scope="col" class="border-0" width="20%">Course Name</th>
                    <th scope="col" class="border-0" width="15%">Course Start Date</th>
                    <th scope="col" class="border-0" width="15%">Course End Date</th>
                    <th scope="col" class="border-0" width="15%">Course Duration</th>
                  </tr>
                </thead>
                <tbody>
                  <?php for($i=0;$i<count($courseListPending);$i++) {?> 
                  <tr>
                    <td class="align-middle border-top-0">
                       <a href="/coursedetails/index/<?php echo $courseListPending[$i]->id;?>" target="_blank">
                           <img
                        src="<?php echo "/assets/images/".$courseListPending[$i]->course_image;?>"
                        alt
                        class="rounded-top card-img-top"
                        style="width:60%;padding-bottom:20px;" 

                          />
                      </a>                     
                    </td>
                    <td>
                       <a href="/profile/dashboard/sso/<?php echo $courseListPending[$i]->moodle_id;?>" target="_blank"><?php echo $courseListPending[$i]->name;?></a></td>

                    <td>
                       <?php  if($courseListPending[$i]->startdateofprogramme!='1970-01-01') {
                        echo date('d-m-Y',strtotime($courseListPending[$i]->startdateofprogramme));
                      } else { 
                        echo date('d-m-Y',strtotime($courseListPending[$i]->created_dt_tm));
                           } ?> 
                        


                      </td>
                     


                    <td>

                         <?php  if($courseListPending[$i]->startdateofprogramme!='1970-01-01') {
                        echo date('d-m-Y',strtotime($courseListPending[$i]->enddateofprogramme));
                      } else { 

                       echo  date('d-m-Y', strtotime($courseListPending[$i]->created_dt_tm. ' + ' .$courseListPending[$i]->max_duration.' '.$courseListPending[$i]->duration_type)); 
                           } ?> 

                        


                      </td>

                    <td class="align-middle border-top-0"><?php echo $courseListPending[$i]->max_duration.' '.$courseListPending[$i]->duration_type;?> <br/>
                      <?php if($courseListPending[$i]->moodle_id=='73') { ?> <b>2 Weeks Gift! </b> <?php } ?> </td>

                  </tr>
                <?php } ?> 
                 
                </tbody>
              </table>
                  </div>

                   
                  </div>
                </div>
              </div>
              <div
                class="tab-pane fade"
                id="completedCourses"
                role="tabpanel"
                aria-labelledby="completedCourses-tab"
              >
                <div class="course-card pt-0">
                  <div class="row">

                      <div class="table-responsive">
                        <table class="courses-table table border-0" style="text-align: center">
                <thead>
                  <tr>
                    <th scope="col" width="50%">COURSES</th>
                    <th scope="col" class="border-0" width="20%">Course Duration</th>
                    <th scope="col" class="border-0" width="20%">Final Marks </th>
                    <th scope="col" class="border-0" width="20%">Download certificate</th>
                  </tr>
                </thead>
                <tbody>
                  <?php for($i=0;$i<count($courseListCompleted);$i++) {?> 
                  <tr>
                    <td class="align-middle border-top-0">
                       <a href="/coursedetails/index/<?php echo $courseListCompleted[$i]->id;?>" class="card-img-top" target="_blank">
                           <img
                        src="<?php echo "/assets/images/".$courseListCompleted[$i]->course_image;?>"
                        class="rounded-top card-img-top"
                        style="width:60%;padding-bottom:20px;" 
                          />
                      </a>
                     <br/>
                      <a href="/coursedetails/index/<?php echo $courseListCompleted[$i]->id;?>" target="_blank"><?php echo $courseListCompleted[$i]->name;?></a>
                    </td>

                       <td class="align-middle border-top-0"><?php echo $courseListCompleted[$i]->max_duration.' '.$courseListCompleted[$i]->duration_type;?></td>
                  

                    <td class="align-middle border-top-0"><?php echo $courseListCompleted[$i]->final_marks;?></td>



                
                     <td class="align-middle border-top-0">
                      <a href="/assets/cert.pdf" target="_blank">Download Certificate</a>
                    </td>
                  </tr>
                <?php } ?> 
                 
                </tbody>
              </table>
                      </div>
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

         <div class="col-md-12">
          <div class="card-header border-bottom-0 p-0 bg-light">
            <div>
              <!-- Nav -->
              <ul class="nav nav-lb-tab" id="tab" role="tablist">
                <li class="nav-item">
                  <a
                    class="nav-link active"
                    id="inProgressCourses-tab"
                    data-toggle="pill"
                    href="#inProgressCourses"
                    role="tab"
                    aria-controls="table"
                    aria-selected="true"
                    >Relevant Courses </a
                  >
                </li>
                
              </ul>
            </div>
          </div>
          <!-- Card Body -->
          <div class="card-body bg-light">
            <div class="tab-content" id="tabContent">
              <div
                class="tab-pane fade show active"
                id="inProgressCourses"
                role="tabpanel"
                aria-labelledby="inProgressCourses-tab"
              >
                <div class="course-card pt-0">
                  <div class="row">
                      <div class="container">
      
          

         <div class="row">
          <?php 
          for($i=0;$i<3;$i++) {

                    for($i=0;$i<count($recomendedprogrammeList);$i++) { 
          


                    $amount = number_format($recomendedprogrammeList[$i]->amount,2);

                       if($amount>0) {
                      $amount = "RM ".$amount;
                    } else {
                      $amount = "FREE";
                    }


            ?>



          <div class="col-lg-4 col-md-6 col-12">
            <div class="card mb-4 card-hover">
              <a href="/coursedetails/index/<?php echo $recomendedprogrammeList[$i]->id;?>" class="card-img-top">
                     <img
                  src="<?php echo "/assets/images/".$recomendedprogrammeList[$i]->course_image;?>"
                  alt
                  class="rounded-top card-img-top"
                />
              </a>
              <div class="card-body">
                <h4 class="mb-2 text-truncate-line-2" class="text-inherit">
                  <a href="/coursedetails/index/<?php echo $recomendedprogrammeList[$i]->id;?>" class="text-inherit"
                    ><?php echo ucwords($recomendedprogrammeList[$i]->name);?></a
                  >
                </h4>
                <!--list-->
                  <ul class="mb-2 list-inline">
                  <li class="list-inline-item">
                    <i class="fe fe-clock mr-1"></i><?php echo $recomendedprogrammeList[$i]->max_duration . " - " . $recomendedprogrammeList[$i]->duration_type ?>
                  </li>
                  <li class="list-inline-item">
                    <i class="fe fe-bar-chart mr-1"></i><?php echo $recomendedprogrammeList[$i]->studylevel;?>
                  </li>
                </ul>
                <div class="1h-1">
                  <div class="d-flex">
                    <div class="h5">  <?php echo $amount;?></div>
                    
                  </div>
                </div>
                   <div class="1h-1">
                  <div class="d-flex">
                     <div class="">
                                          <i class="fe fe-book mr-1"></i>
                                          <?php echo $recomendedprogrammeList[$i]->producttype;?>

                    </div>
                  </div>
                </div>
                

                <div class="d-flex mt-2">
                  <a href="/coursedetails/index/<?php echo $recomendedprogrammeList[$i]->id;?>" class="btn btn-outline-primary btn-sm">More Info</a>

                </div>
              </div>

            </div>
          </div>
         
                  <?php } }  ?> 



            
            </div>
          </div>
        </div>

        </div>
      
      </div>
    </div>

  <div class="footer">
      <div class="container">
        <div class="row align-items-center no-gutters border-top py-2">
          <!-- Desc -->
          <div class="col-md-6 col-12">
            <span>&copy; 2021 Speed. All Rights Reserved.</span>
          </div>
          <!-- Links -->
          <div class="col-12 col-md-6">
            <nav class="nav justify-content-center justify-content-md-end">
              <a class="nav-link active pl-0" href="#!">Privacy</a>
              <a class="nav-link" href="#!">Terms </a>
              <a class="nav-link" href="#!">Feedback</a>
              <a class="nav-link" href="#!">Support</a>
            </nav>
          </div>
        </div>
      </div>
    </div>


<div class="modal fade" id="freeSignupModal" tabindex="-1" role="dialog"  aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Welcome to MyEduSkills</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form-row" method="POST" action="/profile/dashboard/profile">
                <h5 class="col-12 pt-3">Work Experience</h5>
                <div class="form-group col-12 col-md-4">
                  <label class="form-label" for="fname">Occupation</label>
                  <input
                    type="text"
                    id="fname"
                    class="form-control"
                    name="occupation"
                    
                    value="<?php echo $studentDetails->occupation;?>"
                  />
                </div>
                <!-- Last name -->
                <div class="form-group col-12 col-md-4">
                  <label class="form-label" for="lname">Experience Level</label>
                  <input
                    type="text"
                    id="lname"
                    class="form-control"
                    name="experience_level"
                    
                    value="<?php echo $studentDetails->experience_level;?>"                    
                  />
                </div>
                <!-- Phone -->
                <div class="form-group col-12 col-md-4">
                  <label class="form-label" for="phone">Employer</label>
                  <input
                    type="text"
                    id="phone"
                    class="form-control"
                    name="employer"
                    
                    value="<?php echo $studentDetails->employer;?>"                    

                  />
                </div>  

                <h5 class="col-12 pt-3">Education</h5>
                <div class="form-group col-12 col-md-4">
                  <label class="form-label" >Degree</label>
                   <input
                    type="text"
                    id="fname"
                    class="form-control"
                    name="degree"
                    
                    value="<?php echo $studentDetails->degree;?>"
                  />
                </div>
                <div class="form-group col-12 col-md-4">
                  <label class="form-label" >University</label>
                   <input
                    type="text"
                    id="fname"
                    class="form-control"
                    name="university"
                    
                    value="<?php echo $studentDetails->university;?>"
                  />
                </div>   
                <div class="form-group col-12 col-md-4">
                  <label class="form-label" >Field or Major</label>
                  <input
                    type="text"
                    id="phone"
                    class="form-control"
                    name="field_major"
                    
                    value="<?php echo $studentDetails->field_major;?>"                    

                  />               
                </div>  

                <h5 class="col-12 pt-3">Career Goal</h5>
                <div class="form-group col-12 col-md-4">
                  <label class="form-label" >Occupation</label>
                  <input
                    type="text"
                    id="career_occupation"
                    class="form-control"
                    name="career_occupation"
                    
                    value="<?php echo $studentDetails->career_occupation;?>"
                  />
                </div>
                <div class="form-group col-12 col-md-4">
                  <label class="form-label" >Industry</label>
                  <input
                    type="text"
                    id="lname"
                    class="form-control"
                    name="industry"
                    
                    value="<?php echo $studentDetails->industry;?>"                    
                  />
                </div>                  
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Skip</button>        
        <button type="submit" class="btn btn-primary">Continue</button>
      </div> 
      </form>     
    </div>
  </div>
</div> 
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

<script>
  $(document).ready(function(){
        $("#freeSignupModal").modal('show');
  });
</script>



<script type="text/javascript">

  $(document).ready(function(){
        $("#freeSignupModal").modal('show');
    });


   function autologin()
    {
      $.noConflict();

        jQuery.get("/profile/dashboard/autologin", function(data, status){
             console.log(data);
             // parent.location= "<?php echo BASE_PATH;?>/index/checkout";
             window.open(data, '_blank');
         });
    }


   function getcurrentprofie(){

        $.post("/profile/dashboard/saveprofile",
        {
          current_profile: $("#current_profile").val(),
        },
        function(data, status){
          // parent.location='/profile/dashboard/index';
        });

   }

    function getnextprofie(){

        $.post("/profile/dashboard/saveprofile",
        {
          next_profile: $("#next_profile").val(),
        },
        function(data, status){
          // parent.location='/profile/dashboard/index';
        });

   }

   function getnextlearning(){

        $.post("/profile/dashboard/saveprofile",
        {
          learning_goals: $("#learning_goals").val(),
        },
        function(data, status){
          // parent.location='/profile/dashboard/index';
        });

   }


   
  </script>





  </body>
</html>
