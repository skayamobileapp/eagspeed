<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

// Create the Razorpay Order

class Payment extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('dashboard_model');
        $this->id_student =  $this->session->userdata['id_student'];
        error_reporting(0);
    }


    function onlineislamic()
    {
        $id_session = session_id();
        $data['listOfCourses'] = $this->dashboard_model->gerProgrammeFromSession($id_session);

        $data['vpc_Version'] = '1';
        $data['vpc_Command'] = 'pay';
        $data['vpc_AccessCode'] = '7CF5CE4B';
        $data['vpc_MerchTxnRef'] = 'MRT'.date('YmdHis');
        $data['vpc_Merchant'] = '10701400032';
        $data['vpc_OrderInfo'] = 'ORD'.date('YmdHis');
        $data['vpc_Amount'] = '10';
        $data['vpc_ReturnURL'] = 'https://eagspeed.camsedu.com/profile/payment/paymentresponse';
        $data['vpc_Locale'] = 'en_AU';
        $data['vpc_Currency'] = 'MYR';


        $insertArray = array(
            'vpc_Version'=>$data['vpc_Version'],
            'vpc_Command'=>$data['vpc_Command'],
            'vpc_AccessCode'=>$data['vpc_AccessCode'],
            'vpc_MerchTxnRef'=>$data['vpc_MerchTxnRef'],
            'vpc_Merchant'=>$data['vpc_Merchant'],
            'vpc_OrderInfo'=>$data['vpc_OrderInfo'],
            'vpc_Amount'=>$data['vpc_Amount'],
            'vpc_ReturnURL'=>$data['vpc_ReturnURL'],
            'vpc_Locale'=>$data['vpc_Locale'],
            'vpc_Currency'=>$data['vpc_Currency'],
            'id_student'=>$this->id_student 
        );
        $this->dashboard_model->insertBeforeTransaction($insertArray);
        $this->global = '';
        $this->loadViews('payment/paymentonline',$this->global,$data,NULL);
    }


    function paymentsenang()
    {

     $this->paymentresponsesenpaylocal();

        $merchant_id = '438162148509279';
$secretkey = '28355-175';
$orderId = date('Ymdhis');
        $id_session = session_id();


        $listOfCourses = $this->dashboard_model->gerProgrammeFromSession($id_session);



  $staffDiscount = $this->dashboard_model->getDiscountFromStaff($id_session);


   
                   $totalAmount=0;
                   $discountAmount = 0;
                for($l=0;$l<count($listOfCourses);$l++) {
                $totalAmount =  $totalAmount + $listOfCourses[$l]->amount;
            
              }

              if($staffDiscount) { 

                  if($staffDiscount[0]->amount>0) { 
                    $discountAmount = number_format($staffDiscount[0]->amount,2);

                   }
               } 


                $totalAmount = $totalAmount- $discountAmount;
                $totalAmount = number_format($totalAmount,2);


         if($totalAmount<1) {
            echo "<script>parent.location='/profile/payment/freecourseregistration'</script>";
            exit;
         }
 $hashed_string = hash_hmac('sha256', $secretkey.$orderId.$totalAmount.$orderId, $secretkey);

 


        $studentDetails = $this->dashboard_model->getStudentDetailsById($this->id_student);


        $data['detail'] = $orderId;
        $data['amount'] = $totalAmount;
        $data['order_id'] = $orderId;
        $data['name'] = $studentDetails->full_name;
        $data['email'] = $studentDetails->email_id;
        $data['phone'] = $studentDetails->phone;
        $data['hashed_string'] =  $hashed_string;
        $data['merchant_id'] = $merchant_id;

        $this->global = '';
        $this->loadViews('payment/paymentsenang',$this->global,$data,NULL);
    }


    function freecourseregistration(){


               $id_session = session_id();
               $discountAmount = 0;
                $listOfCourses = $this->dashboard_model->gerProgrammeFromSession($id_session);

                $discountStaff = $this->dashboard_model->getDiscountFromStaff($id_session);
                if($discountStaff) {
                 $discountAmount = $discountStaff[0]->amount;
                }
                $invoicegenerated = array();
                for($k=0;$k<count($listOfCourses);$k++)
                {

                  $programme = $this->dashboard_model->getProgramme($listOfCourses[$k]->id_programme);

                    if($programme)
                    {                        
                        
                        $this->insertintomoodle($this->id_student,$listOfCourses[$k]->id_programme);

                    }
                }

                /// empty cart
                 $this->emptycart();
            echo "<script>parent.location='/profile/dashboard/index'</script>";
            exit;

    }


    function paymentdrdo(){

       
            include('VPCPaymentConnection.php');
            $conn = new VPCPaymentConnection();


            // This is secret for encoding the SHA256 hash
            // This secret will vary from merchant to merchant

            $secureSecret = "050624D76BDBE482109E994A9E0D2220";

            // Set the Secure Hash Secret used by the VPC connection object
            $conn->setSecureSecret($secureSecret);


            // *******************************************
            // START OF MAIN PROGRAM
            // *******************************************
            // Sort the POST data - it's important to get the ordering right
            ksort ($_POST);

            // add the start of the vpcURL querystring parameters
            $vpcURL = $_POST["virtualPaymentClientURL"];

            // This is the title for display
            $title  = $_POST["Title"];


            // Remove the Virtual Payment Client URL from the parameter hash as we 
            // do not want to send these fields to the Virtual Payment Client.
            unset($_POST["virtualPaymentClientURL"]); 
            unset($_POST["SubButL"]);
            unset($_POST["Title"]);

            // Add VPC post data to the Digital Order
            foreach($_POST as $key => $value) {
                if (strlen($value) > 0) {
                    $conn->addDigitalOrderField($key, $value);
                }
            }

            // Add original order HTML so that another transaction can be attempted.
            $conn->addDigitalOrderField("AgainLink", $againLink);

            // Obtain a one-way hash of the Digital Order data and add this to the Digital Order
            $secureHash = $conn->hashAllFields();
            $conn->addDigitalOrderField("Title", $title);
            $conn->addDigitalOrderField("vpc_SecureHash", $secureHash);
            $conn->addDigitalOrderField("vpc_SecureHashType", "SHA256");

            // Obtain the redirection URL and redirect the web browser
            $vpcURL = $conn->getDigitalOrder($vpcURL);
            header("Location: ".$vpcURL);

    }

    function tempgenerateinvoice() {

                         $discounttype = '';


         $id_session = session_id();
                $listOfCourses = $this->dashboard_model->gerProgrammeFromSession($id_session);

                $discountStaff = $this->dashboard_model->getDiscountFromStaff($id_session);
                 $discountAmount = $discountStaff[0]->amount;
                 if($discountAmount>0) {
                    $discounttype = "AEU Student / Staff ";
                    $is_discount=0;
                 }


                 if($discountStaff[0]->id_discount>0) {
                        $discounttype = "Coupon code - ";
                                            $is_discount=$discountStaff[0]->id_discount;

                 }
// discount_amount
// discount_type
// id_fee_structures


                $invoicegenerated = array();
                for($k=0;$k<count($listOfCourses);$k++)
                {
                  $data = array();
                    $feeStructures = explode('@',$listOfCourses[$k]->id_feestructure);
                  $data['id_student'] = $this->id_student;

                  $data['id_fee_structures'] = $feeStructures;
                  $data['type'] = 'Student';
                  $data['is_discount'] = $is_discount;
                  $data['discount_type'] = $discounttype;
                  $data['discount_amount'] = $discountAmount;
                  $data['id_programme'] = $listOfCourses[$k]->id_programme;

                  $idinvoiceNumber = $this->generateInvoice($data);
          $programme = $this->dashboard_model->getProgramme($listOfCourses[$k]->id_programme);
          // print_R($programme);exit;

            if($programme)
            {                        
                $max_duration = $programme->max_duration;
                $duration_type = $programme->duration_type;

                $start_date = date('Y-m-d');

                $data_student_has_programme = array(
                    'id_student' => $this->id_student,
                    'id_invoice' => $idinvoiceNumber,
                    'marks_status'=>'Pending',
                    'id_programme' => $listOfCourses[$k]->id_programme,
                    'start_date' => $start_date,
                    'end_date' => date('Y-m-d', strtotime($start_date . "+" . $max_duration." ".$duration_type) )
                );
                $this->dashboard_model->addNewStudentHasProgramme($data_student_has_programme);
                // $this->insertintomoodle($this->id_student,$listOfCourses[$k]->id_programme);

                ///
            }
            array_push($invoicegenerated,$idinvoiceNumber);         
        }

        // echo "<Pre>"; print_r($invoicegenerated);exit;

        $datareceipt['id_student'] = $this->id_student;
        $datareceipt['type'] = 'Student';
        $datareceipt['id_currency'] = '1';
        // $datareceipt['receipt_amount'] = 49;
        $datareceipt['receipt_amount'] = $ReceiptAmount;
        $datareceipt['id_payment_type'] = 3;
        $datareceipt['id_invoice'] = $invoicegenerated;
        // echo "<Pre>"; print_r($datareceipt);exit;
        $this->generateReceipt($datareceipt);
        
        echo "asdf";exit;
    }

    function paymentresponse()
    {

print_R($_GET);exit;

        $insertArray = array(
            'id_student'=>$this->id_student ,
            'vpc_3DSECI'=>$_GET[vpc_3DSECI],
            'vpc_3DSXID'=>$_GET[vpc_3DSXID],
            'vpc_3DSenrolled'=>$_GET[vpc_3DSenrolled],
            'vpc_3DSstatus'=>$_GET[vpc_3DSstatus],
            'vpc_AVSRequestCode'=>$_GET[vpc_AVSRequestCode],
            'vpc_AVSResultCode'=>$_GET[vpc_AVSResultCode],
            'vpc_AcqAVSRespCode'=>$_GET[vpc_AcqAVSRespCode],
            'vpc_AcqCSCRespCode'=>$_GET[vpc_AcqCSCRespCode],
            'vpc_AcqResponseCode'=>$_GET[vpc_AcqResponseCode],
            'vpc_Amount'=>$_GET[vpc_Amount],
            'vpc_AuthorizeId'=>$_GET[vpc_AuthorizeId],
            'vpc_BatchNo'=>$_GET[vpc_BatchNo],
            'vpc_CSCResultCode'=>$_GET[vpc_CSCResultCode],
            'vpc_Card'=>$_GET[vpc_Card],
            'vpc_Command'=>$_GET[vpc_Command],
            'vpc_Currency'=>$_GET[vpc_Currency],
            'vpc_Locale'=>$_GET[vpc_Locale],
            'vpc_MerchTxnRef'=>$_GET[vpc_MerchTxnRef],
            'vpc_Merchant'=>$_GET[vpc_Merchant],
            'vpc_Message'=>$_GET[vpc_Message],
            'vpc_OrderInfo'=>$_GET[vpc_OrderInfo],
            'vpc_ReceiptNo'=>$_GET[vpc_ReceiptNo],
            'vpc_SecureHash'=>$_GET[vpc_SecureHash],
            'vpc_SecureHashType'=>$_GET[vpc_SecureHashType],
            'vpc_TransactionNo'=>$_GET[vpc_TransactionNo],
            'vpc_TxnResponseCode'=>$_GET[vpc_TxnResponseCode],
            'vpc_VerSecurityLevel'=>$_GET[vpc_VerSecurityLevel],
            'vpc_VerStatus'=>$_GET[vpc_VerStatus],
            'vpc_VerToken'=>$_GET[vpc_VerToken],
            'vpc_VerType'=>$_GET[vpc_VerType],
            'vpc_Version'=>$_GET[vpc_Version]
        );

        $this->dashboard_model->insertafterTransaction($insertArray);

        if($_GET['vpc_Message']=='Approved'  && $_GET['vpc_TxnResponseCode']=='0') {


            $ReceiptAmount = $_GET[vpc_Amount]/100;

                $id_session = session_id();
                $listOfCourses = $this->dashboard_model->gerProgrammeFromSession($id_session);

                $discountStaff = $this->dashboard_model->getDiscountFromStaff($id_session);
                 $discountAmount = $discountStaff[0]->amount;
                $invoicegenerated = array();
                for($k=0;$k<count($listOfCourses);$k++)
                {
                  $data = array();
                    $feeStructures = explode('@',$listOfCourses[$k]->id_feestructure);
                  $data['id_student'] = $this->id_student;

                  $data['id_fee_structures'] = $feeStructures;
                  $data['type'] = 'Student';
                  $data['is_discount'] = 0;
                  $data['discount_type'] = 'Staff';
                  $data['discount_amount'] = $discountAmount;
                  $data['id_programme'] = $listOfCourses[$k]->id_programme;
                  $idinvoiceNumber = $this->generateInvoice($data);

                  $programme = $this->dashboard_model->getProgramme($listOfCourses[$k]->id_programme);

                    if($programme)
                    {                        
                        $max_duration = $programme->max_duration;
                        $duration_type = $programme->duration_type;

                        $start_date = date('Y-m-d');

                        $data_student_has_programme = array(
                            'id_student' => $this->id_student,
                                'id_invoice' => $idinvoiceNumber,
                                'marks_status'=>'Pending',
                            'id_programme' => $listOfCourses[$k]->id_programme,
                            'start_date' => $start_date,
                            'end_date' => date('Y-m-d', strtotime($start_date . "+" . $max_duration." ".$duration_type) )
                        );
                        $this->dashboard_model->addNewStudentHasProgramme($data_student_has_programme);
                        $this->insertintomoodle($this->id_student,$listOfCourses[$k]->id_programme);

                        ///
                    }
                  array_push($invoicegenerated,$idinvoiceNumber);         
                }

                $datareceipt['id_student'] = $this->id_student;
                $datareceipt['type'] = 'Student';
                $datareceipt['id_currency'] = '1';
                $datareceipt['receipt_amount'] = $ReceiptAmount;
                $datareceipt['id_payment_type'] = 3;
                $datareceipt['id_invoice'] = $invoicegenerated;
                $this->generateReceipt($datareceipt);
                $this->global = '';
                $data = '';

                //empty cart 
                $this->emptycart();
                 echo "<script>parent.location='/profile/dashboard/success'</script>";
                exit;

        }
        else
        {
               echo "<script>parent.location='/profile/dashboard/failure'</script>";
                exit;

        }
    }

     function paymentresponsesenpaylocal()
    {

        // if($_GET['status_id']=='1') {


                $id_session = session_id();
                $listOfCourses = $this->dashboard_model->gerProgrammeFromSession($id_session);

                $discountStaff = $this->dashboard_model->getDiscountFromStaff($id_session);


                $totalAmount=0;
                   $discountAmount = 0;
                for($l=0;$l<count($listOfCourses);$l++) {
                $totalAmount =  $totalAmount + $listOfCourses[$l]->amount;
              }
               $invoiceTotal = $totalAmount;
               $discountCode = 0;

              if($discountStaff) { 

                  if($discountStaff[0]->amount>0) { 
                    $discountAmount = number_format($discountStaff[0]->amount,2);
                    $discountCode =  $discountStaff[0]->code;
                   }


               } 

                $totalAmount = $totalAmount- $discountAmount;
                $totalAmount = number_format($totalAmount,2);



                     $invoice_number = $this->generateMainInvoiceNumber();

                        $invoice['invoice_number'] = $invoice_number;
                        $invoice['type'] = 'Student';
                        $invoice['remarks'] = 'Invoice';
                        $invoice['id_student'] = $this->id_student;
                        $invoice['currency'] = '1';
                        $invoice['total_amount'] = $totalAmount;
                        $invoice['original_amount'] = $invoiceTotal;
                        $invoice['discount_amount'] = $discountAmount;
                        $invoice['discount_code'] = $discountCode;
                        $invoice['balance_amount'] = '0';
                        $invoice['paid_amount'] = '0';
                        $invoice['amount_before_gst'] = '0';
                        $invoice['gst_amount'] = '0';
                        $invoice['gst_percentage'] = 0;
                        $invoice['status'] = 'ACTIVE';
                        // $invoice['is_migrate_applicant'] = $id;
                        $inserted_id = $this->dashboard_model->addNewMainInvoice($invoice);



        

                for($k=0;$k<count($listOfCourses);$k++)
                {




                    $getPartnerShareDetails = $this->dashboard_model->getPartnerShareDetails($listOfCourses[$k]->id_programme);
                    $speedShare = 0;
                    $partnerShare = 0;

                              if($getPartnerShareDetails->revenue_type=='Fixed Amount')
                              {
                                $speedShare = $getPartnerShareDetails->speed_value;
                                $partnerShare = $getPartnerShareDetails->partner_value;
                              }
                              if($getPartnerShareDetails->revenue_type!='Fixed Amount')
                              {

                                $speedShare = ($listOfCourses[$k]->amount*$getPartnerShareDetails->speed_value)/100;
                                $partnerShare = ($listOfCourses[$k]->amount*$getPartnerShareDetails->partner_value)/100;
                              }


                    $programmeDetails = $this->dashboard_model->getProgrammeDetailsById($listOfCourses[$k]->id_programme);

                     $invoiceDetails = array(
                                'id_main_invoice' => $inserted_id,
                                'id_fee_structure' => $listOfCourses[$k]->id_feestructure,
                                'amount' => $listOfCourses[$k]->amount,
                                'speed_share' => $speedShare,
                                'partner_share' => $partnerShare,
                                'quantity' => 1,
                                'id_programme' => $listOfCourses[$k]->id_programme,
                                'description' => 'Course Registration Fee',
                                'status' => 1,
                                'created_by' => 0
                                );

                     $start_date = date('Y-m-d',strtotime($programmeDetails->start_date));

                     $increase = '+'.$programmeDetails->max_duration.' '.$programmeDetails->duration_type;    
                     $end_date = date("d M Y", strtotime($start_date . $increase));


                     $data_student_has_programme =array(
                                'id_main_invoice' => $inserted_id,
                                'id_programme' => $listOfCourses[$k]->id_programme,
                                'id_student' => $this->id_student,
                                'start_date' => $start_date,
                                'end_date' => $end_date,
                    'marks_status'=>'Pending',

                            );




                        $this->dashboard_model->addNewMainInvoiceDetails($invoiceDetails);

                        $this->dashboard_model->addNewStudentHasProgramme($data_student_has_programme);



                }

                // Reciept master
                $payment_mode='Senang Pay';

                 $receipt_number = $this->dashboard_model->generateReceiptNumber();

                    $receipr_data = array(
                        'receipt_date' =>date('Y-m-d'),
                        'id_student' => $this->id_student,
                        'receipt_number' => $receipt_number,
                        'type' => 'Student',
                        'currency' => 1,
                        'receipt_amount' => $totalAmount,
                        'remarks' => $payment_mode,
                        'status' => '1'
                    );

                    $receiptId = $this->dashboard_model->addNewReceipt($receipr_data);


                $detailsData = array(
                'id_receipt' => $receiptId,
                'id_main_invoice' => $inserted_id,
                'invoice_amount' => $totalAmount,
                'paid_amount' => $totalAmount,                        
                'status' => '1',
                'created_by' => 1
                );
                
                                $id_receipt_details = $this->dashboard_model->addNewReceiptDetails($detailsData);
                
        
    }


    function paymentresponsesenpay()
    {

        if($_GET['status_id']=='1') {


                $id_session = session_id();
                $listOfCourses = $this->dashboard_model->gerProgrammeFromSession($id_session);

                $discountStaff = $this->dashboard_model->getDiscountFromStaff($id_session);


                                   $totalAmount=0;
                   $discountAmount = 0;
                for($l=0;$l<count($listOfCourses);$l++) {
                $totalAmount =  $totalAmount + $listOfCourses[$l]->amount;
              }
               $invoiceTotal = $totalAmount;
               $discountCode = 0;

              if($discountStaff) { 

                  if($discountStaff[0]->amount>0) { 
                    $discountAmount = number_format($discountStaff[0]->amount,2);
                    $discountCode =  $discountStaff[0]->code;
                   }


               } 

                $totalAmount = $totalAmount- $discountAmount;
                $totalAmount = number_format($totalAmount,2);



                     $invoice_number = $this->generateMainInvoiceNumber();

                        $invoice['invoice_number'] = $invoice_number;
                        $invoice['type'] = 'Student';
                        $invoice['remarks'] = 'Invoice';
                        $invoice['id_student'] = $this->id_student;
                        $invoice['currency'] = '1';
                        $invoice['total_amount'] = $totalAmount;
                        $invoice['original_amount'] = $invoiceTotal;
                        $invoice['discount_amount'] = $discountAmount;
                        $invoice['discount_code'] = $discountCode;
                        $invoice['balance_amount'] = '0';
                        $invoice['paid_amount'] = '0';
                        $invoice['amount_before_gst'] = '0';
                        $invoice['gst_amount'] = '0';
                        $invoice['gst_percentage'] = 0;
                        $invoice['status'] = 'ACTIVE';
                        // $invoice['is_migrate_applicant'] = $id;
                        $inserted_id = $this->dashboard_model->addNewMainInvoice($invoice);



        

                for($k=0;$k<count($listOfCourses);$k++)
                {


                    $programmeDetails = $this->dashboard_model->getProgrammeDetailsById($listOfCourses[$k]->id_programme);

                     $invoiceDetails = array(
                                'id_main_invoice' => $inserted_id,
                                'id_fee_structure' => $listOfCourses[$k]->id_feestructure,
                                'amount' => $listOfCourses[$k]->amount,
                              
                                'quantity' => 1,
                                'id_programme' => $listOfCourses[$k]->id_programme,
                                'description' => 'Course Registration Fee',
                                'status' => 1,
                                'created_by' => 0
                                );

                     $start_date = date('Y-m-d',strtotime($programmeDetails->start_date));

                     $increase = '+'.$programmeDetails->max_duration.' '.$programmeDetails->duration_type;    
                     $end_date = date("d M Y", strtotime($start_date . $increase));


                     $data_student_has_programme =array(
                                'id_main_invoice' => $inserted_id,
                                'id_programme' => $listOfCourses[$k]->id_programme,
                                'id_student' => $this->id_student,
                                'start_date' => $start_date,
                                'end_date' => $end_date,
                    'marks_status'=>'Pending',

                            );




                        $this->dashboard_model->addNewMainInvoiceDetails($invoiceDetails);

                        $this->dashboard_model->addNewStudentHasProgramme($data_student_has_programme);
                        // $this->insertintomoodle($this->id_student,$listOfCourses[$k]->id_programme);

                }

                // Reciept master
                $payment_mode='Senang Pay';

                 $receipt_number = $this->dashboard_model->generateReceiptNumber();

                    $receipr_data = array(
                        'receipt_date' =>date('Y-m-d'),
                        'id_student' => $this->id_student,
                        'receipt_number' => $receipt_number,
                        'type' => 'Student',
                        'currency' => 1,
                        'receipt_amount' => $totalAmount,
                        'remarks' => $payment_mode,
                        'status' => '1'
                    );

                    $receiptId = $this->dashboard_model->addNewReceipt($receipr_data);


                $detailsData = array(
                'id_receipt' => $receiptId,
                'id_main_invoice' => $inserted_id,
                'invoice_amount' => $totalAmount,
                'paid_amount' => $totalAmount,                        
                'status' => '1',
                'created_by' => 1
                );
                
                                $id_receipt_details = $this->dashboard_model->addNewReceiptDetails($detailsData);
                $this->loadViews('dashboard/success',$this->global,$data,NULL);

        }
        else
        {
            $this->loadViews('dashboard/failure',$this->global,$data,NULL);
        }
        
    }


     function generateMainInvoiceNumber()
    {
        $Year = date('Y');
            $this->db->select('j.*');
            $this->db->from('main_invoice as j');
            $this->db->order_by("id", "desc");
            $query = $this->db->get();
            $result = $query->num_rows();

     
            $count= $result + 1;
           $jrnumber = $number = "INV" .(sprintf("%'06d", $count)). "/" . $Year;
           return $jrnumber;        
    }


    function emptycart() {
                $id_session = session_id();

$this->dashboard_model->emptycartfunction($id_session);
    }



    function insertintomoodle($idstudent,$idprogramme){


            //get moodleid fro student
        $studentDetails = $this->dashboard_model->getStudentDetailsById($idstudent);

        $studentMoodleId = $studentDetails->moodle_id;



        $programmeDetails = $this->dashboard_model->getProgrammeDetailsById($idprogramme);

        $programmeMoodleId = $programmeDetails->moodle_id;

           // get moodle id for course


        $functionName = 'enrol_manual_enrol_users';
                $rand = rand(00000000,999999999);

                $user1 = new stdClass();
                $user1->roleid = 5;
                $user1->userid = $studentMoodleId;
                $user1->courseid = $programmeMoodleId;


                $users = array($user1);
                $params = array('enrolments' => $users);

                
                $restformat = "json";
                $serverurl = "https://lms.myeduskills.com/webservice/rest/server.php?wstoken=".LOGIN.'&wsfunction=' . $functionName. '&moodlewsrestformat=' . $restformat;
                require_once ('curl.php');
                $curl = new curl();


                $resp = $curl->post($serverurl, $params);
              
   if($resp=="null") {
                    $this->dashboard_model->updatemoodlestatus($idstudent,$idprogramme);
                }

    }


    function generateReceipt($data)
    {


        $id_student = $data['id_student'];
        $type = $data['type'];
        $id_currency = $data['id_currency'];
        $receipt_amount = $data['receipt_amount'];
        $id_payment_type = $data['id_payment_type'];
        $payment_mode = 'Online';
        $inserted_id = 0;

        $payment_type = $this->dashboard_model->getPaymentType($id_payment_type);

        if($payment_type)
        {
            $payment_mode = $payment_type->description;
        }

        $receipt_number = $this->dashboard_model->generateReceiptNumber();

        $receipr_data = array(
            'receipt_date' =>date('Y-m-d'),
            'id_student' => $id_student,
            'receipt_number' => $receipt_number,
            'type' => $type,
            'currency' => $id_currency,
            'receipt_amount' => $receipt_amount,
            'remarks' => $payment_mode,
            'status' => '1'
        );


        // $inserted_id = 1;
        $inserted_id = $this->dashboard_model->addNewReceipt($receipr_data);

        if($inserted_id)
        {

            for($i=0;$i<count($data['id_invoice']);$i++)
            {
                $id = $data['id_invoice'][$i];

                // echo "<Pre>"; print_r($id);exit;
                 
                $main_invoice = $this->dashboard_model->getMainInvoice($id);
                $id_programme = $main_invoice->id_program;

                $detailsData = array(
                'id_receipt' => $inserted_id,
                'id_main_invoice' => $id,
                'invoice_amount' => $main_invoice->total_amount,
                'paid_amount' => $main_invoice->balance_amount,                        
                'status' => '1',
                'created_by' => 1
                );
                
                //print_r($details);exit;
                
                $id_receipt_details = $this->dashboard_model->addNewReceiptDetails($detailsData);


                if($id_receipt_details)
                {
                    $update_invoice['balance_amount'] = '0'; 
                    $update_invoice['paid_amount'] = $main_invoice->balance_amount;

                    // echo "<Pre>";print_r($update_invoice);exit;
                    $this->dashboard_model->editMainInvoice($update_invoice,$id);

                    $update_student_has_programme['status'] = 1;

                    $this->dashboard_model->editStudentHasProgramme($update_student_has_programme,$id,$id_programme,$id_student);


                    $paid_details = array(
                        'id_receipt' => $inserted_id,
                        'id_payment_type' => 'Online Payment',
                        'payment_reference_number' => '',
                        'paid_amount' => $receipt_amount,                        
                        'status' => '1',
                        'created_by' => 0
                        );

                    $this->dashboard_model->addNewReceiptPaymentDetails($paid_details); 
                }
            }
        }

        return $inserted_id;

    }


    
}