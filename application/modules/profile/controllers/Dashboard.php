<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

// Create the Razorpay Order

use Razorpay\Api\Api;

class Dashboard extends BaseController
{
    public function __construct()
    {

        parent::__construct();
        $this->load->model('dashboard_model');
        
        $this->id_student =  $this->session->userdata['id_student'];
        
        if(empty($this->id_student))
        {
            redirect('/index');
        }
        error_reporting(0);
    }


    function ssoold($id) {

    $studentDetails = $this->dashboard_model->getStudentDetais($this->id_student);


        // $user1 = new stdClass();
         $email = $studentDetails->email_id;

       

         $params = [
            'user' => [
                'email' => $studentDetails->email_id
            ]
        ];

        $serverurl = "https://lms.myeduskills.com/webservice/rest/server.php?wstoken=721f6638fb484690528dad6d71d72fcd&wsfunction=auth_userkey_request_login_url&moodlewsrestformat=json";
        require_once ('curl.php');
        $curl = new curl();
        $resp = $curl->post($serverurl, $params);
        $responseArray = json_decode($resp);
        $url = $responseArray->loginurl;


        $data['url'] = $url;

        echo "<script>parent.location='$url'</script>";
        exit;
    }


    function sso($id) {

    $result = $studentDetails = $this->dashboard_model->getStudentDetais($this->id_student);

        $studentMoodleId = $studentDetails->moodle_id;


        //
        if($studentMoodleId>0)   
            {

            } else {

             $functionName = 'core_user_create_users';
                $rand = rand(00000000,999999999);

                $user1 = new stdClass();
                $user1->username = $rand;
                $user1->password = 'Abc12345678*';
                $user1->firstname = $result->full_name;
                $user1->lastname = $result->full_name;
                $user1->email = $result->email_id;





                $users = array($user1);
                $params = array('users' => $users);

                /// REST CALL
                $restformat = "json";
                $serverurl = 'https://lms.myeduskills.com/webservice/rest/server.php?wstoken='.TOKEN.'&wsfunction=' . $functionName. '&moodlewsrestformat=' . $restformat;
                require_once ('curl.php');
                $curl = new curl();


                $resp = $curl->post($serverurl, $params);


                ///echo '</br>************************** Server Response    createUser()**************************</br></br>';
                ///echo $serverurl . '</br></br>';

                $responseArray = json_decode($resp);
                //print_R($responseArray[0]->id);
                $studentdata = array();
               $studentMoodleId = $studentdata['moodle_id'] = $responseArray[0]->id;
                $this->dashboard_model->editStudent($studentdata, $result->id);



        }


    $programmeDetails = $this->dashboard_model->getprogrammebyMoodleid($id);

        $programmeMoodleId = $id;
           // get moodle id for course


        $functionName = 'enrol_manual_enrol_users';
                $rand = rand(00000000,999999999);

                $user1 = new stdClass();
                $user1->roleid = 5;
                $user1->userid = $studentMoodleId;
                $user1->courseid = $programmeMoodleId;


                $users = array($user1);
                $params = array('enrolments' => $users);


                
                $restformat = "json";
                $serverurl = "https://lms.myeduskills.com/webservice/rest/server.php?wstoken=".LOGIN.'&wsfunction=' . $functionName. '&moodlewsrestformat=' . $restformat;
                require_once ('curl.php');
                $curl = new curl();


                $resp = $curl->post($serverurl, $params);



                  if($resp=="null") {

                    $this->dashboard_model->updatemoodlestatus($this->id_student,$programmeDetails->id);
                } 



         $params = [
            'user' => [
                'email' => $studentDetails->email_id
            ]
        ];

       $serverurl = "https://lms.myeduskills.com/webservice/rest/server.php?wstoken=".LOGIN."&wsfunction=auth_userkey_request_login_url&moodlewsrestformat=json";
        require_once ('curl.php');
        $curl = new curl();


        $resp = $curl->post($serverurl, $params);



        $responseArray = json_decode($resp);

  // echo '</br>************************** Server Response    createUser()**************************</br></br>';
  //       echo $responseArray . '</br></br>';
  //       exit;

       $url = $responseArray->loginurl.'&wantsurl=https://lms.myeduskills.com/course/view.php?id='.$id;



        $data['url'] = $url;

        echo "<script>parent.location='$url'</script>";
        exit;
    }


    function index()
    {
        $id_session = session_id();

        if($this->input->post())
        {

            $billing['name'] = $this->input->post('name');
            $billing['email'] = $this->input->post('email');
            $billing['mobile'] = $this->input->post('mobile');

            $this->dashboard_model->updateProfile($billing,$this->id_student);
        }

        $data['studentDetails'] = $this->dashboard_model->getStudentDetais($this->id_student);
        $data['courseListPending'] = $this->dashboard_model->getStudentProgrammeActive($this->id_student);

        $data['courseListCompleted'] = $this->dashboard_model->getStudentProgrammeCompleted($this->id_student);




         $data['recomendedprogrammeList'] = $this->dashboard_model->getrelevantcourses();


         $data['programmeblock'] = $this->dashboard_model->getProgrammeblock();

         for($i=0;$i<count($data['programmeblock']);$i++) {
            if($i==0) {
                $pname = "'".$data['programmeblock'][$i]->code."'";
            } else {
                $pname = $pname.","."'".$data['programmeblock'][$i]->code."'";
            }
         }
         $data['pname'] = $pname;
        $this->global['pageTitle'] = 'Examination Management System : Exam Has Question List';
        $this->loadViews("dashboard/index", $this->global, $data, NULL);
    }

    function saveprofile(){

        if($_POST['current_profile']) {
            $billing['current_profile'] = $_POST['current_profile'];
        }
         if($_POST['next_profile']) {
            $billing['next_profile'] = $_POST['next_profile'];
        }
         if($_POST['learning_goals']) {
            $billing['learning_goals'] = $_POST['learning_goals'];
        }
        $this->dashboard_model->updateProfile($billing,$this->id_student);

    }

    function autologin() {


        // $functionName = 'core_user_create_users';
        // $rand = rand(00000000,999999999);

                 $studentDetails = $this->dashboard_model->getStudentDetais($this->id_student);


        // $user1 = new stdClass();
         $email = $studentDetails->email_id;

         $users = array($user1);
         $params = [
            'user' => [
                'email' => 't@g.com'
            ]
        ];


        // $params['user']['email'] = $studentDetails->email_id;


        /// REST CALL
        $serverurl = "https://lms.myeduskills.com/webservice/rest/server.php?wstoken=".LOGIN."&wsfunction=auth_userkey_request_login_url&moodlewsrestformat=json";
        require_once ('curl.php');
        $curl = new curl();

          // print_R($params);

        $resp = $curl->post($serverurl, $params);


        // echo '</br>************************** Server Response    createUser()**************************</br></br>';
        // echo $serverurl . '</br></br>';
        $responseArray = json_decode($resp);
        print_R($responseArray);


       $url = $responseArray->loginurl.'&wantsurl=https://lms.myeduskills.com/course/view.php?id=63';

       echo $url;exit;


    }

    function coursedetails()
    {
        $id_session = session_id();

        $data['studentDetails'] = $this->dashboard_model->getStudentDetais($this->id_student);


        $data['courseList'] = $this->dashboard_model->getStudentProgramme($this->id_student);
        $this->global['pageTitle'] = 'Examination Management System : Exam Has Question List';
        $this->loadViews("dashboard/coursedetails", $this->global, $data, NULL);
    }

    function invoice()
    {
        $id_session = session_id();

        $data['listOfInvoices'] = $this->dashboard_model->getMainInvoiceByStudentId($this->id_student);
        // print_r($data);
        $data['studentDetails'] = $this->dashboard_model->getStudentDetais($this->id_student);

        $this->global = '';
        $this->loadViews('dashboard/invoice',$this->global,$data,NULL);
    }

    function paymentgateway()
    {
        echo "<script>parent.location='/profile/payment/paymentsenang'</script>";
        exit;
        $id_session = session_id();

        $data['listOfCourses'] = $this->dashboard_model->gerProgrammeFromSession($id_session);
        $data['staffDiscount'] = $this->dashboard_model->getDiscountFromStaff($id_session);

        $this->loadViews('dashboard/paymentgateway',$this->global,$data,NULL);
    }

    function failure()
    {
     

        $this->loadViews('dashboard/failure',$this->global,$data,NULL);
    }

 function success()
    {
     

        $this->loadViews('dashboard/success',$this->global,$data,NULL);
    }





    function soa()
    {
        $id_session = session_id();

        $data['listOfInvoices'] = $this->dashboard_model->getMainInvoiceByStudentId($this->id_student);
        // print_r($data);
        $data['listOfReceipts'] = $this->dashboard_model->getReceiptList($this->id_student);
        $data['studentDetails'] = $this->dashboard_model->getStudentDetais($this->id_student);

        $this->global = '';
        $this->loadViews('dashboard/soa',$this->global,$data,NULL);
    }

    function profile()
    {
        if($this->input->post())
        {

               if($_FILES['image'])
                {

                    $certificate_name = date('YmdHis').$_FILES['image']['name'];
                    $certificate_size = $_FILES['image']['size'];
                    $certificate_tmp =$_FILES['image']['tmp_name'];
                    
                    // echo "<Pre>"; print_r($certificate_tmp);exit();

                    $certificate_ext=explode('.',$certificate_name);
                    $certificate_ext=end($certificate_ext);
                    $certificate_ext=strtolower($certificate_ext);


                    $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'Image File');

                    $image_file = $this->uploadFile($certificate_name,$certificate_tmp,'Image File');
                    $billing['profile_pic'] = $certificate_name;
                }

                      $billing['whatsapp_number'] = $this->input->post('whatsapp_number');
                      $billing['phone'] = $this->input->post('phone');
                      $billing['email_id'] = $this->input->post('email_id');
                      $billing['last_name'] = $this->input->post('last_name');
                      $billing['first_name'] = $this->input->post('first_name');
                      $billing['salutation'] = $this->input->post('salutation');

                      $billing['employer'] = $this->input->post('employer');
                      $billing['degree'] = $this->input->post('degree');
                      $billing['university'] = $this->input->post('university');
                      $billing['field_major'] = $this->input->post('field_major');
                      $billing['career_occupation'] = $this->input->post('career_occupation');
                      $billing['industry'] = $this->input->post('industry');
                       $billing['occupation'] = $this->input->post('occupation');
                       $billing['experience_level'] = $this->input->post('experience_level');
                      // $billing['field_major'] = $this->input->post('field_major');
                      // $billing['field_major'] = $this->input->post('field_major');

            $this->dashboard_model->updateProfile($billing,$this->id_student);
                redirect('/profile/dashboard/profile');

             }

                   $data['salutationList'] = $this->dashboard_model->salutationListSearch();

        $data['studentDetails'] = $this->dashboard_model->getStudentDetais($this->id_student);
         
        $this->global['pageTitle'] = 'Examination Management System : Exam Has Question List';
        $this->loadViews("dashboard/profile", $this->global, $data, NULL);
    }


    function password()
    {
        if($this->input->post())
        {
            $password = $this->input->post('salutation');
            $billing['password'] = md5($password);

            $this->dashboard_model->updateProfile($billing,$this->id_student);
            redirect('/profile/dashboard/password');
        }

        $data['studentDetails'] = $this->dashboard_model->getStudentDetais($this->id_student);

        $this->global['pageTitle'] = 'Examination Management System : Exam Has Question List';
        $this->loadViews("dashboard/password", $this->global, $data, NULL);
    }


    function success1()
    {

        $id_session = session_id();
        $listOfCourses = $this->dashboard_model->gerProgrammeFromSession($id_session);

         $discountStaff = $this->dashboard_model->getDiscountFromStaff($id_session);

        
        for($k=0;$k<count($listOfCourses);$k++)
        {
          $data = array();

          $data['id_student'] = $this->id_student;
          $data['type'] = 'Student';
          $data['is_discount'] = 0;
          $data['id_programme'] = $listOfCourses[$k]->id_programme;


          $idinvoiceNumber = $this->generateInvoice($data);

          $invoicegenerated = array();
          array_push($invoicegenerated,$idinvoiceNumber);         
        }

        $datareceipt['id_student'] = $this->id_student;
        $datareceipt['type'] = 'Student';
        $datareceipt['id_currency'] = '1';
        $datareceipt['receipt_amount'] = '100';
        $datareceipt['id_payment_type'] = 3;
        $datareceipt['id_invoice'] = $invoicegenerated;
        $this->generateReceipt($datareceipt);


        $this->global = '';
        $data = '';
        $this->loadViews('dashboard/success',$this->global,$data,NULL);
    }

    function createNewMainInvoiceForStudent()
    {
        $datareceipt['id_student'] = 137;
        $datareceipt['type'] = 'Student';
        $datareceipt['discount_type'] = 'Staff';
        $datareceipt['is_discount'] = 0;
        $datareceipt['id_programme'] = 47;
        $datareceipt['discount_amount'] = 6;
        $datareceipt['id_fee_structures'] = array(
            0 => 0,
            1 => 311
        );

        // echo "<Pre>";print_r($datareceipt);exit;


        $data = $this->dashboard_model->createNewMainInvoiceForStudent($datareceipt);
        
        return $data;
    }
}