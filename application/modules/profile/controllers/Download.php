<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

// Create the Razorpay Order

use Razorpay\Api\Api;

class Download extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('download_model');
        error_reporting(0);
    }

    function generateMainInvoice($id)
    {
    	$id_main_invoice = $id;

        // To Get Mpdf Library
        $this->getMpdfLibrary();

            
            $mpdf=new \Mpdf\Mpdf(); 

        

            $currentDate = date('d-m-Y');
            $currentTime = date('h:i:s a');
            $currentDateTime = date('d_m_Y_His');

        $organisationDetails = $this->download_model->getOrganisation();


        // echo "<Pre>";print_r($organisationDetails);exit;

        

     $signature = $_SERVER['DOCUMENT_ROOT']."/website/img/myeduskills_logo.svg";





        $mainInvoice = $this->download_model->getMainInvoice($id_main_invoice);




        $type = $mainInvoice->type;
        $invoice_number = $mainInvoice->invoice_number;
        $date_time = $mainInvoice->date_time;
        $remarks = $mainInvoice->remarks;
        $currency = $mainInvoice->currency_name;

        if($mainInvoice->total_amount>0) {
            $total_amount = $mainInvoice->total_amount;
        } else {
            $total_amount = "FREE";
        }

        if($mainInvoice->invoice_total>0) {
        $invoice_total = $mainInvoice->invoice_total;
        } else {
            $invoice_total = "FREE";
        }

        $balance_amount = $mainInvoice->balance_amount;
        $paid_amount = $mainInvoice->paid_amount;
        $programme_name = $mainInvoice->programme_name;
        $programme_code = $mainInvoice->programme_code;
        if($date_time)
        {
            $date_time = date('d-m-Y', strtotime($date_time));
        }

        $invoice_generation_name = strtoupper($mainInvoice->full_name);


            $file_data = "";


        $file_data.="<table align='center' width='100%'>
        <tr>
                  <td style='text-align: left;font-size:30px;'><b>INVOICE</b></td>
                  <td style='text-align: center' width='30%' ></td>

          <td style='text-align: right' width='40%' ><img src='$signature' width='180px' /></td>
          
        </tr>
       
        
        <tr>
          <td style='text-align: center' width='100%'  colspan='3'> <br/><br/><br/></td>
        </tr>
        </table>";

            $invoice_type = $mainInvoice->type;


            $file_data = $file_data ."

            <table width='100%' style='font-size:16px;'>
            <tr>
             <td>$invoice_type Name : $invoice_generation_name</td>
             <td width='25%'></td>
             <td style='text-align:right;'>Invoice No  : $invoice_number</td>
             <td style='text-align:right;'></td>
             </tr>
              <tr>
             <td></td>
             <td></td>
             <td style='text-align:right;'>Invoice Date: $date_time</td>
             <td style='text-align:right;'></td>
             </tr>
             </table>




             <table width='100%' height='50%'  style='margin-top:30px;border-collapse: collapse;padding:10px 10px;height:75%;font-size:16px;' border='1'>
              <tr>
               <th style='text-align:center;line-height:30px;'><b>No</b></th>
               <th style='text-align:center;'><b>COURSE NAME</b></th>
               <th style='text-align:center;'><b>UNIT PRICE (RM)</b></th>
               <th style='text-align:right;'><b>TOTAL (RM)</b></th>
              </tr>
              
              ";

        // 


       

                $mainInvoiceDetailsList = $this->download_model->getMainInvoiceDetails($id_main_invoice);


           for($k=0;$k<count($mainInvoiceDetailsList);$k++) { 
            $programmeName = $mainInvoiceDetailsList[$k]->programme_name;
            $amount = $mainInvoiceDetailsList[$k]->amount;
            if($amount>0) {

            } else {
                $amount = "FREE";
            }
            $p = $k + 1;
               $file_data = $file_data ."<tr>
                              <td style='text-align:center;'>$p</td>

               <td style='text-align:center;'>$programmeName</td>
               <td style='text-align:center;'>$amount</td>
               <td style='text-align:right;'>$amount</td>
               </tr>";
           }





 if($invoice_total=='FREE') {
                $invoice_total = 0;
            }

             if($total_amount=='FREE') {
                $total_amount = 0;
            }


            $amount_c = $total_amount;

           
            $invoice_total = number_format($invoice_total, 2, '.', ',');
            $total_amount = number_format($total_amount, 2, '.', ',');


       

            $amount_word = $this->getAmountWordings($amount_c);

            $amount_word = ucwords($amount_word);

            if($total_amount>0) {

            } else {
                $total_amount = "FREE";
            }


            $file_data = $file_data ."



                <tr>
                   <td colspan='3' style='text-align:center;padding-top:20px;padding-bottom:10px;'><b>GRAND TOTAL</b></td>
                   <td style='text-align:right;padding-top:20px;padding-bottom:10px;'><b>$total_amount</b></td>
                </tr>

              ";

                    $file_data = $file_data ."
                       </table>";




        






        $bankDetails = $this->download_model->getBankRegistration();


        if($bankDetails && $organisationDetails)
        {
            $bank_name = $bankDetails->name;
            $bank_code = $bankDetails->code;
            $account_no = $bankDetails->account_no;
            $state = $bankDetails->state;
            $country = $bankDetails->country;
            $address = $bankDetails->address;
            $city = $bankDetails->city;
            $zipcode = $bankDetails->zipcode;
            

            $organisation_name = $organisationDetails->name;




        $file_data = $file_data ."<br/><br/>
        <p>1. All cheque should be crossed and make payable to:: </p>
        <table align='center' width='100%' style='font-size:16px;'>
          <tr>
                <td style='text-align: left' width='30%' valign='top'>PAYEE</td>
                <td style='text-align: center' width='5%' valign='top'>:</td>
                <td style='text-align: left' width='65%'>$organisation_name</td>
          </tr>

          <tr>
                <td style='text-align: left' width='30%' valign='top'>BANK</td>
                <td style='text-align: center' width='5%' valign='top'>:</td>
                <td style='text-align: left' width='65%'>$bank_name</td>
          </tr>

          <tr>
                <td style='text-align: left' width='30%' valign='top'>ADDRESS</td>
                <td style='text-align: center' width='5%' valign='top'>:</td>
                <td style='text-align: left' width='65%'>$address , $city , $state , $country - $zipcode</td>
          </tr>

          <tr>
                <td style='text-align: left' width='30%' valign='top'>ACCOUNT NO</td>
                <td style='text-align: center' width='5%' valign='top'>:</td>
                <td style='text-align: left' width='65%'>$account_no</td>
          </tr>
          <tr>
                <td style='text-align: left' width='30%' valign='top'>SWIFT CODE</td>
                <td style='text-align: center' width='5%' valign='top'>:</td>
                <td style='text-align: left' width='65%'>$bank_code</td>
          </tr>

          
        </table>
        <p> 2. This is auto generated Receipt. No signature is required. </p>
          ";


        }


    

            $mpdf->WriteHTML($file_data);
            $mpdf->Output($type . '_INVOICE_'.$invoice_number.'_'.$currentDateTime.'.pdf', 'D');
            exit;
    }



    function generateReceipt($id_receipt)
    {

        // echo "<pre>"; print_r($id_receipt);exit;
        
        $this->getMpdfLibrary();


           // echo "<pre>"; print_r($receiptFor);exit;

            // include("/home/camsedu/public_html/assets/mpdf/vendor/autoload.php");
            //  require_once __DIR__ . '/vendor/autoload.php';
            
            $mpdf=new \Mpdf\Mpdf(); 

            // $mpdf->SetHeader("<div style='text-align: left;'>Campus Management System
            //                    </div>");

        $organisationDetails = $this->download_model->getOrganisation();

        // echo "<Pre>";print_r($organisationDetails);exit;


     $signature = $_SERVER['DOCUMENT_ROOT']."/website/img/myeduskills_logo.svg";

        // if($organisationDetails->image != '')
        // {
        //     $signature = $_SERVER['DOCUMENT_ROOT']."/assets/images/" . $organisationDetails->image;
        // }

        // echo "<Pre>";print_r($signature);exit;
        

            $currentDate = date('d-m-Y');
            $currentTime = date('h:i:s a');
            $currentDateTime = date('d_m_Y_His');

        

         $receipt = $this->download_model->getReceipt($id_receipt);

        if($receipt->type == 'Student')
        {
            $receiptFor = $this->download_model->getMainInvoiceStudentData($receipt->id_student);
            $studentDetails = $this->download_model->getStudentByStudentId($receipt->id_student);

            $receipt_generation_name = strtoupper($studentDetails->full_name);
            $receipt_generation_nric = $studentDetails->nric;

        }
        elseif($receipt->type == 'CORPORATE')
        {
            $receiptFor = $this->download_model->getMainInvoiceCorporateData($receipt->id_student);

            $receipt_generation_name = strtoupper($receiptFor->full_name);
            $receipt_generation_nric = $receiptFor->nric;
        }

        // echo "<pre>";print_R($data['receiptFor']);exit;
        

        // echo "<Pre>";print_r($receiptFor);exit;


        $type = $receipt->type;
        $receipt_number = $receipt->receipt_number;
        $receipt_date = $receipt->receipt_date;
        $remarks = $receipt->remarks;
        $receipt_amount = $receipt->receipt_amount;
        $programme_name = $receipt->programme_name;
        $programme_code = $receipt->programme_code;


        if($receipt_date)
        {
            $receipt_date = date('d-m-Y', strtotime($receipt_date));
        }


    

            $file_data = "";

        $file_data.="<table align='center' width='100%'>
        <tr>
                  <td style='text-align: left;font-size:30px;'><b>OFFICIAL RECEIPT</b></td>
                  <td style='text-align: center' width='30%' ></td>

          <td style='text-align: right' width='40%' ><img src='$signature' width='180px' /></td>
          
        </tr>
       
        
        <tr>
          <td style='text-align: center' width='100%'  colspan='3'> <br/><br/><br/></td>
        </tr>
        </table>";



            $file_data = $file_data ."

            <table width='100%' style='font-size:16px;'>
            <tr>
             <td>Received From : $receipt_generation_name </td>
             <td width='25%'></td>
             <td style='text-align:right;'>Receipt No: $receipt_number</td>
             <td style='text-align:right;'></td>
             </tr>
              <tr>
             <td>IC No / Passport No: $receipt_generation_nric </td>
             <td></td>
             <td style='text-align:right;'>Receipt Date: $receipt_date</td>
             <td style='text-align:right;'></td>
             </tr>
             </table>




             <table width='100%' height='50%'  style='margin-top:30px;border-collapse: collapse;padding:10px 10px;height:75%;font-size:16px;' border='1'>
              <tr>
               <th style='text-align:center;line-height:30px;'><b>No</b></th>
               <th style='text-align:center;'><b>DESCRIPTION</b></th>
               <th style='text-align:right;'><b>TOTAL (RM)</b></th>
              </tr>
               <tr>
               <td style='padding-top:20px;padding-bottom:15px;'></td>
               <td style='padding-top:20px;padding-bottom:15px;text-align:center;'>BEING PAYMENT FOR:</td>
               <td style='padding-top:20px;padding-bottom:15px;'></td>
               </tr>
              ";


  
        $invoiceDetails = $this->download_model->getReceiptInvoiceDetails($id_receipt);
    
        // echo "<pre>"; print_r($invoiceDetails);exit;
    

      $total_receipt_detail = 0;
      $i = 1;
      
      foreach ($invoiceDetails as $value)
      {

        $id_invoice = $value->id;
        $remarks = $value->remarks;
        $invoice_number = $value->invoice_number;
        $invoice_total = $value->invoice_total;
        $total_amount = $value->total_amount;
        $paid_amount = $value->paid_amount;
        $balance_amount = $value->balance_amount;
        $total_discount = $value->total_discount;
        $balance_amount = $value->balance_amount;
        $currency = $value->currency_name;

        // $amount = number_format($amount, 2, '.', ',');

            // $acqDate   = date("d/m/Y", strtotime($acqDate));

        if($remarks == 'Student Course Registration')
        {
            $semester = $this->download_model->getSemesterCourseRegistrationByInvoiceId($id_invoice);
            // echo "<pre>"; print_r($semester);exit;

            $remarks = "Semester : " . $semester->semester_code . " - " . $semester->semester_name;


        }
            $paid_amount = number_format($paid_amount, 2, '.', ',');

       $file_data .= "
      <tr>
        <td>$i</td>
        <td>$invoice_number, $remarks</td>
        <td style='text-align:right;'>$paid_amount</td>
      </tr>";

      $total_receipt_detail = $total_receipt_detail + $paid_amount;
      $i++;

      }


      $paymentDetails = $this->download_model->getSingleReceiptPaymentDetails($id_receipt);
      $payment_type = ' ';
      if($paymentDetails)
      {
        $payment_type = $paymentDetails->id_payment_type;
      }



          $file_data .= "
            <tr>
                <td></td>
                <td>Payment Mode : $payment_type</td>
                <td></td>
            </tr>
          ";




            $amount_word = $this->getAmountWordings($receipt_amount);

            $amount_word = ucwords($amount_word);

            $receipt_amount = number_format($receipt_amount, 2, '.', ',');


          
           $file_data .= "
          <tr>
            <td></td>
            <td style='text-align:right;'><b>TOTAL AMOUNT RECEIVED:</b></td>
            <td style='text-align:right;'>$receipt_amount</td>
          </tr>
          <tr>
            <td colspan='3'  style='text-align:center;'><font size='3'><b>$currency : $amount_word</b></font></td>
          </tr>
          <tr>
            <td colspan='3'  style='text-align:left;'><font size='3'>Issued by:<br>Finance & Accounts Department</font></td>
          </tr>
          <tr>
            <td colspan='3'  style='text-align:left;'><font size='3'>This is auto generated Official Receipt. No signature is required. </font></td>
          </tr>
          </table>";

            // echo "<pre>"; print_r($file_data);exit;

            
          $file_data .="
        <pagebreak>";



            // $mpdf->SetFooter('<div>Campus Management System</div>');
            // echo $file_data;exit;

            $mpdf->WriteHTML($file_data);
            $mpdf->Output($type . '_RECEIPT_'.$receipt_number.'_'.$currentDateTime.'.pdf', 'D');
            exit;
    

    }
	
}?>