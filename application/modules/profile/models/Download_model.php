<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Download_model extends CI_Model
{
    function getOrganisation()
    {
        $this->db->select('fc.*');
        $this->db->from('organisation as fc');
        $this->db->where('fc.status', 1);
        $this->db->order_by("fc.id", "DESC");
        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }



    function getMainInvoice($id)
    {
        $this->db->select('mi.*,  p.name as programme_name, p.code as programme_code,s.full_name');
        $this->db->from('main_invoice as mi');
        $this->db->from('main_invoice_details as mid','mi.id=mid.id_invoice','left');
        $this->db->join('programme as p', 'mid.id_programme = p.id','left');
        $this->db->join('student as s', 's.id = mi.id_student','left');

        $this->db->where('mi.id', $id);
        $this->db->where('mi.type', 'Student');

        $query = $this->db->get();
        return $query->row();
    }

    function getMainInvoiceDetails($id)
    {
        $this->db->select('mid.*,p.name as programme_name, p.code as programme_code');
        $this->db->from('main_invoice_details as mid');
        $this->db->join('programme as p', 'mid.id_programme = p.id','left');

        $this->db->where('mid.id_main_invoice', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function getMainInvoiceDiscountDetails($id)
    {
        $this->db->select('*');
        $this->db->from('main_invoice_discount_details');   
        $this->db->where('id_main_invoice', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function getMainInvoiceCorporateData($id)
    {
        $this->db->select('stu.name as full_name, stu.registration_number as nric');
        $this->db->from('company as stu');
        $this->db->where('stu.id', $id);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getMainInvoiceStudentData($id_student)
    {
        $this->db->select('stu.full_name, stu.nric, stu.id_degree_type');
        $this->db->from('student as stu');
        $this->db->where('stu.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getMainInvoiceDetailsForCourseRegistrationShow($id)
    {
        $this->db->select('mid.*, fstp.name as fee_setup, fm.code as frequency_mode, amt.code as amount_calculation_type');
        $this->db->from('main_invoice_details as mid');
        // $this->db->join('fee_structure as fs', 'mid.id_fee_item = fs.id');        
        $this->db->join('fee_setup as fstp', 'mid.id_fee_item = fstp.id');        
        $this->db->join('frequency_mode as fm', 'fstp.id_frequency_mode = fm.id');      
        $this->db->join('amount_calculation_type as amt', 'fstp.id_amount_calculation_type = amt.id');        
        $this->db->where('mid.id_main_invoice', $id);
        $query = $this->db->get();
        $results = $query->result();
        $details = array();
        foreach ($results as $result)
        {
        // echo "<Pre>";print_r($results);exit;

            if($result->id_reference > 0 &&  $result->description == 'CREDIT HOUR MULTIPLICATION')
            {
               $course_registered = $this->getCourseRegistered($result->id_reference);
               $result->course_code = $course_registered->code;
               $result->course_name = $course_registered->name;

            }

           array_push($details, $result);
        }

        return $details;
    }

    function getCourseRegistered($id)
    {
        $this->db->select('c.*');
        $this->db->from('course_registration as cr');
        $this->db->join('course as c', 'cr.id_course = c.id');      
        $this->db->where('cr.id', $id);
        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    function getSemesterByMainInvoiceDetailsForCourseRegistrationShow($id)
    {
        $this->db->select('s.*');
        $this->db->from('main_invoice_details as mid');      
        $this->db->join('course_registration as cr', 'mid.id_reference = cr.id');        
        $this->db->join('semester as s', 'cr.id_semester = s.id');
        $this->db->where('mid.description', 'CREDIT HOUR MULTIPLICATION');
        $this->db->where('mid.id_main_invoice', $id);
        $query = $this->db->get();
        $result = $query->row();

        return $result;
    }

    function getMainInvoiceHasStudentList($id)
    {
        $this->db->select('pmhs.*, s.full_name as student_name, s.nric');
        $this->db->from('main_invoice_has_students as pmhs');   
        $this->db->join('student as s', 'pmhs.id_student = s.id');
        $this->db->where('pmhs.id_main_invoice', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function getBankRegistration()
    {
        $this->db->select('fc.*, c.name as country, s.name as state');
        $this->db->from('bank_registration as fc');
        $this->db->join('country as c', 'fc.id_country = c.id');
        $this->db->join('state as s', 'fc.id_state = s.id');
        $this->db->where('fc.status', 1);
        $this->db->order_by("fc.id", "DESC");
        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    function getStudentByStudentId($id_student)
    {
        $this->db->select('s.*');
        $this->db->from('student as s');
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getReceipt($id)
    {
        $this->db->select('re.*');
        $this->db->from('receipt as re');
        $this->db->where('re.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getReceiptInvoiceDetails($id)
    {
        $this->db->select('r.*,mi.*, r.paid_amount, cs.name as currency_name');
        $this->db->from('receipt_details as r');
        $this->db->join('main_invoice as mi', 'mi.id = r.id_main_invoice');
        $this->db->join('currency_setup as cs', 'mi.currency = cs.id','left');
        $this->db->where('r.id_receipt', $id);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }
    

    function getReceiptPaymentDetails($id)
    {
        $this->db->select('r.*');
        $this->db->from('receipt_paid_details as r');
        $this->db->where('r.id_receipt', $id);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getSemesterCourseRegistrationByInvoiceId($id_invoice)
    {
        $this->db->select('fc.*, s.name as semester_name, s.code as semester_code');
        $this->db->from('main_invoice_details as fc');
        $this->db->join('course_registration as c', 'fc.id_reference = c.id');
        $this->db->join('semester as s', 'c.id_semester = s.id');
        $this->db->order_by("fc.id", "DESC");
        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }
    
    function getSingleReceiptPaymentDetails($id)
    {
        $this->db->select('r.*');
        $this->db->from('receipt_paid_details as r');
        $this->db->where('r.id_receipt', $id);
        $this->db->order_by("r.id", "DESC");
         $query = $this->db->get();
         $result = $query->row();  
         return $result;
    }
}
?>