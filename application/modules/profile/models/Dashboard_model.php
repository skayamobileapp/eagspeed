<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard_model extends CI_Model
{
    function roleListingCount()
    {
        $this->db->select('BaseTbl.id as roleId, BaseTbl.role');
        $this->db->from('roles as BaseTbl');
        $query = $this->db->get();
        
        return $query->num_rows();
    }

    function getDiscountFromStaff($id)
    {
        $this->db->select('tc.*,d.code');
        $this->db->from('temp_cart_discount as tc');
        $this->db->join("discount as d", "tc.id_discount = d.id",'left');    
        $this->db->where('tc.id_session', $id);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function updatemoodlestatus($idstudent,$idprogramme) {
        $sqlupdate="Update student_has_programme set moodle_view='1' where id_student='$idstudent' and id_programme='$idprogramme' ";  
       $this->db->query($sqlupdate);        
    }

    function getprogrammebyMoodleid($id) {
         $this->db->select('ihs.*');
        $this->db->from('programme as ihs');
        $this->db->where('ihs.moodle_id', $id);
        $query = $this->db->get();
        return $query->row();
    }


     function emptycartfunction($id)
       {
           $this->db->where('id_session', $id);
           $this->db->delete('temp_cart');
           return TRUE;
       }


    function getPartnerShareDetails($id) {
        $this->db->select('tc.*');
        $this->db->from('agreement_course_details as tc');
        $this->db->where('tc.id_programme', $id);
         $query = $this->db->get();
         $result = $query->row();  
         return $result;
    }

    function getrelevantcourses(){

         $this->db->select('p.*,f.amount as amount,c.name as cattype,sl.name as studylevel,sf.name as staffname,sf.image as staffimage,pt.name as producttype');
        $this->db->from('programme as p');
        $this->db->join("product_type as pt", "p.id_programme_type = pt.id",'left');    
        $this->db->join("category as c", "p.id_category = c.id",'left');    
        $this->db->join("study_level as sl", "p.id_study_level = sl.id",'left');    
        $this->db->join('fee_structure_master as f', 'f.id_programme = p.id','left');    
        $this->db->join('programme_has_staff as ps', 'ps.id_programme = p.id','left');    
        $this->db->join('staff as sf', 'sf.id = ps.id_staff','left');    
        $this->db->where("p.status='Approved and Published'");
        $this->db->group_by("p.id");
        $this->db->order_by('rand()');
        $this->db->limit(3);             
        $query = $this->db->get();
        $result = $query->result();  
        return $result;
    }

    function getProgrammeblock(){

         $this->db->select('tc.*');
        $this->db->from('programme as tc');
        $this->db->where("tc.id_programme_type='2'");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getCourses($id)
    {
        $this->db->select('tc.*,ct.name as categoryname, c.name as coursename,c.file,ct.image');
        $this->db->from('temp_cart as tc');
        $this->db->join('product as c', 'tc.id_product = c.id');
        $this->db->join('category as ct', 'c.id_category = ct.id');

        $this->db->where('tc.id_session', $id);
        $query = $this->db->get();
        $result = $query->result();  
        return $result;
     }

    function salutationListSearch()
    {
        $this->db->select('ct.*');
        $this->db->from('salutation_setup as ct');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


    function getProgramme($id)
    {
        $this->db->select('ihs.*');
        $this->db->from('programme as ihs');
        $this->db->where('ihs.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

   
    function addNewStudentHasProgramme($data)
    {
        $this->db->trans_start();
        $this->db->insert('student_has_programme', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getStudentDetailsById($id){
        $this->db->select('p.*');
        $this->db->from('student as p');
        $this->db->where("p.id",$id);
        $query = $this->db->get();
        return $query->row();
    }

    function getProgrammeDetailsById($id)
    {
        $this->db->select('p.*');
        $this->db->from('programme as p');
        $this->db->where("p.id",$id);
        $query = $this->db->get();
        return $query->row();    
    }

     function editStudent($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('student', $data);
        return TRUE;
    }
    


    function getStudentProgrammeActive($id) {
        $this->db->select('p.*,sp.start_date as startdateofprogramme,sp.end_date as enddateofprogramme,sp.created_dt_tm');
        $this->db->from('programme as p');
        $this->db->join('student_has_programme as sp', 'sp.id_programme = p.id');
        $this->db->where("sp.id_student",$id);
        $this->db->where("sp.marks_status!='Approved'");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


     function getStudentProgrammeCompleted($id) {
        $this->db->select('p.*,sp.final_marks');
        $this->db->from('programme as p');
        $this->db->join('student_has_programme as sp', 'sp.id_programme = p.id');
        $this->db->where("sp.id_student",$id);
        $this->db->where("sp.marks_status ='Approved'");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


    function getStudentProgramme($id)
    {
        $this->db->select('p.*,sp.start_date,sp.end_date');
        $this->db->from('programme as p');
        $this->db->join('student_has_programme as sp', 'sp.id_programme = p.id');
        $this->db->where("sp.id_student",$id);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    
    function insertBeforeTransaction($data)
    {
        $this->db->trans_start();
        $this->db->insert('migs_payment_transaction', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function insertafterTransaction($data)
    {
        $this->db->trans_start();
        $this->db->insert('migs_payment_response', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function getMainInvoiceByStudentId($id)
    {
        $this->db->select('mi.*');
        $this->db->from('main_invoice as mi');
        $this->db->join('student as s', 'mi.id_student = s.id');
        $this->db->where('s.id', $id);
        $query = $this->db->get();
        $result = $query->result();  
        return $result;
    }

    function getProgrammeByProductTypeId($id)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('id_programme_type', $id);

         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getStudentDetais($id)
    {
        $this->db->select('ct.*');
        $this->db->from('student as ct');
        $this->db->where('ct.id', $id);
        $query = $this->db->get();
        $result = $query->row();  
        return $result;
    }

    function getProductType()
    {
        $this->db->select('ct.*');
        $this->db->from('product_type as ct');
        $this->db->join('programme as p', 'p.id_programme_type=ct.id');
        $this->db->group_by('ct.id');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getCertificateNames($id_category)
    {
        $this->db->select('p.*');
        $this->db->from('programme as p');
        $this->db->where('p.id_category', $id_category);
        $query = $this->db->get();
        $result = $query->result();  
        return $result;
    }

    function getCoursesById($id)
    {
        $this->db->select('ct.*');
        $this->db->from('product as ct');
        $this->db->where('ct.id', $id);

        $query = $this->db->get();
        $result = $query->row();  
        return $result;
    }
    
    function gerProgrammeFromSession($id)
    {
        $this->db->select('tc.*');
        $this->db->from('temp_cart as tc');
        $this->db->where('tc.id_session', $id);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    // Generation Of Main invoice

    function createNewMainInvoiceForStudent($data)
    {


                        
                        $finance_configuration = $this->getFinanceConfiguration();
                        $gst_percentage = $finance_configuration->tax_sst;


                        $invoice_number = $this->generateMainInvoiceNumber();

                        $invoice['invoice_number'] = $invoice_number;
                        $invoice['type'] = $type;
                        $invoice['remarks'] = 'Payment';
                        $invoice['id_application'] = '0';
                        $invoice['id_student'] = $id_student;
                        $invoice['id_program'] = $id_programme;
                        $invoice['currency'] = $id_currency;
                        $invoice['total_amount'] = '0';
                        $invoice['balance_amount'] = '0';
                        $invoice['paid_amount'] = '0';
                        $invoice['amount_before_gst'] = '0';
                        $invoice['gst_amount'] = '0';
                        $invoice['gst_percentage'] = $gst_percentage;
                        $invoice['status'] = '1';
                        // $invoice['is_migrate_applicant'] = $id;
                        $invoice['created_by'] = 0;

                        
                        $inserted_id = $this->addNewMainInvoice($invoice);
                        // echo "<Pre>";print_r($inserted_id);exit;

                        

                        $total_amount = 0;
                        $total_amount_before_gst = 0;
                        $total_gst_amount = 0;
                        $total_discount_amount = 0;
                        $sibling_discount_amount = 0;
                        $employee_discount_amount = 0;
                        $alumni_discount_amount = 0;




                                $data = array(
                                    'id_main_invoice' => $inserted_id,
                                    'id_fee_item' => $id_fee_item,
                                    'amount' => $amount,
                                    'amount_before_gst' => $amount_before_gst,
                                    'gst_amount' => $details_gst_amount,
                                    'gst_percentage' => $gst_tax,
                                    'price' => $amount,
                                    'quantity' => 1,
                                    'id_reference' => $id_fee_structure,
                                    'description' => 'Course Registration Fee',
                                    'status' => 1,
                                    'created_by' => 0
                                );

                                $total_amount = $total_amount + $amount;
                                // $total_amount_before_gst = $total_amount_before_gst + $amount_before_gst;
                                $total_gst_amount = $total_gst_amount + $details_gst_amount;

                                $inserted = $this->addNewMainInvoiceDetails($data);
                          

         return $inserted_id;
    }

       function updateProfile($data,$id)
    {

        $this->db->where('id', $id);
        $this->db->update('student', $data);

        return TRUE;
    }



    function getFeeStructureByIdProgramme($id_programme)
    {
        $this->db->select('*');
        $this->db->from('fee_structure_master');
        $this->db->where('id_programme', $id_programme);
        $this->db->order_by("id", "DESC");
        $query = $this->db->get();
        $result = $query->row();
        // print_r($result);exit();     
        return $result;
    }

    function getFeeStructureMasterById($id)
    {
        $this->db->select('*');
        $this->db->from('fee_structure_master');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getFeeStructure($id)
    {
        $this->db->select('*');
        $this->db->from('fee_structure');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getFeeStructureByData($data)
    {
        $this->db->select('fst.*, fs.name as fee_structure, fs.code as fee_structure_code, cs.name as currency_name');
        $this->db->from('fee_structure as fst');
        $this->db->join('fee_setup as fs', 'fst.id_fee_item = fs.id');
        $this->db->join('currency_setup as cs', 'fst.currency = cs.id','left'); 
        // $this->db->where('fst.id_training_center', $data['id_training_center']);
        $this->db->where('fst.id_program_landscape', $data['id_fee_structure']);
        $this->db->where('fst.currency', $data['currency']);
        // $this->db->where('fstp.name', $data['trigger']);
        $this->db->where('fst.status', '1');
        $query = $this->db->get();

        $result = $query->result();
        // echo "<Pre>";print_r($result);exit();
        return $result;
    }

    function getFinanceConfiguration()
    {
        $this->db->select('*');
        $this->db->from('finance_configuration');
        $this->db->order_by("id", "ASC");
        $query = $this->db->get();
        return $query->row();
    }

    function generateMainInvoiceNumber()
    {
        $Year = date('Y');
            $this->db->select('j.*');
            $this->db->from('main_invoice as j');
            $this->db->order_by("id", "desc");
            $query = $this->db->get();
            $result = $query->num_rows();

     
            $count= $result + 1;
           $jrnumber = $number = "INV" .(sprintf("%'06d", $count)). "/" . $Year;
           return $jrnumber;        
    }

    function addNewMainInvoice($data)
    {
        $this->db->trans_start();
        $this->db->insert('main_invoice', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function editApplicantDetails($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('student', $data);
        return TRUE;
    }

    function getFeeSetup($id)
    {
        $this->db->select('fst.*');
        $this->db->from('fee_setup as fst');
        $this->db->where('fst.id', $id);
        $query = $this->db->get();
        $result = $query->row();
        return $result;
    }

    function getReceiptList($id){
        $this->db->select('mi.*');
        $this->db->from('receipt as mi');
        $this->db->join('student as s', 'mi.id_student = s.id');
        $this->db->where('s.id', $id);
        $query = $this->db->get();
        $result = $query->result();  
        return $result;
    }

    function addNewMainInvoiceDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('main_invoice_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function editMainInvoice($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('main_invoice', $data);
        return TRUE;
    }

    function addNewMainInvoiceDiscountDetail($data)
    {
        $this->db->trans_start();
        $this->db->insert('main_invoice_discount_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }



    // Generate Receipt

    function getPaymentType($id)
    {
        $this->db->select('ct.*');
        $this->db->from('payment_type as ct');
        $this->db->where('ct.id', $id);
        $query = $this->db->get();
        $result = $query->row();  
        return $result;
    }

    function generateReceiptNumber()
    {
        $year = date('y');
        $Year = date('Y');
        
        $this->db->select('j.*');
        $this->db->from('receipt as j');
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        $result = $query->num_rows();

 
        $count= $result + 1;
       $jrnumber = $number = "REC" .(sprintf("%'06d", $count)). "/" . $Year;
       return $jrnumber;
    }

    function addNewReceipt($data)
    {
        $this->db->trans_start();
        $this->db->insert('receipt', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function getMainInvoice($id)
    {
        $this->db->select('mi.*, cs.name as currency_name, p.name as programme_name, p.code as programme_code,s.full_name');
        $this->db->from('main_invoice as mi');
        $this->db->join('currency_setup as cs', 'mi.currency = cs.id','left');
        $this->db->join('student as s', 's.id = mi.id_student','left');
        $this->db->join('programme as p', 'mi.id_program = p.id','left');
        $this->db->where('mi.id', $id);
        $this->db->where('mi.type', 'Student');
        $query = $this->db->get();
        return $query->row();
    }

    function addNewReceiptDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('receipt_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    // function addMainInvoicePaidDetails($data)
    // {
    //     $this->db->trans_start();
    //     $this->db->insert('receipt_paid_details', $data);
    //     $insert_id = $this->db->insert_id();
    //     $this->db->trans_complete();

    //     return $insert_id;

    //     // $this->db->where('id', $id);
    //     // $this->db->update('main_invoice', $data);
    //     // return $this->db->affected_rows();
    // }

    function addNewReceiptPaymentDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('receipt_paid_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editStudentHasProgramme($data,$id_invoice,$id_programme,$id_student)
    {
        $this->db->where('id_invoice', $id_invoice);
        $this->db->where('id_programme', $id_programme);
        $this->db->where('id_student', $id_student);
        $this->db->update('student_has_programme', $data);
        return TRUE;
    }
}