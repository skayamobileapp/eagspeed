<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class FundCode extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('fund_code_model');
        $this->isLoggedIn();
    }

    function list()
    {

        if ($this->checkAccess('fund_code.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;

            $data['fundCodeList'] = $this->fund_code_model->fundCodeListSearch($name);
            $this->global['pageTitle'] = 'FIMS : List Fund Code';
            //print_r($subjectDetails);exit;
            $this->loadViews("fund_code/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('fund_code.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'code' => $code,
                    'name' => $name,
                    'status' => $status
                );

                $duplicate_row = $this->fund_code_model->checkFundCodeDuplication($data);

                if($duplicate_row)
                {
                    echo "Duplicate Fund Code Not Allowed";exit();
                }
            
                $result = $this->fund_code_model->addNewFundCode($data);
                redirect('/glsetup/fundCode/list');
            }
            
            $this->global['pageTitle'] = 'FIMS : Add Fund Code';
            $this->loadViews("fund_code/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('fund_code.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/glsetup/fund_code/list');
            }
            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'code' => $code,
                    'name' => $name,
                    'status' => $status
                );

                $duplicate_row = $this->fund_code_model->checkFundCodeDuplicationEdit($data,$id);

                if($duplicate_row)
                {
                    echo "Duplicate Fund Code Not Allowed";exit();
                }
                
                $result = $this->fund_code_model->editFundCode($data,$id);
                redirect('/glsetup/fundCode/list');
            }
            $data['fundCode'] = $this->fund_code_model->getFundCode($id);
            $this->global['pageTitle'] = 'FIMS : Edit Fund Code';
            $this->loadViews("fund_code/edit", $this->global, $data, NULL);
        }
    }
}
