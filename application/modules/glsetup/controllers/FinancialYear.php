<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class FinancialYear extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('financial_year_model');
        $this->isLoggedIn();
    }

    function list()
    {

        if ($this->checkAccess('financial_year.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;

            $data['financialYearList'] = $this->financial_year_model->financialYearListSearch($name);
            $this->global['pageTitle'] = 'FIMS : List Financial Year';
            //print_r($subjectDetails);exit;
            $this->loadViews("financial_year/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('financial_year.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $year = $this->security->xss_clean($this->input->post('year'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'year' => $year,
                    'name' => $year,
                    'status' => $status
                );

                $duplicate_row = $this->financial_year_model->checkFinancialYearDuplication($data);

                if($duplicate_row)
                {
                    echo "Duplicate Financial Year Not Allowed";exit();
                }
            
                $result = $this->financial_year_model->addNewFinancialYear($data);
                redirect('/glsetup/financialYear/list');
            }
            
            $this->global['pageTitle'] = 'FIMS : Add Financial Year';
            $this->loadViews("financial_year/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('financial_year.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/glsetup/financial_year/list');
            }
            if($this->input->post())
            {
                $year = $this->security->xss_clean($this->input->post('year'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'year' => $year,
                    'name' => $year,
                    'status' => $status
                );

                $duplicate_row = $this->financial_year_model->checkFinancialYearDuplicationEdit($data,$id);

                if($duplicate_row)
                {
                    echo "Duplicate Financial Year Not Allowed";exit();
                }

                
                $result = $this->financial_year_model->editFinancialYear($data,$id);
                redirect('/glsetup/financialYear/list');
            }
            $data['financialYear'] = $this->financial_year_model->getFinancialYear($id);
            $this->global['pageTitle'] = 'FIMS : Edit Financial Year';
            $this->loadViews("financial_year/edit", $this->global, $data, NULL);
        }
    }
}
