<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class TaxCode extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('tax_code_model');
        $this->isLoggedIn();
    }

    function list()
    {

        if ($this->checkAccess('tax_code.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;

            $data['activityCodeList'] = $this->tax_code_model->activityCodeListSearch($name);
            //print_r($subjectDetails);exit;
            $this->global['pageTitle'] = 'FIMS : List TaxCode';
            $this->loadViews("tax_code/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('tax_code.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
        	$id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $percentage = $this->security->xss_clean($this->input->post('percentage'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'code' => $code,
                    'name' => $name,
                    'percentage' => $percentage,
                    'status' => $status,
                    'created_by' => $user_id
                );
            
                $result = $this->tax_code_model->addNewActivityCode($data);
                redirect('/glsetup/taxCode/list');
            }
            
            $this->global['pageTitle'] = 'FIMS : Add Activity Code';
            $this->loadViews("tax_code/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('tax_code.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/glsetup/taxCode/list');
            }
            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $percentage = $this->security->xss_clean($this->input->post('percentage'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'code' => $code,
                    'name' => $name,
                    'percentage' => $percentage,
                    'status' => $status,
                    'updated_by' => $user_id
                );

                
                $result = $this->tax_code_model->editActivityCode($data,$id);
                redirect('/glsetup/activityCode/list');
            }
            $data['activityCode'] = $this->tax_code_model->getActivityCode($id);
            $this->global['pageTitle'] = 'FIMS : Edit TaxCode';
            $this->loadViews("tax_code/edit", $this->global, $data, NULL);
        }
    }
}
