<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Tax_model extends CI_Model
{
    function taxList()
    {
        $this->db->select('*');
        $this->db->from('tax');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function taxListSearch($search)
    {
        $this->db->select('*');
        $this->db->from('tax');
        if (!empty($search))
        {
            $likeCriteria = "(name  LIKE '%" . $search . "%' or code  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getTax($id)
    {
        $this->db->select('*');
        $this->db->from('tax');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function checkTaxCodeDuplication($data)
    {
        $this->db->select('*');
        $this->db->from('tax');
        $this->db->where('code', $data['code']);
        $query = $this->db->get();
        return $query->row();
    }

    function checkTaxCodeDuplicationEdit($data,$id)
    {
        $this->db->select('*');
        $this->db->from('tax');
        $this->db->where('code', $data['code']);
        $this->db->where('id !=', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewTax($data)
    {
        $this->db->trans_start();
        $this->db->insert('tax', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editTax($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('tax', $data);
        return TRUE;
    }
}

