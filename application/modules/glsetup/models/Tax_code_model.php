<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Tax_code_model extends CI_Model
{
    function taxCodeList()
    {
        $this->db->select('*');
        $this->db->from('tax_code');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function taxCodeListSearch($search)
    {
        $this->db->select('*');
        $this->db->from('tax_code');
        if (!empty($search))
        {
            $likeCriteria = "(code  LIKE '%" . $search . "%' or name  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function getTaxCode($id)
    {
        $this->db->select('*');
        $this->db->from('tax_code');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewTaxCode($data)
    {
        $this->db->trans_start();
        $this->db->insert('tax_code', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editTaxCode($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('tax_code', $data);
        return TRUE;
    }
}

