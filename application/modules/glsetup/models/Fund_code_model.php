<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Fund_code_model extends CI_Model
{
    function fundCodeList()
    {
        $this->db->select('*');
        $this->db->from('fund_code');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function fundCodeListSearch($search)
    {
        $this->db->select('*');
        $this->db->from('fund_code');
        if (!empty($search))
        {
            $likeCriteria = "(code  LIKE '%" . $search . "%' or name  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("code", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function getFundCode($id)
    {
        $this->db->select('*');
        $this->db->from('fund_code');
        $this->db->where('id', $id);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->row();
    }

    function checkFundCodeDuplication($data)
    {
        $this->db->select('*');
        $this->db->from('fund_code');
        $this->db->where('code', $data['code']);
        $query = $this->db->get();
        return $query->row();
    }

    function checkFundCodeDuplicationEdit($data,$id)
    {
        $this->db->select('*');
        $this->db->from('fund_code');
        $this->db->where('code', $data['code']);
        $this->db->where('id !=', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewFundCode($data)
    {
        $this->db->trans_start();
        $this->db->insert('fund_code', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editFundCode($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('fund_code', $data);
        return TRUE;
    }
}

