<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Department_code_model extends CI_Model
{
    function departmentCodeList()
    {
        $this->db->select('*');
        $this->db->from('department_code');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function departmentCodeListSearch($search)
    {
        $this->db->select('*');
        $this->db->from('department_code');
        if (!empty($search))
        {
            $likeCriteria = "(code  LIKE '%" . $search . "%' or name  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function getDepartmentCode($id)
    {
        $this->db->select('*');
        $this->db->from('department_code');
        $this->db->where('id', $id);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->row();
    }

    function checkDepartmentCodeDuplication($data)
    {
        $this->db->select('*');
        $this->db->from('department_code');
        $this->db->where('code', $data['code']);
        $query = $this->db->get();
        return $query->row();
    }

    function checkDepartmentCodeDuplicationEdit($data,$id)
    {
        $this->db->select('*');
        $this->db->from('department_code');
        $this->db->where('code', $data['code']);
        $this->db->where('id !=', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewDepartmentCode($data)
    {
        $this->db->trans_start();
        $this->db->insert('department_code', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editDepartmentCode($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('department_code', $data);
        return TRUE;
    }
}

