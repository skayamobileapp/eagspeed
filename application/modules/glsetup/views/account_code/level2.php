<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Account Code Level 2</h3>
        </div>
        <form id="form_grade" action="" method="post">

        <div class="form-container">
        <h4 class="form-group-title">Level 2</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Code Level 1 <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="code1" name="code1" value="<?php echo $accountCode->code; ?>" readonly="readonly">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="code" name="code">
                    </div>
                </div>
                

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name">
                    </div>
                </div>


                <!--  <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                            </label>                              
                        </div>                         
                </div> -->
                
            </div>

        </div>
        
        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="../add" class="btn btn-link">Cancel</a>
            </div>
        </div>

        </form>



        <div class="form-container">
        <h4 class="form-group-title">Level 2 List</h4>

         <div class="custom-table">
            <table class="table">
                <thead>
                    <tr>
                    <th>Sl. No</th>
                    <th>Level 1 CODE</th>
                     <th>CODE</th>
                     <th>Name</th>
                     <th style="text-align:center; ">Action</th>
                    </tr>
                </thead>
                <tbody>
                     <?php
                 $total = 0;
                  for($i=0;$i<count($accountCodeList);$i++)
                 { ?>
                    <tr>
                    <td><?php echo $i+1;?></td>
                    <td><?php echo $accountCode->code;?></td>
                    <td><?php echo $accountCodeList[$i]->code;?></td>
                    <td><?php echo $accountCodeList[$i]->name;?></td>
                    <td style="text-align: center;">
                        <a class="btn btn-sm btn-edit" href="<?php echo '../level3/' . $accountCodeList[$i]->id . '/'.$accountCodeList[$i]->id_parent; ?>" title="Edit">Add/View</a>

                    <a class="btn btn-sm btn-edit" href="<?php echo '../level2Edit/' . $accountCodeList[$i]->id. '/' . $accountCodeList[$i]->id_parent; ?>" title="Edit">Edit</a>
                    </td>
                     </tr>
                  <?php 
              } 
              ?>
                    </tbody>
                </table>
            </div>

        </div>



        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>
    $(document).ready(function() {
        $("#form_grade").validate({
            rules: {
                name: {
                    required: true
                },
                 code: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name required</p>",
                },
                code: {
                    required: "<p class='error-text'>Code required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
