            <div class="sidebar">
                <div class="user-profile clearfix">
                    <a href="#" class="user-profile-link">
                        <span><img src="<?php echo BASE_PATH; ?>assets/img/user_profile.jpg"></span> <?php echo $name; ?>
                    </a>
                    <div class="dropdown">
                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                            <li><a href="/setup/user/profile">Edit Profile</a></li>
                            <li><a href="/setup/user/logout">Logout</a></li>
                        </ul>
                    </div>
                </div>
                <div class="sidebar-nav">
                    <h4>Setup</h4>
                    <ul>
                        <li><a href="/glsetup/financialYear/list">Financial Year</a></li>
                        <li><a href="/glsetup/fundCode/list">Fund Code</a></li>
                        <li><a href="/glsetup/departmentCode/list">Department Code</a></li>
                        <li><a href="/glsetup/activityCode/add">Activity Code</a></li>
                        <li><a href="/glsetup/accountCode/add">Account Code</a></li>
                        <li><a href="/glsetup/tax/list">Tax Code</a></li>
                        <li><a href="/glsetup/bankRegistration/list">Bank Registration</a></li>
                    </ul>
                    <h4>Journal</h4>
                    <ul>
                        <li><a href="/glsetup/journal/list">Journal Entry</a></li>
                        <li><a href="/glsetup/journal/ApprovalList">Journal Approval</a></li>
                        <li><a href="/glsetup/ledger/list">Ledger</a></li>
                    </ul>
                </div>
            </div>