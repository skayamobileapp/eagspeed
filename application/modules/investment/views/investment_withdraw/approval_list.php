<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>Approve Investment registration Withdraw</h3>
      <!-- <a href="add" class="btn btn-primary">+ Add Convocation</a> -->
    </div>
    <form action="" method="post" id="searchForm">
    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">
                
                <div class="row">

                  <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Withdraw Number</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="name" value="<?php echo $searchParam['name']; ?>">
                    </div>
                  </div>
                </div>

                 <!-- <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Programme </label>
                    <div class="col-sm-8">
                      <select name="id_programme" id="id_programme" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($programmeList)) {
                          foreach ($programmeList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['id_programme']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->code ."-".$record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div> -->

              </div>


              <div class="row">

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Registration Number</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="registration_number" value="<?php echo $searchParam['registration_number']; ?>">
                    </div>
                  </div>
                </div>

                <!-- <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Intake</label>
                    <div class="col-sm-8">
                      <select name="id_intake" id="id_intake" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($intakeList)) {
                          foreach ($intakeList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['id_intake']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div> -->

              </div>

        

              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary" name="button" value="search">Search</button>
                <button type="reset" class="btn btn-link" onclick="clearSearchForm()">Clear All Fields</button>
              </div>
            </div>
          </div>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
            <th class="text-center">Sl. No</th>
            <th class="text-center">Withdraw Reference Number</th>
            <th class="text-center">Registration Number</th>
            <th class="text-center">Amount</th>
            <th class="text-center">Contact Person</th>
            <th class="text-center">Contact Email</th>
            <th class="text-center">Effective Date</th>
            <th class="text-center">Maturity Date</th>
            <th class="text-center">Interest To Bank</th>
            <th class="text-center">Interest Day</th>
            <th class="text-center">Duration</th>
            <th class="text-center">Profit Amount</th>
            <th class="text-center">File Name</th>
            <th class="text-center">Status</th> 
            <th class="text-center">Action</th>
            <th style="text-align: center;"><input type="checkbox" id="checkAll" name="checkAll"> Check All</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($investmentWithdrawList)) {
              $i=1;
            foreach ($investmentWithdrawList as $record)
            {
              
          ?>
              <tr>
                <td><?php echo $i ?></td>
                  <td><?php echo $record->reference_number ?></td>
                  <td><?php echo $record->registration_number ?></td>
                  <td><?php echo $record->amount ?></td>
                  <td><?php echo $record->contact_person_one ?></td>
                  <td><?php echo $record->contact_email_one ?></td>
                  <td><?php echo date("d-m-Y", strtotime($record->effective_date)) ?></td>
                  <td><?php echo date("d-m-Y", strtotime($record->maturity_date)) ?></td>
                  <td><?php echo $record->interest_to_bank ?></td>
                  <td><?php echo $record->interest_day ?></td>
                  <td><?php echo $record->duration . " " . $record->duration_type ?></td>
                  <td><?php echo $record->profit_amount ?></td>
                  <td><?php echo $record->file_upload ?></td>

                <td><?php if( $record->status == '1')
                {
                  echo "Approved";
                }
                else if( $record->status == '0')
                {
                   echo "Pending";
                }
                else if( $record->status == '2')
                {
                  echo "Rejected";
                } 
                ?></td>
                <td class="text-center">
                  <a href="<?php echo 'view/' . $record->id; ?>" title="View">View</a>
                </td>

                <?php if($record->status=="1")
                {
                  echo "<td class='text-center'>Approved</td>";
                }
                else
                {
                ?>
                <td class="text-center">
                  <input type="checkbox" name="checkvalue[]" class="check" value="<?php echo $record->id ?>">
                </td>
                <?php
                }
                ?>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
      <div class="app-btn-group">
          <button type="submit" class="btn btn-primary" name="button" value="approve">Withdraw</button>
      </div>
    </div>
  </form>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script>
  $(function () {
      $("#checkAll").click(function () {
          if ($("#checkAll").is(':checked')) {
              $(".check").prop("checked", true);
          } else {
              $(".check").prop("checked", false);
          }
      });
  });
        
  function clearSearchForm()
  {
    window.location.reload();
  }
</script>