<?php $this->load->helper("form"); ?>

<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>Add Investment Application</h3>
            </div>
        <form id="form_pr_entry" action="" method="post">


        <div class="form-container">
            <h4 class="form-group-title">Investment Application</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Description <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Total Investing Amount <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="amount" name="amount" value="0" readonly="readonly">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Investment Application Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="date_time" name="date_time" value="<?php echo date('d-m-Y'); ?>" readonly="readonly">
                    </div>
                </div>

            </div>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Bank <span class='error-text'>*</span></label>
                        <select name="id_investment_bank" id="id_investment_bank" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($investmentBankList))
                            {
                                foreach ($investmentBankList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

            </div>

        </div>


        </form>

        <h3>Investment Application Details</h3>
        <button type="button" class="btn btn-info btn-lg" onclick="opendialog()">Add</button>
        

        <br>
        <br>

            
        <div class="form-container" style="display: none;" id="view_display">
            <h4 class="form-group-title">Investment Application Details</h4>

            <div  id="view">
            </div>

        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="button" class="btn btn-primary btn-lg" onclick="validateDetailsData()">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>
        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Investment Application Details</h4>
      </div>
      <div class="modal-body">
         <h4></h4>

    <form id="form_pr_entry_details" action="" method="post">


         <div class="row">


            <div class="col-sm-3">
                    <div class="form-group">
                        <label>Select Investment Type <span class='error-text'>*</span></label>
                        <select name="id_investment_type" id="id_investment_type" class="form-control" style="width: 196px;">
                            <option value="">Select</option>
                            <?php
                            if (!empty($investmentTypeList))
                            {
                                foreach ($investmentTypeList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->type;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>  

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Select Bank <span class='error-text'>*</span></label>
                        <select name="id_bank" id="id_bank" class="form-control" style="width: 196px;">
                            <option value="">Select</option>
                            <?php
                            if (!empty($bankList))
                            {
                                foreach ($bankList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>  

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Select Duration Type <span class='error-text'>*</span></label>
                        <select name="duration_type" id="duration_type" class="form-control" style="width: 196px;">
                            <option value="">Select</option>
                            <option value="Month">Months</option>
                            <option value="Year">Year</option>
                        </select>
                    </div>
                </div> 


                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Duration <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="duration" name="duration">
                    </div>
                </div>                
            </div>


            <div class="row">

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Investment Institution <span class='error-text'>*</span></label>
                        <select name="id_institution" id="id_institution" class="form-control" style="width: 196px;">
                            <option value="">Select</option>
                            <?php
                            if (!empty($investmentInstitutionList))
                            {
                                foreach ($investmentInstitutionList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>  

                 <div class="col-sm-3">
                    <div class="form-group">
                        <label>Investment Amount <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="total_amount" name="total_amount">
                    </div>
                </div>
                 <div class="col-sm-3">
                    <div class="form-group">
                        <label>Profit Rate <span class='error-text'>*</span></label>
                        <input type="number" maxlength="2" class="form-control" id="profit_rate" name="profit_rate">
                    </div>
               </div>                

                <!-- <div class="col-sm-3">
                    <div class="form-group">
                        <label>Bank Branch <span class='error-text'>*</span></label>
                        <span id='view_bank_branch'></span>
                    </div>
                </div>   -->

                 <div class="col-sm-3">
                    <div class="form-group">
                        <label>Selet Maturity Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="maturity_date" name="maturity_date" autocomplete="off">
                    </div>
               </div>

            </div>

        </form>


      </div>
      <div class="modal-footer">
                <button type="button" class="btn btn-default" onclick="saveData()">Add</button>

        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


</form>
<script>

    function opendialog()
    {
        if($('#form_pr_entry').valid())
        {
            $("#duration_type").val('');
            $("#duration").val('');
            $("#total_amount").val('');
            $("#profit_rate").val('');
            $("#id_institution").val('');
            $("#id_investment_type").val('');
            $("#id_bank").val('');
            $("#maturity_date").val('');
            $('#myModal').modal('show');
        }

    }

    function saveData()
    {
        if($('#form_pr_entry_details').valid())
        {
            $('#myModal').modal('hide');

            var tempPR = {};
            tempPR['duration_type'] = $("#duration_type").val();
            tempPR['duration'] = $("#duration").val();
            tempPR['total_amount'] = $("#total_amount").val();
            tempPR['profit_rate'] = $("#profit_rate").val();
            tempPR['id_institution'] = $("#id_institution").val();
            tempPR['id_investment_type'] = $("#id_investment_type").val();
            tempPR['id_bank'] = $("#id_bank").val();
            tempPR['maturity_date'] = $("#maturity_date").val();
                $.ajax(
                {
                   url: '/investment/investmentApplication/tempadd',
                   type: 'POST',
                   data:
                   {
                    tempData: tempPR
                   },
                   error: function()
                   {
                    alert('Something is wrong');
                   },
                   success: function(result)
                   {
                    $("#view_display").show();
                    $("#view").html(result);
                    var ta = $("#total_detail").val();
                    // alert(ta);
                    $("#amount").val(ta);
                   }
                });
        }
    }

    function deleteTempData(id) {
         $.ajax(
            {
               url: '/investment/investmentApplication/tempDelete/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    $("#view_display").show();
                    $("#view").html(result);
                    var ta = $("#total_detail").val();
                    // alert(ta);
                    $("#amount").val(ta);
                    if(result=='')
                    {
                        $("#view_display").hide();
                        $("#amount").val('0');
                    }
               }
            });
    }


    function getCategory(type)
     {
        // alert(type);
         $.get("/procurement/prEntry/getCategory/"+type, function(data, status)
         {
            // alert(data);
            $("#view_category").html(data);
            $("#view_sub_category").html('');
            $("#view_item").html('');
            // $("#view_student_details").show();
        });
     }

    

    


    function getTempData(id) {
        $.ajax(
            {
               url: '/procurement/prEntry/tempedit/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(resultfromjson)
               {
                    result = JSON.parse(resultfromjson);
                    $("#dt_fund").val(result['dt_fund']);
                    $("#dt_department").val(result['dt_department']);
                    $("#dt_activity").val(result['dt_activity']);
                    $("#dt_account").val(result['dt_account']);
                    $("#cr_fund").val(result['cr_fund']);
                    $("#cr_department").val(result['cr_department']);
                    $("#cr_activity").val(result['cr_activity']);
                    $("#cr_account").val(result['cr_account']);
                    $("#type").val(result['type']);
                    $("#id_category").val(result['id_category']);
                    $("#id_sub_category").val(result['id_sub_category']);
                    $("#id_item").val(result['id_item']);
                    $("#quantity").val(result['quantity']);
                    $("#price").val(result['price']);
                    $("#id_tax").val(result['id_tax']);
                    $("#tax_price").val(result['tax_price']);
                    $("#total_final").val(result['total_final']);
                    $("#id").val(id);
                    $('#myModal').modal('show');
               }
            });

    }


    function validateDetailsData()
    {
        if($('#form_pr_entry').valid())
        {
            console.log($("#view").html());
            var addedProgam = $("#view").html();
            if(addedProgam=='')
            {
                alert("Add Application Details");
            }
            else
            {
                $('#form_pr_entry').submit();
            }
        }    
    }

    $(document).ready(function() {
        $("#form_pr_entry").validate({
            rules: {
                description: {
                    required: true
                },
                 amount: {
                    required: true
                }
                ,
                 date_time: {
                    required: true
                },
                 id_investment_bank: {
                    required: true
                }
            },
            messages: {
                description: {
                    required: "<p class='error-text'>Enter Description",
                },
                amount: {
                    required: "<p class='error-text'>Add Details For Amount",
                },
                date_time: {
                    required: "<p class='error-text'>Select Date</p>",
                },
                id_investment_bank: {
                    required: "<p class='error-text'>Select Investment Bank</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


    $(document).ready(function() {
        $("#form_pr_entry_details").validate({
            rules: {
                duration_type: {
                    required: true
                },
                 duration: {
                    required: true
                }
                ,
                 total_amount: {
                    required: true
                },
                 profit_rate: {
                    required: true
                },
                 id_institution: {
                    required: true
                },
                 id_investment_type: {
                    required: true
                },
                 id_bank: {
                    required: true
                },
                 maturity_date: {
                    required: true
                }
            },
            messages: {
                duration_type: {
                    required: "<p class='error-text'>Select Duration Type",
                },
                duration: {
                    required: "<p class='error-text'>Enter Duration",
                },
                total_amount: {
                    required: "<p class='error-text'>Enter Total Amount</p>",
                },
                profit_rate: {
                    required: "<p class='error-text'>Enter Profit Rate</p>",
                },
                id_institution: {
                    required: "<p class='error-text'>Select Institution</p>",
                },
                id_investment_type: {
                    required: "<p class='error-text'>Select Investment Type</p>",
                },
                id_bank: {
                    required: "<p class='error-text'>Select Bank</p>",
                },
                maturity_date: {
                    required: "<p class='error-text'>Select Maturity Date</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script type="text/javascript">
    $('select').select2();
</script>
<script>
  $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
  } );
</script>