<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>Approve Investment Application</h3>
      <!-- <a href="add" class="btn btn-primary">+ Add Convocation</a> -->
    </div>
    <form action="" method="post" id="searchForm">
    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">
                

              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Application Description / Number</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="name" value="<?php echo $searchParam['name']; ?>">
                    </div>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Select Investment Bank</label>
                    <div class="col-sm-8">
                      <select name="id_investment_bank" id="id_investment_bank" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($investmentBankList)) {
                          foreach ($investmentBankList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['id_investment_bank']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->code ."-".$record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>

              </div>


              </div>
              <div class="app-btn-group">
                <button type="button" class="btn btn-primary">Search</button>
                <button type="reset" class="btn btn-link" onclick="clearSearchForm()">Clear All Fields</button>
              </div>
            </div>
          </div>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
           <th>Sl No.</th>
           <<th>Application Number</th>
            <th>Description</th>
            <th>Investment Bank</th>
            <th>Investment Amount</th>
            <th>Status</th>
            <th style="text-align: center;">Action</th>
            <!-- <th style="text-align: center;"><input type="checkbox" id="checkAll" name="checkAll"> Check All</th> -->
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($investmentApplicationList)) {
              $i=0;
            foreach ($investmentApplicationList as $record)
            {
          ?>
              <tr>
                 <td><?php echo $i+1;?></td>
                <td><?php echo $record->application_number ?></td>
                <td><?php echo $record->description ?></td>
                <td><?php echo $record->investment_bank_code . " - " . $record->investment_bank_name ?></td>
                <td><?php echo $record->amount ?></td>

                <td><?php if( $record->status == '1')
                {
                  echo "Approved";
                }
                else if( $record->status == '0')
                {
                   echo "Pending";
                }
                else if( $record->status == '2')
                {
                  echo "Rejected";
                } 
                ?></td>

                <?php if($record->status=="1")
                {
                  echo "<td class='text-center'>Approved</td>";
                }
                else
                {
                ?>
                <td style="text-align: center;">
                  <a href="<?php echo 'view/' . $record->id; ?>" title="View">Approve</a>
                </td>
                <!-- <td class="text-center">
                  <input type="checkbox" name="approval[]" class="check" value="<?php echo $record->id ?>">
                </td> -->
                <?php
                }
                ?>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
      <!-- <div class="app-btn-group">
          <button type="submit" class="btn btn-primary">Approve</button>
      </div> -->
    </div>
  </form>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script>
    $(function () {
        $("#checkAll").click(function () {
            if ($("#checkAll").is(':checked')) {
                $(".check").prop("checked", true);
            } else {
                $(".check").prop("checked", false);
            }
        });
    });

    $('select').select2();

</script>

<script type="text/javascript">
      function clearSearchForm()
      {
        window.location.reload();
      }
</script>