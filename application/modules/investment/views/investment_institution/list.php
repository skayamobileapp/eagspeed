<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List Investment Institution</h3>
      <a href="add" class="btn btn-primary">+ Add Investment Institution</a>
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">
                <div class="row">
                

              <div class="row">

                
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Name / Code / Branch</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="name" value="<?php echo $searchParam['name']; ?>">
                    </div>
                  </div>
                </div>

                

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Contact Name / Number</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="contact_number" value="<?php echo $searchParam['contact_number']; ?>">
                    </div>
                  </div>
                </div>

              </div>


              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <button type="reset" class="btn btn-link" onclick="clearSearchForm()">Clear All Fields</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Name</th>
            <th>Code</th>
            <th>Contact Person</th>
            <th>Contact Number</th>
            <th>Branch Name</th>
            <th>Bank Name</th>
            <th>Address</th>
            <th>City</th>
            <th>State</th>
            <th>Country</th>
            <th>Zip Code</th>
            <th>Status</th>
            <th style="text-align: center;">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($investmentInstitutionList)) {
            $i=1;
            foreach ($investmentInstitutionList as $record) {
          ?>
              <tr>
                  <td><?php echo $i ?></td>
                  <td><?php echo $record->name ?></td>
                  <td><?php echo $record->code ?></td>
                  <td><?php echo $record->contact_name ?></td>
                  <td><?php echo $record->contact_number ?></td>
                  <td><?php echo $record->branch ?></td>
                  <td><?php echo $record->bank_name ?></td>
                  <td><?php echo $record->address ?></td>
                  <td><?php echo $record->city ?></td>
                  <td><?php echo $record->state_name ?></td>
                  <td><?php echo $record->country_name ?></td>
                  <td><?php echo $record->zipcode ?></td>
                  <td><?php if( $record->status == '1')
                  {
                    echo "Active";
                  }
                  else
                  {
                    echo "In-Active";
                  } 
                  ?></td>
                  <td class="text-center">
                      <a href="<?php echo 'edit/'.$record->id; ?>" title="Edit">Edit</a>
                  </td>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>

<script type="text/javascript">
      function clearSearchForm()
      {
        window.location.reload();
      }
</script>