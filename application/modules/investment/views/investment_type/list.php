<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List Investment Type</h3>
      <a href="add" class="btn btn-primary">+ Add Investment Type</a>
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">
                <div class="row">
                

              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Name</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="name" value="<?php echo $searchParam['name']; ?>">
                    </div>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Account Code</label>
                    <div class="col-sm-8">
                      <select name="account_code" id="account_code" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($accountCodeList)) {
                          foreach ($accountCodeList as $record)
                          {
                            $selected = '';
                            if ($record->code == $searchParam['account_code']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->code;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->code ."-".$record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Contact Number</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="contact_number" value="<?php echo $searchParam['contact_number']; ?>">
                    </div>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Activity Code</label>
                    <div class="col-sm-8">
                      <select name="activity_code" id="activity_code" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($activityCodeList)) {
                          foreach ($activityCodeList as $record)
                          {
                            $selected = '';
                            if ($record->code == $searchParam['activity_code']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->code;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->code . " - " . $record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>
              </div>


              <div class="row">

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Department Code</label>
                    <div class="col-sm-8">
                      <select name="department_code" id="department_code" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($departmentCodeList)) {
                          foreach ($departmentCodeList as $record)
                          {
                            $selected = '';
                            if ($record->code == $searchParam['department_code']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->code;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->code ."-". $record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Fund Code</label>
                    <div class="col-sm-8">
                      <select name="fund_code" id="fund_code" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($fundCodeList)) {
                          foreach ($fundCodeList as $record)
                          {
                            $selected = '';
                            if ($record->code == $searchParam['fund_code']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->code;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->code . " - " . $record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>

              </div>



              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <button type="reset" class="btn btn-link" onclick="clearSearchForm()">Clear All Fields</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Type</th>
            <th>Contact Person</th>
            <th>Contact Number</th>
            <th>GL Code</th>
            <th>Status</th>
            <th style="text-align: center;">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($investmentTypeList)) {
            $i=1;
            foreach ($investmentTypeList as $record) {
          ?>
              <tr>
                  <td><?php echo $i ?></td>
                  <td><?php echo $record->type ?></td>
                  <td><?php echo $record->contact_name ?></td>
                  <td><?php echo $record->contact_number ?></td>
                  <td><?php echo $record->fund_code . " - " . $record->department_code . " - " . $record->activity_code . " - " . $record->account_code ?></td>
                  <td><?php if( $record->status == '1')
                  {
                    echo "Active";
                  }
                  else
                  {
                    echo "In-Active";
                  } 
                  ?></td>
                  <td class="text-center">
                      <a href="<?php echo 'edit/'.$record->id; ?>" title="Edit">Edit</a>
                  </td>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script type="text/javascript">
    
  $('select').select2

  function clearSearchForm()
  {
    window.location.reload();
  }
</script>