<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Book Room</h3>
        </div>
        <form id="form_bank" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Room Details</h4>



            <div class="row">

                

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" value="<?php echo $roomSetup->code;?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" value="<?php echo $roomSetup->name;?>"  readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Building<span class='error-text'>*</span></label>
                        <select name="id_facility_building_registration" id="id_facility_building_registration" class="form-control"   readonly="readonly">
                            <option value="">Select</option>
                            <?php
                            if (!empty($hostelList))
                            {
                                foreach ($hostelList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>" 
                                 <?php 
                                        if($record->id == $roomSetup->id_facility_building_registration)
                                        {
                                            echo "selected=selected";
                                        } ?>
                                >

                                >
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
            
          

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Room Type <span class='error-text'>*</span></label>
                        <select name="id_facility_room_type" id="id_facility_room_type" class="form-control"  readonly="readonly">
                            <option value="">Select</option>
                            <?php
                            if (!empty($roomTypeList))
                            {
                                foreach ($roomTypeList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                                 <?php 
                                        if($record->id == $roomSetup->id_facility_room_type)
                                        {
                                            echo "selected=selected";
                                        } ?>
                                >
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


                <div class="col-sm-4">
                        <div class="form-group">
                            <label>Cost per Day <span class='error-text'>*</span></label>
                            <input type="number" class="form-control"   value="<?php echo $roomSetup->cost; ?>"   readonly="readonly">
                        </div>
                    

                </div>

                 
               
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Max. Capacity <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="max_capacity" name="max_capacity"  readonly="readonly" value="<?php echo $roomSetup->max_capacity; ?>" min="1" >
                    </div>
                </div>


                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status *</p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" <?php if($roomSetup->status=='1') {
                                 echo "checked=checked";
                              };?>><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0" <?php if($roomSetup->status=='0') {
                                 echo "checked=checked";
                              };?>>
                              <span class="check-radio"></span> In-Active
                            </label>                              
                        </div>                         
                </div>

            </div>

        </div>

          <div class="form-container">
            <h4 class="form-group-title">Booking User Details</h4>



            <div class="row">

                

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="booking_name" name="booking_name" value="">
                    </div>
                </div>

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Email <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="booking_email" name="booking_email" value="">
                    </div>
                </div>

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Mobile <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="booking_mobile" name="booking_mobile" value="">
                    </div>
                </div>

                  <div class="col-sm-4">
                    <div class="form-group">
                        <label>Booking Type <span class='error-text'>*</span></label>
                        <select name='booking_type' class="form-control" id='booking_type'>
                            <option value='Hourly'>Hourly</option>
                            <option value='Day Wise'>Day Wise</option>
                        </select>
                    </div>
                </div>


                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Start Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="booking_start_date" name="booking_start_date" autocomplete="off" value="">
                    </div>
                </div>

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>End Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="booking_end_date" name="booking_end_date" autocomplete="off" value="">
                    </div>
                </div>

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Remarks <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="booking_remarks" name="booking_remarks" value="">
                    </div>
                </div>
            </div>
        </div>

        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="../list" class="btn btn-link">Cancel</a>
            </div>
        </div>
            
        </form>



      








    </div>

        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
</div>
<script type="text/javascript">
            $(document).ready(function () {
      
      //DatePicker Example
      $('#booking_start_date').datetimepicker({
           format:'d-m-yy h:i',
        step: 15
      });
       $('#booking_end_date').datetimepicker({
           format:'d-m-yy h:i',
        step: 15
      });




    });





    $('select').select2();

</script>
 <link rel="stylesheet" type="text/css" href="//www.thecodedeveloper.com/demo/add-datetimepicker-jquery-plugin/css/jquery.datetimepicker.min.css"/>
  <script src="//www.thecodedeveloper.com/demo/add-datetimepicker-jquery-plugin/js/jquery.datetimepicker.js"></script>
