<?php
$programme_approval_model = new Programme_Model();

$urlarray = explode ('/',$_SERVER['REQUEST_URI']);

$urlmodule = $urlarray['1'];
$urlcontroller = $urlarray['2'];
$urlaction = $urlarray['3'];

$id_product_type = $programmeDetails->id_programme_type;

$programme_tabs  = $programme_approval_model->getProductTabsByProductId($id_product_type);

// echo "<Pre>";print_r($programme_tabs);exit();
?>

<div class="container-fluid page-wrapper">
   <div class="main-container clearfix">

       <ul class="page-nav-links">
            <li><a href="/partner_university_prdtm/programme/edit/<?php echo $id_programme;?>">Product Details</a></li>

            <?php
            foreach ($programme_tabs as $individual_tab)
            {
              $tab = $individual_tab->tab;
              $title = $individual_tab->title;
            ?>
              <li 
              <?php
              if($tab == $urlaction)
              {
                echo 'class="active"';
              }
              ?>
              ><a href="/partner_university_prdtm/programme/<?php echo $tab;?>/<?php echo $id_programme;?>"><?php echo $title ?></a></li>
            <?php
            }
            ?>
            

            <li class="active"><a href="/partner_university_prdtm/programme/fee/<?php echo $id_programme;?>">Fee Structure</a></li>
            <li><a href="/partner_university_prdtm/programme/markDistribution/<?php echo $id_programme;?>">Mark Distribution</a></li>
            

            <?php
          if ($programmeDetails->send_for_approval == '0')
          {
          ?>

            <li><a href="/partner_university_prdtm/programme/sendForApproval/<?php echo $id_programme;?>">Send For Approval</a></li>
            
           <?php
          }
          else
          {
            ?>
        
            <li><a href="/partner_university_prdtm/programme/approval/<?php echo $id_programme;?>">Approval Details</a></li>

            <?php
          }
          ?>
        
       </ul>
      

      <div class="form-container">
              <h4 class="form-group-title">Fee Structure Main Details</h4> 

          <div class="row">
                

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Fee Structure Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $getProgrammeLandscapeLocal->code; ?>" readonly="readonly">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Fee Structure Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $getProgrammeLandscapeLocal->name; ?>" readonly="readonly">
                    </div>
                </div>    

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name Optional Language</label>
                        <input type="text" class="form-control" id="name_optional_language" name="name_optional_language" value="<?php echo $getProgrammeLandscapeLocal->name_optional_language; ?>" readonly="readonly">
                    </div>
                </div>     


          </div>

          

          <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Programme <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="year" name="year" value="<?php echo $getProgrammeLandscapeLocal->program_code . " - " . $getProgrammeLandscapeLocal->program; ?>" readonly="readonly">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Currency <span class='error-text'>*</span></label>
                        <select name="id_currency" id="id_currency" class="form-control" disabled="true">
                            <option value="">Select</option>
                            <?php
                            if (!empty($currencyList))
                            {
                                foreach ($currencyList as $record)
                                {?>
                                    <option value="<?php echo $record->id;?>"
                                      <?php
                                      if($record->id == $getProgrammeLandscapeLocal->id_currency)
                                      {
                                          echo 'selected';
                                      }
                                      ?>
                                    ><?php echo $record->code . " - " . $record->name;?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                
          </div>

      </div>



    <br>


      <form id="form_one" action="" method="post">

          <div class="form-container">
                  <h4 class="form-group-title">Fee Structure Details</h4> 

              <div class="row">

                    <div class="col-sm-3">
                          <div class="form-group">
                              <label>Fee Item <span class='error-text'>*</span></label>
                              <select name="one_id_fee_item" id="one_id_fee_item" class="form-control" required>
                                  <option value="">Select</option>
                                  <?php
                                  if (!empty($feeSetupList))
                                  {
                                      foreach ($feeSetupList as $record)
                                      {?>
                                          <option value="<?php echo $record->id;?>"
                                            <?php
                                            if($feeStructureDetail->id_fee_item == $record->id)
                                            {
                                              echo 'selected';
                                            }
                                            ?>
                                          ><?php echo $record->code . " - " . $record->name;?>
                                          </option>
                                  <?php
                                      }
                                  }
                                  ?>
                              </select>
                          </div>
                      </div>


                      <!-- <div class="col-sm-3">
                          <div class="form-group">
                              <label>Trigger On <span class='error-text'>*</span></label>
                              <select name="one_id_fee_structure_trigger" id="one_id_fee_structure_trigger" class="form-control">
                                  <option value="">Select</option>
                                  <?php
                                  if (!empty($getFeeStructureTriggerList))
                                  {
                                      foreach ($getFeeStructureTriggerList as $record)
                                      {?>
                                          <option value="<?php echo $record->id;?>"
                                          ><?php echo $record->name;?>
                                          </option>
                                  <?php
                                      }
                                  }
                                  ?>
                              </select>
                          </div>
                      </div> -->


                      <div class="col-sm-3">
                          <div class="form-group">
                              <label>Amount <span class='error-text'>*</span></label>
                              <input type="number" class="form-control" id="one_amount" name="one_amount"  value="<?php echo $feeStructureDetail->amount; ?>" required>
                              <input type="hidden" class="form-control" id="id_programme" name="id_programme"  value="<?php echo $getProgrammeLandscapeLocal->id_programme; ?>">
                          </div>
                      </div>


                      <div class="col-sm-3">
                          <div class="form-group">
                              <p>Registration Fee <span class='error-text'>*</span></p>
                              <label class="radio-inline">
                              <input type="radio" name="is_registration_fee" id="is_registration_fee" value="1" <?php if($feeStructureDetail->is_registration_fee=='1') {
                                  echo "checked=checked";
                              };?>><span class="check-radio"></span> Yes
                              </label>
                              <label class="radio-inline">
                              <input type="radio" name="is_registration_fee" id="is_registration_fee" value="0" <?php if($feeStructureDetail->is_registration_fee=='0') {
                                  echo "checked=checked";
                              };?>>
                              <span class="check-radio"></span> No
                              </label>
                          </div>
                      </div>



                      <div class="col-sm-3">
                          <div class="form-group">
                              <label>Currency <span class='error-text'>*</span></label>
                              <input type="text" class="form-control" id="one_currency" name="one_currency" value="<?php echo $getProgrammeLandscapeLocal->currency_name; ?>" readonly>
                          </div>
                      </div>

              </div>

          </div>

          <div class="button-block clearfix">
            <div class="bttn-group">
                  <button type="button" class="btn btn-primary btn-lg" onclick="getProgrammeFeeStructureDuplication()">Add</button>
                  <?php
                  if($id_fee_details != NULL)
                  {
                    ?>
                    <a href="<?php echo '../'. $id_programme ?>" class="btn btn-link">Cancel</a>
                    <?php
                  }
                  ?>

              </div>

          </div>


      </form>



        <div class="form-container">
            <h4 class="form-group-title">Fee Structure List</h4>        

            <div class="custom-table">
              <table class="table" id="list-table">
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Fee Item</th>
                    <th>Is Registration Fee</th>
                    <th>Tax Applicable</th>
                    <th>Total Amount ( <?php echo $feeStructureLocalList[0]->currency_code ?> )</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  if (!empty($feeStructureLocalList))
                  {
                    $i = 1;
                    $total_amount = 0;
                    foreach ($feeStructureLocalList as $record)
                    {
                      $tax_amount = ($record->amount * 0.01) * $tax_percentage;
                      $tax_amount = number_format($tax_amount, 2, '.', ',');
                  ?>
                      <tr>
                        <td><?php echo $i ?></td>
                        <td><?php echo $record->fee_structure_code . " - " . $record->fee_structure ?>                                
                        </td>
                        <td>
                            <?php
                            if($record->is_registration_fee == '1')
                            {
                              echo 'Yes';
                            }else
                            {
                              echo 'No';
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            if($record->gst_tax == '1')
                            {
                              echo 'Yes';
                            }else
                            {
                              echo 'No';
                            }
                            ?>
                        </td>
                        <td><?php echo $record->amount ?></td>
                        <td>
                          <a href='/partner_university_prdtm/programme/fee/<?php echo $id_programme;?>/<?php echo $record->id;?>'>Edit</a> | 
                          <a onclick="tempDelete(<?php echo $record->id; ?>)" title="Delete">Delete</a>
                        </td>
                      </tr>
                  <?php
                  $total_amount = $total_amount + $record->amount;
                  $i++;
                    }
                     $total_amount = number_format($total_amount, 2, '.', ',');
                    ?>

                    <tr >
                        <td bgcolor="" colspan="3"></td>
                        <td bgcolor="" style="text-align: center;"><b>Total Amount :</b></td>
                        <td bgcolor="">
              <input type="hidden" id="local_amount" name="local_amount" value="<?php echo $total_amount; ?>">

              <b><?php echo $total_amount . " ( " . $feeStructureLocalList[0]->currency_code . " ) ";  ?></b></td>
                        <!-- <td class="text-center"> -->
                        <td bgcolor=""></td>
                      </tr>
                    <?php
                  }
                  ?>
                </tbody>
              </table>
            </div>

        </div>






        

    <footer class="footer-wrapper">
       <p>&copy; 2019 All rights, reserved</p>
    </footer>

   </div>


</div>

<script src="<?php echo BASE_PATH; ?>assets/ckeditor/ckeditor.js"></script>


<script type="text/javascript">


    $('select').select2();

    updateMasterAmount();

    function updateMasterAmount()
    {
        var tempPR = {};
        var id_program_landscape = "<?php echo $id_program_landscape; ?>";

        tempPR['id'] = id_program_landscape;
        tempPR['amount'] = $("#local_amount").val();
        tempPR['international_amount'] = $("#international_amount").val();
        
        // alert(tempPR['international_amount']);

            $.ajax(
            {
               url: '/partner_university_prdtm/programme/updateFeeStructureMasterAmount',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert(result);
                // window.location.reload();
               }
            });
      }

    function getProgrammeFeeStructureDuplication()
    {
      if($('#form_one').valid())
      {
        var one_id_fee_item = $("#one_id_fee_item").val();

        if(one_id_fee_item != '')
        {
          var tempPR = {};
          tempPR['id_fee_item'] = one_id_fee_item;
          tempPR['id_program_landscape'] = "<?php echo $id_program_landscape; ?>";
          tempPR['id'] = "<?php echo $id_fee_details; ?>";

          $.ajax(
          {
             url: '/partner_university_prdtm/programme/getProgrammeFeeStructureDuplication',
              type: 'POST',
             data:
             {
              tempData: tempPR
             },
             error: function()
             {
              alert('Something is wrong');
             },
             success: function(result)
             {
                // alert(result);
                if(result == '0')
                {
                    alert('Duplicate Programme Fee Item Not Allowed');
                    // $("#one_id_fee_item").val('');
                }
                else
                if(result == '1')
                {
                  $("#form_one").submit();
                }
             }
          });
        }
      }
    }

  

    function opendialog()
    {
        $("#id_fee_item").val('');
        $("#amount").val('');
        $("#id_frequency_mode").val('');
        $("#id").val('0');                    
        $('#myModal').modal('show');

    }



    function showInstallments(is_installment)
    {
      // alert(is_installment);
      if(is_installment == 0)
      {
        $('#view_amount').show();
        $('#view_fee_item').show();
      }else
      {

        $('#view_amount').hide();
        $('#view_fee_item').hide();
      }
    }



    function saveData()
    {
      if($('#form_one').valid())
      {
        $('#form_one').submit();
      } 
    }


    function tempDelete(id)
    {
      var cnf= confirm('Do you really want to delete?');
      if(cnf==true)
      {
         $.ajax(
            {
               url: '/partner_university_prdtm/programme/deleteFeeStructure/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                window.location.reload();
               }
            });
       }
    }


    $(document).ready(function()
    {
        $("#form_one").validate({
            rules: {
                one_id_fee_item: {
                    required: true
                },
                one_amount: {
                    required: true
                },
                one_id_frequency_mode: {
                    required: true
                },
                one_id_fee_structure_trigger: {
                  required: true
                }
            },
            messages: {
                one_id_fee_item: {
                    required: "<p class='error-text'>Select Fee Item</p>",
                },
                one_amount: {
                    required: "<p class='error-text'>Enter Amount</p>",
                },
                one_id_frequency_mode: {
                    required: "<p class='error-text'>Select Frequency Mode</p>",
                },
                one_id_fee_structure_trigger: {
                    required: "<p class='error-text'>Select Trigger</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

</script>