<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Programme extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('programme_model');
        $this->isPartnerUniversityLoggedIn();
        error_reporting(0);
    }


   function list()
    {
        $id_partner_university = $this->session->id_partner_university;

        $formData['name'] = $this->security->xss_clean($this->input->post('name'));
        $formData['id_category'] = $this->security->xss_clean($this->input->post('id_category'));
        $formData['id_category_setup'] = $this->security->xss_clean($this->input->post('id_category_setup'));
        $formData['id_programme_type'] = $this->security->xss_clean($this->input->post('id_programme_type'));
        $formData['id_partner_university'] = $id_partner_university;


        $data['searchParam'] = $formData;

        // $data['programmeList'] = $this->programme_model->programmeList();
        $data['categoryList'] = $this->programme_model->categoryListByStatus('1');
        $data['categoryTypeList'] = $this->programme_model->categoryTypeListByStatus('1');
        $data['productTypeSetupList'] = $this->programme_model->productTypeSetupListByStatus('1');

        $data['programmeList'] = $this->programme_model->programmeListSearch($formData,$id_partner_university);

        // echo "<Pre>"; print_r($id_partner_university);exit;

        $this->global['pageTitle'] = 'Campus Management System : Program List';
        $this->loadViews("programme/list", $this->global, $data, NULL);
    }
    
    function add()
    {
        $id_session = $this->session->my_session_id;
        $id_partner_university = $this->session->id_partner_university;

       if($this->input->post())
        {                

            // For file validation from 36 to 44 , file size validationss
            // echo "<Pre>";print_r($this->input->post());exit;

            // echo "<Pre>"; print_r($_FILES['image']);exit;
            if($_FILES['image'])
            {

                $certificate_name = $_FILES['image']['name'];
                $certificate_size = $_FILES['image']['size'];
                $certificate_tmp =$_FILES['image']['tmp_name'];
                
                // echo "<Pre>"; print_r($certificate_tmp);exit();

                $certificate_ext=explode('.',$certificate_name);
                $certificate_ext=end($certificate_ext);
                $certificate_ext=strtolower($certificate_ext);


                $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'Image File');

                $image_file = $this->uploadFile($certificate_name,$certificate_tmp,'Image File');
            }


            $id_session = $this->session->my_session_id;

             $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $credit = $this->security->xss_clean($this->input->post('credit'));
                $max_duration = $this->security->xss_clean($this->input->post('max_duration'));
                $duration_type = $this->security->xss_clean($this->input->post('duration_type'));
                $id_study_level = $this->security->xss_clean($this->input->post('id_study_level'));
                $id_delivery_mode = $this->security->xss_clean($this->input->post('id_delivery_mode'));
                $id_learning_mode = $this->security->xss_clean($this->input->post('id_learning_mode'));
                $entry_qualification = $this->security->xss_clean($this->input->post('entry_qualification'));
                $short_description = $this->security->xss_clean($this->input->post('short_description'));
                $introduction = $this->security->xss_clean($this->input->post('introduction'));
                $synopsys    = $this->security->xss_clean($this->input->post('synopsys'));
                $works = $this->security->xss_clean($this->input->post('works'));
                $pass = $this->security->xss_clean($this->input->post('pass'));
                $assessment_type = $this->security->xss_clean($this->input->post('assessment_type'));
                $assessment_details = $this->security->xss_clean($this->input->post('assessment_details'));
$id_staff = $this->security->xss_clean($this->input->post('id_staff'));               
$id_partner_university = $this->security->xss_clean($this->input->post('id_partner_university'));               
$id_programme_type = $this->security->xss_clean($this->input->post('id_programme_type'));               
$id_category = $this->security->xss_clean($this->input->post('id_category'));               
$id_education_level = $this->security->xss_clean($this->input->post('id_education_level'));      
$course_fee = $this->security->xss_clean($this->input->post('course_fee'));      
$pre_requisite = $this->security->xss_clean($this->input->post('pre_requisite'));      





                $status = 0;
    $is_free_course = 1;
   if($course_fee>0){
    $is_free_course = 0;
   }


                // $id_staff = $this->security->xss_clean($this->input->post('id_staff'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'credit' => $credit,
                    'max_duration' => $max_duration,
                    'duration_type' => $duration_type,
                    'id_study_level' => $id_study_level,
                    'id_delivery_mode' => $id_delivery_mode,
                    'id_study_level' => $id_study_level,
                    'entry_qualification' => $entry_qualification,
                    'short_description' => $short_description,
                    'introduction' => $introduction,
                    'synopsys' => $synopsys,
                    'works' => $works,
                    'pass' => $pass,
                    'assessment_type'=>$assessment_type,
                    'assessment_details' => $assessment_details,
                    'created_by' => $id_user,
                    'id_staff'=>$id_staff,
                    'id_partner_university'=>$id_partner_university,
                    'id_programme_type'=>$id_programme_type,
                    'id_category'=>$id_category,
                    'id_education_level'=>$id_education_level,
                    'pre_requisite'=>$pre_requisite,
                    'id_learning_mode'=>$id_learning_mode,
                    'is_free_course'=>$is_free_course,
                    'status'=>'Pending Approval'
                );

               
                 


                // echo "<Pre>"; print_r($image_file);exit;

                if($image_file)
                {
                    $data['course_image'] = $image_file;
                }

                $inserted_id = $this->programme_model->addNewProgrammeDetails($data);



               $cloResultFromTemp = $this->programme_model->getCloTemp($id_session);
               for($i=0;$i<count($cloResultFromTemp);$i++) {
                    $newclo = array();

                    $id = $cloResultFromTemp[$i]->id;
                    $cloname = $cloResultFromTemp[$i]->clo_name;

                    $dataclo['clo_name'] = $cloname;
                    $dataclo['id_programme'] = $inserted_id;
                    $this->programme_model->insertintoMainCLO($dataclo);
               }


                $topicResultFromTemp = $this->programme_model->getTopicTemp($id_session);
               for($i=0;$i<count($topicResultFromTemp);$i++) {
                    $newclo = array();

                    $id = $topicResultFromTemp[$i]->id;
                    $topic_name = $topicResultFromTemp[$i]->topic_name;
                    $topic_overview = $topicResultFromTemp[$i]->topic_overview;
                    $topic_outcome = $topicResultFromTemp[$i]->topic_outcome;

                    $datatopic['topic_name'] = $topic_name;
                    $datatopic['topic_overview'] = $topic_overview;
                    $datatopic['topic_outcome'] = $topic_outcome;
                    $datatopic['id_programme'] = $inserted_id;
                    $this->programme_model->insertintoMainTopic($datatopic);
               }

                $staffResultFromTemp = $this->programme_model->getStaffTemp($id_session);
               for($i=0;$i<count($staffResultFromTemp);$i++) {
                    $datatopicstaff = array();
                    $id = $staffResultFromTemp[$i]->id;
                    $id_staff = $staffResultFromTemp[$i]->id_staff;

                    $datatopicstaff['id_staff'] = $id_staff;
                    $datatopicstaff['id_programme'] = $inserted_id;
                    $this->programme_model->insertintoMainStaff($datatopicstaff);
               }


               for($i=0;$i<count($_POST['overallassissmentids']);$i++) {

                 $idassignment = $_POST['overallassissmentids'][$i];
                    $datatopicstaff = array();

                    $datatopicstaff['id_assignment'] = $idassignment;
                    $datatopicstaff['marks'] = $_POST['overallassissment'][$idassignment];
                    $datatopicstaff['id_programme'] = $inserted_id;
                    $this->programme_model->insertintoMainAssignment($datatopicstaff);
               }

                for($i=0;$i<count($_POST['overallcertificateids']);$i++) {

                 $idassignment = $_POST['overallcertificateids'][$i];
                    $datatopicstaff = array();

                    $datatopicstaff['id_certificate'] = $_POST['overallcertificate'][$idassignment];
                    $datatopicstaff['id_programme'] = $inserted_id;
                    $this->programme_model->insertintoMainCertificate($datatopicstaff);
               }

                    
                

                if($inserted_id)
                {
                    $fee_structure_data = array(
                        'name' => "Fee Structure for - ".$name,
                        'name_optional_language' => $name,
                        'code' => $code,
                        'amount'=>$course_fee,
                        'id_programme' => $inserted_id,
                        'id_currency' => 1,
                        'status' => 1,
                        'created_by' => $id_user
                    );
                    
                    $id_fee_structure_master = $this->programme_model->addNewFeeStructureMaster($fee_structure_data);

                    $fee_structure_details = array(
                        'id_fee_structure' => $id_fee_structure_master,
                        'id_fee_item' => 2,
                        'amount' => $course_fee,
                        'status' => 1,
                        'created_by' => $id_user
                    );
                    $this->programme_model->addNewFeeStructureMasterDetails($fee_structure_details);

                }

                //delete all temp table
                $this->programme_model->deleteTempCloData($id_session);
                $this->programme_model->deleteTempStaffData($id_session);
                $this->programme_model->deleteTempTopicData($id_session);
                redirect('/partner_university_prdtm/programme/list');
        }

        $data['staffList'] = $this->programme_model->staffListByPu($id_partner_university);
        $data['awardList'] = $this->programme_model->awardLevelListByStatus('1');
        $data['schemeList'] = $this->programme_model->schemeListByStatus('1');

        $data['categoryList'] = $this->programme_model->categoryListByStatus('1');
        $data['categoryTypeList'] = $this->programme_model->categoryTypeListByStatus('1');
        $data['productTypeSetupList'] = $this->programme_model->productTypeSetupListByStatus('1');
        $data['partnerList'] = $this->programme_model->partnerUniversityList();

        $data['deliveryModeList'] = $this->programme_model->getDeliveryMode();

        $data['studyLevelList'] = $this->programme_model->getStudyLevel();

            $data['learningModeList'] = $this->programme_model->getLearningMode();


            $data['assessmentList'] = $this->programme_model->getAssessmentList();
            $data['certificateList'] = $this->programme_model->getCertificateList();

        $data['educationLevelList'] = $this->programme_model->educationLevelListByStatus('1');

        $data['id_partner_university'] = $id_partner_university;
        
        // echo "<Pre>";print_r($data['partnerList']);exit();

        $this->global['pageTitle'] = 'Campus Management System : Add Programme';
        $this->loadViews("programme/add", $this->global, $data, NULL);
    }

    function edit($id = NULL)
    {
                $id_partner_university = $this->session->id_partner_university;

            if ($id == null)
            {
                redirect('/partner_university_prdtm/programme/list');
            }
            if($this->input->post())
            {

                // echo "<Pre>"; print_r($this->input->post());exit();

                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                 // For file validation from 36 to 44 , file size validationss


                if($_FILES['image'])
                {

                    $certificate_name = $_FILES['image']['name'];
                    $certificate_size = $_FILES['image']['size'];
                    $certificate_tmp =$_FILES['image']['tmp_name'];
                    
                    // echo "<Pre>"; print_r($certificate_tmp);exit();

                    $certificate_ext=explode('.',$certificate_name);
                    $certificate_ext=end($certificate_ext);
                    $certificate_ext=strtolower($certificate_ext);

                    $image_file = $this->uploadFile($certificate_name,$certificate_tmp,'Image File');
                }


                $id_session = $this->session->my_session_id;
                $id_user = $this->session->userId;

                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $credit = $this->security->xss_clean($this->input->post('credit'));
                $max_duration = $this->security->xss_clean($this->input->post('max_duration'));
                $duration_type = $this->security->xss_clean($this->input->post('duration_type'));
                $id_study_level = $this->security->xss_clean($this->input->post('id_study_level'));
                $id_delivery_mode = $this->security->xss_clean($this->input->post('id_delivery_mode'));
                $id_learning_mode = $this->security->xss_clean($this->input->post('id_learning_mode'));
                $entry_qualification = $this->security->xss_clean($this->input->post('entry_qualification'));
                $short_description = $this->security->xss_clean($this->input->post('short_description'));
                $introduction = $this->security->xss_clean($this->input->post('introduction'));
                $synopsys    = $this->security->xss_clean($this->input->post('synopsys'));
                $works = $this->security->xss_clean($this->input->post('works'));
                $pass = $this->security->xss_clean($this->input->post('pass'));
                $assessment_type = $this->security->xss_clean($this->input->post('assessment_type'));
                $assessment_details = $this->security->xss_clean($this->input->post('assessment_details'));
$id_staff = $this->security->xss_clean($this->input->post('id_staff'));               
$id_partner_university = $this->security->xss_clean($this->input->post('id_partner_university'));               
$id_programme_type = $this->security->xss_clean($this->input->post('id_programme_type'));               
$id_category = $this->security->xss_clean($this->input->post('id_category'));               
$id_education_level = $this->security->xss_clean($this->input->post('id_education_level'));      
$course_fee = $this->security->xss_clean($this->input->post('course_fee'));      
$pre_requisite = $this->security->xss_clean($this->input->post('pre_requisite'));      

$status = $this->security->xss_clean($this->input->post('status'));      
$course_sub_type = $this->security->xss_clean($this->input->post('course_sub_type'));      
$language_of_delivery = $this->security->xss_clean($this->input->post('language_of_delivery'));      




    $is_free_course = 1;
   if($course_fee>0){
    $is_free_course = 0;
   }


                // $id_staff = $this->security->xss_clean($this->input->post('id_staff'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'credit' => $credit,
                    'max_duration' => $max_duration,
                    'duration_type' => $duration_type,
                    'id_study_level' => $id_study_level,
                    'id_delivery_mode' => $id_delivery_mode,
                    'id_study_level' => $id_study_level,
                    'entry_qualification' => $entry_qualification,
                    'short_description' => $short_description,
                    'introduction' => $introduction,
                    'synopsys' => $synopsys,
                    'works' => $works,
                    'pass' => $pass,
                    'assessment_type'=>$assessment_type,
                    'assessment_details' => $assessment_details,
                    'created_by' => $id_user,
                    'id_staff'=>$id_staff,
                    'id_partner_university'=>$id_partner_university,
                    'id_programme_type'=>$id_programme_type,
                    'id_category'=>$id_category,
                    'id_education_level'=>$id_education_level,
                    'pre_requisite'=>$pre_requisite,
                    'id_learning_mode'=>$id_learning_mode,
                    'is_free_course'=>$is_free_course,
                    'course_sub_type'=>$course_sub_type,
                    'language_of_delivery'=>$language_of_delivery
                );

                $this->programme_model->deleteprogrammebyCert($id);
                $this->programme_model->deleteprogrammeassissment($id);


                $dataupdate['amount']=$course_fee;
                $updateFeeAmount = $this->programme_model->updateFee($id,$dataupdate);




                  for($i=0;$i<count($_POST['overallcertificateids']);$i++) {

                 $idassignment = $_POST['overallcertificateids'][$i];
                    $datatopicstaff = array();

                    $datatopicstaff['id_certificate'] = $_POST['overallcertificate'][$idassignment];
                    $datatopicstaff['id_programme'] = $id;
                    $this->programme_model->insertintoMainCertificate($datatopicstaff);
               }


                 for($i=0;$i<count($_POST['overallassissmentids']);$i++) {

                 $idassignment = $_POST['overallassissmentids'][$i];
                    $datatopicstaffs = array();

                    $datatopicstaffs['id_assignment'] = $idassignment;
                    $datatopicstaffs['marks'] = $_POST['overallassissment'][$idassignment];
                    $datatopicstaffs['id_programme'] = $id;

                    $this->programme_model->insertintoMainAssignment($datatopicstaffs);
               }


                    

               
                 


                // echo "<Pre>"; print_r($image_file);exit;

                if($image_file)
                {
                    $data['course_image'] = $image_file;
                }
                $result = $this->programme_model->editProgrammeDetails($data,$id);

               

                redirect('/partner_university_prdtm/programme/list');
            }
            
            $data['id_programme'] = $id;
            $data['staffList'] = $this->programme_model->staffListByPu($id_partner_university);


            $data['programmeHasDeanList'] = $this->programme_model->getProgrammeHasDean($id);
            $data['programmeHasCourseList'] = $this->programme_model->getProgrammeHasCourse($id);
            $data['certificateProgrammeList'] = $this->programme_model->getProgrammeCertificate($id);
            $data['awardList'] = $this->programme_model->awardLevelListByStatus('1');
            $data['schemeList'] = $this->programme_model->schemeListByStatus('1');
            $data['programTypeList'] = $this->programme_model->programTypeListByStatus('1');
            $data['educationLevelList'] = $this->programme_model->educationLevelListByStatus('1');
            $data['categoryList'] = $this->programme_model->categoryListByStatus('1');
            $data['categoryTypeList'] = $this->programme_model->categoryTypeListByStatus('1');
            $data['productTypeSetupList'] = $this->programme_model->productTypeSetupListByStatus('1');

            $data['deliveryModeList'] = $this->programme_model->getDeliveryMode();

            $data['studyLevelList'] = $this->programme_model->getStudyLevel();

            $data['partnerList'] = $this->programme_model->partnerUniversityList();
            $data['statusList'] = $this->programme_model->statusListByType('Programme');

                        $data['learningModeList'] = $this->programme_model->getLearningMode();

         $data['getprogrammeAssignment'] = $this->programme_model->getprogrammeAssignment($id);
         $data['programmeFees'] = $this->programme_model->getProgrammeFees($id);

            $data['programmeDetails'] = $this->programme_model->getProgrammeDetails($id);


            $data['partnerList'] = $this->programme_model->partnerUniversityList();

            $data['deliveryModeList'] = $this->programme_model->getDeliveryMode();

            $data['learningModeList'] = $this->programme_model->getLearningMode();


            $data['assessmentList'] = $this->programme_model->getAssessmentList();
            $data['certificateList'] = $this->programme_model->getCertificateList();



            $tab_fields = $this->programme_model->getProductFieldsByProductType($data['programmeDetails']->id_programme_type);

            $id_fields = array();
            foreach ($tab_fields as $value)
            {
                $id_field = $value->if_field;
                array_push($id_fields, $id_field);
            }


            $this->global['pageTitle'] = 'Campus Management System : Edit Programme';

        $this->loadViews("programme/edit", $this->global, $data, NULL);
    }


  
     function addstaff(){
        $id_user = $this->session->userId;
        $clodata['id_staff'] = $_POST['tempData']['id_staff'];
        $clodata['session_id'] = $this->session->my_session_id;         
        $result = $this->programme_model->addStaffTemp($clodata);
    }

    function addstaffmain(){
         $id_user = $this->session->userId;
        $clodata['id_staff'] = $_POST['tempData']['id_staff'];
        $clodata['id_programme'] = $_POST['tempData']['id_programme'];
        $result = $this->programme_model->addStaffMain($clodata);
    }


    function editclodata($id){
                 $result = $this->programme_model->getClobyProgrammeId($id);
            if(!empty($result))
        {

        $table.="<div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>CLO NAME</th>
                    <th class='text-center'>Action</th>
                </tr>
                </thead>";
                    for($i=0;$i<count($result);$i++)
                    {
                    $id = $result[$i]->id;
                    $staff = $result[$i]->clo_name;
                    $j = $i+1;
                        $table.= "
                         <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$staff</td>                         
                            <td class='text-center'>
                                <a class='btn btn-sm btn-edit' onclick='deleteclomaindata($id)'>Delete</a>
                            <td>
                        </tr>";
                    }
        $table.= "</tbody>
         </table></div>";
        }
        
        echo  $table;
        exit;

    }


     function editstaffdata($id){
                 $result = $this->programme_model->getStaffbyProgrammeId($id);
            if(!empty($result))
        {

        $table.="<div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>CLO NAME</th>
                    <th class='text-center'>Action</th>
                </tr>
                </thead>";
                    for($i=0;$i<count($result);$i++)
                    {
                    $id = $result[$i]->id;
                    $staff = $result[$i]->name;
                    $j = $i+1;
                        $table.= "
                         <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$staff</td>                         
                            <td class='text-center'>
                                <a class='btn btn-sm btn-edit' onclick='deletestaffmaindata($id)'>Delete</a>
                            <td>
                        </tr>";
                    }
        $table.= "</tbody>
         </table></div>";
        }
        
        echo  $table;
        exit;

    }

    function edittopicdata($id){
                 $result = $this->programme_model->getTopicbyProgrammeId($id);
            if(!empty($result))
        {

        $table.="<div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Topic NAME</th>
                    <th class='text-center'>Action</th>
                </tr>
                </thead>";
                    for($i=0;$i<count($result);$i++)
                    {
                    $id = $result[$i]->id;
                    $staff = $result[$i]->topic_name;
                    $j = $i+1;
                        $table.= "
                         <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$staff</td>                         
                            <td class='text-center'>
                                <a class='btn btn-sm btn-edit' onclick='deletetopicmaindata($id)'>Delete</a>
                            <td>
                        </tr>";
                    }
        $table.= "</tbody>
         </table></div>";
        }
        
        echo  $table;
        exit;

    }


    function stafftext(){
        $result = $this->programme_model->getStaffTemp($this->session->my_session_id);

        if(!empty($result))
        {

        $table.="<div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Staff NAME</th>
                    <th class='text-center'>Action</th>
                </tr>
                </thead>";
                    for($i=0;$i<count($result);$i++)
                    {
                    $id = $result[$i]->id;
                    $staff = $result[$i]->first_name;
                    $j = $i+1;
                        $table.= "
                         <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$staff</td>                         
                            <td class='text-center'>
                                <a class='btn btn-sm btn-edit' onclick='deletestaffData($id)'>Delete</a>
                            <td>
                        </tr>";
                    }
        $table.= "</tbody>
         </table></div>";
        }
        
        echo  $table;
        exit;

    }


    function addclo(){
        $id_user = $this->session->userId;
        $clodata['clo_name'] = $_POST['tempData']['clo'];
        $clodata['session_id'] = $this->session->my_session_id;         
        $result = $this->programme_model->addclo($clodata);
    }

    function addclomain(){
                $id_user = $this->session->userId;
        $clodata['clo_name'] = $_POST['tempData']['clo'];
        $clodata['id_programme'] = $_POST['tempData']['id_programme'];
        $result = $this->programme_model->addMainclo($clodata);

    }

     function addtopicmain(){
                $id_user = $this->session->userId;
        $clodata['topic_name'] = $_POST['tempData']['topic'];
        $clodata['id_programme'] = $_POST['tempData']['id_programme'];
        $result = $this->programme_model->addMainTopic($clodata);

    }

    function clotext(){
        $result = $this->programme_model->getCloTemp($this->session->my_session_id);

        if(!empty($result))
        {

        $table.="<div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>CLO NAME</th>
                    <th class='text-center'>Action</th>
                </tr>
                </thead>";
                    for($i=0;$i<count($result);$i++)
                    {
                    $id = $result[$i]->id;
                    $staff = $result[$i]->clo_name;
                    $j = $i+1;
                        $table.= "
                         <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$staff</td>                         
                            <td class='text-center'>
                                <a class='btn btn-sm btn-edit' onclick='deletecloData($id)'>Delete</a>
                            <td>
                        </tr>";
                    }
        $table.= "</tbody>
         </table></div>";
        }
        
        echo  $table;
        exit;

    }


     function addtopic(){
        $id_user = $this->session->userId;
                $clodata['topic_name'] = $_POST['tempData']['topic'];
        $clodata['topic_outcome'] = $_POST['tempData']['topic_outcome'];
        $clodata['topic_overview'] = $_POST['tempData']['topic_overview'];
        $clodata['session_id'] = $this->session->my_session_id;                 
        $result = $this->programme_model->addtemptopic($clodata);
    }

     function deletetopic(){
        if($_POST){

            $id = $_POST['tempData']['topic_id'];
            $this->programme_model->deletetemptopic($id);
        }
     }

       function deleteclo(){
        if($_POST){

            $id = $_POST['tempData']['clo_id'];
            $this->programme_model->deletetempclo($id);
        }
     }


     function deleteclomain(){
        if($_POST){

            $id = $_POST['tempData']['clo_id'];
            $this->programme_model->deletemainclo($id);
        }
     }

     function deletestaffmain(){
        if($_POST){

            $id = $_POST['tempData']['staff_id'];
            $this->programme_model->deletemainstaff($id);
        }
     }

    function  deletetopicmain(){
        if($_POST){

            $id = $_POST['tempData']['topic_id'];
            $this->programme_model->deletemaintopic($id);
        }
     }



     function deletestaff(){
        if($_POST){

            $id = $_POST['tempData']['staff_id'];
            $this->programme_model->deletetempstaff($id);
        }
     }

    function topictext(){
        $result = $this->programme_model->getTopicTemp($this->session->my_session_id);

        if(!empty($result))
        {

         $table.="<div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Topic </th>
                    <th>Topic Overview</th>
                    <th>Topic Outcome</th>
                    <th class='text-center'>Action</th>
                </tr>
                </thead>";
                    for($i=0;$i<count($result);$i++)
                    {
                    $id = $result[$i]->id;
                    $staff = $result[$i]->topic_name;
                    $topic_overview = $result[$i]->topic_overview;
                    $topic_outcome = $result[$i]->topic_outcome;
                    $j = $i+1;
                        $table.= "
                         <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$staff</td>                         
                            <td>$topic_overview</td>                         
                            <td>$topic_outcome</td>                         
                            <td class='text-center'>
                                <a class='btn btn-sm btn-edit' onclick='deletetopicData($id)'>Delete</a>
                            <td>
                        </tr>";
                    }
        $table.= "</tbody>
         </table></div>";
        }
        
        echo  $table;
        exit;

    }


}