<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class StatementOfAccount extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('statement_of_account_model');
        $this->load->model('admission/student_model');
        $this->load->model('setup/programme_model');
        $this->load->model('setup/intake_model');

        $this->load->model('main_invoice_model');
        $this->load->model('fee_setup_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('statement_of_account.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            // $formData['invoice_number'] = $this->security->xss_clean($this->input->post('invoice_number'));
            $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));
            $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
            $formData['status'] = '';

            $data['searchParam'] = $formData;

            $data['mainInvoiceList'] = $this->statement_of_account_model->getMainInvoiceListByStatus($formData);
            // print_r($data);exit();
            $data['programmeList']= $this->programme_model->programmeList();
            $data['intakeList']= $this->intake_model->intakeList();
            $this->global['pageTitle'] = 'Campus Management System : List Students';
            $this->loadViews("statement_of_account/list", $this->global, $data, NULL);
        }
    }
    
    
    function view($id)
    {
        if ($this->checkAccess('statement_of_account.view') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            
            // $data['studentList'] = $this->main_invoice_model->studentList();
            $data['studentDetails'] = $this->statement_of_account_model->getStudentByStudentId($id);

            // $data['getInvoiceByStudentId'] = $this->statement_of_account_model->getInvoiceByStudentId($id);
            // $data['getReceiptByStudentId'] = $this->statement_of_account_model->getReceiptByStudentId($id);


            $data['getInvoiceByStudentId'] = $this->statement_of_account_model->getInvoiceByStudentId($id,$data['studentDetails']->id_applicant);
            $data['getReceiptByStudentId'] = $this->statement_of_account_model->getReceiptByStudentId($id,$data['studentDetails']->id_applicant);
            
            $data['getStatementOfAccountByStudentId'] = $this->statement_of_account_model->getStatementOfAccountByStudentId($id,$data['studentDetails']->id_applicant);

            $data['creditNoteByStudentId'] = $this->statement_of_account_model->getCreditNoteByStudentId($id);
            $data['debitNoteByStudentId'] = $this->statement_of_account_model->getDebitNoteByStudentId($id);

            // echo "<Pre>";print_r($data['getStatementOfAccountByStudentId']);exit();

            // $data['studentList'] = $this->main_invoice_model->programmeListByStatus('1');

            $this->global['pageTitle'] = 'Campus Management System : View Student Account Statements';
            $this->loadViews("statement_of_account/view", $this->global, $data, NULL);
        }
        
    }

     function tempadd()
    {
        $id_session = $this->session->my_session_id;

        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $tempData['id_session'] = $id_session;
        if($tempData['id'] && $tempData['id']>0)
        {
            $id =  $tempData['id'];
            unset($tempData['id']);
            $inserted_id = $this->main_invoice_model->updateTempDetails($tempData,$id);
        }
        else
        {
            unset($tempData['id']);
            $inserted_id = $this->main_invoice_model->addTempDetails($tempData);
// echo "<Pre>";  print_r($tempData);exit;
        }
        $data = $this->displaytempdata();
        
        echo $data;        
    }

    function displaytempdata()
    {
        $id_session = $this->session->my_session_id;
        
        $temp_details = $this->main_invoice_model->getTempMainInvoiceDetails($id_session); 
        // echo "<Pre>";print_r($details);exit;
        $table = "<table  class='table' id='list-table'>
                  <tr>
                    <th>Sl. No</th>
                    <th>Fee Item</th>
                    <th>Amount</th>
                    <th>Action</th>
                </tr>";
                $total_amount = 0;
                    for($i=0;$i<count($temp_details);$i++)
                    {
                    $id = $temp_details[$i]->id;
                    $fee_setup = $temp_details[$i]->fee_setup;
                    $amount = $temp_details[$i]->amount;
                    $j = $i+1;
                        $table .= "
                        <tr>
                            <td>$j</td>
                            <td>$fee_setup</td>
                            <td>$amount</td>                           
                            <td>
                                <span onclick='deleteTempData($id)'>Delete</a>
                            </td>
                        </tr>";
                        $total_amount = $total_amount + $amount;
                    }

                    $table .= "
                        <tr>
                            <td></td>
                            <td style='text-align: right'>Total : </td>
                            <td><input type='hidden' id='inv-total-amount' value='$total_amount' />$total_amount</td>                           
                            <td></td>
                        </tr>";

        $table.= "</table>";
        return $table;
    }

    function tempDelete($id)
    {
        // echo "<Pre>";  print_r($id);exit;
        $id_session = $this->session->my_session_id;
        $inserted_id = $this->main_invoice_model->deleteTempData($id);
        $data = $this->displaytempdata();
        echo $data; 
    } 

    function tempadd1()
    {
        //echo "<Pre>";  print_r("adaf");exit;
        $id_session = $this->session->my_session_id;
        $id_fee_item = $this->security->xss_clean($this->input->post('id_fee_item'));
        $amount = $this->security->xss_clean($this->input->post('amount'));

        // echo "<Pre>";  print_r($id_session . "=". $amount);exit;
        $data = array(
               'id_session' => $id_session,
               'id_fee_item' => $id_fee_item,
               'amount' => $amount
            );
        $inserted_id = $this->main_invoice_model->addNewTempMainInvoiceDetails($data);
        //echo "<Pre>";  print_r($inserted_id);exit;

        $temp_details = array(
                'id' => $inserted_id,
                'amount' => $amount,
                'id_fee_item' => $id_fee_item,
            );
        $temp_details = $this->main_invoice_model->getTempMainInvoiceDetails($id_session);

        if(!empty($temp_details))
        {  
            $table = "
            <table  class='table' id='list-table'>
                <tr>
                    <th>Fee Item</th>
                    <th>Amount</th>
                    <th>Delete</th>
                </tr>";
                for($i=0;$i<count($temp_details);$i++)
                {
                    $fee_setup = $temp_details[$i]->fee_setup;
                    $amount = $temp_details[$i]->amount;
                    $id = $temp_details[$i]->id;

                    $table .= "
                <tr>
                    <td>$fee_setup</td>
                    <td ><input type='hidden' id='inv-total-amount' value='$amount' />$amount</td>             

                    <td>
                        <span onclick='deleteid($id)'>Delete</a>
                    <td>
                </tr>";
                }
                        
            $table .= "
            </table>";
            echo $table;
        }
    }


     function getStudentByProgrammeId($id)
     {       
            // print_r($id);exit;
            $results = $this->main_invoice_model->getStudentByProgrammeId($id);
            $programme_data = $this->main_invoice_model->getProgrammeById($id);

            // echo "<Pre>"; print_r($programme_data);exit;
            $programme_name = $programme_data->name;
            $programme_code = $programme_data->code;
            $total_cr_hrs = $programme_data->total_cr_hrs;
            $graduate_studies = $programme_data->graduate_studies;
            $foundation = $programme_data->foundation;

            $table="<select name='id_student' id='id_student' class='form-control' onchange='getStudentByStudentId(this.value)'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $full_name = $results[$i]->full_name;
            $table.="<option value=".$id.">".$full_name.
                    "</option>";

            }
            $table.="</select>";

            $view  = "
            <table border='1px' style='width: 100%'>
                <tr>
                    <td colspan='4'><h5 style='text-align: center;'>Programme Details</h5></td>
                </tr>
                <tr>
                    <th style='text-align: center;'>Programme Name</th>
                    <td style='text-align: center;'>$programme_name</td>
                    <th style='text-align: center;'>Programme Code</th>
                    <td style='text-align: center;'>$programme_code</td>
                </tr>
                <tr>
                    <th style='text-align: center;'>Total Credit Hours</th>
                    <td style='text-align: center;'>$total_cr_hrs</td>
                    <th style='text-align: center;'>Graduate Studies</th>
                    <td style='text-align: center;'>$graduate_studies</td>
                </tr>

            </table>
            <br>
            <br>
            ";

            echo $table;
            exit;
    }

    function getStudentByStudentId($id)
    {
         // print_r($id);exit;
            $student_data = $this->main_invoice_model->getStudentByStudentId($id);
            // echo "<Pre>"; print_r($student_data);exit;

            $student_name = $student_data->full_name;
            $student_nric = $student_data->nric;
            $email = $student_data->email;
            $nric = $student_data->nric;
            $intake_name = $student_data->intake_name;
            $programme_name = $student_data->programme_name;


            $table  = "
            <table border='1px' style='width: 100%'>
                <tr>
                    <td colspan='4'><h5 style='text-align: center;'>Student Details</h5></td>
                </tr>
                <tr>
                    <th style='text-align: center;'>Student Name</th>
                    <td style='text-align: center;'>$student_name</td>
                    <th style='text-align: center;'>Intake</th>
                    <td style='text-align: center;'>$intake_name</td>
                </tr>
                <tr>
                    <th style='text-align: center;'>Student Email</th>
                    <td style='text-align: center;'>$email</td>
                    <th style='text-align: center;'>Programme</th>
                    <td style='text-align: center;'>$programme_name</td>
                </tr>
                <tr>
                    <th style='text-align: center;'>Student NRIC</th>
                    <td style='text-align: center;'>$nric</td>
                    <th style='text-align: center;'></th>
                    <td style='text-align: center;'></td>
                </tr>

            </table>
            <br>
            <br>
            ";
            echo $table;
            exit;
    }

    function approvalList()
    {
        // if ($this->checkAccess('pr_entry_approval.list') == 0)
        if ($this->checkAccess('main_invoice_approval.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        { 
            
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;
            $data['mainInvoiceList'] = $this->main_invoice_model->getMainInvoiceListByStatus('0',$name);
            
                $array = $this->security->xss_clean($this->input->post('checkvalue'));
                if (!empty($array))
                {
 // echo "<Pre>"; print_r($array);exit;
                    $result = $this->main_invoice_model->editMainInvoiceList($array);
                    redirect($_SERVER['HTTP_REFERER']);
                }


            $this->global['pageTitle'] = 'Campus Management System : Approve Main Invoice';
            $this->loadViews("main_invoice/approval_list", $this->global, $data, NULL);
        }
    }
}
