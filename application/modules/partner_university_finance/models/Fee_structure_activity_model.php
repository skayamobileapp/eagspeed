<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Fee_structure_activity_model extends CI_Model
{

    function feeSetupListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('fee_setup');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function activityListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('activity_details');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function programListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function currencyListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('currency_setup');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function feeStructureActivityListSearch($data)
    {
        $this->db->select('fsa.*, p.code as program_code, p.name as program_name, ad.name as activity_name, ad.description as activity_description, fs.code as fee_code, fs.name as fee_name, cs.name as currency_name');
        $this->db->from('fee_structure_activity as fsa');
        $this->db->join('programme as p', 'fsa.id_program = p.id');
        $this->db->join('activity_details as ad', 'fsa.id_activity = ad.id');
        $this->db->join('fee_setup as fs', 'fsa.id_fee_setup = fs.id');
        $this->db->join('currency_setup as cs', 'fsa.id_currency = cs.id');
        if($data['id_program'] != '')
        {
            $this->db->where('fsa.id_program', $data['id_program']);
        }
        if($data['id_activity'] != '')
        {
            $this->db->where('fsa.id_activity', $data['id_activity']);
        }
        if($data['id_fee_setup'] != '')
        {
            $this->db->where('fsa.id_fee_setup', $data['id_fee_setup']);
        }
        if($data['trigger'] != '')
        {
            $this->db->where('fsa.trigger', $data['trigger']);
        }
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getFeeStructureActivity($id)
    {
        $this->db->select('*');
        $this->db->from('fee_structure_activity');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewFeeStructureActivity($data)
    {
        $this->db->trans_start();
        $this->db->insert('fee_structure_activity', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editFeeStructureActivity($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('fee_structure_activity', $data);
        return TRUE;
    }
}

