<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Welcome to EAG Applicant portal</h3>

             <table width='100%' class="table" border='1'>

             	  <tr>
             	  	  <th width='15%'> Date Registered / Applied</th>
             	  	  <th width='15%'> Date Submitted</th>
             	  	  <th width='15%'> Application Status</th>
             	  	  <th width='55%'> Need Action</th>
             	  </tr>


                


                 <?php 
                  if($getApplicantDetails->applicant_status=='Migrated')
                  { ?>
                   <tr>
                     <td><?php echo date('d-m-Y',strtotime($getApplicantDetails->created_dt_tm));?></td>
                   <td><?php echo date('d-m-Y',strtotime($getApplicantDetails->submitted_date));?></td>  <td><a href="/applicant/applicant/rstep1">View Application</a> 
                     </td>
                     <td>You have been offered, Kindly login to student portal <br/>
                        <a href="http://eag-student.camsedu.com/" target="_blank">Click here to login</td>         
                   </tr>
                 <?php
                  }
                  ?>


                  <?php 
                  if($getApplicantDetails->applicant_status=='Draft')
                  { ?>
             	  	 <tr>
             	  	 	 <td><?php echo date('d-m-Y',strtotime($getApplicantDetails->created_dt_tm));?></td>
             	  	 	 <td>-</td>
             	  	 	 <td><a href="/applicant/applicant/step1">View Application</a> 
             	  	 	 </td>
                     <td>Complete the application</td>         
             	  	 </tr>
                 <?php
                  }
                  ?>


                  <?php 
                  if($getApplicantDetails->applicant_status=='Submitted'  && $getApplicantDetails->is_temp_offer_letter=='0')
                  {
                   ?>
                     <tr>
                         <td><?php echo date('d-m-Y',strtotime($getApplicantDetails->created_dt_tm));?></td>
                         <td><?php echo date('d-m-Y',strtotime($getApplicantDetails->submitted_date));?></td>
                         <td><?php echo $getApplicantDetails->applicant_status;?> <br/><br/><a href="/applicant/applicant/rstep1">View Application</a> 
                         </td>
                         <td>Pending review and approval by Admission Department.</td>
                     </tr>
                  <?php
                  }
                  ?>


                  <?php 
                  if($getApplicantDetails->applicant_status=='Submitted' && $getApplicantDetails->id_program_requirement=='99999')
                  {
                   ?>
                     <tr>
                         <td><?php echo date('d-m-Y',strtotime($getApplicantDetails->created_dt_tm));?></td>
                         <td><?php echo date('d-m-Y',strtotime($getApplicantDetails->submitted_date));?></td>
                         <td>Pending APEL Test.<br/><br/><a href="/applicant/applicant/rstep1">View Application</a> 
                         </td>
                         <td>The Admission Officer will contact you and arrange for your APEL Test.</td>
                     </tr>
                  <?php
                  }
                  ?>


                  <?php 
                  if($getApplicantDetails->applicant_status=='Submitted' && $getApplicantDetails->is_temp_offer_letter=='1' && $getApplicantDetails->id_program_requirement!='99999')
                  {
                   ?>
                     <tr>
                         <td><?php echo date('d-m-Y',strtotime($getApplicantDetails->created_dt_tm));?></td>
                         <td><?php echo date('d-m-Y',strtotime($getApplicantDetails->submitted_date));?></td>
                         <td>Conditional Offered  <br/><br/>
<a href="/applicant/applicant/generateTempOfferLetter">Click here </a>  for Conditional Offer Letter and Fee Structure Information<br/><br/><a href="/applicant/applicant/rstep1">View Application</a> 
                         </td>
                         <td>The Admission Officer will verify your documents and application before your offer is updated to Offered without any condition.</td>
                     </tr>
                  <?php
                  }
                  ?>



                   <?php 
                  if($getApplicantDetails->applicant_status=='Pending documents')
                  {
                   ?>
                     <tr>
                         <td><?php echo date('d-m-Y',strtotime($getApplicantDetails->created_dt_tm));?></td>
                         <td><?php echo date('d-m-Y',strtotime($getApplicantDetails->submitted_date));?></td>
                         <td><?php echo $getApplicantDetails->applicant_status;?> <br/><br/>
                         </td>
                         <td>Please upload the pending documents. <br/> <a href="/applicant/applicant/step4">Click here to upload the documents</a> </td>
                     </tr>
                  <?php
                  }
                  ?>



                  <?php 
                  if($getApplicantDetails->applicant_status=='Approved')
                  {
                  ?>
                     <tr>
                        <td><?php echo date('d-m-Y',strtotime($getApplicantDetails->created_dt_tm));?></td>
                        <td><?php echo date('d-m-Y',strtotime($getApplicantDetails->submitted_date));?></td>
                        <td>Offered <br/><br/><a href="/applicant/applicant/rstep1">View Application</a> <br/><br/><a href="/applicant/applicant/generateTempOfferLetter">Click here for Offer Letter</a> 
                            <br/><br/><a href="/applicant/applicant/feestructure">View Fee Structure</a> 

                        </td>

                              <td>Please tick (✓). <br/>
                                    <p><input type='checkbox' name="accept" id="accept"/>I hereby confirm my acceptance of your offer.</p> 
 <p>I agree to study and complete the <programme name>  and fulfill other criteria that USAS deems necessary as stated in this Offer of Admission. I understand that USAS reserves the right to terminate my registration at the University, if it is discovered that my application contains incorrect or fraudulent information, or if there is a reassessment of the fee status, or significant information has been omitted from my application form. </p> 
                          <p>I also understand that USAS reserves the right to amend/modify the terms and conditions of the offer from time to time as deemed appropriate</p> 

                          <p>Proceed to pay Registration Fee.

                          <input type="hidden" id="id_applicant" name="id_applicant" class="form-control" value="<?php echo $getApplicantDetails->id ?>" readonly="readonly">
                          <input type="hidden" id="id_main_invoice" name="id_main_invoice" class="form-control" value="<?php echo $applicantInvoice->id ?>" readonly="readonly">
                          <input type="hidden" id="total_amount" name="total_amount" class="form-control" value="<?php echo $applicantInvoice->total_amount ?>" readonly="readonly">
                          <input type="hidden" id="paid_amount" name="paid_amount" class="form-control" value="<?php echo $applicantInvoice->paid_amount ?>" readonly="readonly">
                          <input type="hidden" id="balance_amount" name="balance_amount" class="form-control" value="<?php echo $applicantInvoice->balance_amount ?>" readonly="readonly">
                          <input type="hidden" id="currency" name="currency" class="form-control" value="<?php echo $applicantInvoice->currency ?>" readonly="readonly">



                          <input type='button' class="btn btn-primary next" value="Click here to pay" onclick="validatecheckbox()">Click here to pay</p>
                          <br/>
                          <p>
                              <a href="">Click here to Decline</a>
                        </td>
                       
                               
                      </tr>
                  <?php
                  }
                  ?>



             </table>

        </div>
    </div>
</div>


<script type="text/javascript">
    
    function validatecheckbox()
    {
      if($("#accept").prop('checked')==true)
      {
        generateReceiptAndMoveApplicant();
      }
      else
      {
          alert("Please accept the offer");
      }
    }

     

    function generateReceiptAndMoveApplicant()
    {
          var balance_amount = $("#balance_amount").val();

          var tempPR = {};
          tempPR['id_applicant'] = $("#id_applicant").val();
          tempPR['id_main_invoice'] = $("#id_main_invoice").val();
          tempPR['total_amount'] = $("#total_amount").val();
          tempPR['balance_amount'] = balance_amount;
          tempPR['currency'] = $("#currency").val();
          tempPR['paid_amount'] = $("#paid_amount").val();
          // alert(tempPR['id_program']);


          if(tempPR['id_applicant'] != '')
          {

              $.ajax(
              {
                 url: '/applicant/applicant/generateReceiptAndMoveApplicant',
                  type: 'POST',
                 data:
                 {
                  tempData: tempPR
                 },
                 error: function()
                 {
                  alert('Something is wrong');
                 },
                 success: function(result)
                 {
                      // alert(result);

                    var billing_to = '<?php echo $getApplicantDetails->billing_to; ?>';
                  
                    if(billing_to == 'Student')
                    {
                      alert('Receipt Amount ' + balance_amount + ' Paid And Succesfully Migrated As Student');
                    }
                    else
                    {
                      alert('Applicant Migrated Succesfully');
                    } 

                    window.location.reload();
                 }
              });
          }
        }
      

</script>