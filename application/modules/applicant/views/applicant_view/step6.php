    <form id="form_applicant" action="" method="post" enctype="multipart/form-data">

            <div class="main-container clearfix">
                <div class="page-title clearfix">
                    <h3>Programme Application</h3>                    
                </div>
                <div id="wizard" class="wizard">
                    <div class="wizard__content">
                        <header class="wizard__header">
                        <div class="wizard__steps">
                          <nav class="steps">
                            <div class="step -completed">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="step1" class="step__text">Profile Details</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -start"></div>
    
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
    
                            <div class="step -completed">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="step2" class="step__text">Contact Information</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
    
                            <div class="step -completed">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="step3" class="step__text">Program Interest</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
    
                            <div class="step -completed">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="step4" class="step__text">Document Upload</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
    
                            <div class="step -completed">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="step5" class="step__text">Discount Information</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
    
                            <div class="step">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="step6" class="step__text">Declaration Form</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
                          </nav>
                        </div>
                      </header>
                      <div class="panels">
                        <div class="paneld">
                          
                           <div class="clearfix">
                            <!-- Form Container Starts-->
<div id="view_requirements">
</div>
<div class="form-container">
   <h4 class="form-group-title">Declaration of Submission</h4>
   <br> 
   &emsp;<input type="checkbox" id="is_submitted" name="is_submitted" value="1" >&emsp;
  I, <b><?php echo $getApplicantDetails->first_name;?> <?php echo $getApplicantDetails->last_name;?></b> hereby declare that all information and statements made in this application are correct. <br/>
  I am fully aware that the <b><?php echo $getApplicantDetails->uniname;?></b> Admission has the right to reject my application or terminate  candidature if the information given in this application is incorrect or incomplete. 
</div>
<div class="form-container">
   <h4 class="form-group-title">Submission</h4>
   <br>
   <h4 class="modal-title">Are you sure you wish to submit your application?
      </h4>
 
  <input type='radio' name='submitapp' value='Submitted' required/> Yes
  <input type='radio' name='submitapp' value='Draft' required/> No
</div>
             
                        </div>    
                      </div>
    
                      <div class="wizard__footer">
                        <button  style="position: absolute; right: 0;" class="btn btn-primary next" type="button" class="btn btn-primary btn-lg" onclick="validateSubmission()">Submit </button>
                        <br/>
                        <br/>
                         </div>
                      </div>
                    </div>
    
                    <h2 class="wizard__congrats-message">
                      Congratulations!!
                    </h2>
                  </div>
                <footer class="footer-wrapper">
                    <p>&copy; 2019 All rights, reserved</p>
                </footer>
            </div>        
        </div>
    </div>      
    </form>

<script type="text/javascript">

   function validateSubmission()
    {
        $('#id_intake').prop('disabled', false);
        $('#id_program').prop('disabled', false);
        if($('#form_applicant').valid())
        {
            $('#form_applicant').submit();
            // $('#myModal').modal('show');
            // alert("Data Added");
        }
    }



  $(document).ready(function()
  {

       





            $("#form_applicant").validate({
            rules: {
                submitapp: {
                    required: true
                }
            },
            messages: {
                submitapp: {
                    required: "<p class='error-text'>This field is required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        }); 







  });


  
</script>


