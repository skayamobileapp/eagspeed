<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">


        <div class="page-title clearfix">
            <h3>Upload Profile Picture</h3>
        </div>    


        <div class="form-container">
            <h4 class="form-group-title">Student Details</h4>
            <div class='data-list'>
                <div class='row'> 
                    <div class='col-sm-6'>
                        <dl>
                            <dt>Applicant Name :</dt>
                            <dd><?php echo ucwords($applicantDetails->full_name);?></dd>
                        </dl>
                        <dl>
                            <dt>Student NRIC :</dt>
                            <dd><?php echo $applicantDetails->nric ?></dd>
                        </dl>
                                           
                    </div>
                    
                    <div class='col-sm-6'>  
                        <dl>
                            <dt>Student Email :</dt>
                            <dd><?php echo $applicantDetails->email_id; ?></dd>
                        </dl>
                         
                        
                    </div>
                </div>
            </div>
        </div>

        <form id="form_master" action="" method="post" enctype="multipart/form-data">


        	<div class="row">

	        	<div class="col-sm-4">
                    <div class="form-group">
                        <label>Profile Pic 

                            <span class='error-text'>*</span>
                            <?php 
                            $applicant_profile_pic = $this->session->applicant_profile_pic;
                            if($applicant_profile_pic != '')
                            {
                            ?>
                            <a href="<?php echo '/assets/images/' . $applicant_profile_pic; ?>" target="popup" onclick="window.open(<?php echo '/assets/images/' . $applicant_profile_pic; ?>)" title="<?php echo  $applicant_name; ?>"> View </a>

                            <?php
                            }
                            ?>


                        </label>
                        <input type="file" name="profile_pic" id="profile_pic">
                    </div>  
                </div>



	        </div>


	        <div class="button-block clearfix">
	            <div class="bttn-group">
	                <button type="submit" class="btn btn-primary btn-lg">Save</button>
	            </div>
	        </div>

        </form>




        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>

<script type="text/javascript">
	
	$(document).ready(function() {
        $("#form_master").validate({
            rules: {
                profile_pic: {
                    required: true
                }
            },
            messages: {
                profile_pic: {
                    required: "<p class='error-text'>Select Profile Pic</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

</script>