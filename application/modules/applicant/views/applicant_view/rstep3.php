    <form id="form_applicant" action="" method="post" enctype="multipart/form-data">

            <div class="main-container clearfix">
                <div class="page-title clearfix">
                    <h3>Programme Application</h3>                    
                </div>
                <div id="wizard" class="wizard">
                    <div class="wizard__content">
                       <header class="wizard__header">
                        <div class="wizard__steps">
                          <nav class="steps">
                            <div class="step -completed">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="rstep1" class="step__text">Profile Details</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -start"></div>
    
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
    
                            <div class="step -completed">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="rstep2" class="step__text">Contact Information</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
    
                            <div class="step -completed">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="rstep3" class="step__text">Program Interest</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
    
                            <div class="step -completed">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="rstep4" class="step__text">Document Upload</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
    
                            <div class="step -completed">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="rstep5" class="step__text">Discount Information</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
    
                            <div class="step -completed">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="rstep6" class="step__text">Declaration Form</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
                          </nav>
                        </div>
                      </header>
    
                      <div class="panels">
                        <div class="paneld">

                          
                           <div class="row">
                               <div class="col-sm-4">
                                  <div class="form-group">
                                     <label>Partner University <span class='error-text'>*</span></label>
                                     <select name="id_university" id="id_university" class="form-control selitemIcon"  disabled onchange="getBranchesByPartnerUniversity(this.value)">
                                        <option value="">Select</option>
                                        <?php
                                           if (!empty($partnerUniversityList))
                                           {
                                               foreach ($partnerUniversityList as $record)
                                               {?>
                                        <option value="<?php echo $record->id;  ?>"
                                           <?php 
                                              if($record->id == $getApplicantDetails->id_university)
                                              {
                                                  echo "selected=selected";
                                              } ?>
                                           >
                                           <?php echo $record->code . " - " . $record->name;?>
                                        </option>
                                        <?php
                                           }
                                           }
                                           ?>
                                     </select>
                                  </div>
                               </div>
                              <div class="col-sm-4" style="display: none;">
                                  <div class="form-group">
                                     <label>Branch <span class='error-text'>*</span></label>
                                     <span id="view_branch">
                                         <select class="form-control" disabled   id='id_branch' name='id_branch'>
                                            <option value=''></option>
                                          </select>
                                     </span>
                                  </div>
                              </div>



                               

                               <div class="col-sm-4">
                                  <div class="form-group">
                                     <label>Programme <span class='error-text'>*</span></label>
                                     <span id="view_programme">
                                          <select class="form-control" disabled   id='id_program' name='id_program'>
                                            <option value=''></option>
                                          </select>

                                     </span>
                                  </div>
                               </div>


                              <div class="col-sm-4">
                                  <div class="form-group">
                                     <label>Learning Mode <span class='error-text'>*</span></label>
                                     <span id="view_program_scheme">
                                          <select class="form-control" disabled   id='id_program_scheme' name='id_program_scheme'>
                                            <option value=''></option>
                                          </select>

                                     </span>
                                  </div>
                               </div>

                            

                              <div class="col-sm-4" style="display: none;">
                                  <div class="form-group">
                                     <label>Program Scheme <span class='error-text'>*</span></label>
                                     <span id="view_program_has_scheme">
                                          <select class="form-control" disabled   id='id_program_has_scheme' name='id_program_has_scheme'>
                                            <option value=''></option>
                                          </select>

                                     </span>
                                  </div>
                               </div>
                           
                               <div class="col-sm-4">
                                  <div class="form-group">
                                     <label>Intake <span class='error-text'>*</span></label>
                                     <span id="view_intake">
                                        <select class="form-control" disabled   id='id_intake' name='id_intake'>
                                            <option value=''></option>
                                          </select>
                                     </span>
                                  </div>
                               </div>

                            


                             




                              <div class="col-sm-4">
                                  <div class="form-group">
                                     <label>Programme Structure Type <span class='error-text'>*</span></label>
                                     <span id="view_program_structure_type">
                                         <select class="form-control" disabled   id='id_program_structure_type' name='id_program_structure_type'>
                                            <option value=''></option>
                                          </select>
                                     </span>
                                  </div>
                              </div>



                            </div>


                    <?php if($getApplicantDetails->nationality!='1') { ?> 
                               <div class="row" id='englishrequiredid' style="display: none;">



                            <h4 style="padding-left:20px;">Language Eligibility</h4>



                              <div class="col-sm-4">
                                  <div class="form-group">
                                     <label>Type <span class='error-text'>*</span></label>
                                         <select class="form-control" disabled   id='id_english_test' name='id_english_test'>
                                            <option value='1'>International English Language Testing System</option>
                                            <option value='2'>Malaysian University English Test (MUET)</option>
                                          </select>
                                  </div>
                              </div>
                               <div class="col-sm-4">
                                  <div class="form-group">
                                     <label>Year <span class='error-text'>*</span></label>

                                         <select class="form-control" disabled   id='english_year' name='english_year'>

                                          <?php for($y=1990;$y<2020;$y++) { ?>

                                                <option value='<?php echo $y;?>'><?php echo $y;?></option>
                                          <?php } ?> 
                                           
                                           
                                          </select>

                                  </div>
                              </div>
                               <div class="col-sm-4">
                                  <div class="form-group">
                                     <label>Grade <span class='error-text'>*</span></label>
                                         <input type="text" class="form-control" disabled   id='english_grade' name='english_grade' value="<?php echo $getApplicantDetails->english_grade;?>">
                                  </div>
                              </div>
                              <div class="col-sm-4">
                                  <div class="form-group">
                                     <label>Upload Certificate <span class='error-text'>*</span>
                                      <span style="font-size:10px;font-weight: bold;"><br/>(pdf and images only, Max size:1 MB)</span></label>
                                         <input type="file" class="form-control" disabled   id='english_file' name='english_file'>
                                  </div>
                              </div>




                            </div>
                 
                         <?php } ?>


                         <div class="row">


                            <h4 style="padding-left:20px;">Entry Requirements</h4>



                              <div id="view_requirements">
</div>



                            </div>
                 
                        </div> 

                           
                      </div>
    
                      <div class="wizard__footer">
                        <a href="/applicant/applicant/rstep2" class="btn btn-primary">Previous</a>

                        <button class="btn btn-link mr-3"></button>
                        <a href="rstep4" class="btn btn-primary next">Next</a>
                      </div>
                    </div>
    
                    <h2 class="wizard__congrats-message">
                      Congratulations!!
                    </h2>
                  </div>
                <footer class="footer-wrapper">
                    <p>&copy; 2019 All rights, reserved</p>
                </footer>
            </div>        
        </div>
    </div>      
    </form>
    
 

<script type="text/javascript">

    function getProgrammeByEducationLevelId(id_education_level)
    {
      // alert(id_education_level);
      if(id_education_level != '')
        {

            $.get("/applicant/applicant/getProgrammeByEducationLevelId/"+id_education_level, function(data, status){
           
                $("#view_programme").html(data);
                $("#view_programme").show();
                $("#id_program").prop("disabled", true);

            });
        }

    }


    function getprogramScheme(id) {

      if(id!='') {
         $.get("/applicant/applicant/getSchemeByProgramId/"+id, function(data, status)
            {

              $("#view_program_has_scheme").html(data);
              $("#view_program_has_scheme").show();

              
              var nationality = "<?php echo $getApplicantDetails->nationality;?>";

              if(nationality == '1')
              {
                $("#id_program_has_scheme").find('option[value="1"]').attr('selected',true);
                // alert(nationality);
              }
              else
              {
                $("#id_program_has_scheme").find('option[value="2"]').attr('selected',true);
              }
              
            });


         $.get("/applicant/applicant/getIntakeByProgramme/"+id, function(data, status)
            {
                var idstateselected = "<?php echo $getApplicantDetails->id_intake;?>";

                $("#view_intake").html(data);
                $("#id_intake").find('option[value="'+idstateselected+'"]').attr('selected',true);
                $('select').select2();
            });

          $.get("/applicant/applicant/getProgramSchemeByProgramId/"+id, function(data, status){
                $("#view_program_scheme").html(data);
                $("#view_program_scheme").show();
            });  


             $.get("/applicant/applicant/getProgramStructureTypeByProgramId/"+id, function(data, status){
                $("#view_program_structure_type").html(data);
                $("#view_program_structure_type").show();
            });



              $.get("/applicant/applicant/getIndividualEntryRequirement/"+id, function(data, status){
                            var id_program_requirement = "<?php echo $getApplicantDetails->id_program_requirement;?>";

                $("#view_requirements").html(data);
                $("#view_requirements").show();
            });  



              
              $.get("/applicant/applicant/checkenglish/"+id, function(data, status){
                           var engrequried = data;
                           $("#englishrequiredid").hide();
                           if(engrequried=='1') {
                           $("#englishrequiredid").show();


                           }
            }); 



      }

    }


    function getIntakeByProgramme(id)
     {
        if(id != '')
        {
            $.get("/applicant/applicant/getIntakeByProgramme/"+id, function(data, status){
           
                $("#view_intake").html(data);
                $("#view_intake").show();
            });

            // Programme Learning Mode
           


           
            

            $.get("/applicant/applicant/getIndividualEntryRequirement/"+id, function(data, status){
                $("#view_requirements").html(data);
                $("#view_requirements").show();
            });


            $.get("/applicant/applicant/getProgramStructureTypeByProgramId/"+id, function(data, status){
                $("#view_program_structure_type").html(data);
                $("#view_program_structure_type").show();
            });

        }
     }


     function getBranchesByPartnerUniversity(id)
     {
        $.get("/applicant/applicant/getBranchesByPartnerUniversity/"+id, function(data, status)
        {
          $("#view_branch").html(data);
          $("#view_branch").show();
        });


         $.get("/applicant/applicant/getProgramByPartnerUniversity/"+id, function(data, status)
        {
          $("#view_programme").html(data);
          $("#view_programme").show();
        });

     }

      function checkFeeStructure()
     {
        var tempPR = {};
        tempPR['id_education_level'] = $("#id_degree_type").val();
        tempPR['id_program'] = $("#id_program").val();
        tempPR['id_intake'] = $("#id_intake").val();
        tempPR['id_program_scheme'] = $("#id_program_scheme").val();
        tempPR['id_program_has_scheme'] = $("#id_program_has_scheme").val();
        // alert(tempPR['id_program']);

        if(tempPR['id_program'] != '' && tempPR['id_intake'] != ''  && tempPR['id_program_scheme'] != '' && tempPR['id_program_has_scheme'] != '')
        {

            // $.ajax(
            // {
            //    url: '/applicant/applicant/checkFeeStructure',
            //     type: 'POST',
            //    data:
            //    {
            //     tempData: tempPR
            //    },
            //    error: function()
            //    {
            //     alert('Something is wrong');
            //    },
            //    success: function(result)
            //    {
            //         if(result == '0')
            //         {

            //             alert('No Fee Structure Defined For The Selected Particulars, Select Another Combination');
            //             $(this).data('options', $('#id_intake option').clone());

            //             var idstateselected = 0;

            //             // $("#id_program").find('option[value="'+idstateselected+'"]').attr('selected',true);
            //             // $("#id_intake").find('option[value="'+idstateselected+'"]').attr('selected',true);
            //             // $("#id_program_scheme").find('option[value="'+idstateselected+'"]').attr('selected',true);
            //             // $("#id_program_has_scheme").find('option[value="'+idstateselected+'"]').attr('selected',true);
            //             // $('select').select2();

            //             // $("#id_program_scheme").find('option[value="'+idstateselected+'"]').attr('selected',true);
            //             // $('select').select2();


            //             // $("#id_intake").html('<option value="">').append(options);
            //             // $("#id_intake").val('');

            //             // $(this).data('options', $('#id_program_scheme option').clone());
            //             // $("#id_program_scheme").html('<option value="">').append(options);
            //             // $("#id_program_scheme").val('');
                        
            //         }
            //    }
            // });
        }
     }



    $(document).ready(function() {
        $('select').select2();

        var idprogram = "<?php echo $getApplicantDetails->id_program;?>";
        var id_degree_type = "<?php echo $getApplicantDetails->id_degree_type;?>";

        if(id_degree_type != '' && id_degree_type != 0)
        {

          $.get("/applicant/applicant/getProgrammeByEducationLevelId/"+id_degree_type, function(data, status){
           
                $("#view_programme").html(data);
                // $("#view_programme").show();
                $("#id_program").find('option[value="'+idprogram+'"]').attr('selected',true);
                $("#id_program").find('option[value="'+idprogram+'"]').attr('disabled',true);

                $('select').select2();
            });
        }




      if(idprogram!='')
      {
         $.get("/applicant/applicant/getIntakeByProgramme/"+idprogram, function(data, status)
            {
                var idstateselected = "<?php echo $getApplicantDetails->id_intake;?>";

                $("#view_intake").html(data);
                $("#id_intake").find('option[value="'+idstateselected+'"]').attr('selected',true);
                $("#id_intake").find('option[value="'+idstateselected+'"]').attr('disabled',true);

                $('select').select2();
            });



         $.get("/applicant/applicant/getProgramSchemeByProgramId/"+idprogram, function(data, status)
            {
                var idprogramschemeselected = "<?php echo $getApplicantDetails->id_program_scheme;?>";
                $("#view_program_scheme").html(data);
                $("#id_program_scheme").find('option[value="'+idprogramschemeselected+'"]').attr('selected',true);
                $("#id_program_scheme").find('option[value="'+idprogramschemeselected+'"]').attr('disabled',true);
                $('select').select2();
            });



          $.get("/applicant/applicant/getProgramStructureTypeByProgramId/"+idprogram, function(data, status)
          {
                $("#view_program_structure_type").html(data);
                $("#view_program_structure_type").show();

                var id_program_structure_type = "<?php echo $getApplicantDetails->id_program_structure_type;?>";
          
                $("#id_program_structure_type").find('option[value="'+id_program_structure_type+'"]').attr('selected',true);
                $("#id_program_structure_type").find('option[value="'+id_program_structure_type+'"]').attr('disabled',true);
                
                $('select').select2();
            });



         $.get("/applicant/applicant/getSchemeByProgramId/"+idprogram, function(data, status)
            {
              // alert(data);
                var id_program_has_scheme = "<?php echo $getApplicantDetails->id_program_has_scheme;?>";
                $("#view_program_has_scheme").html(data);
                $("#id_program_has_scheme").find('option[value="'+id_program_has_scheme+'"]').attr('selected',true);
                $("#id_program_has_scheme").find('option[value="'+id_program_has_scheme+'"]').attr('disabled',true);
                $('select').select2();
            });

          var id_university = "<?php echo $getApplicantDetails->id_university;?>";



         $.get("/applicant/applicant/getBranchesByPartnerUniversity/"+id_university, function(data, status)
            {
                var id_branch = "<?php echo $getApplicantDetails->id_branch;?>";
                $("#view_branch").html(data);
                $("#id_branch").find('option[value="'+id_branch+'"]').attr('selected',true);
                $("#id_branch").find('option[value="'+id_branch+'"]').attr('disabled',true);
                $('select').select2();
            });

         $.get("/applicant/applicant/getProgramByPartnerUniversity/"+id_university, function(data, status)
        {

            var id_program = "<?php echo $getApplicantDetails->id_program;?>";
                $("#view_programme").html(data);
                $("#id_program").find('option[value="'+id_program+'"]').attr('selected',true);
                // $("#id_branch").find('option[value="'+id_branch+'"]').attr('disabled',true);
                $('select').select2();


        });

         $("#target :select").prop("disabled", true);





        $.get("/applicant/applicant/getIndividualEntryRequirement/"+idprogram, function(data, status){
            // alert(id);
                $("#view_requirements").html(data);
                $("#view_requirements").show();
            });  

              $.get("/applicant/applicant/checkenglish/"+idprogram, function(data, status){
                           var engrequried = data;
                           $("#englishrequiredid").hide();
                           if(engrequried=='1') {
                           $("#englishrequiredid").show();


                           }
            });    
     }

        $("#form_applicant").validate({
            rules: {
                salutation: {
                    required: true
                },
                 first_name: {
                    required: true
                },
                 last_name: {
                    required: true
                },
                 phone: {
                    required: true
                },
                 email_id: {
                    required: true
                },
                 password: {
                    required: true
                },
                 nric: {
                    required: true
                },
                 gender: {
                    required: true
                },
                 id_program: {
                    required: true
                },
                 id_intake: {
                    required: true
                },
                employee_discount :{
                    required : true
                },
                sibbling_discount :{
                    required : true
                },
                 sibbling_name: {
                    required: true
                },
                 sibbling_nric: {
                    required: true
                },
                 employee_name: {
                    required: true
                },
                 employee_nric: {
                    required: true
                },
                 employee_designation: {
                    required: true
                },
                 date_of_birth: {
                    required: true
                },
                 nationality: {
                    required: true
                },
                 id_race: {
                    required: true
                },
                 mail_address1: {
                    required: true
                },
                 mailing_city: {
                    required: true
                },
                 mailing_country: {
                    required: true
                },
                 mailing_state: {
                    required: true
                },
                 mailing_zipcode: {
                    required: true
                },
                 permanent_address1: {
                    required: true
                },
                 permanent_city: {
                    required: true
                },
                 permanent_country: {
                    required: true
                },
                 permanent_state: {
                    required: true
                },
                 permanent_zipcode: {
                    required: true
                },
                 is_submitted: {
                    required: true
                },
                is_hostel: {
                    required: true
                },
                id_degree_type: {
                    required: true
                },
                passport: {
                    required: true
                },
                program_scheme: {
                    required: true
                },
                 alumni_discount: {
                    required: true
                },
                alumni_name: {
                    required: true
                },
                alumni_email: {
                    required: true
                },
                alumni_nric: {
                    required: true
                },
                id_program_scheme: {
                    required: true
                },
                id_branch: {
                    required: true
                },
                id_university: {
                  required: true
                },
                id_program_has_scheme: {
                  required: true
                },
                id_program_structure_type: {
                  required: true
                }
            },
            messages: {
                salutation: {
                    required: "<p class='error-text'>Salutation required</p>",
                },
                first_name: {
                    required: "<p class='error-text'>First Name required</p>",
                },
                last_name: {
                    required: "<p class='error-text'>Last Name required</p>",
                },
                email_id: {
                    required: "<p class='error-text'>Email required</p>",
                },
                phone: {
                    required: "<p class='error-text'>Phone required</p>",
                },
                gender: {
                    required: "<p class='error-text'>Gender required</p>",
                },
                nric: {
                    required: "<p class='error-text'>NRIC required</p>",
                },
                password: {
                    required: "<p class='error-text'>Password required</p>",
                },
                id_program: {
                    required: "<p class='error-text'>Select Program</p>",
                },
                id_intake: {
                    required: "<p class='error-text'>Select Intake</p>",
                },
                employee_discount: {
                    required: "<p class='error-text'>Employee Discount required</p>",
                },
                sibbling_discount: {
                    required: "<p class='error-text'>Sibbling Discount required</p>",
                },
                sibbling_name: {
                    required: "<p class='error-text'>Sibling Name required</p>",
                },
                sibbling_nric: {
                    required: "<p class='error-text'>Sibling NRIC required</p>",
                },
                employee_name: {
                    required: "<p class='error-text'>Employee Name required</p>",
                },
                employee_nric: {
                    required: "<p class='error-text'>Employee NRIC required</p>",
                },
                employee_designation: {
                    required: "<p class='error-text'>Employee Designation required</p>",
                },
                date_of_birth: {
                    required: "<p class='error-text'>Select Date Of Birth</p>",
                },
                nationality: {
                    required: "<p class='error-text'>Select Type Of Nationality</p>",
                },
                id_race: {
                    required: "<p class='error-text'>Select Race</p>",
                },
                mail_address1: {
                    required: "<p class='error-text'>Enter Mailing Address 1</p>",
                },
                mailing_city: {
                    required: "<p class='error-text'>Enter Mailimg City</p>",
                },
                mailing_country: {
                    required: "<p class='error-text'>Select Mailing Country</p>",
                },
                mailing_state: {
                    required: "<p class='error-text'>Select Mailing State</p>",
                },
                mailing_zipcode: {
                    required: "<p class='error-text'>Enter Mailing Zipcode</p>",
                },
                permanent_address1: {
                    required: "<p class='error-text'>Enter Permanent Address 1</p>",
                },
                permanent_city: {
                    required: "<p class='error-text'>Enter Permanent City</p>",
                },
                permanent_country: {
                    required: "<p class='error-text'>Select Permanent Country</p>",
                },
                permanent_state: {
                    required: "<p class='error-text'>Select Permanent State</p>",
                },
                permanent_zipcode: {
                    required: "<p class='error-text'>Enter Permanent Zipcode</p>",
                },
                is_submitted: {
                    required: "<p class='error-text'>Check Indicate that you accept the Terms and Conditions</p>",
                },
                is_hostel: {
                    required: "<p class='error-text'>Select Accomodation Required</p>",
                },
                id_degree_type: {
                    required: "<p class='error-text'>Select Degree Level</p>",
                },
                passport: {
                    required: "<p class='error-text'>Passport No. Required</p>",
                },
                program_scheme: {
                    required: "<p class='error-text'>Select Program Scheme</p>",
                },
                alumni_discount: {
                    required: "<p class='error-text'>Select Alumni Discount Applicable </p>",
                },
                alumni_name: {
                    required: "<p class='error-text'>Alumni Name Required</p>",
                },
                alumni_email: {
                    required: "<p class='error-text'>Alumni Email Required </p>",
                },
                alumni_nric: {
                    required: "<p class='error-text'>Alumni NRIC Required</p>",
                },
                id_program_scheme: {
                    required: "<p class='error-text'>Select Learning Mode</p>",
                },
                id_branch: {
                    required: "<p class='error-text'>Select Branch</p>",
                },
                id_university: {
                    required: "<p class='error-text'>Select University</p>",
                },
                id_program_has_scheme: {
                    required: "<p class='error-text'>Select Program Scheme</p>",
                },
                id_program_structure_type: {
                    required: "<p class='error-text'>Select Program Structure Type</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
  });




    $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
    });

</script>