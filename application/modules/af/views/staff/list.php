<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List of Academic Facilitator</h3>
      <a href="add" class="btn btn-primary">+ Add Academic Facilitator</a>
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
               Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Name</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="name" value="<?php echo $searchParam['name']; ?>">
                      </div>
                    </div>
                  </div>

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Department</label>
                      <div class="col-sm-8">
                        <select name="id_department" id="id_department" class="form-control">
                          <option value="">Select</option>
                          <?php
                          if (!empty($departmentList)) {
                            foreach ($departmentList as $record)
                            {
                              $selected = '';
                              if ($record->id == $searchParam['id_department']) {
                                $selected = 'selected';
                              }
                          ?>
                              <option value="<?php echo $record->id;  ?>"
                                <?php echo $selected;  ?>><?php echo  $record->code ."-".$record->name;  ?></option>
                          <?php
                            }
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                  </div>

                </div>
                
              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href='list' class="btn btn-link">Clear All Fields</a>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Name</th>
            <th>Mobile Number</th>
            <th>Email</th>
            <th>Staff Image</th>
              <th style="text-align: center;">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($staffDetails))
          {
            $i=1;
            foreach ($staffDetails as $record)
            {
             ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->name; ?></td>
                <td><?php echo $record->mobile_number; ?></td>
                <td><?php echo $record->email; ?></td>
                  <td>
                  <?php if($record->image!='') {?>

                                <a href="/assets/images/<?php echo $record->image;?>" target="_blank">View Image</a>
                              <?php } else { 
                                 echo "No image";
                               }?>  
                </td>

                  <td class="text-center">

                  <a href="<?php echo 'edit/' . $record->id; ?>" title="Edit">
                    Edit
                  </a>
                </td>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script type="text/javascript">
    $('select').select2();
  
    function clearSearchForm()
    {
      window.location.reload();
    }
</script>