
<div class="container-fluid page-wrapper">
   <div class="main-container clearfix">
       <ul class="page-nav-links">
            <li><a href="/af/staff/edit/<?php echo $id_staff;?>">Faculty Details</a></li>
            <li><a href="/af/staff/bankaccount/<?php echo $id_staff;?>">Bank Account Details</a></li>
            <li class="active"><a href="/af/staff/qualification/<?php echo $id_staff;?>">Qualification Details</a></li>
            <li><a href="/af/staff/workexperience/<?php echo $id_staff;?>">Work Experience Details</a></li>
            <li><a href="/af/staff/specialization/<?php echo $id_staff;?>">Specialization Details</a></li>
        </ul>

        <form id="form_qualification" action="" method="post" enctype="multipart/form-data">


            <br>


            <div class="form-container">
                <h4 class="form-group-title">Education Qualification Details</h4>

                <div class="row">


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Qualification Level <span class='error-text'>*</span></label>
                            <select name="qualification_level" id="qualification_level" class="form-control">
                                <option value="">Select</option>
                                <option value="diploma"
                                <?php 
                                if('diploma' == $educationQualificationDetails->level)
                                {
                                    echo "selected=selected";
                                } ?>>diploma</option>
                                <option value="degree"
                                <?php 
                                if('degree' == $educationQualificationDetails->level)
                                {
                                    echo "selected=selected";
                                } ?>>degree</option>
                                <option value="master"
                                <?php 
                                if('master' == $educationQualificationDetails->level)
                                {
                                    echo "selected=selected";
                                } ?>>master</option>
                                <option value="PHD"
                                <?php 
                                if('PHD' == $educationQualificationDetails->level)
                                {
                                    echo "selected=selected";
                                } ?>>PHD</option>
                                <option value="professional"
                                <?php 
                                if('professional' == $educationQualificationDetails->level)
                                {
                                    echo "selected=selected";
                                } ?>>professional</option>
                                <option value="certificate"
                                <?php 
                                if('certificate' == $educationQualificationDetails->level)
                                {
                                    echo "selected=selected";
                                } ?>>certificate</option>
                                <option value="certified"
                                <?php 
                                if('certified' == $educationQualificationDetails->level)
                                {
                                    echo "selected=selected";
                                } ?>>certified</option>
                                <option value="chartered"
                                <?php 
                                if('chartered' == $educationQualificationDetails->level)
                                {
                                    echo "selected=selected";
                                } ?>>chartered</option>
                            </select>
                        </div>
                    </div>



                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Qualification Name <span class='error-text'>*</span> </label>
                            <input type="text" class="form-control" id="qualification_name" name="qualification_name" value="<?php echo $educationQualificationDetails->name ?>">
                        </div>
                    </div>



                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Awarding Institute <span class='error-text'>*</span> </label>
                            <input type="text" class="form-control" id="awarding_institute" name="awarding_institute" value="<?php echo $educationQualificationDetails->awarding_institute ?>">
                        </div>
                    </div>


                </div>

                <div class="row">


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Country Of Awarding Institution <span class='error-text'>*</span></label>
                            <select name="country" id="country" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($countryList))
                                {
                                    foreach ($countryList as $record)
                                    {?>
                                 <option value="<?php echo $record->id;  ?>"
                                    <?php 
                                    if($record->id == $educationQualificationDetails->country)
                                    {
                                        echo "selected";
                                    } ?>>
                                    <?php echo $record->name;  ?></option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Year Obtained <span class='error-text'>*</span> </label>
                            <input type="number" class="form-control" id="year_obtained" name="year_obtained" value="<?php echo $educationQualificationDetails->year?>">
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Upload Certificate 
                            <?php
                            if ($educationQualificationDetails->certificate != '')
                            {
                            ?>
                            <a href="<?php echo '/assets/images/' . $educationQualificationDetails->certificate; ?>" target="popup" onclick="window.open(<?php echo '/assets/images/' . $educationQualificationDetails->certificate; ?>)" title="<?php echo $educationQualificationDetails->certificate; ?>"> View </a>
                            <?php
                            }
                            ?>
                             </label>
                            <input type="file" class="form-control" id="certificate" name="certificate" value="<?php echo $educationQualificationDetails->certificate ?>">
                            <input type="hidden" class="form-control" id="btn_submit" name="btn_submit"  value="7">
                        </div>
                    </div>

                </div>

            </div>


            <div class="button-block clearfix">
                <div class="bttn-group pull-right">
                    <button type="button" onclick="saveEducationQualificationDetails()"  class="btn btn-primary btn-lg">Save</button>
                    <?php
                    if($id_qualification != NULL)
                    {
                      ?>
                      <a href="<?php echo '../../qualification/'. $id_staff ?>" class="btn btn-link">Cancel</a>
                      <?php
                    }
                    ?>
                </div>
            </div>


        </form>



        <div class="form-container">
                <h4 class="form-group-title">Education Qualification Details</h4>

            <div class="custom-table">
              <table class="table" id="list-table">
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Qualification Level</th>
                    <th>Qualification Name</th>
                    <th>Awarded Institution</th>
                    <th>Country Of Institution</th>
                    <th>Year Obtained</th>
                    <th>Certificate</th>
                    <th class="text-center">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  if (!empty($staffEducationQualificationList))
                  {
                    $i=1;
                    foreach ($staffEducationQualificationList as $record)
                    {
                  ?>
                      <tr>
                        <td><?php echo $i ?></td>
                        <td><?php echo $record->level ?></td>
                        <td><?php echo $record->name ?></td>
                        <td><?php echo $record->awarding_institute ?></td>
                        <td><?php echo $record->country ?></td>
                        <td><?php echo $record->year ?></td>
                        <td class="text-center">

                        <?php
                        if($record->certificate)
                        {
                            ?>

                            <a href="<?php echo '/assets/images/' . $record->certificate; ?>" target="popup" onclick="window.open(<?php echo '/assets/images/' . $record->certificate; ?>)" title="<?php echo $record->certificate; ?>">View</a>

                        
                            <?php
                        }
                        ?>
                        </td>
                        <td class="text-center">
                            <a href='/af/staff/qualification/<?php echo $id_staff;?>/<?php echo $record->id;?>'>Edit</a> |
                            <a onclick="deleteEducationQualificationDetails(<?php echo $record->id; ?>)">Delete</a>
                        </td>

                      </tr>
                  <?php
                    $i++;
                    }
                  }
                  ?>
                </tbody>
              </table>
            </div>

        </div>


   </div>
</div>
<footer class="footer-wrapper">
   <p>&copy; 2019 All rights, reserved</p>
</footer>

<script type="text/javascript">

    $('select').select2();

  $(function()
  {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
  });

  function saveEducationQualificationDetails()
    {
        if($('#form_qualification').valid())
        {
            $('#form_qualification').submit();    
        }
    }



    function deleteEducationQualificationDetails(id)
    {
      var cnf= confirm('Do you really want to delete?');
      if(cnf==true)
      {

        $.ajax(
            {
               url: '/af/staff/deleteEducationQualificationDetails/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view").html(result);
                    // alert('Deleted Sessessfully');
                    location.reload();
               }
            });
      }
    }


    $(document).ready(function()
    {
        $("#form_qualification").validate(
        {
            rules:
            {
                qualification_level:
                {
                    required: true
                },
                qualification_name:
                {
                    required: true
                },
                awarding_institute:
                {
                    required: true
                },
                country:
                {
                    required: true
                },
                year_obtained:
                {
                    required: true
                }
            },
            messages:
            {
                qualification_level:
                {
                    required: "<p class='error-text'>Select Level </p>",
                },
                qualification_name:
                {
                    required: "<p class='error-text'>Qualification Name Required</p>",
                },
                awarding_institute:
                {
                    required: "<p class='error-text'>Awarding Institute Required</p>",
                },
                country:
                {
                    required: "<p class='error-text'>Select The Country</p>",
                },
                year_obtained:
                {
                    required: "<p class='error-text'>Year Obtained Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

</script>