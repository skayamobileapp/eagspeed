<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit School Details</h3>
        </div>
        <form id="form_school" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Class Details</h4>


            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name *</label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $classDetails->name;?>">
                    </div>
                </div>
            </div>

        </div>

        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="../list" class="btn btn-link">Cancel</a>
            </div>
        </div>

        
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>
</div>

<script>
    $(document).ready(function()
    {
        $("#form_school").validate(
        {
            rules:
            {
                name:
                {
                    required: true
                }
            },
            messages:
            {
                name:
                {
                    required: "Class Name Required",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>