<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Payment_rate_model extends CI_Model
{
    function paymentRateListSearch($data)
    {
        $this->db->select('pr.*, cs.code as currency_code, cs.name as currency_name, aw.code as award_code, aw.name as award_name, cre.name as creater_name, upd.name as updater_name');
        $this->db->from('payment_rate as pr');
        $this->db->join('currency_setup as cs', 'pr.id_currency = cs.id');
        $this->db->join('award as aw', 'pr.id_award = aw.id');
        $this->db->join('users as cre','pr.created_by = cre.id','left');
        $this->db->join('users as upd','pr.updated_by = upd.id','left');
        // if ($data['name'])
        // {
        //     $likeCriteria = "(name  LIKE '%" . $data['name'] . "%')";
        //     $this->db->where($likeCriteria);
        // }
        // if ($data['name'])
        // {
        //     $likeCriteria = "(name  LIKE '%" . $data['name'] . "%')";
        //     $this->db->where($likeCriteria);
        // }
        $this->db->order_by("pr.id", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function getPaymentRate($id)
    {
        $this->db->select('*');
        $this->db->from('payment_rate');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewPaymentRate($data)
    {
        $this->db->trans_start();
        $this->db->insert('payment_rate', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editPaymentRate($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('payment_rate', $data);
        return $this->db->affected_rows();
        // return TRUE;
    }
    
    function deleteActivityDetails($id, $date)
    {
        $this->db->where('id', $id);
        $this->db->update('payment_rate', $date);
        return $this->db->affected_rows();
    }

    function awardLevelListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('award');
        $this->db->where('status', $status);
        $query = $this->db->get();
        return $query->result();
    }


    function currencyListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('currency_setup');
        $this->db->where('status', $status);
        $query = $this->db->get();
        return $query->result();
    }
}