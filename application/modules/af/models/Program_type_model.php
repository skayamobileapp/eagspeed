<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Program_type_model extends CI_Model
{
    function programTypeList()
    {
        $this->db->select('*');
        $this->db->from('program_type');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function programTypeListSearch($search)
    {
        $this->db->select('pt.*, cre.name as creater_name, upd.name as updater_name');
        $this->db->from('program_type as pt');
        $this->db->join('users as cre','pt.created_by = cre.id','left');
        $this->db->join('users as upd','pt.updated_by = upd.id','left');
        if (!empty($search))
        {
            $likeCriteria = "(pt.name  LIKE '%" . $search . "%' or pt.code  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("pt.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getProgramType($id)
    {
        $this->db->select('*');
        $this->db->from('program_type');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewProgramType($data)
    {
        $this->db->trans_start();
        $this->db->insert('program_type', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editProgramType($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('program_type', $data);
        return TRUE;
    }
}

