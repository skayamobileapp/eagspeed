<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Semester extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('semester_model');
        $this->load->model('academic_year_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('semester.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;
            $data['semesterDetails'] = $this->semester_model->semesterListSearch($name);
            $this->global['pageTitle'] = 'Campus Management System : Semester List';
            //print_r($subjectDetails);exit;
            $this->loadViews("semester/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('semester.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $id_user = $this->session->userId;

            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $id_academic_year = $this->security->xss_clean($this->input->post('id_academic_year'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));
                $semester_type = $this->security->xss_clean($this->input->post('semester_type'));
                $semester_sequence = $this->security->xss_clean($this->input->post('semester_sequence'));
                $name_optional_language = $this->security->xss_clean($this->input->post('name_optional_language'));
                $is_countable = $this->security->xss_clean($this->input->post('is_countable'));
                $id_scheme = $this->security->xss_clean($this->input->post('id_scheme'));
                $special_semester = $this->security->xss_clean($this->input->post('special_semester'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'id_academic_year' => $id_academic_year,
                    'start_date' => date('Y-m-d',strtotime($start_date)),
                    'end_date' => date('Y-m-d',strtotime($end_date)),
                    'semester_type' => $semester_type,
                    'semester_sequence' => $semester_sequence,
                    'is_countable' => $is_countable,
                    'id_scheme' => $id_scheme,
                    'special_semester' => $special_semester,
                    'name_optional_language' => $name_optional_language,
                    'status' => $status
                );
                $result = $this->semester_model->addNewSemester($data);
                if($result)
                {
                    $moved = $this->semester_model->moveTempToDetailsTable($result);
                }
                redirect('/setup/semester/list');
            }
            else
            {
                $deleted_temp_details = $this->semester_model->deleteTempSemesterHasSchemeBySession($id_session);
            }
            //print_r($data['stateList']);exit;
            $data['academicYearList'] = $this->semester_model->academicYearListByStatus('1');
            $data['schemeList'] = $this->semester_model->schemeListByStatus('1');
            // $data['semesterDetails'] = $this->semester_model->semesterList();

            
            $this->global['pageTitle'] = 'Campus Management System : Add Semester';
            $this->loadViews("semester/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('semester.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/semester/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $id_academic_year = $this->security->xss_clean($this->input->post('id_academic_year'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));
                $semester_type = $this->security->xss_clean($this->input->post('semester_type'));
                $semester_sequence = $this->security->xss_clean($this->input->post('semester_sequence'));
                $name_optional_language = $this->security->xss_clean($this->input->post('name_optional_language'));
                $is_countable = $this->security->xss_clean($this->input->post('is_countable'));
                $id_scheme = $this->security->xss_clean($this->input->post('id_scheme'));
                $special_semester = $this->security->xss_clean($this->input->post('special_semester'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'id_academic_year' => $id_academic_year,
                    'start_date' => date('Y-m-d',strtotime($start_date)),
                    'end_date' => date('Y-m-d',strtotime($end_date)),
                    'semester_type' => $semester_type,
                    'name_optional_language' => $name_optional_language,
                    'is_countable' => $is_countable,
                    'semester_sequence' => $semester_sequence,
                    'id_scheme' => $id_scheme,
                    'special_semester' => $special_semester,
                    'status' => $status
                );
                //echo "<Pre>"; print_r($data);exit;
                
                $result = $this->semester_model->editSemester($data,$id);
                redirect('/setup/semester/list');
            }
            $data['academicYearList'] = $this->semester_model->academicYearListByStatus('1');
            $data['semesterDetails'] = $this->semester_model->getSemester($id);
            $data['semesterHasScheme'] = $this->semester_model->semesterHasScheme($id);
            $data['schemeList'] = $this->semester_model->schemeListByStatus('1');

            //echo "<Pre>"; print_r($data['semesterDetails']);exit;
            $this->global['pageTitle'] = 'Campus Management System : Edit Semester';
            $this->loadViews("semester/edit", $this->global, $data, NULL);
        }
    }

    function delete()
    {
        if ($this->checkAccess('semester.delete') == 0)
        {
            echo (json_encode(array('status' => 'access')));
        }
        else
        {
            $countryId = $this->input->post('countryId');
            $countryInfo = array('isDeleted' => 1, 'updatedBy' => $this->vendorId, 'updatedDtm' => date('Y-m-d H:i:s'));
            $result = $this->semester_model->deleteSemmester($countryId, $countryInfo);
            if ($result > 0)
            {
                echo (json_encode(array('status' => TRUE)));
            }
            else
            {
                echo (json_encode(array('status' => FALSE)));
            }
        }
    }



    function tempSemesterHasScheme()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $id_session = $this->session->my_session_id;
        $id_user = $this->session->userId;

        $tempData['id_session'] = $id_session;
        $tempData['created_by'] = $id_user;
            // echo "<Pre>"; print_r($tempData);exit;
        $inserted_id = $this->semester_model->tempSemesterHasScheme($tempData);
        $data = $this->displayTempdata();
        print_r($data);exit; 
        // echo "success";exit;
    }

    function deleteTempSemesterHasScheme($id)
    {
        // $id_session = $this->session->my_session_id;
        $inserted_id = $this->semester_model->deleteTempSemesterHasScheme($id);
        $data = $this->displayTempdata();
        print_r($data);exit; 
    }


    function displayTempdata()
    {
        $id_session = $this->session->my_session_id;
        
        $details = $this->semester_model->getTempSemesterHasSchemeBySessionIdDisplay($id_session); 
        if(!empty($details))
        {
            
        // echo "<Pre>";print_r($details);exit;
        $table = "
        <div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Scheme</th>
                    <th>Action</th>
                    </tr>
                </thead>";
                    $total_detail = 0;
                    for($i=0;$i<count($details);$i++)
                    {
                        $id = $details[$i]->id;
                        $scheme_code = $details[$i]->scheme_code;
                        $scheme_name = $details[$i]->scheme_name;

                        $j=$i+1;
                        
                        $table .= "
                        <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$scheme_code - $scheme_name</td>                            
                            <td>
                                <a class='btn btn-sm btn-edit' onclick='deleteTempSemesterHasScheme($id)'>Delete</a>
                            <td>
                        </tr>";

                    }

                     $table .= "
                    </tbody>";
        $table.= "</table>
        </div>";
        }
        else
        {
            $table = "";
        }

        return $table;
    }








    function addSemesterHasScheme()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
            // echo "<Pre>"; print_r($tempData);exit;

        $inserted_id = $this->semester_model->addSemesterHasScheme($tempData);

        echo "success";exit;
    }

    function deleteSemesterHasScheme($id)
    {
        $inserted_id = $this->semester_model->deleteSemesterHasScheme($id);
        echo "Success";exit; 
    }
}
