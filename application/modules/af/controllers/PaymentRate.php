<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class PaymentRate extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('payment_rate_model');
        $this->isLoggedIn();
    }

    function list()
    {
       if ($this->checkAccess('payment_rate.list') == 0)
       {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['awardLevelList'] = $this->payment_rate_model->awardLevelListByStatus('1');

            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            // $formData['learning_mode'] = $this->security->xss_clean($this->input->post('learning_mode'));
            // $formData['teaching_component'] = $this->security->xss_clean($this->input->post('teaching_component'));
            // $formData['id_currency'] = $this->security->xss_clean($this->input->post('id_currency'));
            
            $data['searchParam'] = $formData;

            $data['paymentRateList'] = $this->payment_rate_model->paymentRateListSearch($formData);
            
            // echo "<Pre>"; print_r($data['paymentRateList']);exit;

            $this->global['pageTitle'] = 'Campus Management System : Payment Rate List';
            $this->loadViews("payment_rate/list", $this->global, $data, NULL);
        }
    }

    function add()
    {
        if ($this->checkAccess('payment_rate.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                $id_award = $this->security->xss_clean($this->input->post('id_award'));
                $learning_mode = $this->security->xss_clean($this->input->post('learning_mode'));
                $teaching_component = $this->security->xss_clean($this->input->post('teaching_component'));
                $id_currency = $this->security->xss_clean($this->input->post('id_currency'));
                $calculation_type = $this->security->xss_clean($this->input->post('calculation_type'));
                $amount = $this->security->xss_clean($this->input->post('amount'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $data = array(
                    'id_award' => $id_award,
                    'learning_mode' => $learning_mode,
                    'teaching_component' => $teaching_component,
                    'id_currency' => $id_currency,
                    'calculation_type' => $calculation_type,
                    'amount' => $amount,
                    'status' => $status,
                    'created_by' => $id_user
                );

                $result = $this->payment_rate_model->addNewPaymentRate($data);
                redirect('/af/paymentRate/list');
            }

            $data['awardLevelList'] = $this->payment_rate_model->awardLevelListByStatus('1');
            $data['currencyList'] = $this->payment_rate_model->currencyListByStatus('1');

            $this->global['pageTitle'] = 'Campus Management System : Add New Payment Rate';
            $this->loadViews("payment_rate/add", $this->global, $data, NULL);
        }
    }

    function edit($id)
    {

        if ($this->checkAccess('payment_rate.edit') == 0)
        {
                      
            $this->loadAccessRestricted();
        
        } else {

            if ($id == null)
            {
                redirect('/af/paymentRate/list');
            }
            if($this->input->post())
            {

                
                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                $id_award = $this->security->xss_clean($this->input->post('id_award'));
                $learning_mode = $this->security->xss_clean($this->input->post('learning_mode'));
                $teaching_component = $this->security->xss_clean($this->input->post('teaching_component'));
                $id_currency = $this->security->xss_clean($this->input->post('id_currency'));
                $calculation_type = $this->security->xss_clean($this->input->post('calculation_type'));
                $amount = $this->security->xss_clean($this->input->post('amount'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $data = array(
                    'id_award' => $id_award,
                    'learning_mode' => $learning_mode,
                    'teaching_component' => $teaching_component,
                    'id_currency' => $id_currency,
                    'calculation_type' => $calculation_type,
                    'amount' => $amount,
                    'status' => $status,
                    'updated_by' => $id_user
                );

                $result = $this->payment_rate_model->editPaymentRate($data, $id);
                redirect('/af/paymentRate/list');
            }


            $data['awardLevelList'] = $this->payment_rate_model->awardLevelListByStatus('1');
            $data['currencyList'] = $this->payment_rate_model->currencyListByStatus('1');

            $data['paymentRate'] = $this->payment_rate_model->getPaymentRate($id);

            // echo "<Pre>";print_r($data['payment_rate']);exit;
            $this->global['pageTitle'] = 'Campus Management System : Edit PaymentRate';
            $this->loadViews("payment_rate/edit", $this->global, $data, NULL);
        }
    }
}
