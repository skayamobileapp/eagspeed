<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Qualification extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('qualification_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('qualification.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;
            $data['qualificationList'] = $this->qualification_model->qualificationListSearch($name);
            $this->global['pageTitle'] = 'Scholarship Management System : Qualification List';
            $this->loadViews("qualification/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('qualification.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'status' => $status
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->qualification_model->addNewQualification($data);
                redirect('/setup/qualification/list');
            }
            $this->global['pageTitle'] = 'Scholarship Management System : Add Qualification';
            $this->loadViews("qualification/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('qualification.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/qualification/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'status' => $status
                );

                $result = $this->qualification_model->editQualification($data,$id);
                redirect('/setup/qualification/list');
            }
            $data['qualification'] = $this->qualification_model->getQualification($id);
            $this->global['pageTitle'] = 'Scholarship Management System : Edit Qualification';
            $this->loadViews("qualification/edit", $this->global, $data, NULL);
        }
    }
}
