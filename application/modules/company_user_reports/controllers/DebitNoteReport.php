<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class DebitNoteReport extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('debit_note_report_model');
        $this->isCompanyUserLoggedIn();
    }

    function list()
    {
        $id_company = $this->session->id_company;

        $from_date = $this->security->xss_clean($this->input->post('from_date'));
        $to_date = $this->security->xss_clean($this->input->post('to_date'));
        $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));
        $formData['type'] = $this->security->xss_clean($this->input->post('type'));
        $formData['status'] = $this->security->xss_clean($this->input->post('status'));
        $formData['from_date'] = '';
        $formData['to_date'] = '';
        $formData['id_company'] = $id_company;

        if($from_date != '')
        {
            $formData['from_date'] = date('Y-m-d',strtotime($from_date));
        }
        if($to_date != '')
        {
            $formData['to_date'] = date('Y-m-d',strtotime($to_date));
        }
        
        $data['searchParam'] = $formData;


        if($this->input->post())
        {
            $formSubmit = $this->input->post();

            $btn_submit = $formSubmit['btn_submit'];

            if($btn_submit == 'download')
            {
                $this->downloadDebitNoteReportCSV($formData);
            }

            $data['debitNoteReportList'] = $this->debit_note_report_model->debitNoteReportListSearch($formData);
        }
        else
        {
            $data['debitNoteReportList'] = array();
        }

        $data['programmeList'] = $this->debit_note_report_model->programmeListByStatus('1');

        $this->global['pageTitle'] = 'Campus Management System : Corporate Company List';
        $this->loadViews("debit_note_report/list", $this->global, $data, NULL);
    }

    function view($id = NULL)
    {
        if ($this->checkAccess('debit_note.view') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/company_user_reports/debitNoteReport/list');
            }
            if($this->input->post())
            {
                redirect('/company_user_reports/debitNoteReport/list');
            }

            $data['debitNote'] = $this->debit_note_report_model->getDebitNote($id);
            $data['invoiceDetails'] = $this->debit_note_report_model->getInvoice($data['debitNote']->id_invoice);

            if($data['debitNote']->type == 'CORPORATE')
            {
                $data['companyDetails'] = $this->debit_note_report_model->getCompanyDetails($data['debitNote']->id_student);
            }
            elseif($data['debitNote']->type == 'Student')
            {
                $data['studentDetails'] = $this->debit_note_report_model->getStudentData($data['debitNote']->id_student);
            }

            $data['programmeList'] = $this->debit_note_report_model->programmeListByStatus('1');
            // $data['studentList'] = $this->debit_note_report_model->studentList();
            $data['debitNoteTypeList'] = $this->debit_note_report_model->debitNoteTypeListByStatus('1');


            $this->global['pageTitle'] = 'Campus Management System : View Debit Note Details';
            $this->loadViews("debit_note_report/view", $this->global, $data, NULL);
        }
    }

    function downloadDebitNoteReportCSV($tempData)
    {
        $fileData = $this->debit_note_report_model->downloadDebitNoteInvoiceReportCSVBtwnDates($tempData);
        // echo "<Pre>";print_r($fileData);exit;
        $fileName = gmdate("YmdHis");
        $this->generateCSVDebitNoteReport($fileData, $fileName, $tempData);
    }

    public function generateCSVDebitNoteReport($fileData, $fileName, $data)
    {
        $fromDate = $data['from_date'];
        $toDate = $data['to_date'];
        $pay_date   = date("d/m/Y");

        $file_name = $fileName . "_DebitNoteReport.csv";

        $fp = fopen('php://output', 'w');

        $header = array();

        array_push($header, "Debit Note Report");

        header("Content-type: application/csv");
        header("Content-Disposition: attachment; filename=" . $file_name);
        header("Pragma: no-cache");
        header("Expires: 0");

        fputcsv($fp, $header);

        $empty_header = array();

        array_push($empty_header, " ");
        fputcsv($fp, $empty_header);


        $dtheader = array();

        array_push($dtheader, "No");
        array_push($dtheader, "Participant Name");
        array_push($dtheader, "IC / Passport No.");
        array_push($dtheader, "COURSE Name");
        array_push($dtheader, "Debit Note Amount (RM / USD)");
        array_push($dtheader, "Debit Note Date");
        array_push($dtheader, "DESCRIPTION");

        fputcsv($fp, $dtheader);

        $i=1;
        foreach ($fileData as $data)
        {
            $reference_number = $data->reference_number;
            $date_time = date('d-m-Y', strtotime($data->created_dt_tm));
            $type = $data->type;
            $id_student = $data->id_student;
            $amount = $data->amount;
            $total_amount = $data->total_amount;
            $description = $data->description;
            $programme_code = $data->programme_code;
            $programme_name = $data->programme_name;

            $programme = $programme_code  . " - " . $programme_name;
            $total_currency = $amount . " ( MYR ) ";

            $student_name = '';
            $student_nric = '';

            if($type == 'CORPORATE')
            {
                $company = $this->debit_note_report_model->getCompanyDetails($id_student);
                if($company)
                {
                    $student_name = $company->name; 
                    $student_nric = $company->registration_number; 
                }
            }
            elseif($type == 'Student')
            {
                $student = $this->debit_note_report_model->getStudentData($id_student);
                if($student)
                {
                    $student_name = $student->full_name; 
                    $student_nric = $student->nric; 
                }
            }

            $data_array = array();

            array_push($data_array, $i);
            array_push($data_array, $student_name);
            array_push($data_array, $student_nric);
            array_push($data_array, $programme);
            array_push($data_array, $total_currency);
            array_push($data_array, $date_time);
            array_push($data_array, $description);

            fputcsv($fp, $data_array);
            $i++;
        }

        fclose($fp);
        exit;
    }
}