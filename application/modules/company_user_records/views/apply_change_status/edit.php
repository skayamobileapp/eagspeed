<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>View Apply Change Status</h3> 
            <a href="../list" class="btn btn-link btn-back">‹ Back</a>
        </div>
        <form id="form_apply_change_status" action="" method="post">


             <h4 class='sub-title'>Student Profile</h4>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student Name :</dt>
                                <dd><?php echo ucwords($studentDetails->full_name); ?></dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd><?php echo $studentDetails->nric ?></dd>
                            </dl>
                            <dl>
                                <dt>Student Email :</dt>
                                <dd><?php echo $studentDetails->email_id; ?></dd>
                            </dl>
                            <dl>
                                <dt>Mailing Address :</dt>
                                <dd></dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd><?php echo $studentDetails->mail_address1 ; ?></dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd><?php echo $studentDetails->mail_address2; ?></dd>
                            </dl>
                            <dl>
                                <dt>Mailing City :</dt>
                                <dd><?php echo $studentDetails->mailing_city; ?></dd>
                            </dl>
                            <dl>
                                <dt>Mailing Zipcode :</dt>
                                <dd><?php echo $studentDetails->mailing_zipcode; ?></dd>
                            </dl>
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Nationality :</dt>
                                <dd><?php echo $studentDetails->nationality ?></dd>
                            </dl>
                            <dl>
                                <dt>Programme :</dt>
                                <dd><?php echo $studentDetails->programme_name ?></dd>
                            </dl>
                            <dl>
                                <dt>Permanent Address :</dt>
                                <dd></dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd><?php echo $studentDetails->permanent_address1; ?></dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd><?php echo $studentDetails->permanent_address2; ?></dd>
                            </dl>
                            <dl>
                                <dt>Permanent City :</dt>
                                <dd><?php echo $studentDetails->permanent_city; ?></dd>
                            </dl>
                            <dl>
                                <dt>Permanent Zipcode :</dt>
                                <dd><?php echo $studentDetails->permanent_zipcode; ?></dd>
                            </dl>
                        </div>
    
                    </div>
                </div>

            <div class="form-container">
                <h4 class="form-group-title">Apply Change Status Details</h4>
                <div class="row">

                    <!-- <div class="col-sm-4">
                        <div class="form-group">
                            <label>Select Semester <span class='error-text'>*</span></label>
                            <select name="id_semester" disabled="disabled" id="id_semester" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($semesterList))
                                {
                                    foreach ($semesterList as $record)
                                    {?>
                                <option value="<?php echo $record->id;  ?>"
                                    <?php 
                                    if($record->id == $applyChangeStatus->id_semester)
                                    {
                                        echo "selected=selected";
                                    } ?>>
                                    <?php echo $record->code . " - " . $record->name;  ?>
                                    </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div> -->
        

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Select Change Status <span class='error-text'>*</span></label>
                            <select name="id_change_status" disabled="disabled" id="id_change_status" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($changeStatusList))
                                {
                                    foreach ($changeStatusList as $record)
                                    {?>
                                <option value="<?php echo $record->id;  ?>"
                                    <?php 
                                    if($record->id == $applyChangeStatus->id_change_status)
                                    {
                                        echo "selected=selected";
                                    } ?>>
                                    <?php echo $record->code . " - " . $record->name;  ?>
                                    </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>   

                


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Reason <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="reason" name="reason" value="<?php echo $applyChangeStatus->reason; ?>" readonly>
                        </div>
                    </div>    

                </div>


                <div class="row">


                    <!--  <div class="col-sm-4">
                        <div class="form-group">
                            <label>Select Student <span class='error-text'>*</span></label>
                            <select name="id_student" id="id_student" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($studentList))
                                {
                                    foreach ($studentList as $record)
                                    {?>
                                <option value="<?php echo $record->id;  ?>"
                                    <?php 
                                    if($record->id == $applyChangeStatus->id_student)
                                    {
                                        echo "selected=selected";
                                    } ?>>
                                    <?php echo $record->nric . " - " . $record->name;  ?>
                                    </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>  -->

                    

                    <!-- <div class="col-sm-4">
                            <div class="form-group">
                                <p>Status <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                <input type="radio" name="status" id="status" value="1" <?php if($applyChangeStatus->status=='1') {
                                    echo "checked=checked";
                                };?>><span class="check-radio"></span> Active
                                </label>
                                <label class="radio-inline">
                                <input type="radio" name="status" id="status" value="0" <?php if($applyChangeStatus->status=='0') {
                                    echo "checked=checked";
                                };?>>
                                <span class="check-radio"></span> In-Active
                                </label>                              
                            </div>                         
                    </div> -->
                </div>
            </div>

        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>
</div>

<script>
    $(document).ready(function()
    {
        $("#form_apply_change_status").validate(
        {
            rules:
            {
                id_student:
                {
                    required: true
                },
                id_change_status:
                {
                    required: true
                },
                id_semester:
                {
                    required: true
                },
                reason:
                {
                    required: true
                }
            },
            messages:
            {
                id_student:
                {
                    required: "<p class='error-text'>Select Student</p>",
                },
                id_change_status:
                {
                    required: "<p class='error-text'>Select Change Status</p>",
                },
                id_semester:
                {
                    required: "<p class='error-text'>Select Semester</p>",
                },
                reason:
                {
                    required: "<p class='error-text'>Reason Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script type="text/javascript">
    $('select').select2();
</script>