<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Visa_details_model extends CI_Model
{


    function intakeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('intake');
        $this->db->where('status', $status);
        $query = $this->db->get();
        $result = $query->result();
        // echo "<pre>";print_r($query);die;
        return $result;
    }

    function programListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', $status);
        $query = $this->db->get();
        $result = $query->result();
        // echo "<pre>";print_r($query);die;
        return $result;
    }
    
   

    function getStudentByStudentId($id_student)
    {
        $this->db->select('s.*, p.code as program_code, p.name as program_name, i.name as intake_name, qs.name as qualification_name, qs.code as qualification_code');
        $this->db->from('student as s');
        $this->db->join('programme as p', 's.id_program = p.id'); 
        $this->db->join('intake as i', 's.id_intake = i.id'); 
        $this->db->join('qualification_setup as qs', 's.id_degree_type = qs.id');
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getStudentByStudentIdForPassport($id_student)
    {
        $this->db->select('s.*');
        $this->db->from('student as s');
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function studentListSearch($applicantList)
    {
        $this->db->select('a.*, i.name as intake, p.code as program_code, p.name as program');
        $this->db->from('student as a');
        $this->db->join('intake as i', 'a.id_intake = i.id');
        $this->db->join('programme as p', 'a.id_program = p.id');
        if($applicantList['first_name']) {
            $likeCriteria = "(a.full_name  LIKE '%" . $applicantList['first_name'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($applicantList['email_id']) {
            $likeCriteria = "(a.email_id  LIKE '%" . $applicantList['email_id'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($applicantList['nric']) {
            $likeCriteria = "(a.nric  LIKE '%" . $applicantList['nric'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($applicantList['id_program']) {
            $likeCriteria = "(a.id_program  LIKE '%" . $applicantList['id_program'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($applicantList['id_intake']) {
            $likeCriteria = "(a.id_intake  LIKE '%" . $applicantList['id_intake'] . "%')";
            $this->db->where($likeCriteria);
        }
        if($applicantList['applicant_status']) {
            $likeCriteria = "(a.applicant_status  LIKE '%" . $applicantList['applicant_status'] . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('a.applicant_status !=', 'Graduated');
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }















     function visaDetailsList()
    {
        $this->db->select('b.*, stu.first_name as student, sem.name as semester, bt.name as change_status');
        $this->db->from('apply_change_status as b');
        $this->db->join('student as stu', 'b.id_student = stu.id');
        $this->db->join('semester as sem', 'b.id_semester = sem.id');
        $this->db->join('change_status as bt', 'b.id_change_status = bt.id');
        $this->db->order_by("b.id", "DESC");
        $query = $this->db->get();
        $result = $query->result(); 

        return $result;
    }

    function getApplyChangeStatus($id)
    {
        $this->db->select('*');
        $this->db->from('apply_change_status');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $result = $query->row();
        // echo "<pre>";print_r($query);die;
        return $result;
    }

    

    function addNewApplyChangeStatus($data)
    {
        $this->db->trans_start();
        $this->db->insert('apply_change_status', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function editApplyChangeStatus($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('apply_change_status', $data);

        return TRUE;
    }


       function updateData($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('apply_change_status', $data);

        return TRUE;
    }

    function deleteApplyChangeStatus($id, $stateInfo)
    {
        $this->db->where('id', $id);
        $this->db->update('state', $stateInfo);

        return $this->db->affected_rows();
    }

    function studentList()
    {
        $this->db->select('*, full_name as name');
        // $this->db->from('student');
        $this->db->from('student');
        $this->db->order_by("first_name", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function visaDetailsListForApprovalSearch($formData)
    {
        $this->db->select('b.*, stu.full_name as student, stu.nric, sem.name as semester, bt.name as change_status, p.name as programme_name, p.code as programme_code, i.name as intake_name');
        $this->db->from('apply_change_status as b');
        $this->db->join('student as stu', 'b.id_student = stu.id');
        $this->db->join('semester as sem', 'b.id_semester = sem.id');
        $this->db->join('change_status as bt', 'b.id_change_status = bt.id');
        $this->db->join('programme as p', 'stu.id_program = p.id');
        $this->db->join('intake as i', 'stu.id_intake = i.id');
        if (!empty($formData['name']))
        {
            $likeCriteria = "(b.reason  LIKE '%" . $formData['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if (!empty($formData['id_semester']))
        {
            $likeCriteria = "(b.id_semester  LIKE '%" . $formData['id_semester'] . "%')";
            $this->db->where($likeCriteria);
        }
        if (!empty($formData['id_student']))
        {
            $likeCriteria = "(b.id_student  LIKE '%" . $formData['id_student'] . "%')";
            $this->db->where($likeCriteria);
        }
        if (!empty($formData['id_change_status']))
        {
            $likeCriteria = "(b.id_change_status  LIKE '%" . $formData['id_change_status'] . "%')";
            $this->db->where($likeCriteria);
        }
        $where_pending = "(b.status  = '0')";
        $this->db->where($where_pending);
        $this->db->order_by("b.id", "DESC");
        $query = $this->db->get();
        $result = $query->result(); 

        return $result;
    }

    function semesterList()
    {
        $this->db->select('s.*, ay.name as academic_year');
        $this->db->from('semester as s');
        $this->db->join('academic_year as ay', 'ay.id = s.id_academic_year');
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function programmeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', $status);
        $this->db->order_by("id", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function getStudentByProgrammeId($id_programme)
    {
        $this->db->select('*');
        $this->db->from('student');
        $this->db->where('id_program', $id_programme);
        $this->db->order_by("full_name", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function getProgrammeById($id_programme)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('id', $id_programme);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getVisaDetails($id_student)
    {
        $this->db->select('*');
        $this->db->from('visa_details');
        $this->db->where('id_student', $id_student);
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function countryListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('country');
        $this->db->where('status', $status);
        $this->db->order_by("id", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function addVisaDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('student_visa_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function addInsuranceDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('student_insurance_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
        
    }

    function addPassportDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('student_passport_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }


    function getStudentVisaDetails($id_student)
    {
        $this->db->select('svd.*');
        $this->db->from('student_visa_details as svd');
        $this->db->where('svd.id_student', $id_student);
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }



    function getStudentInsuranceDetails($id_student)
    {
        $this->db->select('sid.*, vd.visa_number, vd.visa_expiry_date');
        $this->db->from('student_insurance_details as sid');
        $this->db->join('visa_details as vd', 'sid.insurance_id_visa = vd.id');
        $this->db->where('sid.id_student', $id_student);
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }



    function getStudentPassportDetails($id_student)
    {
        $this->db->select('spd.*, c.name as country');
        $this->db->from('student_passport_details spd');
        $this->db->join('country as c', 'spd.passport_country_of_issue = c.id');        
        $this->db->where('spd.id_student', $id_student);
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }


    function deleteVisaDetails($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('student_visa_details');

        return TRUE;
    }

    function deleteInsuranceDetails($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('student_insurance_details');

        return TRUE;
    }

    function deletePassportDetails($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('student_passport_details');

        return TRUE;
    }

}