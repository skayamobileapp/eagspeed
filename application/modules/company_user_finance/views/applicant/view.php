<div class="container-fluid page-wrapper">


    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>View Employee</h3>

        </div>
               
            



            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>


            <div class="clearfix">

                <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                    <li role="presentation" class="active" ><a href="#program_detail" class="nav-link border rounded text-center"
                            aria-controls="program_detail" aria-selected="true"
                            role="tab" data-toggle="tab">Profile Details</a>
                    </li>

                    <li role="presentation"><a href="#program_scheme" class="nav-link border rounded text-center"
                            aria-controls="program_scheme" role="tab" data-toggle="tab">Company Information</a>
                    </li>
                    
                </ul>


            





            
                
                <div class="tab-content offers-tab-content">



                    <div role="tabpanel" class="tab-pane active" id="program_detail">
                        <div class="col-12 mt-4">






                        <div class="form-container">
                            <h4 class="form-group-title">Profile Details</h4>      


                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Salutation <span class='error-text'>*</span></label>
                                        <select name="salutation" id="salutation" class="form-control" disabled>
                                            <?php
                                            if (!empty($salutationList)) {
                                                foreach ($salutationList as $record) {
                                            ?>
                                                    <option value="<?php echo $record->id;  ?>"
                                                        <?php if($getApplicantDetails->salutation==$record->id)
                                                        {
                                                            echo "selected=selected";
                                                        }
                                                        ?>
                                                        >
                                                        <?php echo $record->name;  ?>        
                                                    </option>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>First Name <span class='error-text'>*</span></label>
                                        <input type="text" class="form-control" id="first_name" name="first_name" value="<?php echo $getApplicantDetails->first_name ?>" readonly>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Last Name <span class='error-text'>*</span></label>
                                        <input type="text" class="form-control" id="last_name" name="last_name" value="<?php echo $getApplicantDetails->last_name ?>" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="row">

                                <div class="col-sm-4">
                                   <div class="form-group">
                                      <label>Type Of Nationality <span class='error-text'>*</span></label>
                                      <select name="nationality" id="nationality" class="form-control" disabled="true">
                                         <option value="">Select</option>
                                         <?php
                                            if(!empty($nationalityList))
                                            {
                                              foreach ($nationalityList as $record)
                                              {
                                            ?>
                                         <option value="<?php echo $record->id;  ?>"
                                            <?php if($getApplicantDetails->nationality==$record->id)
                                               {
                                                   echo "selected=selected";
                                               }
                                               ?>
                                            >
                                            <?php echo $record->name;  ?>        
                                         </option>
                                          <?php
                                              }
                                            }
                                            ?>
                                      </select>
                                   </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>NRIC <span class='error-text'>*</span></label>
                                        <input type="text" class="form-control" id="nric" name="nric" value="<?php echo $getApplicantDetails->nric ?>" readonly>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Date Of Birth <span class='error-text'>*</span></label>
                                        <input type="text" class="form-control datepicker" id="date_of_birth" name="date_of_birth" autocomplete="off" value="<?php echo $getApplicantDetails->date_of_birth ?>" readonly>
                                    </div>
                                </div>

                            </div>


                            <div class="row">


                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Email <span class='error-text'>*</span></label>
                                        <input type="text" class="form-control" id="email_id" name="email_id" value="<?php echo $getApplicantDetails->email_id ?>" readonly>
                                    </div>
                                </div>


                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Password <span class='error-text'>*</span></label>
                                        <input type="password" class="form-control" id="password" name="password" value="<?php echo $getApplicantDetails->password ?>" readonly>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Phone Number <span class='error-text'>*</span></label>
                                        <input type="text" class="form-control" id="phone" name="phone" value="<?php echo $getApplicantDetails->phone ?>" readonly>
                                    </div>
                                </div>


                            </div>


                            <div class="row">
                                
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Gender <span class='error-text'>*</span></label>
                                        <select class="form-control" id="gender" name="gender" disabled>
                                            <option value="">SELECT</option>
                                            <option value="Male" <?php if($getApplicantDetails->gender=='Male'){ echo "selected"; } ?>>MALE</option>
                                            <option value="Female" <?php if($getApplicantDetails->gender=='Female'){ echo "selected"; } ?>>FEMALE</option>
                                            <!-- <option value="Others"<?php if($getApplicantDetails->gender=='Others'){ echo "selected"; } ?> >OTHERS</option> -->
                                        </select>
                                    </div>
                                </div>


                                
                            </div>


                        </div>



                        <div class="form-container">
                            <h4 class="form-group-title">Application Details</h4>
                            <div class="row">  

                             <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Applicant Status <span class='error-text'>*</span></label>
                                            <input type="text" id="user_name" name="user_name" class="form-control" value="<?php echo $getApplicantDetails->applicant_status ?>" readonly="readonly">
                                        </div>
                                </div>

                            </div>

                                  
                        </div>  


                        <div class="button-block clearfix">
                            <div class="bttn-group">
                                <a href="../list" class="btn btn-link">Back</a>
                            </div>
                        </div>

                            

                         



                        </div> 
                    
                    </div>




                    <div role="tabpanel" class="tab-pane" id="program_scheme">
                        <div class="mt-4">


                            

                            
                        <form id="form_programme" action="" method="post" enctype="multipart/form-data">
                            <div class="form-container">
                                <h4 class="form-group-title">Company Details</h4>
                                    

                                    <div class="row">

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Company Level <span class='error-text'>*</span></label>
                                                <select name="level" id="level" class="form-control" onchange="showParentCompany()" disabled>
                                                    <option value="">Select</option>
                                                    <option value="Parent"
                                                    <?php
                                                    if($companyDetails->level == 'Parent')
                                                    {
                                                        echo 'selected';
                                                    }
                                                    ?>
                                                    >Parent</option>
                                                    <option value="Branch"
                                                    <?php
                                                    if($companyDetails->level == 'Branch')
                                                    {
                                                        echo 'selected';
                                                    }
                                                    ?>>Branch</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-sm-4" id='partnerdropdown' style="display: none;">
                                            <div class="form-group">
                                                <label>Parent Company <span class='error-text'>*</span></label>
                                                <select name="id_parent_company" id="id_parent_company" class="form-control">
                                                    <option value="">Select</option>
                                                    <?php
                                                    if (!empty($parentCompanyList))
                                                    {
                                                        foreach ($parentCompanyList as $record)
                                                        {
                                                            if ($record->id != $companyDetails->id)
                                                            {
                                                            ?>
                                                            <option value="<?php echo $record->id;  ?>"
                                                            <?php
                                                            if($companyDetails->id_parent_company == $record->id)
                                                            {
                                                                echo 'selected';
                                                            }
                                                            ?>
                                                            >
                                                            <?php echo $record->registration_number . " - " . $record->name;  ?>
                                                            </option>
                                                    <?php
                                                            }
                                                        }
                                                    }
                                                    ?>

                                                </select>
                                            </div>
                                        </div>

                                        
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Name <span class='error-text'>*</span></label>
                                                <input type="text" class="form-control" id="name" name="name" value="<?php echo $companyDetails->name; ?>">
                                            </div>
                                        </div>

                                        
                                    </div>

                                    <div class="row">

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Registration Number <span class='error-text'>*</span></label>
                                                <input type="text" class="form-control" id="registration_number" name="registration_number" value="<?php echo $companyDetails->registration_number; ?>">
                                            </div>
                                        </div>


                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Joined Date <span class='error-text'>*</span></label>
                                                <input type="text" class="form-control datepicker" id="joined_date" name="joined_date" value="<?php echo $companyDetails->joined_date; ?>">
                                            </div>
                                        </div>



                                    </div>

                                    <div class="row">



                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Website <span class='error-text'>*</span></label>
                                                <input type="text" class="form-control" id="website" name="website" value="<?php echo $companyDetails->website; ?>">
                                            </div>
                                        </div>




                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Chairman <span class='error-text'>*</span></label>
                                                <input type="text" class="form-control" id="chairman" name="chairman" value="<?php echo $companyDetails->chairman; ?>">
                                            </div>
                                        </div>



                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>CEO <span class='error-text'>*</span></label>
                                                <input type="text" class="form-control" id="ceo" name="ceo" value="<?php echo $companyDetails->ceo; ?>">
                                            </div>
                                        </div>



                                    </div>

                                    <div class="row">


                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Address 1 <span class='error-text'>*</span></label>
                                                <input type="text" class="form-control" id="address1" name="address1" value="<?php echo $companyDetails->address1; ?>">
                                            </div>
                                        </div>


                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Address 2 <span class='error-text'>*</span></label>
                                                <input type="text" class="form-control" id="address2" name="address2" value="<?php echo $companyDetails->address2; ?>">
                                            </div>
                                        </div>



                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Select Country <span class='error-text'>*</span></label>
                                                <select name="id_country" id="id_country" class="form-control" onchange="getStateByCountry(this.value)">
                                                    <option value="">Select</option>
                                                    <?php
                                                    if (!empty($countryList))
                                                    {
                                                        foreach ($countryList as $record)
                                                        {?>
                                                            <option value="<?php echo $record->id;?>"
                                                            <?php
                                                            if($companyDetails->id_country == $record->id)
                                                            {
                                                                echo 'selected';
                                                            }
                                                            ?>
                                                            ><?php echo $record->name;?>
                                                            </option>
                                                    <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>



                                    </div>

                                    <div class="row">


                                        <div class="col-sm-4">
                                           <div class="form-group">
                                              <label>Select State <span class='error-text'>*</span></label>
                                              <span id='view_state'>
                                                   <select name="id_state" id="id_state" class="form-control">
                                                    <option value=''>Select</option>
                                                   </select>
                                              </span>
                                           </div>
                                        </div>


                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>City <span class='error-text'>*</span></label>
                                                <input type="text" class="form-control" id="city" name="city" value="<?php echo $companyDetails->city; ?>">
                                            </div>
                                        </div>


                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Postal Code <span class='error-text'>*</span></label>
                                                <input type="number" class="form-control" id="zipcode" name="zipcode" value="<?php echo $companyDetails->zipcode; ?>">
                                            </div>
                                        </div>


                                    </div>

                                    <div class="row">



                                        <div class="col-sm-4">
                                            <label>Telephone Number <span class='error-text'>*</span></label>
                                              <div class="row">
                                                 <div class="col-sm-4">

                                                   <select name="country_code" id="country_code" class="form-control" required>
                                                    <option value="">Select</option>                    
                                                    <?php
                                                        if (!empty($countryList))
                                                        {
                                                          foreach ($countryList as $record)
                                                          {
                                                        ?>
                                                     <option value="<?php echo $record->phone_code;  ?>"
                                                        <?php
                                                        if($companyDetails->country_code == $record->phone_code)
                                                        {
                                                            echo 'selected';
                                                        }
                                                        ?>
                                                        >
                                                        <?php echo $record->phone_code . "  " . $record->name;  ?>
                                                     </option>
                                                       <?php
                                                        }
                                                      }
                                                    ?>
                                                  </select>
                                            </div>
                                            <div class="col-sm-8">
                                              <input type="number" class="form-control" id="phone" name="phone" value="<?php echo $companyDetails->phone; ?>" required>
                                            </div>
                                            </div>

                                        </div>



                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Staff Strength <span class='error-text'>*</span></label>
                                                <input type="number" class="form-control" id="staff_strength" name="staff_strength" value="<?php echo $companyDetails->staff_strength; ?>">
                                            </div>
                                        </div>



                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Corporate Membership Status <span class='error-text'>*</span></label>
                                                <select name="corporate_membership_status" id="corporate_membership_status" class="form-control">
                                                    <option value="">Select</option>
                                                    <option value="Active"
                                                    <?php
                                                    if($companyDetails->corporate_membership_status == 'Active')
                                                    {
                                                        echo 'selected';
                                                    }
                                                    ?>>Active</option>
                                                    <option value="In-Active"
                                                    <?php
                                                    if($companyDetails->corporate_membership_status == 'In-Active')
                                                    {
                                                        echo 'selected';
                                                    }
                                                    ?>>In-Active</option>
                                                    </select>
                                            </div>
                                        </div>


                                        

                                    </div>

                                    <div class="row">


                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Debtor Code <span class='error-text'>*</span></label>
                                                <input type="number" class="form-control" id="debtor_code" name="debtor_code" value="<?php echo $companyDetails->debtor_code; ?>">
                                            </div>
                                        </div>


                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Credit Terms(DAYS) <span class='error-text'>*</span></label>
                                                <input type="number" class="form-control" id="credit_term" name="credit_term" value="<?php echo $companyDetails->credit_term; ?>">
                                            </div>
                                        </div>


                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Control Account <span class='error-text'>*</span></label>
                                                <input type="text" class="form-control" id="control_account" name="control_account" value="<?php echo $companyDetails->control_account; ?>">
                                            </div>
                                        </div>


                                    </div>

                                    <div class="row">

                                        

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Credit Term Code <span class='error-text'>*</span></label>
                                                <input type="text" class="form-control" id="credit_term_code" name="credit_term_code" value="<?php echo $companyDetails->credit_term_code; ?>">
                                            </div>
                                        </div>                


                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <p>Status <span class='error-text'>*</span></p>
                                                <label class="radio-inline">
                                                  <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                                                </label>
                                                <label class="radio-inline">
                                                  <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                                                </label>
                                            </div>
                                        </div>


                                    </div>


                                </div>

                                <br>



                                <div class="form-container">
                                 <h4 class="form-group-title">Primary Contact Details</h4>

                                 <div class="row">
                                 
                                    <div class="col-sm-4">
                                       <div class="form-group">
                                          <label>Name <span class='error-text'>*</span></label>
                                          <input type="text" class="form-control" id="primary_contact_name" name="primary_contact_name" value="<?php echo $companyDetails->primary_contact_name; ?>">
                                       </div>
                                    </div>

                                    <div class="col-sm-4">
                                       <div class="form-group">
                                          <label>Designation </label>
                                          <input type="text" class="form-control" id="primary_contact_designation" name="primary_contact_designation" value="<?php echo $companyDetails->primary_contact_designation; ?>">
                                       </div>
                                    </div>


                                    <div class="col-sm-4">
                                       <div class="form-group">
                                          <label>Email </label>
                                          <input type="text" class="form-control" id="primary_contact_email" name="primary_contact_email" value="<?php echo $companyDetails->primary_contact_email; ?>">
                                       </div>
                                    </div>
                                    
                                    
                                 
                                 </div>

                                 <div class="row">
                                 
                                    <div class="col-sm-4">
                                        <label>Telephone Number <span class='error-text'>*</span></label>
                                          <div class="row">
                                             <div class="col-sm-4">

                                               <select name="primary_country_code" id="primary_country_code" class="form-control" required>
                                                <option value="">Select</option>                    
                                                <?php
                                                    if (!empty($countryList))
                                                    {
                                                      foreach ($countryList as $record)
                                                      {
                                                    ?>
                                                 <option value="<?php echo $record->phone_code;  ?>"
                                                    <?php
                                                    if($companyDetails->primary_country_code == $record->phone_code)
                                                    {
                                                        echo 'selected';
                                                    }
                                                    ?>
                                                    >
                                                    <?php echo $record->phone_code . "  " . $record->name;  ?>
                                                 </option>
                                                   <?php
                                                    }
                                                  }
                                                ?>
                                              </select>
                                        </div>
                                        <div class="col-sm-8">
                                          <input type="number" class="form-control" id="primary_phone" name="primary_phone" value="<?php echo $companyDetails->primary_phone; ?>" required>
                                        </div>
                                        </div>

                                    </div>


                                 </div>
                              
                              </div>




                              <div class="form-container">
                                 <h4 class="form-group-title">Secondary Contact Details</h4>

                                 <div class="row">
                                 
                                    <div class="col-sm-4">
                                       <div class="form-group">
                                          <label>Name <span class='error-text'>*</span></label>
                                          <input type="text" class="form-control" id="second_contact_name" name="second_contact_name" value="<?php echo $companyDetails->second_contact_name; ?>">
                                       </div>
                                    </div>

                                    <div class="col-sm-4">
                                       <div class="form-group">
                                          <label>Designation </label>
                                          <input type="text" class="form-control" id="second_contact_designation" name="second_contact_designation" value="<?php echo $companyDetails->second_contact_designation; ?>">
                                       </div>
                                    </div>


                                    <div class="col-sm-4">
                                       <div class="form-group">
                                          <label>Email </label>
                                          <input type="text" class="form-control" id="second_contact_email" name="second_contact_email" value="<?php echo $companyDetails->second_contact_email; ?>">
                                       </div>
                                    </div>
                                    
                                    
                                 
                                 </div>

                                 <div class="row">
                                 
                                    <div class="col-sm-4">
                                        <label>Telephone Number <span class='error-text'>*</span></label>
                                          <div class="row">
                                             <div class="col-sm-4">

                                               <select name="second_country_code" id="second_country_code" class="form-control" required>
                                                <option value="">Select</option>                    
                                                <?php
                                                    if (!empty($countryList))
                                                    {
                                                      foreach ($countryList as $record)
                                                      {
                                                    ?>
                                                 <option value="<?php echo $record->phone_code;  ?>"
                                                    <?php
                                                    if($companyDetails->second_country_code == $record->phone_code)
                                                    {
                                                        echo 'selected';
                                                    }
                                                    ?>
                                                    >
                                                    <?php echo $record->phone_code . "  " . $record->name;  ?>
                                                 </option>
                                                   <?php
                                                    }
                                                  }
                                                ?>
                                              </select>
                                        </div>
                                        <div class="col-sm-8">
                                          <input type="number" class="form-control" id="second_phone" name="second_phone" value="<?php echo $companyDetails->second_phone; ?>" required>
                                        </div>
                                        </div>

                                    </div>


                                 </div>
                              
                              </div>





                            <div class="button-block clearfix">
                                <div class="bttn-group">
                                    <a href="../list" class="btn btn-link">Back</a>
                                </div>
                            </div>


                        </form>

                            





                        </div>
                    
                    </div>






                </div>
                
            </div>




        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>


    </div>
</div>

<script>

    function showParentCompany()
    {
        var value = $("#level").val();
        if(value=='Parent')
        {
            $("#partnerdropdown").hide();
        }
        else if(value=='Branch')
        {
            $("#partnerdropdown").show();
        }
    }


    $(document).ready(function()
    {

        var value = "<?php echo $companyDetails->level ?>";
        // alert(value);
        if(value == 'Branch')
        {
            $("#partnerdropdown").show();
        }


        var id_country = "<?php echo $companyDetails->id_country ?>";

        // alert(id_country);
        if(id_country != '')
        {
            $.get("/company_user/applicant/getStateByCountry/"+id_country, function(data, status)
            {
                var id_state = "<?php echo $companyDetails->id_state ?>";
                
                // alert(id_state);
                $("#view_state").html(data);
                $("#id_state").find('option[value="'+id_state+'"]').attr('selected',true);
                $('select').select2();
            });
        }


        $("#form_applicant").validate({
            rules: {
                sibbling_status: {
                    required: true
                },
                reason: {
                    required: true
                }
            },
            messages: {
                sibbling_status: {
                    required: "<p class='error-text'>Status Required</p>",
                },
                reason: {
                    required: "<p class='error-text'>Reason Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    function submitApp()
    {
        $('#id_intake').prop('disabled', false);
        $('#id_program').prop('disabled', false);
        if($('#form_applicant').valid())
        {    
            $('#form_applicant').submit();
        }
    }

    $('select').select2();


    $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
    });
</script>