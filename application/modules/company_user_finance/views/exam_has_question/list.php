<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
    <h1 class="h3">Exam Exam Has Question</h1>
    <a href="add" class="btn btn-primary ml-auto">+ Add Exam Has Question</a>
  </div>
  <div class="page-container">
    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="">
              Advanced Search
              <i class="fa fa-angle-right" aria-hidden="true"></i>
              <i class="fa fa-angle-down" aria-hidden="true"></i>
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">

          <div id="collapseOne" class="panel-collapse collapse show" role="tabpanel" aria-labelledby="headingOne" aria-expanded="true" style="">
            <div class="panel-body p-2">

              <div class="row">

                <div class="col-lg-6">
                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Course</label>
                    <div class="col-sm-9">
                      <select name="id_course" id="id_course" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($courseList)) {
                          foreach ($courseList as $record) {
                            $selected = '';
                            if ($record->id == $searchParam['id_course']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>" <?php echo $selected;  ?>>
                              <?php echo  $record->code . " - " . $record->name;  ?>
                            </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>

                <div class="col-lg-6">
                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Topic</label>
                    <div class="col-sm-9">
                      <select name="id_topic" id="id_topic" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($topicList)) {
                          foreach ($topicList as $record) {
                            $selected = '';
                            if ($record->id == $searchParam['id_topic']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>" <?php echo $selected;  ?>>
                              <?php echo  $record->code . " - " . $record->name;  ?>
                            </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>

              </div>

              <div class="row">

                <div class="col-lg-6">
                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Difficult Level</label>
                    <div class="col-sm-9">
                      <select name="id_difficult_level" id="id_difficult_level" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($difficultLevelList)) {
                          foreach ($difficultLevelList as $record) {
                            $selected = '';
                            if ($record->id == $searchParam['id_difficult_level']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>" <?php echo $selected;  ?>>
                              <?php echo $record->code . " - " . $record->name;  ?>
                            </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>


                <div class="col-lg-6">
                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Bloom Taxonomy</label>
                    <div class="col-sm-9">
                      <select name="id_bloom_taxonomy" id="id_bloom_taxonomy" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($bloomTaxonomyList)) {
                          foreach ($bloomTaxonomyList as $record) {
                            $selected = '';
                            if ($record->id == $searchParam['id_bloom_taxonomy']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>" <?php echo $selected;  ?>>
                              <?php echo  $record->code . " - " . $record->name;  ?>
                            </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>

              </div>

              <div class="row">

                <div class="col-lg-6">
                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Course Learning Objective</label>
                    <div class="col-sm-9">
                      <select name="id_course_learning_objective" id="id_course_learning_objective" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($courseLearningObjectList)) {
                          foreach ($courseLearningObjectList as $record) {
                            $selected = '';
                            if ($record->id == $searchParam['id_course_learning_objective']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>" <?php echo $selected;  ?>>
                              <?php echo  $record->code . " - " . $record->name;  ?>
                            </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>



              </div>


              <hr />
              <div class="d-flex justify-content-center">
                <button type="submit" class="btn btn-primary">Search</button>
                <button type="reset" class="btn btn-link" onclick="clearSearchForm()">Clear All Fields</button>
              </div>
            </div>
          </div>

        </form>

      </div>
    </div>
    <?php
    if ($this->session->flashdata('success')) {
    ?>
      <div class="alert alert-success alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
      </div>
    <?php
    }
    if ($this->session->flashdata('error')) {
    ?>
      <div class="alert alert-danger alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Failure!</strong> <?php echo $this->session->flashdata('error'); ?>
      </div>
    <?php
    }
    ?>
    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Total Questions</th>
            <th>Course</th>
            <th>Topic</th>
            <th>Bloom Taxonomy</th>
            <th>Course Learning Objective</th>
            <th>Difficult Level</th>
            <th style="text-align: center;">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($examHasQuestionList)) {
            $i = 1;
            foreach ($examHasQuestionList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo ucfirst($record->question_count) ?></td>
                <td><?php echo $record->course_code . " - " . $record->course_name ?></td>
                <td><?php echo $record->topic_code . " - " . $record->topic_name ?></td>
                <td><?php echo $record->bloom_taxonomy_code . " - " . $record->bloom_taxonomy_name ?></td>
                <td><?php echo $record->course_learning_objective_code . " - " . $record->course_learning_objective_name ?></td>
                <td><?php echo $record->difficult_level_code . " - " . $record->difficult_level_name ?></td>
                <td class="text-center">

                  <a href="<?php echo 'edit/' . $record->id; ?>" class="btn" type="button" data-toggle="tooltip" data-placement="top" title="View">
                    <i class="fa fa-eye" style="color:magenta" aria-hidden="true">
                    </i>
                  </a>

                </td>
              </tr>
          <?php
              $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</main>
<script>
  $('select').select2();

  function clearSearchForm() {
    window.location.reload();
  }
</script>