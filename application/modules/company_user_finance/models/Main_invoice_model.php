<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Main_invoice_model extends CI_Model
{

    function getMainInvoiceListSearch($data)
    {

        $this->db->select('mi.*, p.code as programme_code, p.name as programme_name, cs.name as currency_name, cmp.name as company_name, cmp.registration_number');
        $this->db->from('main_invoice as mi');
        $this->db->join('company as cmp', 'mi.id_student = cmp.id');
        $this->db->join('programme as p', 'mi.id_program = p.id','left');
        $this->db->join('currency_setup as cs', 'mi.currency = cs.id','left');
        if ($data['invoice_number'] != '')
        {
            $likeCriteria = "(mi.invoice_number  LIKE '%" . $data['invoice_number'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['id_company'] != '')
        {
            $this->db->where('mi.id_student', $data['id_company']);
        }
        // if ($data['id_company_user'] != '')
        // {
        //     $this->db->where('mi.id_company_user', $data['id_company_user']);
        // }
        if($data['status'] != '')
        {
            // if($data['status'] == '0')
            // {
            //     $this->db->where('mi.paid_amount', 0);
            // }
            // else
            // {
                // $this->db->where('mi.status', $data['status']);
            // }
        }
        if($data['paid'] != '')
        {
            if($data['paid'] == '0')
            {
                $this->db->where('mi.paid_amount', 0);
            }
            elseif($data['paid'] == '1')
            {
                $this->db->where('mi.balance_amount', 0);
            }
            elseif($data['paid'] == '2')
            {
                $this->db->where('mi.paid_amount > ', 0);
                $this->db->where('mi.balance_amount >', 0);
            }
        }
        $this->db->where("mi.type ", "CORPORATE");
        $this->db->order_by("mi.id", "DESC");
        // $this->db->join('country as c', 'sp.id_country = c.id');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getMainInvoiceListSearchForPay($data)
    {

        $this->db->select('mi.*, p.code as programme_code, p.name as programme_name, cs.name as currency_name, cmp.name as company_name, cmp.registration_number');
        $this->db->from('main_invoice as mi');
        $this->db->join('company as cmp', 'mi.id_student = cmp.id');
        $this->db->join('programme as p', 'mi.id_program = p.id','left');
        $this->db->join('currency_setup as cs', 'mi.currency = cs.id','left');
        if ($data['invoice_number'] != '')
        {
            $likeCriteria = "(mi.invoice_number  LIKE '%" . $data['invoice_number'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['id_company'] != '')
        {
            $this->db->where('mi.id_student', $data['id_company']);
        }
        // if ($data['id_company_user'] != '')
        // {
        //     $this->db->where('mi.id_company_user', $data['id_company_user']);
        // }
        if($data['status'] != '')
        {
            if($data['status'] == '0')
            {
                $this->db->where('mi.paid_amount', 0);
            }
            // else
            // {
                // $this->db->where('mi.status', $data['status']);
            // }
        }
        if($data['paid'] != '')
        {
            if($data['paid'] == '0')
            {
                $this->db->where('mi.paid_amount', 0);
            }
            elseif($data['paid'] == '1')
            {
                $this->db->where('mi.balance_amount', 0);
            }
            elseif($data['paid'] == '2')
            {
                $this->db->where('mi.paid_amount > ', 0);
                $this->db->where('mi.balance_amount >', 0);
            }
        }
        $this->db->where("mi.type ", "CORPORATE");
        $this->db->order_by("mi.id", "DESC");
        // $this->db->join('country as c', 'sp.id_country = c.id');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function countryListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('country');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();      
         return $result;
    }

    function companyUserRoleListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('company_user_role');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function parentCompanyList($level)
    {
        $this->db->select('*');
        $this->db->from('company');
        $this->db->where('level', $level);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function getCompanyDetails($id)
    {
        $this->db->select('*');
        $this->db->from('company');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getMainInvoice($id)
    {
        $this->db->select('mi.*, cs.name as currency_name, p.name as programme_name, p.code as programme_code');
        $this->db->from('main_invoice as mi');
        $this->db->join('currency_setup as cs', 'mi.currency = cs.id','left');
        $this->db->join('programme as p', 'mi.id_program = p.id','left');
        $this->db->where('mi.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getMainInvoiceDetails($id)
    {
        $this->db->select('mid.*, fstp.name as fee_setup');
        $this->db->from('main_invoice_details as mid');
        $this->db->join('fee_setup as fstp', 'mid.id_fee_item = fstp.id');
        $this->db->where('mid.id_main_invoice', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function getMainInvoiceDiscountDetails($id)
    {
        $this->db->select('*');
        $this->db->from('main_invoice_discount_details');   
        $this->db->where('id_main_invoice', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function getMainInvoiceHasStudentList($id)
    {
        $this->db->select('pmhs.*, s.full_name as student_name, s.nric');
        $this->db->from('main_invoice_has_students as pmhs');   
        $this->db->join('student as s', 'pmhs.id_student = s.id');
        $this->db->where('pmhs.id_main_invoice', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function editPerformaMainInvoice($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('main_invoice', $data);
        return $this->db->affected_rows();
    }

    function updateStudentHasProgramme($data,$id_invoice)
    {
        $this->db->where_in('id_invoice', $id_invoice);
        $this->db->update('student_has_programme', $data);
        return $this->db->affected_rows();
    }

    function movePerformaInvoiceToMainInvoice($id_performa_invoice)
    {
        $performa_main_invoice = $this->getMainInvoice($id_performa_invoice);

        if($performa_main_invoice)
        {
            unset($performa_main_invoice->id);
            unset($performa_main_invoice->id_main_invoice);
            unset($performa_main_invoice->currency_name);

            $performa_main_invoice->invoice_number = $this->generateMainInvoiceNumber();


            $id_main_invoice = $this->addNewMainInvoice($performa_main_invoice);

            // echo "<Pre>";print_r($id_main_invoice);exit;

            if($id_main_invoice)
            {
                $performa_main_invoice_details = $this->getMainInvoiceDetails($id_performa_invoice);

                foreach ($performa_main_invoice_details as $detail)
                {
                    unset($detail->id);
                    unset($detail->fee_setup);
                    $detail->id_main_invoice = $id_main_invoice;

                   $id_main_invoice_details = $this->addNewMainInvoiceDetails($detail);
                }


                $performa_main_invoice_discount_details = $this->getMainInvoiceDiscountDetails($id_performa_invoice);

                foreach ($performa_main_invoice_discount_details as $discount_detail)
                {
                    unset($discount_detail->id);
                    unset($discount_detail->fee_setup);
                    $discount_detail->id_main_invoice = $id_main_invoice;
                    
                   $id_main_invoice_discount_details = $this->addNewMainInvoiceDiscountDetail($discount_detail);
                }


                $performa_main_invoice_student = $this->getMainInvoiceDiscountDetails($id_performa_invoice);

                foreach ($performa_main_invoice_student as $student_detail)
                {
                    unset($student_detail->id);
                    unset($student_detail->student_name);
                    unset($student_detail->nric);
                    $student_detail->id_main_invoice = $id_main_invoice;
                    
                   $id_main_invoice_student_details = $this->addNewMainInvoiceHasStudents($student_detail);
                }

                $update_performa_data['id_main_invoice'] = $id_main_invoice;

                $updated_performa_invoice = $this->updatePerformaMainInvoice($update_performa_data, $id_performa_invoice);
            }

            return $id_main_invoice;
        }

        return TRUE;
    }

    function generateMainInvoiceNumber()
    {
        $year = date('y');
        $Year = date('Y');
            $this->db->select('j.*');
            $this->db->from('main_invoice as j');
            $this->db->order_by("id", "desc");
            $query = $this->db->get();
            $result = $query->num_rows();

     
            $count= $result + 1;
           $jrnumber = $number = "INV" .(sprintf("%'06d", $count)). "/" . $Year;
           return $jrnumber;        
    }


    function addNewMainInvoice($data)
    {
        $this->db->trans_start();
        $this->db->insert('main_invoice', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function addNewMainInvoiceDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('main_invoice_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function addNewMainInvoiceHasStudents($id_students,$id_main_invoice,$amount)
    {

            $this->db->trans_start();
            $this->db->insert('main_invoice_has_students', $details);
            $insert_id = $this->db->insert_id();
            $this->db->trans_complete();
    
           return $insert_id;
    }

    function addNewMainInvoiceDiscountDetail($data)
    {
        $this->db->trans_start();
        $this->db->insert('main_invoice_discount_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function updatePerformaMainInvoice($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('performa_main_invoice', $data);
        return $this->db->affected_rows();
    }

    function generateReceiptNumber()
    {
        $year = date('y');
        $Year = date('Y');
        
        $this->db->select('j.*');
        $this->db->from('receipt as j');
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        $result = $query->num_rows();

 
        $count= $result + 1;
       $jrnumber = $number = "REC" .(sprintf("%'06d", $count)). "/" . $Year;
       return $jrnumber;
    }

    function addNewReceipt($data)
    {
        $this->db->trans_start();
        $this->db->insert('receipt', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function getOrganisation()
    {
        $this->db->select('fc.*');
        $this->db->from('organisation as fc');
        $this->db->where('fc.status', 1);
        $this->db->order_by("fc.id", "DESC");
        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    function addNewReceiptDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('receipt_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function addNewReceiptPaymentDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('receipt_paid_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editMainInvoice($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('main_invoice', $data);
        return $this->db->affected_rows();
    }

    function getMainInvoiceStudentData($id_student)
    {
        $this->db->select('stu.full_name, stu.nric, stu.id_degree_type');
        $this->db->from('student as stu');
        $this->db->where('stu.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getMainInvoiceCorporateData($id)
    {
        $this->db->select('stu.name as full_name, stu.registration_number as nric');
        $this->db->from('company as stu');
        $this->db->where('stu.id', $id);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getBankRegistration()
    {
        $this->db->select('fc.*, c.name as country, s.name as state');
        $this->db->from('bank_registration as fc');
        $this->db->join('country as c', 'fc.id_country = c.id');
        $this->db->join('state as s', 'fc.id_state = s.id');
        $this->db->where('fc.status', 1);
        $this->db->order_by("fc.id", "DESC");
        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }
}