<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>View Budget Allocation</h3>
        </div>
        <form id="form_subject" action="" method="post">


        <div class="form-container">
            <h4 class="form-group-title">Budget Allocation</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Financial Year <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $budgetAmount->financial_year;?>" readonly>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Budget Year <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $budgetAmount->budget_year;?>" readonly>
                    </div>
                </div>



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Department Code<span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $budgetAmount->department . " - " . $budgetAmount->department_name;?>" readonly>
                    </div>
                </div>
            </div>


             <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Allocated Amount <span class='error-text'>*</span></label>
                       <input type="text" class="form-control" id="name" name="name" value="<?php echo $budgetAmount->amount; ?>" readonly>
                          <!-- . " - " . $budgetAmount->name;?>"> -->
                    </div>
                </div>

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Balance Amount <span class='error-text'>*</span></label>
                       <input type="text" class="form-control" id="name" name="name" value="<?php echo $budgetAmount->balance_amount; ?>" readonly>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Increment Amount <span class='error-text'>*</span></label>
                       <input type="text" class="form-control" id="name" name="name" value="<?php echo $budgetAmount->increment_amount; ?>" readonly>
                          <!-- . " - " . $budgetAmount->name;?>"> -->
                    </div>
                </div>

            </div>


             <div class="row">
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Status</label>
                       <input type="text" class="form-control" id="name" name="name" value="<?php 
                       if( $budgetAmount->allocation_status == '0')
                      {
                        echo "Pending";
                      }
                      elseif($budgetAmount->allocation_status == '1')
                      {
                        echo "Approved";
                      } 
                      elseif($budgetAmount->allocation_status == '2')
                      {
                        echo "Rejected";
                      } 

                     ?>" readonly>
                          <!-- . " - " . $budgetAmount->name;?>"> -->
                    </div>
                </div>

            </div>

          </div>

            </form>

            <h3>Budget Allocation</h3>

            <button type="button" class="btn btn-info btn-lg" onclick="opendialog()">Add</button>

            <br>
            <br>    

            <div class="row">
                <div id="view"></div>
            </div>




            <!-- <h3>Budget Allocation</h3> -->



    <div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Budget Allocation</h4>
      </div>
      <div class="modal-body">

 <form id="form_pr_entry_details" action="" method="post">

             <div class="row">

                    <div class="col-sm-3">
                    <div class="form-group">
                        <label>Fund Code <span class='error-text'>*</span></label>
                        <select name="fund_code" id="fund_code" class="form-control" style="width: 196px">
                            <option value="">Select</option>
                            <?php
                            if (!empty($fundCodeList))
                            {
                                foreach ($fundCodeList as $record)
                                {?>
                             <option value="<?php echo $record->code;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div> 

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Department Code <span class='error-text'>*</span></label>
                        <select name="department_code" id="department_code" class="form-control" style="width: 196px">
                            <option value="">Select</option>
                            <?php
                            if (!empty($departmentCodeList))
                            {
                                foreach ($departmentCodeList as $record)
                                {?>
                             <option value="<?php echo $record->code;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>  

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Activity Code <span class='error-text'>*</span></label>
                        <select name="activity_code" id="activity_code" class="form-control" style="width: 196px">
                            <option value="">Select</option>
                            <?php
                            if (!empty($activityCodeList))
                            {
                                foreach ($activityCodeList as $record)
                                {?>
                             <option value="<?php echo $record->code;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>  

                
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Account Code <span class='error-text'>*</span></label>
                        <select name="account_code" id="account_code" class="form-control" style="width: 196px">
                            <option value="">Select</option>
                            <?php
                            if (!empty($accountCodeList))
                            {
                                foreach ($accountCodeList as $record)
                                {?>
                             <option value="<?php echo $record->code;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>  

            </div>

            <div class="row">  


                 <div class="col-sm-3">
                    <div class="form-group">
                        <label>Allocated Amount <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="allocated_amount" name="allocated_amount">
                    </div>
                </div>
            </div>

    </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" onclick="saveData()">Add</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>





            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="button" class="btn btn-primary btn-lg" onclick="validateDetailsData()">Save</button>
                    <a href="../list" class="btn btn-link">Back</a>
                </div>
            </div>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>
</div>

<script>

    $('select').select2();

     function opendialog()
    {
        $('#myModal').modal('show');
    }


    function saveData()
    {
        if($('#form_pr_entry_details').valid())
        {
            $('#myModal').modal('hide');
            var tempPR = {};
            tempPR['fund_code'] = $("#fund_code").val();
            tempPR['department_code'] = $("#department_code").val();
            tempPR['activity_code'] = $("#activity_code").val();
            tempPR['account_code'] = $("#account_code").val();
            tempPR['budget_amount'] = $("#budget_amount").val();
            tempPR['allocated_amount'] = $("#allocated_amount").val();
            tempPR['id_budget_amount'] = <?php echo $budgetAmount->id; ?>;
        
            $.ajax(
            {
               // url: '/budget/budgetAllocation/addAllocationData',
               url: '/budget/budgetAllocation/tempadd',
               type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // $("#view").html(result);
                window.location.reload();
                // var ta = $("#total_detail").val();
                // // alert(ta);
                // $("#amount").val(ta);
                // if($result== '')
                // {
                //     $("#amount").val('0');
                // }
                // if(ta == '')
                // {
                //     $("#amount").val('0');
                // }
               }
            });
        }
    }

      function viewData()
    {
            var id_budget_amount = <?php echo $id_budget_amount; ?>;
        
            $.ajax(
            {
               url: '/budget/budgetAllocation/viewData',
               type: 'POST',
               data:
               {
                id_budget_amount: id_budget_amount
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                if(result != '')
                {
                    $("#view").html(result);
                }              
            }

            });
    }


    function validateDetailsData()
    {
        if($('#form_subject').valid())
        {
            
            var total_detail = <?php echo $total ?>;
            var budget_amount = <?php echo $budgetAmount->amount; ?>;

            if(total_detail != budget_amount)
            {
                alert('Sum Of All Budget Allocated Amount '+total_detail+' Should Be Equal To Budget Amount '+budget_amount)
            }
            else
            {
                $('#form_subject').submit();
            }
        }    
    }



    function deleteBudgetAllocation(id)
    {
        // alert(id);

         $.ajax(
            {

               url: '/budget/budgetAllocation/tempDeleteEdit/'+id,
               // url: '/budget/budgetAllocation/deleteBudgetAllocation/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                 // $("#view").html(result);
                 window.location.reload();
               }
            });
    }



    $(document).ready(function()
    {
        viewData();
        $("#form_pr_entry_details").validate(
        {
            rules:
            {
                fund_code:
                {
                    required: true
                },
                department_code:
                {
                    required: true
                },
                activity_code:
                {
                    required: true
                },
                account_code:
                {
                    required: true
                },
                allocated_amount:
                {
                    required: true
                }
            },
            messages:
            {
                fund_code:
                {
                    required: "<p class='error-text'>Select Fund Code</p>",
                },
                department_code:
                {
                    required: "<p class='error-text'>Select Department Code</p>",
                },
                activity_code:
                {
                    required: "<p class='error-text'>Select Activity Code</p>",
                },
                account_code:
                {
                    required: "<p class='error-text'>Select Account Code</p>",
                },
                allocated_amount:
                {
                    required: "<p class='error-text'>Enter Allocated Amount</p>",
                }

            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>