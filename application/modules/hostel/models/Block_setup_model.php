<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Block_setup_model extends CI_Model
{
    function getHostelRegistrationListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('hostel_registration');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getBuildingListByStatus($status)
    {
         $this->db->select('*');
        $this->db->from('hostel_room');
        $this->db->where('status', $status);
        $this->db->where('level', '1');
        $query = $this->db->get();
        return $query->result();
    }

    function getBuildingList()
    {
         $this->db->select('*');
        $this->db->from('hostel_room');
        $this->db->where('level', '1');
        $query = $this->db->get();
        return $query->result();
    }

    function blockListListSearch($data)
    {
        $this->db->select('fc.*, hr.name as hostel_name, hr.code as hostel_code, hra.code as apartment_code, hra.name as apartment_name');
        $this->db->from('hostel_room as fc');
        $this->db->join('hostel_registration as hr', 'fc.id_hostel = hr.id');
        $this->db->join('hostel_room as hra', 'fc.id_parent = hra.id');
        if ($data['name'] != '')
        {
            $likeCriteria = "(fc.name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        {
        if ($data['id_hostel'] != '')
            $this->db->where('fc.id_hostel', $data['id_hostel']);
        }
        if ($data['id_building'] != '')
        {
            $this->db->where('fc.id_parent', $data['id_parent']);
        }
        // if ($data['id_intake'] != '')
        // {
        //     $this->db->where('fc.id_intake', $data['id_intake']);
        // }

        $this->db->where('fc.level', '2');
        $this->db->order_by("fc.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getHostelRoom($id)
    {
        $this->db->select('*');
        $this->db->from('hostel_room');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }


    function addNewHostelRoom($data)
    {
        $this->db->trans_start();
        $this->db->insert('hostel_room', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editHostelRoom($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('hostel_room', $data);
        return TRUE;
    }

    function checkHostelRoomDuplication($data)
    {
        $this->db->select('*');
        $this->db->from('hostel_room');
        $this->db->where('code', $data['code']);
        $query = $this->db->get();
        return $query->row();
    }

    function deleteInventoryAllotment($id_inventory_allotment)
    {
        $this->db->where('id', $id_inventory_allotment);
        $this->db->delete('inventory_allotment');
        return TRUE;
    }

    function getBuildingListByHostelId($id_hostel)
    {
        $this->db->select('*');
        $this->db->from('hostel_room');
        $this->db->where('level', '1');
        $this->db->where('id_hostel', $id_hostel);
        $this->db->where('status', '1');
        $query = $this->db->get();
        return $query->result();
    }
}