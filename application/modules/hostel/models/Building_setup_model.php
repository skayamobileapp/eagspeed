<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Building_setup_model extends CI_Model
{
    function getHostelRegistrationListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('hostel_registration');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function buildingListListSearch($data)
    {
        $this->db->select('fc.*, hr.name as hostel_name, hr.code as hostel_code');
        $this->db->from('hostel_room as fc');
        $this->db->join('hostel_registration as hr', 'fc.id_hostel = hr.id');
        if ($data['name'] != '')
        {
            $likeCriteria = "(fc.name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        {
        if ($data['id_hostel'] != '')
            $this->db->where('fc.id_hostel', $data['id_hostel']);
        }
        // if ($data['type'] != '')
        // {
        //     $this->db->where('fc.type', $data['type']);
        // }
        // if ($data['id_intake'] != '')
        // {
        //     $this->db->where('fc.id_intake', $data['id_intake']);
        // }

        $this->db->where('fc.level', '1');
        $this->db->order_by("fc.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getHostelRoom($id)
    {
        $this->db->select('*');
        $this->db->from('hostel_room');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }


    function addNewHostelRoom($data)
    {
        $this->db->trans_start();
        $this->db->insert('hostel_room', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editHostelRoom($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('hostel_room', $data);
        return TRUE;
    }

    function checkHostelRoomDuplication($data)
    {
        $this->db->select('*');
        $this->db->from('hostel_room');
        $this->db->where('code', $data['code']);
        $query = $this->db->get();
        return $query->row();
    }

    function deleteInventoryAllotment($id_inventory_allotment)
    {
        $this->db->where('id', $id_inventory_allotment);
        $this->db->delete('inventory_allotment');
        return TRUE;
    }
}