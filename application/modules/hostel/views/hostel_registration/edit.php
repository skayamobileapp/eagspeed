<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Hostel</h3>
        </div>
        <form id="form_bank" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Hostel Details</h4>



            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $hostelRegistration->name;?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="code" name="code" value="<?php echo $hostelRegistration->code;?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Type</label>
                        <select class="form-control" id="type" name="type" style="width: 405px;">
                            <option value="">SELECT</option>
                            <option value="Male" <?php if($hostelRegistration->type == "Male"){ echo "selected=selected"; } ?>>MALE</option>
                            <option value="Female" <?php if($hostelRegistration->type == "Female"){ echo "selected=selected"; } ?>>FEMALE</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Max. Capacity <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="max_capacity" name="max_capacity" value="<?php echo $hostelRegistration->max_capacity;?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Staff Incharge <span class='error-text'>*</span></label>
                        <select name="id_staff" id="id_staff" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($staffList))
                            {
                                foreach ($staffList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>"
                                        <?php 
                                        if($record->id == $hostelRegistration->id_staff)
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->ic_no . " - " . $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Contact Number <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="contact_number" name="contact_number" value="<?php echo $hostelRegistration->contact_number;?>">
                    </div>
                </div>

            </div>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Address <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="address" name="address" value="<?php echo $hostelRegistration->address;?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Country <span class='error-text'>*</span></label>
                        <select name="id_country" id="id_country" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($countryList))
                            {
                                foreach ($countryList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>"
                                        <?php 
                                        if($record->id == $hostelRegistration->id_country)
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>State <span class='error-text'>*</span></label>
                        <select name="id_state" id="id_state" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($stateList))
                            {
                                foreach ($stateList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>"
                                        <?php 
                                        if($record->id == $hostelRegistration->id_state)
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

               <!--  <div class="col-sm-4">
                    <div class="form-group">
                        <label>State <span class='error-text'>*</span></label>
                        <span id='view_state'></span>
                    </div>
                </div> -->
            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Landmark <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="landmark" name="landmark" value="<?php echo $hostelRegistration->landmark;?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>City <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="city" name="city" value="<?php echo $hostelRegistration->city;?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Zipcode <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="zipcode" name="zipcode" value="<?php echo $hostelRegistration->zipcode;?>">
                    </div>
                </div>

            </div>



            <div class="row">

               <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status *</p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" <?php if($hostelRegistration->status=='1') {
                                 echo "checked=checked";
                              };?>><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0" <?php if($hostelRegistration->status=='0') {
                                 echo "checked=checked";
                              };?>>
                              <span class="check-radio"></span> In-Active
                            </label>                              
                        </div>                         
                </div>
            </div>

        </div>

        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="../list" class="btn btn-link">Cancel</a>
            </div>
        </div>
            
        </form>

    </div>

        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
</div>
<script>

    function getStateByCountry(id)
    {
        $.get("/finance/hostelRegistration/getStateByCountry/"+id, function(data, status){
       
            $("#view_state").html(data);
        });
    }



    $(document).ready(function() {
        $("#form_bank").validate({
            rules: {
                code: {
                    required: true
                },
                name: {
                    required: true
                },
                type: {
                    required: true
                },
                address: {
                    required: true
                },
                id_country: {
                    required: true
                },
                id_state: {
                    required: true
                },
                landmark: {
                    required: true
                },
                city: {
                    required: true
                },
                zipcode: {
                    required: true
                },
                id_staff: {
                    required: true
                },
                contact_number: {
                    required: true
                },
                max_capacity: {
                    required: true
                }
            },
            messages: {
                code: {
                    required: "<p class='error-text'>Hostel Code Required</p>",
                },
                name: {
                    required: "<p class='error-text'>Hostel Name Required</p>",
                },
                type: {
                    required: "<p class='error-text'>Select Hostel Type</p>",
                },
                address: {
                    required: "<p class='error-text'>Address Required</p>",
                },
                id_country: {
                    required: "<p class='error-text'>Select Country</p>",
                },
                id_state: {
                    required: "<p class='error-text'>Select State</p>",
                },
                landmark: {
                    required: "<p class='error-text'>Enter Landmark</p>",
                },
                city: {
                    required: "<p class='error-text'>Enter City Name</p>",
                },
                zipcode: {
                    required: "<p class='error-text'>Enter Zipcode</p>",
                },
                id_staff: {
                    required: "<p class='error-text'>Select Staff Incharge</p>",
                },
                contact_number: {
                    required: "<p class='error-text'>Contact Number Required</p>",
                },
                max_capacity: {
                    required: "<p class='error-text'>Maximum Capacity Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    $('select').select2();

</script>
