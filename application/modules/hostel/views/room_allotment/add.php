<?php $this->load->helper("form"); ?>

<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>Room Allotment</h3>
            </div>


      <form id="form_pr_entry" action="" method="post">



        <div class="form-container">
            <h4 class="form-group-title">Select Room For Allocation</h4>
            <!-- <h4 >Search Room</h4> -->


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Hostel Name <span class='error-text'>*</span></label>
                        <select name="id_hostel" id="id_hostel" class="form-control" onchange="getBuildingByHostel(this.value)">
                            <option value="">Select</option>
                            <?php
                            if (!empty($hostelList))
                            {
                                foreach ($hostelList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Apartment <span class='error-text'>*</span></label>
                        <span id="view_building"></span>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                           <label>Unit <span class='error-text'>*</span></label>
                        <span id="view_block"></span>
                    </div>
                </div>

              </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label style="display: none;" id="display_room">Room <span class='error-text'>*</span></label>
                        <span id='view_room' ></span>
                    </div>
                </div>

                <div class="col-sm-4">
                        <div class="form-group">
                            <label>From Date <span class='error-text'>*</span></label>
                            <input type="text" class="form-control datepicker" id="from_dt" name="from_dt" autocomplete="off">
                        </div>
                </div>

                <div class="col-sm-4">
                      <div class="form-group">
                          <label>To Date <span class='error-text'>*</span></label>
                          <input type="text" class="form-control datepicker" id="to_dt" name="to_dt" autocomplete="off">
                      </div>
                </div>
                
            

                <!-- <div class="col-sm-4">
                  <div class="form-group">
                        <label style="display: none;" id="display_room_vacant">Room <span class='error-text'>*</span></label>
                        <span id='view_room_vacant' ></span>
                    </div>
                </div> -->
                
            </div>


            <!-- <div class="modal-footer">
                <button type="button" class="btn btn-default" onclick="formValidate()">Search</button>
              </div> -->

        
        </div>

        <div class="custom-table">
              <div id="view_room_vacant"></div>
        </div>

        <br>





        <div class="form-container">
            <h4 class="form-group-title">Student Details</h4>


            <div class="row">

            
              <div class="col-sm-4">
                    <div class="form-group">
                        <label>Program <span class='error-text'>*</span></label>
                        <select name="id_programme" id="id_programme" class="form-control" onchange="getIntakes()">
                            <option value="">Select</option>
                            <?php
                            if (!empty($programmeList))
                            {
                                foreach ($programmeList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label style="display: none;" id="display_intake">Intake <span class='error-text'>*</span></label>
                        <span id='view_intake' ></span>
                    </div>
                </div>

                <div class="col-sm-4" style="display: none;" id="display_degree">
                    <div class="form-group">
                        <label >Degree Type <span class='error-text'>*</span></label>
                        <select name="id_degree_type" id="id_degree_type" class="form-control" onchange="getStudentByProgNIntake()" style="width: 398px">
                            <option value="">Select</option>
                            <?php
                            if (!empty($degreeTypeList))
                            {
                                foreach ($degreeTypeList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
              </div>


            </div>

            <div class="row">
               
                <div class="col-sm-4">
                    <div class="form-group">
                        <label style="display: none;" id="display_student">Student <span class='error-text'>*</span></label>
                        <span id='view_student' ></span>
                    </div>
                </div>
            </div>

        </div>




        
        <div class="custom-table">
              <div id="view"></div>
        </div>



        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="button" class="btn btn-primary btn-lg" onclick="formValidate()">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>

      </form>
        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>

<script>

   function getIntakes()
    {

     var tempPR = {};
        tempPR['id_programme'] = $("#id_programme").val();
            $.ajax(
            {
               url: '/hostel/roomAllotment/getIntakes',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_intake").html(result);
                  $("#display_intake").show();
                  $("#display_degree").show();


                // if (tempPR['id_programme'] != '' && tempPR['id_intake'] == '')
                // {
                    
                // }
                // var ita = $("#invoice_total_amount").val();
                // $("#receipt_amount").val(ita);
               }
            });
    }


    function getStudentByProgNIntake()
    {

     
        // alert(tempPR);

        if($("#id_programme").val() != '' && $("#id_intake").val() != '' && $("#id_degree_type").val() != '')
        {

          var tempPR = {};
        tempPR['id_programme'] = $("#id_programme").val();
        tempPR['id_intake'] = $("#id_intake").val();
        tempPR['id_degree_type'] = $("#id_degree_type").val();

            $.ajax(
            {
               url: '/hostel/roomAllotment/getStudentByProgNIntake',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {

                  $("#display_student").show();
                  $("#view_student").html(result);
                

                // if (tempPR['id_programme'] != '' && tempPR['id_intake'] == '')
                // {
                    
                // }
                // var ita = $("#invoice_total_amount").val();
                // $("#receipt_amount").val(ita);
               }
            });
          }
    }

    function displaydata()
    {

        var id_intake = $("#id_intake").val();
        var id_programme = $("#id_programme").val();
        var id_student = $("#id_student").val();
        // alert(id_student);

            $.ajax(
            {
               url: '/hostel/roomAllotment/displaydata',
                type: 'POST',
               data:
               {
                'id_intake': id_intake,
                'id_programme': id_programme,
                'id_student': id_student
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                if (id_programme != '' && id_intake != '')
                {
                  $("#view").html(result);
                  
                  var student_allotment_count = $("#student_allotment_count").val();
                  // alert(student_allotment_count);
                  if(student_allotment_count > 0)
                  {
                     alert('Room Already Alloted For this Student, Allotment Restricted Only One Seat For Student');
                  }else
                  {

                  }
                }
               }

              });        
    }

    function getBuildingByHostel(id)
    {

        $.get("/hostel/roomAllotment/getBuildingListByHostelId/"+id, function(data, status){
       
            $("#view_building").html(data);
        });
    }

    function getBlockListData()
    {
            // alert('id_hostel');

        var id_hostel = $("#id_hostel").val();
        var id_building = $("#id_building").val();

        if(id_hostel != '' && id_building != '')
        {

        var tempPR = {};
        tempPR['id_hostel'] = id_hostel;
        tempPR['id_building'] = id_building;

            $.ajax(
            {
               url: '/hostel/roomAllotment/getBlockList',
               type: 'POST',
               data:
               {
                data: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert(result);
                if(result == '')
                {
                    alert('No Block Found For Entered Data')
                }
                else
                {
                    // alert(result);
                    $("#view_block").html(result);
                }
               }
            });
        }
    }

    function getRoomByBlockNHostelId()
    {
       var id_hostel = $("#id_hostel").val();
        var id_block = $("#id_block").val();

        if(id_hostel != '' && id_block != '')
        {

        var tempPR = {};
        tempPR['id_hostel'] = id_hostel;
        tempPR['id_block'] = id_block;

            $.ajax(
            {
               url: '/hostel/roomAllotment/getRoomByBlockNHostelId',
               type: 'POST',
               data:
               {
                data: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert(result);
                if(result == '')
                {
                    alert('No Room Found For Entered Data')
                }
                else
                {
                    // alert(result);
                    $("#view_room").html(result);
                    $("#display_room").show();

                }
               }
            });
        }
    }

    function getRoomDetails()
    {
        var id_hostel = $("#id_hostel").val();
        var id_room = $("#id_room").val();

        if(id_hostel != '' && id_room != '')
        {

        var tempPR = {};
        tempPR['id_hostel'] = id_hostel;
        tempPR['id_room'] = id_room;

            $.ajax(
            {
               url: '/hostel/roomAllotment/getRoomDetails',
               type: 'POST',
               data:
               {
                data: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert(result);
                if(result == '')
                {
                    alert('No Room Found For Entered Data')
                }
                else
                {
                    // alert(result);
                    $("#view_room_vacant").html(result);
                    // $("#display_room").show();

                }
               }
            });
        }


    }

    function formValidate()
    {
      if($('#form_pr_entry').valid())
      {
        var vacant_seats = $("#vacant_seats").val();
        // alert(vacant_seats);
        if(vacant_seats == 0)
        {
          alert('No Vacancy Available In This Room, Select Another Room');
        }
        else
        {
          $('#form_pr_entry').submit();
        }
      }
    }

    $(document).ready(function() {
        $("#form_pr_entry").validate({
            rules: {
                id_programme: {
                    required: true
                },
                 id_intake: {
                    required: true
                },
                id_degree_type: {
                    required: true
                },
                 id_hostel: {
                    required: true
                },
                 id_building: {
                    required: true
                },
                 id_block: {
                    required: true
                },
                 id_student: {
                    required: true
                },
                 id_room: {
                    required: true
                },
                 from_dt: {
                    required: true
                },
                 to_dt: {
                    required: true
                }
            },
            messages: {
                id_programme: {
                    required: "<p class='error-text'>Select Programme</p>",
                },
                id_intake: {
                    required: "<p class='error-text'>Select Student</p>",
                },
                id_degree_type: {
                    required: "<p class='error-text'>Select Degree Type</p>",
                },
                id_hostel: {
                    required: "<p class='error-text'>Select Hostel</p>",
                },
                id_building: {
                    required: "<p class='error-text'>Select Apartment</p>",
                },
                id_block: {
                    required: "<p class='error-text'>Select Unit</p>",
                },
                id_student: {
                    required: "<p class='error-text'>Select Student</p>",
                },
                id_room: {
                    required: "<p class='error-text'>Select Room</p>",
                },
                from_dt: {
                    required: "<p class='error-text'>Select From Date</p>",
                },
                to_dt: {
                    required: "<p class='error-text'>Select To Date</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script type="text/javascript">
    $('select').select2();
</script>
<script>
  $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
  } );
</script>