<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>Placement Form</h3>
      <!-- <a href="add" class="btn btn-primary">+ Add Convocation</a> -->
    </div>
    <form action="" method="post" id="searchForm">
    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">
                

              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Student Name / NRIC</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="name" value="<?php echo $searchParam['name']; ?>">
                    </div>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Program</label>
                    <div class="col-sm-8">
                      <select name="id_program" id="id_program" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($programList)) {
                          foreach ($programList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['id_program']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->code ."-".$record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>

              </div>


              <div class="row">

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Intake</label>
                    <div class="col-sm-8">
                      <select name="id_intake" id="id_intake" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($intakeList)) {
                          foreach ($intakeList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['id_intake']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->year ." - " . $record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Qualification Level</label>
                    <div class="col-sm-8">
                      <select name="id_qualification" id="id_qualification" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($qualificationList)) {
                          foreach ($qualificationList as $record)
                          {
                            $selected = '';
                            if ($record->code == $searchParam['id_qualification']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->code;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->code ."-".$record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>

              </div>

              <!-- <div class="row">

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Budget Year</label>
                    <div class="col-sm-8">
                      <select name="id_budget_year" id="id_budget_year" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($budgetYearList)) {
                          foreach ($budgetYearList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['id_budget_year']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">PR Type</label>
                    <div class="col-sm-8">
                      <select name="type_of_pr" id="type_of_pr" class="form-control">
                        <option value=''>Select</option>
                          <option value='PO' <?php if($searchParam['type_of_pr'] =='PO')
                            { echo "selected=selected";} ?> >PO
                          </option>
                          <option value='Direct Purchase' <?php if($searchParam['type_of_pr'] =='Non-PO')
                          { echo "selected=selected";} ?> >Non-PO
                          </option>
                          <option value='Warrant' <?php if($searchParam['type_of_pr'] =='Warrant')
                          { echo "selected=selected";} ?> >Warrant
                          </option>
                          <option value='Tender' <?php if($searchParam['type_of_pr'] =='Tender')
                          { echo "selected=selected";} ?> >Tender
                          </option>
                      </select>
                    </div>
                  </div>
                </div>
                
              </div>
 -->

              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary" name="button" value="search">Search</button>
                <button type="reset" class="btn btn-link" onclick="clearSearchForm()">Clear All Fields</button>
              </div>
            </div>
          </div>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
           <th>Sl No.</th>
           <th>Student</th>
            <th>Program</th>
            <th>Intake</th>
            <th>Qualification</th>
            <th>Company Name</th>
            <th>Joining Date</th>
            <th>Company Address</th>
            <th>Company Phone</th>
            <th>Company EMail</th>
            <th>City</th>
            <!-- <th>Status</th> -->
            <th class="text-center">Action</th>
            <!-- <th style="text-align: center;"><input type="checkbox" id="checkAll" name="checkAll"> Check All</th> -->
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($placementList)) {
              $i=0;
            foreach ($placementList as $record)
            {
          ?>
              <tr>
                <td><?php echo $i+1;?></td>
                <td><?php echo $record->nric . " - ". $record->full_name ?></td>
                <td><?php echo $record->program_code . " - ". $record->program_name ?></td>
                <td><?php echo $record->intake_year . " - ". $record->intake_name ?></td>
                <td><?php echo $record->qualification_code . " - ". $record->qualification_name ?></td>
                <td><?php echo $record->company_name ?></td>
                <td><?php echo date('d-m-Y', strtotime($record->joining_dt)) ?></td>
                <td><?php echo $record->company_address ?></td>
                <td><?php echo $record->phone ?></td>
                <td><?php echo $record->email ?></td>
                <td><?php echo $record->city ?></td>
                <!-- <td><?php echo $record->duration ?></td>
                <td><?php if( $record->status == '1')
                {
                  echo "Approved";
                }
                else if( $record->status == '0')
                {
                   echo "Pending";
                }
                else if( $record->status == '2')
                {
                  echo "Rejected";
                } 
                ?></td> -->
                <td class="text-center">
                  <a href="<?php echo 'edit/' . $record->id; ?>" title="View">View</a>
                </td>
              
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
      <div class="app-btn-group">
          <!-- <button type="submit" class="btn btn-primary" name="button" value="approve">Approve</button> -->
      </div>
    </div>
  </form>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script>
  $(function () {
      $("#checkAll").click(function () {
          if ($("#checkAll").is(':checked')) {
              $(".check").prop("checked", true);
          } else {
              $(".check").prop("checked", false);
          }
      });
  });

  $('select').select2();

    function clearSearchForm()
    {
      window.location.reload();
    }
</script>