<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>View Placement Form</h3>
        </div>



            <div class="form-container">
                <h4 class="form-group-title">Student Details</h4>
                <div class='data-list'>
                    <div class='row'> 
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student Name :</dt>
                                <dd><?php echo ucwords($studentDetails->full_name);?></dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd><?php echo $studentDetails->nric ?></dd>
                            </dl>
                            <dl>
                                <dt>Student Email :</dt>
                                <dd><?php echo $studentDetails->email_id; ?></dd>
                            </dl>
                             <dl>
                                <dt>Nationality :</dt>
                                <dd><?php echo $studentDetails->nationality ?></dd>
                            </dl>                            
                        </div>        
                        
                        <div class='col-sm-6'>                           
                            <dl>
                                <dt>Intake :</dt>
                                <dd><?php echo $studentDetails->intake_name ?></dd>
                            </dl>
                            <dl>
                                <dt>Program :</dt>
                                <dd><?php echo $studentDetails->programme_name; ?></dd>
                            </dl>
                            <dl>
                                <dt>Qualification Type :</dt>
                                <dd><?php echo $studentDetails->qualification_code . " - " . $studentDetails->qualification_name; ?></dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd><?php echo ""; ?></dd>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>


    <form id="form_pr_entry" action="" method="post">


       

        
        <div class="form-container">
            <h4 class="form-group-title">Placement Form Details</h4>


         <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Company Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="company_name" name="company_name" readonly="readonly" value="<?php echo $placement->company_name;?>">
                    </div>
                </div>

              

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Company Phone No. <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="phone" name="phone" readonly="readonly" value="<?php echo $placement->phone;?>">
                    </div>
                </div>   

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Email <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="email" name="email" readonly="readonly" value="<?php echo $placement->email;?>">
                    </div>
                </div>     

            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Company Address <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="company_address" name="company_address" readonly="readonly" value="<?php echo $placement->company_address;?>">
                    </div>
                </div>

                <div class="col-sm-4">
                        <div class="form-group">
                            <label>Country <span class='error-text'>*</span></label>
                            <select name="id_country" id="id_country" class="form-control" disabled="disabled">
                                <option value="">Select</option>
                                <?php
                                if (!empty($countryList))
                                {
                                    foreach ($countryList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;  ?>"
                                            <?php 
                                            if($record->id == $placement->id_country)
                                            {
                                                echo "selected=selected";
                                            } ?>>
                                            <?php echo $record->name;  ?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>State <span class='error-text'>*</span></label>
                            <select name="id_state" id="id_state" class="form-control" disabled="disabled">
                                <option value="">Select</option>
                                <?php
                                if (!empty($stateList))
                                {
                                    foreach ($stateList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;  ?>"
                                            <?php 
                                            if($record->id == $placement->id_state)
                                            {
                                                echo "selected=selected";
                                            } ?>>
                                            <?php echo $record->name;  ?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

            </div>


             <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>City <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="city" name="city" readonly="readonly" value="<?php echo $placement->city;?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Zipcode <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="zipcode" name="zipcode" readonly="readonly" value="<?php echo $placement->zipcode;?>">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Joining Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="joining_dt" name="joining_dt" autocomplete="off" readonly="readonly" value="<?php echo date('d-m-Y', strtotime($placement->joining_dt));?>">
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Designation <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="designation" name="designation" readonly="readonly" value="<?php echo $placement->designation;?>">
                    </div>
                </div>

            </div>


        </div>


       

        <div class="button-block clearfix">
            <div class="bttn-group">

                <a href="../list" class="btn btn-link">Back</a>
            </div>
        </div>

        </form>

    </div>

        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
</div>


<script>

    $('select').select2();




  $(document).ready(function() {
        $("#form_pr_entry").validate({
            rules: {
                status: {
                    required: true
                },
                 reason: {
                    required: true
                }
            },
            messages: {
                status: {
                    required: "<p class='error-text'>Select Status",
                },
                reason: {
                    required: "<p class='error-text'>Reason Required",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    function showRejectField(){
            $("#view_reject").show();
    }

    function hideRejectField(){
            $("#view_reject").hide();
    }
</script>