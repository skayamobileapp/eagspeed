<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Partner University</h3>
        </div>
        <form id="form_award" action="" method="post">

         <div class="form-container">
            <h4 class="form-group-title">Partner University Details</h4>


            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $partnerUniversity->name; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Short Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="short_name" name="short_name" value="<?php echo $partnerUniversity->short_name; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name In Malay <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name_in_malay" name="name_in_malay" value="<?php echo $partnerUniversity->name_in_malay; ?>">
                    </div>
                </div>

                
            </div>

             <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Url <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="url" name="url" value="<?php echo $partnerUniversity->url; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Country <span class='error-text'>*</span></label>
                        <select name="id_country" id="id_country" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($countryList))
                            {
                                foreach ($countryList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>"
                                        <?php 
                                        if($record->id == $partnerUniversity->id_country)
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" <?php if($partnerUniversity->status=='1') {
                                 echo "checked=checked";
                              };?>><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0" <?php if($partnerUniversity->status=='0') {
                                 echo "checked=checked";
                              };?>>
                              <span class="check-radio"></span> In-Active
                            </label>                              
                        </div>                         
                </div>               
            </div>

        </div>
        

        <div class="form-container">
            <h4 class="form-group-title">Contact Details</h4>

              <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Contace Number <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="contact_number" name="contact_number" value="<?php echo $partnerUniversity->contact_number; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Contace Email <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="email" name="email" value="<?php echo $partnerUniversity->email; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Permanent Address <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="permanent_address" name="permanent_address" value="<?php echo $partnerUniversity->permanent_address; ?>">
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Correspondence Address <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="correspondence_address" name="correspondence_address" value="<?php echo $partnerUniversity->correspondence_address; ?>">
                    </div>
                </div>

            </div>

        </div>


        

        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="../list" class="btn btn-link">Cancel</a>
            </div>
        </div>


        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>
    $(document).ready(function() {
        $("#form_award").validate({
            rules: {
                name: {
                    required: true
                },
                short_name: {
                    required: true
                },
                name_in_malay: {
                    required: true
                },
                url: {
                    required: true
                },
                id_country: {
                    required: true
                },
                contact_number: {
                    required: true
                },
                permanent_address: {
                    required: true
                },
                correspondence_address: {
                    required: true
                },
                email: {
                    required: true
                },
                status: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>University Name Required</p>",
                },
                short_name: {
                    required: "<p class='error-text'>University Short Name Required</p>",
                },
                name_in_malay: {
                    required: "<p class='error-text'>University Name In Malay Required</p>",
                },
                url: {
                    required: "<p class='error-text'>URL Required</p>",
                },
                id_country: {
                    required: "<p class='error-text'>Select Country</p>",
                },
                contact_number: {
                    required: "<p class='error-text'>Contact Number Required</p>",
                },
                permanent_address: {
                    required: "<p class='error-text'>Permanent Address Required</p>",
                },
                correspondence_address: {
                    required: "<p class='error-text'>Correspondence Address Required</p>",
                },
                email: {
                    required: "<p class='error-text'>Contact Email Required</p>",
                },
                status: {
                    required: "<p class='error-text'>Status Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    $('select').select2();


</script>
<script>
  $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true
    });
  } );
</script>