<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ApelApproval extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('apel_approval_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('apel_approval.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['intakeList'] = $this->apel_approval_model->intakeList();
            $data['programList'] = $this->apel_approval_model->programList();


                $formData['first_name'] = $this->security->xss_clean($this->input->post('first_name'));
                $formData['email_id'] = $this->security->xss_clean($this->input->post('email_id'));
                $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
                $formData['id_program'] = $this->security->xss_clean($this->input->post('id_program'));
                $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
                $formData['applicant_status'] = $this->security->xss_clean($this->input->post('applicant_status'));

                
            $data['searchParam'] = $formData;

            $data['apelList'] = $this->apel_approval_model->apelList($formData);

            $this->global['pageTitle'] = 'Campus Management System : Employee Discount Approval';
            // echo "<Pre>";print_r($data['apelList']);exit;
            $this->loadViews("apel_approval/list", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('apel_approval.approve') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/admission/apelDiscountApproval/list');
            }

            $data['employeeDiscountDetails'] = $this->apel_approval_model->getApelDetails($id);

            $id_applicant = $data['employeeDiscountDetails']->id;
            // echo "<Pre>";print_r($id_applicant);exit();


            if($this->input->post())
            {

            // echo "<Pre>";print_r($this->input->post());exit();
                
            $id_user = $this->session->userId;
            $id_session = $this->session->session_id;


            
            $apel_status = $this->security->xss_clean($this->input->post('apeal_status'));
            $reason = $this->security->xss_clean($this->input->post('reason'));

            $is_apeal_applied = 2;
            
            if($apel_status == 8)
            {
                $is_apeal_applied = 1;
            }
            $data = array(
                    'is_apeal_applied' => $is_apeal_applied,
                    'id_apeal_status' => $apel_status,
                    'apel_reject_reason' => $reason
                );
                $result = $this->apel_approval_model->editApplicantDetails($data,$id);
                
                redirect('/admission/apelApproval/list');
            }
            
            $data['raceList'] = $this->apel_approval_model->raceList();
            $data['religionList'] = $this->apel_approval_model->religionList();
            $data['salutationList'] = $this->apel_approval_model->salutationListByStatus('1');
            $data['degreeTypeList'] = $this->apel_approval_model->qualificationList();
            $data['countryList'] = $this->apel_approval_model->countryList();
            $data['stateList'] = $this->apel_approval_model->stateList();
            $data['programList'] = $this->apel_approval_model->programList();
            $data['intakeList'] = $this->apel_approval_model->intakeList();
            $data['branchList'] = $this->apel_approval_model->branchListByStatus();
            $data['requiremntListList'] = $this->apel_approval_model->programRequiremntListList();
            $data['apelStatusList'] = $this->apel_approval_model->apelStatusList();
            $data['schemeList'] = $this->apel_approval_model->schemeListByStatus('1');
            $data['partnerUniversityList'] = $this->apel_approval_model->getUniversityListByStatus('1');
            $data['programStructureTypeList'] = $this->apel_approval_model->programStructureTypeListByStatus('1');

            $data['nationalityList'] = $this->apel_approval_model->nationalityListByStatus('1');
            
            $data['programEntryRequirementList'] = $this->apel_approval_model->programEntryRequirementList($data['employeeDiscountDetails']->id_program);
            $data['programDetails'] = $this->apel_approval_model->getProgramDetails($data['employeeDiscountDetails']->id_program);
            $data['applicantUploadedFiles'] = $this->apel_approval_model->getApplicantUploadedFiles($id);


            // echo "<Pre>";print_r($data['apelDiscountDetails']);exit();
            $this->global['pageTitle'] = 'Campus Management System : Edit Employee Discount Approval';
            $this->loadViews("apel_approval/edit", $this->global, $data, NULL);
        }
    }
}

