<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Employee_discount_model extends CI_Model
{
    function employeeList($name)
    {
        $this->db->select('ed.*');
        $this->db->from('employee_discount as ed');
         if (!empty($name)) {
            $likeCriteria = "(ed.name  LIKE '%" . $name . "%')";
            $this->db->where($likeCriteria);
        }

         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }


    function getEmployeeDiscountDetails($id)
    {
        $this->db->select('*');
        $this->db->from('employee_discount');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewEmployeeDiscount($data)
    {
        $this->db->trans_start();
        $this->db->insert('employee_discount', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editEmployeeDiscountDetails($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('employee_discount', $data);
        return $this->db->affected_rows();
    }
    
    // function deleteActivityDetails($id, $date)
    // {
    //     $this->db->where('id', $countryId);
    //     $this->db->update('academic_year', $countryInfo);
    //     return $this->db->affected_rows();
    // }

    function employeesList()
    {
        $this->db->select('*');
        $this->db->from('staff');
        $this->db->order_by("id", "DESC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }
}
