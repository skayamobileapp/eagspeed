<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Program_entry_requirement_model extends CI_Model
{
    function programList()
    {
        $this->db->select('p.*, a.code as award_code, a.name as award_name');
        $this->db->from('programme as p');
        $this->db->join('award as a', 'p.id_award = a.id');
        $this->db->order_by("p.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function programListBystatus($status)
    {
        $this->db->select('p.*, a.code as award_code, a.name as award_name');
        $this->db->from('programme as p');
        $this->db->join('award as a', 'p.id_award = a.id');
        $this->db->where('p.status', $status);
        $this->db->order_by("p.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function programEntryRequirementListSearch($data)
    {
        $this->db->select('a.*, p.name as program_name, p.code as program_code');
        $this->db->from('program_entry_requirement as a');
        $this->db->join('programme as p', 'a.id_program = p.id');
        if($data['id_program'] != '')
        {
            $this->db->where('a.id_program', $data['id_program']);
        }
        if($data['status'] != '')
        {
            $this->db->where('a.status', $data['status']);
        }
        if($data['name'] != '')
        {
            $likeCriteria = "(a.description  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("a.id", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function getEnteryRequirement($id)
    {
        $this->db->select('a.*, p.name as program_name, p.code as program_code');
        $this->db->from('program_entry_requirement as a');
        $this->db->join('programme as p', 'a.id_program = p.id');
        $this->db->where('a.id', $id);
        $query = $this->db->get();
         $result = $query->row();   
         return $result;
    }


    function groupListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('group_setup');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }


    function qualificationListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('qualification_setup');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }


    function specializationListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('specialization_setup');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function addTempRequirementDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('temp_entry_requirement_requirements', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getTempRequirementDetailsBySessionId($id_session)
    {
        $this->db->select('terr.*, gs.name as group_name, gs.code as group_code');
        $this->db->from('temp_entry_requirement_requirements as terr');
        $this->db->join('group_setup as gs', 'terr.id_group = gs.id');
        // $this->db->join('specialization_setup as ss', 'terr.id_specialization = ss.id');
        // $this->db->join('qualification_setup as qs', 'terr.id_qualification = qs.id');
        $this->db->where('terr.id_session', $id_session);
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function deleteTempRequirementDetails($id) {
        $this->db->where('id', $id);
        $this->db->delete('temp_entry_requirement_requirements');
        return TRUE;
    }

    function deleteTempRequirementDetailsBySession($id_session) {
        $this->db->where('id_session', $id_session);
        $this->db->delete('temp_entry_requirement_requirements');
        return TRUE;
    }




    function addTempOtherRequirementDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('temp_entry_requirement_other_requirements', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getTempOtherRequirementDetailsBySessionId($id_session)
    {
        $this->db->select('terr.*, gs.name as group_name, gs.code as group_code');
        $this->db->from('temp_entry_requirement_other_requirements as terr');
        $this->db->join('group_setup as gs', 'terr.id_group = gs.id');
        $this->db->where('terr.id_session', $id_session);
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function deleteTempOtherRequirementDetails($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('temp_entry_requirement_other_requirements');
        return TRUE;
    }

    function deleteTempOtherRequirementDetailsBySession($id_session) {
        $this->db->where('id_session', $id_session);
        $this->db->delete('temp_entry_requirement_other_requirements');
        return TRUE;
    }

    function addNewEntryRequirement($data)
    {
        $this->db->trans_start();
        $this->db->insert('program_entry_requirement', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editEntryRequirement($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('program_entry_requirement', $data);
        return TRUE;
    }


    function getTempRequirementDetails($id_session)
    {
        $this->db->select('terr.*');
        $this->db->from('temp_entry_requirement_requirements as terr');
        $this->db->where('terr.id_session', $id_session);
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }


    function getTempOtherRequirementDetailBySessionId($id_session)
    {
        $this->db->select('terr.*');
        $this->db->from('temp_entry_requirement_other_requirements as terr');
        $this->db->where('terr.id_session', $id_session);
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }


    function moveTempRequirementsToDetail($id_requirement_entry)
    {
        $id_session = $this->session->my_session_id;

        $temp_req_details = $this->getTempRequirementDetails($id_session);

        foreach ($temp_req_details as $temp_req_detail)
        {
           unset($temp_req_detail->id);
           unset($temp_req_detail->id_session);
           $temp_req_detail->id_requirement_entry = $id_requirement_entry;

           $inserted_req = $this->addRequirementDetails($temp_req_detail);
        }

        $deleted_temp_req = $this->deleteTempRequirementDetailsBySession($id_session);
    }

    function moveTempOtherRequirementsToDetail($id_requirement_entry)
    {
        $id_session = $this->session->my_session_id;

        $temp_other_req_details = $this->getTempOtherRequirementDetailBySessionId($id_session);

        foreach ($temp_other_req_details as $temp_other_req_detail)
        {
           unset($temp_other_req_detail->id);
           unset($temp_other_req_detail->id_session);
           $temp_other_req_detail->id_requirement_entry = $id_requirement_entry;

           $inserted_req = $this->addOtherRequirementDetails($temp_other_req_detail);
        }

        $deleted_temp_req = $this->deleteTempOtherRequirementDetailsBySession($id_session);
    }

    function addRequirementDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('entry_requirement_requirements', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }


    function addOtherRequirementDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('entry_requirement_other_requirements', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }


    function getRequirementDetailsByEntryId($id_requirement_entry)
    {
         $this->db->select('terr.*, gs.name as group_name, gs.code as group_code');
        $this->db->from('entry_requirement_requirements as terr');
        $this->db->join('group_setup as gs', 'terr.id_group = gs.id');
        // $this->db->join('specialization_setup as ss', 'terr.id_specialization = ss.id');
        // $this->db->join('qualification_setup as qs', 'terr.id_qualification = qs.id');
        $this->db->where('terr.id_requirement_entry', $id_requirement_entry);
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }


    function getOtherRequirementDetailByEntryId($id_requirement_entry)
    {
        $this->db->select('terr.*, gs.name as group_name, gs.code as group_code');
        $this->db->from('entry_requirement_other_requirements as terr');
        $this->db->join('group_setup as gs', 'terr.id_group = gs.id');
        $this->db->where('terr.id_requirement_entry', $id_requirement_entry);
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function deleteRequirementDetails($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('entry_requirement_requirements');
        return TRUE;
    }

    function deleteOtherRequirementDetails($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('entry_requirement_other_requirements');
        return TRUE;
    }

}