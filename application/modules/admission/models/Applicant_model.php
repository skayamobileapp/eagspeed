<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Applicant_model extends CI_Model
{
    function nationalityListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('nationality');
        $this->db->where('status', $status);
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

  function getApplicantDiscountDetailsByApplicant($id) {
         $this->db->select('*');
        $this->db->from('applicant_has_discount');
        $this->db->where('id_applicant', $id);
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }
      function getApplicantDiscountDetails($intake) {
        $this->db->select('d.id,dt.name,dt.description,d.start_date,d.end_date');
        $this->db->from('discount as d');
        $this->db->join('discount_type as dt','d.id_discount_type = dt.id','left');
        $this->db->where('d.id_intake', $intake);
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }


    function applicantListForApproval($data)
    {
        $status = 'Draft';
        $this->db->select('a.*, ahed.employee_status, ahad.alumni_status, ahsd.sibbling_status, p.code as program_code, p.name as program_name, inta.year as intake_year, inta.name as intake_name, pt.code as program_structure_code, pt.name as program_structure_name, train.name as training_center_name, train.code as training_center_code');
        $this->db->from('applicant as a');
        $this->db->join('programme as p', 'a.id_program = p.id','left');
        $this->db->join('intake as inta', 'a.id_intake = inta.id','left');
        $this->db->join('program_type as pt', 'a.id_program_structure_type = pt.id');
        $this->db->join('organisation_has_training_center as train', 'a.id_branch = train.id');
        $this->db->join('applicant_has_employee_discount as ahed', 'a.id = ahed.id_applicant','left');
        $this->db->join('applicant_has_alumni_discount as ahad', 'a.id = ahad.id_applicant','left');
        $this->db->join('applicant_has_sibbling_discount as ahsd', 'a.id = ahsd.id_applicant','left');

        if($data['first_name']) {
            $likeCriteria = "(a.full_name  LIKE '%" . $data['first_name'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($data['email_id']) {
            $likeCriteria = "(a.email_id  LIKE '%" . $data['email_id'] . "%')";
            $this->db->where($likeCriteria);
        }
        
         if($data['nric']) {
            $likeCriteria = "(a.nric  LIKE '%" . $data['nric'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($data['id_program']) {
            $likeCriteria = "(a.id_program  LIKE '%" . $data['id_program'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($data['id_intake']) {
            $likeCriteria = "(a.id_intake  LIKE '%" . $data['id_intake'] . "%')";
            $this->db->where($likeCriteria);
        }
        
        // $likeCriteria = "(a.is_sibbling_discount  != '0' and a.is_employee_discount  != '0' and a.is_alumni_discount  != '0')";
        // $this->db->where($likeCriteria);

        $this->db->where('a.applicant_status', $status);
        $this->db->where('a.is_updated', '1');
        $this->db->where('a.email_verified', '1');
        $this->db->where('a.is_submitted', '1');
        $likeCriteria = "(a.is_apeal_applied  = 3 or a.is_apeal_applied  = 1)";
        $this->db->where($likeCriteria);
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }



   function applicantList($data)
    {
        $this->db->select('a.*, p.code as program_code, p.name as program_name, inta.year as intake_year, inta.name as intake_name, el.name as education_level, train.name as training_center_name, train.code as training_center_code, pt.code as program_structure_code, pt.name as program_structure_name');
        $this->db->from('applicant as a');
        $this->db->join('programme as p', 'a.id_program = p.id');
        $this->db->join('intake as inta', 'a.id_intake = inta.id');
        $this->db->join('education_level as el', 'a.id_degree_type = el.id');
        $this->db->join('program_type as pt', 'a.id_program_structure_type = pt.id','left');
        $this->db->join('organisation_has_training_center as train', 'a.id_branch = train.id','left');
        if($data['first_name']) {
            $likeCriteria = "(a.full_name  LIKE '%" . $data['first_name'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($data['email_id']) {
            $likeCriteria = "(a.email_id  LIKE '%" . $data['email_id'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($data['nric']) {
            $likeCriteria = "(a.nric  LIKE '%" . $data['nric'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($data['id_program']) {
            $likeCriteria = "(a.id_program  LIKE '%" . $data['id_program'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($data['id_intake']) {
            $likeCriteria = "(a.id_intake  LIKE '%" . $data['id_intake'] . "%')";
            $this->db->where($likeCriteria);
        }
        if($data['applicant_status']) {
            $likeCriteria = "(a.applicant_status  LIKE '%" . $data['applicant_status'] . "%')";
            $this->db->where($likeCriteria);
        }
        // $this->db->where('a.is_submitted', '1');
        // $likeCriteria = "(a.is_apeal_applied  = 3 or a.is_apeal_applied  = 1)";
        // $this->db->where($likeCriteria);

        $this->db->where('a.is_updated', '1');
        $this->db->where('a.email_verified', '1');
        // $this->db->where('el.name !=', 'POSTGRADUATE');
        $this->db->order_by("a.id", "DESC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function intakeList()
    {
        $this->db->select('*');
        $this->db->from('intake');
        $this->db->where('status', '1');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function nationalityList()
    {
        $this->db->select('*');
        $this->db->from('nationality');
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function programList()
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function programListForPostgraduate($name)
    {
        $this->db->select('p.*');
        $this->db->from('programme as p');
        $this->db->join('education_level as el', 'p.id_education_level = el.id');
        $this->db->where('el.name !=', $name);
        $this->db->where('p.status', '1');
        $this->db->order_by("p.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function salutationListByStatus($status)
    {
        $this->db->select('a.*');
        $this->db->from('salutation_setup as a');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function raceListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('race_setup');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function programStructureTypeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('program_type');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result; 
    }


    function religionListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('religion_setup');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function programListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function getApplicantDetailsById($id)
    {
        $this->db->select('*');
        $this->db->from('applicant');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $sd = $query->row();
        return $sd;
    }

      function getProgramSchemeByProgramId($id_programme)
    {
        $this->db->select('ihs.*');
        $this->db->from('programme_has_scheme as ihs');
        $this->db->where('ihs.id_program', $id_programme);
        $query = $this->db->get();
        return $query->result();
    }
     function getProgramStructureType($id)
    {
        $this->db->select('shd.*');
        $this->db->from('program_type as shd');
        $this->db->where('shd.id', $id);
        $query = $this->db->get();
        return $query->row();
    }
       function getSchemeByProgramId($id_programme)
    {
        // echo "<Pre>"; print_r($id_programme);exit;
        $this->db->select('DISTINCT(phs.id_scheme) as id_scheme');
        $this->db->from('program_has_scheme as phs');
        $this->db->join('scheme as sch', 'phs.id_scheme = sch.id');
        $this->db->where('phs.id_program', $id_programme);
        $query = $this->db->get();
        $results = $query->result();
            
        // echo "<Pre>"; print_r($results);exit;

        $details = array();

        foreach ($results as $result)
        {

            $id_scheme = $result->id_scheme;

            $scheme = $this->getScheme($id_scheme);
            if($scheme)
            {
                $result = $scheme;
                array_push($details, $result);
            }
        }

        return $details;
    }

     function getProgramByPartnerUniversity($id_organisation)
    {
        $this->db->select('p.*');
        $this->db->from('programme as p');
        $this->db->where('p.id_partner_university', $id_organisation);
        $query = $this->db->get();
        $results = $query->result();
        return $results;
    }

    function getTrainingCenterById($id)
    {
        $this->db->select('a.*');
        $this->db->from('organisation_has_training_center as a');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $result = $query->row();  
        return $result;
    }

      function getBranchesByPartnerUniversity($id_organisation)
    {
        $this->db->select('ahemd.*');
        $this->db->from('organisation_has_training_center as ahemd');
        $this->db->where('ahemd.id_organisation', $id_organisation);
        $query = $this->db->get();
        $results = $query->result();
        return $results;
    }


  function programAppelRequirementList($id_program)
    {
        $this->db->select('ier.*, qs.code as qualification_code, qs.name as qualification_name, ws.code as work_code, ws.name as work_name');
        $this->db->from('scholarship_individual_entry_requirement as ier');
        $this->db->join('education_level as qs','ier.id_education_qualification = qs.id','left');
        $this->db->join('scholarship_work_specialisation as ws','ier.id_work_specialisation = ws.id','left');
        $this->db->where('ier.id_program', $id_program);
        $this->db->where('ier.entry_type', 'APPEL');
        $query = $this->db->get();
        return $query->result();
    }



function programDetails($id_program)
    {
        $this->db->select('a.*');
        $this->db->from('programme as a');
        $this->db->where('a.id', $id_program);
        $query = $this->db->get();
        $result = $query->row(); 

        return $result;
    }



    function getScheme($id_scheme)
    {
        $this->db->select('ihs.*');
        $this->db->from('scheme as ihs');
        $this->db->where('ihs.id', $id_scheme);
        $query = $this->db->get();
        return $query->row();
    }

    function getProgramScheme($id_program_scheme)
    {
        $this->db->select('ihs.*');
        $this->db->from('programme_has_scheme as ihs');
        $this->db->where('ihs.id', $id_program_scheme);
        $query = $this->db->get();
        return $query->row();
    }

     function getProgramSchemeBySchemeAndProgramId($scheme_id,$idprogram)
    {
        $this->db->select('ihs.*');
        $this->db->from('programme_has_scheme as ihs');
        $this->db->where('ihs.id', $id_program_scheme);
        $query = $this->db->get();
        return $query->row();
    }


     function getProgramStructureTypeByProgramId($id_programme)
    {
        $this->db->select('DISTINCT(tphd.id_program_type) as id_program_type');
        $this->db->from('programme_has_scheme as tphd');
        $this->db->where('tphd.id_program', $id_programme);
        $query = $this->db->get();
        $results = $query->result();

        // echo "<Pre>";print_r($results);exit();

        $details = array();

        foreach ($results as $result)
        {
            $id_program_type = $result->id_program_type;
            $program_structure_type = $this->getProgramStructureType($id_program_type);

            if($program_structure_type)
            {
                array_push($details, $program_structure_type);
            }
        }
        
        return  $details;
    }




      function getProgrammeByEducationLevelId($id_education_level)
    {
        $this->db->select('ihs.*');
        $this->db->from('programme as ihs');
        // $this->db->join('intake as i', 'ihs.id_intake = i.id');
        $this->db->where('ihs.id_education_level', $id_education_level);
        $this->db->where('ihs.status', 1);
        $query = $this->db->get();
        return $query->result();
    }

  



    function qualificationListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('education_level');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function qualificationList()
    {
        $this->db->select('*');
        $this->db->from('education_level');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function getApplicantDetails($id)
    {
        $this->db->select('*');
        $this->db->from('applicant');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $sd = $query->row();
        if($sd->sibbling_discount== "Yes" && $sd->employee_discount== "No"){

            $this->db->select('a.*, sd.sibbling_name, sd.sibbling_nric');
            $this->db->from('applicant as a');
            $this->db->join('applicant_has_sibbling_discount as sd', 'a.id = sd.id_applicant');
            $this->db->where('a.id', $id);
            $query = $this->db->get();
            // $d = $query->row();
            // print_r($d);exit();
            return $query->row();
        }
        else
        if($sd->employee_discount== "Yes" && $sd->sibbling_discount== "No"){

            $this->db->select('a.*, ed.employee_name, ed.employee_nric, ed.employee_designation');
            $this->db->from('applicant as a');
            $this->db->join('applicant_has_employee_discount as ed', 'a.id = ed.id_applicant');
            $this->db->where('a.id', $id);
            $query = $this->db->get();
            // print_r($query);exit();
            return $query->row();
        }
        else
         if($sd->employee_discount== "Yes" && $sd->sibbling_discount== "Yes"){

            $this->db->select('a.*, sd.sibbling_name, sd.sibbling_nric, ed.employee_name, ed.employee_nric, ed.employee_designation');
            $this->db->from('applicant as a');
            $this->db->join('applicant_has_sibbling_discount as sd', 'a.id = sd.id_applicant');
            $this->db->join('applicant_has_employee_discount as ed', 'a.id = ed.id_applicant');
            $this->db->where('a.id', $id);
            $query = $this->db->get();
            // print_r($query);exit();
            return $query->row();
        }
        else{
            $this->db->select('a.*');
            $this->db->from('applicant as a');
            $this->db->where('a.id', $id);
            $query = $this->db->get();
            return $query->row();
        }
    }
    
    function addNewApplicant($data)
    {
        $this->db->trans_start();
        $this->db->insert('applicant', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addNewSibblingDiscount($data)
    {
        $this->db->trans_start();
        $this->db->insert('applicant_has_sibbling_discount', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }


     function addFileDownload($data)
    {
        $this->db->trans_start();
        $this->db->insert('applicant_has_document', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }


    function addNewEmployeeDiscount($data)
    {
        $this->db->trans_start();
        $this->db->insert('applicant_has_employee_discount', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editApplicantDetails($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('applicant', $data);
        return TRUE;
    }

    function updateFileApplicant($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('applicant_has_document', $data);
        return TRUE;
    }

    function editSibblingDetails($data, $id)
    {
        $this->db->where('id_applicant', $id);
        $this->db->update('applicant_has_sibbling_discount', $data);
        return TRUE;
    }

    function editEmployeeDetails($data, $id)
    {
        $this->db->where('id_applicant', $id);
        $this->db->update('applicant_has_employee_discount', $data);
        return TRUE;
    }

    function stateList()
    {
        $this->db->select('*');
        $this->db->from('state');
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();
        //echo "<Pre>"; print_r($result);exit;
        return $result;
    }

    function countryListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('country');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();      
         return $result;
    }

    function getStateByCountryId($id_country)
    {
        $this->db->select('*');
        $this->db->from('state');
        $this->db->where('id_country', $id_country);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getIntakeByProgrammeId($id_programme)
    {
        $this->db->select('DISTINCT(i.id) as id, i.*');
        $this->db->from('intake_has_programme as ihs');
        $this->db->join('intake as i', 'ihs.id_intake = i.id');
        $this->db->where('ihs.id_programme', $id_programme);
        $query = $this->db->get();
        return $query->result();
    }


   

    function checkFeeStructure($data)
    {
        $this->db->select('*');
        $this->db->from('fee_structure');
        $this->db->where('id_intake', $data['id_intake']);
        $this->db->where('id_programme', $data['id_program']);
        $this->db->where('status', '1');
        $query = $this->db->get();
        // print_r($query->row());exit();
        return $query->row();
    }

    function branchListByStatus()
    {
        $organisation = $this->getOrganisaton();
        $details = array();

        if($organisation)
        {
            array_push($details, $organisation);
        }


        $this->db->select('a.*');
        $this->db->from('organisation_has_training_center as a');
        $query = $this->db->get();
        $result = $query->result();  

        foreach ($result as $value)
        {
           array_push($details, $value);
        }
        return $details;
    }

    function getOrganisaton()
    {
        $this->db->select('a.*, a.short_name as code');
        $this->db->from('organisation as a');
        $query = $this->db->get();
        $result = $query->row();  
        return $result;
    }

    function programRequiremntListList()
    {
        $this->db->select('ier.*, qs.code as qualification_code, qs.name as qualification_name, ws.code as work_code, ws.name as work_name');
        $this->db->from('scholarship_individual_entry_requirement as ier');
        $this->db->join('education_level as qs','ier.id_education_qualification = qs.id','left');
        $this->db->join('scholarship_work_specialisation as ws','ier.id_work_specialisation = ws.id','left');
        $query = $this->db->get();
        return $query->result();
    }

    function getIntakeDetails($id)
    {
        $this->db->select('a.*');
        $this->db->from('intake as a');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $result = $query->row();  
        return $result;
    }

    function getProgramDetails($id_program)
    {
        $this->db->select('ihs.*');
        $this->db->from('programme as ihs');
        $this->db->where('ihs.id', $id_program);
        $query = $this->db->get();
        return $query->row();
    }

    function programEntryRequirementList($id_program)
    {
        $this->db->select('ier.*, qs.code as qualification_code, qs.name as qualification_name, ws.code as work_code, ws.name as work_name');
        $this->db->from('scholarship_individual_entry_requirement as ier');
        $this->db->join('education_level as qs','ier.id_education_qualification = qs.id','left');
        $this->db->join('scholarship_work_specialisation as ws','ier.id_work_specialisation = ws.id','left');
        $this->db->where('ier.id_program', $id_program);
        $query = $this->db->get();
        return $query->result();
    }
    
    // function deleteActivityDetails($id, $date)
    // {
    //     $this->db->where('id', $countryId);
    //     $this->db->update('academic_year', $countryInfo);
    //     return $this->db->affected_rows();
    // }

    function getApplicantUploadedFiles($id_applicant)
    {
        $this->db->select('shd.*, d.code as document_code, d.name as document_name');
        $this->db->from('applicant_has_document as shd');
        $this->db->join('documents as d','shd.id_document = d.id');
        $this->db->where('shd.id_applicant', $id_applicant);
        $query = $this->db->get();
        return $query->result();
    }

    function schemeListByStatus($status)
    {
        $this->db->select('ihs.*');
        $this->db->from('scheme as ihs');
        $this->db->where('ihs.status', $status);
        $query = $this->db->get();
        return $query->result();
    }

    function getUniversityListByStatus($status)
    {
        $organisation = $this->getOrganisaton();
        $details = array();

        if($organisation)
        {
            array_push($details, $organisation);
        }

        $this->db->select('ahemd.*');
        $this->db->from('partner_university as ahemd');
        $this->db->where('ahemd.status', $status);
        $query = $this->db->get();
        $results = $query->result();

        foreach ($results as $result)
        {
            array_push($details, $result);
        }
        return $details;
    }

    function getApplicantSibblingDiscountDetails($id_applicant)
    {
        $this->db->select('ahsd.*, usr.name as user_name');
        $this->db->from('applicant_has_sibbling_discount as ahsd');
        $this->db->join('tbl_users as usr', 'ahsd.rejected_by = usr.userId','left'); 
        $this->db->where('id_applicant', $id_applicant);
        $query = $this->db->get();
        return $query->row();
    }

    function getApplicantEmployeeDiscountDetails($id_applicant)
    {
        $this->db->select('ahemd.*, usr.name as user_name');
        $this->db->from('applicant_has_employee_discount as ahemd');
        $this->db->join('tbl_users as usr', 'ahemd.rejected_by = usr.userId','left'); 
        $this->db->where('id_applicant', $id_applicant);
        $query = $this->db->get();
        return $query->row();
    }

    function getApplicantAlumniDiscountDetails($id_applicant)
    {
        $this->db->select('ahemd.*, usr.name as user_name');
        $this->db->from('applicant_has_alumni_discount as ahemd');
        $this->db->join('tbl_users as usr', 'ahemd.rejected_by = usr.userId','left'); 
        $this->db->where('id_applicant', $id_applicant);
        $query = $this->db->get();
        return $query->row();
    }   

    function createNewMainInvoiceForStudent($id)
    {
        $id_student = $id;
        $user_id = $this->session->userId;

        $this->db->select('*');
        $this->db->from('applicant');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $applicant_data = $query->row();

        // echo "<Pre>";print_r($applicant_data);exit;

        $id_applicant = $applicant_data->id;
        $id_branch = $applicant_data->id_branch;
        $id_university = $applicant_data->id_university;




        $id_program = $applicant_data->id_program;
        $id_intake = $applicant_data->id_intake;
        $nationality = $applicant_data->nationality;
        $id_program_scheme = $applicant_data->id_program_scheme;
        $id_program_has_scheme = $applicant_data->id_program_has_scheme;
        $id_program_landscape = $applicant_data->id_program_landscape;
        $is_sibbling_discount = $applicant_data->is_sibbling_discount;
        $is_employee_discount = $applicant_data->is_employee_discount;
        $is_alumni_discount = $applicant_data->is_alumni_discount;

        // echo "<Pre>";print_r($id_program_landscape);exit;


        $is_installment = 0;
        $installments = 0;
        $currency = 'MYR';

        if($id_university != 1)
        {
            $fee_structure_training_data = $this->getFeeStructureForTrainingCenterInstallment($id_program,$id_intake,$id_program_has_scheme,$id_university);

        // echo "<Pre>";print_r($fee_structure_training_data);exit;

            if($fee_structure_training_data)
            {
                if($fee_structure_training_data->is_installment != '')
                {
                    $is_installment = $fee_structure_training_data->is_installment;
                }

                if($fee_structure_training_data->installments != '')
                {
                    $installments = $fee_structure_training_data->installments;
                }
            }
        }
        // echo "<Pre>";print_r($fee_structure_training_data);exit;

        // echo "<Pre>";print_r($is_installment);exit;


        if($id_university == 1 && $is_installment == 0 && $installments == 0)
        {

            if($nationality == '1')
            {
                $currency = 'MYR';
                $detail_data = $this->getFeeStructureForLocalStudentsForLandscape($id_program,$id_intake,$id_program_landscape,'MYR');
            }
            elseif($nationality != '')
            {
                $currency = 'USD';
                $detail_data = $this->getFeeStructureForLocalStudentsForLandscape($id_program,$id_intake,$id_program_landscape,'USD');
            }
        }




        if($id_university > 1 && $is_installment == 0 && $installments == 0)
        {

            // if($nationality == 'Malaysian')
            // {

                if($fee_structure_training_data)
                {
                    $currency = $fee_structure_training_data->currency;
                }

                $detail_data = $this->getFeeStructureByTrainingCenterForInvoiceGeneration($id_program,$id_intake,$id_program_scheme,$id_university);
            // }
            // elseif($nationality == 'Other')
            // {
            //     $currency = 'USD';
            //     $detail_data = $this->getFeeStructureByTrainingCenterForInvoiceGeneration($id_program,$id_intake,$id_program_scheme,'USD');
            // }
        }

        // echo "<Pre>";print_r($detail_data);exit;
        // echo "<Pre>";print_r($is_employee_discount);exit;


        if($is_installment == 0 && $installments == 0)
        {




            $invoice_number = $this->generateMainInvoiceNumber();

            $invoice['invoice_number'] = $invoice_number;
            $invoice['type'] = 'Applicant';
            $invoice['remarks'] = 'Application Registration Fee';
            $invoice['id_application'] = '1';
            $invoice['id_student'] = $id_student;
            $invoice['id_program'] = $id_program;
            $invoice['id_intake'] = $id_intake;
            $invoice['currency'] = $currency;
            $invoice['total_amount'] = '0';
            $invoice['balance_amount'] = '0';
            $invoice['paid_amount'] = '0';
            $invoice['status'] = '1';
            $invoice['created_by'] = $user_id;

            // $detail_data = $this->getFeeStructure('13','5');

            
            // echo "<Pre>";print_r($invoice);exit;
            $inserted_id = $this->addNewMainInvoice($invoice);



            $total_amount = 0;
            $total_discount_amount = 0;
            $sibling_discount_amount = 0;
            $employee_discount_amount = 0;
            $alumni_discount_amount = 0;


            // echo "<Pre>";print_r($detail_data);exit;

            foreach ($detail_data as $fee_structure)
            {
                $data = array(
                        'id_main_invoice' => $inserted_id,
                        'id_fee_item' => $fee_structure->id_fee_item,
                        'amount' => $fee_structure->amount,
                        'price' => $fee_structure->amount,
                        'quantity' => 1,
                        'id_reference' => $id_student,
                        'description' => 'Application Registration Fee',
                        'status' => 1,
                        'created_by' => $user_id
                    );
                $total_amount = $total_amount + $fee_structure->amount;

                // echo "<Pre>";print_r($data);exit;

                $this->addNewMainInvoiceDetails($data);

            }

            $total_invoice_amount = $total_amount;

            if($is_sibbling_discount == '1')
            {
                $this->db->select('*');
                $this->db->from('sibbling_discount');
                // $likeCriteria = "(date(start_date)  <= '" . date('Y-m-d') . "')";
                // $this->db->where($likeCriteria);
                // $likeCriteria = "(date(end_date)  <= '" . date('Y-m-d') . "')";
                // $this->db->where($likeCriteria);

            //     $SQL = "Select * From sibbling_discount where date(start_date) <= 'getdate()' and date(end_date) >= 'getdate()' order by id DESC limit 0,1";
            //     $query = $this->db->query($SQL);
               
            // echo "<Pre>";print_r($query->row());exit;

                $this->db->where('currency', $currency);
                $this->db->where('status', '1');
                $query = $this->db->get();
                $sibling_discount_data = $query->row();

                if($sibling_discount_data)
                {
                    $amount = $sibling_discount_data->amount;
                    $id_discount = $sibling_discount_data->id;

                    $sibling_insert = array(
                        'id_main_invoice' => $inserted_id,
                        'id_student' => $id_student,
                        'name' => 'Sibbling Discount Applied',
                        'amount' => $amount,
                        'id_reference' => $id_discount,
                    );
                    $discount_inserted_id = $this->addNewMainInvoiceDiscountDetail($sibling_insert);
                    if($discount_inserted_id)
                    {
                        $total_amount = $total_amount - $sibling_discount_data->amount;
                        $sibling_discount_amount = $sibling_discount_data->amount;
                    }
                }

            // echo "<Pre>";print_r($amount);exit;
            }

            if($is_employee_discount == '1')
            {

                $this->db->select('*');
                $this->db->from('employee_discount');
                // $this->db->where(date('Y-m-d').' BETWEEN  date(start_date) and date(end_date)');
                // $this->db->where('date(start_date) >=', date('Y-m-d'));
                // $this->db->where('date(end_date) <=', date('Y-m-d'));
                $this->db->where('currency', $currency);
                $this->db->where('status', '1');
                $query = $this->db->get();
                $employee_discount_data = $query->row();
                if($employee_discount_data)
                {
                    $amount = $employee_discount_data->amount;
                    $id_discount = $employee_discount_data->id;

                    $employee_insert = array(
                        'id_main_invoice' => $inserted_id,
                        'id_student' => $id_student,
                        'name' => 'Employee Discount Applied',
                        'amount' => $amount,
                        'id_reference' => $id_discount,
                    );
                    $sibbling_inserted_id = $this->addNewMainInvoiceDiscountDetail($employee_insert);
                    if($sibbling_inserted_id)
                    {
                        $total_amount = $total_amount - $employee_discount_data->amount;
                        $employee_discount_amount = $employee_discount_data->amount;
                    }
                }
            }




            if($is_alumni_discount == '1')
            {

                $this->db->select('*');
                $this->db->from('alumni_discount');
                // $this->db->where(date('Y-m-d').' BETWEEN  date(start_date) and date(end_date)');
                // $this->db->where('date(start_date) >=', date('Y-m-d'));
                // $this->db->where('date(end_date) <=', date('Y-m-d'));
                $this->db->where('currency', $currency);
                $this->db->where('status', '1');
                $query = $this->db->get();
                $alumni_discount_data = $query->row();
                if($alumni_discount_data)
                {
                    $amount = $alumni_discount_data->amount;
                    $id_discount = $alumni_discount_data->id;

                    $employee_insert = array(
                        'id_main_invoice' => $inserted_id,
                        'id_student' => $id_student,
                        'name' => 'Alumni Discount Applied',
                        'amount' => $amount,
                        'id_reference' => $id_discount,
                    );
                    $alumni_inserted_id = $this->addNewMainInvoiceDiscountDetail($employee_insert);
                    if($alumni_inserted_id)
                    {
                        $total_amount = $total_amount - $alumni_discount_data->amount;
                        $alumni_discount_amount = $alumni_discount_data->amount;
                    }
                }
            }


            $total_discount_amount = $sibling_discount_amount + $employee_discount_amount + $alumni_discount_amount;
            // $total_amount = number_format($total_amount, 2, '.', ',');
            // echo "<Pre>";print_r($total_amount);exit;

            $invoice_update['total_amount'] = $total_amount;
            $invoice_update['balance_amount'] = $total_amount;
            $invoice_update['invoice_total'] = $total_invoice_amount;
            $invoice_update['total_discount'] = $total_discount_amount;
            $invoice_update['paid_amount'] = '0';
            // $invoice_update['inserted_id'] = $inserted_id;
            // echo "<Pre>";print_r($invoice_update);exit;
            $this->editMainInvoice($invoice_update,$inserted_id);
        }

        return TRUE;
    }

    function addNewMainInvoice($data)
    {
        $this->db->trans_start();
        $this->db->insert('main_invoice', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function addNewMainInvoiceDiscountDetail($data)
    {
        $this->db->trans_start();
        $this->db->insert('main_invoice_discount_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function addNewMainInvoiceDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('main_invoice_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        // return $insert_id;
    }

    function editMainInvoice($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('main_invoice', $data);
        return TRUE;
    }

    function generateMainInvoiceNumber()
    {
        $year = date('y');
        $Year = date('Y');
            $this->db->select('j.*');
            $this->db->from('main_invoice as j');
            $this->db->order_by("id", "desc");
            $query = $this->db->get();
            $result = $query->num_rows();

     
            $count= $result + 1;
           $jrnumber = $number = "INV" .(sprintf("%'06d", $count)). "/" . $Year;
           return $jrnumber;        
    }

    function getFeeStructureByTrainingCenterForInvoiceGeneration($id_programme,$id_intake,$id_program_scheme,$id_training_center)
    {
       $this->db->select('p.*');
        $this->db->from('fee_structure as p');
        $this->db->join('fee_setup as fs', 'p.id_fee_item = fs.id'); 
        $this->db->join('frequency_mode as fm', 'fs.id_frequency_mode = fm.id'); 
        $this->db->where('p.id_programme', $id_programme);
        $this->db->where('p.id_intake', $id_intake);
        $this->db->where('p.id_program_scheme', $id_program_scheme);
        $this->db->where('p.id_training_center', $id_training_center);
        $query = $this->db->get();
        $fee_structure = $query->result();

        return $fee_structure;
    }



    function getFeeStructureForTrainingCenterInstallment($id_programme,$id_intake,$id_program_scheme,$id_training_center)
    {
       $this->db->select('p.*');
        $this->db->from('fee_structure as p');
        $this->db->join('fee_setup as fs', 'p.id_fee_item = fs.id','left'); 
        $this->db->join('frequency_mode as fm', 'fs.id_frequency_mode = fm.id','left'); 
        $this->db->where('p.id_programme', $id_programme);
        $this->db->where('p.id_intake', $id_intake);
        $this->db->where('p.id_program_scheme', $id_program_scheme);
        $this->db->where('p.id_training_center', $id_training_center);
        $this->db->order_by("p.id", "desc");
        $query = $this->db->get();
        $fee_structure = $query->row();

        return $fee_structure;
    }

    function getFeeStructureForLocalStudentsForLandscape($id_programme,$id_intake,$id_program_landscape,$currency)
    {
       $this->db->select('p.*');
        $this->db->from('fee_structure as p');
        $this->db->join('fee_setup as fs', 'p.id_fee_item = fs.id'); 
        $this->db->join('frequency_mode as fm', 'fs.id_frequency_mode = fm.id'); 
        $this->db->where('p.id_programme', $id_programme);
        $this->db->where('p.id_intake', $id_intake);
        $this->db->where('p.id_program_landscape', $id_program_landscape);
        // $this->db->where('p.id_program_scheme', $id_program_scheme);
        // $this->db->where('p.id_training_center', 0);
        $this->db->where('p.currency', $currency);
        // $this->db->where('fm.code', 'ONE TIME');
        // $this->db->or_where('fm.code', 'APPLICATION');
        $query = $this->db->get();
        $fee_structure = $query->result();
        // $detail_data = $this->getFeeStructureDetails($fee_structure->id);
        return $fee_structure;
    }

    function programModeList()
    {
        $this->db->select('*');
        $this->db->from('mode_of_program');
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function getReceiptStatus($id_applicant)
    {
        $type = "Applicant";
        $zero= "0";
        $one= "1";
        $this->db->select('*');
        $this->db->from('receipt');
        // $likeCriteria = "(id_student  = '" . $id_applicant . "' and type  ='" . $type . "' and (status  ='" . $zero . "' or status  ='" . $one . "'))";
        $likeCriteria = "(id_student  = '" . $id_applicant . "' and type  ='" . $type . "')";
        $this->db->where($likeCriteria);

        // $this->db->where('id_student', $id_applicant);
        // $this->db->where('status', '0');
        // $this->db->or_where('status', '1');
        $query = $this->db->get();
        return $query->row();
    }

    function getfeeStructureMasterByApplicantID($id_applicant)
    {
        $applicant = $this->getApplicantDetailsById($id_applicant);
        
        // echo "<Pre>";print_r($applicant);exit();

        $this->db->select('*');
        $this->db->from('fee_structure_master');
        $this->db->where('id_education_level', $applicant->id_degree_type);
        $this->db->where('id_intake', $applicant->id_intake);
        $this->db->where('id_programme', $applicant->id_program);
        $this->db->where('id_learning_mode', $applicant->id_program_scheme);
        // $this->db->where('id_program_scheme', $applicant->id_program_has_scheme);
        $this->db->where('id', $applicant->id_fee_structure);
        $this->db->where('status', '1');
        $query = $this->db->get();

        $result = $query->row();
        // echo "<Pre>";print_r($result);exit();
        
        if($result)
        {
            return $result->id;
        }
        else
        {
            return 0;
        }
    }

    function applicantInvoice($id_applicant)
    {

        $this->db->select('fst.*');
        $this->db->from('main_invoice as fst');  
        $this->db->join('applicant as fm', 'fst.id_student = fm.id','left'); 
        $this->db->where('fst.id_student', $id_applicant);
        $this->db->where('fst.type', 'Applicant');
        $this->db->where('fst.is_migrate_applicant !=', 0);
        $this->db->where('fst.status', '1');
        $query = $this->db->get();

        $result = $query->row();
        // echo "<Pre>";print_r($applicant);exit();
        // echo "<Pre>";print_r($result);exit();

        return $result;
    }

    function applicantInvoiceDetails($id_main_invoice)
    {
        $this->db->select('fst.*, fs.name as fee_structure, fs.code as fee_structure_code, fm.name as frequency_mode ');
        $this->db->from('main_invoice_details as fst');  
        $this->db->join('fee_setup as fs', 'fst.id_fee_item = fs.id','left');
        $this->db->join('frequency_mode as fm', 'fs.id_frequency_mode = fm.id','left'); 
        $this->db->where('fst.id_main_invoice', $id_main_invoice);
        $this->db->where('fst.status', '1');
        $query = $this->db->get();

        $result = $query->result();

        return $result;
    }

    function getMainInvoiceDiscountDetails($id)
    {
        $this->db->select('*');
        $this->db->from('main_invoice_discount_details');   
        $this->db->where('id_main_invoice', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function getfeeStructureMasterByApplicant($id_applicant)
    {
        $applicant = $this->getApplicantDetailsById($id_applicant);

        $id_university = $applicant->id_university;
        
        // echo "<Pre>";print_r($id_university);exit();

        $this->db->select('*');
        $this->db->from('fee_structure_master');
        $this->db->where('id_education_level', $applicant->id_degree_type);
        $this->db->where('id_intake', $applicant->id_intake);
        $this->db->where('id_programme', $applicant->id_program);
        $this->db->where('id_learning_mode', $applicant->id_program_scheme);
        // $this->db->where('id_program_scheme', $applicant->id_program_has_scheme);
        // $this->db->where('id_partner_university', $applicant->id_university);
        $this->db->where('status', '1');
        $query = $this->db->get();

        $result = $query->row();

        // echo "<Pre>";print_r($result);exit();


        if($result)
        {
            $id_fee_structure = $result->id;
            
                $nationality = $applicant->nationality;
                $currency = 'MYR';

                if($nationality == '1')
                {
                    $currency = 'MYR';
                }
                else
                {
                    $currency = 'USD';
                }

                // echo "<Pre>";print_r($nationality);exit();

                $fee_structure = $this->getfeeStructureDetailsByIdFeeStructureMaster($id_fee_structure,$currency);
                
                // echo "<Pre>";print_r($fee_structure);exit();
                
                return $fee_structure;
            
        }
        else
        {
            return array();
        }
    }

    function getfeeStructureDetailsByIdFeeStructureMaster($id_fee_structure_master,$currency)
    {

        $this->db->select('fst.*, fs.name as fee_structure, fs.code as fee_structure_code, fm.name as frequency_mode, fstp.name as trigger_name, cs.name as currency_name');
        $this->db->from('fee_structure as fst');
        $this->db->join('fee_setup as fs', 'fst.id_fee_item = fs.id','left');   
        $this->db->join('frequency_mode as fm', 'fs.id_frequency_mode = fm.id','left'); 
        $this->db->join('fee_structure_triggering_point as fstp', 'fst.id_fee_structure_trigger = fstp.id','left'); 
        $this->db->join('currency_setup as cs', 'fst.currency = cs.id','left'); 
        $this->db->where('fst.id_program_landscape', $id_fee_structure_master);
        $this->db->where('fst.currency', $currency);
        $this->db->where('fst.status', '1');
        $query = $this->db->get();

        $result = $query->result();
        // echo "<Pre>";print_r($result);exit();
        return $result;
    }

    function getfeeStructureDetailsByIdFeeStructureMasterForPartneruniversity($id_fee_structure_master,$id_university)
    {
        $this->db->select('fst.*, fs.name as fee_structure, fs.code as fee_structure_code, fm.name as frequency_mode, fstp.name as trigger_name, cs.name as currency_name');
        $this->db->from('fee_structure as fst');
        $this->db->join('fee_setup as fs', 'fst.id_fee_item = fs.id','left');   
        $this->db->join('frequency_mode as fm', 'fs.id_frequency_mode = fm.id','left'); 
        $this->db->join('fee_structure_triggering_point as fstp', 'fst.id_fee_structure_trigger = fstp.id','left'); 
        // $this->db->join('fee_structure as fss', 'fst.id_fee_structure = fss.id','left'); 
        $this->db->join('currency_setup as cs', 'fst.currency = cs.id','left'); 
        $this->db->where('fst.id_program_landscape', $id_fee_structure_master);
        $this->db->where('fst.id_training_center', $id_university);
        $this->db->where('fst.status', '1');
        $query = $this->db->get();

        $result = $query->result();
        // echo "<Pre>";print_r($result);exit();
        
        return $result;
    }


    function getTrainingCenterInstallmentDetails($data)
    {
        $this->db->select('p.*, sem.name as fee_name, sem.code as fee_code, fm.name as frequency_mode, fstp.name as trigger_name');
        $this->db->from('fee_structure_has_training_center as p');
        $this->db->join('fee_setup as sem', 'p.id_fee_item = sem.id');
        $this->db->join('frequency_mode as fm', 'sem.id_frequency_mode = fm.id');
        $this->db->join('fee_structure_triggering_point as fstp', 'p.id_fee_structure_trigger = fstp.id');
        $this->db->where('p.id_fee_structure', $data['id_fee_structure']);
        $this->db->where('p.id_program_landscape', $data['id_fee_structure_master']);
         $query = $this->db->get();
         
         $result = $query->result();  
         return $result;
    }

    function getSibblingDiscountByApplicantIdCurrency($id)
    {
        $applicant = $this->getApplicantDetailsById($id);

        if($applicant->nationality == '1')
        {
            $currency = 'MYR';
        }
        elseif($applicant->nationality != '')
        {
            $currency = 'USD';
        }

        $this->db->select('fst.*');
        $this->db->from('sibbling_discount as fst');
        $this->db->where('fst.currency', $currency);
        $this->db->where('fst.status', '1');
        $query = $this->db->get();
        $result = $query->row();

        return $result;
    }

    function getEmployeeDiscountByApplicantIdCurrency($id)
    {
        $applicant = $this->getApplicantDetailsById($id);

        if($applicant->nationality == '1')
        {
            $currency = 'MYR';
        }
        elseif($applicant->nationality != '')
        {
            $currency = 'USD';
        }

        $this->db->select('fst.*');
        $this->db->from('employee_discount as fst');
        $this->db->where('fst.currency', $currency);
        $this->db->where('fst.status', '1');
        $query = $this->db->get();
        $result = $query->row();

        return $result;
    }

    function getAlumniDiscountByApplicantIdCurrency($id)
    {
        $applicant = $this->getApplicantDetailsById($id);

        if($applicant->nationality == '1')
        {
            $currency = 'MYR';
        }
        elseif($applicant->nationality != '')
        {
            $currency = 'USD';

        }

        $this->db->select('fst.*');
        $this->db->from('alumni_discount as fst');
        $this->db->where('fst.currency', $currency);
        $this->db->where('fst.status', '1');
        $query = $this->db->get();
        $result = $query->row();

        return $result;
    }
}