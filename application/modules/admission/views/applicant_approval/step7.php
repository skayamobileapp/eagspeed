 <div class="container-fluid page-wrapper">
   <div class="main-container clearfix">
      <div class="page-title clearfix">
         <h3>Applicant Approval</h3>
         <a href="../list" class="btn btn-link">< Back</a>
      </div>
      <form id="form_applicant" action="" method="post" enctype="multipart/form-data">
         <div class="clearfix">
                <div id="wizard" class="wizard">
                    <div class="wizard__content">
                         <header class="wizard__header">
                         <div class="wizard__steps">
                           <nav class="steps">
                            <div class="step -completed">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="/admission/applicantApproval/step1/<?php echo $getApplicantDetails->id;?>" class="step__text">Profile Details</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -start"></div>
    
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
    
                            <div class="step -completed">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="/admission/applicantApproval/step2/<?php echo $getApplicantDetails->id;?>" class="step__text">Contact Information</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
    
                            <div class="step -completed">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="/admission/applicantApproval/step3/<?php echo $getApplicantDetails->id;?>" class="step__text">Program Interest</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
    
                            <div class="step -completed">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="/admission/applicantApproval/step4/<?php echo $getApplicantDetails->id;?>" class="step__text">Document Upload</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
    
                            <div class="step -completed">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="/admission/applicantApproval/step5/<?php echo $getApplicantDetails->id;?>" class="step__text">Discount Information</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
    
                            <div class="step -completed">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="/admission/applicantApproval/step6/<?php echo $getApplicantDetails->id;?>" class="step__text">Declaration Form</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
                             <div class="step">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="/admission/applicantApproval/step7/<?php echo $getApplicantDetails->id;?>" class="step__text">Confirmation</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
                          </nav>
                        </div>
                      </header>
    
                      <div class="panels">
                        <div class="paneld">
                          
                           <div class="clearfix">
                            

                          




                                <div class="form-container">
                                        <h4 class="form-group-title">Application Confirmation</h4>


                                    

                                      <div class="custom-table">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                <th style="width:10%">Sl. No</th>
                                                 <th style="width:10%">Tab Name</th>
                                                 <th style="width:30%">Status</th>
                                                 <th style="width:50%">Comments</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                              <tr>
                                                 <td>1</td>
                                                 <td>Profile details</td>
                                                 <td><?php 
                                                  if($getApplicantDetails->step1_status=='1') 
                                                      {
                                                         echo "Verified";
                                                      }
                                                  else if($getApplicantDetails->step1_status=='2') {
                                                      echo "Rejected";
                                                  } else {
                                                       echo "Other Reason";
                                                     }?></td>
                                                 <td><?php echo $getApplicantDetails->step1_comments;?></td>
                                                </tr>

                                               <tr>
                                                 <td>2</td>
                                                 <td>Contact information</td>
                                                 <td><?php 
                                                  if($getApplicantDetails->step2_status=='1') 
                                                      {
                                                         echo "Verified";
                                                      }
                                                  else if($getApplicantDetails->step2_status=='2') {
                                                      echo "Rejected";
                                                  } else {
                                                       echo "Other Reason";
                                                     }?></td>
                                                 <td><?php echo $getApplicantDetails->step2_comments;?></td>
                                                </tr>

                                                <tr>
                                                 <td>3</td>
                                                 <td>Programme interest</td>
                                                 <td><?php 
                                                  if($getApplicantDetails->step3_status=='1') 
                                                      {
                                                         echo "Verified";
                                                      }
                                                  else if($getApplicantDetails->step3_status=='2') {
                                                      echo "Rejected";
                                                  } else {
                                                       echo "Other Reason";
                                                     }?></td>
                                                 <td><?php echo $getApplicantDetails->step3_comments;?></td>
                                                </tr>

                                                <tr>
                                                 <td>4</td>
                                                 <td>Document Upload</td>
                                                 <td><?php 
                                                  if($getApplicantDetails->step4_status=='1') 
                                                      {
                                                         echo "Verified";
                                                      }
                                                  else if($getApplicantDetails->step4_status=='2') {
                                                      echo "Rejected";
                                                  } else {
                                                       echo "Other Reason";
                                                     }?></td>
                                                 <td><?php echo $getApplicantDetails->step4_comments;?></td>
                                                </tr>

                                                <tr>
                                                 <td>5</td>
                                                 <td>Discount Information</td>
                                                 <td><?php 
                                                  if($getApplicantDetails->step5_status=='1') 
                                                      {
                                                         echo "Verified";
                                                      }
                                                  else if($getApplicantDetails->step5_status=='2') {
                                                      echo "Rejected";
                                                  } else {
                                                       echo "Other Reason";
                                                     }?></td>
                                                 <td><?php echo $getApplicantDetails->step5_comments;?></td>
                                                </tr>

                                                <tr>
                                                 <td>6</td>
                                                 <td>Declaration</td>
                                                 <td><?php 
                                                  if($getApplicantDetails->step6_status=='1') 
                                                      {
                                                         echo "Verified";
                                                      } 
                                                  else if($getApplicantDetails->step6_status=='2') {
                                                      echo "Rejected";
                                                  } else {
                                                       echo "Other Reason";
                                                     }?></td>
                                                 <td><?php echo $getApplicantDetails->step6_comments;?></td>
                                                </tr>

                                               

                                            </tbody>
                                            
                                        </table>
                                      </div>

                                    </div>

                          
 <div class="form-container">
                             <h4 class="form-group-title">Approval Details</h4>
                             <div class="row">
                           <div class="col-sm-4">
                              <label>Applicant Status <span class='error-text'>*</span></label>
                                    <select name="applicant_status" id="applicant_status" class="form-control" required>
                                       <option value="Approved" <?php if($getApplicantDetails->applicant_status=='Approved') { echo "Selected=selected";} ?>>Approved</option>

                                        <option value="Pending documents" <?php if($getApplicantDetails->applicant_status=='Pending documents') { echo "Selected=selected";} ?>>Pending documents</option>


                                       <option value="Rejected" <?php if($getApplicantDetails->applicant_status=='Rejected') { echo "Selected=selected";} ?>>Rejected</option>

                                         <option value="Pending APEL Test" <?php if($getApplicantDetails->applicant_status=='Pending APEL Test') { echo "Selected=selected";} ?>>Pending APEL Tes</option>


                                      
                                    </select>
                                
                           </div>
                       </div>
                                               <div class="row">

                           <div class="col-sm-8">
                              <div class="form-group">
                                 <label>Comments <span class='error-text'>*</span></label>
                                 <input type="text" class="form-control" id="applicant_comments" name="applicant_comments"  value="<?php echo $getApplicantDetails->applicant_comments ?>">
                              </div>
                           </div>
                        </div>
                      </div>   

                           </div>              
                        </div>    
                      </div>
    
                      <div class="wizard__footer">
                        <a href="/applicant/applicant/step1" class="btn btn-primary">Previous</a>

                        <button class="btn btn-link mr-3"></button>
                        <button class="btn btn-primary next" type="submit">Save </button>
                      </div>
                    </div>
    
                  </div>
                <footer class="footer-wrapper">
                    <p>&copy; 2019 All rights, reserved</p>
                </footer>
            </div>   
                </form>     
        </div>
    </div>      

    
 