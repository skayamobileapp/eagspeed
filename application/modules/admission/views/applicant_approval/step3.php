<div class="container-fluid page-wrapper">
   <div class="main-container clearfix">
      <div class="page-title clearfix">
         <h3>Applicant Approval</h3>
      </div>
      <form id="form_applicant" action="" method="post" enctype="multipart/form-data">
         <div class="clearfix">
                <div id="wizard" class="wizard">
                    <div class="wizard__content">
                       <header class="wizard__header">
                        <div class="wizard__steps">
                           <nav class="steps">
                            <div class="step -completed">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="/admission/applicantApproval/step1/<?php echo $getApplicantDetails->id;?>" class="step__text">Profile Details</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -start"></div>
    
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
    
                            <div class="step -completed">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="/admission/applicantApproval/step2/<?php echo $getApplicantDetails->id;?>" class="step__text">Contact Information</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
    
                            <div class="step">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="/admission/applicantApproval/step3/<?php echo $getApplicantDetails->id;?>" class="step__text">Program Interest</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
    
                            <div class="step">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="/admission/applicantApproval/step4/<?php echo $getApplicantDetails->id;?>" class="step__text">Document Upload</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
    
                            <div class="step">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="/admission/applicantApproval/step5/<?php echo $getApplicantDetails->id;?>" class="step__text">Discount Information</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
    
                            <div class="step">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="/admission/applicantApproval/step6/<?php echo $getApplicantDetails->id;?>" class="step__text">Declaration Form</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
                             <div class="step">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="/admission/applicantApproval/step7/<?php echo $getApplicantDetails->id;?>" class="step__text">Confirmation</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
                          </nav>
                        </div>
                      </header>
    
                      <div class="panels">
                        <div class="paneld">

                          
                           <div class="row">
                               <div class="col-sm-4">
                                  <div class="form-group">
                                     <label>Partner University <span class='error-text'>*</span></label>
                                     <select name="id_university" id="id_university" class="form-control selitemIcon"  onchange="getBranchesByPartnerUniversity(this.value)">
                                        <option value="">Select</option>
                                        <?php
                                           if (!empty($partnerUniversityList))
                                           {
                                               foreach ($partnerUniversityList as $record)
                                               {?>
                                        <option value="<?php echo $record->id;  ?>"
                                           <?php 
                                              if($record->id == $getApplicantDetails->id_university)
                                              {
                                                  echo "selected=selected";
                                              } ?>
                                           >
                                           <?php echo $record->code . " - " . $record->name;?>
                                        </option>
                                        <?php
                                           }
                                           }
                                           ?>
                                     </select>
                                  </div>
                               </div>
                              <div class="col-sm-4" style="display: none;">
                                  <div class="form-group">
                                     <label>Branch <span class='error-text'>*</span></label>
                                     <span id="view_branch">
                                         <select class="form-control" id='id_branch' name='id_branch'>
                                            <option value=''></option>
                                          </select>
                                     </span>
                                  </div>
                              </div>



                               

                               <div class="col-sm-4">
                                  <div class="form-group">
                                     <label>Programme <span class='error-text'>*</span></label>
                                     <span id="view_programme">
                                          <select class="form-control" id='id_program' name='id_program'>
                                            <option value=''></option>
                                          </select>

                                     </span>
                                  </div>
                               </div>


                              <div class="col-sm-4">
                                  <div class="form-group">
                                     <label>Learning Mode <span class='error-text'>*</span></label>
                                     <span id="view_program_scheme">
                                          <select class="form-control" id='id_program_scheme' name='id_program_scheme'>
                                            <option value=''></option>
                                          </select>

                                     </span>
                                  </div>
                               </div>

                            

                              <div class="col-sm-4" style="display: none;">
                                  <div class="form-group">
                                     <label>Program Scheme <span class='error-text'>*</span></label>
                                     <span id="view_program_has_scheme">
                                          <select class="form-control" id='id_program_has_scheme' name='id_program_has_scheme'>
                                            <option value=''></option>
                                          </select>

                                     </span>
                                  </div>
                               </div>
                           
                               <div class="col-sm-4">
                                  <div class="form-group">
                                     <label>Intake <span class='error-text'>*</span></label>
                                     <span id="view_intake">
                                        <select class="form-control" id='id_intake' name='id_intake'>
                                            <option value=''></option>
                                          </select>
                                     </span>
                                  </div>
                               </div>

                            


                             




                              <div class="col-sm-4">
                                  <div class="form-group">
                                     <label>Programme Structure Type <span class='error-text'>*</span></label>
                                     <span id="view_program_structure_type">
                                         <select class="form-control" id='id_program_structure_type' name='id_program_structure_type'>
                                            <option value=''></option>
                                          </select>
                                     </span>
                                  </div>
                              </div>



                            </div>


                    <?php if($getApplicantDetails->nationality!='1') { ?> 
                               <div class="row" id='englishrequiredid' style="display: none;">



                            <h4 style="padding-left:20px;">Language Eligibility</h4>



                              <div class="col-sm-4">
                                  <div class="form-group">
                                     <label>Type <span class='error-text'>*</span></label>
                                         <select class="form-control" id='id_english_test' name='id_english_test'>
                                            <option value='1'>International English Language Testing System</option>
                                            <option value='2'>Malaysian University English Test</option>
                                          </select>
                                  </div>
                              </div>
                               <div class="col-sm-4">
                                  <div class="form-group">
                                     <label>Year <span class='error-text'>*</span></label>
                                         <input type="text" class="form-control" id='english_year' name='english_year' value="<?php echo $getApplicantDetails->english_year;?>">
                                  </div>
                              </div>
                               <div class="col-sm-4">
                                  <div class="form-group">
                                     <label>Grade <span class='error-text'>*</span></label>
                                         <input type="text" class="form-control" id='english_grade' name='english_grade' value="<?php echo $getApplicantDetails->english_grade;?>">
                                  </div>
                              </div>
                              <div class="col-sm-4">
                                  <div class="form-group">
                                     <label>Upload Certificate <span class='error-text'>*</span></label>
                                         <input type="file" class="form-control" id='english_file' name='english_file'>
                                  </div>
                              </div>




                            </div>
                 
                         <?php } ?>


                         <div class="row">


                            <h4 style="padding-left:20px;">Entry Requirements</h4>



                              <div id="view_requirements">
                    </div>



                            </div>
                 
                        </div> 

                        <?php if($getApplicantDetails->id_program_requirement=='99999') { ?>

                        <div class="form-container">
                             <h4 class="form-group-title">Apel Exam Details</h4>
                             <div class="row">
                           <div class="col-sm-4">
                              <label>Portfolio Test Result <span class='error-text'>*</span></label>
                                    <select name="portfolio_result" id="portfolio_result" class="form-control" required>
                                      <option value="Pass" <?php if($getApplicantDetails->portfolio_result=='Pass') { echo "Selected=selected";} ?>>Pass</option>
                                       <option value="Fail" <?php if($getApplicantDetails->portfolio_result=='Fail') { echo "Selected=selected";} ?>>Fail</option>
                                       <option value="Yet to Take" <?php if($getApplicantDetails->portfolio_result=='Yet to Take') { echo "Selected=selected";} ?>>Yet to Take</option>
                                    </select>
                                
                           </div>

                           <div class="col-sm-4">
                              <div class="form-group">
                                 <label>Grade <span class='error-text'>*</span></label>
                                 <input type="text" class="form-control" id="portfolio_grade" name="portfolio_grade"  required value="<?php echo $getApplicantDetails->portfolio_grade ?>">
                              </div>
                           </div>
                       </div>

                        <div class="row">
                           <div class="col-sm-4">
                              <label>Challenged Test <span class='error-text'>*</span></label>
                                    <select name="challenged_test_status" id="challenged_test_status" class="form-control" required>
                                      <option value="Pass" <?php if($getApplicantDetails->challenged_test_status=='Pass') { echo "Selected=selected";} ?>>Pass</option>
                                       <option value="Fail" <?php if($getApplicantDetails->challenged_test_status=='Fail') { echo "Selected=selected";} ?>>Fail</option>
                                       <option value="Yet to Take" <?php if($getApplicantDetails->challenged_test_status=='Yet to Take') { echo "Selected=selected";} ?>>Yet to Take</option>
                                    </select>
                                
                           </div>

                           <div class="col-sm-4">
                              <div class="form-group">
                                 <label>Grade <span class='error-text'>*</span></label>
                                 <input type="text" class="form-control" id="challenged_test_grade" name="challenged_test_grade"  required value="<?php echo $getApplicantDetails->challenged_test_grade ?>">
                              </div>
                           </div>
                       </div>

                        <div class="row">
                           <div class="col-sm-4">
                              <label>Interview Result <span class='error-text'>*</span></label>
                                    <select name="interview_result" id="interview_result" class="form-control" required>
                                      <option value="Pass" <?php if($getApplicantDetails->interview_result=='Pass') { echo "Selected=selected";} ?>>Pass</option>
                                       <option value="Fail" <?php if($getApplicantDetails->interview_result=='Fail') { echo "Selected=selected";} ?>>Fail</option>
                                       <option value="Yet to Take" <?php if($getApplicantDetails->interview_result=='Yet to Take') { echo "Selected=selected";} ?>>Yet to Take</option>
                                    </select>
                                
                           </div>

                           <div class="col-sm-4">
                              <div class="form-group">
                                 <label>Interview Grade <span class='error-text'>*</span></label>
                                 <input type="text" class="form-control" id="interview_grade" name="interview_grade"  required value="<?php echo $getApplicantDetails->interview_grade ?>">
                              </div>
                           </div>
                       </div>
                                              
                      </div>
                    <?php } ?>

                         <div class="form-container">
                             <h4 class="form-group-title">Approval Details</h4>
                             <div class="row">
                           <div class="col-sm-4">
                              <label>Verification Status <span class='error-text'>*</span></label>
                                    <select name="step3_status" id="step3_status" class="form-control" required>
                                      <option value="">Select</option>
                                      <option value="1" <?php if($getApplicantDetails->step3_status=='1') { echo "Selected=selected";} ?>>Verified</option>
                                       <option value="2" <?php if($getApplicantDetails->step3_status=='2') { echo "Selected=selected";} ?>>Rejected</option>
                                       <option value="3" <?php if($getApplicantDetails->step3_status=='3') { echo "Selected=selected";} ?>>Other Reason</option>

                                    </select>
                                
                           </div>
                       </div>
                                               <div class="row">

                           <div class="col-sm-8">
                              <div class="form-group">
                                 <label>Comments <span class='error-text'>*</span></label>
                                 <input type="text" class="form-control" id="step3_comments" name="step3_comments"  value="<?php echo $getApplicantDetails->step3_comments ?>">
                              </div>
                           </div>
                        </div>
                      </div>
                 
                        </div>    
                      </div>
    
                       <div class="wizard__footer">
                        <a href="/admission/applicantApproval/step2/<?php echo $getApplicantDetails->id;?>" class="btn btn-primary">Previous</a>

                        <button class="btn btn-link mr-3">Cancel</button>
                        <button class="btn btn-primary next" type="submit">Save & Continue</button>
                      </div>

                    </div>

                           
                      </div>
    
                   
                    </div>
    
                    <h2 class="wizard__congrats-message">
                      Congratulations!!
                    </h2>
                </div>
               
            </div> 
              </form>
       
        </div>
    </div>      
    
 

<script type="text/javascript">

    function getProgrammeByEducationLevelId(id_education_level)
    {
      // alert(id_education_level);
      if(id_education_level != '')
        {

            $.get("/admission/applicantApproval/getProgrammeByEducationLevelId/"+id_education_level, function(data, status){
           
                $("#view_programme").html(data);
                $("#view_programme").show();
            });
        }

    }


    function getprogramScheme(id)
    {

      if(id!='')
      {
         $.get("/admission/applicantApproval/getSchemeByProgramId/"+id, function(data, status)
            {

              $("#view_program_has_scheme").html(data);
              $("#view_program_has_scheme").show();

              
              var nationality = "<?php echo $getApplicantDetails->nationality;?>";

              if(nationality == '1')
              {
                $("#id_program_has_scheme").find('option[value="1"]').attr('selected',true);
                // alert(nationality);
              }
              else
              {
                $("#id_program_has_scheme").find('option[value="2"]').attr('selected',true);
              }
              
            });


         $.get("/admission/applicantApproval/getIntakeByProgramme/"+id, function(data, status)
            {
                var idstateselected = "<?php echo $getApplicantDetails->id_intake;?>";

                $("#view_intake").html(data);
                $("#id_intake").find('option[value="'+idstateselected+'"]').attr('selected',true);
                $('select').select2();
            });

          $.get("/admission/applicantApproval/getProgramSchemeByProgramId/"+id, function(data, status){
                $("#view_program_scheme").html(data);
                $("#view_program_scheme").show();
            });  


             $.get("/admission/applicantApproval/getProgramStructureTypeByProgramId/"+id, function(data, status){
                $("#view_program_structure_type").html(data);
                $("#view_program_structure_type").show();
            });

                            var applicantId = "<?php echo $getApplicantDetails->id;?>";


              $.get("/admission/applicantApproval/getIndividualEntryRequirement/"+id+"/"+applicantId, function(data, status){
                            var id_program_requirement = "<?php echo $getApplicantDetails->id_program_requirement;?>";

                $("#view_requirements").html(data);
                $("#view_requirements").show();
            });  



              
              $.get("/admission/applicantApproval/checkenglish/"+id, function(data, status)
              {
                 var engrequried = data;
                 $("#englishrequiredid").hide();
                 if(engrequried=='1')
                 {
                  $("#englishrequiredid").show();
                 }

              }); 
        }
      }


    function getIntakeByProgramme(id)
     {
        if(id != '')
        {
            $.get("/admission/applicantApproval/getIntakeByProgramme/"+id, function(data, status){
           
                $("#view_intake").html(data);
                $("#view_intake").show();
            });

            // Programme Learning Mode
           


           
                                        var applicantId = "<?php echo $getApplicantDetails->id;?>";


              $.get("/admission/applicantApproval/getIndividualEntryRequirement/"+id+"/"+applicantId, function(data, status){
                $("#view_requirements").html(data);
                $("#view_requirements").show();
            });


            $.get("/admission/applicantApproval/getProgramStructureTypeByProgramId/"+id, function(data, status){
                $("#view_program_structure_type").html(data);
                $("#view_program_structure_type").show();
            });

        }
     }


     function getBranchesByPartnerUniversity(id)
     {
        $.get("/admission/applicantApproval/getBranchesByPartnerUniversity/"+id, function(data, status)
        {
          $("#view_branch").html(data);
          $("#view_branch").show();
        });


         $.get("/admission/applicantApproval/getProgramByPartnerUniversity/"+id, function(data, status)
        {
          $("#view_programme").html(data);
          $("#view_programme").show();
        });

     }

      function checkFeeStructure()
     {
        var tempPR = {};
        tempPR['id_education_level'] = $("#id_degree_type").val();
        tempPR['id_program'] = $("#id_program").val();
        tempPR['id_intake'] = $("#id_intake").val();
        tempPR['id_program_scheme'] = $("#id_program_scheme").val();
        tempPR['id_program_has_scheme'] = $("#id_program_has_scheme").val();
        // alert(tempPR['id_program']);

        if(tempPR['id_program'] != '' && tempPR['id_intake'] != ''  && tempPR['id_program_scheme'] != '' && tempPR['id_program_has_scheme'] != '')
        {

            // $.ajax(
            // {
            //    url: '/applicant/applicant/checkFeeStructure',
            //     type: 'POST',
            //    data:
            //    {
            //     tempData: tempPR
            //    },
            //    error: function()
            //    {
            //     alert('Something is wrong');
            //    },
            //    success: function(result)
            //    {
            //         if(result == '0')
            //         {

            //             alert('No Fee Structure Defined For The Selected Particulars, Select Another Combination');
            //             $(this).data('options', $('#id_intake option').clone());

            //             var idstateselected = 0;

            //             // $("#id_program").find('option[value="'+idstateselected+'"]').attr('selected',true);
            //             // $("#id_intake").find('option[value="'+idstateselected+'"]').attr('selected',true);
            //             // $("#id_program_scheme").find('option[value="'+idstateselected+'"]').attr('selected',true);
            //             // $("#id_program_has_scheme").find('option[value="'+idstateselected+'"]').attr('selected',true);
            //             // $('select').select2();

            //             // $("#id_program_scheme").find('option[value="'+idstateselected+'"]').attr('selected',true);
            //             // $('select').select2();


            //             // $("#id_intake").html('<option value="">').append(options);
            //             // $("#id_intake").val('');

            //             // $(this).data('options', $('#id_program_scheme option').clone());
            //             // $("#id_program_scheme").html('<option value="">').append(options);
            //             // $("#id_program_scheme").val('');
                        
            //         }
            //    }
            // });
        }
     }



    $(document).ready(function() {
        $('select').select2();

        var idprogram = "<?php echo $getApplicantDetails->id_program;?>";
        var id_degree_type = "<?php echo $getApplicantDetails->id_degree_type;?>";

        if(id_degree_type != '' && id_degree_type != 0)
        {

          $.get("/admission/applicantApproval/getProgrammeByEducationLevelId/"+id_degree_type, function(data, status){
           
                $("#view_programme").html(data);
                // $("#view_programme").show();
                $("#id_program").find('option[value="'+idprogram+'"]').attr('selected',true);
                $('select').select2();
            });
        }




      if(idprogram!='')
      {
         $.get("/admission/applicantApproval/getIntakeByProgramme/"+idprogram, function(data, status)
            {
                var idstateselected = "<?php echo $getApplicantDetails->id_intake;?>";

                $("#view_intake").html(data);
                $("#id_intake").find('option[value="'+idstateselected+'"]').attr('selected',true);
                $('select').select2();
            });



         $.get("/admission/applicantApproval/getProgramSchemeByProgramId/"+idprogram, function(data, status)
            {
                var idprogramschemeselected = "<?php echo $getApplicantDetails->id_program_scheme;?>";
                $("#view_program_scheme").html(data);
                $("#id_program_scheme").find('option[value="'+idprogramschemeselected+'"]').attr('selected',true);
                $('select').select2();
            });



          $.get("/admission/applicantApproval/getProgramStructureTypeByProgramId/"+idprogram, function(data, status)
          {
                $("#view_program_structure_type").html(data);
                $("#view_program_structure_type").show();

                var id_program_structure_type = "<?php echo $getApplicantDetails->id_program_structure_type;?>";
          
                $("#id_program_structure_type").find('option[value="'+id_program_structure_type+'"]').attr('selected',true);
                $('select').select2();
            });



         $.get("/admission/applicantApproval/getSchemeByProgramId/"+idprogram, function(data, status)
            {
              // alert(data);
                var id_program_has_scheme = "<?php echo $getApplicantDetails->id_program_has_scheme;?>";
                $("#view_program_has_scheme").html(data);
                $("#id_program_has_scheme").find('option[value="'+id_program_has_scheme+'"]').attr('selected',true);
                $('select').select2();
            });

          var id_university = "<?php echo $getApplicantDetails->id_university;?>";



         $.get("/admission/applicantApproval/getBranchesByPartnerUniversity/"+id_university, function(data, status)
            {
                var id_branch = "<?php echo $getApplicantDetails->id_branch;?>";
                $("#view_branch").html(data);
                $("#id_branch").find('option[value="'+id_branch+'"]').attr('selected',true);
                $('select').select2();
            });

         $.get("/admission/applicantApproval/getProgramByPartnerUniversity/"+id_university, function(data, status)
        {

            var id_program = "<?php echo $getApplicantDetails->id_program;?>";
                $("#view_programme").html(data);
                $("#id_program").find('option[value="'+id_program+'"]').attr('selected',true);
                $('select').select2();

        });




                            var applicantId = "<?php echo $getApplicantDetails->id;?>";

              $.get("/admission/applicantApproval/getIndividualEntryRequirement/"+idprogram+"/"+applicantId, function(data, status){
            // alert(id);
                $("#view_requirements").html(data);
                $("#view_requirements").show();
            });  

              $.get("/admission/applicantApproval/checkenglish/"+idprogram, function(data, status){
                           var engrequried = data;
                           $("#englishrequiredid").hide();
                           if(engrequried=='1') {
                           $("#englishrequiredid").show();


                           }
            });    
     }

        $("#form_applicant").validate({
            rules: {
                salutation: {
                    required: true
                },
                 first_name: {
                    required: true
                },
                 last_name: {
                    required: true
                },
                 phone: {
                    required: true
                },
                 email_id: {
                    required: true
                },
                 password: {
                    required: true
                },
                 nric: {
                    required: true
                },
                 gender: {
                    required: true
                },
                 id_program: {
                    required: true
                },
                 id_intake: {
                    required: true
                },
                employee_discount :{
                    required : true
                },
                sibbling_discount :{
                    required : true
                },
                 sibbling_name: {
                    required: true
                },
                 sibbling_nric: {
                    required: true
                },
                 employee_name: {
                    required: true
                },
                 employee_nric: {
                    required: true
                },
                 employee_designation: {
                    required: true
                },
                 date_of_birth: {
                    required: true
                },
                 nationality: {
                    required: true
                },
                 id_race: {
                    required: true
                },
                 mail_address1: {
                    required: true
                },
                 mailing_city: {
                    required: true
                },
                 mailing_country: {
                    required: true
                },
                 mailing_state: {
                    required: true
                },
                 mailing_zipcode: {
                    required: true
                },
                 permanent_address1: {
                    required: true
                },
                 permanent_city: {
                    required: true
                },
                 permanent_country: {
                    required: true
                },
                 permanent_state: {
                    required: true
                },
                 permanent_zipcode: {
                    required: true
                },
                 is_submitted: {
                    required: true
                },
                is_hostel: {
                    required: true
                },
                id_degree_type: {
                    required: true
                },
                passport: {
                    required: true
                },
                program_scheme: {
                    required: true
                },
                 alumni_discount: {
                    required: true
                },
                alumni_name: {
                    required: true
                },
                alumni_email: {
                    required: true
                },
                alumni_nric: {
                    required: true
                },
                id_program_scheme: {
                    required: true
                },
                id_branch: {
                    required: true
                },
                id_university: {
                  required: true
                },
                id_program_has_scheme: {
                  required: true
                },
                id_program_structure_type: {
                  required: true
                }
            },
            messages: {
                salutation: {
                    required: "<p class='error-text'>Salutation required</p>",
                },
                first_name: {
                    required: "<p class='error-text'>First Name required</p>",
                },
                last_name: {
                    required: "<p class='error-text'>Last Name required</p>",
                },
                email_id: {
                    required: "<p class='error-text'>Email required</p>",
                },
                phone: {
                    required: "<p class='error-text'>Phone required</p>",
                },
                gender: {
                    required: "<p class='error-text'>Gender required</p>",
                },
                nric: {
                    required: "<p class='error-text'>NRIC required</p>",
                },
                password: {
                    required: "<p class='error-text'>Password required</p>",
                },
                id_program: {
                    required: "<p class='error-text'>Select Program</p>",
                },
                id_intake: {
                    required: "<p class='error-text'>Select Intake</p>",
                },
                employee_discount: {
                    required: "<p class='error-text'>Employee Discount required</p>",
                },
                sibbling_discount: {
                    required: "<p class='error-text'>Sibbling Discount required</p>",
                },
                sibbling_name: {
                    required: "<p class='error-text'>Sibling Name required</p>",
                },
                sibbling_nric: {
                    required: "<p class='error-text'>Sibling NRIC required</p>",
                },
                employee_name: {
                    required: "<p class='error-text'>Employee Name required</p>",
                },
                employee_nric: {
                    required: "<p class='error-text'>Employee NRIC required</p>",
                },
                employee_designation: {
                    required: "<p class='error-text'>Employee Designation required</p>",
                },
                date_of_birth: {
                    required: "<p class='error-text'>Select Date Of Birth</p>",
                },
                nationality: {
                    required: "<p class='error-text'>Select Type Of Nationality</p>",
                },
                id_race: {
                    required: "<p class='error-text'>Select Race</p>",
                },
                mail_address1: {
                    required: "<p class='error-text'>Enter Mailing Address 1</p>",
                },
                mailing_city: {
                    required: "<p class='error-text'>Enter Mailimg City</p>",
                },
                mailing_country: {
                    required: "<p class='error-text'>Select Mailing Country</p>",
                },
                mailing_state: {
                    required: "<p class='error-text'>Select Mailing State</p>",
                },
                mailing_zipcode: {
                    required: "<p class='error-text'>Enter Mailing Zipcode</p>",
                },
                permanent_address1: {
                    required: "<p class='error-text'>Enter Permanent Address 1</p>",
                },
                permanent_city: {
                    required: "<p class='error-text'>Enter Permanent City</p>",
                },
                permanent_country: {
                    required: "<p class='error-text'>Select Permanent Country</p>",
                },
                permanent_state: {
                    required: "<p class='error-text'>Select Permanent State</p>",
                },
                permanent_zipcode: {
                    required: "<p class='error-text'>Enter Permanent Zipcode</p>",
                },
                is_submitted: {
                    required: "<p class='error-text'>Check Indicate that you accept the Terms and Conditions</p>",
                },
                is_hostel: {
                    required: "<p class='error-text'>Select Accomodation Required</p>",
                },
                id_degree_type: {
                    required: "<p class='error-text'>Select Degree Level</p>",
                },
                passport: {
                    required: "<p class='error-text'>Passport No. Required</p>",
                },
                program_scheme: {
                    required: "<p class='error-text'>Select Program Scheme</p>",
                },
                alumni_discount: {
                    required: "<p class='error-text'>Select Alumni Discount Applicable </p>",
                },
                alumni_name: {
                    required: "<p class='error-text'>Alumni Name Required</p>",
                },
                alumni_email: {
                    required: "<p class='error-text'>Alumni Email Required </p>",
                },
                alumni_nric: {
                    required: "<p class='error-text'>Alumni NRIC Required</p>",
                },
                id_program_scheme: {
                    required: "<p class='error-text'>Select Learning Mode</p>",
                },
                id_branch: {
                    required: "<p class='error-text'>Select Branch</p>",
                },
                id_university: {
                    required: "<p class='error-text'>Select University</p>",
                },
                id_program_has_scheme: {
                    required: "<p class='error-text'>Select Program Scheme</p>",
                },
                id_program_structure_type: {
                    required: "<p class='error-text'>Select Program Structure Type</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
  });




    $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
    });

</script>