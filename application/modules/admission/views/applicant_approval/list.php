<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List Applicants For Approval</h3>
      <!-- <a href="add" class="btn btn-primary">+ Add Applicant</a> -->
    </div>

   <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">

                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Applicant Name</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="first_name" value="<?php echo $searchParam['first_name']; ?>">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-4 control-label">Email</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="email_id" value="<?php echo $searchParam['email_id']; ?>">
                      </div>
                    </div>
                     <div class="form-group">
                      <label class="col-sm-4 control-label">NRIC</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="nric" value="<?php echo $searchParam['nric']; ?>">
                      </div>
                    </div>

                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Intake</label>
                      <div class="col-sm-8">
                        <select name="id_intake" id="id_intake" class="form-control selitemIcon">
                            <option value="">Select</option>
                            <?php
                            if (!empty($intakeList))
                            {
                              // echo "<Pre>";print_r($searchParam);exit;
                                foreach ($intakeList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                                <?php if($searchParam['id_intake'] == $record->id)
                                { 
                                    echo "selected";
                                } 
                                ?>>
                                <?php echo $record->year . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                      </div>
                    </div>

                     <div class="form-group">
                      <label class="col-sm-4 control-label">Program</label>
                      <div class="col-sm-8">
                        <select name="id_program" id="id_program" class="form-control selitemIcon">
                            <option value="">Select</option>
                            <?php
                            if (!empty($programList))
                            {
                              // echo "<Pre>";print_r($searchParam);exit;
                                foreach ($programList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                                <?php if($searchParam['id_program'] == $record->id)
                                { 
                                    echo "selected";
                                } 
                                ?>>
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                  </div>


                  <div class="form-group">
                      <label class="col-sm-4 control-label">Applicant Status</label>
                      <div class="col-sm-8">
                        <select name="applicant_status" id="applicant_status" class="form-control selitemIcon">
                            <option value="">Select</option>
                             <option value="Draft"
                                <?php if($searchParam['applicant_status'] == 'Draft')
                                { 
                                    echo "selected";
                                } 
                                ?>>
                                <?php echo 'Draft';?>
                             </option>

                             <option value="Submitted"
                                <?php if($searchParam['applicant_status'] == 'Submitted')
                                { 
                                    echo "selected";
                                } 
                                ?>>
                                <?php echo 'Submitted';?>
                             </option>

                             <option value="Approved"
                                <?php if($searchParam['applicant_status'] == 'Approved')
                                { 
                                    echo "selected";
                                } 
                                ?>>
                                <?php echo 'Approved';?>
                             </option>

                             <option value="Pending documents"
                                <?php if($searchParam['applicant_status'] == 'Pending documents')
                                { 
                                    echo "selected";
                                } 
                                ?>>
                                <?php echo 'Pending documents';?>
                             </option>
                        </select>
                    </div>
                  </div>
                    
                     <!-- <div class="form-group">
                      <label class="col-sm-4 control-label">Applicant Status</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="applicant_status" value="<?php echo $searchParam['applicant_status']; ?>">
                      </div>
                    </div> -->

                  </div>

                </div>
              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href="list" class="btn btn-link">Clear All Fields</a>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Applicant Name</th>
            <th>Email</th>
            <th>Phone Number</th>
            <th>NRIC</th>
            <th>Program Scheme</th>
            <th>Program</th>
            <th>Intake</th>
            <th>Education Level</th>
            <th>Program Structure</th>
            <th>Learning Center</th>
            <th>Applicant Status</th>
            <th style="text-align: center;">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($applicantList))
          {
            $i=1;
            foreach ($applicantList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->full_name ?></td>
                <td><?php echo $record->email_id ?></td>
                <td><?php echo $record->phone?></td>
                <td><?php echo $record->nric ?></td>
                <td><?php echo $record->program_scheme ?></td>
                <td><?php echo $record->program_code . " - " . $record->program_name ?></td>
                <td><?php echo $record->intake_year . " - " . $record->intake_name ?></td>
                <td><?php echo $record->education_level ?></td>
                <td><?php echo $record->program_structure_code . " - " . $record->program_structure_name ?></td>
                <td><?php echo $record->training_center_code . " - " . $record->training_center_name ?></td>
               
                <td><?php echo $record->applicant_status ?></td>
                <td class="text-center">
                  <a href="<?php echo 'step1/' . $record->id; ?>" title="Approve">Review</a>
                  
                </td>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script>

  $('select').select2();
  
  function clearSearchForm()
      {
        window.location.reload();
      }
</script>