<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Applicant</h3>
        </div>
        <form id="form_applicant" action="" method="post">
            <div class="form-container">
                <h4 class="form-group-title">Applicant Details</h4>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Salutation <span class='error-text'>*</span></label>
                            <select name="salutation" id="salutation" class="form-control">
                                <option value="">Select</option>
                                <option value="Mr">Mr</option>
                                <option value="Miss">Miss</option>
                                <option value="Mrs">Mrs</option>
                                <option value="Dr">Dr</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>First Name <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="first_name" name="first_name">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Last Name <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="last_name" name="last_name">
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Phone Number <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="phone" name="phone" minlength="10" maxlength="10">
                        </div>
                    </div><div class="col-sm-4">
                        <div class="form-group">
                            <label>Email <span class='error-text'>*</span></label>
                            <input type="email" class="form-control" id="email_id" name="email_id">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Password <span class='error-text'>*</span></label>
                            <input type="password" class="form-control" id="password" name="password">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>NRIC <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="nric" name="nric">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Gender <span class='error-text'>*</span></label>
                            <select class="form-control" id="gender" name="gender">
                                <option value="">SELECT</option>
                                <option value="Male" >MALE</option>
                                <option value="Female" >FEMALE</option>
                                <!-- <option value="Others" >OTHERS</option> -->
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Date Of Birth <span class='error-text'>*</span></label>
                            <input type="text" class="form-control datepicker" id="date_of_birth" name="date_of_birth" autocomplete="off">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Martial Status <span class='error-text'>*</span></label>
                            <select class="form-control" id="martial_status" name="martial_status">
                                <option value="">SELECT</option>
                                <option value="Single">SINGLE</option>
                                <option value="Married">MARRIED</option>
                                <option value="Divorced">DIVORCED</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Religion <span class='error-text'>*</span></label>
                            <select name="religion" id="religion" class="form-control">
                            <option value="">Select</option>
                            <option value="Islam">Islam</option>
                            <option value="Buddhism">Buddhism</option>
                            <option value="Christianity">Christianity</option>
                            <option value="Hinduism">Hinduism</option>
                            <option value="Other">Other</option>
                        </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Type Of Nationality <span class='error-text'>*</span></label>
                            <select name="nationality" id="nationality" class="form-control">
                            <option value="">Select</option>
                            <option value="Malaysian">Malaysian</option>
                            <option value="Other">Other</option>
                        </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Race <span class='error-text'>*</span></label>
                            <select name="id_race" id="id_race" class="form-control">
                                <option value="">Select</option>
                                <option value="Bhumiputra">Bhumiputra</option>
                                <option value="International">International</option>
                                <!-- <?php
                                if (!empty($raceList))
                                {
                                    foreach ($raceList as $record)
                                    {?>
                                <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->name;?>
                                </option>
                                <?php
                                    }
                                }
                                ?> -->
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Program <span class='error-text'>*</span></label>
                            <select name="id_program" id="id_program" class="form-control selitemIcon"  onchange="getIntakeByProgramme(this.value)">
                                <option value="">Select</option>
                                <?php
                                if (!empty($programList))
                                {
                                    foreach ($programList as $record)
                                    {?>
                                <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->name;?>
                                </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label> Intake <span class='error-text'>*</span></label>
                            <span id="view_intake"></span>
                            <!-- <select name="id_intake" id="id_intake" class="form-control selitemIcon">
                                <option value="">Select</option>
                                <?php
                                if (!empty($intakeList))
                                {
                                    foreach ($intakeList as $record)
                                    {?>
                                <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->name;?>
                                </option>
                                <?php
                                    }
                                }
                                ?>
                            </select> -->
                        </div>
                    </div>
                    
                </div>

                <div class="row">
                    
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Degree Level <span class='error-text'>*</span></label> <a href="editProgram" class="btn btn-link">
                                <!-- <span style='font-size:18px;'>&#9998;</span> -->
                            </a>
                            <select name="id_degree_type" id="id_degree_type" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($degreeTypeList))
                                {
                                    foreach ($degreeTypeList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;  ?>"
                                            <?php 
                                            if($record->id == $getApplicantDetails->id_degree_type)
                                            {
                                                echo "selected=selected";
                                            } ?>>
                                            <?php echo $record->code . " - " . $record->name;  ?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <p>Do you Wish To Apply For Hostel Accomodation <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                            <input type="radio" name="is_hostel" id="sd1" value="1" ><span class="check-radio"></span> Yes
                            </label>
                            <label class="radio-inline">
                            <input type="radio" name="is_hostel" id="sd2" value="0" checked="checked"><span class="check-radio"></span> No
                            </label>                              
                        </div>                         
                    </div>
                    
                </div>
            </div>            
            
            <div class="form-container">
                <h4 class="form-group-title">Mailing Address</h4>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Mailing Address 1 <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="mail_address1" name="mail_address1">
                        </div>
                    </div><div class="col-sm-4">
                        <div class="form-group">
                            <label>Mailing Address 2 </label>
                            <input type="text" class="form-control" id="mail_address2" name="mail_address2">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Mailing Country <span class='error-text'>*</span></label>
                            <select name="mailing_country" id="mailing_country" class="form-control" onchange="getStateByCountry(this.value)">
                                <option value="">Select</option>
                                <?php
                                if (!empty($countryList))
                                {
                                    foreach ($countryList as $record)
                                    {?>
                                <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->name;?>
                                </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Mailing State <span class='error-text'>*</span></label>
                            <span id='view_mailing_state'></span>
                            <!-- <select name="mailing_state" id="mailing_state" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($stateList))
                                {
                                    foreach ($stateList as $record)
                                    {?>
                                <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->name;?>
                                </option>
                                <?php
                                    }
                                }
                                ?>
                            </select> -->
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Mailing City <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="mailing_city" name="mailing_city">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Mailing Zipcode <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="mailing_zipcode" name="mailing_zipcode">
                        </div>
                    </div>
                </div>
            </div>


            <div class="form-group">
                <div class="checkbox no-label">
                    <label>
                        <input type="checkbox" id="chkPassport" onclick="copyAddress()" />
                        <span class="check-radio"></span>
                        Permanent Address Same As Mailing Address
                    </label>
                </div>
            </div>


            <div class="form-container">
                <h4 class="form-group-title">Permanent Address</h4>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Permanent Address 1 <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="permanent_address1" name="permanent_address1">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Permanent Address 2 </label>
                            <input type="text" class="form-control" id="permanent_address2" name="permanent_address2">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Permanent Country <span class='error-text'>*</span></label>
                            <select name="permanent_country" id="permanent_country" class="form-control" onchange="getStateByCountryPermanent(this.value)">
                                <option value="">Select</option>
                                <?php
                                if (!empty($countryList))
                                {
                                    foreach ($countryList as $record)
                                    {?>
                                <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->name;?>
                                </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Permanent State <span class='error-text'>*</span></label>
                            <span id='view_permanent_state'></span>
                            <!-- <select name="permanent_state" id="permanent_state" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($stateList))
                                {
                                    foreach ($stateList as $record)
                                    {?>
                                <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->name;?>
                                </option>
                                <?php
                                    }
                                }
                                ?>
                            </select> -->
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Permanent City <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="permanent_city" name="permanent_city">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Permanent Zipcode <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="permanent_zipcode" name="permanent_zipcode">
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-container">
                <h4 class="form-group-title">Other Details</h4>


                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <p>Do you have sibbling/s studying with university? <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                            <input type="radio" name="sibbling_discount" id="sd1" value="Yes" onclick="showSibblingFields()"><span class="check-radio"></span> Yes
                            </label>
                            <label class="radio-inline">
                            <input type="radio" name="sibbling_discount" id="sd2" value="No" onclick="hideSibblingFields()"><span class="check-radio"></span> No
                            </label>                              
                        </div>                         
                    </div>
                </div>

                <div class="row" id="sibbling" style="display: none;">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Name <span class='error-text'>*</span></label>
                            <input type="text" id="sibbling_name" name="sibbling_name" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>NRIC <span class='error-text'>*</span></label>
                            <input type="text" id="sibbling_nric" name="sibbling_nric" class="form-control">
                        </div>
                    </div>
                </div>

                <br>
                
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <p>Do you eligible for Employee discount <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                            <input type="radio" name="employee_discount" id="" value="Yes" onclick="showEmployeeFields()"><span class="check-radio"></span> Yes
                            </label>
                            <label class="radio-inline">
                            <input type="radio" name="employee_discount" id="" value="No" onclick="hideEmployeeFields()"><span class="check-radio"></span> No
                            </label>                              
                        </div>                         
                    </div>
                </div>
                <div class="row" id="employee" style="display: none;">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Name <span class='error-text'>*</span></label>
                            <input type="text" id="employee_name" name="employee_name" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>NRIC <span class='error-text'>*</span></label>
                            <input type="text" id="employee_nric" name="employee_nric" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Designation <span class='error-text'>*</span></label>
                            <input type="text" id="employee_designation" name="employee_designation" class="form-control">
                        </div>
                    </div>
                </div>
            </div>
            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="list" class="btn btn-link">Cancel</a>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>


    function copyAddress()
    {
        document.getElementById('permanent_address1').value = document.getElementById('mail_address1').value;
        document.getElementById('permanent_address2').value = document.getElementById('mail_address2').value;
        document.getElementById('permanent_city').value = document.getElementById('mailing_city').value;
        document.getElementById('permanent_country').value = document.getElementById('mailing_country').value;
        document.getElementById('permanent_state').value = document.getElementById('mailing_state').value;
        document.getElementById('permanent_zipcode').value = document.getElementById('mailing_zipcode').value;
    }





    function getStateByCountry(id)
    {

        $.get("/admission/applicant/getStateByCountry/"+id, function(data, status){
       
            $("#view_mailing_state").html(data);
            // $("#view_programme_details").html(data);
            // $("#view_programme_details").show();
        });
    }

    function getStateByCountryPermanent(id)
    {
        if(id != '')
        {

            $.get("/admission/applicant/getStateByCountryPermanent/"+id, function(data, status){
           
                $("#view_permanent_state").html(data);
                // $("#view_programme_details").html(data);
                // $("#view_programme_details").show();
            });
        }
    }


    function getIntakeByProgramme(id)
     {
        if(id != '')
        {
            $.get("/admission/applicant/getIntakeByProgramme/"+id, function(data, status){
           
                $("#view_intake").html(data);
                $("#view_intake").show();
            });
        }
     }

     function checkFeeStructure()
     {
        var tempPR = {};
        tempPR['id_program'] = $("#id_program").val();
        tempPR['id_intake'] = $("#id_intake").val();
        // alert(tempPR['id_program']);

        if(tempPR['id_program'] != '' && tempPR['id_intake'] != '' )
        {
            // alert(tempPR['id_program']);

            $.ajax(
            {
               url: '/admission/applicant/checkFeeStructure',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // $("#view").html(result);
                // $('#myModal').modal('hide');
                // window.location.reload();
                    if(result == '0')
                    {
                        alert('No Fee Structure Defined For This Programme & Intake, Select Another Combination');
                        // document.getElementById("id_program").setTextValue() = "";
                        // document.getElementById("id_intake").setTextValue() = "";
                        // $("#id_program").val('');
                        // $("#id_intake").val('');
                        // $("#id_program").html("");
                        $(this).data('options', $('#id_intake option').clone());
                        $("#id_intake").html('<option value="">').append(options);
                        $("#id_intake").val('');
                        
                    }
               }
            });
        }
     }


    $(document).ready(function() {
        $("#form_applicant").validate({
            rules: {
                salutation: {
                    required: true
                },
                 first_name: {
                    required: true
                },
                 last_name: {
                    required: true
                },
                 phone: {
                    required: true
                },
                 email_id: {
                    required: true
                },
                 password: {
                    required: true
                },
                 nric: {
                    required: true
                },
                 gender: {
                    required: true
                },
                 id_program: {
                    required: true
                },
                 id_intake: {
                    required: true
                },
                employee_discount :{
                    required : true
                },
                sibbling_discount :{
                    required : true
                },
                 sibbling_name: {
                    required: true
                },
                 sibbling_nric: {
                    required: true
                },
                 employee_name: {
                    required: true
                },
                 employee_nric: {
                    required: true
                },
                 employee_designation: {
                    required: true
                },
                 date_of_birth: {
                    required: true
                },
                 martial_status: {
                    required: true
                },
                 religion: {
                    required: true
                },
                 nationality: {
                    required: true
                },
                 id_race: {
                    required: true
                },
                 mail_address1: {
                    required: true
                },
                 mailing_city: {
                    required: true
                },
                 mailing_country: {
                    required: true
                },
                 mailing_state: {
                    required: true
                },
                 mailing_zipcode: {
                    required: true
                },
                 permanent_address1: {
                    required: true
                },
                 permanent_city: {
                    required: true
                },
                 permanent_country: {
                    required: true
                },
                 permanent_state: {
                    required: true
                },
                 permanent_zipcode: {
                    required: true
                },
                is_hostel: {
                    required: true
                },
                id_degree_type: {
                    required: true
                }
            },
            messages: {
                salutation: {
                    required: "<p class='error-text'>Salutation required</p>",
                },
                first_name: {
                    required: "<p class='error-text'>First Name required</p>",
                },
                last_name: {
                    required: "<p class='error-text'>Last Name required</p>",
                },
                email_id: {
                    required: "<p class='error-text'>Email required</p>",
                },
                phone: {
                    required: "<p class='error-text'>Phone required</p>",
                },
                gender: {
                    required: "<p class='error-text'>Gender required</p>",
                },
                nric: {
                    required: "<p class='error-text'>NRIC required</p>",
                },
                password: {
                    required: "<p class='error-text'>Password required</p>",
                },
                id_program: {
                    required: "<p class='error-text'>Select Program</p>",
                },
                id_intake: {
                    required: "<p class='error-text'>Select Intake</p>",
                },
                employee_discount: {
                    required: "<p class='error-text'>Employee Discount required</p>",
                },
                sibbling_discount: {
                    required: "<p class='error-text'>Sibbling Discount required</p>",
                },
                sibbling_name: {
                    required: "<p class='error-text'>Sibling Name required</p>",
                },
                sibbling_nric: {
                    required: "<p class='error-text'>Sibling NRIC required</p>",
                },
                employee_name: {
                    required: "<p class='error-text'>Employee Name required</p>",
                },
                employee_nric: {
                    required: "<p class='error-text'>Employee NRIC required</p>",
                },
                employee_designation: {
                    required: "<p class='error-text'>Employee Designation required</p>",
                },
                date_of_birth: {
                    required: "<p class='error-text'>Select Date Of Birth</p>",
                },
                martial_status: {
                    required: "<p class='error-text'>Select Maritual Status</p>",
                },
                religion: {
                    required: "<p class='error-text'>Select Religion</p>",
                },
                nationality: {
                    required: "<p class='error-text'>Select Type Of Nationality</p>",
                },
                id_race: {
                    required: "<p class='error-text'>Select Race</p>",
                },
                mail_address1: {
                    required: "<p class='error-text'>Enter Mailing Address 1</p>",
                },
                mailing_city: {
                    required: "<p class='error-text'>Enter Mailimg City</p>",
                },
                mailing_country: {
                    required: "<p class='error-text'>Select Mailing Country</p>",
                },
                mailing_state: {
                    required: "<p class='error-text'>Select Mailing State</p>",
                },
                mailing_zipcode: {
                    required: "<p class='error-text'>Enter Mailing Zipcode</p>",
                },
                permanent_address1: {
                    required: "<p class='error-text'>Enter Permanent Address 1</p>",
                },
                permanent_city: {
                    required: "<p class='error-text'>Enter Permanent City</p>",
                },
                permanent_country: {
                    required: "<p class='error-text'>Select Permanent Country</p>",
                },
                permanent_state: {
                    required: "<p class='error-text'>Select Permanent State</p>",
                },
                permanent_zipcode: {
                    required: "<p class='error-text'>Enter Permanent Zipcode</p>",
                },
                is_hostel: {
                    required: "<p class='error-text'>Select Accomodation Required</p>",
                },
                id_degree_type: {
                    required: "<p class='error-text'>Select Degree Type</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script type="text/javascript">
    $('select').select2();
</script>
<script>
  $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
        yearRange: "1960:2001"
    });
  } );
</script>
<script>
    function showSibblingFields(){
            $("#sibbling").show();
    }

    function hideSibblingFields(){
            $("#sibbling").hide();
    }

    function showEmployeeFields(){
            $("#employee").show();
    }

    function hideEmployeeFields(){
            $("#employee").hide();
    }
</script>
