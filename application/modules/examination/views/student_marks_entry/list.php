<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List Course</h3>
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">


                <div class="row">

                


                 <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Course</label>
                      <div class="col-sm-8">
                        <select name="id_programme" id="id_programme" class="form-control selitemIcon">
                            <option value="">Select</option>
                            <?php
                            if (!empty($programmeList))
                            {
                                foreach ($programmeList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                              <?php
                              if($record->id == $searchParam['id_programme'])
                              {
                                echo "selected";
                              }
                              ?>
                              >
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                      </div>
                    </div>

                  </div>

                </div>




              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href='list' class="btn btn-link">Clear All Fields</a>
              </div>
            </div>
          </div>
        </form>
      </div>
  </div>

                </div>
<?php if(!empty($studentList)) {?> 
        <form action="/examination/studentMarksEntry/studentList" method="post" id="searchForm">

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Student Name</th>


            <?php for($i=0;$i<count($componentList);$i++) {?>
              <th><?php echo $componentList[$i]->exam_component;?>
 <input type='hidden' class='form-control' id='maincomponentarray[]' name='maincomponentarray[]' value="<?php echo $componentList[$i]->id;?>" style='width:10%'>
</th>
            <?php } ?> 
            <th></th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($studentList)) {
            $i=1;

            foreach ($studentList as $record) {

            

          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->full_name ?></td>
                 <?php for($m=0;$m<count($componentList);$m++) {?>
               <td> <input type='number' class='form-control' id='component_marks_obtained[<?php echo $record->id_student_has_programme; ?>][]' name='component_marks_obtained[<?php echo $record->id_student_has_programme; ?>][]' value="">

                


                <input type='hidden' class='form-control'  name='studentcomponent[<?php echo $record->id_student_has_programme; ?>][]' value="<?php echo $componentList[$m]->id; ?>">

             
            </td>

            <?php } ?> 

             <td>                      <input type='hidden' class='form-control' id='id_student_has_programme[]' name='id_student_has_programme[]' value="<?php echo $record->id_student_has_programme; ?>">
</td>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
       <div class="button-block clearfix">
        <div class="bttn-group">
            <button type="submit" class="btn btn-primary btn-lg" name="btn_submit" value="marks">Save</button>
        </div>
    </div>

    </div>
  </form>
  </div>
<?php } ?> 
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script>

  $('select').select2();

  function clearSearchForm()
  {
    window.location.reload();
  }

</script>