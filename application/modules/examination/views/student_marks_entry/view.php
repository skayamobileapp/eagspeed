<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <form id="form_receipt" action="" method="post">
        <div class="page-title clearfix">
            <h3>Approve Student Marks Entry</h3>
        </div>

        <h4 class='sub-title'>Student Profile</h4>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student Name :</dt>
                                <dd><?php echo ucwords($marksEntry->student_name); ?></dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd><?php echo $marksEntry->nric ?></dd>
                            </dl>
                            <dl>
                                <dt>Student Email :</dt>
                                <dd><?php echo $marksEntry->email_id; ?></dd>
                            </dl>
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Semester :</dt>
                                <dd><?php echo $marksEntry->semester_code . " - " . $marksEntry->semester_name ?></dd>
                            </dl>
                            <dl>
                                <dt>Programme :</dt>
                                <dd><?php echo $marksEntry->programme_code . " - " . $marksEntry->programme_name ?></dd>
                            </dl>
                            <dl>
                                <dt>Intake :</dt>
                                <dd><?php echo $marksEntry->intake_name; ?></dd>
                            </dl>
                        </div>
    
                    </div>
                </div>

        

        <div class="form-container">
            <h4 class="form-group-title">Student Marks</h4>
            
            <div class="row">
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Grade Obtailned <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="grade" name="grade" readonly="readonly" value="<?php echo $marksEntry->grade;?>" >
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Total Marks <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="marks" name="marks" readonly="readonly" value="<?php echo $marksEntry->marks;?>" >
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Result <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="result" name="result" value="<?php echo $marksEntry->result;?>" readonly="readonly">
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-sm-4">
                        <div class="form-group">
                            <label>Status <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="result" name="result" value="<?php
                            if($marksEntry->status == '1')
                            {
                                echo "Approved";
                            }
                            elseif($marksEntry->status == '2')
                            {
                                echo "Rejected";
                            }
                            else
                            {
                                echo "Pending";
                            }

                            ?>"
                             readonly="readonly">
                        </div>
                </div>

             <?php
            if($marksEntry->status=='2')
                {
                ?>


                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Reason <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="result" name="result" value="<?php echo $marksEntry->reason;?>" readonly="readonly">
                    </div>
                </div>
                
            <?php
                }
            ?>


            </div>

        </div>
            
    

        <hr/>

        <div class="form-container">
            <h4 class="form-group-title">Student Marks Details</h4>

                <div class="row">
                 <div class="custom-table">
                      <table class="table">
                        <thead>
                          <tr>
                            <th>Sl. No </th>
                            <th>Course </th>
                            <th>Grade </th>
                            <th>Marks </th>
                            <th>Result</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                          $total_marks = 0;
                          if (!empty($marksEntryDetails)) {
                            $i=1;
                            foreach ($marksEntryDetails as $record) {
                          ?>
                              <tr>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $record->course_code . " - " . $record->course_name; ?></td>
                                <td><?php echo $record->grade ?></td>
                                <td><?php echo $record->marks ?></td>
                                <td><?php echo $record->result ?></td>

                              </tr>
                          <?php
                          $i++;
                          $total_marks = $total_marks + $record->marks;
                            }
                          }
                          ?>
                          <tr >
                        <td bgcolor="" colspan="3" style="text-align: right;"><b>Total : </b></td>
                        <td bgcolor=""><b><?php echo $total_marks ?></b></td>
                        <td bgcolor=""></td>
                      </tr>


                        </tbody>
                      </table>
                    </div>
            
                </div>

        </div>


          <?php
            if($marksEntry->status=='0')
            {
            ?>

        <div class="form-container">
            <h4 class="form-group-title">Student Marks Approval</h4>

              <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <p> Marks Entry Status <span class='error-text'>*</span></p>
                        <label class="radio-inline">
                            <input type="radio" id="ed1" name="status" value="1" onclick="hideRejectField()"><span class="check-radio"></span> Approve
                        </label>
                        <label class="radio-inline">
                            <input type="radio" id="ed2" name="status" value="2" onclick="showRejectField()"><span class="check-radio"></span> Reject
                        </label>
                    </div>
                </div>


                <div class="col-sm-4" id="view_reject" style="display: none">
                    <div class="form-group">
                        <label>Reason <span class='error-text'>*</span></label>
                        <input type="text" id="reason" name="reason" class="form-control">
                    </div>
                </div>

            </div>

        </div>

         <?php
        }
        ?>

       <!--  <div class="row">

          
        </div>
 -->



           

         



            <div class="button-block clearfix">
                <div class="bttn-group">
        <?php    
        if($marksEntry->status == '0')
        {
         ?>

                     <button type="submit" class="btn btn-primary btn-lg" >Submit</button>
        <?php    
        }
         ?>

                    <a href="../approvalList" class="btn btn-link">Back</a>
                </div>
            </div>

            </form>
    </div>

        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
</form>
<script type="text/javascript">

     $(document).ready(function() {
        $("#form_receipt").validate({
            rules: {
                status: {
                    required: true
                },
                 reason: {
                    required: true
                }
            },
            messages: {
                status: {
                    required: "<p class='error-text'>Select Status",
                },
                reason: {
                    required: "<p class='error-text'>Reason Required",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


    function showRejectField(){
            $("#view_reject").show();
    }

    function hideRejectField(){
            $("#view_reject").hide();
    }
</script>