<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <form id="form_receipt" action="" method="post">
        <div class="page-title clearfix">
            <h3>Student Marks Entry</h3>
        </div>

         <div class="form-container">
            <h4 class="form-group-title">Select Student For Marks Entry</h4>

            <div class="row">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Select Program <span class='error-text'>*</span></label>
                        <select name="id_programme" id="id_programme" class="form-control" onchange="getStudents()">
                            <option value="">Select</option>
                            <?php
                            if (!empty($programmeList))
                            {
                                foreach ($programmeList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div> 

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Select Intake <span class='error-text'>*</span></label>
                        <select name="id_intake" id="id_intake" class="form-control" onchange="getStudents()">
                            <option value="">Select</option>
                            <?php
                            if (!empty($intakeList))
                            {
                                foreach ($intakeList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->year . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-group">
                        <label style="display: none;" id="display_student">Select Student *</label>
                        <span id='student'></span>
                    </div>
                </div>

                <div  class="col-sm-3" >
                    <div class="form-group">
                        <label id="display_semester" style="display: none;">Select Semester *</label>
                        <span id='semester'></span>
                    </div>
                </div>

            </div>
            
        </div>

        <div id="view_course"  style="display: none;">
        </div>


        <br>
        <hr>



        <div class="form-container">
            <h4 class="form-group-title">Semester Result</h4>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Grade <span class='error-text'>*</span></label>
                        <select name="main_grade" id="main_grade" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($gradeList))
                            {
                                foreach ($gradeList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Total Marks <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="main_marks" name="main_marks">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Result <span class='error-text'>*</span></label>
                        <select name="main_result" id="main_result" class="form-control">
                            <option value="">Select</option>
                            <option value="Pass">Pass</option>
                            <option value="Fail">Fail</option>
                        </select>
                    </div>
                </div>
            </div>

        </div>


            <div class="button-block clearfix">
                <div class="btn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="list" class="btn btn-link">Cancel</a>
                </div>
            </div>
            
    

        <!-- <h3>Semester Result</h3> -->
        <!-- <button type="button" class="btn btn-info btn-lg" onclick="opendialog()">Add</button> -->
          
            <!-- <div class="row">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>PAyment Mode *</label>
                        <select name="id_payment_mode" id="id_payment_mode" class="form-control">
                             <option value="Cash">Cash</option>
                             <option value="DD">DD</option>
                             
                        </select>
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-group">
                        <label> Amount *</label>
                        <input type="text" class="form-control" id="payment_mode_amount" name="payment_mode_amount">
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Reference Number *</label>
                        <input type="text" class="form-control" id="payment_reference_number" name="payment_reference_number">
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="btn-group">
                        <button type="button" class="btn btn-primary btn-lg" onclick="saveData()">Add</button>
                </div>
            
                </div>
              </div> -->
                <!-- <div class="row">
                     <div id="view"></div>
                 --></div>

            </div>
           </form>

    </div>

        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>




</form>
<script>

    function getStudents()
    {

     var tempPR = {};
        tempPR['id_programme'] = $("#id_programme").val();
        tempPR['id_intake'] = $("#id_intake").val();
            $.ajax(
            {
               url: '/examination/StudentMarksEntry/searchStudentList',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {

                if (tempPR['id_programme'] != '' && tempPR['id_intake'] != '')
                {
                    $("#student").html(result);
                    $("#display_student").show();
                }
                // var ita = $("#invoice_total_amount").val();
                // $("#receipt_amount").val(ita);
               }
            });
    }

    function getSemesterByStudentId(id)
    {
        $.get("/examination/StudentMarksEntry/getSemesterByStudentId/"+id, function(data, status){
   
        $("#semester").html(data);
        $("#display_semester").show();
        
        });
    }

    function getCourse()
    {
        var tempPR = {};
        tempPR['id_programme'] = $("#id_programme").val();
        tempPR['id_intake'] = $("#id_intake").val();
        tempPR['id_student'] = $("#id_student").val();
        tempPR['id_semester'] = $("#id_semester").val();
            $.ajax(
            {
               url: '/examination/StudentMarksEntry/getCources',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_course").html(result);
                $("#view_course").show();

                // var ita = $("#invoice_total_amount").val();
                // $("#receipt_amount").val(ita);
               }
            });
    }

 $(document).ready(function() {
        $("#form_receipt").validate({
            rules: {
                id_programme: {
                    required: true
                },
                id_intake: {
                    required: true
                },
                id_student: {
                    required: true
                },
                id_semester: {
                    required: true
                },
                main_grade: {
                    required: true
                },
                main_marks: {
                    required: true
                },
                main_result: {
                    required: true
                }
            },
            messages: {
                id_programme:
                {
                    required: "<p class='error-text'>Select Program</p>",
                },
                id_intake:
                {
                    required: "<p class='error-text'>Select Intake</p>",
                },
                id_student:
                {
                    required: "<p class='error-text'>Select Student</p>",
                },
                id_semester:
                {
                    required: "<p class='error-text'>Select Semester</p>",
                },
                main_grade:
                {
                    required: "<p class='error-text'>Select Grade</p>",
                },
                main_marks:
                {
                    required: "<p class='error-text'>Enter Total Marks</p>",
                },
                main_result:
                {
                    required: "<p class='error-text'>Select Result</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script type="text/javascript">
    $('select').select2();
</script>