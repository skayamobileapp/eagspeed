<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>Student Certificate List</h3>
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">
                <div class="row">

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Course Name</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="name" value="">
                      </div>
                    </div>
                  </div>

                

                </div>
              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <button type="reset" class="btn btn-link" onclick="clearSearchForm()">Clear All Fields</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
             <th>Sl. No</th>
            <th>Student Name</th>
            <th>Course</th>
            <th>Certificate Type</th>
            <th>Download</th>
          </tr>
        </thead>
        <tbody>
       
              <tr>
                <td>1</td>
                <td>Hazna</td>
                <td>Inventory Management</td>
                <td>Certificat of Completion</td>
                
                <td class="text-center"><a href="/115039872.jpeg">Download</a>
                </td>
              </tr>
               <tr>
                <td>2</td>
                <td>Testing one</td>
                <td>How Childrens Learn</td>
                <td>Certificate of Achievement</td>
                
                <td class="text-center"><a href="/achievement.pdf">Download</a>
                </td>
              </tr>

              <tr>
                <td>3</td>
                <td>Testing One</td>
                <td>Inventory Management</td>
                <td>Certificat of Completion</td>
                
                <td class="text-center"><a href="/3144062087.jpeg">Download</a>
                </td>
              </tr>
        
        </tbody>
      </table>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script>
    function clearSearchForm()
    {
      window.location.reload();
    }
    $('select').select2();
</script>