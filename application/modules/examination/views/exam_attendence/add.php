<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Exam Attendence</h3>
        </div>
        <form id="form_grade" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Exam Attendence Details</h4>


            <div class="row">

               <!--  <div class="col-sm-4">
                    <div class="form-group">
                        <label>Room / Hall Name</label>
                        <input type="text" class="form-control" id="room" name="room">
                    </div>
                </div> -->


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Exam Location <span class='error-text'>*</span></label>
                        <select name="id_location" id="id_location" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($examLocationList))
                            {
                                foreach ($examLocationList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name; ?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>



            </div>

        </div>


        <div class="form-container">
            <h4 class="form-group-title">Search Students</h4>
            <!-- <h4 >Search Asset For Disposal</h4> -->

        <!-- <form id="form_award" action="" method="post"> -->


            <div class="row">

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Program <span class='error-text'>*</span></label>
                        <select name="id_program" id="id_program" class="form-control" >
                            <option value="">Select</option>
                            <?php
                            if (!empty($programList))
                            {
                                foreach ($programList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>  


                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Intake <span class='error-text'>*</span></label>
                        <select name="id_intake" id="id_intake" class="form-control" >
                            <option value="">Select</option>
                            <?php
                            if (!empty($intakeList))
                            {
                                foreach ($intakeList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->year . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>  

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Semester <span class='error-text'>*</span></label>
                        <select name="id_semester" id="id_semester" class="form-control" >
                            <option value="">Select</option>
                            <?php
                            if (!empty($semesterList))
                            {
                                foreach ($semesterList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>  


                <!--  <div class="col-sm-3">
                    <div class="form-group">
                        <label>Student NRIC</label>
                        <input type="text" class="form-control" id="nric" name="nric">
                    </div>
               </div> -->
                
            </div>


            <div class="modal-footer">
                <button type="button" class="btn btn-default" onclick="searchStudent()">Search</button>
              </div>



        </div>

         <div class="form-container" style="display: none;" id="view_student_display">
            <h4 class="form-group-title">Students For Attendence</h4>


            <div  id='view_student'>
            </div>

        </div>


        <br>

        <div class="form-container" id="course_student_view" style="display: none;">
            <h4 class="form-group-title">Student Details</h4>

            <div class="custom-table">
                  <div id="view"></div>
            </div>

        </div>
            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="list" class="btn btn-link">Cancel</a>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script type="text/javascript">
    $('select').select2();
</script>
<script type="text/javascript">


    function searchStudent()
    {
         if($("#id_location").val() != '')
        {

        var tempPR = {};
        tempPR['id_program'] = $("#id_program").val();
        tempPR['id_intake'] = $("#id_intake").val();
        tempPR['id_semester'] = $("#id_semester").val();
        tempPR['id_location'] = $("#id_location").val();
        // tempPR['nric'] = $("#nric").val();
            $.ajax(
            {
               url: '/examination/examAttendence/searchStudentForAttendence',
               type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_student_display").show();
                $("#view_student").html(result);
                // $('#myModal').modal('hide');
                // var ta = $("#total_detail").val();
                // alert(ta);
                // $("#amount").val(ta);
               }
            });
        }
    }    


    $(document).ready(function() {
        $("#form_grade").validate({
            rules: {
                 id_location: {
                    required: true
                },
                 room: {
                    required: true
                }
            },
            messages: {
                id_location: {
                    required: "<p class='error-text'>Select Exam Location</p>",
                },
                room: {
                    required: "<p class='error-text'>Room / Hall Name Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>