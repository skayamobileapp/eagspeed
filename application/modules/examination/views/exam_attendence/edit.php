<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>View Exam Attendence</h3>
        </div>
         <!-- <h4>Select Student Program & Intake For Course Registration Details</h4> -->

        <form id="form_grade" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Exam Attendence Details</h4>


            <div class="row">

                <!-- <div class="col-sm-4">
                    <div class="form-group">
                        <label>Room / Hall Name</label>
                        <input type="text" class="form-control" id="programme" name="programme" value="<?php echo $examAttendence->room?>" readonly="readonly">
                    </div>
                </div> -->

                <div class="col-sm-4">
                    <div class="form-group">
                      <label>Exam Location</label>
                      <input type="text" class="form-control" id="location" name="location" value="<?php echo $examAttendence->location?>" readonly="readonly">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                      <label>Date & Time</label>
                      <input type="text" class="form-control" id="location" name="location" value="<?php echo date('d-m-Y - g:i A', strtotime($examAttendence->created_dt_tm))?>" readonly="readonly">
                    </div>
                </div>
                
            </div>

        </div>           

      </form>




      <br>


        <div class="custom-table">
              <div id="view"></div>
        </div>


        <h4>Student Attended Details</h4>
        

      <div class="form-container">
            <h4 class="form-group-title">Student Detail</h4>


         <div class="custom-table">
          <table class="table" id="list-table">
            <thead>
              <tr>
                <th>Sl. No</th>
                <th>Student Name</th>
                <th>Gender</th>
                <th>Program</th>
                <th>Intake</th>
                <th>Semester</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>
              <?php
              if (!empty($examAttendenceDetails))
              {
                $i=1;
                foreach ($examAttendenceDetails as $record) {
              ?>
                  <tr>
                    <td><?php echo $i?></td>
                    <td><?php echo $record->nric . " - " . $record->full_name?></td>
                    <td><?php echo $record->gender?></td>
                    <td><?php echo $record->program_code . ", " . $record->program_name?></td>
                    <td><?php echo $record->intake_year . ", " . $record->intake?></td>
                    <td><?php echo $record->semester_code . ", " . $record->semester_name?></td>
                    <td><?php echo $record->email_id?></td>
                    <td><?php echo $record->phone ?></td>
                    <td><?php if( $record->status == '0')
                      {
                        echo "Absent";
                      }
                      elseif( $record->status == '1')
                      {
                        echo "Present";
                      }
                      ?></td>
                   
                  </tr>
              <?php
              $i++;
                }
              }
              ?>
            </tbody>
          </table>
        </div>

    </div>


            <div class="button-block clearfix">
                <div class="bttn-group">
                    <!-- <button type="submit" class="btn btn-primary btn-lg">Save</button> -->
                    <a href="../list" class="btn btn-link">Back</a>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script type="text/javascript">
    $('select').select2();
</script>
<script type="text/javascript">


  function getIntakes()
    {

     var tempPR = {};
        tempPR['id_programme'] = $("#id_programme").val();
            $.ajax(
            {
               url: '/registration/courseRegistration/getIntakesForView',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_intake").html(result);
                    $("#display_intake").show();

                // if (tempPR['id_programme'] != '' && tempPR['id_intake'] == '')
                // {
                    
                // }
                // var ita = $("#invoice_total_amount").val();
                // $("#receipt_amount").val(ita);
               }
            });
    }



function viewData() {

        var id_intake = $("#id_intake").val();
        var id_programme = $("#id_programme").val();
        var id_student = $("#id_student").val();
            $.ajax(
            {
               url: '/registration/courseRegistration/viewData',
                type: 'POST',
               data:
               {
                'id_intake': id_intake,
                'id_programme': id_programme,
                'id_student': id_student
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view").html(result);
               }
            });
        
    }
    
</script>