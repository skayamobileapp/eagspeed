<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
       
        <div class="page-title clearfix">
            <h3>Approve Student Marks Entry</h3>
        </div>


            <div class="form-container">
                <h4 class="form-group-title">Student Details</h4>
                <div class='data-list'>
                    <div class='row'> 
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student Name :</dt>
                                <dd><?php echo ucwords($studentDetails->full_name);?></dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd><?php echo $studentDetails->nric ?></dd>
                            </dl>
                            <dl>
                                <dt>Student Email :</dt>
                                <dd><?php echo $studentDetails->email_id; ?></dd>
                            </dl>
                             <dl>
                                <dt>Nationality :</dt>
                                <dd><?php echo $studentDetails->nationality ?></dd>
                            </dl> 
                            <dl>
                                <dt>Program Scheme :</dt>
                                <dd><?php echo $studentDetails->program_scheme ?></dd>
                            </dl>                            
                        </div>        
                        
                        <div class='col-sm-6'>                           
                            <dl>
                                <dt>Intake :</dt>
                                <dd><?php echo $studentDetails->intake_name ?></dd>
                            </dl>
                            <dl>
                                <dt>Program :</dt>
                                <dd><?php echo $studentDetails->programme_name; ?></dd>
                            </dl>
                            <dl>
                                <dt>Qualification Type :</dt>
                                <dd><?php echo $studentDetails->qualification_code . " - " . $studentDetails->qualification_name; ?></dd>
                            </dl>
                            <dl>
                                <dt>Program Scheme :</dt>
                                <dd><?php echo $studentDetails->program_scheme; ?></dd>
                            </dl>
                            <dl>
                                <dt>Academic Advisor :</dt>
                                <dd><?php echo $studentDetails->ic_no . " - " . $studentDetails->advisor; ?></dd>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>


            <br>


            <div class="form-container">
                <h4 class="form-group-title">Course Registered Details</h4>
                <div class='data-list'>
                    <div class='row'> 
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Course Name :</dt>
                                <dd><?php echo ucwords($courseRegisteredDetails->course_name);?></dd>
                            </dl>
                            <dl>
                                <dt>Course Code :</dt>
                                <dd><?php echo $courseRegisteredDetails->course_code ?></dd>
                            </dl>                     
                        </div>        
                        
                        <div class='col-sm-6'>                           
                            <dl>
                                <dt>Pre Reuisite :</dt>
                                <dd><?php echo $courseRegisteredDetails->pre_requisite ?></dd>
                            </dl>
                            <dl>
                                <dt>Course Type :</dt>
                                <dd><?php echo $courseRegisteredDetails->course_type; ?></dd>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>

        
        <br>

        <div class="form-container">
            <h4 class="form-group-title">Marks Entry</h4>
            

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Total Result <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="result" name="result" value="<?php echo $marksEntry->result;?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Grade <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="grade" name="grade" value="<?php echo $marksEntry->grade;?>" readonly="readonly">
                    </div>
                </div>



                <div class="col-sm-4">
                        <div class="form-group">
                            <label>Status <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="result" name="result" value="<?php
                            if($marksEntry->status == '1')
                            {
                                echo "Approved";
                            }
                            elseif($marksEntry->status == '2')
                            {
                                echo "Rejected";
                            }
                            else
                            {
                                echo "Pending";
                            }

                            ?>"
                             readonly="readonly">
                        </div>
                </div>

             <?php
            if($marksEntry->status=='2')
                {
                ?>


                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Reason <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="result" name="result" value="<?php echo $marksEntry->reason;?>" readonly="readonly">
                    </div>
                </div>
                
            <?php
                }
            ?>


            </div>

        </div>
            
    

        <hr/>

            <?php

            if(!empty($marksEntryDetails))
            {
                ?>
                <br>

                <div class="form-container">
                        <h4 class="form-group-title">Marks Entry Details</h4>
                    

                      <div class="custom-table">
                        <table class="table">
                            <thead>
                                <tr>
                                <th>Sl. No</th>
                                <th>Exam Components</th>
                                <th>Max. Marks</th>
                                <th>Pass Marks</th>
                                <th style="text-align: center;">Marks Obtained</th>
                                <th style="text-align: center;">Result</th>
                                </tr>
                            </thead>
                            <tbody>
                                 <?php
                             $total = 0;
                              for($i=0;$i<count($marksEntryDetails);$i++)
                             { ?>
                                <tr>
                                <td><?php echo $i+1;?></td>
                                <td><?php echo $marksEntryDetails[$i]->component_code . " - " . $marksEntryDetails[$i]->component_name;?></td>
                                <td><?php echo $marksEntryDetails[$i]->max_marks; ?></td>
                                <td><?php echo $marksEntryDetails[$i]->pass_marks; ?></td>
                                <td style="text-align: center;"><?php echo $marksEntryDetails[$i]->obtained_marks; ?></td>
                                <td style="text-align: center;"><?php echo $marksEntryDetails[$i]->result; ?></td>
                                <!-- <td><?php if($marksEntryDetails[$i]->is_pass_compulsary == 1)
                                {
                                    echo 'Yes';
                                }else
                                {
                                    echo 'No';
                                }
                                ?></td>
                                <td><?php if($marksEntryDetails[$i]->attendance_status == 1)
                                {
                                    echo 'Yes';
                                }else
                                {
                                    echo 'No';
                                }
                                ?></td> -->
                                

                                 </tr>
                              <?php 
                          } 
                          ?>
                            </tbody>
                        </table>
                      </div>

                    </div>

            <?php
            
            }
             ?>



     <form id="form_receipt" action="" method="post">

          <?php
            if($marksEntry->status=='0')
            {
            ?>
            <br>

        <div class="form-container">
            <h4 class="form-group-title">Student Marks Approval</h4>

              <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <p> Marks Entry Status <span class='error-text'>*</span></p>
                        <label class="radio-inline">
                            <input type="radio" id="ed1" name="status" value="1" onclick="hideRejectField()"><span class="check-radio"></span> Approve
                        </label>
                        <label class="radio-inline">
                            <input type="radio" id="ed2" name="status" value="2" onclick="showRejectField()"><span class="check-radio"></span> Reject
                        </label>
                    </div>
                </div>


                <div class="col-sm-4" id="view_reject" style="display: none">
                    <div class="form-group">
                        <label>Reason <span class='error-text'>*</span></label>
                        <input type="text" id="reason" name="reason" class="form-control">
                    </div>
                </div>

            </div>

        </div>

         <?php
        }
        ?>          

         



            <div class="button-block clearfix">
                <div class="bttn-group">
        <?php    
        if($marksEntry->status == '0')
        {
         ?>

                     <button type="submit" class="btn btn-primary btn-lg" >Submit</button>
        <?php    
        }
         ?>

                    <a href="../marksEntryApprovalList" class="btn btn-link">Back</a>
                </div>
            </div>

        </form>
        
    </div>

    <footer class="footer-wrapper">
        <p>&copy; 2019 All rights, reserved</p>
    </footer>

    </div>
</div>
<script type="text/javascript">

     $(document).ready(function() {
        $("#form_receipt").validate({
            rules: {
                status: {
                    required: true
                },
                 reason: {
                    required: true
                }
            },
            messages: {
                status: {
                    required: "<p class='error-text'>Select Status",
                },
                reason: {
                    required: "<p class='error-text'>Reason Required",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


    function showRejectField(){
            $("#view_reject").show();
    }

    function hideRejectField(){
            $("#view_reject").hide();
    }
</script>