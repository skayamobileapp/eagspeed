<div class="container-fluid page-wrapper">
  <form id="form_search" method="post" id="searchForm">


  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>Registered Students List</h3>
      <!-- <a href="edit" class="btn btn-primary">+ Add Applicant</a> -->
    </div>

      <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Course Code :</dt>
                                <dd><?php echo ucwords($programmeDetails->name); ?></dd>
                            </dl>
                            <dl>
                                <dt>Course Name :</dt>
                                <dd><?php echo $programmeDetails->code ?></dd>
                            </dl>
                            
                        </div>   

                        <div class='col-sm-6'>
                            <dl>
                                <dt>Course Type :</dt>
                                <dd><?php echo ucwords($programmeDetails->coursetype); ?></dd>
                            </dl>
                            <dl>
                                <dt>Course Category :</dt>
                                <dd><?php echo $programmeDetails->categoryname ?></dd>
                            </dl>
                            
                        </div>        
                        
                      
                    </div>
                </div>


  <?php
    if(!empty($applicantList))
    {

      ?>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
             <th>Sl. No</th>
            <th>Student Name</th>
            <th>Student NRIC</th>
            <th>Email</th>
            <th>Start Date</th>
            <th>End Date</th>

           
          </tr>
        </thead>
        <tbody>

          <?php          
            $i=1;
            foreach ($applicantList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo ucwords($record->full_name); ?></td>
                <td><?php echo $record->nric ?></td>
                <td><?php echo $record->email_id ?></td>
                <td>
                    <input type='hidden' name='studentids[]' value="<?php echo $record->id;?>"/>
                    <input type="text" name='start_date[]' class="form-control datepicker" value="<?php echo date('d-m-Y',strtotime($record->start_date)); ?>"/></td>
                <td><input type="text" name='end_date[]' class="form-control datepicker" value="<?php echo date('d-m-Y',strtotime($record->end_date)); ?>"/></td>         
              </tr>
          <?php
          $i++;
            }
          ?>
        </tbody>
      </table>
    </div>

    <div class="button-block clearfix">
        <div class="bttn-group">
            <button type="submit" class="btn btn-primary btn-lg" name="btn_submit" value="marks">Save</button>
        </div>
    </div>


    <?php
    }
    ?>

  </div>

  </form>

  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>


<script type="text/javascript">


  $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
        dateFormat: 'dd-mm-yy',
    });
    });


    $('select').select2();
    
    function clearSearchForm()
    {
      window.location.reload();
    }

     $(document).ready(function() {
        $("#form_search").validate({
            rules: {
                id_program: {
                    required: true
                },
                id_intake: {
                    required: true
                },
                id_programme_landscape: {
                    required: true
                },
                id_course: {
                    required: true
                }
            },
            messages: {
                id_program: {
                    required: "<p class='error-text'>Select Program</p>",
                },
                id_intake: {
                    required: "<p class='error-text'>Select Intake</p>",
                },
                id_programme_landscape: {
                    required: "<p class='error-text'>Select Programme Landscape</p>",
                },
                id_course: {
                    required: "<p class='error-text'>Select Course</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

</script>