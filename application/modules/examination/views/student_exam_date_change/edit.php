<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <form id="form_receipt" action="" method="post">
        <div class="page-title clearfix">
            <h3>View Student Marks Entry</h3>
        </div>

        <h4 class='sub-title'>Student Profile</h4>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student Name :</dt>
                                <dd><?php echo ucwords($marksEntry->student_name); ?></dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd><?php echo $marksEntry->nric ?></dd>
                            </dl>
                            <dl>
                                <dt>Student Email :</dt>
                                <dd><?php echo $marksEntry->email_id; ?></dd>
                            </dl>
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Semester :</dt>
                                <dd><?php echo $marksEntry->semester_code . " - " . $marksEntry->semester_name ?></dd>
                            </dl>
                            <dl>
                                <dt>Programme :</dt>
                                <dd><?php echo $marksEntry->programme_code . " - " . $marksEntry->programme_name ?></dd>
                            </dl>
                            <dl>
                                <dt>Intake :</dt>
                                <dd><?php echo $marksEntry->intake_name; ?></dd>
                            </dl>
                        </div>
    
                    </div>
                </div>

        

        <div class="form-container">
            <h4 class="form-group-title">Student Marks</h4>


            <div class="row">
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Grade Obtailned <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="grade" name="grade" readonly="readonly" value="<?php echo $marksEntry->grade;?>" >
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Total Marks <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="marks" name="marks" readonly="readonly" value="<?php echo $marksEntry->marks;?>" >
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Result <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="result" name="result" value="<?php echo $marksEntry->result;?>" readonly="readonly">
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-sm-4">
                        <div class="form-group">
                            <label>Status <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="result" name="result" value="<?php
                            if($marksEntry->status == '1')
                            {
                                echo "Approved";
                            }
                            elseif($marksEntry->status == '2')
                            {
                                echo "Rejected";
                            }
                            else
                            {
                                echo "Pending";
                            }

                            ?>"
                             readonly="readonly">
                        </div>
                </div>
            </div>

        </div>
            
    

          <hr/>

          <div class="form-container">
            <h4 class="form-group-title">Student Marks Details</h4>

                <div class="row">
                 <div class="custom-table">
                      <table class="table">
                        <thead>
                          <tr>
                            <th>Sl. No </th>
                            <th>Course </th>
                            <th>Grade </th>
                            <th>Marks </th>
                            <th>Result</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                          $total_marks = 0;
                          if (!empty($marksEntryDetails)) {
                            $i=1;
                            foreach ($marksEntryDetails as $record) {
                          ?>
                              <tr>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $record->course_code . " - " . $record->course_name; ?></td>
                                <td><?php echo $record->grade ?></td>
                                <td><?php echo $record->marks ?></td>
                                <td><?php echo $record->result ?></td>

                              </tr>
                          <?php
                          $i++;
                          $total_marks = $total_marks + $record->marks;
                            }
                          }
                          ?>
                          <tr >
                        <td bgcolor="" colspan="3" style="text-align: right;"><b>Total : </b></td>
                        <td bgcolor=""><b><?php echo $total_marks ?></b></td>
                        <td bgcolor=""></td>
                      </tr>


                        </tbody>
                      </table>
                    </div>
            
                </div>
        </div>
        </form>

       <div class="button-block clearfix">
            <div class="bttn-group">
                <a href="../list" class="btn btn-link">Back</a>
            </div>
        </div>

    </div>

        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>