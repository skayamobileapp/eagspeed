<div class="container-fluid page-wrapper">
  <form id="form_search" method="post" id="searchForm">


  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>Students Marks Entry Approval</h3>
      <!-- <a href="edit" class="btn btn-primary">+ Add Applicant</a> -->
    </div>

         <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Course Code :</dt>
                                <dd><?php echo ucwords($programmeDetails->name); ?></dd>
                            </dl>
                            <dl>
                                <dt>Course Name :</dt>
                                <dd><?php echo $programmeDetails->code ?></dd>
                            </dl>
                            
                        </div>        
                        
                      
                    </div>
                </div>

  <?php
    if(!empty($applicantList))
    {

      ?>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
             <th>Sl. No</th>
            <th>Student Name</th>
            <th>Student NRIC</th>
            <th>Email</th>
                        <th>Start Date</th>
            <th>End Date</th>


            <?php for($m=0;$m<count($componentList);$m++){ 



                          $this->load->model('student_marks_entry_approval_model');
                $marksobtained = $this->student_marks_entry_approval_model->getmarksdistributionById($componentList[$m]->id);

                $weightage = $marksobtained->weightage;

            ?> 
              <th style="width:10%;"><?php echo $componentList[$m]->exam_component;?> (<?php echo $weightage;?> ) (%)</th>
 <input type='hidden' class='form-control' id='maincomponentarray[]' name='maincomponentarray[]' value="<?php echo $componentList[$m]->id;?>"> 

            <?php } ?> 
            <th style="width:10%;">Final Marks</th>
            <!-- <th class="text-center">Action</th> -->
          </tr>
        </thead>
        <tbody>

          <?php          
            $i=1;
            foreach ($applicantList as $record) {
          ?>
              <tr>
                <td><input type='checkbox' name='std[]' value='<?php echo $record->id_student_has_programme;?>'/><?php echo $i ?></td>
                <td><?php echo ucwords($record->full_name); ?></td>
                <td><?php echo $record->nric ?></td>
                <td><?php echo $record->email_id ?></td>
                <td><?php echo date('d-m-Y',strtotime($record->start_date)); ?></td>
                <td><?php echo date('d-m-Y',strtotime($record->end_date)); ?></td>

                <?php 

                $finalMarks = 0; 
                for($m=0;$m<count($componentList);$m++){


                          $this->load->model('student_marks_entry_approval_model');
                $marksobtained = $this->student_marks_entry_approval_model->getmarksbycomponentstudent($record->id_student_has_programme,$componentList[$m]->id);

                $weightage = $marksobtained->weightage;

                $marksforComponentObtained = ($marksobtained->marks_obtained / 100 ) * $weightage;



                $finalMarks = $finalMarks + $marksforComponentObtained;


               



  ?>
              <td>


               <input type='number' class='form-control' id='component_marks_obtained[<?php echo $record->id_student_has_programme; ?>][]' name='component_marks_obtained[<?php echo $record->id_student_has_programme; ?>][]' value="<?php echo $marksobtained->marks_obtained;?>" readonly>

                


                <input type='hidden' class='form-control'  name='studentcomponent[<?php echo $record->id_student_has_programme; ?>][]' value="<?php echo $componentList[$m]->id; ?>">

             
            </td>

            <?php } ?> 

                <td style='text-align: center;'>
                  <div class='form-group'>
                      <input type='number' class='form-control' id='marks_obtained[]' name='marks_obtained[<?php echo $record->id_student_has_programme; ?>][]' value="<?php echo $finalMarks;?>" readonly>
                      <input type='hidden' class='form-control' id='id_student_has_programme[]' name='id_student_has_programme[]' value="<?php echo $record->id_student_has_programme; ?>">
                  </div>
                </td>
             
              </tr>
          <?php
          $i++;
            }
          ?>
        </tbody>
      </table>
    </div>
      <div class="row">

       

        <div class="col-sm-4">
            <div class="form-group">
                <label>Endorsement Date <span class='error-text'>*</span></label>
                <input type="date" class="form-control" id="endorsement_date" name="endorsement_date">
            </div>
        </div>
         <div class="col-sm-4">
            <div class="form-group">
                <label>Comments <span class='error-text'>*</span></label>
                <input type="text" class="form-control" id="reason" name="reason">
            </div>
        </div>


         <div class="col-sm-4">
            <div class="form-group">
                <label>Status <span class='error-text'>*</span></label>
                <select class="form-control" name='status' id='status'>
                     <option value="Approved">Approved</option>
                     <option value="Mark Adjustment">Marks Adjustment</option>
                </select>
            </div>
        </div>

      
    </div>




    <div class="button-block clearfix">
        <div class="bttn-group">
            <button type="submit" class="btn btn-primary btn-lg" name="btn_submit" value="marks">Save</button>
        </div>
    </div>


    <?php
    }
    ?>

  </div>

  </form>

  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>


<script type="text/javascript">

    $('select').select2();
    
    function clearSearchForm()
    {
      window.location.reload();
    }

     $(document).ready(function() {
        $("#form_search").validate({
            rules: {
                id_program: {
                    required: true
                },
                id_intake: {
                    required: true
                },
                id_programme_landscape: {
                    required: true
                },
                id_course: {
                    required: true
                }
            },
            messages: {
                id_program: {
                    required: "<p class='error-text'>Select Program</p>",
                },
                id_intake: {
                    required: "<p class='error-text'>Select Intake</p>",
                },
                id_programme_landscape: {
                    required: "<p class='error-text'>Select Programme Landscape</p>",
                },
                id_course: {
                    required: "<p class='error-text'>Select Course</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

</script>