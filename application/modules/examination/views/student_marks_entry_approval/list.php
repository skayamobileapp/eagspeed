<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List Course</h3>
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">





                <div class="row">

                
                 <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Course</label>
                      <div class="col-sm-8">
                        <select name="id_programme" id="id_programme" class="form-control selitemIcon">
                            <option value="">Select</option>
                            <?php
                            if (!empty($programmeList))
                            {
                                foreach ($programmeList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                              <?php
                              if($record->id == $searchParam['id_programme'])
                              {
                                echo "selected";
                              }
                              ?>
                              >
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                      </div>
                    </div>

                  </div>

                </div>






              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href='list' class="btn btn-link">Clear All Fields</a>
              </div>
            </div>
          </div>
        </form>
      </div>

   <?php
    if(!empty($studentList))
    {

      ?>
        <form action="/examination/studentMarksEntryApproval/studentList" method="post" id="searchForm">

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
             <th>Sl. No</th>
            <th>Student Name</th>
            <th>Student NRIC</th>
            <th>Email</th>


            <?php for($m=0;$m<count($componentList);$m++){ 



                          $this->load->model('student_marks_entry_approval_model');
                $marksobtained = $this->student_marks_entry_approval_model->getmarksdistributionById($componentList[$m]->id);

                $weightage = $marksobtained->weightage;

            ?> 
              <th style="width:10%;"><?php echo $componentList[$m]->exam_component;?> (<?php echo $weightage;?> ) (%)</th>
 <input type='hidden' class='form-control' id='maincomponentarray[]' name='maincomponentarray[]' value="<?php echo $componentList[$m]->id;?>"> 

            <?php } ?> 
            <th style="width:10%;">Final Marks</th>
            <!-- <th class="text-center">Action</th> -->
          </tr>
        </thead>
        <tbody>

          <?php          
            $i=1;
            foreach ($studentList as $record) {
          ?>
              <tr>
                <td><input type='checkbox' name='std[]' value='<?php echo $record->id_student_has_programme;?>'/><?php echo $i ?></td>
                <td><?php echo ucwords($record->full_name); ?></td>
                <td><?php echo $record->nric ?></td>
                <td><?php echo $record->email_id ?></td>

                <?php 

                $finalMarks = 0; 
                for($m=0;$m<count($componentList);$m++){


                          $this->load->model('student_marks_entry_approval_model');
                $marksobtained = $this->student_marks_entry_approval_model->getmarksbycomponentstudent($record->id_student_has_programme,$componentList[$m]->id);

                $weightage = $marksobtained->weightage;

                $marksforComponentObtained = ($marksobtained->marks_obtained / 100 ) * $weightage;



                $finalMarks = $finalMarks + $marksforComponentObtained;


               



  ?>
              <td>


               <input type='number' class='form-control' id='component_marks_obtained[<?php echo $record->id_student_has_programme; ?>][]' name='component_marks_obtained[<?php echo $record->id_student_has_programme; ?>][]' value="<?php echo $marksobtained->marks_obtained;?>" readonly>

                


                <input type='hidden' class='form-control'  name='studentcomponent[<?php echo $record->id_student_has_programme; ?>][]' value="<?php echo $componentList[$m]->id; ?>">

             
            </td>

            <?php } ?> 

                <td style='text-align: center;'>
                  <div class='form-group'>
                      <input type='number' class='form-control' id='marks_obtained[]' name='marks_obtained[<?php echo $record->id_student_has_programme; ?>][]' value="<?php echo $finalMarks;?>" readonly>
                      <input type='hidden' class='form-control' id='id_student_has_programme[]' name='id_student_has_programme[]' value="<?php echo $record->id_student_has_programme; ?>">
                  </div>
                </td>
             
              </tr>
          <?php
          $i++;
            }
          ?>
        </tbody>
      </table>
    </div>
      <div class="row">

       

        <div class="col-sm-4">
            <div class="form-group">
                <label>Endorsement Date <span class='error-text'>*</span></label>
                <input type="date" class="form-control" id="endorsement_date" name="endorsement_date">
            </div>
        </div>
         <div class="col-sm-4">
            <div class="form-group">
                <label>Comments <span class='error-text'>*</span></label>
                <input type="text" class="form-control" id="reason" name="reason">
            </div>
        </div>


         <div class="col-sm-4">
            <div class="form-group">
                <label>Status <span class='error-text'>*</span></label>
                <select class="form-control" name='status' id='status'>
                     <option value="Approved">Approved</option>
                     <option value="Mark Adjustment">Marks Adjustment</option>
                </select>
            </div>
        </div>

      
    </div>




    <div class="button-block clearfix">
        <div class="bttn-group">
            <button type="submit" class="btn btn-primary btn-lg" name="btn_submit" value="marks">Save</button>
        </div>
    </div>


    <?php
    }
    ?>

  </div>

  </form>
</div>
<script>

  $('select').select2();

  function clearSearchForm()
  {
    window.location.reload();
  }

</script>