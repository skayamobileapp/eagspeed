<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">

        <div class="page-title clearfix">
            <h3>Edit Grade Setup </h3>
        </div>



        <form id="form_main" action="" method="post">

        <div class="form-container">
                <h4 class="form-group-title">Grade Setup</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Based On <span class='error-text'>*</span></label>
                        <select name="based_on" id="based_on" class="form-control" onchange="showFields()">
                            <option value=''>Select</option>

                          <option value='Award' <?php if($gradeSetup->based_on =='Award')
                          { echo "selected=selected";} ?> >Award
                          </option>

                          <option value='Program & Subject' <?php if($gradeSetup->based_on =='Program & Subject')
                            { echo "selected=selected";} ?> >Program & Subject
                          </option>
                          
                          <option value='Program' <?php if($gradeSetup->based_on =='Program')
                          { echo "selected=selected";} ?> >Program
                          </option>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Intake <span class='error-text'>*</span></label>
                        <select name="id_intake" id="id_intake" class="form-control selitemIcon">
                            <option value="">Select</option>
                            <?php
                            if (!empty($intakeList))
                            {
                                foreach ($intakeList as $record)
                                {?>

                             <option value="<?php echo $record->id;  ?>"
                                        <?php 
                                        if($record->id == $gradeSetup->id_intake)
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->name;  ?></option>
                                        <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-4" id="view_program">
                    <div class="form-group">
                        <label>Program <span class='error-text'>*</span></label>
                        <select name="id_program" id="id_program" class="form-control selitemIcon">
                            <option value="">Select</option>
                            <?php
                            if (!empty($programList))
                            {
                                foreach ($programList as $record)
                                {?>

                             <option value="<?php echo $record->id;  ?>"
                                        <?php 
                                        if($record->id == $gradeSetup->id_program)
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->code . " - " . $record->name;  ?></option>
                                        <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


                <div class="col-sm-4" id="view_course">
                    <div class="form-group">
                        <label>Course <span class='error-text'>*</span></label>
                        <select name="id_course" id="id_course" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($courseList))
                            {
                                foreach ($courseList as $record)
                                {?>

                             <option value="<?php echo $record->id;  ?>"
                                        <?php 
                                        if($record->id == $gradeSetup->id_course)
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->code . " - " . $record->name;  ?></option>
                                        <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4" id="view_award">
                    <div class="form-group">
                        <label>Award <span class='error-text'>*</span></label>
                        <select name="id_award" id="id_award" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($awardList))
                            {
                                foreach ($awardList as $record)
                                {?>

                             <option value="<?php echo $record->id;  ?>"
                                        <?php 
                                        if($record->id == $gradeSetup->id_award)
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->name . " - " . $record->description;  ?></option>
                                        <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Status <span class='error-text'>*</span></label> <br>
                        <label class="radio-inline">
                          <input type="radio" name="status" id="status" value="1" <?php if($gradeSetup->status =='1'){ echo 'checked';} ?>><span class="check-radio"></span> Active
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="status" id="status" value="0" <?php if($gradeSetup->status =='0'){ echo 'checked';} ?>><span class="check-radio"></span> In-Active
                        </label>
                    </div>
                </div>
                
            </div>

        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="../list" class="btn btn-link">Cancel</a>
            </div>
        </div>
        
        
        </form>










        <div class="form-container">
                    <h4 class="form-group-title"> Grade Setup Details</h4>
                    <div class="m-auto text-center">
                        <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
                    </div>
                    <div class="clearfix">
                        <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                            <li role="presentation" class="active" ><a href="#invoice" class="nav-link border rounded text-center"
                                    aria-controls="invoice" aria-selected="true"
                                    role="tab" data-toggle="tab">Grade Setup Details</a>
                            </li>
                            
                        </ul>

                        
                        <div class="tab-content offers-tab-content">

                            <div role="tabpanel" class="tab-pane active" id="invoice">
                                <div class="col-12 mt-4">




                                <form id="form_detail" action="" method="post">


                            <!-- <div class="form-container">
                                <h4 class="form-group-title">Head Of Department Details</h4> -->
                                <br>

                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Grade <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="id_grade" name="id_grade">
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Grade Point <span class='error-text'>*</span></label>
                                            <input type="number" class="form-control" id="point" name="point">
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Min Marks <span class='error-text'>*</span></label>
                                            <input type="number" class="form-control" id="min_marks" name="min_marks">
                                        </div>
                                    </div>

                                </div>

                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Max. Marks <span class='error-text'>*</span></label>
                                            <input type="number" class="form-control" id="max_marks" name="max_marks">
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Description <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="description" name="description">
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Description In Other Language</label>
                                            <input type="text" class="form-control" id="description_optional_language" name="description_optional_language">
                                        </div>
                                    </div>


                                </div>

                                <div class="row">


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <p>Pass <span class='error-text'>*</span></p>
                                            <label class="radio-inline">
                                              <input type="radio" name="pass" id="pass" value="No" checked="checked"><span class="check-radio"></span> No
                                            </label>
                                            <label class="radio-inline">
                                              <input type="radio" name="pass" id="pass" value="Yes"><span class="check-radio"></span> Yes
                                            </label>
                                        </div>
                                    </div>



                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Rank <span class='error-text'>*</span></label>
                                            <input type="number" class="form-control" id="rank" name="rank" min="1">
                                        </div>
                                    </div>

                                </div>

                                <div class="row">
                                  
                                    <div class="col-sm-4">
                                        <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="saveDetailData()">Add</button>
                                    </div>
                                </div>

                                <br>

                                <div class="row">
                                    <div id="view"></div>
                                </div>

                            <!-- </div> -->


                        </form>


                        <?php

                        if(!empty($getGradeSetupDetails))
                        {
                            ?>

                            <div class="form-container">
                                    <h4 class="form-group-title">Grade Setup Details</h4>

                                

                                  <div class="custom-table">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                            <th>Sl. No</th>
                                            <th>Grade</th>
                                            <th>Point</th>
                                            <th>Min. Marks</th>
                                            <th>Max. Marks</th>
                                            <th>Description</th>
                                            <th>Description Optional Language</th>
                                            <th>Pass</th>
                                            <th>Rank</th>
                                            <th>Edit</th>
                                            <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                             <?php
                                         $total = 0;
                                          for($i=0;$i<count($getGradeSetupDetails);$i++)
                                         { ?>
                                            <tr>
                                            <td><?php echo $i+1;?></td>
                                            <td><?php echo $getGradeSetupDetails[$i]->grade_name;?></td>
                                            <td><?php echo $getGradeSetupDetails[$i]->point;?></td>
                                            <td><?php echo $getGradeSetupDetails[$i]->min_marks;?></td>
                                            <td><?php echo $getGradeSetupDetails[$i]->max_marks;?></td>
                                            <td><?php echo $getGradeSetupDetails[$i]->description;?></td>
                                            <td><?php echo $getGradeSetupDetails[$i]->description_optional_language;?></td>
                                            <td><?php echo $getGradeSetupDetails[$i]->pass;?></td>
                                            <td><?php echo $getGradeSetupDetails[$i]->rank;?></td>
                                            <td> <a href="/examination/gradeSetup/editdetails/<?php echo $getGradeSetupDetails[$i]->id_grade_setup; ?>/<?php echo $getGradeSetupDetails[$i]->id; ?>">Edit</a>

                                            </td>
                                            <td>

                                            <a onclick="deleteGradeDetails(<?php echo $getGradeSetupDetails[$i]->id; ?>)">Delete</a>

                                           

                                            </td>

                                             </tr>
                                          <?php 
                                      } 
                                      ?>
                                        </tbody>
                                    </table>
                                  </div>

                                </div>




                        <?php
                        
                        }
                         ?>

                                



                                </div> 
                            </div>



                        </div>


                    </div>
                </div> 












        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>
</div>


<script type="text/javascript">
    
    $('select').select2();

    $( function() {


    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
    showFields();
  } );


    function showFields()
    {
        var based_on = $("#based_on").val();

        if(based_on == 'Award')
        {
            $("#view_intake").hide();
            $("#view_program").hide();
            $("#view_course").hide();
            $("#view_semester").show();
            $("#view_award").show();
        }
        else if(based_on == 'Program & Subject')
        {
            $("#view_intake").hide();
            $("#view_program").show();
            $("#view_course").show();
            $("#view_semester").show();
            $("#view_award").hide();
        }
        else if(based_on == 'Program')
        {
            $("#view_intake").hide();
            $("#view_program").show();
            $("#view_course").hide();
            $("#view_semester").show();
            $("#view_award").hide();
        }

    }

    function saveDetailData()
    {
        if($('#form_detail').valid())
        {
        

        var tempPR = {};
        tempPR['id_grade'] = $("#id_grade").val();
        tempPR['point'] = $("#point").val();
        tempPR['min_marks'] = $("#min_marks").val();
        tempPR['max_marks'] = $("#max_marks").val();
        tempPR['pass'] =  $("input[name='pass']:checked"). val();
        tempPR['rank'] = $("#rank").val();
        tempPR['description'] = $("#description").val();
        tempPR['description_optional_language'] = $("#description_optional_language").val();
        tempPR['id_grade_setup'] = <?php echo $id_grade_setup;?>;
            $.ajax(
            {
               url: '/examination/gradeSetup/directadd',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert(result);
                location.reload();
                // windows().location.reload();

               }
            });
        }
    }


    function deleteGradeDetails(id) {
         $.ajax(
            {
               url: '/examination/gradeSetup/deleteGradeDetails/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view").html(result);
                    // alert('Deleted Sessessfully');
                    window.location.reload();
               }
            });
    }



    $(document).ready(function() {
        $("#form_detail").validate({
            rules: {
                id_grade: {
                    required: true
                },
                point: {
                    required: true
                },
                min_marks: {
                    required: true
                },
                max_marks: {
                    required: true
                },
                pass: {
                    required: true
                },
                rank: {
                    required: true
                },
                description: {
                    required: true
                }
            },
            messages: {
                id_grade: {
                    required: "<p class='error-text'>Select Grade</p>",
                },
                point: {
                    required: "<p class='error-text'>Grade Point Required</p>",
                },
                min_marks: {
                    required: "<p class='error-text'>Min. Marks Required</p>",
                },
                max_marks: {
                    required: "<p class='error-text'>Max. Marks Required</p>",
                },
                pass: {
                    required: "<p class='error-text'>Select Pass</p>",
                },
                rank: {
                    required: "<p class='error-text'>Rank Required</p>",
                },
                description: {
                    required: "<p class='error-text'>Description Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    




    $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                based_on: {
                    required: true
                },
                id_intake: {
                    required: true
                },
                id_semester: {
                    required: true
                },
                id_program: {
                    required: true
                },
                id_course: {
                    required: true
                },
                status: {
                    required: true
                },
                id_award: {
                    required: true
                }
            },
            messages: {
                based_on: {
                    required: "<p class='error-text'>Select Based On</p>",
                },
                id_intake: {
                    required: "<p class='error-text'>Select Intake</p>",
                },
                id_semester: {
                    required: "<p class='error-text'>Select Semester</p>",
                },
                id_program: {
                    required: "<p class='error-text'>Select Program</p>",
                },
                id_course: {
                    required: "<p class='error-text'>Select Course</p>",
                },
                status: {
                    required: "<p class='error-text'>Select Status</p>",
                },
                id_award: {
                    required: "<p class='error-text'>Select Award</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>