<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Document Checklist</h3>
        </div>
        <form id="form_grade" action="" method="post">

        <div class="form-container">
                <h4 class="form-group-title">Document Checklist Details</h4>


            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Document Name</label>
                        <input type="text" class="form-control" id="document_name" name="document_name">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Category Name *</label>
                        <select name="id_category" id="id_category" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($categoryTypeList))
                            {
                                foreach ($categoryTypeList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->category_name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Mandetory</label><br>
                        <label class="radio-inline">
                              <input type="radio" name="mandetory" value="Yes"><span class="check-radio"></span> Yes
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="mandetory" value="No"><span class="check-radio"></span> No
                        </label>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Program</label>
                        <select name="id_program" id="id_program" class="form-control selitemIcon">
                            <option value="">Select</option>
                            <?php
                            if (!empty($programList))
                            {
                                foreach ($programList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>

        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>


        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script type="text/javascript">
    $('select').select2();
</script>
<script>
    $(document).ready(function() {
        $("#form_grade").validate({
            rules: {
                document_name: {
                    required: true
                },
                 id_category: {
                    required: true
                },
                 mandetory: {
                    required: true
                },
                 id_program: {
                    required: true
                }
            },
            messages: {
                document_name: {
                    required: "Document Name required",
                },
                id_category: {
                    required: "Category Name required",
                },
                mandetory: {
                    required: "Mandetory required",
                },
                id_program: {
                    required: "Program required",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
