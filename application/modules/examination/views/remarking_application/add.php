<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <form id="form_receipt" action="" method="post">
        <div class="page-title clearfix">
            <h3>Add Remarking </h3>
        </div>

                
        <div class="form-container">
            <h4 class="form-group-title">Remarking Details</h4> 
            <div class="row">


                <div class="col-sm-4" id="view_program">
                    <div class="form-group">
                        <label>Program <span class='error-text'>*</span></label>
                        <select name="id_programme" id="id_programme" class="form-control" onchange="getIntakesProgramme(this.value)">
                            <option value="">Select</option>
                            <?php
                            if (!empty($programmeList))
                            {
                                foreach ($programmeList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div> 

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Intake <span class='error-text'>*</span></label>
                        <span id="view_new_intake">
                          <select class="form-control" id='id_intake' name='id_intake'>
                            <option value=''></option>
                          </select>
                        </span>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Student <span class='error-text'>*</span></label>
                        <span id="student">
                          <select class="form-control" id='id_student' name='id_student'>
                            <option value=''></option>
                          </select>
                        </span>
                    </div>
                </div>



                <!-- <div class="col-sm-4" id="view_intake">
                    <div class="form-group">
                        <label>Intake <span class='error-text'>*</span></label>
                        <span id="view_new_intake"></span>
                    </div>
                </div> 

            

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Student <span class='error-text'>*</span></label>
                        <span id='student'></span>
                    </div>
                </div>  -->


                

            </div>
            <div class="row">   

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Course <span class='error-text'>*</span></label>
                        <span id="student_course">
                          <select class="form-control" id='id_mark_entry' name='id_mark_entry'>
                            <option value=''></option>
                          </select>
                        </span>
                    </div>
                </div>


                <!-- <div class="col-sm-4">
                    <div class="form-group">
                        <label>Student Course <span class='error-text'>*</span></label>
                        <span id='student_course'></span>
                    </div>
                </div>    -->                           
            
                
                <!-- <div class="col-sm-4">
                    <div class="form-group"> -->
                        <!-- <label>Select Student <span class='error-text'>*</span></label> -->
                        
                   <!--  </div>
                </div>   --> 

            </div>
        </div>

        <div id="view_student_details"  style="display: none;">
        </div>

        <br>

        <div id="view_marks_details"  style="display: none;">
        </div>

        

        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>

           </form>

    </div>

        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>




</form>
<script>

    $('select').select2();

    // function getStudentByProgramme(id)
    // {

    //  $.get("/finance/receipt/getStudentByProgrammeId/"+id, function(data, status){
   
    //     $("#student").html(data);
    //     });
    // }


    function getIntakesProgramme(id)
 {

    var id_programme = $("#id_programme").val();



    $.get("/examination/remarkingApplication/getIntakeByProgramme/"+id, function(data, status){
   
        $("#view_new_intake").html(data);
        $("#view_new_intake").show();
    });

 }

    function getStudentsListByMarksEntry()
    {
        var tempPR = {};
        tempPR['id_program'] = $("#id_programme").val();
        tempPR['id_intake'] = $("#id_intake").val();
        // tempPR['id'] = $("#id").val();
        if(tempPR['id_program'] != '' && tempPR['id_intake'] != '')
        {
            $.ajax(
            {
               url: '/examination/remarkingApplication/getStudentsListByMarksEntry',
                type: 'POST',
               data:
               {
                formData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#student").html(result);
                $("#student").show();
               }
            });
        }   
    }

    function getStudentByStudentId(id)
    {

     $.get("/examination/remarkingApplication/getStudentByStudentId/"+id, function(data, status){

        $("#view_student_details").html(data);
        $("#view_student_details").show();
        });

        var tempPR = {};
        tempPR['id_program'] = $("#id_programme").val();
        tempPR['id_intake'] = $("#id_intake").val();
        tempPR['id_student'] = $("#id_student").val();
        // tempPR['id'] = $("#id").val();
        if(tempPR['id_program'] != '' && tempPR['id_intake'] != '')
        {
            $.ajax(
            {
               url: '/examination/remarkingApplication/getStudentCoursesByStudentId',
                type: 'POST',
               data:
               {
                formData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#student_course").html(result);
                $("#student_course").show();
               }
            });
        }   
    }

    function getMarksByMarksEntry(id_marks)
    {
        

        $.get("/examination/remarkingApplication/getMarksByMarksEntry/"+id_marks, function(data, status){
   
        if(data)
        {

            $("#view_marks_details").html(data);
            $("#view_marks_details").show();
        }
        else
        {
            $("#id_mark_entry").val('');
            alert('Max. Remarking Limit Exceeded.');
        }
    });
    }





 $(document).ready(function() {
        $("#form_receipt").validate({
            rules: {
                id_programme: {
                    required: true
                },
                id_intake: {
                    required: true
                },
                id_student: {
                    required: true
                },
                id_mark_entry: {
                    required: true
                }
            },
            messages: {
                id_programme:
                {
                    required: "<p class='error-text'>Select Program</p>",
                },
                id_intake:
                {
                    required: "<p class='error-text'>Select Intake</p>",
                },
                id_student:
                {
                    required: "<p class='error-text'>Select Receipt Generating For</p>",
                },
                id_mark_entry:
                {
                    required: "<p class='error-text'>Select Course For Remark Entry </p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>


