<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Grade</h3>
        </div>
        <form id="form_grade" action="" method="post">

        <div class="form-container">
                <h4 class="form-group-title">Grade Details</h4>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Exam Component Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name">
                    </div>
                </div>

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Exam Component Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="code" name="code" >
                    </div>
                </div>


               

            </div>


          
        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>

        
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    $('select').select2();


    $(document).ready(function() {
        $("#form_grade").validate({
            rules: {
                name: {
                    required: true
                },
                 id_programme: {
                    required: true
                },
                min_range: {
                    required: true
                },
                max_range: {
                    required: true
                },
                grade_level_up: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Grade Name Required</p>",
                },
                id_programme: {
                    required: "<p class='error-text'>Select Programme</p>",
                },
                min_range: {
                    required: "<p class='error-text'>Min. Range Required</p>",
                },
                max_range: {
                    required: "<p class='error-text'>Max. Range Required</p>",
                },
                grade_level_up: {
                    required: "<p class='error-text'>Grade Level Up Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
