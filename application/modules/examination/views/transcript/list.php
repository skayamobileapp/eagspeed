<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Transcript</h3>
        </div>
        <form id="form_grade" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Student Details</h4>

            <div class="row">
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Programme <span class='error-text'>*</span></label>
                        <select name="id_programme" id="id_programme" class="form-control" onchange="getIntakes()">
                            <option value="">Select</option>
                            <?php
                            if (!empty($programList))
                            {
                                foreach ($programList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Intake <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="dummy_intake" name="dummy_intake" placeholder="Select">
                        <span id='view_intake' ></span>
                    </div>
                </div>
               
                <!-- <div class="col-sm-4">
                    <div class="form-group">
                        <label>Course <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="dummy_student" name="dummy_student" placeholder="Select">
                        <span id='view_student' ></span>
                    </div>
                </div> -->
            </div>

        </div>


        <div class="form-container" id="view_search_students">
          <h4 class="form-group-title">Search Students</h4>

          <div class="row">

                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Student Name</label>
                          <input type="text" class="form-control" id="name" name="name">
                      </div>
                  </div>
              

                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>NRIC</label>
                          <input type="text" class="form-control" id="nric" name="nric">
                      </div>
                  </div>


                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Email</label>
                          <input type="text" class="form-control" id="email_id" name="email_id">
                      </div>
                  </div>

              </div>

              <br>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-default" onclick="displayStudentsByIdCourseRegisteredLandscape()">Search</button>
                  </div>

          </div>


        
        <br>
        <div class="custom-table">
              <div id="view"></div>
        </div>
            <div class="button-block clearfix">
                <div class="bttn-group">
                    <!-- <button type="submit" class="btn btn-primary btn-lg">Save</button> -->
                    <!-- <a href="list" class="btn btn-link">Back</a> -->
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script type="text/javascript">
    $('select').select2();
</script>
<script type="text/javascript">

    function getIntakes()
    {

     var tempPR = {};
        tempPR['id_programme'] = $("#id_programme").val();
            $.ajax(
            {
               url: '/examination/transcript/getIntakes',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_intake").html(result);
                    $("#dummy_intake").hide();


                // if (tempPR['id_programme'] != '' && tempPR['id_intake'] == '')
                // {
                    
                // }
                // var ita = $("#invoice_total_amount").val();
                // $("#receipt_amount").val(ita);
               }
            });
    }


    function getCourseRegisteredByProgNIntake()
    {
      var tempPR = {};
        tempPR['id_programme'] = $("#id_programme").val();
        tempPR['id_intake'] = $("#id_intake").val();
        // alert(tempPR['id_programme']);
            $.ajax(
            {
               url: '/examination/transcript/getCourseRegisteredByProgNIntake',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_student").html(result);
                    $("#dummy_student").hide();
               }
            });
    }


    function getStudentByProgNIntake()
    {

     var tempPR = {};
        tempPR['id_programme'] = $("#id_programme").val();
        tempPR['id_intake'] = $("#id_intake").val();
        // alert(tempPR['id_programme']);
            $.ajax(
            {
               url: '/examination/transcript/getStudentByProgNIntake',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_student").html(result);
                    $("#dummy_student").hide();

                // if (tempPR['id_programme'] != '' && tempPR['id_intake'] == '')
                // {
                    
                // }
                // var ita = $("#invoice_total_amount").val();
                // $("#receipt_amount").val(ita);
               }
            });
    }

    function displaydata()
    {

        var id_intake = $("#id_intake").val();
        var id_programme = $("#id_programme").val();
        var id_student = $("#id_student").val();
        // alert(id_student);

        if(id_student != '')
        {

            $.ajax(
            {
               url: '/examination/transcript/displaydata',
                type: 'POST',
               data:
               {
                'id_intake': id_intake,
                'id_programme': id_programme,
                'id_student': id_student
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                
                    $("#view").html(result);
                
               }
            });

        }        
    }

    function displaySearch()
    {
      var id_intake = $("#id_intake").val();
        var id_programme = $("#id_programme").val();
        // var id_course_registered_landscape = $("#id_course_registered_landscape").val();
        // alert(id_student);

        if(id_intake != '' && id_programme != '')
        {
            // $("#view_search_students").show();
        }
    }


    function displayStudentsByIdCourseRegisteredLandscape()
    {
      var tempPR = {};
      tempPR['id_programme'] = $("#id_programme").val();
      tempPR['id_intake'] = $("#id_intake").val();
      tempPR['name'] = $("#name").val();
      tempPR['nric'] = $("#nric").val();
      tempPR['email_id'] = $("#email_id").val();

      if($("#id_programme").val() != '' && $("#id_intake").val() != '')
      {


      $.ajax(
        {
           url: '/examination/transcript/displayStudentsByIdCourseRegisteredLandscape',
            type: 'POST',
           data:
           {
            tempData: tempPR
           },
           error: function()
           {
            alert('Something is wrong');
           },
           success: function(result)
           {
            
                $("#view").html(result);
            
           }
        });
      }
      else
      {
        alert('Select Program & Intake To Search Student');
      }
    }
    
</script>
<script>

    $(document).ready(function() {
        $("#form_grade").validate({
            rules: {
                id_programme: {
                    required: true
                },
                id_intake: {
                    required: true
                },
                 id_semester: {
                    required: true
                },
                 id_student: {
                    required: true
                }
            },
            messages: {
                id_programme: {
                    required: "<p class='error-text'>Select Programme</p>",
                },
                id_intake: {
                    required: "<p class='error-text'>Intake required</p>",
                },
                id_semester: {
                    required: "<p class='error-text'>Semester required</p>",
                },
                id_student: {
                    required: "<p class='error-text'>Student required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>