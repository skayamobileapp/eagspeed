<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Publish_assesment_result_date_model extends CI_Model
{
    function publishAssesmentResultDateListSearch($data)
    {
        // $date = 
        $this->db->select('a.*, s.name as semester_name, s.code as semester_code, i.year as intake_year, i.name as intake_name, c.name as course_name, c.code as course_code, p.code as program_code, p.name as program_name');
        $this->db->from('publish_assesment_result_date as a');
        $this->db->join('semester as s', 'a.id_semester = s.id');
        $this->db->join('intake as i', 'a.id_intake = i.id');
        $this->db->join('course as c', 'a.id_course = c.id');
        $this->db->join('programme as p', 'a.id_program = p.id');
        if ($data['id_intake'] != '')
        {
            $this->db->where('a.id_intake', $data['id_intake']);
        }
        if ($data['id_semester'] != '')
        {
            $this->db->where('a.id_semester', $data['id_semester']);
        }
        if ($data['id_course'] != '')
        {
            $this->db->where('a.id_course', $data['id_course']);
        }
        if ($data['id_program'] != '')
        {
            $this->db->where('a.id_program', $data['id_program']);
        }
         $query = $this->db->get();
         $result = $query->result();  

         return $result;
    }

    function getPublishAssesmentResultDate($id)
    {
        $this->db->select('*');
        $this->db->from('publish_assesment_result_date');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getCourse($id)
    {
    	$this->db->select('*');
        $this->db->from('course');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addPublishAssesmentResultDate($data)
    {
        $this->db->trans_start();
        $this->db->insert('publish_assesment_result_date', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editPublishAssesmentResultDate($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('publish_assesment_result_date', $data);
        return TRUE;
    }

    function semesterListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('semester');
        $this->db->order_by("name", "ASC");
        $this->db->where('status', $status);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function programListByStatus($status)
    {
    	$this->db->select('*');
        $this->db->from('programme');
        $this->db->order_by("name", "ASC");
        $this->db->where('status', $status);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function courseListByStatus($status)
    {
    	$this->db->select('*');
        $this->db->from('course');
        $this->db->order_by("name", "ASC");
        $this->db->where('status', $status);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function intakeListByStatus($status)
    {
    	$this->db->select('*');
        $this->db->from('intake');
        $this->db->order_by("name", "ASC");
        $this->db->where('status', $status);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getIntakeByProgrammeId($id_programme)
    {
        $this->db->select('DISTINCT(i.id) as id, i.*');
        $this->db->from('programme_landscape as ihs');
        $this->db->join('intake as i', 'ihs.id_intake = i.id');
        $this->db->where('ihs.id_programme', $id_programme);
        $query = $this->db->get();
        return $query->result();
    }


    function getProgramLandscapeCourses($id_intake,$id_programme,$id_semester)
    {
        $this->db->select('DISTINCT(c.id) as id, c.name , c.code, a.id as id_course_registered');
        $this->db->from('add_course_to_program_landscape as a');
        $this->db->join('intake as in', 'a.id_intake = in.id');
        $this->db->join('course as c', 'a.id_course = c.id');
        $this->db->join('programme_landscape as pl', 'a.id_program_landscape = pl.id');
        $this->db->where('a.id_intake', $id_intake);
        $this->db->where('a.id_semester', $id_semester);
         $this->db->where('a.id_program', $id_programme);
        $this->db->where('pl.status', '1');
        $query = $this->db->get();
         $result = $query->result();
         return $result; 
    }
}