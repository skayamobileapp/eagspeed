<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Student_marks_adjustment_approval_model extends CI_Model
{
    function programListByStatus()
    {
        $this->db->select('*');
        $this->db->from('programme');
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

       function getProgrammeListByStatus($status)
    {
         $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }


    function getProgrammeDetailsById($id){
        $this->db->select('*');
        $this->db->from('programme as p');
        $this->db->where('p.id', $id);

        $query = $this->db->get();
        $result = $query->row();
        return $result;
    }

      function getMarkDistributionByProgramme($id_programme)
    {
        $this->db->select('md.*, ec.name as exam_component');
        $this->db->from('marks_distribution as md');
        $this->db->join('exam_components as ec', 'md.id_exam_component = ec.id');
        $this->db->where('md.id_programme', $id_programme);
        $query = $this->db->get();
        return $query->result();
    }



    function getStudentListForCourseRegisteredStudent($id)
    {
        $this->db->select('DISTINCT(s.id) as id, s.*, p.code as programme_code, p.name as programme_name, c.registration_number as company_registration_number, c.name as company_name, ihs.id as id_student_has_programme,ihs.start_date,ihs.end_date');
        $this->db->from('student_has_programme as ihs');
        $this->db->join('student as s', 'ihs.id_student = s.id');
        $this->db->join('programme as p', 'ihs.id_programme = p.id');
        $this->db->join('company as c', 's.id_company = c.id','left');
        $this->db->where('ihs.id_programme', $id);
        $this->db->where('ihs.marks_status', 'Adjustment Entry');
        $query = $this->db->get();

        return $query->result();
    }

    function getmarksdistributionById($id) {
         $this->db->select('md.*');
        $this->db->from('marks_distribution as md');

        $this->db->where('md.id', $id);
        $query = $this->db->get();
        $result = $query->row(); 

        return $result;
    }
    function getmarksbycomponentstudent($shpid,$componetid) {
         $this->db->select('md.*,mds.weightage');
        $this->db->from('student_has_programme_marks as md');
        $this->db->join('marks_distribution as mds', 'mds.id = md.id_marks_distribution');

        $this->db->where('md.id_student_has_programme', $shpid);
        $this->db->where('md.id_marks_distribution', $componetid);
        $this->db->order_by('md.id', 'DESC');
        $query = $this->db->get();
        $result = $query->row(); 

        return $result;
    }

    function getStudentHasProgramme($id)
    {
        $this->db->select('md.*');
        $this->db->from('student_has_programme as md');
        $this->db->where('md.id', $id);
        $this->db->order_by('md.id', 'DESC');
        $query = $this->db->get();
        $result = $query->row(); 

        return $result;
    }

    function getStudentMarksEntry($id)
    {   
        $this->db->select('md.*');
        $this->db->from('student_marks_entry as md');
        $this->db->where('md.id', $id);
        $this->db->order_by('md.id', 'DESC');
        $query = $this->db->get();
        $result = $query->row(); 

        return $result;
    }

    function addStudentMarksEntry($data)
    {
        $this->db->trans_start();
        $this->db->insert('student_marks_entry', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function updateStudentHasProgramme($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('student_has_programme', $data);
        return TRUE;
    }

    function updateStudentMarksEntry($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('student_marks_entry', $data);
        return TRUE;
    }
}