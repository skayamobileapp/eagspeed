<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Mark_distribution_model extends CI_Model
{
    function categoryListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('category');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function categoryTypeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('category_type');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function productTypeSetupListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('product_type');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function examComponentsListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('exam_components');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function programmeListSearch($data)
    {
        
         $this->db->select('p.*,pt.name as coursetype,c.name as categoryname,pu.name as partneruniversityname');
        $this->db->from('programme as p');
        $this->db->join('product_type as pt', 'pt.id = p.id_programme_type','left');
        $this->db->join('partner_university as pu', 'pu.id = p.id_partner_university','left');
        $this->db->join('category as c', 'c.id = p.id_category');

        if ($data['name'] != '')
        {
            $likeCriteria = "(p.name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['id_category'] != '')
        {
            $this->db->where('p.id_category', $data['id_category']);
        }
        if ($data['id_category_setup'] != '')
        {
            $this->db->where('p.id_category_setup', $data['id_category_setup']);
        }
        if ($data['id_programme_type'] != '')
        {
            $this->db->where('p.id_programme_type', $data['id_programme_type']);
        }
        if($data['id_partner_university'] != '')
        {
            $this->db->where('p.id_partner_university', $data['id_partner_university']);
        }
        $this->db->order_by("p.name", "ASC");
         $query = $this->db->get();
         $results = $query->result();



         return $results;
    }


    function programListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function getProgrammeById($id_programme)
    {
        $this->db->select('p.*');
        $this->db->from('programme as p');
        $this->db->where('p.id', $id_programme);
        $query = $this->db->get();
        return $query->row();
    }

    function saveMarksDistribution($data)
    {
        $this->db->trans_start();
        $this->db->insert('marks_distribution', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getMarkDistributionByProgramme($id_programme)
    {
        $this->db->select('md.*, ec.name as exam_component');
        $this->db->from('marks_distribution as md');
        $this->db->join('exam_components as ec', 'md.id_exam_component = ec.id');
        $this->db->where('md.id_programme', $id_programme);
        $query = $this->db->get();
        return $query->result();
    }

    function getMarkDistribution($id)
    {
        $this->db->select('*');
        $this->db->from('marks_distribution');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function deleteMarksDistribution($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('marks_distribution');
        return TRUE;
    }
    
    function addMarkDistribution($data)
    {
        $this->db->trans_start();
        $this->db->insert('marks_distribution', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editMarkDistribution($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('marks_distribution', $data);
        return TRUE;
    }
}