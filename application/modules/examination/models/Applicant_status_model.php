<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Applicant_status_model extends CI_Model
{
    function applicantStatusList()
    {
        $this->db->select('*');
        $this->db->from('applicant_status');
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }


    function getApplicantStatusDetails($id)
    {
        $this->db->select('*');
        $this->db->from('applicant_status');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewApplicantStatus($data)
    {
        $this->db->trans_start();
        $this->db->insert('applicant_status', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editApplicantStatusDetails($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('applicant_status', $data);
        return TRUE;
    }
    
    // function deleteActivityDetails($id, $date)
    // {
    //     $this->db->where('id', $countryId);
    //     $this->db->update('academic_year', $countryInfo);
    //     return $this->db->affected_rows();
    // }
}

