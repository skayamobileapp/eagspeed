<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Exam_component_model extends CI_Model
{
   

    function examcomponentList()
    {
        $this->db->select('*');
        $this->db->from('exam_components');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

   
    function getExamComponent($id)
    {
        $this->db->select('*');
        $this->db->from('exam_components');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewExamComponent($data)
    {
        $this->db->trans_start();
        $this->db->insert('exam_components', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editExamComponentDetails($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('exam_components', $data);
        return TRUE;
    }
}

