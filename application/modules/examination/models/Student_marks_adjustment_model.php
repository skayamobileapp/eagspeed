<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Student_marks_adjustment_model extends CI_Model
{
   
     function programListByStatus()
    {
        $this->db->select('*');
        $this->db->from('programme');
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

       function getProgrammeListByStatus($status)
    {
         $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }


    function  getstudentandprogrammedetails($id)
    {
        $this->db->select('s.*, p.code as programme_code, p.name as programme_name, c.registration_number as company_registration_number, c.name as company_name, ihs.id as id_student_has_programme,ihs.start_date,ihs.end_date');
        $this->db->from('student_has_programme as ihs');
        $this->db->join('student as s', 'ihs.id_student = s.id');
        $this->db->join('programme as p', 'ihs.id_programme = p.id');
        $this->db->join('company as c', 's.id_company = c.id','left');
        $this->db->where('ihs.id', $id);
        $query = $this->db->get();

        return $query->row();
    }



     function getMarkDistributionByProgramme($id_programme)
    {
        $this->db->select('md.*, ec.name as exam_component');
        $this->db->from('marks_distribution as md');
        $this->db->join('exam_components as ec', 'md.id_exam_component = ec.id');
        $this->db->where('md.id_programme', $id_programme);
        $query = $this->db->get();
        return $query->result();
    }

    function getProgrammeDetailsById($id){
                $this->db->select('p.*,pt.name as coursetype,c.name as categoryname,pu.name as partneruniversityname');
        $this->db->from('programme as p');
        $this->db->join('product_type as pt', 'pt.id = p.id_programme_type','left');
        $this->db->join('partner_university as pu', 'pu.id = p.id_partner_university','left');
        $this->db->join('category as c', 'c.id = p.id_category');


        $this->db->where('p.id', $id);

        $query = $this->db->get();
        $result = $query->row();
        return $result;
    }


    function getStudentListPast($fromdate,$idprogramme) {
         $this->db->select('md.*');
        $this->db->from('student_has_programme as md');
        $this->db->where('md.id_programme', $idprogramme);
        $this->db->where('date(md.end_date)<=', $fromdate);
        $query = $this->db->get();
        return $query->result();
    }

  


    function getStudentListInprogress($fromdate,$idprogramme) {
         $this->db->select('md.*');
        $this->db->from('student_has_programme as md');
        $this->db->where('md.id_programme', $idprogramme);
        $this->db->where('date(md.start_date)<=', $fromdate);
        $this->db->where('date(md.end_date)>=', $fromdate);
        $query = $this->db->get();
        return $query->result();
    }


    function getStudentListInprogressByProgrammeId($idprogramme,$fromdate) {
         $this->db->select('shp.*,s.full_name,s.nric,s.email_id');
        $this->db->from('student_has_programme as shp');
        $this->db->join('student as s', 'shp.id_student = s.id');

        $this->db->where('shp.id_programme', $idprogramme);
        $this->db->where('date(shp.start_date)<=', $fromdate);
        $this->db->where('date(shp.end_date)>=', $fromdate);
        $query = $this->db->get();
                return $query->result();
    }


    function getStudentListYettostart($fromdate,$idprogramme) {
         $this->db->select('md.*');
        $this->db->from('student_has_programme as md');
        $this->db->where('md.id_programme', $idprogramme);
        $this->db->where('date(md.start_date)<=', $fromdate);
        $query = $this->db->get();
        return $query->result();
    }


  function addNewMarksEntry($data)
    {
        $this->db->trans_start();
        $this->db->insert('student_has_programme_marks', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }
    

    function getMarks($id,$idstudenthasprog) {
         $this->db->select('md.*');
        $this->db->from('student_has_programme_marks as md');
        $this->db->where('md.id_marks_distribution', $id);
        $this->db->where('md.id_student_has_programme', $idstudenthasprog);
        $query = $this->db->get();
        return $query->result();
    }
    
    
    function getStudentListForCourseRegisteredStudent($id)
    {
        $this->db->select('DISTINCT(s.id) as id, s.*, p.code as programme_code, p.name as programme_name, c.registration_number as company_registration_number, c.name as company_name, ihs.id as id_student_has_programme,ihs.start_date,ihs.end_date');
        $this->db->from('student_has_programme as ihs');
        $this->db->join('student as s', 'ihs.id_student = s.id');
        $this->db->join('programme as p', 'ihs.id_programme = p.id');
        $this->db->join('company as c', 's.id_company = c.id','left');
        $this->db->where('ihs.id_programme', $id);
        $this->db->where("ihs.marks_status='Mark Adjustment'");
        $query = $this->db->get();

        return $query->result();
    }

    function getStudentHasProgramme($id)
    {
        $this->db->select('md.*');
        $this->db->from('student_has_programme as md');
        $this->db->where('md.id', $id);
        $this->db->order_by('md.id', 'DESC');
        $query = $this->db->get();
        $result = $query->row(); 

        return $result;
    }

    function addStudentMarksEntry($data)
    {
        $this->db->trans_start();
        $this->db->insert('student_marks_entry', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }



    function updateStudentHasProgramme($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('student_has_programme', $data);
        return TRUE;
    }
}