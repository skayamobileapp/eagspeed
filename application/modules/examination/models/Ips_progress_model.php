<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Ips_progress_model extends CI_Model
{
	function studentListSearch($applicantList)
    {
        $this->db->select('a.*, i.name as intake, p.code as program_code, p.name as program, el.name as education_level');
        $this->db->from('student as a');
        $this->db->join('intake as i', 'a.id_intake = i.id');
        $this->db->join('programme as p', 'a.id_program = p.id');
        $this->db->join('education_level el', 'p.id_education_level = el.id');
        if($applicantList['first_name']) {
            $likeCriteria = "(a.first_name  LIKE '%" . $applicantList['first_name'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($applicantList['last_name']) {
            $likeCriteria = "(a.last_name  LIKE '%" . $applicantList['last_name'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($applicantList['email_id']) {
            $likeCriteria = "(a.email_id  LIKE '%" . $applicantList['email_id'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($applicantList['nric']) {
            $likeCriteria = "(a.nric  LIKE '%" . $applicantList['nric'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($applicantList['id_program']) {
            $likeCriteria = "(a.id_program  LIKE '%" . $applicantList['id_program'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($applicantList['id_intake']) {
            $likeCriteria = "(a.id_intake  LIKE '%" . $applicantList['id_intake'] . "%')";
            $this->db->where($likeCriteria);
        }
        if($applicantList['current_deliverable'])
        {
            $this->db->where('a.current_deliverable', $applicantList['current_deliverable']);

            // $likeCriteria = "(a.current_deliverable  LIKE '%" . $applicantList['current_deliverable'] . "%')";
            // $this->db->where($likeCriteria);
        }
        $this->db->where('a.applicant_status !=', 'Graduated');
        $this->db->where('el.name', 'POSTGRADUATE');
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function postGraduateprogramListByStatus($status)
    {
    	$this->db->select('a.*');
        $this->db->from('programme as a');
        $this->db->join('education_level el', 'a.id_education_level = el.id');
        $this->db->where('el.name', 'POSTGRADUATE');
        $this->db->where('a.status', $status);
        $query = $this->db->get();
        return $query->result();
    }

    function intakeListByStatus($status)
    {
    	$this->db->select('a.*');
        $this->db->from('intake as a');
        $this->db->where('a.status', $status);
        $query = $this->db->get();
        return $query->result();
    }

    function studentDeliverablesList()
    {
        $this->db->select('DISTINCT(a.current_deliverable) as name');
        $this->db->from('student as a');
        $this->db->where('a.applicant_status !=', 'Graduated');
        $query = $this->db->get();
        return $query->result();
    }

    function getOrganisation()
    {
        $this->db->select('fc.*');
        $this->db->from('organisation as fc');
        $this->db->where('fc.status', 1);
        $this->db->order_by("fc.id", "DESC");
        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    function getStudentByStudentId($id_student)
    {
        $this->db->select('s.*, p.code as programme_code, p.name as programme_name, i.name as intake_name, st.type as supervisor_type, st.full_name as supervisor_name, ms.name as mailing_state, mc.name as mailing_country, ps.name as permanent_state, pc.name as permanent_country, rs.name as race, rels.name as religion, brch.code as branch_code, brch.name as branch_name, salut.name as salutation, pu.code as partner_university_code, pu.name as partner_university_name, sch.code as scheme_code, sch.description as scheme_name, q.name as qualification_name, q.short_name as qualification_code');
        $this->db->from('student as s');
        $this->db->join('programme as p', 's.id_program = p.id'); 
        $this->db->join('organisation_has_training_center as brch', 's.id_branch = brch.id','left'); 
        $this->db->join('salutation_setup as salut', 's.salutation = salut.id','left'); 
        $this->db->join('partner_university as pu', 's.id_university = pu.id','left'); 
        $this->db->join('scheme as sch', 's.id_program_has_scheme = sch.id','left'); 
        $this->db->join('intake as i', 's.id_intake = i.id'); 
        $this->db->join('state as ms', 's.mailing_state = ms.id'); 
        $this->db->join('country as mc', 's.mailing_country = mc.id');
        $this->db->join('state as ps', 's.permanent_state = ps.id'); 
        $this->db->join('country as pc', 's.permanent_country = pc.id'); 
        $this->db->join('race_setup as rs', 's.id_race = rs.id'); 
        $this->db->join('religion_setup as rels', 's.religion = rels.id','left');
        $this->db->join('research_supervisor as st', 's.id_supervisor = st.id','left'); 
        $this->db->join('education_level as q', 'p.id_education_level = q.id','left'); 
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getStudentData($id)
    {
        $this->db->select('a.*, in.name as intake, p.name as program, qs.name as qualification_name, qs.code as qualification_code, st.ic_no, st.name as advisor, brch.code as branch_code, brch.name as branch_name');
        $this->db->from('student as a');
        $this->db->join('intake as in', 'a.id_intake = in.id');
        $this->db->join('organisation_has_training_center as brch', 'a.id_branch = brch.id'); 
        $this->db->join('programme as p', 'a.id_program = p.id');
        $this->db->join('qualification_setup as qs', 'a.id_degree_type = qs.id');
        $this->db->join('staff as st', 'a.id_advisor = st.id','left');
        $this->db->where('a.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getStudentDetails($id)
    {
        $this->db->select('*');
        $this->db->from('student');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function addPostgraduateStudentProgress($data)
    {
        $this->db->trans_start();
        $this->db->insert('student_progress', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function getPostgraduateStudentProgressByIdStudent($id_student)
    {
        $this->db->select('a.*, u.name as created_by');
        $this->db->from('student_progress as a');
        $this->db->join('users as u', 'a.created_by = u.id','left');
        $this->db->where('a.id_student', $id_student);
        $query = $this->db->get();
        return $query->result();
    }


}
?>