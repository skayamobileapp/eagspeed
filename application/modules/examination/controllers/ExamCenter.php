<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ExamCenter extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('exam_center_model');
        $this->load->model('setup/country_model');
        $this->load->model('setup/state_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('exam_center.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        { 
            $formData['id_location'] = $this->security->xss_clean($this->input->post('id_location'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
 

            $data['searchParam'] = $formData;


            $data['examCenterList'] = $this->exam_center_model->examCenterListSearch($formData);
            $data['locationList'] = $this->exam_center_model->examCenterLocationList();
            // $name = $this->security->xss_clean($this->input->post('name'));

            // $data['searchName'] = $name;
            // $data['examCenterList'] = $this->exam_center_model->examCenterListSearch();


            $this->global['pageTitle'] = 'Campus Management System : Exam Centers';
            $this->loadViews("exam_center/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('exam_center.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;

                $name = $this->security->xss_clean($this->input->post('name'));
                $id_country = $this->security->xss_clean($this->input->post('id_country'));
                $id_state = $this->security->xss_clean($this->input->post('id_state'));
                $city = $this->security->xss_clean($this->input->post('city'));
                $zipcode = $this->security->xss_clean($this->input->post('zipcode'));
                $address = $this->security->xss_clean($this->input->post('address'));
                $status = $this->security->xss_clean($this->input->post('status'));
                $id_location = $this->security->xss_clean($this->input->post('id_location'));
                
                $data = array(
                   'name' => $name,
                    'id_country' => $id_country,
                    'id_state' => $id_state,
                    'city' => $city,
                    'zipcode' => $zipcode,
                    'address' => $address,
                    'status' => $status,
                    'id_location' => $id_location,
                    'created_by' => $user_id
                );

                $result = $this->exam_center_model->addExamCenter($data);
                redirect('/examination/examCenter/list');
            }

            $data['countryList'] = $this->country_model->countryList();
            $data['stateList'] = $this->state_model->stateList();
            $data['locationList'] = $this->exam_center_model->examCenterLocationListByStatus('1');


            $this->global['pageTitle'] = 'Campus Management System : Add Exam Center';
            $this->loadViews("exam_center/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('exam_center.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/examination/examCenter/list');
            }
            if($this->input->post())
            {
                $id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;
                
                $name = $this->security->xss_clean($this->input->post('name'));
                $id_country = $this->security->xss_clean($this->input->post('id_country'));
                $id_state = $this->security->xss_clean($this->input->post('id_state'));
                $city = $this->security->xss_clean($this->input->post('city'));
                $zipcode = $this->security->xss_clean($this->input->post('zipcode'));
                $address = $this->security->xss_clean($this->input->post('address'));
                $status = $this->security->xss_clean($this->input->post('status'));
                $id_location = $this->security->xss_clean($this->input->post('id_location'));
                
                $data = array(
                   'name' => $name,
                    'id_country' => $id_country,
                    'id_state' => $id_state,
                    'city' => $city,
                    'zipcode' => $zipcode,
                    'address' => $address,
                    'status' => $status,
                    'id_location' => $id_location,
                    'updated_by' => $user_id
                );

                // echo "<Pre>"; print_r($id);
                // exit;

                $result = $this->exam_center_model->editExamCenter($data,$id);
                redirect('/examination/examCenter/list');
            }
            $data['countryList'] = $this->country_model->countryList();
            $data['stateList'] = $this->state_model->stateList();
            $data['locationList'] = $this->exam_center_model->examCenterLocationListByStatus('1');
            
            $data['getExamCenterList'] = $this->exam_center_model->getExamCenterList($id);
            $this->global['pageTitle'] = 'Campus Management System : Edit Exam Center';
            $this->loadViews("exam_center/edit", $this->global, $data, NULL);
        }
    }


    function getStateByCountry($id_country)
    {
            $results = $this->exam_center_model->getStateByCountryId($id_country);

            // echo "<Pre>"; print_r($programme_data);exit;
            $table="   
                <script type='text/javascript'>
                     $('select').select2();
                 </script>
         ";

            $table.="
            <select name='id_state' id='id_state' class='form-control'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;
    }
}
