<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class DocumentChecklist extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('document_checklist_model');
        $this->load->model('category_type_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('document_checklist.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['documentList'] = $this->document_checklist_model->documentList();

            $this->global['pageTitle'] = 'Campus Management System : Document Checklist';
            //print_r($subjectDetails);exit;
            $this->loadViews("document_checklist/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('document_checklist.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            
            // print_r(expression)

            if($this->input->post())
            {
                $document_name = $this->security->xss_clean($this->input->post('document_name'));
                $id_category = $this->security->xss_clean($this->input->post('id_category'));
                $mandetory = $this->security->xss_clean($this->input->post('mandetory'));
                $id_program = $this->security->xss_clean($this->input->post('id_program'));

            
                $data = array(
                    'document_name' => $document_name,
                    'id_category' => $id_category,
                    'mandetory' => $mandetory,
                    'id_program' => $id_program
                );
            
                $result = $this->document_checklist_model->addNewDocument($data);
                redirect('/examination/documentChecklist/list');
            }

            $data['categoryTypeList'] = $this->category_type_model->categoryTypeList();
            $data['programList'] = $this->document_checklist_model->programList();

            $this->global['pageTitle'] = 'Campus Management System : Add Document Checklist';
            $this->loadViews("document_checklist/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('document_checklist.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/examination/documentChecklist/list');
            }
            if($this->input->post())
            {
                $document_name = $this->security->xss_clean($this->input->post('document_name'));
                $id_category = $this->security->xss_clean($this->input->post('id_category'));
                $mandetory = $this->security->xss_clean($this->input->post('mandetory'));
                $id_program = $this->security->xss_clean($this->input->post('id_program'));

            
                $data = array(
                    'document_name' => $document_name,
                    'id_category' => $id_category,
                    'mandetory' => $mandetory,
                    'id_program' => $id_program
                );

                $result = $this->document_checklist_model->editDocumentDetails($data,$id);
                redirect('/examination/documentChecklist/list');
            }
            $data['programList'] = $this->document_checklist_model->programList();
            $data['categoryTypeList'] = $this->category_type_model->categoryTypeList();
            $data['documentDetails'] = $this->document_checklist_model->getDocumentDetails($id);
            $this->global['pageTitle'] = 'Campus Management System : Edit Document Checklist';
            $this->loadViews("document_checklist/edit", $this->global, $data, NULL);
        }
    }
}
