<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class StudentMarksApproval extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('student_marks_entry_model');
        $this->load->model('setup/semester_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('student_marks_approval.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
             if($this->input->post())
            {


                $formData = $this->input->post();
                if($formData['Approve']=='Approve') {


                    for($i=0;$i<count($formData['results']);$i++) {
                         $id = $formData['results'][$i];
                         $data = array(
                            'status' => 1,
                        );
                        $result = $this->student_marks_entry_model->editStudentChangeStatus($data, $id);

                    }
                  $this->loadViews("student_marks_approval/list", $this->global, $data, NULL);


                }
                $data['resultData'] = $this->student_marks_entry_model->getApprovalList($formData);


            }
            $name='';
            $data['programmeList'] = $this->student_marks_entry_model->getProgrammeListByStatus('1');



            $this->global['pageTitle'] = 'Campus Management System : Receipt List';
            $this->loadViews("student_marks_approval/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        $user_id = $this->session->userId;
        if ($this->checkAccess('student_marks_entry.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {

                $formData = $this->input->post();

                $id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;

                $id_programme = $this->security->xss_clean($this->input->post('id_programme'));
                $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
                $main_grade = $this->security->xss_clean($this->input->post('main_grade'));
                $main_marks = $this->security->xss_clean($this->input->post('main_marks'));
                $main_result = $this->security->xss_clean($this->input->post('main_result'));
                $id_student = $this->security->xss_clean($this->input->post('id_student'));
                $id_semester = $this->security->xss_clean($this->input->post('id_semester'));

                $student_marks_entry_number = $this->student_marks_entry_model->generateReceiptNumber();

                $data = array(
                    'id_semester' => $id_semester,
                    'id_student' => $id_student,
                    'id_program' => $id_programme,
                    'id_intake' => $id_intake,
                    'grade' => $main_grade,
                    'marks' => $main_marks,
                    'result' => $main_result,
                    'status' => '0',
                    'created_by' => $user_id
                );
                // echo "<Pre>";print_r($data);exit;
                $inserted_id = $this->student_marks_entry_model->addNewMarksEntry($data);


                // $id_main_invoice = $this->security->xss_clean($this->input->post('id_main_invoice'));
                // $invoice_amount = $this->security->xss_clean($this->input->post('invoice_amount'));
                // $paid_amount = $this->security->xss_clean($this->input->post('paid_amount'));



                 for($i=0;$i<count($formData['id_course']);$i++)
                 {
                    $id_course = $formData['id_course'][$i];
                    $grade = $formData['grade'][$i];
                    $marks = $formData['marks'][$i];
                    $result = $formData['result'][$i];

                    if($marks > 0)
                    {
                        $detailsData = array(
                        'id_student_semester_result' => $inserted_id,
                        'id_course' => $id_course,
                        'grade' => $grade,
                        'marks' => $marks,                        
                        'result' => $result,
                        'status' => '0',
                        'created_by' => $user_id
                    );
                    $result = $this->student_marks_entry_model->addNewMarksEntryByCourse($detailsData);
                    }
                 }


                // $this->student_marks_entry_model->deleteTempDataBySession($id_session);
                // $this->student_marks_entry_model->deleteTempAmountDataBySession($id_session);

                redirect('/examination/student_marks_entry/add');
            }
            $data['programmeList'] = $this->student_marks_entry_model->getProgrammeListByStatus('1');
            $data['intakeList'] = $this->student_marks_entry_model->getIntakeListByStatus('1');
            $this->global['pageTitle'] = 'Campus Management System : Add Receipt';
            $this->loadViews("student_marks_entry/add", $this->global, $data, NULL);
        }
    }

     function searchStudentList()
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        // $tempData['id_semester'] = '';
        // $tempData['status'] = '';

        $student_list_data = $this->student_marks_entry_model->searchStudentList($tempData);
        // echo "<Pre>";print_r($student_list_data);exit;


        $table="<select name='id_student' id='id_student' class='form-control' onchange='getSemesterByStudentId(this.value)'>";
        $table.="<option value=''>Select</option> 

        <script type='text/javascript'>
            $('select').select2();
        </script>
                ";

        for($i=0;$i<count($student_list_data);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $student_list_data[$i]->id;
        $full_name = $student_list_data[$i]->full_name;
        $table.="<option value=".$id.">".$full_name.
                "</option>";

        }
        $table.="</select>";
        
        echo $table;
        exit();
    }

    function getSemesterByStudentId($id_student)
    {

        $semester_list_data = $this->student_marks_entry_model->searchSemesterByStudentList($id_student);
        // echo "<Pre>";print_r($semester_list_data);exit;


        $table="<select name='id_semester' id='id_semester' class='form-control' onchange='getCourse()'>";
        $table.="<option value=''>Select</option>


        <script type='text/javascript'>
            $('select').select2();
        </script>";

        for($i=0;$i<count($semester_list_data);$i++)
        {



        // $id = $results[$i]->id_procurement_category;
        $id = $semester_list_data[$i]->id;
        $semester_name = $semester_list_data[$i]->semester_name;
        $semester_code = $semester_list_data[$i]->semester_code;
        $table.="<option value=".$id.">".$semester_name.
                "</option>";

        }
        $table.="</select>";
        
        echo $table;
        exit();

    }

    function getCources()
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        // $tempData['id_semester'] = '';
        // $tempData['status'] = '';
        // echo "<Pre>";print_r($tempData);exit;

        if(empty($tempData['id_programme']) || empty($tempData['id_intake']) || empty($tempData['id_semester']) || empty($tempData['id_student']))
        {
            echo "Select The Proper Data Of The Student";exit();
        }

        if($tempData['id_programme'] == '' || $tempData['id_intake'] == '' || $tempData['id_semester'] == '' || $tempData['id_student'] == '')
        {
            echo "Select The Proper Data Of The Student";exit();
        }

        // echo "<Pre>";print_r($tempData);exit;
        $student_data = $this->student_marks_entry_model->getStudentInfo($tempData);
        // echo "<Pre>";print_r($student_data);exit;

        if(empty($student_data))
        {
            echo "Courses Not Available For This Student";exit();
        }

        $student_name = $student_data->full_name;
        $nric = $student_data->nric;
        $semester_name = $student_data->semester_name;
        $semester_code = $student_data->semester_code;
        // echo "<Pre>";print_r($student_name);exit;

        $table  = "
        <br>
            <table border='1px' style='width: 98%' align='center'>
                <tr>
                    <td colspan='4'><h5 style='text-align: center;'>Student Details</h5></td>
                </tr>
                <tr>
                    <th style='text-align: center;'>Student Name</th>
                    <td style='text-align: center;'>$student_name</td>
                    <th style='text-align: center;'>Semester Name</th>
                    <td style='text-align: center;'>$semester_name</td>
                </tr>
                <tr>
                    <th style='text-align: center;'>Student NRIC</th>
                    <td style='text-align: center;'>$nric</td>
                    <th style='text-align: center;'>Semester Code</th>
                    <td style='text-align: center;'>$semester_code</td>
                </tr>
            </table>
            <br>
            ";

        // echo "<Pre>";print_r($table);exit;


            $table .= "
            <h3>Course Details</h3>
            <div class='custom-table'>
                <table class='table' id='list-table'>
                  <tr>
                    <th>Sl. No</th>
                    <th >Course Name</th>
                    <th style='text-align: center;'>Grade</th>
                    <th style='text-align: center;'>Marks</th>
                    <th style='text-align: center;'>Result</th>
                </tr>

                ";

            // $invoice_data = $this->student_marks_entry_model->getInvoicesByStudentId($id);
            // echo "<Pre>";print_r($tempData);exit;
            $course_list_data = $this->student_marks_entry_model->getCourse($tempData);
            // echo "<Pre>";print_r($course_list_data);exit;

            for($i=0;$i<count($course_list_data);$i++)
                {
                    $id_exam_registration = $course_list_data[$i]->id_exam_registration;
                    $id_course = $course_list_data[$i]->id;
                    $course_name = $course_list_data[$i]->name;
                    $course_code = $course_list_data[$i]->code;
                    $course = $course_code . " - " . $course_name;
                    $j=$i+1;
                    $table .= "

                    <script type='text/javascript'>
                        $('select').select2();
                    </script>
                <tr>
                    <td>
                    $j
                    <input type='hidden' class='form-control' id='id_course[]' name='id_course[]' value='$id_course' readonly='readonly'/>
                    </td>

                    <td >
                    $course
                    </td>

                    <td style='text-align: center;'>

                        <div class='form-group'>
                            <select name='grade[]' id='grade[]' class='form-control'>
                                <option value=''>Select</option>
                                <option value='A'>A</option>
                                <option value='B'>B</option>
                                <option value='C'>C</option>
                            </select>
                        </div>
                    </td>

                    <td style='text-align: center;'>
                        <div class='form-group'>
                            <input type='number' class='form-control' id='marks[]' name='marks[]' >
                        </div>
                    </td>

                    <td style='text-align: center;'>
                        <div class='form-group'>
                            <select name='result[]' id='result[]' class='form-control'>
                                <option value=''>Select</option>
                                <option value='Pass'>Pass</option>
                                <option value='Fail'>Fail</option>
                            </select>
                        </div>
                    </td>

                </tr>";
                }
                        
            $table .= "
            </table>";


            echo $table;
            exit;
    }







    function approvalList()
    {
        // if ($this->checkAccess('pr_entry_approval.list') == 0)
        if ($this->checkAccess('student_marks_entry_approval.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        { 
            
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;
            // echo "<Pre>";print_r("ss");exit();
            $data['student_marks_entryList'] = $this->student_marks_entry_model->getReceiptListByStatus('0',$name);
            
                $array = $this->security->xss_clean($this->input->post('checkvalue'));
                if (!empty($array))
                {
 // echo "<Pre>"; print_r($array);exit;
                    $result = $this->student_marks_entry_model->editReceiptList($array);
                    redirect($_SERVER['HTTP_REFERER']);
                }


            $this->global['pageTitle'] = 'Campus Management System : Approve Receipt';
            $this->loadViews("student_marks_entry/approval_list", $this->global, $data, NULL);
        }
    }
}
