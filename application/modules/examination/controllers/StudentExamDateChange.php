<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class StudentExamDateChange extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('student_marks_entry_model');
        $this->load->model('prdtm/programme_model');
        // $this->load->model('main_invoice_model');
        $this->isLoggedIn();
    }

     function list()
    {
        if ($this->checkAccess('programme.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_category'] = $this->security->xss_clean($this->input->post('id_category'));
            $formData['id_category_setup'] = $this->security->xss_clean($this->input->post('id_category_setup'));
            $formData['id_programme_type'] = $this->security->xss_clean($this->input->post('id_programme_type'));
            $formData['id_partner_university'] = $this->security->xss_clean($this->input->post('id_partner_university'));

            $data['searchParam'] = $formData;

            $data['categoryList'] = $this->programme_model->categoryListByStatus('1');
            $data['productTypeSetupList'] = $this->programme_model->productTypeSetupListByStatus('1');
            $data['organisationDetails'] = $this->programme_model->getOrganisation();

            $data['programmeList'] = $this->programme_model->programmeListSearch($formData);



            $this->global['pageTitle'] = 'Campus Management System : Program List';
            $this->loadViews("student_exam_date_change/list", $this->global, $data, NULL);
        }
    }


    function studentList($id = Null)
    {
        if ($this->checkAccess('student_marks_entry.student_list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $user_id = $this->session->userId;
            $date = date('Y-m-d');

                $data['applicantList'] = $this->student_marks_entry_model->getStudentListInprogressByProgrammeId($id,$date);

            if($this->input->post())
            {
                $postData = $this->input->post();
                    
                    
                    
                for($i=0;$i<count($postData['studentids']);$i++) {

                    $shpid = $postData['studentids'][$i];
                    $start_date = date('Y-m-d',strtotime($postData['start_date'][$i]));
                    $end_date = date('Y-m-d',strtotime($postData['end_date'][$i]));


                $data = array(
                    'start_date' => date('Y-m-d',strtotime($start_date)),
                    'end_date' => date('Y-m-d',strtotime($end_date))
                );
                $result = $this->student_marks_entry_model->updateStudentHasProgramme($data,$shpid);


                }

                    redirect('/examination/studentExamDateChange/list');


            }


                $data['programmeDetails'] = $this->student_marks_entry_model->getProgrammeDetailsById($id);


            $this->global['pageTitle'] = 'Campus Management System : Receipt List';
            $this->loadViews("student_exam_date_change/student_list", $this->global, $data, NULL);
        }
    }
}