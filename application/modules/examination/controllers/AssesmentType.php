<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class AssesmentType extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('assesment_type_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('assesment_type.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['type'] = $this->security->xss_clean($this->input->post('type'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_semester'] = $this->security->xss_clean($this->input->post('id_semester'));
            $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
            $formData['status'] = '';
 
            $data['searchParam'] = $formData;


            $data['intakeList'] = $this->assesment_type_model->intakeListByStatus('1');
            $data['semesterList'] = $this->assesment_type_model->semesterListByStatus('1');
            $data['assesmentTypeList'] = $this->assesment_type_model->assesmentTypeListSearch($formData);

            $this->global['pageTitle'] = 'Campus Management System : Assesment Type List';
            $this->loadViews("assesment_type/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('assesment_type.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $id_user = $this->session->userId;

            if($this->input->post())
            {
                $type_id = $this->security->xss_clean($this->input->post('type_id'));
                $type = $this->security->xss_clean($this->input->post('type'));
                $exam = $this->security->xss_clean($this->input->post('exam'));
                $id_semester = $this->security->xss_clean($this->input->post('id_semester'));
                $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $description_optional_language = $this->security->xss_clean($this->input->post('description_optional_language'));
                $order = $this->security->xss_clean($this->input->post('order'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'type_id' => $type_id,
                    'type' => $type,
                    'exam' => $exam,
                    'id_semester' => $id_semester,
                    'id_intake' => $id_intake,
                    'description' => $description,
                    'description_optional_language' => $description_optional_language,
                    'order' => $order,
                    'status' => $status,
                    'created_by' => $id_user
                );

                $inserted_id = $this->assesment_type_model->addNewAssesmentType($data);
                redirect('/examination/assesmentType/list');
            }
            
            $data['semesterList'] = $this->assesment_type_model->semesterListByStatus('1');            
            $data['intakeList'] = $this->assesment_type_model->intakeListByStatus('1');

            // echo "<Pre>";print_r($data['partnerCategoryList']);exit();

            $this->global['pageTitle'] = 'Campus Management System : Add Assesment Type';
            $this->loadViews("assesment_type/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('assesment_type.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/examination/assesmentType/list');
            }
            if($this->input->post())
            {
                $id_session = $this->session->my_session_id;
                $id_user = $this->session->userId;

                $type_id = $this->security->xss_clean($this->input->post('type_id'));
                $type = $this->security->xss_clean($this->input->post('type'));
                $exam = $this->security->xss_clean($this->input->post('exam'));
                $id_semester = $this->security->xss_clean($this->input->post('id_semester'));
                $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $description_optional_language = $this->security->xss_clean($this->input->post('description_optional_language'));
                $order = $this->security->xss_clean($this->input->post('order'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'type_id' => $type_id,
                    'type' => $type,
                    'exam' => $exam,
                    'id_semester' => $id_semester,
                    'id_intake' => $id_intake,
                    'description' => $description,
                    'description_optional_language' => $description_optional_language,
                    'order' => $order,
                    'status' => $status,
                    'updated_by' => $id_user
                );

                // echo "<Pre>";print_r($data);exit;
                $result = $this->assesment_type_model->editAssesmentType($data,$id);
                redirect('/examination/assesmentType/list');
            }

            $data['assesmentType'] = $this->assesment_type_model->getAssesmentType($id);
            $data['semesterList'] = $this->assesment_type_model->semesterListByStatus('1');
            $data['intakeList'] = $this->assesment_type_model->intakeListByStatus('1');

            $this->global['pageTitle'] = 'Campus Management System : Edit Assesment Type';
            $this->loadViews("assesment_type/edit", $this->global, $data, NULL);
        }
    }
}
