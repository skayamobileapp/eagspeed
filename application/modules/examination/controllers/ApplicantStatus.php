<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ApplicantStatus extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('applicant_status_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('applicant_status.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['applicantStatusList'] = $this->applicant_status_model->applicantStatusList();

            $this->global['pageTitle'] = 'Campus Management System : Applicant Status';
            //print_r($subjectDetails);exit;
            $this->loadViews("applicant_status/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('applicant_status.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            
            // print_r(expression)

            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));

            
                $data = array(
                    'name' => $name

                );
            
                $result = $this->applicant_status_model->addNewApplicantStatus($data);
                redirect('/examination/applicantStatus/list');
            }

            $this->global['pageTitle'] = 'Campus Management System : Add Applicant Status';
            $this->loadViews("applicant_status/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('applicant_status.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/examination/applicantStatus/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));

                $data = array(
                    'name' => $name
                );
                $result = $this->applicant_status_model->editApplicantStatusDetails($data,$id);
                redirect('/examination/applicantStatus/list');
            }
            $data['applicantStatusDetails'] = $this->applicant_status_model->getApplicantStatusDetails($id);
            $this->global['pageTitle'] = 'Campus Management System : Edit Applicant Status';
            $this->loadViews("applicant_status/edit", $this->global, $data, NULL);
        }
    }
}
