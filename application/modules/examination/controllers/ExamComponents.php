<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ExamComponents extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('exam_component_model');
        $this->isLoggedIn();
    }

    function list()
    {

        if ($this->checkAccess('grade_setup.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            
            $data['gradeList'] = $this->exam_component_model->examcomponentList();

            $this->global['pageTitle'] = 'Campus Management System : Grade';
            $this->loadViews("exam_components/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('grade_setup.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
              
                $data = array(
                    'name' => $name,
                    'code' => $code,
                );
            
                $result = $this->exam_component_model->addNewExamComponent($data);
                redirect('/examination/examComponents/list');
            }
            $data['name'] = '';

            
            $this->global['pageTitle'] = 'Campus Management System : Add Grade';
            $this->loadViews("exam_components/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('grade_setup.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/examination/exam_components/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
               
                $data = array(
                    'name' => $name,
                    'code' => $code,
                );
                
                $result = $this->exam_component_model->editExamComponentDetails($data,$id);
                redirect('/examination/examComponents/list');
            }
            
            $data['grade'] = $this->exam_component_model->getExamComponent($id);

            $this->global['pageTitle'] = 'Campus Management System : Edit Grade';
            $this->loadViews("exam_components/edit", $this->global, $data, NULL);
        }
    }
}
