<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class CourseGrade extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('course_grade_model');
        $this->load->model('grade_model');
        $this->load->model('setup/programme_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('course_grade.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));
            $formData['id_course'] = $this->security->xss_clean($this->input->post('id_course'));
            $formData['id_grade'] = $this->security->xss_clean($this->input->post('id_grade'));

            $data['searchParam'] = $formData;

            $data['programmeList'] = $this->course_grade_model->getProgrammeListByStatus('1');
            $data['courseList'] = $this->course_grade_model->courseListByStatus('1');
            $data['gradeList'] = $this->course_grade_model->gradeListByStatus('1');


            $data['courseGradeList'] = $this->course_grade_model->courseGradeListSearch($formData);
            $this->global['pageTitle'] = 'Campus Management System : Course Grade';
            //print_r($subjectDetails);exit;
            $this->loadViews("course_grade/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('course_grade.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_program = $this->security->xss_clean($this->input->post('id_program'));
                $id_course = $this->security->xss_clean($this->input->post('id_course'));
                $id_grade = $this->security->xss_clean($this->input->post('id_grade'));
                $minmarks = $this->security->xss_clean($this->input->post('minmarks'));
                $maxmarks = $this->security->xss_clean($this->input->post('maxmarks'));
                $result = $this->security->xss_clean($this->input->post('result'));
            
                $data = array(
                    'id_program' => $id_program,
                    'id_course' => $id_course,
                    'id_grade' => $id_grade,
                    'minmarks' => $minmarks,
                    'maxmarks' => $maxmarks,
                    'result' => $result
                );
            
                $result = $this->course_grade_model->addNewCourseGrade($data);
                redirect('/examination/courseGrade/list');
            }

            $data['programList'] = $this->programme_model->programmeList();
            $data['gradeList'] = $this->grade_model->gradeList();
            $data['courseList'] = $this->course_grade_model->courseListByStatus('1');
            $this->global['pageTitle'] = 'Campus Management System : Add Course Grade';
            $this->loadViews("course_grade/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('course_grade.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/examination/courseGrade/list');
            }
            if($this->input->post())
            {
                $id_program = $this->security->xss_clean($this->input->post('id_program'));
                $id_course = $this->security->xss_clean($this->input->post('id_course'));
                $id_grade = $this->security->xss_clean($this->input->post('id_grade'));
                $minmarks = $this->security->xss_clean($this->input->post('minmarks'));
                $maxmarks = $this->security->xss_clean($this->input->post('maxmarks'));
                $result = $this->security->xss_clean($this->input->post('result'));
            
                $data = array(
                    'id_program' => $id_program,
                    'id_course' => $id_course,
                    'id_grade' => $id_grade,
                    'minmarks' => $minmarks,
                    'maxmarks' => $maxmarks,
                    'result' => $result
                );

                $result = $this->course_grade_model->editCourseGradeDetails($data,$id);
                redirect('/examination/courseGrade/list');
            }
            $data['programList'] = $this->programme_model->programmeList();
            $data['courseList'] = $this->course_grade_model->courseList();
            $data['gradeList'] = $this->grade_model->gradeList();
            
            $data['coursegradeList'] = $this->course_grade_model->getCourseGradeDetails($id);
            $this->global['pageTitle'] = 'Campus Management System : Edit Course Grade';
            $this->loadViews("course_grade/edit", $this->global, $data, NULL);
        }
    }
}
