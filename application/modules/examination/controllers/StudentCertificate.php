<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class StudentCertificate extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('student_marks_entry_model');
        $this->load->model('prdtm/programme_model');
        // $this->load->model('main_invoice_model');
        $this->isLoggedIn();
    }

     function list()
    {
        if ($this->checkAccess('programme.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_category'] = $this->security->xss_clean($this->input->post('id_category'));
            $formData['id_category_setup'] = $this->security->xss_clean($this->input->post('id_category_setup'));
            $formData['id_programme_type'] = $this->security->xss_clean($this->input->post('id_programme_type'));
            $formData['id_partner_university'] = $this->security->xss_clean($this->input->post('id_partner_university'));

            $data['searchParam'] = $formData;

            $data['categoryList'] = $this->programme_model->categoryListByStatus('1');
            $data['productTypeSetupList'] = $this->programme_model->productTypeSetupListByStatus('1');
            $data['organisationDetails'] = $this->programme_model->getOrganisation();

            $data['programmeList'] = $this->programme_model->programmeListSearch($formData);



            $this->global['pageTitle'] = 'Campus Management System : Program List';
            $this->loadViews("student_certificate/list", $this->global, $data, NULL);
        }
    }


    function studentList($id = Null)
    {
        if ($this->checkAccess('student_marks_entry.student_list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $user_id = $this->session->userId;

                $data['applicantList'] = $this->student_marks_entry_model->getStudentListForCourseRegisteredStudent($id);
                $data['componentList'] = $this->student_marks_entry_model->getMarkDistributionByProgramme($id);


            $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));    

            if($this->input->post())
            {
                $postData = $this->input->post();
                    
                    for($s=0;$s<count($_POST['id_student_has_programme']);$s++) {
                        $id_student_has_programme = $_POST['id_student_has_programme'][$s];

                        for($c=0;$c<count($_POST['maincomponentarray']);$c++) {

                            //component Id
                            $componentId = $_POST['maincomponentarray'][$c];

                            //Markss
                            $marksforcomponent  = $_POST['component_marks_obtained'][$id_student_has_programme][$c];


                              $data = array(
                    'id_student_has_programme' => $id_student_has_programme,
                    'id_component' => $componentId,
                    'marks_obtained' => $marksforcomponent
                );
            
                $this->student_marks_entry_model->addNewMarksEntry($data);

                        }

                    
                }



            }


                $data['programmeDetails'] = $this->student_marks_entry_model->getProgrammeDetailsById($id);



            $this->global['pageTitle'] = 'Campus Management System : Receipt List';
            $this->loadViews("student_certificate/student_list", $this->global, $data, NULL);
        }
    }

    function viewcertachievement($id) {

        $details = $this->student_marks_entry_model->getstudentandprogrammedetails($id);

                   $font = "/var/www/html/speed/assets/fonts/cambria/cambriab.ttf";


// Load And Create Image From Source
$dest = imagecreatefromjpeg('/var/www/html/speed/assets/Achievement.jpg');
 $white = imagecolorallocate($dest, 0, 0, 0);
// imagettftext($dest, 25, 0, 200, 750, $white, $font, $firstrow);
$intake =strtoupper($details->full_name);
  imagettftext($dest, 25, 0, 160, 580, $white, $font, $intake);


$course = strtoupper($details->programme_name);
  imagettftext($dest, 25, 0, 160, 750, $white, $font, $course);

$course = date('dS M',strtotime($details->start_date)).' - '.date('dS M Y',strtotime($details->end_date));
  imagettftext($dest, 25, 0, 160, 820, $white, $font, $course);


              $rand = rand(0000000000,9999999999);
                            imagejpeg($dest,$rand.".jpeg");

//               header("Content-Type: image/jpeg");
// header("Content-Length: " . filesize($dest,$rand.".jpeg"));
// imagejpeg($dest,$rand.".jpeg");


$filename = "/var/www/html/speed/".$rand.".jpeg"; 
$handle = fopen($filename, "rb"); 
$contents = fread($handle, filesize($filename)); 
fclose($handle); 
 
header("content-type: image/jpeg"); 
 
echo $contents; 




    }

    function viewcert($id) {

        $details = $this->student_marks_entry_model->getstudentandprogrammedetails($id);

                   $font = "/var/www/html/speed/assets/fonts/cambria/cambriab.ttf";


// Load And Create Image From Source
$dest = imagecreatefromjpeg('/var/www/html/speed/assets/Certificate.jpg');
 $white = imagecolorallocate($dest, 0, 0, 0);
// imagettftext($dest, 25, 0, 200, 750, $white, $font, $firstrow);
$intake =strtoupper($details->full_name);
  imagettftext($dest, 25, 0, 160, 580, $white, $font, $intake);


$course = strtoupper($details->programme_name);
  imagettftext($dest, 25, 0, 160, 750, $white, $font, $course);

$course = date('dS M',strtotime($details->start_date)).' - '.date('dS M Y',strtotime($details->end_date));
  imagettftext($dest, 25, 0, 160, 820, $white, $font, $course);


              $rand = rand(0000000000,9999999999);
                            imagejpeg($dest,$rand.".jpeg");

//               header("Content-Type: image/jpeg");
// header("Content-Length: " . filesize($dest,$rand.".jpeg"));
// imagejpeg($dest,$rand.".jpeg");


$filename = "/var/www/html/speed/".$rand.".jpeg"; 
$handle = fopen($filename, "rb"); 
$contents = fread($handle, filesize($filename)); 
fclose($handle); 
 
header("content-type: image/jpeg"); 
 
echo $contents; 




    }
}