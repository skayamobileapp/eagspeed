<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class RemarkingApplication extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('remarking_application_model');
        // $this->load->model('main_invoice_model');
        $this->isLoggedIn();
    }


    

    function list()
    {
        if ($this->checkAccess('remarking_application.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            if($this->input->post())
            {

                $formData = $this->input->post();
                $btnvalue = $formData['btn_submit'];
                
                if($btnvalue == 2)
                {
                    for($i=0;$i<count($formData['id_mark_entry']);$i++)
                    {
                        $id_mark_entry = $formData['id_mark_entry'][$i];

                        $mark_entry = $this->remarking_application_model->getStudentMarksEntry($id_mark_entry);
                        $mark_entry->id_student_marks_entry = $id_mark_entry;
                        $mark_entry->status = 0;

                        unset($mark_entry->is_remarking);
                        unset($mark_entry->id);

                        $added = $this->remarking_application_model->addStudentReMarksEntry($mark_entry);
                        // echo "<Pre>";print_r($added);exit;

                        if($added)
                        {
                            $this->remarking_application_model->updateMarkEntryCount($id_mark_entry,$added);
                        }

                    }
                }
            }

            $formData['id_course_registered_landscape'] = $this->security->xss_clean($this->input->post('id_course_registered_landscape'));

            $data['searchParam'] = $formData;

            $data['studentMarksEntryList'] = $this->remarking_application_model->getStudentMarkEntryList($formData);

            $data['courseRegisteredLandscapeList'] = $this->remarking_application_model->getCoursesRegisteredLandscape('1');
            // echo "<Pre>";print_r($data['studentMarksEntryList']);exit;

            $this->global['pageTitle'] = 'Campus Management System : Publish Assesment Result Date List';
            $this->loadViews("remarking_application/list", $this->global, $data, NULL);
        }
    }

    function add()
    {
        // if ($this->checkAccess('pr_entry_approval.list') == 0)
        if ($this->checkAccess('remarking_application.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        { 
            
             if($this->input->post())
            {

            // echo "<Pre>";print_r($this->input->post());exit;

            $id_mark_entry = $this->security->xss_clean($this->input->post('id_mark_entry'));


            $mark_entry = $this->remarking_application_model->getStudentMarksEntry($id_mark_entry);
            $mark_entry->id_student_marks_entry = $id_mark_entry;
            $mark_entry->status = 0;

            unset($mark_entry->is_remarking);
            unset($mark_entry->id);

            $added = $this->remarking_application_model->addStudentReMarksEntry($mark_entry);

            if($added)
            {
                $this->remarking_application_model->updateMarkEntryCount($id_mark_entry,$added);

                // Hided Due To New Invoice Generation
                // $this->remarking_application_model->addMainInvoice($id_mark_entry,$added);

                $id_program = $mark_entry->id_program;

                $check_apply_status = $this->remarking_application_model->getFeeStructureActivityType('EXAMINATION REMARKING','Application Level',$id_program);

                // echo "<Pre>";print_r($check_apply_status);exit();


                if($check_apply_status)
                {
                    $data['add'] = 1;
                    $data['id_student'] = $mark_entry->id_student;
                    $this->remarking_application_model->generateMainInvoice($data,$added);
                }


            }


                redirect('/examination/remarkingApplication/list');
            }


            // echo "<Pre>";print_r($data['semesterResultList']);exit;


            $data['programmeList'] = $this->remarking_application_model->programListByStatus('1');
            $data['intakeList'] = $this->remarking_application_model->intakeListByStatus('1');
            $data['studentList'] = $this->remarking_application_model->gradeListByStatus('1');



            $this->global['pageTitle'] = 'Campus Management System : Approve Student ReMarks Entry';
            $this->loadViews("remarking_application/add", $this->global, $data, NULL);
        }

    }




    function remarkingList()
    {
        // if ($this->checkAccess('pr_entry_approval.list') == 0)
        if ($this->checkAccess('remarking_application.entry_list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        { 
            
            // echo "<Pre>";print_r("ss");exit();
            $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));
            $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['status'] = '';
            $formData['updated'] = '0';



            $data['searchParam'] = $formData;

            $data['marksEntryList'] = $this->remarking_application_model->marksReEntryListSearch($formData);





            $data['programmeList'] = $this->remarking_application_model->programListByStatus('1');
            $data['intakeList'] = $this->remarking_application_model->intakeListByStatus('1');
            // $data['studentList'] = $this->remarking_application_model->gradeListByStatus('1');

            // echo "<Pre>";print_r($data['studentList']);exit;


            $this->global['pageTitle'] = 'Campus Management System : Approve Student ReMarks Entry';
            $this->loadViews("remarking_application/remarks_entry_list", $this->global, $data, NULL);
        }

    }


    function edit($id_remark_entry)
    {
        // echo "<Pre>";print_r($id_student);exit;

        $user_id = $this->session->userId;
        if ($this->checkAccess('remarking.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id_remark_entry == null)
            {
                redirect('/examination/remarkingApplication/remarkingList');
            }

            if($this->input->post())
            {

                $formData = $this->input->post();
                // echo "<Pre>";print_r($formData);exit;

                $id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;

                $grade = $this->security->xss_clean($this->input->post('grade'));

                // $remarking_application_number = $this->remarking_application_model->generateReceiptNumber();

                $data = array(
                    'grade' => $grade,
                    'updated' => 1,
                    'updated_by' => $user_id
                );
                // echo "<Pre>";print_r($data['studentDetails']);exit;
                $inserted_id = $this->remarking_application_model->editStudentRemark($data,$id_remark_entry);

                // echo "<Pre>";print_r($inserted_id);exit;

                $remark_entry = $this->remarking_application_model->getStudentMarksEntry($id_remark_entry);
                
                $id_student = $remark_entry->id_student;
                $id_course_registration = $remark_entry->id_course_registration;



                $total_course_marks = 0;
                $total_obtained_marks = 0;
                $total_min_pass_marks = 0;

                if($inserted_id)
                {

                     for($i=0;$i<count($formData['obtained_marks']);$i++)
                     {
                        $obtained_marks = $formData['obtained_marks'][$i];
                    
                        // echo "<Pre>";print_r($obtained_marks);exit;

                        if($obtained_marks > 0)
                        {


                            $id_mark_details = $formData['id_details'][$i];

                            $mark_details = $this->remarking_application_model->getStudentMarkingDetails($id_mark_details);
                            $pass_marks = $mark_details->pass_marks;
                            $max_marks = $mark_details->max_marks;


                            $result = 'Pass';

                            if($obtained_marks < $pass_marks)
                            {
                                $result = 'Fail';
                            }
                            
                                $detailsData = array(
                                'id_student_remarks_entry' => $id_remark_entry,
                                'id_marks_distribution_details' => $mark_details->id_marks_distribution_details,
                                'max_marks' => $mark_details->max_marks,
                                'obtained_marks' => $obtained_marks,
                                'pass_marks' => $mark_details->pass_marks,
                                'id_component' => $mark_details->id_component,
                                'result' => $result,
                                'status' => '1',
                                'created_by' => $user_id
                            );

                            $inserted_detail = $this->remarking_application_model->addStudentReMarksEntryDetails($detailsData);
                            // echo "<Pre>";print_r($inserted_detail);exit;

                            $total_course_marks = $total_course_marks + $max_marks;
                            $total_obtained_marks = $total_obtained_marks + $obtained_marks;
                            $total_min_pass_marks = $total_min_pass_marks + $pass_marks;
                             

                        }

                    }

                    $total_result = 'Pass';
                    if($total_obtained_marks < $total_min_pass_marks)
                    {
                        $total_result = 'Fail';
                    }

                    $student_marks_data = array(
                        'total_course_marks' => $total_course_marks,
                        'total_obtained_marks' => $total_obtained_marks,
                        'total_min_pass_marks' => $total_min_pass_marks,
                        'result' => $total_result
                    );

                    $updated_student_remarks_data = $this->remarking_application_model->updateStudentReMarksEntry($student_marks_data,$id_remark_entry);

                    
                    if($updated_student_remarks_data)
                    {

                        

                        $exam_marks_data = array(
                            'total_course_marks' => $total_course_marks,
                            'total_obtained_marks' => $total_obtained_marks,
                            'total_min_pass_marks' => $total_min_pass_marks,
                            'total_result' => $total_result,
                            'is_result_announced' => $id_remark_entry,
                            'grade' => $grade,
                            'updated_by' => $user_id
                        );

                        $add_course_marks_history = $this->remarking_application_model->combineDataAddMarkHistoryOfStudent($exam_marks_data,$id_course_registration,$id_student,$id_remark_entry);
                        
                        
                        if($add_course_marks_history)
                        {

                        $updated_exam_reg = $this->remarking_application_model->updateCourseRegistration($exam_marks_data,$id_course_registration);
                        }
                        // echo "<Pre>";print_r($updated_exam_reg);exit;
                    }

                }


                // $this->remarking_application_model->deleteTempDataBySession($id_session);
                // $this->remarking_application_model->deleteTempAmountDataBySession($id_session);

                redirect('/examination/remarkingApplication/remarkingList');
            }


            $data['marksEntry'] = $this->remarking_application_model->getRemarkEntry($id_remark_entry);
            $data['studentData'] = $this->remarking_application_model->getStudentByStudent($data['marksEntry']->id_student);

            $data['marksEntryDetails'] = $this->remarking_application_model->getStudentMarksEntryDetailsByMasterId($data['marksEntry']->id_student_marks_entry);
            $data['courseRegisteredDetails'] = $this->remarking_application_model->getCourseRegisteredDetails($data['marksEntry']->id_course_registered_landscape);
            $data['studentDetails'] = $this->remarking_application_model->getStudentByStudent($data['marksEntry']->id_student);

                // echo "<Pre>";print_r($data['marksEntryDetails']);exit;

            $data['gradeList'] = $this->remarking_application_model->gradeListByStatus('1');

            
                // echo "<Pre>";print_r($data['marksEntryDetails']);exit;

            $this->global['pageTitle'] = 'Campus Management System : Add Receipt';
            $this->loadViews("remarking_application/view", $this->global, $data, NULL);
        }
    }

    function viewSummary($id_remark_entry = NULL)
    {
        if ($this->checkAccess('remarking_application.view') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $user_id = $this->session->userId;

            if ($id_remark_entry == null)
            {
                redirect('/examination/remarkingApplication/summaryList');
            }

             if($this->input->post())
            {

            }


            $data['marksEntry'] = $this->remarking_application_model->getRemarkEntry($id_remark_entry);
            $data['studentData'] = $this->remarking_application_model->getStudentByStudent($data['marksEntry']->id_student);

            $data['marksEntryDetails'] = $this->remarking_application_model->getStudentReMarksEntryDetailsByMasterId($id_remark_entry);
            $data['courseRegisteredDetails'] = $this->remarking_application_model->getCourseRegisteredDetails($data['marksEntry']->id_course_registered_landscape);
            $data['studentDetails'] = $this->remarking_application_model->getStudentByStudent($data['marksEntry']->id_student);

            // echo "<Pre>";print_r($data['marksEntryDetails']);exit();

            $this->global['pageTitle'] = 'Campus Management System : View Marks Entry';
            $this->loadViews("student_marks_entry/view_summary", $this->global, $data, NULL);
        }
    }


    function marksEntryApprovalList()
    {
        // if ($this->checkAccess('pr_entry_approval.list') == 0)
        if ($this->checkAccess('remarking_application.approval_list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        { 
            
            // echo "<Pre>";print_r("ss");exit();
            // echo "<Pre>";print_r("ss");exit();
            $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));
            $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['status'] = '0';
            $formData['updated'] = '1';

            $data['searchParam'] = $formData;

            $data['marksEntryList'] = $this->remarking_application_model->marksReEntryListSearch($formData);



            // echo "<Pre>";print_r($data['semesterResultList']);exit;


            $data['programmeList'] = $this->remarking_application_model->programListByStatus('1');
            $data['intakeList'] = $this->remarking_application_model->intakeListByStatus('1');
            $data['studentList'] = $this->remarking_application_model->gradeListByStatus('1');



            $this->global['pageTitle'] = 'Campus Management System : Approve Student Marks Entry';

            $this->loadViews("remarking_application/marks_approval_list", $this->global, $data, NULL);
        }

    }




    function approve($id_remark_entry = NULL)
    {
        if ($this->checkAccess('remarking_application.approve') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $user_id = $this->session->userId;

            if ($id_remark_entry == null)
            {
                redirect('/examination/studentMarksEntry/marksEntryApprovalList');
            }

             if($this->input->post())
            {
                // $formData = $this->input->post();
                // echo "<Pre>";print_r($formData);exit;
                
                $status = $this->security->xss_clean($this->input->post('status'));
                $reason = $this->security->xss_clean($this->input->post('reason'));


                $data = array(
                    'status' => $status,
                    'reason' => $reason,
                    'updated_by' => $user_id
                );
                $result = $this->remarking_application_model->updateStudentReMarksEntry($data,$id_remark_entry);

                // Hided Need To Ask Kiran Sir For Flow

                // if($result)
                // {

                //     $remark_entry = $this->remarking_application_model->getRemarkEntry($id_remark_entry);
                //     $id_student = $remark_entry->id_student;
                //     $id_program = $remark_entry->id_program;


                //     $check_apply_status = $this->remarking_application_model->getFeeStructureActivityType('EXAMINATION REMARKING','Approval Level',$id_program);

                //     // echo "<Pre>"; print_r($remark_entry);exit;
                //     if($check_apply_status)
                //     {
                //         $data['add'] = 0;
                //         $data['id_student'] = $id_student;
                        
                //         $this->remarking_application_model->generateMainInvoice($data,$id_remark_entry);
                //     }

                // }

                redirect('/examination/remarkingApplication/marksEntryApprovalList');
            }

            $data['marksEntry'] = $this->remarking_application_model->getRemarkEntry($id_remark_entry);
            $data['studentData'] = $this->remarking_application_model->getStudentByStudent($data['marksEntry']->id_student);

            $data['marksEntryDetails'] = $this->remarking_application_model->getStudentReMarksEntryDetailsByMasterId($id_remark_entry);
            $data['courseRegisteredDetails'] = $this->remarking_application_model->getCourseRegisteredDetails($data['marksEntry']->id_course_registered_landscape);
            $data['studentDetails'] = $this->remarking_application_model->getStudentByStudent($data['marksEntry']->id_student);


            // $data['marksEntry'] = $this->remarking_application_model->getStudentMarksEntry($id);
            // $data['marksEntryDetails'] = $this->remarking_application_model->getStudentMarksEntryDetailsByMasterId($id);

            // $data['courseRegisteredDetails'] = $this->remarking_application_model->getCourseRegisteredDetails($data['marksEntry']->id_course_registered_landscape);
            // $data['studentDetails'] = $this->remarking_application_model->getStudentByStudent($data['marksEntry']->id_student);

            // echo "<Pre>";print_r($data['marksEntryDetails']);exit();

            $this->global['pageTitle'] = 'Campus Management System : Approve Re Marks Entry';
            $this->loadViews("remarking_application/approve", $this->global, $data, NULL);
        }
    }


    function summaryList()
    {
        // if ($this->checkAccess('pr_entry_approval.list') == 0)
        if ($this->checkAccess('remarking_application.summary_list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        { 
            
            // echo "<Pre>";print_r("ss");exit();
            $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));
            $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['status'] = '';
            $formData['updated'] = '1';

            $data['searchParam'] = $formData;

            $data['marksEntryList'] = $this->remarking_application_model->marksReEntryListSearch($formData);



            // echo "<Pre>";print_r($data['semesterResultList']);exit;


            $data['programmeList'] = $this->remarking_application_model->programListByStatus('1');
            $data['intakeList'] = $this->remarking_application_model->intakeListByStatus('1');
            $data['studentList'] = $this->remarking_application_model->gradeListByStatus('1');



            $this->global['pageTitle'] = 'Campus Management System : Approve Student Marks Entry';

            $this->loadViews("remarking_application/summary_list", $this->global, $data, NULL);
        }

    }


    
     function searchStudentList()
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        // $tempData['id_semester'] = '';
        // $tempData['status'] = '';

        $student_list_data = $this->remarking_application_model->searchStudentList($tempData);
        // echo "<Pre>";print_r($student_list_data);exit;


        $table="<select name='id_student' id='id_student' class='form-control' onchange='getSemesterByStudentId(this.value)'>";
        $table.="<option value=''>Select</option> 

        <script type='text/javascript'>
            $('select').select2();
        </script>
                ";

        for($i=0;$i<count($student_list_data);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $student_list_data[$i]->id;
        $full_name = $student_list_data[$i]->full_name;
        $nric = $student_list_data[$i]->nric;
        $table.="<option value=".$id.">".$nric . " - " .$full_name.
                "</option>";

        }
        $table.="</select>";
        
        echo $table;
        exit();
    }

    function getSemesterByStudentId($id_student)
    {

        $semester_list_data = $this->remarking_application_model->getSemesterByStudentId($id_student);
        // echo "<Pre>";print_r($semester_list_data);exit;


        $table="<select name='id_semester' id='id_semester' class='form-control' onchange='getCourse()'>";
        $table.="<option value=''>Select</option>


        <script type='text/javascript'>
            $('select').select2();
        </script>";

        for($i=0;$i<count($semester_list_data);$i++)
        {



        // $id = $results[$i]->id_procurement_category;
        $id = $semester_list_data[$i]->id;
        $semester_name = $semester_list_data[$i]->semester_name;
        $semester_code = $semester_list_data[$i]->semester_code;
        $table.="<option value=".$id.">".$semester_code . " - " . $semester_name.
                "</option>";

        }
        $table.="</select>";
        
        echo $table;
        exit();

    }

    function getCources()
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        // $tempData['id_semester'] = '';
        // $tempData['status'] = '';
        // echo "<Pre>";print_r($tempData);exit;

        if(empty($tempData['id_programme']) || empty($tempData['id_intake']) || empty($tempData['id_semester']) || empty($tempData['id_student']))
        {
            echo "Select The Proper Data Of The Student";exit();
        }

        if($tempData['id_programme'] == '' || $tempData['id_intake'] == '' || $tempData['id_semester'] == '' || $tempData['id_student'] == '')
        {
            echo "Select The Proper Data Of The Student";exit();
        }

        // echo "<Pre>";print_r($tempData);exit;
        $student_data = $this->remarking_application_model->getStudentInfo($tempData);
        // echo "<Pre>";print_r($student_data);exit;

        if(empty($student_data))
        {
            echo "Courses Not Available For This Student";exit();
        }

        $student_name = $student_data->full_name;
        $nric = $student_data->nric;
        $semester_name = $student_data->semester_name;
        $semester_code = $student_data->semester_code;
        // echo "<Pre>";print_r($student_name);exit;

        $table  = "

        <div class='form-container'>
            <h4 class='form-group-title'>Student Details</h4>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student Name :</dt>
                                <dd>$student_name</dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd>$nric</dd>
                            </dl>
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Semester Code :</dt>
                                <dd>
                                    $semester_code
                                </dd>
                            </dl>
                            <dl>
                                <dt>Semester Name :</dt>
                                <dd>$semester_name</dd>
                            </dl>
                        </div>
    
                    </div>
                </div>
        </div>
                <br>
            ";

        // echo "<Pre>";print_r($table);exit;


            

            // $invoice_data = $this->remarking_application_model->getInvoicesByStudentId($id);
            // echo "<Pre>";print_r($tempData);exit;
            $course_list_data = $this->remarking_application_model->getCourseForMarksEntry($tempData);


            if(!empty($course_list_data))
            {

                $table .= "
            <div class='form-container'>
            <h4 class='form-group-title'>Course Details</h4>

            <div class='custom-table'>
                <table class='table' id='list-table'>
                  <tr>
                    <th>Sl. No</th>
                    <th >Course Name</th>
                    <th style='text-align: center;'>Grade</th>
                    <th style='text-align: center;'>Marks</th>
                    <th style='text-align: center;'>Result</th>
                </tr>

                ";


                // echo "<Pre>";print_r($course_list_data);exit;

            for($i=0;$i<count($course_list_data);$i++)
                {
                    $id_exam_registration = $course_list_data[$i]->id_exam_registration;
                    $id_course = $course_list_data[$i]->id;
                    $course_name = $course_list_data[$i]->name;
                    $course_code = $course_list_data[$i]->code;
                    $course = $course_code . " - " . $course_name;
                    $j=$i+1;
                    $table .= "

                    <script type='text/javascript'>
                        $('select').select2();
                    </script>
                <tr>
                    <td>
                    $j
                    <input type='hidden' class='form-control' id='id_course[]' name='id_course[]' value='$id_course' readonly='readonly'/>
                    <input type='hidden' class='form-control' id='id_exam_registration[]' name='id_exam_registration[]' value='$id_exam_registration' readonly='readonly'/>
                    </td>

                    <td >
                    $course
                    </td>

                    <td style='text-align: center;'>

                        <div class='form-group'>

                            <select name='grade[]' id='grade[]' class='form-control' style='width:300px'>
                                <option value=''>Select</option>";

            $gradeList = $this->remarking_application_model->getGradeListByStatus('1');

                        for($j=0;$j<count($gradeList);$j++)
                        {
                            $id = $gradeList[$j]->id;
                            $name = $gradeList[$j]->name;
                            $table.="<option value=".$id.">".$name.
                                    "</option>";
                        }
                        $table.="</select>

                        </div>
                    </td>

                    <td style='text-align: center;'>
                        <div class='form-group'>
                            <input type='number' class='form-control' id='marks[]' name='marks[]' >
                        </div>
                    </td>

                    <td style='text-align: center;'>
                        <div class='form-group'>
                            <select name='result[]' id='result[]' class='form-control' style='width:300px'>
                                <option value=''>Select</option>
                                <option value='Pass'>Pass</option>
                                <option value='Fail'>Fail</option>
                            </select>
                        </div>
                    </td>

                </tr>";
                }
                        
            $table .= "
            </table>
            </div>
            </div>";
                
            }
            else
            {
                $table .= "
            <h4 style='text-align: center;'>Exam Registered Courses Not Available For This Student</h4>";

                // echo "Courses Not Available For This Student";exit();
            }


            echo $table;
            exit;
    }

    function getCoursesBySemester($id_semester)
    {
            $intake_data = $this->remarking_application_model->getCoursesBySemester($id_semester);

            // echo "<Pre>"; print_r($intake_data);exit;

            $table="
            <script type='text/javascript'>
                $('select').select2();
            </script>


            <select name='id_course' id='id_course' class='form-control' onchange='getStudentMarkEntryDetailsByIdSemesterNIdCourse()'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($intake_data);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $intake_data[$i]->id;
            $id_course_landscape = $intake_data[$i]->id_course_landscape;
            $code = $intake_data[$i]->code;
            $name = $intake_data[$i]->name;

            $table.="<option value=".$id_course_landscape.">".$code . " - " . $name.
                    "</option>";

            }
            $table.="</select>";

            echo $table;
    }

    function getStudentMarkEntryDetailsByIdSemesterNIdCourse()
    {
        
        $id_semester = $this->security->xss_clean($this->input->post('id_semester'));
        $id_course_registered_landscape = $this->security->xss_clean($this->input->post('id_course_registered_landscape'));

         // echo "<Pre>"; print_r($id_student);exit();

        $temp_details = $this->remarking_application_model->getStudentMarkEntryDetailsByIdSemesterNIdCourse($id_semester,$id_course_registered_landscape);

        echo "<Pre>"; print_r($temp_details);exit();
        
        $table="
            <script type='text/javascript'>
                $('select').select2();
            </script>


            <select name='id_course' id='id_course' class='form-control'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($temp_details);$i++)
            {

            // Id Landscape Course Id Taken As id_course_registered
            // $id = $results[$i]->id_procurement_category;
            $id = $temp_details[$i]->id;
            $id_course_registered = $temp_details[$i]->id_course_registered;
            $name = $temp_details[$i]->name;
            $code = $temp_details[$i]->code;

            $table.="<option value=".$id_course_registered.">".$code . " - " . $name.
                    "</option>";

            }
            $table.="</select>";

            echo $table;
    }

    function getIntakeByProgramme($id_programme)
    {
            $intake_data = $this->remarking_application_model->getIntakeByProgrammeId($id_programme);

            // echo "<Pre>"; print_r($intake_data);exit;

            $table="
            <script type='text/javascript'>
                $('select').select2();
            </script>


            <select name='id_intake' id='id_intake' class='form-control' onchange='getStudentsListByMarksEntry()'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($intake_data);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $intake_data[$i]->id;
            $name = $intake_data[$i]->name;
            $year = $intake_data[$i]->year;

            $table.="<option value=".$id.">".$year . " - " . $name.
                    "</option>";

            }
            $table.="</select>";

            echo $table;
    }

    function getStudentsListByMarksEntry()
    {
        $formData = $this->security->xss_clean($this->input->post('formData'));
        
        $studentList = $this->remarking_application_model->getStudentsListByMarksEntry($formData);
            // echo "<Pre>"; print_r($studentList);exit;

        $table="
            <script type='text/javascript'>
                $('select').select2();
            </script>


            <select name='id_student' id='id_student' class='form-control' onchange='getStudentByStudentId(this.value)'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($studentList);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $studentList[$i]->id;
            $nric = $studentList[$i]->nric;
            $full_name = $studentList[$i]->full_name;

            $table.="<option value=".$id.">".$nric . " - " . $full_name.
                    "</option>";

            }
            $table.="</select>";

            echo $table;

    }

    function getStudentCoursesByStudentId()
    {
        $formData = $this->security->xss_clean($this->input->post('formData'));

        $studentList = $this->remarking_application_model->getStudentCoursesByStudentId($formData);
        
        // echo "<Pre>"; print_r($studentList);exit;

        $table="
            <script type='text/javascript'>
                $('select').select2();
            </script>


            <select name='id_mark_entry' id='id_mark_entry' class='form-control' onchange='getMarksByMarksEntry(this.value)'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($studentList);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id_mark_entry = $studentList[$i]->id_mark_entry;
            $code = $studentList[$i]->code;
            $name = $studentList[$i]->name;

            $table.="<option value=".$id_mark_entry.">".$code . " - " . $name.
                    "</option>";

            }
            $table.="</select>";

            echo $table;
    }

    function getStudentByStudentId($id)
    {
         // print_r($id);exit;
            $student_data = $this->remarking_application_model->getStudentByStudentId($id);
            // echo "<Pre>"; print_r($student_data);exit;

            $student_name = $student_data->full_name;
            $student_nric = $student_data->nric;
            $email = $student_data->email_id;
            $nric = $student_data->nric;
            $intake_name = $student_data->intake_name;
            $programme_name = $student_data->programme_name;


            $table  = "



             <h4 class='sub-title'>Student Details</h4>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student Name :</dt>
                                <dd>$student_name</dd>
                            </dl>
                            <dl>
                                <dt>Student Email :</dt>
                                <dd>$email</dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd>$nric</dd>
                            </dl>
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Intake :</dt>
                                <dd>
                                    $intake_name
                                </dd>
                            </dl>
                            <dl>
                                <dt>Programme :</dt>
                                <dd>$programme_name</dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd></dd>
                            </dl>
                        </div>
    
                    </div>
                </div>
                <br>";

            echo $table;
            exit;
    }

    function getMarksByMarksEntry($id_marks_entry)
    {

        // print_r($id);exit;
            $marks_entry = $this->remarking_application_model->getStudentMarksEntryForDetails($id_marks_entry);
            $marks_entry_details = $this->remarking_application_model->getStudentMarksEntryDetailsByMasterIdForDetails($id_marks_entry);


        if(!empty($marks_entry))
        {



            // echo "<Pre>"; print_r($marks_entry_details);exit;

            $total_course_marks = $marks_entry->total_course_marks;
            $total_obtained_marks = $marks_entry->total_obtained_marks;
            $total_min_pass_marks = $marks_entry->total_min_pass_marks;
            $result = $marks_entry->result;
            $is_remarking = $marks_entry->is_remarking;
            $id_course_registered_landscape = $marks_entry->id_course_registered_landscape;

            $course_registered_land = $this->remarking_application_model->getCourseByRegisteredlandscape($id_course_registered_landscape);

            $course_code = $course_registered_land->code;
            $course_name = $course_registered_land->name;
            $credit_hours = $course_registered_land->credit_hours;

            $table  = "



             <h4 class='sub-title'>Course Examination Marks Entered Details</h4>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Course Code :</dt>
                                <dd>$course_code</dd>
                            </dl>
                            <dl>
                                <dt>Course Name :</dt>
                                <dd>$course_name</dd>
                            </dl>
                            <dl>
                                <dt>Credit Hours :</dt>
                                <dd>$credit_hours</dd>
                            </dl>
                            <dl>
                                <dt>Result :</dt>
                                <dd>$result</dd>
                            </dl>
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Total Marks :</dt>
                                <dd>
                                    $total_course_marks
                                </dd>
                            </dl>
                            <dl>
                                <dt>Total Obtained :</dt>
                                <dd>$total_obtained_marks</dd>
                            </dl>
                            <dl>
                                <dt>Min. Marks</dt>
                                <dd>$total_min_pass_marks</dd>
                            </dl>
                        </div>
    
                    </div>
                </div>
                <br>";

        if(!empty($marks_entry_details))
        {


             $table.="

            <h3> &nbsp;&nbsp;&nbsp;Mark Entry Details </h3>

            <div class='custom-table'>

            <table class='table' width='100%'>
            <thead>
             <tr>
                 <th>Sl. No</th>
                 <th>Component</th>
                 <th>Max. Marks</th>
                 <th>Min. Marks</th>
                 <th>Obtained Marks</th>
             </tr>
            </thead>
            <tbody>";
              for($i=0;$i<count($marks_entry_details);$i++)
              {
              $j=$i+1; 
                $id = $marks_entry_details[$i]->id;
                $component_name = $marks_entry_details[$i]->component_name;
                $component_code = $marks_entry_details[$i]->component_code;
                $max_marks = $marks_entry_details[$i]->max_marks;
                $obtained_marks = $marks_entry_details[$i]->obtained_marks;
                $pass_marks = $marks_entry_details[$i]->pass_marks;
                $result = $marks_entry_details[$i]->result;
                $j=$i+1;
                $table .= "
                <tbody>
                <tr>
                    <td>$j</td>
                    <td>$component_code - $component_name</td>
                    <td>$max_marks</td>
                    <td>$pass_marks</td>
                    <td>$obtained_marks</td>
                </tr>";
             }

            $table.="
            </tbody>
        </table>
        </div>";


            }

        }
        else
        {
            $table = '';
        }

            echo $table;
            exit;
    }
}
