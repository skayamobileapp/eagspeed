<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Grade extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('grade_model');
        $this->isLoggedIn();
    }

    function list()
    {

        if ($this->checkAccess('grade_setup.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            
            $data['gradeList'] = $this->grade_model->gradeList();

            $this->global['pageTitle'] = 'Campus Management System : Grade';
            $this->loadViews("grade/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('grade_setup.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $description = $this->security->xss_clean($this->input->post('description'));
              
                $data = array(
                    'name' => $name,
                    'description' => $description,
                );
            
                $result = $this->grade_model->addNewGrade($data);
                redirect('/examination/grade/list');
            }

            
            $this->global['pageTitle'] = 'Campus Management System : Add Grade';
            $this->loadViews("grade/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('grade_setup.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/examination/grade/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $description = $this->security->xss_clean($this->input->post('description'));
               
                $data = array(
                    'name' => $name,
                    'description' => $description,
                );
                
                $result = $this->grade_model->editGradeDetails($data,$id);
                redirect('/examination/grade/list');
            }
            
            $data['grade'] = $this->grade_model->getGradeDetails($id);

            $this->global['pageTitle'] = 'Campus Management System : Edit Grade';
            $this->loadViews("grade/edit", $this->global, $data, NULL);
        }
    }
}
