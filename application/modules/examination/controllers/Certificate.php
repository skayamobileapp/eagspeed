<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Certificate extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        error_reporting(0);
        
        $this->isLoggedIn();
    }

    function list()
    {
        
            $this->global['pageTitle'] = 'Campus Management System : Exam Centers';
            $this->loadViews("certificate/list", $this->global, $data, NULL);
    }
    
}
