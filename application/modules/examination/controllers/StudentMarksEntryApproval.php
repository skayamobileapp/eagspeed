<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class StudentMarksEntryApproval extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('student_marks_entry_approval_model');
        $this->load->model('prdtm/programme_model');
        $this->isLoggedIn();
    }

       function list()
    {
        if ($this->checkAccess('programme.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_category'] = $this->security->xss_clean($this->input->post('id_category'));
            $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));
            $formData['id_category_setup'] = $this->security->xss_clean($this->input->post('id_category_setup'));
            $formData['id_programme_type'] = $this->security->xss_clean($this->input->post('id_programme_type'));
            $formData['id_partner_university'] = $this->security->xss_clean($this->input->post('id_partner_university'));

            $data['searchParam'] = $formData;

            $data['categoryList'] = $this->programme_model->categoryListByStatus('1');
            $data['productTypeSetupList'] = $this->programme_model->productTypeSetupListByStatus('1');
            $data['organisationDetails'] = $this->programme_model->getOrganisation();

            $data['programmeList'] = $this->student_marks_entry_approval_model->programListByStatus();

            if($formData['id_programme']) {

                $data['studentList'] = $this->student_marks_entry_approval_model->getStudentListForCourseRegisteredStudent($formData['id_programme']);
                $data['componentList'] = $this->student_marks_entry_approval_model->getMarkDistributionByProgramme($formData['id_programme']);


            }


            $this->global['pageTitle'] = 'Campus Management System : Program List';
            $this->loadViews("student_marks_entry_approval/list", $this->global, $data, NULL);
        }
    }


    function studentList($id = NULL)
    {
        
            $user_id = $this->session->userId;

           
            if($_POST) {

          

                for($i=0;$i<count($_POST['std']);$i++) {

                    $ihp = $_POST['std'][$i];
                    $endoresementdate = date('Y-m-d',strtotime($_POST['endorsement_date']));
                    $reason = $_POST['reason'];
                    $status = $_POST['status'];
                    $marks = $_POST['marks_obtained'][$ihp][0];


                     $marks_adjustment_data = array(
                                'endorsement_date' => $endoresementdate,
                                'reason' => $reason,
                                'final_marks' => $marks,
                                'marks_status'=>$status
                            );

                          
                            $id_mark_adjustment = $this->student_marks_entry_approval_model->updateStudentHasProgramme($marks_adjustment_data,$ihp);

                }

                  echo "<script>alert('Marks Approved has been saved')</script>";
          echo "<script>parent.location='/examination/StudentMarksEntryApproval/list'</script>";
          exit;


            }
                
       
    }
}