<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class StudentMarksEntry extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('student_marks_entry_model');
        $this->load->model('prdtm/programme_model');
        // $this->load->model('main_invoice_model');
        $this->isLoggedIn();
    }

     function list()
    {
        if ($this->checkAccess('programme.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));
            $formData['id_category_setup'] = $this->security->xss_clean($this->input->post('id_category_setup'));
            $formData['id_programme_type'] = $this->security->xss_clean($this->input->post('id_programme_type'));
            $formData['id_partner_university'] = $this->security->xss_clean($this->input->post('id_partner_university'));

            $data['searchParam'] = $formData;

            $data['categoryList'] = $this->programme_model->categoryListByStatus('1');
            $data['productTypeSetupList'] = $this->programme_model->productTypeSetupListByStatus('1');
            $data['organisationDetails'] = $this->programme_model->getOrganisation();
            $data['programmeList'] = $this->student_marks_entry_model->programListByStatus();

            if($formData['id_programme']) {

                $data['studentList'] = $this->student_marks_entry_model->getStudentListForCourseRegisteredStudent($formData['id_programme']);
                $data['componentList'] = $this->student_marks_entry_model->getMarkDistributionByProgramme($formData['id_programme']);


            }




            $this->global['pageTitle'] = 'Campus Management System : Program List';
            $this->loadViews("student_marks_entry/list", $this->global, $data, NULL);
        }
    }


    function studentList($id = Null)
    {
         
            if($this->input->post())
            {
                $postData = $this->input->post();



                    for($s=0;$s<count($_POST['id_student_has_programme']);$s++) {
                        $id_student_has_programme = $_POST['id_student_has_programme'][$s];

                        for($c=0;$c<count($_POST['maincomponentarray']);$c++) {

                            //component Id
                            $id_marks_distribution = $_POST['maincomponentarray'][$c];

                            //Markss
                            $marksforcomponent  = $_POST['component_marks_obtained'][$id_student_has_programme][$c];


                            if($marksforcomponent>0) {


                              $data = array(
                                'id_student_has_programme' => $id_student_has_programme,
                                'id_marks_distribution' => $id_marks_distribution,
                                'marks_obtained' => $marksforcomponent
                                );
                            
                                $this->student_marks_entry_model->addNewMarksEntry($data);

                              $shp = array('marks_status'=>'Entry');
                              $this->student_marks_entry_model->updateStudentHasProgramme($shp,$id_student_has_programme);
                            }



                        }

                    
                }

                

        }

          echo "<script>alert('Marks entry has been saved')</script>";
          echo "<script>parent.location='/examination/studentMarksEntry/list'</script>";
          exit;
    }
}