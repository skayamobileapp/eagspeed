<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class MarkAdjustmentApproval extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('mark_adjustment_approval_model');
        // $this->load->model('main_invoice_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('mark_adjustment.student_list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $user_id = $this->session->userId;

            $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));    
            $data['searchParam'] = $formData;

            if($this->input->post())
            {
                $reason = $this->security->xss_clean($this->input->post('reason'));    

                $postData = $this->input->post();
                
                // echo "<Pre>";print_r($postData);exit;

                $btn_submit = $postData['btn_submit'];

                if($btn_submit == 'search')
                {
                // echo "<Pre>";print_r($this->input->post());exit;
        
                $data['applicantList'] = $this->mark_adjustment_approval_model->getStudentListForCourseRegisteredStudent($formData);
                }
                elseif($btn_submit == 'approve')
                {
                    // echo "<Pre>";print_r($postData);exit;


                    for($i=0;$i<count($postData['id_mark_adjustment']);$i++)
                    {
                        $id_mark_adjustment = $postData['id_mark_adjustment'][$i];
                        $obtained_marks = $postData['obtained_marks'][$i];

                        if($id_mark_adjustment > 0)
                        {
                            $marks_adjustment_data = array(
                                'status' => 1
                            );

                            // echo "<Pre>";print_r($marks_entry_data);exit;
                            $updated_mark_adjustment = $this->mark_adjustment_approval_model->updateStudentMarkAdjustment($marks_adjustment_data,$id_mark_entry);

                            if($updated_mark_adjustment)
                            {
                                $mark_adjustment = $this->mark_adjustment_approval_model->getStudentMarkAdjustment($id_mark_adjustment);

                                if($mark_adjustment)
                                {
                                    $id_student_has_programme = $mark_adjustment->id_student_has_programme;

                                    $update_student_has_programme = array(
                                        'id_marks_adjustment' => $id_mark_adjustment,
                                        'marks' => $obtained_marks
                                    );

                                    $updated_student_has_programme = $this->mark_adjustment_approval_model->updateStudentHasProgramme($update_student_has_programme,$id_student_has_programme);
                                }
                            }

                        }
                    }
                    
                    redirect('/examination/markAdjustmentApproval/list');
                }
                // elseif($btn_submit == 'reject')
                // {
                //     // echo "<Pre>";print_r($postData);exit;


                //     for($i=0;$i<count($postData['id_mark_entry']);$i++)
                //     {
                //         $id_mark_entry = $postData['id_mark_entry'][$i];
                //         $obtained_marks = $postData['obtained_marks'][$i];

                //         if($id_mark_entry > 0)
                //         {
                //             $marks_entry_data = array(
                //                 'status' => 2,
                //                 'reason' => $reason
                //             );

                //             // echo "<Pre>";print_r($marks_entry_data);exit;
                //             $updated_mark_adjustment = $this->mark_adjustment_approval_model->updateStudentMarksEntry($marks_entry_data,$id_mark_entry);


                //         redirect('/examination/markAdjustmentApproval/list');
                //         }
                //     }
                // }

            }

            $data['programList'] = $this->mark_adjustment_approval_model->programListByStatus('1');
            

            // $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));        
            // $data['searchParam'] = $formData;
            $data['applicantList'] = $this->mark_adjustment_approval_model->getStudentListForCourseRegisteredStudent($formData);


            // echo "<Pre>";print_r($data['applicantList']);exit;

            $this->global['pageTitle'] = 'Campus Management System : Receipt List';
            $this->loadViews("mark_adjustment_approval/list", $this->global, $data, NULL);
        }
    }
}