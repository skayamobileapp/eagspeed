<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ExamConfiguration extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('exam_configuration_model');
        $this->isLoggedIn();
    }


    function edit()
    {
        if ($this->checkAccess('exam_configuration.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $max_count = $this->security->xss_clean($this->input->post('max_count'));
                $id = $this->security->xss_clean($this->input->post('id'));

            
                $data = array(
                    'name' => $name,
                    'max_count' => $max_count
                );

                // echo "<Pre>";print_r($data);exit();
                
                $result = $this->exam_configuration_model->editExamConfiguration($data,$id);
                redirect('/examination/examConfiguration/edit/'.$id);
            }
            $data['examConfiguration'] = $this->exam_configuration_model->getExamConfiguration();

            $this->global['pageTitle'] = 'Campus Management System : Edit ExamConfiguration';
            $this->loadViews("exam_configuration/edit", $this->global, $data, NULL);
        }
    }
}
