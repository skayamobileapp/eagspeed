<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Emergency_fund_allocation_model extends CI_Model
{

    function emergencyFundAllocationListSearch($data)
    {
         $this->db->select('efa.*, fy.year as financial_year, fy.name as financial_name, d.name as department_name, d.code as department_code, bty.name as budget_year');
        $this->db->from('emergency_fund_allocation as efa');
        $this->db->join('financial_year as fy','efa.id_financial_year = fy.id');
        $this->db->join('budget_year as bty','efa.id_budget_year = bty.id');
        $this->db->join('department_code as d','efa.department_code = d.code');
        if ($data['name']!='')
        {
            $likeCriteria = "(efa.description  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['type']!='')
        {
            $this->db->where('efa.type', $data['type']);
        }
        if ($data['department_code'] !='')
        {
            $this->db->where('efa.department_code', $data['department_code']);
        }
        if ($data['id_financial_year']!='')
        {
            $this->db->where('efa.id_financial_year', $data['id_financial_year']);
        }
        if ($data['id_budget_year']!='')
        {
            $this->db->where('efa.id_budget_year', $data['id_budget_year']);
        }
        if ($data['status'] !='')
        {
            $this->db->where('efa.status', $data['status']);
        }
        $this->db->order_by("efa.id", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // echo "<Pre>";print_r($result);exit();     
         return $result;
    }

    function getEmergencyFundAllocation($id)
    {
        $this->db->select('efa.*, fy.year as financial_year, fy.name as financial_name, d.name as department_name, d.code as department_code, bty.name as budget_year');
        $this->db->from('emergency_fund_allocation as efa');
        $this->db->join('financial_year as fy','efa.id_financial_year = fy.id');
        $this->db->join('budget_year as bty','efa.id_budget_year = bty.id');
        $this->db->join('department_code as d','efa.department_code = d.code');
        $this->db->where('efa.id', $id);
        $query = $this->db->get();
        return $query->row();
    }


	function getAccountCodeList()
    {
        $this->db->select('*');
        $this->db->from('account_code');
        $this->db->where('level', '3');
        $this->db->where('status', '1');
        $this->db->order_by("code", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getActivityCodeList()
    {
        $this->db->select('*');
        $this->db->from('activity_code');
        $this->db->where('level', '3');
        $this->db->where('status', '1');
        $this->db->order_by("code", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getDepartmentCodeList()
    {
        $this->db->select('*');
        $this->db->from('department_code');
         $this->db->where('status', '1');
        $this->db->order_by("code", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getFundCodeList()
    {
        $this->db->select('*');
        $this->db->from('fund_code');
        $this->db->where('status', '1');
        $this->db->order_by("code", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function financialYearListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('financial_year');
         $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function budgetYearListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('budget_year');
         $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getStaffListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('staff');
         $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getStudentListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('student');
         $this->db->where('applicant_status', $status);
        $this->db->order_by("full_name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function addNewEmergencyFundAllocation($data)
    {
         $this->db->trans_start();
        $this->db->insert('emergency_fund_allocation', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function updateEmergencyFundAllocation($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('emergency_fund_allocation', $data);
        return TRUE;
    }
}