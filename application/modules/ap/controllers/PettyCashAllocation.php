<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class PettyCashAllocation extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('petty_cash_allocation_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('petty_cash_allocation.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $data['financialYearList'] = $this->petty_cash_allocation_model->financialYearListByStatus('1');
            $data['budgetYearList'] = $this->petty_cash_allocation_model->budgetYearListByStatus('1');
            $data['departmentCodeList'] = $this->petty_cash_allocation_model->getDepartmentCodeList();
           
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_financial_year'] = $this->security->xss_clean($this->input->post('id_financial_year'));
            $formData['id_budget_year'] = $this->security->xss_clean($this->input->post('id_budget_year'));
            $formData['department_code'] = $this->security->xss_clean($this->input->post('department_code'));
            $formData['type'] = $this->security->xss_clean($this->input->post('type'));
            $formData['status'] = '';
 
            $data['searchParam'] = $formData;

            $data['pettyCashAllocationList'] = $this->petty_cash_allocation_model->pettyCashAllocationListSearch($formData);
            
           
            // echo "<Pre>";
            // print_r($data);exit();


            $this->global['pageTitle'] = 'FIMS : List Petty Cash Allocation';
            $this->loadViews("petty_cash_allocation/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('petty_cash_allocation.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;

                $generated_number = $this->petty_cash_allocation_model->generatePettyCashAllocationNumber();

                $id_financial_year = $this->security->xss_clean($this->input->post('id_financial_year'));
                $id_budget_year = $this->security->xss_clean($this->input->post('id_budget_year'));
                $activity_code = $this->security->xss_clean($this->input->post('activity_code'));
                $account_code = $this->security->xss_clean($this->input->post('account_code'));
                $department_code = $this->security->xss_clean($this->input->post('department_code'));
                $fund_code = $this->security->xss_clean($this->input->post('fund_code'));
                $min_amount = $this->security->xss_clean($this->input->post('min_amount'));
                $max_amount = $this->security->xss_clean($this->input->post('max_amount'));
                $min_reimbursement_amount = $this->security->xss_clean($this->input->post('min_reimbursement_amount'));
                $max_reimbursement_amount = $this->security->xss_clean($this->input->post('max_reimbursement_amount'));
                $min_reimbursement_percentage = $this->security->xss_clean($this->input->post('min_reimbursement_percentage'));
                $max_reimbursement_percentage = $this->security->xss_clean($this->input->post('max_reimbursement_percentage'));
                $amount = $this->security->xss_clean($this->input->post('amount'));
                // $id_staff = $this->security->xss_clean($this->input->post('id_staff'));

                $data = array(
                    'reference_number' => $generated_number,
					'id_financial_year' => $id_financial_year,
					'id_budget_year' => $id_budget_year,
					'activity_code' => $activity_code,
					'account_code' => $account_code,
					'department_code' => $department_code,
					'fund_code' => $fund_code,
					'min_amount' => $min_amount,
                    'max_amount' => $max_amount,
					'min_reimbursement_amount' => $min_reimbursement_amount,
					'max_reimbursement_amount' => $max_reimbursement_amount,
					'min_reimbursement_percentage' => $min_reimbursement_percentage,
					'max_reimbursement_percentage' => $max_reimbursement_percentage,
                    'amount' => $amount,
                    'balance_amount' => $amount,
                    'status' => 0,
                    'created_by' => $user_id
                );
                // echo "<Pre>"; print_r($data);exit;

                $result = $this->petty_cash_allocation_model->addNewPettyCashAllocation($data);
                redirect('/ap/pettyCashAllocation/list');
            }

            $data['financialYearList'] = $this->petty_cash_allocation_model->financialYearListByStatus('1');
            $data['budgetYearList'] = $this->petty_cash_allocation_model->budgetYearListByStatus('1');
            $data['fundCodeList'] = $this->petty_cash_allocation_model->getFundCodeList();
            $data['departmentCodeList'] = $this->petty_cash_allocation_model->getDepartmentCodeList();
            $data['activityCodeList'] = $this->petty_cash_allocation_model->getActivityCodeList();
            $data['accountCodeList'] = $this->petty_cash_allocation_model->getAccountCodeList();
            $data['staffList'] = $this->petty_cash_allocation_model->getStaffListByStatus('1');



            $this->global['pageTitle'] = 'FIMS : Add Petty Cash Allocation';
            $this->loadViews("petty_cash_allocation/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('petty_cash_allocation.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/ap/pettyCashAllocation/list');
            }
            if($this->input->post())
            {

                redirect('/ap/pettyCashAllocation/list');
            }

            $data['staffList'] = $this->petty_cash_allocation_model->getStaffListByStatus('1');

            $data['pettyCashAllocation'] = $this->petty_cash_allocation_model->getPettyCashAllocation($id);

            // echo "<Pre>"; print_r($data);exit;


            $this->global['pageTitle'] = 'FIMS : View Petty Cash Allocation';
            $this->loadViews("petty_cash_allocation/edit", $this->global, $data, NULL);
        }
    }

    function view($id = NULL)
    {
        if ($this->checkAccess('petty_cash_allocation.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/ap/pettyCashAllocation/approvalList');
            }
            if($this->input->post())
            {

                $status = $this->security->xss_clean($this->input->post('status'));
                $reason = $this->security->xss_clean($this->input->post('reason'));


                $data = array(
                    'status' => $status,
                    'reason' => $reason
                );
            // echo "<Pre>"; print_r($data);exit;

                
                 $result = $this->petty_cash_allocation_model->updatePettyCashAllocation($data,$id);

                redirect('/ap/pettyCashAllocation/approvalList');
            }


            $data['pettyCashAllocation'] = $this->petty_cash_allocation_model->getPettyCashAllocation($id);
            // echo "<Pre>"; print_r($data);exit;


            $this->global['pageTitle'] = 'FIMS : Approve Petty Cash Allocation';
            $this->loadViews("petty_cash_allocation/view", $this->global, $data, NULL);
        }
    }

    function approvalList()
    {
        if ($this->checkAccess('petty_cash_allocation.approval_list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $data['financialYearList'] = $this->petty_cash_allocation_model->financialYearListByStatus('1');
            $data['budgetYearList'] = $this->petty_cash_allocation_model->budgetYearListByStatus('1');
            $data['departmentCodeList'] = $this->petty_cash_allocation_model->getDepartmentCodeList();



           
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_financial_year'] = $this->security->xss_clean($this->input->post('id_financial_year'));
            $formData['id_budget_year'] = $this->security->xss_clean($this->input->post('id_budget_year'));
            $formData['department_code'] = $this->security->xss_clean($this->input->post('department_code'));
            $formData['type'] = $this->security->xss_clean($this->input->post('type'));
            $formData['status'] = '0';
 
            $data['searchParam'] = $formData;

            $data['pettyCashAllocationList'] = $this->petty_cash_allocation_model->pettyCashAllocationListSearch($formData);
            
           
            // echo "<Pre>";
            // print_r($data);exit();


            $this->global['pageTitle'] = 'FIMS : Approval List Petty Cash Allocation';
            $this->loadViews("petty_cash_allocation/approval_list", $this->global, $data, NULL);
        }
    }


    function getEFADataByType()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $type = $tempData['type'];
        
        switch ($type)
        {
            case 'Staff':

                $table = $this->getStaffList();

                break;

            case 'Student':

                $table = $this->getStudentList();
                
                break;


            default:
                # code...
                break;
        }
        echo $table;        
    }

    function getStaffList()
    {
         $data = $this->petty_cash_allocation_model->getStaffListByStatus('1');
                // echo "<Pre>";print_r($bill_data);exit();

        $table="
                <script type='text/javascript'>
                    $('select').select2();
                </script>

                 <div class='col-sm-4'>
                    <div class='form-group'>
                    <label>Select Staff <span class='error-text'>*</span></label>
                <select name='id_staff' id='id_staff' class='form-control'>";
                $table.="<option value=''>Select</option>";

                for($i=0;$i<count($data);$i++)
                {

                // $id = $results[$i]->id_procurement_category;
                $id = $data[$i]->id;
                $name = $data[$i]->name;
                $ic_no = $data[$i]->ic_no;

                $table.="<option value=".$id.">".$name. " - " . $ic_no . 
                        "</option>";

                }
                $table.="</select>
                </div>
                </div>
                ";

                echo $table;
    }

    function getStudentList()
    {
         $data = $this->petty_cash_allocation_model->getStudentListByStatus('Approved');
                // echo "<Pre>";print_r($data);exit();
        
        $table="
                <script type='text/javascript'>
                    $('select').select2();
                </script>

                 <div class='col-sm-4'>
                    <div class='form-group'>
                    <label>Select Student <span class='error-text'>*</span></label>
                <select name='id_student' id='id_student' class='form-control' >";
                $table.="<option value=''>Select</option>";

                for($i=0;$i<count($data);$i++)
                {

                // $id = $results[$i]->id_procurement_category;
                $id = $data[$i]->id;
                $nric = $data[$i]->nric;
                $full_name = $data[$i]->full_name;

                $table.="<option value=".$id.">".$nric. " - " . $full_name . 
                        "</option>";

                }
                $table.="</select>
                </div>
                </div>
                ";

                echo $table;
    }
}
