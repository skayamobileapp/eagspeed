<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class PaymentVoucher extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('payment_voucher_model');
        $this->isLoggedIn();
    }

    function list()
    {

        if ($this->checkAccess('payment_voucher.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['bankList'] = $this->payment_voucher_model->getBankList();
            $data['financialYearList'] = $this->payment_voucher_model->financialYearListByStatus('1');
            $data['budgetYearList'] = $this->payment_voucher_model->budgetYearListByStatus('1');

            $formData['id_financial_year'] = $this->security->xss_clean($this->input->post('id_financial_year'));
            $formData['id_budget_year'] = $this->security->xss_clean($this->input->post('id_budget_year'));
            $formData['id_bank'] = $this->security->xss_clean($this->input->post('id_bank'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['status'] = '';
            $formData['cancel'] = '';

 
            $data['searchParam'] = $formData;

            $data['paymentVoucherList'] = $this->payment_voucher_model->getPaymentVoucherListSearch($formData);

            // echo "<Pre>";print_r($data['paymentVoucherList']);exit;
            $this->global['pageTitle'] = 'FIMS : List Payment Voucher';
            $this->loadViews("payment_voucher/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('payment_voucher.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {

                $id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;

                $formData = $this->input->post();

                // echo "<Pre>"; print_r($formData);exit;

                $generated_number = $this->payment_voucher_model->generatePaymentVoucherNumber();

                $type = $this->security->xss_clean($this->input->post('type'));
                $entry = $this->security->xss_clean($this->input->post('entry'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $payment_reference_number = $this->security->xss_clean($this->input->post('payment_reference_number'));
                $account_number = $this->security->xss_clean($this->input->post('account_number'));
                $id_bank = $this->security->xss_clean($this->input->post('id_bank'));
                $payee_name = $this->security->xss_clean($this->input->post('payee_name'));
                $payment_mode = $this->security->xss_clean($this->input->post('payment_mode'));
                $id_staff = $this->security->xss_clean($this->input->post('id_staff'));

                $id_financial_year = $this->security->xss_clean($this->input->post('id_financial_year'));
                $id_budget_year = $this->security->xss_clean($this->input->post('id_budget_year'));
                
                if($type != 'Petty Cash')
                {
                    $id_staff = 0;
                }

                $data = array(
                    'type'=> $type,
                    'entry'=> $entry,
                    'description'=> $description,
                    'payment_reference_number'=> $payment_reference_number,
                    'account_number'=> $account_number,
                    'id_bank'=> $id_bank,
                    'id_financial_year'=> $id_financial_year,
                    'id_budget_year'=> $id_budget_year,
                    'id_staff'=> $id_staff,
                    'payee_name'=> $payee_name,
                    'payment_mode'=> $payment_mode,
                    'reference_number'=> $generated_number,
                    'created_by' => $user_id,
                    'status'=>'0'
                );
                            // echo "<Pre>"; print_r($data);exit;

             $inserted_id = $this->payment_voucher_model->addPaymentVoucher($data);

             if($inserted_id)
                {
                    $referal_update['ap_status'] = 'V0';
                    $referal_update['is_payment_voucher'] = $inserted_id;

                    $total_bill_amount = 0;
                 for($i=0;$i<count($formData['id_detail']);$i++)
                 {
                    $id_detail = $formData['id_detail'][$i];
                    if($id_detail > 0)
                    {
                        switch ($type)
                        {
                            

                        case 'Investment':

                            $detail_data = $this->payment_voucher_model->getInvestmentRegistrationById($id_detail);
                            // echo "<Pre>"; print_r($detail_data);exit;

                            $detailsData = array(
                                'id_payment_voucher'=>$inserted_id,
                                'type'=>$type,
                                'dt_account' => $detail_data->account_code,
                                'dt_activity' => $detail_data->activity_code,
                                'dt_department' => $detail_data->department_code,
                                'dt_fund' => $detail_data->fund_code,
                                'id_referal'=>$id_detail,
                                'amount'=>$detail_data->amount,
                                'created_by'=>$user_id,
                                'status'=> 1
                            );

                            $total_bill_amount = $total_bill_amount + $detail_data->amount;

                        $inserted_detail_id = $this->payment_voucher_model->addPaymentVoucherDetail($detailsData);

                        if($inserted_detail_id)
                        {

                            $updated_referal = $this->payment_voucher_model->updateInvestmentRegistration($referal_update,$id_detail);

                        }  
                        break;


                        case 'Bill Registration':

                         $detail_data = $this->payment_voucher_model->getBillRegistrationById($id_detail);
                             $detailsData = array(
                                'id_payment_voucher'=>$inserted_id,
                                'type'=>$type,
                                'id_referal'=>$id_detail,
                                'amount'=> $detail_data->total_amount,
                                'created_by'=>$user_id,
                                'status'=> 1
                            );
                // echo "<Pre>"; print_r($detailsData);exit;
                        $total_bill_amount = $total_bill_amount + $detail_data->total_amount;

                        $inserted_detail_id = $this->payment_voucher_model->addPaymentVoucherDetail($detailsData);

                        if($inserted_detail_id)
                        {
                            $updated_referal = $this->payment_voucher_model->updateBillRegistration($referal_update,$id_detail);
                        }        
                        break;

                        
                        case 'Petty Cash':
                         $detail_data = $this->payment_voucher_model->getPettyCashByEntryIdForBillRegistration($id_detail);
                // echo "<Pre>"; print_r($detail_data);exit;
                             $detailsData = array(
                                'id_payment_voucher'=>$inserted_id,
                                'type'=>$type,
                                'cr_account' => $detail_data->cr_account,
                                'cr_activity' => $detail_data->cr_activity,
                                'cr_department' => $detail_data->cr_department,
                                'cr_fund' => $detail_data->cr_fund,
                                'dt_account' => $detail_data->dt_account,
                                'dt_activity' => $detail_data->dt_activity,
                                'dt_department' => $detail_data->dt_department,
                                'dt_fund' => $detail_data->dt_fund,
                                'id_referal'=>$id_detail,
                                'amount'=>$detail_data->paid_amount,
                                'created_by'=>$user_id,
                                'status'=> 1
                            );
                // echo "<Pre>"; print_r($detailsData);exit;
                        $total_bill_amount = $total_bill_amount + $detail_data->paid_amount;

                        $inserted_detail_id = $this->payment_voucher_model->addPaymentVoucherDetail($detailsData);

                        if($inserted_detail_id)
                        {
                            $updated_referal = $this->payment_voucher_model->updatePettyCash($referal_update,$id_detail);
                        }

                        break;


                        default:
                        # code...
                        break;


                        }
                    }
                }
            }
            if($inserted_id)
            {
                    // echo "<Pre>"; print_r($total_bill_amount);exit;
                $bill_data['total_amount'] = $total_bill_amount;
                $updated_bill_amount = $this->payment_voucher_model->updatePaymentVoucher($bill_data,$inserted_id);
            }
                redirect('/ap/paymentVoucher/list');
            }

            $data['bankList'] = $this->payment_voucher_model->getBankList();
            $data['financialYearList'] = $this->payment_voucher_model->financialYearListByStatus('1');
            $data['budgetYearList'] = $this->payment_voucher_model->budgetYearListByStatus('1');

            // echo "<Pre>";print_r($data);exit();


            $this->global['pageTitle'] = 'FIMS : Add Payment Voucher';
            $this->loadViews("payment_voucher/add", $this->global, $data, NULL);
        }
    }

    function edit($id = NULL)
    {
        if ($this->checkAccess('payment_voucher.view') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            if ($id == null)
            {
                redirect('/ap/paymentVoucher/list');
            }
            
            $data['paymentVoucher'] = $this->payment_voucher_model->getPaymentVoucher($id);

            $type = $data['paymentVoucher']->type;
            if($type == 'Investment')
            {
                $data['paymentVoucherDetailsByInvestment'] = $this->payment_voucher_model->getPaymentVoucherDetailsByInvestment($id);
            }elseif($type == 'Bill Registration')
            {
                $data['paymentVoucherDetailsByBillRegistration'] = $this->payment_voucher_model->getPaymentVoucherDetailsByBillRegistration($id);
            }elseif($type == 'Petty Cash')
            {
                $data['paymentVoucherDetailsByPettyCash'] = $this->payment_voucher_model->getPaymentVoucherDetailsByPettyCash($id);
                $data['paymentVoucher']->staff_name = $this->payment_voucher_model->getStaffById($data['paymentVoucher']->id_staff);
            }
            $data['financialYearList'] = $this->payment_voucher_model->financialYearListByStatus('1');
            $data['budgetYearList'] = $this->payment_voucher_model->budgetYearListByStatus('1');
            // echo "<Pre>";print_r($data);exit();

            

            

            $this->global['pageTitle'] = 'FIMS : View Payment Voucher';
            $this->loadViews("payment_voucher/edit", $this->global, $data, NULL);
        }
    }

     function approvalList()
    {

        if ($this->checkAccess('payment_voucher.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['bankList'] = $this->payment_voucher_model->getBankList();
            $data['financialYearList'] = $this->payment_voucher_model->financialYearListByStatus('1');
            $data['budgetYearList'] = $this->payment_voucher_model->budgetYearListByStatus('1');

            $formData['id_financial_year'] = $this->security->xss_clean($this->input->post('id_financial_year'));
            $formData['id_budget_year'] = $this->security->xss_clean($this->input->post('id_budget_year'));
            $formData['id_bank'] = $this->security->xss_clean($this->input->post('id_bank'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['status'] = '0';
            $formData['cancel'] = '';
 
            $data['searchParam'] = $formData;

            $data['paymentVoucherList'] = $this->payment_voucher_model->getPaymentVoucherListSearch($formData);

            // echo "<Pre>";print_r($data['paymentVoucherList']);exit;
            $this->global['pageTitle'] = 'FIMS : Approval List Payment Voucher';
            $this->loadViews("payment_voucher/approval_list", $this->global, $data, NULL);
        }
    }


    function view($id = NULL)
    {

        if ($this->checkAccess('payment_voucher.approve') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            if ($id == null)
            {
                redirect('/ap/paymentVoucher/approvalList');
            }

            if($this->input->post())
            {
                $user_id = $this->session->userId;

                $formData = $this->input->post();

                // echo "<Pre>"; print_r($formData);exit;

                $status = $this->security->xss_clean($this->input->post('status'));
                $reason = $this->security->xss_clean($this->input->post('reason'));
                $type = $this->security->xss_clean($this->input->post('type'));
                $id_bank = $this->security->xss_clean($this->input->post('id_bank'));
                $id_financial_year = $this->security->xss_clean($this->input->post('id_financial_year'));
                $id_budget_year = $this->security->xss_clean($this->input->post('id_budget_year'));
                $total_amount = $this->security->xss_clean($this->input->post('total_amount'));



                $data = array(
                    'status' => $status,
                    'reason' => $reason
                );
                 $result = $this->payment_voucher_model->updatePaymentVoucher($data,$id);

                 $crebit_details = $this->payment_voucher_model->getBankByIdForLedgerDetails($id_bank);

                // echo "<Pre>"; print_r($crebit_details);exit;
                 
                 if($result && $status == '1')
                 {


                    $crebit_details->credit_amount  = $total_amount;
                    $crebit_details->journal_type  = 'Credit';

                    // $generated_number = $this->payment_voucher_model->generateLedgerNumber();

                    $ledger_data['ledger_number'] = $this->payment_voucher_model->generateLedgerNumber();
                    $ledger_data['id_financial_year'] = $id_financial_year;
                    $ledger_data['id_budget_year'] = $id_budget_year;
                    $ledger_data['id_journal'] = $id;
                    $ledger_data['total_amount'] = $total_amount * 2;
                    $ledger_data['description'] = "Payment Voucher";
                    $ledger_data['status'] = 1;
                    $ledger_data['created_by'] = $user_id;
                    $ledger_data['approval_status'] = 1;
                    $ledger_data['approved_by'] = $user_id;


                    $inserted_ledger_id = $this->payment_voucher_model->addNewLedger($ledger_data);                   
                    $voucher_detail_datas = $this->payment_voucher_model->getPaymentVoucherDetailsByMasterId($id);

                    if($type == 'Bill Registration')
                    {
                        $detail_credit_for_bill = array(
                            // 'id_ledger' => $id_ledger,
                            'id_ledger' => $inserted_ledger_id,
                            'journal_type' => "Credit",
                            'account_code' => $crebit_details->account_code,
                            'activity_code' => $crebit_details->activity_code,
                            'department_code' => $crebit_details->department_code,
                            'fund_code' => $crebit_details->fund_code,
                            'credit_amount' => $total_amount,
                            'debit_amount' => 0

                            );
                    
                    $get_details_data = $this->payment_voucher_model->addNewLedgerDetails($detail_credit_for_bill);
                
                    }
                    
                    $ledger_details = array();
                    // echo "<Pre>";print_r($voucher_detail_datas);exit;
                    foreach ($voucher_detail_datas as $voucher_detail_data)
                    {
                        $id_referal = $voucher_detail_data->id_referal;

                        if($id_referal > 0)
                        {
                            switch ($type)
                            {
                                case 'Bill Registration':
                                    $get_details_data = $this->payment_voucher_model->getBillRegistrationDetailsByMasterId($id_referal);

                                // echo "<Pre>";print_r($get_details_data);exit;
                                // $ledger_detail_data = array();
                                foreach ($get_details_data as $get_detail_data)
                                {


                                    $detail_data = array(
                                        // 'id_ledger' => $id_ledger,
                                        'id_ledger' => $inserted_ledger_id,
                                        'journal_type' => "Debit",
                                        'account_code' => $get_detail_data->dt_account,
                                        'activity_code' => $get_detail_data->dt_activity,
                                        'department_code' => $get_detail_data->dt_department,
                                        'fund_code' => $get_detail_data->dt_fund,
                                        'credit_amount' => 0,
                                        'debit_amount' => $get_detail_data->total_final

                                        );


                                     $get_details_data = $this->payment_voucher_model->addNewLedgerDetails($detail_data);

                                    // array_push($ledger_detail_data, $detail_data);
                                }

                                    break;

                                case 'Investment':
                                    $payment_details_data = $this->payment_voucher_model->getPaymentVoucherDetailsByMasterIdForLedgerDetails($id);
                                    break;

                                case 'Petty Cash':

                                   $payment_details_data = $this->payment_voucher_model->getPaymentVoucherDetailsByMasterIdForLedgerDetails($id);

                    // echo "<Pre>";print_r($payment_details_data);exit;

                                    break;

                                default:
                                    # code...
                                    break;
                            }
                        }
                        // array_merge($ledger_details,$ledger_detail_data);
                        // $c=array_combine($fname,$age);
                        // array_push($ledger_details, $ledger_detail_data);
                    }
                 }
                 if($type != 'Bill Registration')
                 {
                    array_push($payment_details_data, $crebit_details);
                     if($inserted_ledger_id)
                     {
                        $inserted_ledger_detail_id = $this->payment_voucher_model->addNewLedgerDetailsFiltered($payment_details_data,$inserted_ledger_id);

                     }
                 }
                 
                // echo "<Pre>";print_r($payment_details_data);exit;
                

                redirect('/ap/paymentVoucher/approvalList');

            }
            
            
            $data['paymentVoucher'] = $this->payment_voucher_model->getPaymentVoucher($id);

            $type = $data['paymentVoucher']->type;
            if($type == 'Investment')
            {
                $data['paymentVoucherDetailsByInvestment'] = $this->payment_voucher_model->getPaymentVoucherDetailsByInvestment($id);
            }elseif($type == 'Bill Registration')
            {
                $data['paymentVoucherDetailsByBillRegistration'] = $this->payment_voucher_model->getPaymentVoucherDetailsByBillRegistration($id);

                // $data['paymentVoucher']->staff_name = $this->payment_voucher_model->getStaffById($data['paymentVoucher']->id_staff);

            }elseif($type == 'Petty Cash')
            {
                $data['paymentVoucherDetailsByPettyCash'] = $this->payment_voucher_model->getPaymentVoucherDetailsByPettyCash($id);
                $data['paymentVoucher']->staff_name = $this->payment_voucher_model->getStaffById($data['paymentVoucher']->id_staff);
            }

            $data['financialYearList'] = $this->payment_voucher_model->financialYearListByStatus('1');
            $data['budgetYearList'] = $this->payment_voucher_model->budgetYearListByStatus('1');

            // echo "<Pre>";print_r($data['paymentVoucherList']);exit;
            

            $this->global['pageTitle'] = 'FIMS : Approve Payment Voucher';
            $this->loadViews("payment_voucher/view", $this->global, $data, NULL);
        }
    }


    function cancellationList()
    {
        if ($this->checkAccess('payment_voucher.cancel') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['bankList'] = $this->payment_voucher_model->getBankList();
            $data['financialYearList'] = $this->payment_voucher_model->financialYearListByStatus('1');
            $data['budgetYearList'] = $this->payment_voucher_model->budgetYearListByStatus('1');

            $formData['id_financial_year'] = $this->security->xss_clean($this->input->post('id_financial_year'));
            $formData['id_budget_year'] = $this->security->xss_clean($this->input->post('id_budget_year'));
            $formData['id_bank'] = $this->security->xss_clean($this->input->post('id_bank'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['status'] = '1';
            $formData['cancel'] = '1';
 
            $data['searchParam'] = $formData;

            $data['paymentVoucherList'] = $this->payment_voucher_model->getPaymentVoucherListSearch($formData);

            // echo "<Pre>";print_r($data['paymentVoucherList']);exit;
            $this->global['pageTitle'] = 'FIMS : Cancellaion List Payment Voucher';
            $this->loadViews("payment_voucher/cancellation_list", $this->global, $data, NULL);
        }
    }

    function cancel($id = NULL)
    {

        if ($this->checkAccess('payment_voucher.cancel') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            if ($id == null)
            {
                redirect('/ap/paymentVoucher/cancellationList');
            }

            if($this->input->post())
            {

                $formData = $this->input->post();

                // echo "<Pre>"; print_r($formData);exit;

                $status = $this->security->xss_clean($this->input->post('status'));
                $reason = $this->security->xss_clean($this->input->post('reason'));

                $data = array(
                    'status' => $status,
                    'reason' => $reason
                );
                 $result = $this->payment_voucher_model->updatePaymentVoucher($data,$id);
                 
                redirect('/ap/paymentVoucher/cancellationList');

            }
            
            
            $data['paymentVoucher'] = $this->payment_voucher_model->getPaymentVoucher($id);

            $type = $data['paymentVoucher']->type;
            if($type == 'Investment')
            {
                $data['paymentVoucherDetailsByInvestment'] = $this->payment_voucher_model->getPaymentVoucherDetailsByInvestment($id);
            }elseif($type == 'Bill Registration')
            {
                $data['paymentVoucherDetailsByBillRegistration'] = $this->payment_voucher_model->getPaymentVoucherDetailsByBillRegistration($id);
            }
            elseif($type == 'Petty Cash')
            {
                $data['paymentVoucherDetailsByPettyCash'] = $this->payment_voucher_model->getPaymentVoucherDetailsByPettyCash($id);
                $data['paymentVoucher']->staff_name = $this->payment_voucher_model->getStaffById($data['paymentVoucher']->id_staff);
            }

            $data['financialYearList'] = $this->payment_voucher_model->financialYearListByStatus('1');
            $data['budgetYearList'] = $this->payment_voucher_model->budgetYearListByStatus('1');

            

            $this->global['pageTitle'] = 'FIMS : Approve Payment Voucher';
            $this->loadViews("payment_voucher/cancel", $this->global, $data, NULL);
        }
    }



    function getVoucherDataByType()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $type = $tempData['type'];

        // $view_data = $this->getDataEntryTemplateForBillRegistration();
        
        // echo "<Pre>";print_r($type);exit();

        
        switch ($type)
        {
            case 'Investment':

                $table = $this->getInvestmentRegistrationForVoucher();

                break;

            case 'Bill Registration':

                $table = $this->getBillRegistrationForVoucher($tempData);
                
                break;

            case 'Petty Cash':

            $table = $this->getPettyCashForBillRegistration($tempData);
            
                break;

            default:
                # code...
                break;
        }
        echo $table;        
        
        
    }

    function getInvestmentRegistrationForVoucher()
    {
         $voucher_data = $this->payment_voucher_model->getInvestmentRegistrationForVoucher();

         if(!empty($voucher_data))
         {
             $table = "
        <h4>Investment Details For Payment Voucher</h4>
        <div class='custom-table'>
        <table class='table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Registration Number</th>
                    <th>Credit GL Code </th>
                    <th>Duration</th>
                    <th>Maturity Date</th>
                    <th>Profit Amount</th>
                    <th>Amount</th>
                    <th style='text-align: center;'><input type='checkbox' id='checkAll' name='checkAll'> Check All</th>
                    </tr>
                    </thead>";
                    $total_detail = 0;
                    for($i=0;$i<count($voucher_data);$i++)
                    {
                        $id = $voucher_data[$i]->id;
                        $registration_number = $voucher_data[$i]->registration_number;
                        $name = $voucher_data[$i]->name;
                        $amount = $voucher_data[$i]->amount;
                        $rate_of_interest = $voucher_data[$i]->rate_of_interest;
                        $contact_person_one = $voucher_data[$i]->contact_person_one;
                        $contact_email_one = $voucher_data[$i]->contact_email_one;
                        $maturity_date = $voucher_data[$i]->maturity_date;
                        $duration = $voucher_data[$i]->duration;
                        $duration_type = $voucher_data[$i]->duration_type;
                        $profit_amount = $voucher_data[$i]->profit_amount;


                        $cr_fund = $voucher_data[$i]->fund_code;
                        $cr_department = $voucher_data[$i]->department_code;
                        $cr_activity = $voucher_data[$i]->activity_code;
                        $cr_account = $voucher_data[$i]->account_code;
                        $dt_fund = $voucher_data[$i]->fund_code;
                        $dt_department = $voucher_data[$i]->department_code;
                        $dt_activity = $voucher_data[$i]->activity_code;
                        $dt_account = $voucher_data[$i]->account_code;


                        $j=$i+1;
                        $table .= "
                        <tbody>
                        <tr>
                            <td>$j .
                            </td>
                            <td>$registration_number</td>
                            <td>$cr_fund - $cr_department - $cr_activity - $cr_account</td>
                            <td>$duration  $duration_type</td>
                            <td>$maturity_date</td>
                            <td>$profit_amount</td>
                            <td>$amount</td>
                            <td class='text-center'>
                            <input type='checkbox' id='id_detail[]' name='id_detail[]' class='check' value='".$id."'>
                            </td>
                        </tr>
                        </tbody>";
                        $total_detail = $total_detail + $amount;
                    }

                    // <td>
                    //             <span onclick='getTempData($id)'>Edit</a>
                    //         <td>
        $table.= "</table>
                </div>";

         

         }
         else
         {

               $table = "
        <h4>No Investment Registration Available</h4>";

       
            }
        

        echo $table;exit();
    }

   

    function getBillRegistrationForVoucher($data)
    {
       

        $payment_data = $this->payment_voucher_model->getBillRegistrationForVoucher($data);
            
        // echo "<Pre>";print_r($payment_data);exit();
        if(!empty($payment_data))
         {

        $table = "
        <h4>Bill Registration Details For Payment Voucher</h4>
        
        <div class='custom-table'>
        <table class='table'>
                <thead>
                 <tr>
                    <th>Sl. No</th>
                    <th>Reference Number</th>
                    <th>Type</th>
                    <th>Description</th>
                    <th>Contact Person</th>
                    <th>Bank Account Number</th>
                    <th>Total Amount</th>
                    <th style='text-align: center;'><input type='checkbox' id='checkAll' name='checkAll'> Check All</th>
                    </tr>
                </thead>";
                    $total_detail = 0;
                    for($i=0;$i<count($payment_data);$i++)
                    {

                        $id = $payment_data[$i]->id;
                        $reference_number = $payment_data[$i]->reference_number;
                        $type = $payment_data[$i]->type;
                        $description = $payment_data[$i]->description;
                        $ef_for = $payment_data[$i]->ef_for;
                        $ef_for_id = $payment_data[$i]->ef_for_id;
                        $contact_person_one = $payment_data[$i]->contact_person_one;
                        $email = $payment_data[$i]->email;
                        $bank_acc_no = $payment_data[$i]->bank_acc_no;
                        $total_amount = $payment_data[$i]->total_amount;
                        $id_staff = $payment_data[$i]->id_staff;

                        if($type == 'General Claim')
                        {
                            $contact_person_one = $payment_data[$i]->customer_name;
                        }
                        elseif($type == 'Emergency Fund')
                        {
                            if($ef_for == 'Staff')
                            {
                                $contact_person_one = $this->payment_voucher_model->getStaffById($ef_for_id);
                            }elseif($ef_for == 'Student')
                            {
                                $contact_person_one = $this->payment_voucher_model->getStudentById($ef_for_id);
                            }
                        }
                        elseif($type == 'Staff Claim' || $type == 'Petty Cash')
                        {
                                $contact_person_one = $this->payment_voucher_model->getStaffById($id_staff);
                        }

                        // $maturity_date = date("d-m-Y", strtotime($grn_details[$i]->maturity_date));

                        $j=$i+1;

                         // <input type='hidden' name='id_detail[]' id='id_detail[]' value='$id' />

                        $table .= "
                        <tbody>
                        <tr>
                            <td>$j .
                            </td>
                            <td>$reference_number</td>
                            <td>$type</td>
                            <td>$description</td>
                            <td>
                                $contact_person_one
                            </td>
                            <td>
                                $bank_acc_no
                            </td>
                            <td>
                                $total_amount
                            </td>
                            <td class='text-center'>
                            <input type='checkbox' id='id_detail[]' name='id_detail[]' class='check' value='$id'>


                            </td>
                        </tr>
                     </tbody>";
                    }

            

        $table.= "</table>
        </div>";

          
         }
         else
         {

              $table = "
        <h4>No Bill Registration Available</h4>";

        }
        

        echo $table;exit();

    }

    function getPettyCashForBillRegistration($data)
    {
                // echo "<Pre>";print_r($data);exit();
        $bill_data = $this->payment_voucher_model->getPCStaffDataForBillRegistration($data);
                // echo "<Pre>";print_r($bill_data);exit();

        $table="
                <script type='text/javascript'>
                    $('select').select2();
                </script>

                 <div class='col-sm-4'>
                    <div class='form-group'>
                    <label>Select Staff <span class='error-text'>*</span></label>
                <select name='id_staff' id='id_staff' class='form-control' onchange='getPCByStaff()'>";
                $table.="<option value=''>Select</option>";

                for($i=0;$i<count($bill_data);$i++)
                {

                // $id = $results[$i]->id_procurement_category;
                $id_staff = $bill_data[$i]->id_staff;
                $code = $bill_data[$i]->code;
                $full_name = $bill_data[$i]->full_name;

                $table.="<option value=".$id_staff.">".$code. " - " . $full_name . 
                        "</option>";

                }
                $table.="</select>
                </div>
                </div>
                ";

                echo $table;
    }
}
