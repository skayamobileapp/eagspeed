<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class EmergencyFundEntry extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('emergency_fund_entry_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('emergency_fund_entry.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $data['financialYearList'] = $this->emergency_fund_entry_model->financialYearListByStatus('1');
            $data['budgetYearList'] = $this->emergency_fund_entry_model->budgetYearListByStatus('1');
            $data['departmentCodeList'] = $this->emergency_fund_entry_model->getDepartmentCodeList();

           
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_financial_year'] = $this->security->xss_clean($this->input->post('id_financial_year'));
            $formData['id_budget_year'] = $this->security->xss_clean($this->input->post('id_budget_year'));
            $formData['department_code'] = $this->security->xss_clean($this->input->post('department_code'));
            $formData['type'] = $this->security->xss_clean($this->input->post('type'));
            $formData['status'] = '';
 
            $data['searchParam'] = $formData;

            $data['emergencyFundEntryList'] = $this->emergency_fund_entry_model->emergencyFundEntryListSearch($formData);
            
           
            // echo "<Pre>";
            // print_r($data);exit();


            $this->global['pageTitle'] = 'FIMS : List Emergency Fund Entry';
            $this->loadViews("emergency_fund_entry/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('emergency_fund_entry.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;
            
            if($this->input->post())
            {
                

                $formData = $this->input->post();

                // echo "<Pre>"; print_r($formData);exit;


                $generated_number = $this->emergency_fund_entry_model->generateEmergencyFundEntryNumber();

                $type = $this->security->xss_clean($this->input->post('type'));
                $id_financial_year = $this->security->xss_clean($this->input->post('id_financial_year'));
                $id_budget_year = $this->security->xss_clean($this->input->post('id_budget_year'));
                $fund_code = $this->security->xss_clean($this->input->post('fund_code'));
                $department_code = $this->security->xss_clean($this->input->post('department_code'));
                $activity_code = $this->security->xss_clean($this->input->post('activity_code'));
                $account_code = $this->security->xss_clean($this->input->post('account_code'));
                $id_emergency_fund_allocation = $this->security->xss_clean($this->input->post('id_emergency_fund_allocation'));
                $requested_amount = $this->security->xss_clean($this->input->post('requested_amount'));
                $transation_type = $this->security->xss_clean($this->input->post('transation_type'));
                $id_staff = $this->security->xss_clean($this->input->post('id_staff'));
                $id_student = $this->security->xss_clean($this->input->post('id_student'));
                $description = $this->security->xss_clean($this->input->post('description'));


                $emergency_fund_allocation_used_amount = $this->security->xss_clean($this->input->post('emergency_fund_allocation_used_amount'));
                $emergency_fund_allocation_balance_amount = $this->security->xss_clean($this->input->post('emergency_fund_allocation_balance_amount'));



                $data = array(
					'type' => $type,
					'id_financial_year' => $id_financial_year,
                    'id_budget_year' => $id_budget_year,
					'id_emergency_fund_allocation' => $id_emergency_fund_allocation,
					'fund_code' => $fund_code,
					'department_code' => $department_code,
					'activity_code' => $activity_code,
					'account_code' => $account_code,
                    'reference_number' => $generated_number,
					'requested_amount' => $requested_amount,
					'id_staff' => $id_staff,
					'id_student' => $id_student,
                    'description' => $description,
                    'status' => 0,
                    'created_by' => $user_id
                );
                // echo "<Pre>"; print_r($data);exit;

                $inserted_id = $this->emergency_fund_entry_model->addNewEmergencyFundEntry($data);
                if($inserted_id)
                {
                    $update_allocation['used_amount'] = $emergency_fund_allocation_used_amount + $requested_amount;
                    $update_allocation['balance_amount'] = $emergency_fund_allocation_balance_amount - $requested_amount;

                    $inserted_id = $this->emergency_fund_entry_model->updateEmergencyFundAllocation($update_allocation,$id_emergency_fund_allocation);
                }
                redirect('/ap/emergencyFundEntry/list');
            }

            $data['financialYearList'] = $this->emergency_fund_entry_model->financialYearListByStatus('1');
            $data['budgetYearList'] = $this->emergency_fund_entry_model->budgetYearListByStatus('1');
            $data['fundCodeList'] = $this->emergency_fund_entry_model->getFundCodeList();
            $data['departmentCodeList'] = $this->emergency_fund_entry_model->getDepartmentCodeList();
            $data['activityCodeList'] = $this->emergency_fund_entry_model->getActivityCodeList();
            $data['accountCodeList'] = $this->emergency_fund_entry_model->getAccountCodeList();



            $this->global['pageTitle'] = 'FIMS : Add Emergency Fund Entry';
            $this->loadViews("emergency_fund_entry/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('emergency_fund_entry.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/ap/emergencyFundEntry/list');
            }
            if($this->input->post())
            {

                redirect('/ap/emergencyFundEntry/list');
            }

            // $data['financialYearList'] = $this->emergency_fund_entry_model->financialYearListByStatus('1');
            // $data['budgetYearList'] = $this->emergency_fund_entry_model->budgetYearListByStatus('1');
            // $data['fundCodeList'] = $this->emergency_fund_entry_model->getFundCodeList();
            // $data['departmentCodeList'] = $this->emergency_fund_entry_model->getDepartmentCodeList();
            // $data['activityCodeList'] = $this->emergency_fund_entry_model->getActivityCodeList();
            // $data['accountCodeList'] = $this->emergency_fund_entry_model->getAccountCodeList();
            $data['staffList'] = $this->emergency_fund_entry_model->getStaffListByStatus('1');
            $data['studentList'] = $this->emergency_fund_entry_model->getStudentListByStatus('Approved');

            $data['emergencyFundEntry'] = $this->emergency_fund_entry_model->getEmergencyFundEntry($id);

            // echo "<Pre>"; print_r($data['emergencyFundEntry']);exit;


            $this->global['pageTitle'] = 'FIMS : View Emergency Fund Entry';
            $this->loadViews("emergency_fund_entry/edit", $this->global, $data, NULL);
        }
    }

    function view($id = NULL)
    {
        if ($this->checkAccess('emergency_fund_entry.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/ap/emergencyFundEntry/list');
            }
            if($this->input->post())
            {

                $formData = $this->input->post();

                // echo "<Pre>"; print_r($formData);exit;


                $status = $this->security->xss_clean($this->input->post('status'));
                $reason = $this->security->xss_clean($this->input->post('reason'));
                $id_emergency_fund_allocation = $this->security->xss_clean($this->input->post('id_emergency_fund_allocation'));
                $requested_amount = $this->security->xss_clean($this->input->post('requested_amount'));


                $data = array(
                    'status' => $status,
                    'reason' => $reason
                );
            // echo "<Pre>"; print_r($data);exit;

                
                 $result = $this->emergency_fund_entry_model->updateEmergencyFundEntry($data,$id);
                 if($status == '2')
                 {
                    if($result)
                    {
                    
                        $allocation_data = $this->emergency_fund_entry_model->getEmergencyFundAllocationById($id_emergency_fund_allocation);


                        $updated_allocation['used_amount'] = $allocation_data->used_amount - $requested_amount;
                        $updated_allocation['balance_amount'] = $allocation_data->balance_amount + $requested_amount;

                    // echo "<Pre>"; print_r($updated_allocation);exit;
                        $result = $this->emergency_fund_entry_model->updateEmergencyFundAllocation($updated_allocation,$id_emergency_fund_allocation);
                    }
                 }

                redirect('/ap/emergencyFundEntry/approvalList');
            }

            $data['staffList'] = $this->emergency_fund_entry_model->getStaffListByStatus('1');
            $data['studentList'] = $this->emergency_fund_entry_model->getStudentListByStatus('Approved');

            $data['emergencyFundEntry'] = $this->emergency_fund_entry_model->getEmergencyFundEntry($id);

            // echo "<Pre>"; print_r($data);exit;


            $this->global['pageTitle'] = 'FIMS : Approve Emergency Fund Entry';
            $this->loadViews("emergency_fund_entry/view", $this->global, $data, NULL);
        }
    }

    function approvalList()
    {
        if ($this->checkAccess('emergency_fund_entry.approval_list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $data['financialYearList'] = $this->emergency_fund_entry_model->financialYearListByStatus('1');
            $data['budgetYearList'] = $this->emergency_fund_entry_model->budgetYearListByStatus('1');
            $data['departmentCodeList'] = $this->emergency_fund_entry_model->getDepartmentCodeList();



           
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_financial_year'] = $this->security->xss_clean($this->input->post('id_financial_year'));
            $formData['id_budget_year'] = $this->security->xss_clean($this->input->post('id_budget_year'));
            $formData['department_code'] = $this->security->xss_clean($this->input->post('department_code'));
            $formData['type'] = $this->security->xss_clean($this->input->post('type'));
            $formData['status'] = '0';
 
            $data['searchParam'] = $formData;

            $data['emergencyFundEntryList'] = $this->emergency_fund_entry_model->emergencyFundEntryListSearch($formData);
            
           
            // echo "<Pre>";
            // print_r($data);exit();


            $this->global['pageTitle'] = 'FIMS : Approval List Emergency Fund Allocation';
            $this->loadViews("emergency_fund_entry/approval_list", $this->global, $data, NULL);
        }
    }


    function getEFADataByType()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $type = $tempData['type'];
        
        switch ($type)
        {
            case 'Staff':

                $table = $this->getStaffList();

                break;

            case 'Student':

                $table = $this->getStudentList();
                
                break;


            default:
                # code...
                break;
        }
        echo $table;        
    }

    function getStaffList()
    {
         $data = $this->emergency_fund_entry_model->getStaffListByStatus('1');
                // echo "<Pre>";print_r($bill_data);exit();

        $table="
                <script type='text/javascript'>
                    $('select').select2();
                </script>

                 <div class='col-sm-4'>
                    <div class='form-group'>
                    <label>Select Staff <span class='error-text'>*</span></label>
                <select name='id_staff' id='id_staff' class='form-control' onchange='getAllocationData()'>";
                $table.="<option value=''>Select</option>";

                for($i=0;$i<count($data);$i++)
                {

                // $id = $results[$i]->id_procurement_category;
                $id = $data[$i]->id;
                $name = $data[$i]->name;
                $ic_no = $data[$i]->ic_no;

                $table.="<option value=".$id.">".$name. " - " . $ic_no . 
                        "</option>";

                }
                $table.="</select>
                </div>
                </div>
                ";

                echo $table;
    }

    function getStudentList()
    {
         $data = $this->emergency_fund_entry_model->getStudentListByStatus('Approved');
                // echo "<Pre>";print_r($data);exit();
        
        $table="
                <script type='text/javascript'>
                    $('select').select2();
                </script>

                 <div class='col-sm-4'>
                    <div class='form-group'>
                    <label>Select Student <span class='error-text'>*</span></label>
                <select name='id_student' id='id_student' class='form-control' onchange='getAllocationData()'>";
                $table.="<option value=''>Select</option>";

                for($i=0;$i<count($data);$i++)
                {

                // $id = $results[$i]->id_procurement_category;
                $id = $data[$i]->id;
                $nric = $data[$i]->nric;
                $full_name = $data[$i]->full_name;

                $table.="<option value=".$id.">".$nric. " - " . $full_name . 
                        "</option>";

                }
                $table.="</select>
                </div>
                </div>
                ";

                echo $table;
    }

    function getAllocationData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        // $type = $tempData['type'];
        $data = $this->emergency_fund_entry_model->getEmergencyFundAllocation($tempData);

        if($data == '')
        {
            echo "";exit();
        }
        else
        {
        // echo "<Pre>";print_r($data);exit();
            $table = "


                <div class='col-sm-4'>
                    <div class='form-group'>
                        <label>Emergency Fund Allocation <span class='error-text'>*</span></label>
                        <input type='text' class='form-control' id='emergency_fund_allocation' name='emergency_fund_allocation' value='$data->description - $data->department_code' readonly>

                        <input type='hidden' class='form-control' id='id_emergency_fund_allocation' name='id_emergency_fund_allocation' value='$data->id'>
                        <input type='hidden' class='form-control' id='emergency_fund_allocation_used_amount' name='emergency_fund_allocation_used_amount' value='$data->used_amount'>
                    </div>
                </div>



                  <div class='col-sm-4'>
                    <div class='form-group'>
                        <label>Balance Amount <span class='error-text'>*</span></label>
                        <input type='text' class='form-control' id='emergency_fund_allocation_balance_amount' name='emergency_fund_allocation_balance_amount' value='$data->balance_amount' readonly>
                    </div>
                </div>

            ";
        }
        
        
        echo $table;   
    }
}
