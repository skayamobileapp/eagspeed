<?php $this->load->helper("form"); ?>
<form id="form_pr_entry" action="" method="post">
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>View Emergency Fund Entry</h3>
            </div>

    <div class="form-container">
            <h4 class="form-group-title">Emergency Fund Entry Details</h4>




            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Emergency Fund Type <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $emergencyFundEntry->type;?>" readonly="readonly">
                    </div>
                </div>


                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Financial Year <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $emergencyFundEntry->financial_year;?>" readonly="readonly">
                    </div>
                </div>


                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Budget Year <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $emergencyFundEntry->budget_year;?>" readonly="readonly">
                    </div>
                </div>



               

            </div>



            <div class="row">



                <?php 
                if($emergencyFundEntry->type == 'Staff')
                {
                    ?>


                 <div class="col-sm-4">
                    <div class="form-group">
                      <label>Staff <span class='error-text'>*</span></label>
                      <select name="id_bank" id="id_bank" class="form-control" disabled="true">
                          <option value="">Select</option>
                          
                           <?php
                          if (!empty($staffList)) {
                            foreach ($staffList as $record)
                            {
                              $selected = '';
                              if ($record->id == $emergencyFundEntry->id_staff) {
                                $selected = 'selected';
                              }
                          ?>
                              <option value="<?php echo $record->id;  ?>"
                                <?php echo $selected;  ?>>
                                <?php echo  $record->ic_no . " - " . $record->salutation . ". " . $record->name;  ?>
                                </option>
                          <?php
                            }
                          }
                          ?>

                      </select>
                  </div>
                </div>

                <?php
                }elseif ($emergencyFundEntry->type == 'Student')
                {
                   ?>

                   <div class="col-sm-4">
                    <div class="form-group">
                      <label>Staff <span class='error-text'>*</span></label>
                      <select name="id_bank" id="id_bank" class="form-control" disabled="true">
                          <option value="">Select</option>
                          
                           <?php
                          if (!empty($studentList)) {
                            foreach ($studentList as $record)
                            {
                              $selected = '';
                              if ($record->id == $emergencyFundEntry->id_student) {
                                $selected = 'selected';
                              }
                          ?>
                              <option value="<?php echo $record->id;  ?>"
                                <?php echo $selected;  ?>>
                                <?php echo  $record->nric . " - " . $record->full_name;  ?>
                                </option>
                          <?php
                            }
                          }
                          ?>

                      </select>
                  </div>
                </div>


                <?php   
                }

                ?>



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Emergency Fund Allocation <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="amount" name="amount" value="<?php echo $emergencyFundEntry->emergency_fund_allocation . " - " . $emergencyFundEntry->emergency_fund_allocation_department_code;?>" readonly="readonly">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Description <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="amount" name="amount" value="<?php echo $emergencyFundEntry->description;?>" readonly="readonly">
                    </div>
                </div>
                 

            </div>



            <div class="row">

              <div class="col-sm-4">
                    <div class="form-group">
                        <label>Fund Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $emergencyFundEntry->fund_code;?>" readonly="readonly">
                    </div>
                </div>



                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Department Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $emergencyFundEntry->department_code;?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Activity Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $emergencyFundEntry->activity_code;?>" readonly="readonly">
                    </div>
                </div>

            </div>


            <div class="row">


               <div class="col-sm-4">
                    <div class="form-group">
                        <label>Account Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $emergencyFundEntry->account_code;?>" readonly="readonly">
                    </div>
                </div>



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Requested Amount <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="date_time" name="date_time" value="<?php echo $emergencyFundEntry->requested_amount;?>" readonly="readonly">
                    </div>
                </div>

            </div>

             <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Status <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="date_time" name="date_time" value="<?php
                        if($emergencyFundEntry->status == '0')
                        {
                            echo "Pending";
                        }
                        elseif($emergencyFundEntry->status == '1')
                        {
                            echo "Approved";
                        }
                        elseif($emergencyFundEntry->status == '2')
                        {
                            echo "Rejected";
                        }
                         ?>" readonly="readonly">
                    </div>
                </div>
            


             <?php
            if($emergencyFundEntry->status == '2')
            {
             ?>


                <div class="col-sm-4" id="view_reject">
                    <div class="form-group">
                        <label>Reject Reason <span class='error-text'>*</span></label>
                        <input type="text" id="reason" name="reason" class="form-control" value="<?php echo $emergencyFundEntry->reason; ?>" readonly>
                    </div>
                </div>

            <?php
            }
            ?>


          </div>


        </div>

        <div class="button-block clearfix">
            <div class="bttn-group">
                <!-- <button type="submit" class="btn btn-primary btn-lg">Save</button> -->
                <a href="../list" class="btn btn-link">Back</a>
            </div>
        </div>
        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>

<script type="text/javascript">
     $('select').select2();
</script>