<?php $this->load->helper("form"); ?>
<form id="form_pr_entry" action="" method="post">
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>View ETF Batch</h3>
            </div>

    <div class="form-container">
            <h4 class="form-group-title">ETF Batch Entry</h4>



            <div class="row">

                  <div class="col-sm-4">
                    <div class="form-group">
                        <label>Bank Reference Number <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $etfBatch->bank_reference_number;?>" readonly="readonly">
                    </div>
                </div>

                  <div class="col-sm-4">
                    <div class="form-group">
                        <label>Batch Id <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $etfBatch->batch_id;?>" readonly="readonly">
                    </div>
                </div>


                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Description <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $etfBatch->description;?>" readonly="readonly">
                    </div>
                </div>





            </div>




            <div class="row">


                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Batch Reference Number <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $etfBatch->reference_number;?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Total Amount <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $etfBatch->total_amount;?>" readonly="readonly">
                    </div>
                </div>

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Status <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="date_time" name="date_time" value="<?php
                        if($etfBatch->status == '0')
                        {
                            echo "Pending";
                        }
                        elseif($etfBatch->status == '1')
                        {
                            echo "Approved";
                        }
                        elseif($etfBatch->status == '2')
                        {
                            echo "Rejected";
                        }
                         ?>" readonly="readonly">
                    </div>
                </div>
            

            </div>

             <div class="row">


             <?php
            if($etfBatch->status == '2')
            {
             ?>


                <div class="col-sm-4" id="view_reject">
                    <div class="form-group">
                        <label>Reject Reason <span class='error-text'>*</span></label>
                        <input type="text" id="reason" name="reason" class="form-control" value="<?php echo $etfBatch->reason; ?>" readonly>
                    </div>
                </div>

            <?php
            }
            ?>


            </div>


        </div>



            <br>
            <h3>Etf Voucher Deails</h3>


        <div class="form-container">
            <h4 class="form-group-title">ETF Voucher Details</h4>






        <div class="custom-table">
            <table class="table">
            <thead>
                 <tr>
                    <th>Sl. No</th>
                    <th>Reference Number</th>
                    <th>Payment Mode </th>
                    <th>Description </th>
                    <th>Letter Reference Number </th>
                    <th>Amount</th>
                 </tr>
            </thead>
            <tbody>
                 <?php 
                  $total = 0;
                 for($i=0;$i<count($etfBatchDetails);$i++)
                    { 
                    // echo "<Pre>";print_r($poDetails[$i]);exit();

                        ?>
                   <tr>
                    <td><?php echo $i+1; ?></td>
                    <td><?php echo $etfBatchDetails[$i]->reference_number;?></td>
                    <td><?php echo $etfBatchDetails[$i]->payment_mode;?></td>
                    <td><?php echo $etfBatchDetails[$i]->description;?></td>
                    <td><?php echo $etfBatchDetails[$i]->letter_reference_number;?></td>
                    <td><?php echo $etfBatchDetails[$i]->amount;?></td>
                    <!-- <td><?php echo $etfBatchDetails[$i]->bank_code . " - " . $etfBatchDetails[$i]->bank_name;?></td> -->
                     </tr>
                  <?php 
                }
                ?>


            </tbody>
        </table>
        </div>


        </div>




  

        <div class="button-block clearfix">
            <div class="bttn-group">
                <!-- <button type="submit" class="btn btn-primary btn-lg">Save</button> -->
                <a href="../list" class="btn btn-link">Back</a>
            </div>
        </div>
        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>