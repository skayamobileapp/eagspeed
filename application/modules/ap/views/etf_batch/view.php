<?php $this->load->helper("form"); ?>
<form id="form_pr_entry" action="" method="post">
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
             <div class="page-title clearfix">
                <h3>Approve ETF Voucher</h3>
            </div>


    <div class="form-container">
            <h4 class="form-group-title">ETF Batch Entry</h4>



            <div class="row">

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Payment Mode <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $etfVoucher->payment_mode;?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Payment Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $etfVoucher->payment_date;?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Bank <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $etfVoucher->bank_code . " - " . $etfVoucher->bank_name;?>" readonly="readonly">
                    </div>
                </div>


            </div>



            <div class="row">

                  <div class="col-sm-4">
                    <div class="form-group">
                        <label>Description <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $etfVoucher->description;?>" readonly="readonly">
                    </div>
                </div>


                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Letter Reference Number <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $etfVoucher->letter_reference_number;?>" readonly="readonly">
                    </div>
                </div>

                  <div class="col-sm-4">
                    <div class="form-group">
                        <label>Batch Id <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $etfVoucher->batch_id;?>" readonly="readonly">
                    </div>
                </div>

            </div>





            <div class="row">

               


                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Total Amount <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="date_time" name="date_time" value="<?php echo $etfVoucher->total_amount;?>" readonly="readonly">
                    </div>
                </div>

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Status <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="date_time" name="date_time" value="<?php
                        if($etfVoucher->status == '0')
                        {
                            echo "Pending";
                        }
                        elseif($etfVoucher->status == '1')
                        {
                            echo "Approved";
                        }
                        elseif($etfVoucher->status == '2')
                        {
                            echo "Rejected";
                        }
                         ?>" readonly="readonly">
                    </div>
                </div>
            

            </div>

             <div class="row">


             <?php
            if($etfVoucher->status == '2')
            {
             ?>


                <div class="col-sm-4" id="view_reject">
                    <div class="form-group">
                        <label>Reject Reason <span class='error-text'>*</span></label>
                        <input type="text" id="reason" name="reason" class="form-control" value="<?php echo $etfVoucher->reason; ?>" readonly>
                    </div>
                </div>

            <?php
            }
            ?>


            </div>




             <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <p> Approval <span class='error-text'>*</span></p>
                        <label class="radio-inline">
                            <input type="radio" id="ed1" name="status" value="1" onclick="hideRejectField()"><span class="check-radio"></span> Approve
                        </label>
                        <label class="radio-inline">
                            <input type="radio" id="ed2" name="status" value="2" onclick="showRejectField()"><span class="check-radio"></span> Reject
                        </label>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-sm-4" id="view_reject" style="display: none">
                    <div class="form-group">
                        <label>Reason <span class='error-text'>*</span></label>
                        <input type="text" id="reason" name="reason" class="form-control">
                    </div>
                </div>
            </div>


        </div>






            <br>
            <h3>Etf Voucher Deails</h3>

    <div class="form-container">
            <h4 class="form-group-title">ETF Voucher Details</h4>






            <div class="custom-table">
            <table class="table">
            <thead>
                 <tr>
                    <th>Sl. No</th>
                    <th>Reference Number</th>
                    <th>Type </th>
                    <th>Entry </th>
                    <th>Payment Mode </th>
                    <th>payee name</th>
                    <th>Amount</th>
                 </tr>
            </thead>
            <tbody>
                 <?php 
                  $total = 0;
                 for($i=0;$i<count($etfVoucherDetails);$i++)
                    { 
                    // echo "<Pre>";print_r($poDetails[$i]);exit();

                        ?>
                   <tr>
                    <td><?php echo $i+1; ?></td>
                    <td><?php echo $etfVoucherDetails[$i]->reference_number;?></td>
                    <td><?php echo $etfVoucherDetails[$i]->type;?></td>
                    <td><?php echo $etfVoucherDetails[$i]->entry;?></td>
                    <td><?php echo $etfVoucherDetails[$i]->payment_mode;?></td>
                    <td><?php echo $etfVoucherDetails[$i]->payee_name;?></td>
                    <!-- <td><?php echo $etfVoucherDetails[$i]->bank_code . " - " . $etfVoucherDetails[$i]->bank_name;?></td> -->
                    <td><?php echo $etfVoucherDetails[$i]->amount;?></td>
                     </tr>
                  <?php 
                  // $total = $total + $poDetails[$i]->total_final;
                }
                // $total = number_format($total, 2, '.', ',');
                ?>

                <!-- <tr>
                    <td bgcolor="" colspan="9"></td>
                    <td bgcolor=""><b> Total : </b></td>
                    <td bgcolor=""><b><?php echo $total; ?></b></td>
                </tr> -->

            </tbody>
            </table>
            </div>

        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="../approvalList" class="btn btn-link">Back</a>
            </div>
        </div>
        
        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>


<script>

  $(document).ready(function() {
        $("#form_pr_entry").validate({
            rules: {
                status: {
                    required: true
                },
                 reason: {
                    required: true
                }
            },
            messages: {
                status: {
                    required: "<p class='error-text'>Select Status",
                },
                reason: {
                    required: "<p class='error-text'>Reason Required",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    function showRejectField(){
            $("#view_reject").show();
    }

    function hideRejectField(){
            $("#view_reject").hide();
    }
</script>