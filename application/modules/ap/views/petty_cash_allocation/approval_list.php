<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>Approval List Petty Cash Allocation</h3>
      <!-- <a href="add" class="btn btn-primary">+ Add Petty Cash Allocation</a> -->
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">
                
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Reference Number</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="name" value="<?php echo $searchParam['name']; ?>">
                    </div>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Budget Year</label>
                    <div class="col-sm-8">
                      <select name="id_budget_year" id="id_budget_year" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($budgetYearList)) {
                          foreach ($budgetYearList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['id_budget_year']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>

              </div>


              <div class="row">

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Financial Year</label>
                    <div class="col-sm-8">
                      <select name="id_financial_year" id="id_financial_year" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($financialYearList)) {
                          foreach ($financialYearList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['id_financial_year']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->year;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Department Code</label>
                    <div class="col-sm-8">
                      <select name="department_code" id="department_code" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($departmentCodeList)) {
                          foreach ($departmentCodeList as $record)
                          {
                            $selected = '';
                            if ($record->code == $searchParam['department_code']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->code;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->code ."-".$record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>

              </div>
              

              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <button type="reset" class="btn btn-link" onclick="clearSearchForm()">Clear All Fields</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Reference Number</th>
            <th>Financial Year</th>
            <th>Budget Year</th>
            <th>GL Code</th>
            <th>Amount</th>
            <th>Min Amount</th>
            <th>Max Amount</th>
            <th>Min Reimbursement Amount</th>
            <th>Max Reimbursement Amount</th>
            <th>Status</th>
            <th style="text-align:center; ">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($pettyCashAllocationList))
          {
            $i=0;
            foreach ($pettyCashAllocationList as $record) {
          ?>
              <tr>
                 <td><?php echo $i+1;?></td>
                <td><?php echo $record->reference_number ?></td>
                <td><?php echo $record->financial_year ?></td>
                <td><?php echo $record->budget_year ?></td>
                <td><?php echo $record->fund_code . " - " . $record->department_code . " - " . $record->activity_code . " - " . $record->account_code ?></td>
                <td><?php echo $record->amount ?></td>
                <td><?php echo $record->min_amount ?></td>
                <td><?php echo $record->max_amount ?></td>
                <td><?php echo $record->min_reimbursement_amount ?></td>
                <td><?php echo $record->max_reimbursement_amount ?></td>
                <td><?php if( $record->status == '0')
                {
                  echo "Pending";
                }
                elseif( $record->status == '1')
                {
                  echo "Approved";
                }
                elseif( $record->status == '2')
                {
                  echo "Rejected";
                } 
                ?></td>
                <td class="text-center">
                  <a href="<?php echo 'view/' . $record->id; ?>" title="View">Approve</a>
                  <!--  -->
                </td>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script type="text/javascript">
    $('select').select2();
</script>