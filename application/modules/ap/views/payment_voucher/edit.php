<?php $this->load->helper("form"); ?>
<form id="form_pr_entry" action="" method="post">
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>View Payment Voucher</h3>
            </div>

      <div class="form-container">
        <h4 class="form-group-title">Payment Voucher Entry</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Voucher Type <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $paymentVoucher->type;?>" readonly="readonly">
                    </div>
                </div>

                 <div class="col-sm-4">
                    <div class="form-group">
                      <label>Financial Year <span class='error-text'>*</span></label>
                      <select name="id_financial_year" id="id_financial_year" class="form-control" disabled="true">
                          <option value="">Select</option>
                          
                           <?php
                          if (!empty($financialYearList)) {
                            foreach ($financialYearList as $record)
                            {
                              $selected = '';
                              if ($record->id == $paymentVoucher->id_financial_year) {
                                $selected = 'selected';
                              }
                          ?>
                              <option value="<?php echo $record->id;  ?>"
                                <?php echo $selected;  ?>>
                                <?php echo  $record->name;  ?>
                                </option>
                          <?php
                            }
                          }
                          ?>

                      </select>
                  </div>
                </div>


                 <div class="col-sm-4">
                    <div class="form-group">
                      <label>Budget Year <span class='error-text'>*</span></label>
                      <select name="id_budget_year" id="id_budget_year" class="form-control" disabled="true">
                          <option value="">Select</option>
                          
                           <?php
                          if (!empty($budgetYearList)) {
                            foreach ($budgetYearList as $record)
                            {
                              $selected = '';
                              if ($record->id == $paymentVoucher->id_budget_year) {
                                $selected = 'selected';
                              }
                          ?>
                              <option value="<?php echo $record->id;  ?>"
                                <?php echo $selected;  ?>>
                                <?php echo  $record->name;  ?>
                                </option>
                          <?php
                            }
                          }
                          ?>

                      </select>
                  </div>
                </div>

                

                 

            </div>



            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Entry Type <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="entry" name="entry" value="<?php echo $paymentVoucher->entry;?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Voucher Number <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="entry" name="entry" value="<?php echo $paymentVoucher->reference_number;?>" readonly="readonly">
                    </div>
                </div>

                


                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Payment Reference Number <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $paymentVoucher->payment_reference_number;?>" readonly="readonly">
                    </div>
                </div>


            </div>



            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Account Number <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $paymentVoucher->account_number;?>" readonly="readonly">
                    </div>
                </div>

               

              <div class="col-sm-4">
                    <div class="form-group">
                        <label>Bank <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $paymentVoucher->bank_code . " - " . $paymentVoucher->bank_name;?>" readonly="readonly">
                    </div>
                </div>
              

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Bank Account Number <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $paymentVoucher->account_number;?>" readonly="readonly">
                    </div>
                </div>

            </div>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Payment Mode <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $paymentVoucher->payment_mode;?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Voucher Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $paymentVoucher->payment_date;?>" readonly="readonly">
                    </div>
                </div>

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Description <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $paymentVoucher->description;?>" readonly="readonly">
                    </div>
                </div>


            </div>


            <div class="row">


                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Total Amount <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="date_time" name="date_time" value="<?php echo $paymentVoucher->total_amount;?>" readonly="readonly">
                    </div>
                </div>


                <?php
                if($paymentVoucher->type == 'Petty Cash')
                {
                 ?>


                    <div class="col-sm-4" id="view_reject">
                        <div class="form-group">
                            <label>Staff  <span class='error-text'>*</span></label>
                            <input type="text" id="reason" name="reason" class="form-control" value="<?php echo $paymentVoucher->staff_name; ?>" readonly>
                        </div>
                    </div>

                <?php
                }
                ?>



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Status <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="date_time" name="date_time" value="<?php
                        if($paymentVoucher->status == '0')
                        {
                            echo "Pending";
                        }
                        elseif($paymentVoucher->status == '1')
                        {
                            echo "Approved";
                        }
                        elseif($paymentVoucher->status == '2')
                        {
                            echo "Rejected";
                        }
                        elseif($paymentVoucher->status == '3')
                        {
                            echo "Cancelled";
                        }
                         ?>" readonly="readonly">
                    </div>
                </div>

                

            </div>



            <div class="row">

           
             <?php
            if($paymentVoucher->status == '2')
            {
             ?>

                <div class="col-sm-4" id="view_reject">
                    <div class="form-group">
                        <label>Reject Reason <span class='error-text'>*</span></label>
                        <input type="text" id="reason" name="reason" class="form-control" value="<?php echo $paymentVoucher->reason; ?>" readonly>
                    </div>
                </div>

            <?php
            }elseif($paymentVoucher->status == '3')
            {
             ?>


                <div class="col-sm-4" id="view_reject">
                    <div class="form-group">
                        <label>Cancelled Reason <span class='error-text'>*</span></label>
                        <input type="text" id="reason" name="reason" class="form-control" value="<?php echo $paymentVoucher->reason; ?>" readonly>
                    </div>
                </div>

            <?php
            }
            ?>


            </div>

        </div>



            <br>
            <h3>Payment Voucher Deails</h3>

          <div class="form-container">
        <h4 class="form-group-title">Payment Voucher Details</h4>




            <?php

             if($paymentVoucher->type == 'Investment')
            {
             ?>


            <div class="custom-table">
            <table class="table">
            <thead>
                 <tr>
                    <th>Sl. No</th>
                    <th>Registration Number</th>
                    <th>Credit GL Code </th>
                    <th>Duration</th>
                    <th>Maturity Date</th>
                    <th>Profit Amount</th>
                    <th>Amount</th>
                 </tr>
            </thead>
            <tbody>
                 <?php 
                  $total = 0;
                 for($i=0;$i<count($paymentVoucherDetailsByInvestment);$i++)
                    { 
                    // echo "<Pre>";print_r($poDetails[$i]);exit();

                        ?>
                   <tr>
                    <td><?php echo $i+1; ?></td>
                    <td><?php echo $paymentVoucherDetailsByInvestment[$i]->registration_number;?></td>
                    <td><?php echo $paymentVoucherDetailsByInvestment[$i]->fund_code . " - " . $paymentVoucherDetailsByInvestment[$i]->department_code . " - " . $paymentVoucherDetailsByInvestment[$i]->activity_code  . " - " . $paymentVoucherDetailsByInvestment[$i]->account_code;?></td>
                    <td><?php echo $paymentVoucherDetailsByInvestment[$i]->duration . " - " . $paymentVoucherDetailsByInvestment[$i]->duration_type;?></td>
                    <td><?php echo $paymentVoucherDetailsByInvestment[$i]->maturity_date;?></td>
                    <td><?php echo $paymentVoucherDetailsByInvestment[$i]->profit_amount;?></td>
                    <td><?php echo $paymentVoucherDetailsByInvestment[$i]->amount;?></td>
                     </tr>
                  <?php 
                  // $total = $total + $poDetails[$i]->total_final;
                }
                // $total = number_format($total, 2, '.', ',');
                ?>

                <!-- <tr>
                    <td bgcolor="" colspan="9"></td>
                    <td bgcolor=""><b> Total : </b></td>
                    <td bgcolor=""><b><?php echo $total; ?></b></td>
                </tr> -->

            </tbody>
        </table>
        </div>

            <?php
            }
            


            if($paymentVoucher->type == 'Bill Registration')
            {
             ?>


                  <div class="custom-table">
        <table class="table">
            <thead>
                 <tr>
                    <th>Sl. No</th>
                    <th>Reference Number</th>
                    <th>Type</th>
                    <th>Description</th>
                    <th>Contact Person</th>
                    <th>Bank Account Number</th>
                    <th>Total Amount</th>
                 </tr>
            </thead>
            <tbody>
                 <?php 
                  $total = 0;
                 for($i=0;$i<count($paymentVoucherDetailsByBillRegistration);$i++)
                    { 
                        ?>
                   <tr>
                    <td><?php echo $i+1; ?></td>
                    <td><?php echo $paymentVoucherDetailsByBillRegistration[$i]->reference_number;?></td>
                    <td><?php echo $paymentVoucherDetailsByBillRegistration[$i]->type;?></td>
                    <td><?php echo $paymentVoucherDetailsByBillRegistration[$i]->description;?></td>
                    <td><?php echo $paymentVoucherDetailsByBillRegistration[$i]->contact_person_one;?></td>
                    <td><?php echo $paymentVoucherDetailsByBillRegistration[$i]->bank_acc_no;?></td>
                    <td><?php echo $paymentVoucherDetailsByBillRegistration[$i]->total_amount;?></td>
                     </tr>
                  <?php 
                }
                ?>

            </tbody>
        </table>
        </div>

            <?php
            }

            if($paymentVoucher->type == 'Petty Cash')
            {
             ?>


                  <div class="custom-table">
        <table class="table">
            <thead>
                 <tr>
                    <th>Sl. No</th>
                    <th>Reference Number</th>
                    <th>Description</th>
                    <th>Debit Gl Code</th>
                    <th>Credit GL Code</th>
                    <th>Amount</th>
                 </tr>
            </thead>
            <tbody>
                 <?php 
                  $total = 0;
                 for($i=0;$i<count($paymentVoucherDetailsByPettyCash);$i++)
                    { 
                        ?>
                   <tr>
                    <td><?php echo $i+1; ?></td>
                    <td><?php echo $paymentVoucherDetailsByPettyCash[$i]->reference_number;?></td>
                    <td><?php echo $paymentVoucherDetailsByPettyCash[$i]->description;?></td>
                    <td><?php echo $paymentVoucherDetailsByPettyCash[$i]->dt_fund . " - " . $paymentVoucherDetailsByPettyCash[$i]->dt_department . " - " . $paymentVoucherDetailsByPettyCash[$i]->dt_activity  . " - " . $paymentVoucherDetailsByPettyCash[$i]->dt_account;?></td>
                    <td><?php echo $paymentVoucherDetailsByPettyCash[$i]->cr_fund . " - " . $paymentVoucherDetailsByPettyCash[$i]->cr_department . " - " . $paymentVoucherDetailsByPettyCash[$i]->cr_activity  . " - " . $paymentVoucherDetailsByPettyCash[$i]->cr_account;?></td>
                    <td><?php echo $paymentVoucherDetailsByPettyCash[$i]->amount;?></td>
                     </tr>
                  <?php 
                }
                ?>

            </tbody>
        </table>
        </div>

            <?php
            }
            ?>   


          </div>




      

            <div class="button-block clearfix">
                <div class="bttn-group">
                    <!-- <button type="submit" class="btn btn-primary btn-lg">Save</button> -->
                    <a href="../list" class="btn btn-link">Back</a>
                </div>
            </div>
        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>