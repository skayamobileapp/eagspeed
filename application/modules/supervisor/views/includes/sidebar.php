            <div class="sidebar">
                <div class="user-profile clearfix">
                    <a href="/supervisor/profile" class="user-profile-link">
                        <span>
                            <img src="<?php echo BASE_PATH; ?>assets/img/user_profile.jpg">
                        </span> <?php echo $supervisor_name; ?>
                    </a>
                    <div class="dropdown">
                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                            <!-- <li><a href="/sponser/user/profile">Edit Profile</a></li> -->
                            <li><a href="/supervisor/profile/logout">Logout</a></li>
                        </ul>
                    </div>
                </div>

                <div class="sidebar-nav">

                    <h4>
                        <a role="button" data-toggle="collapse" class="collapse" href="#collapse_one">Profile <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a>
                    </h4>
                    <ul class="collapse" id="collapse_one">                  
                        <li><a href="/supervisor/profile/view">Profile</a></li>
                        <li><a href="/supervisor/viewStudents/view">View Students</a></li>
                    </ul>

                    <h4>
                        <a role="button" data-toggle="collapse" class="collapse" href="#collapse_two">Research Stage 1 <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a>
                    </h4>
                    <ul class="collapse" id="collapse_two">                  
                        <li><a href="/supervisor/deliverables/list">Research Proposal Application</a></li>
                        <li><a href="/supervisor/proposalReporting/list">Research Proposal Reporting</a></li>
                    </ul>


                    <h4>
                        <a role="button" data-toggle="collapse" class="collapse" href="#collapse_three">Stage 2 <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a>
                    </h4>
                    <ul class="collapse" id="collapse_three">                  
                        <li><a href="/supervisor/proposalReporting/list2">Listing of students and their research progress report (Chapter 3)</a></li>
                    </ul>

                    <h4>
                        <a role="button" data-toggle="collapse" class="collapse" href="#collapse_four">Stage 3 <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a>
                    </h4>
                    <ul class="collapse" id="collapse_four">                  
                        <li><a href="/supervisor/proposalReporting/list3">Listing of students and their research progress report (Chapter 4)</a></li>
                    </ul>


                    <h4>
                        <a role="button" data-toggle="collapse" class="collapse" href="#collapse_five">Stage 4 <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a>
                    </h4>
                    <ul class="collapse" id="collapse_five">                  
                        <li><a href="/supervisor/toc/list">Research TOC</a></li>
                        <li><a href="/supervisor/abstractt/list">Research Abstract</a></li>
                        <li><a href="/supervisor/bound/list">5 copies of hard bound submission</a></li>
                        <li><a href="/supervisor/sco/list">Research Soft Copy Of File</a></li>
                        <li><a href="/supervisor/ppt/list">Research Power Point Presentation</a></li>
                    </ul>

                </div>

            </div>