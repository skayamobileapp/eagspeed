<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>View Proposal Reporting Comments (Stage <?php echo $stage; ?>)</h3>
        </div>


        <div class="form-container">
            <h4 class="form-group-title">Student Details</h4>
            <div class='data-list'>
               <div class='row'>
                  <div class='col-sm-6'>
                     <dl>
                        <dt>Student Name :</dt>
                        <dd><?php echo ucwords($studentDetails->full_name);?></dd>
                     </dl>
                     <dl>
                        <dt>Student NRIC :</dt>
                        <dd><?php echo $studentDetails->nric ?></dd>
                     </dl>
                     <dl>
                        <dt>Student Email :</dt>
                        <dd><?php echo $studentDetails->email_id; ?></dd>
                     </dl>
                     <dl>
                        <dt>Learning Mode :</dt>
                        <dd><?php echo $studentDetails->program_scheme ?></dd>
                     </dl>
                     <dl>
                        <dt>Organisation :</dt>
                        <dd><?php
                           if($studentDetails->id_university != 1 && $studentDetails->id_university != 0)
                           {
                               echo $studentDetails->partner_university_code . " - " . $studentDetails->partner_university_name;
                           }
                           else
                           {
                               echo $organisationDetails->short_name . " - " . $organisationDetails->name;
                           }
                             ?></dd>
                     </dl>
                     
                  </div>
                  <div class='col-sm-6'>
                     <dl>
                        <dt>Intake :</dt>
                        <dd><?php echo $studentDetails->intake_name ?></dd>
                     </dl>
                     <dl>
                        <dt>Program :</dt>
                        <dd><?php echo $studentDetails->programme_code . " - " . $studentDetails->programme_name; ?></dd>
                     </dl>
                     <dl>
                        <dt>Nationality :</dt>
                        <dd><?php echo $studentDetails->nationality ?></dd>
                     </dl>
                     <dl>
                        <dt>Advisor :</dt>
                        <dd><?php echo $studentDetails->ic_no . " - " . $studentDetails->advisor_name; ?></dd>
                     </dl>
                     <dl>
                        <dt>Branch :</dt>
                        <dd><?php
                           if($studentDetails->id_branch != 1 && $studentDetails->id_branch != 0)
                           {
                               echo $studentDetails->branch_code . " - " . $studentDetails->branch_name;
                           }
                           else
                           {
                               echo $organisationDetails->short_name . " - " . $organisationDetails->name;
                           }
                             ?></dd>
                     </dl>
                  </div>
               </div>
            </div>
         </div>



        

        <div class="form-container">
            <h4 class="form-group-title">Proposal Reporting Details</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Application Number <span class='error-text'>*</span></label>
                        <input type="email" class="form-control" id="application_number" name="application_number" value="<?php echo $proposalReporting->application_number;?>" readonly>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Phd Duration <span class='error-text'>*</span></label>
                        <select name="phd_duration" id="phd_duration" class="form-control" disabled>
                            <option value="">Select</option>
                            <?php
                            if (!empty($durationList))
                            {
                                foreach ($durationList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                                <?php if($proposalReporting->phd_duration == $record->id)
                                {
                                    echo 'selected'; 
                                }
                                ?>
                                >
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Chapter <span class='error-text'>*</span></label>
                        <select name="id_chapter" id="id_chapter" class="form-control" disabled>
                            <option value="">Select</option>
                            <?php
                            if (!empty($chapterList))
                            {
                                foreach ($chapterList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                                <?php if($proposalReporting->id_chapter == $record->id)
                                {
                                    echo 'selected'; 
                                }
                                ?>
                                >
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


            </div>    

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Deliverable <span class='error-text'>*</span></label>
                        <select name="id_topic" id="id_topic" class="form-control" disabled>
                            <option value="">Select</option>
                            <?php
                            if (!empty($deliverableList))
                            {
                                foreach ($deliverableList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                                <?php if($proposalReporting->id_deliverable == $record->id)
                                {
                                    echo 'selected'; 
                                }
                                ?>
                                >
                                <?php echo $record->topic;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>        





                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Start Date <span class='error-text'>*</span></label>
                        <input type="email" class="form-control" id="from_dt" name="from_dt" value="<?php if($proposalReporting->from_dt){ echo date('d-m-Y', strtotime($proposalReporting->from_dt)); }?>" readonly>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>To Date <span class='error-text'>*</span></label>
                        <input type="email" class="form-control" id="to_dt" name="to_dt" value="<?php if($proposalReporting->to_dt){ echo date('d-m-Y', strtotime($proposalReporting->to_dt)); }?>" readonly>
                    </div>
                </div>


            </div>    

            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Target Date <span class='error-text'>*</span></label>
                        <input type="email" class="form-control" id="target_dt" name="target_dt" value="<?php if($proposalReporting->target_dt){ echo date('d-m-Y', strtotime($proposalReporting->target_dt)); }?>" readonly>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Submitted On <span class='error-text'>*</span></label>
                        <input type="email" class="form-control" id="created_dt_tm" name="created_dt_tm" value="<?php if($proposalReporting->created_dt_tm){ echo date('d-m-Y', strtotime($proposalReporting->created_dt_tm)); }?>" readonly>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Status <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="status" name="status" value="<?php
                        if($proposalReporting->status == '0')
                        {
                            echo "Pending";
                        }
                        elseif($proposalReporting->status == '1')
                        {
                            echo 'Approved';
                        }
                        // elseif($proposalReporting->status == '2')
                        // {
                        //     echo "Rejected";
                        // }
                         ?>" readonly="readonly">
                    </div>
                </div>





            </div>    



            <!-- <div class="row">

                <div class="col-sm-12">
                    <div class="form-group shadow-textarea">
                      <label for="message">Description <span class='error-text'>*</span></label>
                      <textarea class="form-control z-depth-1" rows="3" placeholder="Write Description..." name="description" id="description"><?php echo $proposalReporting->description;?></textarea>
                    </div>
                </div>

            </div>
 -->


        </div>


        <br>


        <form id="form_data" action="" method="post" enctype="multipart/form-data">

            <div class="form-container">
            <h4 class="form-group-title">Report Comments Details</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Upload File <span class='error-text'>*</span></label>
                        <input type="file" class="form-control" id="upload_file" name="upload_file">
                    </div>
                </div>



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Comment / Reason <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="comments" name="comments">
                    </div>
                </div>


            </div>


            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="<?php $st = $stage; if($stage == 1) { $st = ''; } echo '../list' . $st ?>" class="btn btn-link">Back</a>
                </div>
            </div>



        </form>


            <?php

            if(!empty($proposalReportingComments))
            {
                ?>
                <br>

                <div class="form-container">
                        <h4 class="form-group-title">Reporting Comments Details</h4>

                    

                      <div class="custom-table">
                        <table class="table">
                            <thead>
                                <tr>
                                <th>Sl. No</th>
                                 <th>Comments</th>
                                 <th>Updated By</th>
                                 <th>Date</th>
                                 <th style="text-align: center;">File</th>
                                 <!-- <th style="text-align: center;">Action</th> -->
                                </tr>
                            </thead>
                            <tbody>
                                 <?php
                             $total = 0;
                              for($i=0;$i<count($proposalReportingComments);$i++)
                             { ?>
                                <tr>
                                <td><?php echo $i+1;?></td>
                                <td><?php echo $proposalReportingComments[$i]->comments;?></td>
                                <td><?php echo 'Supervisor'; ?></td>
                                <td><?php echo date('d-m-Y', strtotime($proposalReportingComments[$i]->created_dt_tm));?></td>
                                <td class="text-center">

                                    <a href="<?php echo '/assets/images/' . $proposalReportingComments[$i]->upload_file; ?>" target="popup" onclick="window.open(<?php echo '/assets/images/' . $proposalReportingComments[$i]->upload_file; ?>)" title="<?php echo $proposalReportingComments[$i]->upload_file; ?>">View</a>
                                </td>
                                <!-- <td class="text-center">
                                    <a onclick="deleteApplicantUploadedDocument(<?php echo $proposalReportingComments[$i]->id; ?>)">Delete</a>
                                </td> -->

                                 </tr>
                              <?php
                          } 
                          ?>
                            </tbody>
                        </table>
                      </div>

                    </div>

            <?php
            
            }
             ?>





        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>

<script src="<?php echo BASE_PATH; ?>assets/ckeditor/ckeditor.js"></script>

<style type="text/css">
    .shadow-textarea textarea.form-control::placeholder {
    font-weight: 300;
}
.shadow-textarea textarea.form-control {
    padding-left: 0.8rem;
}
</style>

<script>

    $('select').select2();

    CKEDITOR.replace('description',{

      width: "800px",
      height: "200px"

    }); 


    function getChapterByDuration(id)
    {
        if(id != '')
        {
            $.get("/student/proposalReporting/getChapterByDuration/"+id, function(data, status){
           
                $("#view_chapter").html(data);
            });
        }
    }


    function getTopicByData()
     {
        var tempPR = {};
        tempPR['id_phd_duration'] = $("#id_phd_duration").val();
        tempPR['id_chapter'] = $("#id_chapter").val();
        // alert(tempPR['id_program']);

        if(tempPR['id_phd_duration'] != '' && tempPR['id_chapter'] != '')
        {

            $.ajax(
            {
               url: '/student/proposalReporting/getTopicByData',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_topic").html(result);
               }
            });
        }
     }


    $(document).ready(function() {
        $("#form_data").validate({
            rules: {
                comments: {
                    required: true
                },
                id_chapter: {
                    required: true
                },
                id_topic: {
                    required: true
                }
            },
            messages: {
                comments: {
                    required: "<p class='error-text'>Comments Required</p>",
                },
                id_chapter: {
                    required: "<p class='error-text'>Select Chapter</p>",
                },
                id_topic: {
                    required: "<p class='error-text'>Select Topic</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    $(function(){
        $( ".datepicker" ).datepicker();
      });

</script>