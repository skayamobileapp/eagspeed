<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Research Proposal Reporting(Stage <?php echo $stage; ?>)</h3>
        </div>




        <h4 class='sub-title'>Student Details</h4>

        <div class='data-list'>
            <div class='row'>

                <div class='col-sm-6'>
                    <dl>
                        <dt>Student Name :</dt>
                        <dd><?php echo $studentDetails->full_name ?></dd>
                    </dl>
                    <dl>
                        <dt>Student Email :</dt>
                        <dd><?php echo $studentDetails->email_id ?></dd>
                    </dl>
                    <dl>
                        <dt>Student NRIC :</dt>
                        <dd><?php echo $studentDetails->nric ?></dd>
                    </dl>
                    
                </div>        
                
                <div class='col-sm-6'>
                    <dl>
                        <dt>Intake :</dt>
                        <dd>
                            <?php echo $studentDetails->intake_name ?>
                        </dd>
                    </dl>
                    <dl>
                        <dt>Programme :</dt>
                        <dd><?php echo $studentDetails->programme_name ?></dd>
                    </dl>
                    <dl>
                        <dt>Nationality :</dt>
                        <dd><?php echo $studentDetails->nationality ?></dd>
                    </dl>
                </div>

            </div>
        </div>






        <form id="form_internship" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Research Proposal Reporting Details</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Chapter Duration <span class='error-text'>*</span></label>
                        <select name="phd_duration" id="phd_duration" class="form-control" onchange="getChapterByDuration(this.value)">
                            <option value="">Select</option>
                            <?php
                            if (!empty($durationList))
                            {
                                foreach ($durationList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Chapter <span class='error-text'>*</span></label>
                        <span id="view_chapter">
                            <select class="form-control" id='id_chapter' name='id_chapter'>
                            <option value=''></option>
                        </select>
                        </span>
                    </div>
                </div>


                

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Deliverable <span class='error-text'>*</span></label>
                        <span id="view_topic">
                            <select class="form-control" id='id_deliverable' name='id_deliverable'>
                            <option value=''></option>
                        </select>
                        </span>
                    </div>
                </div>


            </div>


            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Start Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="from_dt" name="from_dt" autocomplete="off">
                    </div>
               </div>


               <div class="col-sm-4">
                    <div class="form-group">
                        <label>End Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="to_dt" name="to_dt" autocomplete="off">
                    </div>
               </div>


               <div class="col-sm-4">
                    <div class="form-group">
                        <label>Target Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="target_dt" name="target_dt" autocomplete="off">
                    </div>
               </div>


            </div>

    

            <div class="row">


                <div class="col-sm-12">
                    <div class="form-group shadow-textarea">
                      <label for="message">Description <span class='error-text'>*</span></label>
                      <textarea class="form-control z-depth-1" rows="3" placeholder="Write Description..." name="description" id="description"></textarea>
                    </div>
                </div>

            </div>



        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
        
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
          
                <a href="<?php $st = $stage; if($stage == 1) { $st = ''; } echo 'list' . $st; ?>" class="btn btn-link">Cancel</a>
            </div>
        </div>

        </form>


        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>

<script src="<?php echo BASE_PATH; ?>assets/ckeditor/ckeditor.js"></script>


<style type="text/css">
    .shadow-textarea textarea.form-control::placeholder {
    font-weight: 300;
}
.shadow-textarea textarea.form-control {
    padding-left: 0.8rem;
}
</style>

<script>

    $('select').select2();


    $(function(){
        $( ".datepicker" ).datepicker();
      });



    CKEDITOR.replace('description',{

      width: "800px",
      height: "200px"

    }); 


    function getChapterByDuration(id)
    {
        if(id != '')
        {
            $.get("/student/proposalReporting/getChapterByDuration/"+id, function(data, status){
           
                $("#view_chapter").html(data);
            });
        }
    }


    function getTopicByData()
     {
        var tempPR = {};
        tempPR['id_phd_duration'] = $("#phd_duration").val();
        tempPR['id_chapter'] = $("#id_chapter").val();
        // alert(tempPR['id_program']);

        if(tempPR['id_phd_duration'] != '' && tempPR['id_chapter'] != '')
        {

            $.ajax(
            {
               url: '/student/proposalReporting/getTopicByData',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_topic").html(result);
               }
            });
        }
     }


    $(document).ready(function() {
        $("#form_internship").validate({
            rules: {
                phd_duration: {
                    required: true
                },
                status: {
                    required: true
                },
                id_deliverable: {
                    required: true
                },
                from_dt: {
                    required: true
                },
                to_dt: {
                    required: true
                },
                target_dt: {
                    required: true
                },
                id_chapter: {
                    required: true
                }
            },
            messages: {
                phd_duration: {
                    required: "<p class='error-text'>Select Phd Duration</p>",
                },
                status: {
                    required: "<p class='error-text'>Select Status</p>",
                },
                id_deliverable: {
                    required: "<p class='error-text'>Select Deliverable</p>",
                },
                from_dt: {
                    required: "<p class='error-text'>Select Start Date</p>",
                },
                to_dt: {
                    required: "<p class='error-text'>Select End Date</p>",
                },
                target_dt: {
                    required: "<p class='error-text'>Select Target Deadline Date</p>",
                },
                id_chapter: {
                    required: "<p class='error-text'>Select Chapter</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>