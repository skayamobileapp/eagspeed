<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Deliverable Application</h3>
        </div>
        <form id="form_internship" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Deliverable Details</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Phd Duration <span class='error-text'>*</span></label>
                        <select name="id_phd_duration" id="id_phd_duration" class="form-control" onchange="getChapterByDuration(this.value)">
                            <option value="">Select</option>
                            <?php
                            if (!empty($durationList))
                            {
                                foreach ($durationList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Chapter <span class='error-text'>*</span></label>
                        <span id="view_chapter">
                            <select class="form-control" id='id_chapter' name='id_chapter'>
                            <option value=''></option>
                        </select>
                        </span>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Topic <span class='error-text'>*</span></label>
                        <span id="view_topic">
                            <select class="form-control" id='id_topic' name='id_topic'>
                            <option value=''></option>
                        </select>
                        </span>
                    </div>
                </div>

        



            </div>

    

            <div class="row">


                <div class="col-sm-12">
                    <div class="form-group shadow-textarea">
                      <label for="message">Description <span class='error-text'>*</span></label>
                      <textarea class="form-control z-depth-1" rows="3" placeholder="Write Description..." name="description" id="description"></textarea>
                    </div>
                </div>

            </div>



        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
        
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
          
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>

        </form>


        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>

<script src="<?php echo BASE_PATH; ?>assets/ckeditor/ckeditor.js"></script>


<style type="text/css">
    .shadow-textarea textarea.form-control::placeholder {
    font-weight: 300;
}
.shadow-textarea textarea.form-control {
    padding-left: 0.8rem;
}
</style>

<script>

    $('select').select2();

    CKEDITOR.replace('description',{

      width: "800px",
      height: "200px"

    }); 


    function getChapterByDuration(id)
    {
        if(id != '')
        {
            $.get("/student/deliverables/getChapterByDuration/"+id, function(data, status){
           
                $("#view_chapter").html(data);
            });
        }
    }


    function getTopicByData()
     {
        var tempPR = {};
        tempPR['id_phd_duration'] = $("#id_phd_duration").val();
        tempPR['id_chapter'] = $("#id_chapter").val();
        // alert(tempPR['id_program']);

        if(tempPR['id_phd_duration'] != '' && tempPR['id_chapter'] != '')
        {

            $.ajax(
            {
               url: '/student/deliverables/getTopicByData',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_topic").html(result);
               }
            });
        }
     }


    $(document).ready(function() {
        $("#form_internship").validate({
            rules: {
                id_phd_duration: {
                    required: true
                },
                id_chapter: {
                    required: true
                },
                id_topic: {
                    required: true
                }
            },
            messages: {
                id_phd_duration: {
                    required: "<p class='error-text'>Select Phd Duration</p>",
                },
                id_chapter: {
                    required: "<p class='error-text'>Select Chapter</p>",
                },
                id_topic: {
                    required: "<p class='error-text'>Select Topic</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    $(function(){
        $( ".datepicker" ).datepicker();
      });

</script>