<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>View Research Proposal Application</h3>
        </div>

        <div class="form-container">
            <h4 class="form-group-title">Student Details</h4>
            <div class='data-list'>
               <div class='row'>
                  <div class='col-sm-6'>
                     <dl>
                        <dt>Student Name :</dt>
                        <dd><?php echo ucwords($studentDetails->full_name);?></dd>
                     </dl>
                     <dl>
                        <dt>Student NRIC :</dt>
                        <dd><?php echo $studentDetails->nric ?></dd>
                     </dl>
                     <dl>
                        <dt>Student Email :</dt>
                        <dd><?php echo $studentDetails->email_id; ?></dd>
                     </dl>
                     <dl>
                        <dt>Learning Mode :</dt>
                        <dd><?php echo $studentDetails->program_scheme ?></dd>
                     </dl>
                     <dl>
                        <dt>Organisation :</dt>
                        <dd><?php
                           if($studentDetails->id_university != 1 && $studentDetails->id_university != 0)
                           {
                               echo $studentDetails->partner_university_code . " - " . $studentDetails->partner_university_name;
                           }
                           else
                           {
                               echo $organisationDetails->short_name . " - " . $organisationDetails->name;
                           }
                             ?></dd>
                     </dl>
                     
                  </div>
                  <div class='col-sm-6'>
                     <dl>
                        <dt>Intake :</dt>
                        <dd><?php echo $studentDetails->intake_name ?></dd>
                     </dl>
                     <dl>
                        <dt>Program :</dt>
                        <dd><?php echo $studentDetails->programme_code . " - " . $studentDetails->programme_name; ?></dd>
                     </dl>
                     <dl>
                        <dt>Nationality :</dt>
                        <dd><?php echo $studentDetails->nationality ?></dd>
                     </dl>
                     <dl>
                        <dt>Advisor :</dt>
                        <dd><?php echo $studentDetails->ic_no . " - " . $studentDetails->advisor_name; ?></dd>
                     </dl>
                     <dl>
                        <dt>Branch :</dt>
                        <dd><?php
                           if($studentDetails->id_branch != 1 && $studentDetails->id_branch != 0)
                           {
                               echo $studentDetails->branch_code . " - " . $studentDetails->branch_name;
                           }
                           else
                           {
                               echo $organisationDetails->short_name . " - " . $organisationDetails->name;
                           }
                             ?></dd>
                     </dl>
                  </div>
               </div>
            </div>
         </div>

         
        <form id="form_internship" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Deliverable Details</h4>


            <div class="row">

                <!-- <div class="col-sm-4">
                    <div class="form-group">
                        <label>Application Number <span class='error-text'>*</span></label>
                        <input type="email" class="form-control" id="application_number" name="application_number" value="<?php echo $deliverables->application_number;?>" readonly>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Phd Duration <span class='error-text'>*</span></label>
                        <select name="id_phd_duration" id="id_phd_duration" class="form-control" disabled>
                            <option value="">Select</option>
                            <?php
                            if (!empty($durationList))
                            {
                                foreach ($durationList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                                <?php if($deliverables->id_phd_duration == $record->id)
                                {
                                    echo 'selected'; 
                                }
                                ?>
                                >
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Chapter <span class='error-text'>*</span></label>
                        <select name="id_chapter" id="id_chapter" class="form-control" disabled>
                            <option value="">Select</option>
                            <?php
                            if (!empty($chapterList))
                            {
                                foreach ($chapterList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                                <?php if($deliverables->id_chapter == $record->id)
                                {
                                    echo 'selected'; 
                                }
                                ?>
                                >
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div> -->

                


            </div>    

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Application Number <span class='error-text'>*</span></label>
                        <input type="email" class="form-control" id="application_number" name="application_number" value="<?php echo $deliverables->application_number;?>" readonly>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Topic <span class='error-text'>*</span></label>
                        <select name="id_topic" id="id_topic" class="form-control" disabled>
                            <option value="">Select</option>
                            <?php
                            if (!empty($topicList))
                            {
                                foreach ($topicList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                                <?php if($deliverables->id_topic == $record->id)
                                {
                                    echo 'selected'; 
                                }
                                ?>
                                >
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>        


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Status <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="status" name="status" value="<?php echo $deliverables->status;
                         ?>" readonly="readonly">
                    </div>
                </div>


               <!--  <div class="col-sm-4">
                    <div class="form-group">
                        <label>Comments / Reason <span class='error-text'>*</span></label>
                        <input type="email" class="form-control" id="reason" name="reason" value="<?php echo $deliverables->reason;?>" readonly>
                    </div>
                </div> -->




            </div>

            <!-- <?php
            if($deliverables->status != 0)
            {
            ?>
            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Approved / Rejected On <span class='error-text'>*</span></label>
                        <input type="email" class="form-control" id="approved_on" name="approved_on" value="<?php if($deliverables->approved_on){ echo date('d-m-Y', strtotime($deliverables->approved_on)); }?>" readonly>
                    </div>
                </div>

            </div>

            <?php
            }
            ?> -->


            <div class="row">



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>FILE 

                            <span class='error-text'>*</span>
                            <?php 
                            if($deliverables->upload_file != '')
                            {
                            ?>
                            <a href="<?php echo '/assets/images/' . $deliverables->upload_file; ?>" target="popup" onclick="window.open(<?php echo '/assets/images/' . $deliverables->upload_file; ?>)" title="<?php echo  $deliverables->upload_file; ?>"> View </a>

                            <?php
                            }
                            ?>


                        </label>
                        <input type="file" name="image" />
                    </div>                    
                </div>


            </div>


            <div class="row">

                <div class="col-sm-12">
                    <div class="form-group shadow-textarea">
                      <label for="message">Description <span class='error-text'>*</span></label>
                      <textarea class="form-control z-depth-1" rows="3" placeholder="Write Description..." name="description" id="description"><?php echo $deliverables->description;?></textarea>
                    </div>
                </div>

            </div>



        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
                <!-- <button type="submit" class="btn btn-primary btn-lg">Save</button> -->
                <a href="../list" class="btn btn-link">Back</a>
            </div>
        </div>

        </form>


        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>

<script src="<?php echo BASE_PATH; ?>assets/ckeditor/ckeditor.js"></script>

<style type="text/css">
    .shadow-textarea textarea.form-control::placeholder {
    font-weight: 300;
}
.shadow-textarea textarea.form-control {
    padding-left: 0.8rem;
}
</style>

<script>

    $('select').select2();

    CKEDITOR.replace('description',{

      width: "800px",
      height: "200px"

    }); 


    function getChapterByDuration(id)
    {
        if(id != '')
        {
            $.get("/student/deliverables/getChapterByDuration/"+id, function(data, status){
           
                $("#view_chapter").html(data);
            });
        }
    }


    function getTopicByData()
     {
        var tempPR = {};
        tempPR['id_phd_duration'] = $("#id_phd_duration").val();
        tempPR['id_chapter'] = $("#id_chapter").val();
        // alert(tempPR['id_program']);

        if(tempPR['id_phd_duration'] != '' && tempPR['id_chapter'] != '')
        {

            $.ajax(
            {
               url: '/student/deliverables/getTopicByData',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_topic").html(result);
               }
            });
        }
     }


    $(document).ready(function() {
        $("#form_internship").validate({
            rules: {
                id_phd_duration: {
                    required: true
                },
                id_chapter: {
                    required: true
                },
                id_topic: {
                    required: true
                }
            },
            messages: {
                id_phd_duration: {
                    required: "<p class='error-text'>Select Phd Duration</p>",
                },
                id_chapter: {
                    required: "<p class='error-text'>Select Chapter</p>",
                },
                id_topic: {
                    required: "<p class='error-text'>Select Topic</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    $(function(){
        $( ".datepicker" ).datepicker();
      });

</script>