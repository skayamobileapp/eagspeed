<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>List Research 5 copies of hard bound submission </h3>
        </div>    
        <form id="form_main_invoice" action="" method="post">


        <?php
        if($supervisor)
        {
            ?>

            <br>

             <div class="form-container">
                <h4 class="form-group-title">Supervisor Details</h4>
                <div class='data-list'>
                    <div class='row'> 
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Supervisor Name :</dt>
                                <dd><?php echo ucwords($supervisor->full_name);?></dd>
                            </dl>
                            <dl>
                                <dt>Supervisor Email :</dt>
                                <dd><?php echo $supervisor->email ?></dd>
                            </dl>                     
                        </div>        
                        
                        <div class='col-sm-6'>                           
                            <dl>
                                <dt>Supervisor Type :</dt>
                                <dd><?php 
                                if($supervisor->type == 0)
                                {
                                    echo 'External';
                                }elseif($supervisor->type == 1)
                                {
                                    echo 'Internal';
                                } ?></dd>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>


        <?php
        }
        ?>



        <div class="form-container">
            <h4 class="form-group-title">Research 5 copies of hard bound submission Reporting</h4>          
            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>
            <div class="clearfix">
                <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                    <li role="presentation" class="active" ><a href="#invoice" class="nav-link border rounded text-center"
                            aria-controls="invoice" aria-selected="true"
                            role="tab" data-toggle="tab">Application</a>
                    </li>
                   <!--  <li role="presentation"><a href="#receipt" class="nav-link border rounded text-center"
                            aria-controls="receipt" role="tab" data-toggle="tab">Receipt</a>
                    </li> -->
                    
                </ul>
                <br>

                
                <div class="tab-content offers-tab-content">
                    <div role="tabpanel" class="tab-pane active" id="invoice">
                        <div class="mt-4">
                            <div class="custom-table" id="printInvoice">
                                <table class="table" id="list-table">
                                    <thead>
                                    <tr>
                                        <th>Sl. No</th>
                                        <th>Student</th>
                                        <th>Submitted On</th>
                                        <!-- <th>Approved / Rejected On</th> -->
                                        <th class="text-center">File</th>
                                        <th class="text-center">File 2</th>
                                        <th class="text-center">File 3</th>
                                        <th>Status</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    if (!empty($boundList))
                                    {
                                        $i=1;
                                        foreach ($boundList as $record) {
                                    ?>
                                        <tr>
                                            <td><?php echo $i ?></td>
                                            <td><?php echo $record->nric . " - " . $record->student_name ?></td>
                                            <td><?php echo date('d-m-Y',strtotime($record->created_dt_tm)) ?></td>
                                           <td class="text-center">
                                                <a href="<?php echo '/assets/images/' . $record->upload_file; ?>" target="popup" onclick="window.open(<?php echo '/assets/images/' . $record->upload_file; ?>)" title="<?php echo $record->upload_file; ?>">View</a>
                                            </td>
                                            <td class="text-center">
                                                <a href="<?php echo '/assets/images/' . $record->upload_file2; ?>" target="popup" onclick="window.open(<?php echo '/assets/images/' . $record->upload_file2; ?>)" title="<?php echo $record->upload_file2; ?>">View</a>
                                            </td>
                                            <td class="text-center">
                                                <a href="<?php echo '/assets/images/' . $record->upload_file3; ?>" target="popup" onclick="window.open(<?php echo '/assets/images/' . $record->upload_file3; ?>)" title="<?php echo $record->upload_file3; ?>">View</a>
                                            </td>
                                            <td><?php 
                                            if( $record->status == 0)
                                            {
                                              echo "Pending";
                                            }
                                            elseif( $record->status == 1)
                                            {
                                              echo "Approved";
                                            }
                                            elseif( $record->status == 2)
                                            {
                                              echo "Rejected";
                                            }
                                            ?></td>
                                            <td class="text-center">
                                                <a href="<?php echo 'view/' . $record->id; ?>" title="Add Comments">Comments</a>
                                            </td>
                                        </tr>
                                    <?php
                                    $i++;
                                        }
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div> 
                    </div>


                </div>
            </div>

        </div>
            
        </form>

    </div>

        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
</div>


</form>

<script>

    $('select').select2();

    function getStudentByProgramme(id)
    {

     $.get("/finance/mainInvoice/getStudentByProgrammeId/"+id, function(data, status){
   
        $("#student").html(data);
        // $("#view_programme_details").html(data);
        // $("#view_programme_details").show();
    });
 }

 function getStudentByStudentId(id)
 {
     $.get("/finance/mainInvoice/getStudentByStudentId/"+id, function(data, status)
     {
        $("#view_student_details").html(data);
        $("#view_student_details").show();
    });
 }



    function opendialog()
    {
        $("#id_fee_item").val('');
        $("#amount").val('');
        $("#id").val('0');                    
        $('#myModal').modal('show');

    }
    function saveData() {


        var tempPR = {};
        tempPR['id_fee_item'] = $("#id_fee_item").val();
        tempPR['amount'] = $("#amount").val();
        tempPR['id'] = $("#id").val();
            $.ajax(
            {
               url: '/finance/mainInvoice/tempadd',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view").html(result);
                var ta = $("#inv-total-amount").val();
                $("#total_amount").val(ta);
                $('#myModal').modal('hide');
               }
            });
        
    }

    function deleteTempData(id) {
         $.ajax(
            {
               url: '/finance/mainInvoice/tempDelete/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    $("#view").html(result);
               }
            });
    }


    function getTempData(id) {
        $.ajax(
            {
               url: '/finance/mainInvoice/tempedit/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(resultfromjson)
               {
                    result = JSON.parse(resultfromjson);
                    $("#dt_fund").val(result['dt_fund']);
                    $("#dt_department").val(result['dt_department']);
                    $("#id").val(id);
                    $('#myModal').modal('show');
               }
            });

    }


    $(document).ready(function() {
        $("#form_main_invoice").validate({
            rules: {
                total_amount: {
                    required: true
                },
                id_student: {
                    required: true
                },
                date_time: {
                    required: true
                }
            },
            messages: {
                total_amount: {
                    required: "Enter Total Amount",
                },
                id_student: {
                    required: "Select Student",
                },
                date_time: {
                    required: "Select Date ",
                }
            },
            errorElement: "span",
            errorDeliverables: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>