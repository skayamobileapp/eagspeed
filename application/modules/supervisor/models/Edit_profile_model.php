<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Edit_profile_model extends CI_Model
{
   function applicantList($applicantList)
    {
        $this->db->select('a.*, i.name as intake, p.code as program_code, p.name as program');
        $this->db->from('student as a');
        $this->db->join('intake as i', 'a.id_intake = i.id');
        $this->db->join('programme as p', 'a.id_program = p.id');
        if($applicantList['first_name']) {
            $likeCriteria = "(a.first_name  LIKE '%" . $applicantList['first_name'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($applicantList['last_name']) {
            $likeCriteria = "(a.last_name  LIKE '%" . $applicantList['last_name'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($applicantList['email_id']) {
            $likeCriteria = "(a.email_id  LIKE '%" . $applicantList['email_id'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($applicantList['nric']) {
            $likeCriteria = "(a.nric  LIKE '%" . $applicantList['nric'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($applicantList['id_program']) {
            $likeCriteria = "(a.id_program  LIKE '%" . $applicantList['id_program'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($applicantList['id_intake']) {
            $likeCriteria = "(a.id_intake  LIKE '%" . $applicantList['id_intake'] . "%')";
            $this->db->where($likeCriteria);
        }
        if($applicantList['applicant_status']) {
            $likeCriteria = "(a.applicant_status  LIKE '%" . $applicantList['applicant_status'] . "%')";
            $this->db->where($likeCriteria);
        }
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function courseRegistrationList($id)
    {
        $this->db->select('a.id, i.name as intake, p.name as program, std.full_name, c.name as course');
        $this->db->from('course_registration as a');
        $this->db->join('intake as i', 'a.id_intake = i.id');
        $this->db->join('programme as p', 'a.id_programme = p.id');
        $this->db->join('student as std', 'a.id_student = std.id');
        $this->db->join('course as c', 'a.id_course = c.id');
        $this->db->where('a.id_student', $id);
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();
         return $result;
    }

    function addExamDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('examination_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addProficiencyDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('english_proficiency_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addEmploymentDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('employment_status', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editProfileDetails($data, $id_student)
    {
        $this->db->where('id_student', $id_student);
        $this->db->update('profile_details', $data);
        return TRUE;
    }

    function updateStudentData($data)
    {
        $id = $data['id_student'];
        unset($data['id_student']);
        // unset($data['id_type']);
        // unset($data['passport_number']);
        // unset($data['passport_expiry_date']);
        unset($data['nationality_type']);
        // echo "<Pre>";print_r($data);exit();
        $this->db->where('id', $id);
        $this->db->update('student', $data);
        return TRUE;

    }

    function updateVisaDetails($data)
    {
        $id_student = $data['id_student'];
        unset($data['id_student']);

        $this->db->where('id_student', $id_student);
        $this->db->update('visa_details', $data);
        return TRUE;
    }

    function addVisaDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('visa_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addOtherDocuments($data)
    {
        $this->db->trans_start();
        $this->db->insert('other_documents', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getExamDetails($id)
    {
        $this->db->select('*');
        $this->db->from('examination_details');
        $this->db->where('id_student', $id);
        $query = $this->db->get();
         $result = $query->result();
        return $result;
    }

    function getProficiencyDetails($id)
    {
        $this->db->select('*');
        $this->db->from('english_proficiency_details');
        $this->db->where('id_student', $id);
        $query = $this->db->get();
         $result = $query->result();
        return $result;
    }

    function salutationListByStatus($status)
    {
        $this->db->select('a.*');
        $this->db->from('salutation_setup as a');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getSalutation($id)
    {
        $this->db->select('*');
        $this->db->from('salutation_setup');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getEmploymentDetails($id)
    {
        $this->db->select('*');
        $this->db->from('employment_status');
        $this->db->where('id_student', $id);
        $query = $this->db->get();
         $result = $query->result();
        return $result;
    }

    function getProfileDetails($id)
    {
        $this->db->select('*');
        $this->db->from('profile_details');
        $this->db->where('id_student', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getVisaDetails($id)
    {
        $this->db->select('*');
        $this->db->from('visa_details');
        $this->db->where('id_student', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function getOtherDocuments($id)
    {
        $this->db->select('*');
        $this->db->from('other_documents');
        $this->db->where('id_student', $id);
        // $this->db->limit('0,1');
        $query = $this->db->get();
         $result = $query->result();
        return $result;
    }

    function intakeList()
    {
        $this->db->select('*');
        $this->db->from('intake');
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function nationalityList()
    {
        $this->db->select('*');
        $this->db->from('nationality');
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function programList()
    {
        $this->db->select('*');
        $this->db->from('programme');
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function addNewStudent()
    {
        $query = $this->db->get('applicant_table');
        foreach ($query->result() as $row) {
              $this->db->insert('student',$row);
        }
    }

    function getStudentData($id)
    {
        $this->db->select('a.*, in.name as intake, p.name as program, qs.name as qualification_name');
        $this->db->from('student as a');
        $this->db->join('intake as in', 'a.id_intake = in.id','left');
        $this->db->join('programme as p', 'a.id_program = p.id','left');
        $this->db->join('education_level as qs', 'a.id_degree_type = qs.id','left');
        $this->db->where('a.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function countryList()
    {
        $this->db->select('*');
        $this->db->from('country');
        $this->db->where('status', '1');
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function stateList()
    {
        $this->db->select('*');
        $this->db->from('state');
        $this->db->where('status', '1');
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function getStudentDetails($id)
    {
        $this->db->select('*');
        $this->db->from('student');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function editApplicantDetails($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('applicant_table', $data);
        return TRUE;
    }
    
    function deleteExamDetails($id)
    {
        $this->db->where('id', $id);
       $this->db->delete('examination_details');
         return TRUE;
    }

    function deleteProficiencyDetails($id)
    {
        $this->db->where('id', $id);
       $this->db->delete('english_proficiency_details');
         return TRUE;
    }

    function deleteEmploymentDetails($id)
    {
        $this->db->where('id', $id);
       $this->db->delete('employment_status');
         return TRUE;
    }

    function deleteOtherDocument($id)
    {
        $this->db->where('id', $id);
       $this->db->delete('other_documents');
         return TRUE;
    }

    function checkDuplicateStudent($data,$id)
    {
        $this->db->select('id, email_id, full_name');
        $this->db->from('student');
        $this->db->where('email_id', $data['email_id']);
        $this->db->or_where('phone', $data['phone']);
        $this->db->or_where('nric', $data['nric']);
        $this->db->where('id !=', $id);
        $query = $this->db->get();
        return $query->row();
    }


    function raceListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('race_setup');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }


    function religionListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('religion_setup');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function deleteVisaDetails($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('visa_details');
         return TRUE;
    }
}