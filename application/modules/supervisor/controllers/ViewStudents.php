<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ViewStudents extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('view_model');
        $this->isSupervisorLoggedIn();
    }

    function view()
    {       
        $id_supervisor = $this->session->id_supervisor;
        
        $data['supervisor'] = $this->view_model->getSupervisor($id_supervisor);
        $data['studentList'] = $this->view_model->getStudentListByIdSupervisor($id_supervisor);
        // echo "<Pre>";print_r($data['studentList']);exit();

        $this->global['pageTitle'] = 'Supervisor Portal : View Student List';
        $this->loadViews("profile/view_student", $this->global, $data, NULL);
    }
}