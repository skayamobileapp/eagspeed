<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Bulk extends BaseController
{
    public function __construct()
    {
        // error_reporting(0);
        parent::__construct();
        $this->load->model('sponser_model');
        $this->isLoggedIn();
    }

    function list()
    {

       
        $data['programmeList'] = $this->sponser_model->programListByStatus("Approved and Published");
        $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));

        $formData['duration'] = $this->security->xss_clean($this->input->post('duration'));
        $formData['start_date'] = $this->security->xss_clean($this->input->post('start_date'));
        $formData['end_date'] = $this->security->xss_clean($this->input->post('end_date'));

        $data['searchParam'] = $formData;

            $data['sponserList'] = $this->sponser_model->sponserList();




        if($_POST) {


            $feeStructureDetails =  $this->sponser_model->getCourseAmount($formData['id_programme']);
            $data['amount'] = $feeStructureDetails->amount;
            $data['feestructureId'] = $feeStructureDetails->id;


             $certificate_name = $_FILES['file']['name'];
                    $certificate_size = $_FILES['file']['size'];
                    $certificate_tmp =$_FILES['file']['tmp_name'];
                    
                    // echo "<Pre>"; print_r($certificate_tmp);exit();

                    $certificate_ext=explode('.',$certificate_name);
                    $certificate_ext=end($certificate_ext);
                    $certificate_ext=strtolower($certificate_ext);


                    $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'Image File');

                    $image_file = $this->uploadFile($certificate_name,$certificate_tmp,'Image File');

                    chmod("/var/www/html/speed/assets/images/".$image_file,0777);

                    $file = fopen("/var/www/html/speed/assets/images/".$image_file, "r");
                   $studentsArray = array();

            while(! feof($file))
                  {
                 $atnarray = fgetcsv($file);

                     array_push($studentsArray,$atnarray);

                        }
                } else {
                    $studentsArray = array();
                }

            $data['studentList'] = $studentsArray;

            $this->global['pageTitle'] = 'Campus Management System : Sponsor List';
            $this->loadViews("bulk/list", $this->global, $data, NULL);
        
    }
    
    function add()
    {
        
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $address = $this->security->xss_clean($this->input->post('address'));
                $mobile_number = $this->security->xss_clean($this->input->post('mobile_number'));
                $id_state = $this->security->xss_clean($this->input->post('id_state'));
                $id_country = $this->security->xss_clean($this->input->post('id_country'));
                $zip_code = $this->security->xss_clean($this->input->post('zip_code'));
                $address2 = $this->security->xss_clean($this->input->post('address2'));
                $location = $this->security->xss_clean($this->input->post('location'));
                $fax = $this->security->xss_clean($this->input->post('fax'));
                $email = $this->security->xss_clean($this->input->post('email'));
                $status = $this->security->xss_clean($this->input->post('status'));


                $generated_number = $this->sponser_model->generateSponserCode();


            
                $data = array(
                    'name' => $name,
                    'code' => $generated_number,
                    'address' => $address,
                    'mobile_number' => $mobile_number,
                    'id_state' => $id_state,
                    'id_country' => $id_country,
                    'zip_code' => $zip_code,
                    'address2' => $address2,
                    'location' => $location,
                    'fax' => $fax,
                    'email' => $email,
                    'status' => $status
                );
                // echo "<Pre>"; print_r($data);exit;
                $result = $this->sponser_model->addNewSponser($data);
                redirect('/sponser/sponser/list/');
            }

            $data['stateList'] = $this->sponser_model->stateList();
            $data['countryList'] = $this->sponser_model->countryList();
            $data['feeList'] = $this->sponser_model->feeListByStatus('1');
            // $data['calculationModeList'] = $this->sponser_model->calculationModeListByStatus('1');
            $data['frequencyModeList'] = $this->sponser_model->frequencyModeListByStatus('1');



            $this->global['pageTitle'] = 'Campus Management System : Add Sponsor';
            $this->loadViews("sponser_view/add", $this->global, $data, NULL);
       
    }


    function update()
    {
        
         
            if($this->input->post())
            {



                $sponsorUniqueArray = array_unique($_POST['sponsor']);







                $idprogramme = $_POST['id_programme'];

                for($s=0;$s<count($sponsorUniqueArray);$s++)
                {


                    if($sponsorUniqueArray[$s]!=0) {

                      $mainsponsorInvId =   $this->generateMainSponsorInvoice($sponsorUniqueArray[$s]);
                    }
                    for($i=1;$i<=count($_POST['name']);$i++) { 

                        if($_POST['sponsor'][$i]==$sponsorUniqueArray[$s]) {



                        $name = $_POST['name'][$i];
                        $email = $_POST['email'][$i];
                        $nric = $_POST['nric'][$i];
                        $phone = $_POST['phone'][$i];
                        $course_amount = $_POST['course_amount'][$i];
                        $discount = $_POST['discount'][$i];
                        $final_amount = $_POST['final_amount'][$i];
                        $fee_structure = $_POST['fee_structure'][$i];
                        $start_date = date('Y-m-d',strtotime($_POST['start_date'][$i]));
                        $end_date = date('Y-m-d',strtotime($_POST['end_date'][$i]));
                        $sponsor = $_POST['sponsor'][$i];
                        if($sponsor==0) {
                            $this->createStudent($name,$email,$nric,$idprogramme,$start_date,$end_date,$course_amount,$discount,$final_amount,$fee_structure);
                        }
                        else {
                            $this->createStudentSponsor($name,$email,$nric,$idprogramme,$start_date,$end_date,$course_amount,$discount,$final_amount,$fee_structure,$sponsorUniqueArray[$s],$mainsponsorInvId);

                        }
                       }
                    }
                }
                
                redirect('/sponser/sponser/list');
            }

            
       
    }

    function createStudentSponsor($full_name,$user_email,$nric,$idprogramme,$start_date,$end_date,$courseAmount,$discountAmount,$finalAmount,$feeStructureId,$sponsorId,$mainsponsorInvId) {
          $passworddec = 'Abcde12345#';
            $student = array(
                'full_name' =>$full_name,
                'email_id' =>$user_email,
                'nric' =>$nric,
                'password' =>$passworddec,
            );
            $studentId = $this->sponser_model->insertStudent($student);



                $functionName = 'core_user_create_users';
                $rand = rand(00000000,999999999);

                $user1 = new stdClass();
                $user1->username = $nric;
                $user1->password = 'Abc12345678*';
                $user1->firstname = $full_name;
                $user1->lastname = $full_name;
                $user1->email = $user_email;


                $users = array($user1);
                $params = array('users' => $users);

                // /// REST CALL
                // $restformat = "json";
                // $serverurl = 'https://lms.myeduskills.com/webservice/rest/server.php?wstoken='.TOKEN.'&wsfunction=' . $functionName. '&moodlewsrestformat=' . $restformat;
                // require_once ('curl.php');
                // $curl = new curl();


                // $resp = $curl->post($serverurl, $params);




                // $responseArray = json_decode($resp);

                // $studentdata = array();
                // $studentdata['moodle_id'] = $responseArray[0]->id;
                // $this->sponser_model->editStudent($studentdata, $result->id);


                     $invoiceDetails = array(
                                'id_sponsor_main_invoice' => $mainsponsorInvId,
                                'id_fee_structure' => $feeStructureId,
                                'id_sponsor'=>$sponsorId,
                                'amount' => $courseAmount - $discountAmount,
                                'quantity' => 1,
                                'id_programme' => $idprogramme,
                                'description' => 'Course Registration Fee',
                                'status' => 1,
                                'created_by' => 0,
                                'id_student'=>$studentId
                                );



                     $data_student_has_programme =array(
                                'id_sponsor_main_invoice' => $mainsponsorInvId,
                                'id_programme' => $idprogramme,
                                'id_student' => $studentId,
                                'start_date' => $start_date,
                                'end_date' => $end_date,
                                'marks_status'=>'Pending',
                            );




                        $this->sponser_model->addNewSponsorMainInvoiceDetails($invoiceDetails);

                        $this->sponser_model->addNewStudentHasProgramme($data_student_has_programme);

                        $mainsponsorDetails = $this->sponser_model->getMainSponsorData($mainsponsorInvId);
                        $initialAmount = $mainsponsorDetails[0]->total_amount;
                        $finalamountsponsor = $initialAmount + ($courseAmount - $discountAmount);

                         $finalInvoice = array('total_amount'=> $finalamountsponsor);
                         $this->sponser_model->editSponsorMainInvoice($finalInvoice,$mainsponsorInvId);
                        print_R($mainsponsorDetails);exit;

    }


    function createStudent($full_name,$user_email,$nric,$idprogramme,$start_date,$end_date,$courseAmount,$discountAmount,$finalAmount,$feeStructureId) {


            $passworddec = 'Abcde12345#';
            $student = array(
                'full_name' =>$full_name,
                'email_id' =>$user_email,
                'nric' =>$nric,
                'password' =>$passworddec,
            );
            $studentId = $this->sponser_model->insertStudent($student);



                $functionName = 'core_user_create_users';
                $rand = rand(00000000,999999999);

                $user1 = new stdClass();
                $user1->username = $nric;
                $user1->password = 'Abc12345678*';
                $user1->firstname = $full_name;
                $user1->lastname = $full_name;
                $user1->email = $user_email;


                $users = array($user1);
                $params = array('users' => $users);

                // /// REST CALL
                // $restformat = "json";
                // $serverurl = 'https://lms.myeduskills.com/webservice/rest/server.php?wstoken='.TOKEN.'&wsfunction=' . $functionName. '&moodlewsrestformat=' . $restformat;
                // require_once ('curl.php');
                // $curl = new curl();


                // $resp = $curl->post($serverurl, $params);




                // $responseArray = json_decode($resp);

                // $studentdata = array();
                // $studentdata['moodle_id'] = $responseArray[0]->id;
                // $this->sponser_model->editStudent($studentdata, $result->id);

                $invoice_number = $this->generateMainInvoiceNumber();

                        $invoice['invoice_number'] = $invoice_number;
                        $invoice['type'] = 'Student';
                        $invoice['remarks'] = 'Invoice';
                        $invoice['id_student'] = $studentId;
                        $invoice['currency'] = '1';
                        $invoice['total_amount'] = $courseAmount - $discountAmount;
                        $invoice['original_amount'] = $courseAmount;
                        $invoice['discount_amount'] = $discountAmount;
                        $invoice['discount_code'] = 0;
                        $invoice['balance_amount'] = '0';
                        $invoice['paid_amount'] = $courseAmount - $discountAmount;
                        $invoice['amount_before_gst'] = '0';
                        $invoice['gst_amount'] = '0';
                        $invoice['gst_percentage'] = 0;
                        $invoice['status'] = 'ACTIVE';
                        $inserted_idinvoice = $this->sponser_model->addNewMainInvoice($invoice);



                     $invoiceDetails = array(
                                'id_main_invoice' => $inserted_idinvoice,
                                'id_fee_structure' => $feeStructureId,
                                'amount' => $courseAmount - $discountAmount,
                                'quantity' => 1,
                                'id_programme' => $idprogramme,
                                'description' => 'Course Registration Fee',
                                'status' => 1,
                                'created_by' => 0
                                );



                     $data_student_has_programme =array(
                                'id_main_invoice' => $inserted_idinvoice,
                                'id_programme' => $idprogramme,
                                'id_student' => $studentId,
                                'start_date' => $start_date,
                                'end_date' => $end_date,
                                'marks_status'=>'Pending',
                            );




                        $this->sponser_model->addNewMainInvoiceDetails($invoiceDetails);

                        $this->sponser_model->addNewStudentHasProgramme($data_student_has_programme);

                // // Reciept master

                //  $receipt_number = $this->sponser_model->generateReceiptNumber();

                //     $receipr_data = array(
                //         'receipt_date' =>date('Y-m-d'),
                //         'id_student' => $studentId,
                //         'receipt_number' => $receipt_number,
                //         'type' => 'Student',
                //         'currency' => 1,
                //         'receipt_amount' => $courseAmount - $discountAmount,
                //         'remarks' => 'Bulk Upload Data Receipt',
                //         'status' => '1'
                //     );

                //     $receiptId = $this->sponser_model->addNewReceipt($receipr_data);


                // $detailsData = array(
                // 'id_receipt' => $receiptId,
                // 'id_main_invoice' => $inserted_idinvoice,
                // 'invoice_amount' => $courseAmount - $discountAmount,
                // 'paid_amount' => $courseAmount - $discountAmount,                        
                // 'status' => '1',
                // 'created_by' => 1
                // );
                
                // $this->sponser_model->addNewReceiptDetails($detailsData);

                echo "Data displayed";
                exit;
                               

    }



    function course($id)
    {
          
          $programmeDetails = $this->sponser_model->getProgrammeDetails($id);
          $totalDuration = $programmeDetails[0]->max_duration.' '.$programmeDetails[0]->duration_type;

            echo  $totalDuration;
            return;
    }

    function getenddate($startdate,$programmeid)
    {

                  $programmeDetails = $this->sponser_model->getProgrammeDetails($programmeid);
          $totalDuration = $programmeDetails[0]->max_duration.' '.$programmeDetails[0]->duration_type;



        $start_date = date('Y-m-d',strtotime($startdate));  
        $date = strtotime($start_date);
        $date = strtotime($totalDuration, $date);
        echo date('d-m-Y', $date);

        exit;

    }

     function insertintomoodle($idstudent,$idprogramme){


            //get moodleid fro student
        $studentDetails = $this->dashboard_model->getStudentDetailsById($idstudent);

        $studentMoodleId = $studentDetails->moodle_id;



        $programmeDetails = $this->dashboard_model->getProgrammeDetailsById($idprogramme);

        $programmeMoodleId = $programmeDetails->moodle_id;

           // get moodle id for course


        $functionName = 'enrol_manual_enrol_users';
                $rand = rand(00000000,999999999);

                $user1 = new stdClass();
                $user1->roleid = 5;
                $user1->userid = $studentMoodleId;
                $user1->courseid = $programmeMoodleId;


                $users = array($user1);
                $params = array('enrolments' => $users);

                
                $restformat = "json";
                $serverurl = "https://lms.myeduskills.com/webservice/rest/server.php?wstoken=".LOGIN.'&wsfunction=' . $functionName. '&moodlewsrestformat=' . $restformat;
                require_once ('curl.php');
                $curl = new curl();


                $resp = $curl->post($serverurl, $params);
              
            if($resp=="null") {
                    $this->dashboard_model->updatemoodlestatus($idstudent,$idprogramme);
            }

    }


     function generateMainInvoiceNumber()
    {
        $Year = date('Y');
            $this->db->select('j.*');
            $this->db->from('main_invoice as j');
            $this->db->order_by("id", "desc");
            $query = $this->db->get();
            $result = $query->num_rows();

     
            $count= $result + 1;
           $jrnumber = $number = "INV" .(sprintf("%'06d", $count)). "/" . $Year;
           return $jrnumber;        
    }


     function generateSponsorMainInvoiceNumber()
    {
        $Year = date('Y');
            $this->db->select('j.*');
            $this->db->from('main_invoice_sponser as j');
            $this->db->order_by("id", "desc");
            $query = $this->db->get();
            $result = $query->num_rows();

     
            $count= $result + 1;
           $jrnumber = $number = "SP-INV" .(sprintf("%'06d", $count)). "/" . $Year;
           return $jrnumber;        
    }



  function  generateMainSponsorInvoice($idsponsor){


      $invoice['invoice_number'] = $this->generateSponsorMainInvoiceNumber();
                        $invoice['type'] = 'Sponsor';
                        $invoice['remarks'] = 'Sponsor Invoice';
                        $inserted_idinvoice = $this->sponser_model->addNewSponsorMainInvoice($invoice);

                        return $inserted_idinvoice;
                        exit;
  }


}
