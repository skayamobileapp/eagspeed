<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Sponser extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('sponser_model');
        $this->isLoggedIn();
    }

    function list()
    {
        
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;

            $data['sponserList'] = $this->sponser_model->sponserListSearch($name);
            //echo "<Pre>"; print_r($data);exit;
            $this->global['pageTitle'] = 'Campus Management System : Sponsor List';
            $this->loadViews("sponser_view/list", $this->global, $data, NULL);
        
    }
    
    function add()
    {
        
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $address = $this->security->xss_clean($this->input->post('address'));
                $mobile_number = $this->security->xss_clean($this->input->post('mobile_number'));
                $id_state = $this->security->xss_clean($this->input->post('id_state'));
                $id_country = $this->security->xss_clean($this->input->post('id_country'));
                $zip_code = $this->security->xss_clean($this->input->post('zip_code'));
                $address2 = $this->security->xss_clean($this->input->post('address2'));
                $location = $this->security->xss_clean($this->input->post('location'));
                $fax = $this->security->xss_clean($this->input->post('fax'));
                $email = $this->security->xss_clean($this->input->post('email'));
                $status = $this->security->xss_clean($this->input->post('status'));


                $generated_number = $this->sponser_model->generateSponserCode();


            
                $data = array(
                    'name' => $name,
                    'code' => $generated_number,
                    'address' => $address,
                    'mobile_number' => $mobile_number,
                    'id_state' => $id_state,
                    'id_country' => $id_country,
                    'zip_code' => $zip_code,
                    'address2' => $address2,
                    'location' => $location,
                    'fax' => $fax,
                    'email' => $email,
                    'status' => $status
                );
                // echo "<Pre>"; print_r($data);exit;
                $result = $this->sponser_model->addNewSponser($data);
                redirect('/sponser/sponser/list/');
            }

            $data['stateList'] = $this->sponser_model->stateList();
            $data['countryList'] = $this->sponser_model->countryList();
            $data['feeList'] = $this->sponser_model->feeListByStatus('1');
            // $data['calculationModeList'] = $this->sponser_model->calculationModeListByStatus('1');
            $data['frequencyModeList'] = $this->sponser_model->frequencyModeListByStatus('1');



            $this->global['pageTitle'] = 'Campus Management System : Add Sponsor';
            $this->loadViews("sponser_view/add", $this->global, $data, NULL);
       
    }


    function edit($id = NULL)
    {
        
            if ($id == null)
            {
                redirect('/sponser/sponser/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $address = $this->security->xss_clean($this->input->post('address'));
                $mobile_number = $this->security->xss_clean($this->input->post('mobile_number'));
                $id_state = $this->security->xss_clean($this->input->post('id_state'));
                $id_country = $this->security->xss_clean($this->input->post('id_country'));
                $zip_code = $this->security->xss_clean($this->input->post('zip_code'));
                $address2 = $this->security->xss_clean($this->input->post('address2'));
                $location = $this->security->xss_clean($this->input->post('location'));
                $fax = $this->security->xss_clean($this->input->post('fax'));
                $email = $this->security->xss_clean($this->input->post('email'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'address' => $address,
                    'mobile_number' => $mobile_number,
                    'id_state' => $id_state,
                    'id_country' => $id_country,
                    'zip_code' => $zip_code,
                    'address2' => $address2,
                    'location' => $location,
                    'fax' => $fax,
                    'email' => $email,
                    'status' => $status
                );
                
                $result = $this->sponser_model->editSponser($data,$id);
                redirect('/sponser/sponser/list');
            }

            $data['stateList'] = $this->sponser_model->stateList();
            $data['countryList'] = $this->sponser_model->countryList();
            $data['sponserDetails'] = $this->sponser_model->getSponser($id);
                // echo "<Pre>"; print_r($data);exit;

            
            $this->global['pageTitle'] = 'Campus Management System : Edit Sponsor';
            $this->loadViews("sponser_view/edit", $this->global, $data, NULL);
       
    }



    function getStateByCountry($id_country)
    {
            $results = $this->sponser_model->getStateByCountryId($id_country);

            
                 
            $table="  


            <script type='text/javascript'>
                     $('select').select2();
                 </script> 
                
         ";

            $table.="
            <select name='id_state' id='id_state' class='form-control'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;
    }

    function saveFeeDetailData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $inserted_id = $this->sponser_model->saveFeeDetailData($tempData);
        echo "success";exit();
    }

    function saveCoordinatorDetailData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $inserted_id = $this->sponser_model->saveCoordinatorDetailData($tempData);
        echo "success";exit();
    }

    function deleteFeeDetailData($id)
    {
        $deleted = $this->sponser_model->deleteFeeDetailData($id);
        echo 'success';exit;
    }

    function deleteCoordinatorDetailData($id)
    {
        $deleted = $this->sponser_model->deleteCoordinatorDetailData($id);
        echo 'success';exit;
    }
}
