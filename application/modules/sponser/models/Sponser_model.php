<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Sponser_model extends CI_Model
{
    function sponserList()
    {
        $this->db->select('sp.*, s.name as state, c.name as country');
        $this->db->from('sponser as sp');
        $this->db->join('state as s', 'sp.id_state = s.id');
        $this->db->join('country as c', 'sp.id_country = c.id');
        $this->db->order_by("sp.id", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         //echo "<Pre>"; print_r($result);exit;
         return $result;
    }

      function insertStudent($student) {
        $this->db->insert('student', $student);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }


      function addNewStudentHasProgramme($data)
    {
        $this->db->trans_start();
        $this->db->insert('student_has_programme', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }
    

      function getCourseAmount($id) {
         $this->db->select('f.*');
        $this->db->from('fee_structure_master as f');
        $this->db->where('f.id_programme', $id);

         $query = $this->db->get();
         $result = $query->row();  
         return $result;
     }

  function addNewReceipt($data)
    {
        $this->db->trans_start();
        $this->db->insert('receipt', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }


 function addNewReceiptDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('receipt_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }
    

    function generateReceiptNumber()
    {
        $year = date('y');
        $Year = date('Y');
        
        $this->db->select('j.*');
        $this->db->from('receipt as j');
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        $result = $query->num_rows();

 
        $count= $result + 1;
       $jrnumber = $number = "REC" .(sprintf("%'06d", $count)). "/" . $Year;
       return $jrnumber;
    }



     function getProgrammeDetailsById($id)
    {
        $this->db->select('p.*');
        $this->db->from('programme as p');
        $this->db->where("p.id",$id);
        $query = $this->db->get();
        return $query->row();    
    }


     function addNewMainInvoiceDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('main_invoice_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addNewSponsorMainInvoiceDetails($data){
        $this->db->trans_start();
        $this->db->insert('sponsor_invoice_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }


     function addNewMainInvoice($data)
    {
        $this->db->trans_start();
        $this->db->insert('main_invoice', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }


    function addNewSponsorMainInvoice($data){
         $this->db->trans_start();
        $this->db->insert('main_invoice_sponser', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }


       function editSponsorMainInvoice($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('main_invoice_sponser', $data);
        return TRUE;
    }



     function getMainSponsorData($id)
    {
        $this->db->select('*');
        $this->db->from('main_invoice_sponser');
        $this->db->where('id', $id);
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }



     function editStudent($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('student', $data);
        return TRUE;
    }





     function programListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }


     function getProgrammeDetails($id)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('id', $id);
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }


    function sponserListSearch($search)
    {
        $this->db->select('sp.*, s.name as state, c.name as country');
        $this->db->from('sponser as sp');
        $this->db->join('state as s', 'sp.id_state = s.id');
        $this->db->join('country as c', 'sp.id_country = c.id');
        if (!empty($search))
        {
            $likeCriteria = "(sp.name  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("sp.id", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         //echo "<Pre>"; print_r($result);exit;
         return $result;
    }

    function getSponser($id)
    {
        $this->db->select('*');
        $this->db->from('sponser');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewSponser($data)
    {
        $this->db->trans_start();
        $this->db->insert('sponser', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editSponser($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('sponser', $data);
        return TRUE;
    }

    function stateList()
    {
        $this->db->select('*');
        $this->db->from('state');
        $this->db->where('status', '1');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function countryList()
    {
        $this->db->select('c.id, c.name');
        $this->db->from('country as c');
        $this->db->where('c.status', '1');
        $this->db->order_by("c.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getStateByCountryId($id_country)
    {
        $this->db->select('*');
        $this->db->from('state');
        $this->db->where('id_country', $id_country);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function generateSponserCode()
    {
        $year = date('y');
        $Year = date('Y');
            $this->db->select('*');
            $this->db->from('sponser');
            $this->db->order_by("id", "desc");
            $query = $this->db->get();
            $result = $query->num_rows();

     
            $count= $result + 1;

           $generated_number = "SP" .(sprintf("%'06d", $count)). "/" . $Year;
           // echo "<Pre>";print_r($generated_number);exit();
           return $generated_number;
    }

    function feeListByStatus($status)
    {
        $this->db->select('c.*');
        $this->db->from('fee_setup as c');
        $this->db->where('c.status', $status);
        $this->db->order_by("c.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function calculationModeListByStatus($status)
    {
        $this->db->select('c.*');
        $this->db->from('amount_calculation_type as c');
        $this->db->where('c.status', $status);
        $this->db->order_by("c.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function frequencyModeListByStatus($status)
    {
        $this->db->select('c.*');
        $this->db->from('frequency_mode as c');
        $this->db->where('c.status', $status);
        $this->db->order_by("c.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getSponserFeeInfoDetails($id)
    {
        $this->db->select('sp.*, fs.name as fee_name, fs.code as fee_code, fm.name as frequency_name, fm.code as frequency_code');
        $this->db->from('sponser_fee_info_details as sp');
        $this->db->join('fee_setup as fs', 'sp.id_fee_item = fs.id');
        $this->db->join('frequency_mode as fm', 'sp.id_frequency_mode = fm.id');
        $this->db->where('sp.id_sponser', $id);
         $query = $this->db->get();
         $result = $query->result();  
         //echo "<Pre>"; print_r($result);exit;
         return $result;
    }

    function getSponserCoordinatorDetails($id)
    {
        $this->db->select('sp.*');
        $this->db->from('sponser_coordinator_details as sp');
        $this->db->where('sp.id_sponser', $id);
         $query = $this->db->get();
         $result = $query->result();  
         //echo "<Pre>"; print_r($result);exit;
         return $result;
    }

    function saveFeeDetailData($data)
    {
        $this->db->trans_start();
        $this->db->insert('sponser_fee_info_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function saveCoordinatorDetailData($data)
    {
        $this->db->trans_start();
        $this->db->insert('sponser_coordinator_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function deleteFeeDetailData($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('sponser_fee_info_details');
        return TRUE;
    }

    function deleteCoordinatorDetailData($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('sponser_coordinator_details');
        return TRUE;
    }



}

