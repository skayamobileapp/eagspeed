<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Payment_type_model extends CI_Model
{
    function paymentTypeList()
    {
        $this->db->select('p.*');
        $this->db->from('payment_type as p');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getPaymentType($id)
    {
        $this->db->select('p.*');
        $this->db->from('payment_type as p');
        $this->db->where('p.id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewPaymentType($data)
    {
        $this->db->trans_start();
        $this->db->insert('payment_type', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editPaymentType($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('payment_type', $data);
        return TRUE;
    }
}

