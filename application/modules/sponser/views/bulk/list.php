<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List Course</h3>
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm" enctype="multipart/form-data">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">


                <div class="row">

                


                 <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Course</label>
                      <div class="col-sm-8">
                        <select name="id_programme" id="id_programme" class="form-control selitemIcon" onchange="getProgrammeDetails(this.value)">
                            <option value="">Select</option>
                            <?php
                            if (!empty($programmeList))
                            {
                                foreach ($programmeList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                              <?php
                              if($record->id == $searchParam['id_programme'])
                              {
                                echo "selected";
                              }
                              ?>
                              >
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                      </div>
                    </div>

                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-6">
                     <div class="form-group">
                      <label class="col-sm-4 control-label">Duration</label>
                      <div class="col-sm-8">
                        <input type='text' name="duration" id="duration" class="form-control" value="<?php echo $searchParam['duration'];?>">
                        </div>
                    </div>
                  </div>
                </div>

                 <div class="row">
                  <div class="col-sm-6">
                     <div class="form-group">
                      <label class="col-sm-4 control-label">Start Date</label>
                      <div class="col-sm-8">
                        <input type='text' name="start_date" id="start_date" class="form-control datepicker" onchange="getlastDate(this.value)" autocomplete="off" value="<?php echo $searchParam['start_date'];?>">
                        </div>
                    </div>
                  </div>
                </div>

                 <div class="row">
                  <div class="col-sm-6">
                     <div class="form-group">
                      <label class="col-sm-4 control-label">End Date</label>
                      <div class="col-sm-8">
                        <input type='text' name="end_date" id="end_date" class="form-control datepicker"  autocomplete="off" value="<?php echo $searchParam['end_date'];?>">
                        </div>
                    </div>
                  </div>
                </div>


                <div class="row">

                   <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Upload File</label>
                      <div class="col-sm-8">
                        <input type='file' name="file" id="file" class="form-control selitemIcon">
                        </div>
                    </div>

                  </div>


                </div>




              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Upload</button>
                <a href='list' class="btn btn-link">Clear All Fields</a>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>

  <?php if(!empty($studentList)) { ?> 
        <form action="/sponser/Bulk/update" method="post" id="searchForm">

    <div class="custom-table">
      <table class="table" >
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Student Name</th>
            <th>Student Email</th>
            <th>Student NRIC</th>
            <th>Student Phone</th>
            <th>Start Date</th>
            <th>End Date</th>
            <th>Course Amount</th>
            <th>Discount</th>
            <th>Final Amount</th>
            <th>Payee<input type='hidden' name='id_programme' id='id_programme' value="<?php echo $searchParam['id_programme'];?>" /></th>
            <th>Action</th>



            
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($studentList)) {

            for ($i=1;$i<count($studentList);$i++) {
                 if($studentList[$i][0]!='') {

          ?>
              <tr id="student<?php echo $i;?>">
                <td><?php echo $i ?></td>
                <td><input type='text' class="form-control" name='name[<?php echo $i;?>]' value="<?php echo $studentList[$i][0]?>" /></td>
                <td><input type='text' class="form-control" name='email[<?php echo $i;?>]' value="<?php echo $studentList[$i][1]?>" /></td>
                <td><input type='text' class="form-control" name='nric[<?php echo $i;?>]' value="<?php echo $studentList[$i][2]?>" /></td>
                  <td><input type='text' class="form-control" name='phone[<?php echo $i;?>]' value="<?php echo $studentList[$i][3]?>" /></td>

                  <td><input type='text' class="form-control datepicker" name='start_date[<?php echo $i;?>]' value="<?php echo $searchParam['start_date'];?>" /></td>

                  <td><input type='text' class="form-control datepicker" name='end_date[<?php echo $i;?>]' value="<?php echo $searchParam['end_date'];?>" />
                  <input type='text' class="form-control" name='fee_structure[<?php echo $i;?>]' value="<?php echo $feestructureId;?>" style="display:none;"/></td>


                  <td><input type='text' class="form-control" name='course_amount[<?php echo $i;?>]' value="<?php echo $amount;?>" style="width:75px;"/></td>
                  <td><input type='text' class="form-control" name='discount[<?php echo $i;?>]' value="0" style="width:75px;"/></td>
                  <td><input type='text' class="form-control" name='final_amount[<?php echo $i;?>]' value="<?php echo $amount;?>" style="width:75px;"/></td>

                <td>
                  <select class="form-container" name='sponsor[<?php echo $i;?>]'>
                    <?php for($j=0;$j<count($sponserList);$j++) { ?> 
                      <option value="<?php echo $sponserList[$j]->id;?>"><?php echo $sponserList[$j]->name;?></option>


                    <?php } ?> 
                    <option value='0'>Self</option>
                  </select>
                </td>
               
                  <td><a onclick="deleterow(<?php echo $i;?>)"><img src='/assets/img/close-line-icon.svg' style="width:15px;" /></a></td>
              </tr>
          <?php
        } 
            }
          }
          ?>
        </tbody>
      </table>
       <div class="button-block clearfix">
        <div class="bttn-group">
            <button type="submit" class="btn btn-primary btn-lg" name="btn_submit" value="marks">Save</button>
        </div>
    </div>

    </div>
  </form>
  </div>
<?php } ?> 

                </div>

  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script>

  $('select').select2();

  $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
        dateFormat: 'dd-mm-yy'
    });
  } );



  function clearSearchForm()
  {
    window.location.reload();
  }

  function deleterow(id) {

    var cnf = confirm("Do you really want to delete?");
    if(cnf==true) {
     $("#student"+id).remove();
    }
  }

  function getProgrammeDetails(id) {
     $.ajax(
            {
               url: '/sponser/Bulk/course/'+id,
               type: 'GET',
               error: function()
               {
                alert('Please select Course');
               },
               success: function(result)
               {
                 console.log(result);
                 $("#duration").val(result);
               }
            });
  }


  function getlastDate(startdate) {
    var id_programme  = $("#id_programme").val();
     $.ajax(
            {
               url: '/sponser/Bulk/getenddate/'+startdate+'/'+id_programme,
               type: 'GET',
               error: function()
               {
                alert('Please select Course');
               },
               success: function(result)
               {
                 console.log(result);
                 $("#end_date").val(result);
               }
            });
  }

</script>