<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Sponsor Rececipt</h3>
        </div>
        <form id="form_receipt_paid_details" action="" method="post">

            <div class="row">

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Sponsor Receipt *</label>
                        <select name="id_sponser_receipt" id="id_sponser_receipt" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($receiptList))
                            {
                                foreach ($receiptList as $record)
                                {?>
                                    <option value="<?php echo $record->id;?>"
                                    ><?php echo $record->receipt_number;?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Payment Type *</label>
                        <select name="id_payment_type" id="id_payment_type" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($paymentTypeList))
                            {
                                foreach ($paymentTypeList as $record)
                                {?>
                                    <option value="<?php echo $record->id;?>"
                                    ><?php echo $record->name;?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Paid Amount *</label>
                        <input type="text" class="form-control" id="paid_amount" name="paid_amount">
                    </div>
                </div>
            </div>

            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="list" class="btn btn-link">Cancel</a>
                </div>
            </div>
            
        </form>
    </div>

        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div> -->
</div>
<script>
    $(document).ready(function() {
        $("#form_receipt_paid_details").validate({
            rules: {
                id_sponser_receipt: {
                    required: true
                },
                id_payment_type: {
                    required: true
                },
                paid_amount: {
                    required: true
                }
            },
            messages: {
                id_sponser_receipt: {
                    required: "Select Sponsor Receipt",
                },
                id_payment_type: {
                    required: "Select Payment Type",
                },
                paid_amount: {
                    required: "Enter Amount",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
