<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Student_model extends CI_Model
{
    function intakeList()
    {
        $this->db->select('*');
        $this->db->from('intake');
        $this->db->where('status', '1');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function nationalityList()
    {
        $this->db->select('*');
        $this->db->from('nationality');
        $this->db->where('status', 1);
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }


      function addFileDownload($data)
    {
        $this->db->trans_start();
        $this->db->insert('student_has_document', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function stateList()
    {
        $this->db->select('*');
        $this->db->from('state');
        $this->db->where('status', 1);
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }


    function gettemplate($id) {
        $this->db->select('*');
        $this->db->from('communication_template');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $intake = $query->row();
        return $intake;
    }

    function schemeListByStatus($status)
    {
        $this->db->select('ihs.*');
        $this->db->from('scheme as ihs');
        $this->db->where('ihs.status', $status);
        $query = $this->db->get();
        return $query->result();
    }

    function qualificationListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('education_level');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function raceListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('race_setup');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }


    function religionListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('religion_setup');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function qualificationList()
    {
        $this->db->select('*');
        $this->db->from('education_level');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function programList()
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', '1');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function getStudentDetailsById($id)
    {
        $this->db->select('*');
        $this->db->from('student');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $sd = $query->row();
        return $sd;
    }

    function getOrganisation()
    {
        $this->db->select('*');
        $this->db->from('organisation');
        $query = $this->db->get();
        $sd = $query->row();
        return $sd;
    }


    function applicantListSearch($applicantList)
    {
        $this->db->select('a.*, i.name as intake, p.code as program_code, p.name as program');
        $this->db->from('student as a');
        $this->db->join('intake as i', 'a.id_intake = i.id','left');
        $this->db->join('programme as p', 'a.id_program = p.id','left');
        if($applicantList['first_name'])
        {
            $likeCriteria = "(a.full_name  LIKE '%" . $applicantList['first_name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if($applicantList['email_id'])
        {
            $likeCriteria = "(a.email_id  LIKE '%" . $applicantList['email_id'] . "%')";
            $this->db->where($likeCriteria);
        }
        if($applicantList['nric'])
        {
            $likeCriteria = "(a.nric  LIKE '%" . $applicantList['nric'] . "%')";
            $this->db->where($likeCriteria);
        }
        if($applicantList['id_program'])
        {
            $this->db->where('a.id_program', $applicantList['id_program']);
        }
        if($applicantList['id_intake'])
        {
            $this->db->where('a.id_intake', $applicantList['id_intake']);
        }
        if($applicantList['id_partner_university'])
        {
            $this->db->where('a.id_university', $applicantList['id_partner_university']);
        }
        if($applicantList['applicant_status'])
        {
            $this->db->where('a.applicant_status', $applicantList['applicant_status']);
        }
        // $this->db->where('a.applicant_status !=', 'Graduated');
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }


    function getStudentInformation($id)
    {
        $this->db->select('a.*, p.code as program_code, p.name as program_name, i.year as intake_year, i.name as intake_name,ps.mode_of_program,ps.mode_of_study,o.name as branchname');
        $this->db->from('student as a');
        $this->db->join('programme as p', 'a.id_program = p.id');
        $this->db->join('intake as i', 'a.id_intake = i.id');
        $this->db->join('programme_has_scheme as ps', 'a.id_program_scheme  = ps.id','left');
        $this->db->join('organisation_has_training_center as o', 'a.id_branch = o.id','left');

        $this->db->where('a.id', $id);
        $query = $this->db->get();
        // echo $query;exit;
        $student = $query->row();
        return $student;
    }

    function getTemperoryOfferLetterByIntake($id)
    {
        $this->db->select('*');
        $this->db->from('intake');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $intake = $query->row();

        $temp_offer_letter =$intake->is_temp_offer_letter;

        return $temp_offer_letter;
    }

    function getStudent($id)
    {
        $this->db->select('a.is_submitted, a.student_status, p.name as program_name, p.code as program_code, i.name as intake_name, i.year as intake_year');
        $this->db->from('student as a');
            $this->db->join('programme as p', 'a.id_program = p.id');
            $this->db->join('intake as i', 'a.id_intake = i.id');
        $this->db->where('a.id', $id);
        $query = $this->db->get();
        $sd = $query->row();
        return $sd;
    }

    function getStudentDetails($id)
    {
        $this->db->select('*');
        $this->db->from('student');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $sd = $query->row();
        if($sd->sibbling_discount== "Yes" && $sd->employee_discount== "No"){

            $this->db->select('a.*, sd.sibbling_name, sd.sibbling_nric');
            $this->db->from('student as a');
            $this->db->join('student_has_sibbling_discount as sd', 'a.id = sd.id_student');
            $this->db->where('a.id', $id);
            $query = $this->db->get();
            // $d = $query->row();
            // print_r($d);exit();
            return $query->row();
        }
        else
        if($sd->employee_discount== "Yes" && $sd->sibbling_discount== "No"){

            $this->db->select('a.*, ed.employee_name, ed.employee_nric, ed.employee_designation');
            $this->db->from('student as a');
            $this->db->join('student_has_employee_discount as ed', 'a.id = ed.id_student');
            $this->db->where('a.id', $id);
            $query = $this->db->get();
            // print_r($query);exit();
            return $query->row();
        }
        else
         if($sd->employee_discount== "Yes" && $sd->sibbling_discount== "Yes"){

            $this->db->select('a.*, sd.sibbling_name, sd.sibbling_nric, ed.employee_name, ed.employee_nric, ed.employee_designation');
            $this->db->from('student as a');
            $this->db->join('student_has_sibbling_discount as sd', 'a.id = sd.id_student');
            $this->db->join('student_has_employee_discount as ed', 'a.id = ed.id_student');
            $this->db->where('a.id', $id);
            $query = $this->db->get();
            // print_r($query);exit();
            return $query->row();
        }
        else{
            $this->db->select('a.*');
            $this->db->from('student as a');
            $this->db->where('a.id', $id);
            $query = $this->db->get();
            return $query->row();
        }
    }
    
    function addStudent($data)
    {
        $this->db->trans_start();
        $this->db->insert('student', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addNewSibblingDiscount($data)
    {
        $this->db->trans_start();
        $this->db->insert('student_has_sibbling_discount', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addNewEmployeeDiscount($data)
    {
        $this->db->trans_start();
        $this->db->insert('student_has_employee_discount', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addNewAlumniDiscount($data)
    {
        $this->db->trans_start();
        $this->db->insert('student_has_alumni_discount', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editStudentDetails($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('student', $data);
        return TRUE;
    }

    function editSibblingDetails($data, $id)
    {
        $this->db->where('id_student', $id);
        $this->db->update('student_has_sibbling_discount', $data);
        return TRUE;
    }

    function editEmployeeDetails($data, $id)
    {
        $this->db->where('id_student', $id);
        $this->db->update('student_has_employee_discount', $data);
        return TRUE;
    }

    function editAlumniDetails($data, $id)
    {
        $this->db->where('id_student', $id);
        $this->db->update('student_has_alumni_discount', $data);
        return TRUE;
    }

    function countryListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('country');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();      
         return $result;
    }

    function getStateByCountryId($id_country)
    {
        $this->db->select('*');
        $this->db->from('state');
        $this->db->where('id_country', $id_country);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getProgramSchemeByProgramId($id_programme)
    {
        $this->db->select('DISTINCT(ihs.mode_of_program) as mode_of_program, ihs.*');
        $this->db->from('programme_has_scheme as ihs');
        $this->db->where('ihs.id_program', $id_programme);
        $query = $this->db->get();
        return $query->result();
    }


    function getSchemeByProgramId($id_programme)
    {
        // echo "<Pre>"; print_r($id_programme);exit;
        $this->db->select('DISTINCT(phs.id_scheme) as id_scheme');
        $this->db->from('program_has_scheme as phs');
        $this->db->join('scheme as sch', 'phs.id_scheme = sch.id');
        $this->db->where('phs.id_program', $id_programme);
        $query = $this->db->get();
        $results = $query->result();
            
        // echo "<Pre>"; print_r($results);exit;

        $details = array();

        foreach ($results as $result)
        {

            $id_scheme = $result->id_scheme;

            $scheme = $this->getScheme($id_scheme);
            if($scheme)
            {
                $result = $scheme;
                array_push($details, $result);
            }
        }

        return $details;
    }

    function getScheme($id_scheme)
    {
        $this->db->select('ihs.*');
        $this->db->from('scheme as ihs');
        $this->db->where('ihs.id', $id_scheme);
        $query = $this->db->get();
        return $query->row();
    }


    function getProgramScheme($id_program_scheme)
    {
        $this->db->select('ihs.*');
        $this->db->from('programme_has_scheme as ihs');
        $this->db->where('ihs.id', $id_program_scheme);
        $query = $this->db->get();
        return $query->row();
    }

    function getProgramLandscape($id_intake,$id_program,$id_program_scheme,$id_program_has_scheme)
    {
        $this->db->select('ihs.*');
        $this->db->from('programme_landscape as ihs');
        $this->db->where('ihs.id_intake', $id_intake);
        $this->db->where('ihs.id_programme', $id_program);
        $this->db->where('ihs.program_scheme', $id_program_has_scheme);
        $this->db->where('ihs.learning_mode', $id_program_scheme);
        $this->db->order_by("ihs.id", "DESC");
        $query = $this->db->get();
        return $query->row();
    }

    function getProgramDetails($id_program)
    {
        $this->db->select('ihs.*');
        $this->db->from('programme as ihs');
        $this->db->where('ihs.id', $id_program);
        $query = $this->db->get();
        return $query->row();
    }

    function getIntakeByProgrammeId($id_programme)
    {
        $this->db->select('DISTINCT(i.id) as id, i.*');
        $this->db->from('intake_has_programme as ihs');
        $this->db->join('intake as i', 'ihs.id_intake = i.id');
        $this->db->where('ihs.id_programme', $id_programme);
        $query = $this->db->get();
        return $query->result();
    }


     function getDocumentByProgrammeId($id_programme)
    {
        $this->db->select('DISTINCT(d.id) as id, d.*');
        $this->db->from('documents_program_details as dp');
        $this->db->join('documents as d', 'dp.id_document = d.id');
        $this->db->where('dp.id_program', $id_programme);
        $query = $this->db->get();
        return $query->result();
    }


    function checkFeeStructure($data)
    {
        $this->db->select('*');
        $this->db->from('fee_structure');
        $this->db->where('id_intake', $data['id_intake']);
        $this->db->where('id_programme', $data['id_program']);
        $this->db->where('id_program_scheme', $data['id_program_scheme']);
        $this->db->where('status', '1');
        $query = $this->db->get();
        // print_r($query->row());exit();
        $result = $query->row();
        if($result)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }

    function getSibblingDiscountByStudentId($id_student)
    {
        $this->db->select('ahsd.*');
        $this->db->from('student_has_sibbling_discount as ahsd');
        $this->db->where('id_student', $id_student);
        $query = $this->db->get();
        return $query->row();
    }

    function getEmployeeDiscountStudentId($id_student)
    {
        $this->db->select('ahemd.*');
        $this->db->from('student_has_employee_discount as ahemd');
        $this->db->where('id_student', $id_student);
        $query = $this->db->get();
        return $query->row();
    }

    function getAlumniDiscountStudentId($id_student)
    {
        $this->db->select('ahemd.*');
        $this->db->from('student_has_alumni_discount as ahemd');
        $this->db->where('id_student', $id_student);
        $query = $this->db->get();
        return $query->row();
    }

    function checkDuplicateStudent($data,$id)
    {
            // echo "<pre>"; print_r($id);exit();

        $this->db->select('*');
        $this->db->from('student');
        $this->db->where('email_id', $data['email_id']);
        $this->db->or_where('phone', $data['phone']);
        $this->db->or_where('nric', $data['nric']);
        $this->db->where('id !=', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getReceiptStatus($id_student)
    {
        $type = "Student";
        $zero= "0";
        $one= "1";
        $this->db->select('*');
        $this->db->from('receipt');
        // $likeCriteria = "(id_student  = '" . $id_student . "' and type  ='" . $type . "' and (status  ='" . $zero . "' or status  ='" . $one . "'))";
        $likeCriteria = "(id_student  = '" . $id_student . "' and type  ='" . $type . "')";
        $this->db->where($likeCriteria);

        // $this->db->where('id_student', $id_student);
        // $this->db->where('status', '0');
        // $this->db->or_where('status', '1');
        $query = $this->db->get();
        return $query->row();
    }
    
    // function deleteActivityDetails($id, $date)
    // {
    //     $this->db->where('id', $countryId);
    //     $this->db->update('academic_year', $countryInfo);
    //     return $this->db->affected_rows();
    // }


    function getStudentSibblingDiscountDetails($id_student)
    {
        $this->db->select('ahsd.*, usr.name as user_name');
        $this->db->from('student_has_sibbling_discount as ahsd');
        $this->db->join('tbl_users as usr', 'ahsd.rejected_by = usr.userId','left'); 
        $this->db->where('id_student', $id_student);
        $query = $this->db->get();
        return $query->row();
    }

    function getStudentEmployeeDiscountDetails($id_student)
    {
        $this->db->select('ahemd.*, usr.name as user_name');
        $this->db->from('student_has_employee_discount as ahemd');
        $this->db->join('tbl_users as usr', 'ahemd.rejected_by = usr.userId','left'); 
        $this->db->where('id_student', $id_student);
        $query = $this->db->get();
        return $query->row();
    }

    function getStudentAlumniDiscountDetails($id_student)
    {
        $this->db->select('ahemd.*, usr.name as user_name');
        $this->db->from('student_has_alumni_discount as ahemd');
        $this->db->join('tbl_users as usr', 'ahemd.rejected_by = usr.userId','left'); 
        $this->db->where('id_student', $id_student);
        $query = $this->db->get();
        return $query->row();
    }

    function salutationListByStatus($status)
    {
        $this->db->select('a.*');
        $this->db->from('salutation_setup as a');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getSalutation($id)
    {
        $this->db->select('a.*');
        $this->db->from('salutation_setup as a');
        $this->db->where('id', $id);
         $query = $this->db->get();
         $result = $query->row();  
         return $result;
    }

    function getBranchByProgramId($id_program)
    {
        $this->db->select('DISTINCT(ahemd.id_training_center) as id_training_center');
        $this->db->from('organisation_training_center_has_program as ahemd');
        $this->db->join('organisation_has_training_center as ohtc', 'ahemd.id_training_center = ohtc.id');
        $this->db->where('ahemd.id_program', $id_program);
        $query = $this->db->get();
        $results = $query->result();

        $organisation = $this->getOrganisaton();
        $details = array();

        if($organisation)
        {
            array_push($details, $organisation);
        }


        foreach ($results as $result)
        {
            $training_center = $this->getTrainingCenterById($result->id_training_center);
            array_push($details, $training_center);
        }
        return $details;
    }

    function getOrganisaton()
    {
        $this->db->select('a.*, a.short_name as code');
        $this->db->from('organisation as a');
        $query = $this->db->get();
        $result = $query->row();  
        return $result;
    }

    function getUniversityListByStatus($status)
    {
        $organisation = $this->getOrganisaton();
        $details = array();

        if($organisation)
        {
            array_push($details, $organisation);
        }

        $this->db->select('ahemd.*');
        $this->db->from('partner_university as ahemd');
        $this->db->where('ahemd.status', $status);
        $query = $this->db->get();
        $results = $query->result();

        foreach ($results as $result)
        {
            array_push($details, $result);
        }
        return $details;
    }


    function getBranchesByPartnerUniversity($id_organisation)
    {
        $this->db->select('ahemd.*');
        $this->db->from('organisation_has_training_center as ahemd');
        $this->db->where('ahemd.id_organisation', $id_organisation);
        $query = $this->db->get();
        $results = $query->result();
        return $results;
    }

    function getTrainingCenterById($id)
    {
        $this->db->select('a.*');
        $this->db->from('organisation_has_training_center as a');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $result = $query->row();  
        return $result;
    }

    function branchListByStatus()
    {
        $organisation = $this->getOrganisaton();
        $details = array();

        if($organisation)
        {
            array_push($details, $organisation);
        }

        $this->db->select('a.*');
        $this->db->from('organisation_has_training_center as a');
        $query = $this->db->get();
        $results = $query->result();  

        foreach ($results as $result)
        {
           array_push($details, $result);
        }
        return $details;
    }

    function getIntakeDetails($id)
    {
        $this->db->select('a.*');
        $this->db->from('intake as a');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $result = $query->row();  
        return $result;
    }

    function programEntryRequirementList($id_program)
    {
        $this->db->select('ier.*, qs.code as qualification_code, qs.name as qualification_name, ws.code as work_code, ws.name as work_name');
        $this->db->from('scholarship_individual_entry_requirement as ier');
        $this->db->join('education_level as qs','ier.id_education_qualification = qs.id','left');
        $this->db->join('scholarship_work_specialisation as ws','ier.id_work_specialisation = ws.id','left');
        $this->db->where('ier.id_program', $id_program);
        $query = $this->db->get();
        return $query->result();
    }

    function programDetails($id_program)
    {
        $this->db->select('a.*');
        $this->db->from('programme as a');
        $this->db->where('a.id', $id_program);
        $query = $this->db->get();
        $result = $query->row(); 

        return $result;
    }

    function programRequiremntListList()
    {
        $this->db->select('ier.*, qs.code as qualification_code, qs.name as qualification_name, ws.code as work_code, ws.name as work_name');
        $this->db->from('scholarship_individual_entry_requirement as ier');
        $this->db->join('education_level as qs','ier.id_education_qualification = qs.id','left');
        $this->db->join('scholarship_work_specialisation as ws','ier.id_work_specialisation = ws.id','left');
        $query = $this->db->get();
        return $query->result();
    }

    function getStudentUploadedFiles($id_student)
    {
        $this->db->select('shd.*, d.code as document_code, d.name as document_name');
        $this->db->from('student_has_document as shd');
        $this->db->join('documents as d','shd.id_document = d.id');
        $this->db->where('shd.id_student', $id_student);
        $query = $this->db->get();
        return $query->result();
    }

    function getApplicantUploadedFiles($id_applicant)
    {
        $this->db->select('shd.*, d.code as document_code, d.name as document_name');
        $this->db->from('applicant_has_document as shd');
        $this->db->join('documents as d','shd.id_document = d.id');
        $this->db->where('shd.id_applicant', $id_applicant);
        $query = $this->db->get();
        return $query->result();
    }

    function deleteStudentUploadedDocument($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('student_has_document');
        return TRUE;
    }
}