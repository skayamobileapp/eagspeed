<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Partner_university_model extends CI_Model
{

    function countryListByStatus($status)
    {
        $this->db->select('a.*');
        $this->db->from('country as a');
        $this->db->where('a.status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function statusListByType($type)
    {
        $this->db->select('*');
        $this->db->from('status_table');
        $this->db->where('type', $type);
        $this->db->where('status', '1');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function stateListByStatus($status)
    {
        $this->db->select('a.*');
        $this->db->from('state as a');
        $this->db->where('a.status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function staffListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('staff');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function currencyListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('currency_setup');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function bankListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('bank_registration');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }


    function partnerUniversityListSearch($data)
    {
        $this->db->select('d.*, c.name as country');
        $this->db->from('partner_university as d');
        $this->db->join('country as c','d.id_country = c.id');
        if (!empty($data['name']))
        {
            $likeCriteria = "(d.name  LIKE '%" . $data['name'] . "%' or d.short_name  LIKE '%" . $data['name'] . "%' or d.code  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if (!empty($data['id_country']))
        {
            $this->db->where('d.id_country', $data['id_country']);
        }
        if (!empty($data['url']))
        {
            $likeCriteria = "(d.url  LIKE '%" . $data['url'] . "%')";
            $this->db->where($likeCriteria);
        }
        if (!empty($data['email']))
        {
            $likeCriteria = "(d.email  LIKE '%" . $data['email'] . "%')";
            $this->db->where($likeCriteria);
        }
        if (!empty($data['contact_number']))
        {
            $likeCriteria = "(d.contact_number  LIKE '%" . $data['contact_number'] . "%'";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("d.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function getPartnerUniversity($id)
    {
        $this->db->select('d.*, pc.name as partner_category, c.name as country, s.name as state');
        $this->db->from('partner_university as d');
        $this->db->join('partner_category as pc','d.id_partner_category = pc.id','left');
        $this->db->join('country as c','d.id_country = c.id','left');
        $this->db->join('state as s','d.id_state = s.id','left');
        $this->db->where('d.id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewPartnerUniversity($data)
    {
        $this->db->trans_start();
        $this->db->insert('partner_university', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editPartnerUniversity($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('partner_university', $data);
        return TRUE;
    }
    
    function deletePartnerUniversity($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('partner_university', $data);
        return $this->db->affected_rows();
    }

    function addComitee($data)
    {
        $this->db->trans_start();
        $this->db->insert('partner_university_comitee', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function deleteComitee($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('partner_university_comitee');
        return TRUE;
    }

    function comiteeList($id_partner_university)
    {
        $this->db->select('a.*');
        $this->db->from('partner_university_comitee as a');
        $this->db->where('a.id_partner_university', $id_partner_university);
        // $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }




    function partnerUniversityListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('partner_university');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function partnerCategoryListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('partner_category');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function getPartnerUniversityCategory($id)
    {
        $this->db->select('*');
        $this->db->from('partner_category');
        $this->db->where('id', $id);
         $query = $this->db->get();
         $result = $query->row();
         return $result;
    }

    function getAggrementByPartnerUnversityNAggrementId($id_aggrement,$id_partner_university)
    {
        $this->db->select('*');
        $this->db->from('scholarship_partner_university_has_aggrement');
        $this->db->where('id', $id_aggrement);
        $this->db->where('id_partner_university', $id_partner_university);
         $query = $this->db->get();
         $result = $query->row();
         return $result;
    }

    function getStateByCountryId($id_country)
    {
        $this->db->select('*');
        $this->db->from('state');
        $this->db->where('id_country', $id_country);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function trainingCenterList($id_partner_university)
    {
        // This Table Is Not Using Now
        // $this->db->select('a.*');
        // $this->db->from('scholarship_partner_university_has_training_center as a');
        // $this->db->where('a.id_partner_university', $id_partner_university);
        // // $this->db->order_by("name", "ASC");
        // $query = $this->db->get();
        // $result = $query->result();
        // return $result;

        $this->db->select('a.*');
        $this->db->from('organisation_has_training_center as a');
        $this->db->where('a.id_organisation', $id_partner_university);
        // $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();
        return $result;

        
    }

       function getPartnerUniversityAggrementList($id_partner_university)
    {
        $this->db->select('aggr.*, cs.code as currency_code, cs.name as currency_name');
        $this->db->from('partner_university_has_aggrement as aggr');
        $this->db->join('currency_setup as cs','aggr.id_currency = cs.id');
        $this->db->where('aggr.id_partner_university', $id_partner_university);
        $query = $this->db->get();
        return $query->result();
    }

    function partnerProgramDetails($id_partner_university)
    {
        $this->db->select('*');
        $this->db->from('scholarship_partner_university_has_program');
        $this->db->where('id_partner_university', $id_partner_university);
        $query = $this->db->get();
        return $query->row();
    }

    function partnerProgramStudyModeDetails($id_partner_university,$id_program)
    {
        $this->db->select('pupd.*, c.code as training_code, c.name as training_name, c.location, c.city');
        $this->db->from('scholarship_partner_university_program_has_study_mode as pupd');
        $this->db->join('organisation_has_training_center as c','pupd.id_training_center = c.id');
        $this->db->where('pupd.id_partner_university', $id_partner_university);
        $this->db->where('pupd.id_program_detail', $id_program);
        $query = $this->db->get();
        return $query->result();
    }

    function partnerProgramApprenticeshipDetails($id_partner_university,$id_program)
    {
        $this->db->select('pupd.*, c.code as apprenticeship_code, c.name as apprenticeship_name, c.location, c.city');
        $this->db->from('scholarship_partner_university_program_has_apprenticeship as pupd');
        $this->db->join('organisation_has_training_center as c','pupd.id_apprenticeship_center = c.id');
        $this->db->where('pupd.id_partner_university', $id_partner_university);
        $this->db->where('pupd.id_program_detail', $id_program);
        $query = $this->db->get();
        return $query->result();
    }

    function partnerProgramInternshipDetails($id_partner_university,$id_program)
    {
        $this->db->select('pupd.*, c.code as internship_code, c.name as internship_name, c.location, c.city');
        $this->db->from('scholarship_partner_university_program_has_internship as pupd');
        $this->db->join('organisation_has_training_center as c','pupd.id_internship_center = c.id');
        $this->db->where('pupd.id_partner_university', $id_partner_university);
        $this->db->where('pupd.id_program_detail', $id_program);
        $query = $this->db->get();
        return $query->result();
    }

    function partnerProgramSyllabusDetails($id_partner_university,$id_program)
    {
        $this->db->select('pupd.*, c.code as module_code, c.name as module_name');
        $this->db->from('scholarship_partner_university_program_has_syllabus as pupd');
        $this->db->join('scholarship_module_type_setup as c','pupd.id_module_type = c.id');
        $this->db->where('pupd.id_partner_university', $id_partner_university);
        $this->db->where('pupd.id_program_detail', $id_program);
        $query = $this->db->get();
        return $query->result();
    }

    function deleteMoaAggrement($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('scholarship_partner_university_has_aggrement');
        return TRUE;
    }

    function addTrainingCenter($data)
    {
        $this->db->trans_start();
        $this->db->insert('organisation_has_training_center', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function deleteTrainingCenter($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('organisation_has_training_center');
        return TRUE;
    }


    function addNewPartnerUniversityProgram($data)
    {
        $this->db->trans_start();
        $this->db->insert('scholarship_partner_university_has_program', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

     function editPartnerUniversityProgram($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('scholarship_partner_university_has_program', $data);
        return TRUE;
    }

    function addNewPartnerProgramStudyMode($data)
    {
        $this->db->trans_start();
        $this->db->insert('scholarship_partner_university_program_has_study_mode', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addNewPartnerProgramInternship($data)
    {
        $this->db->trans_start();
        $this->db->insert('scholarship_partner_university_program_has_internship', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addNewPartnerProgramApprenticeship($data)
    {
        $this->db->trans_start();
        $this->db->insert('scholarship_partner_university_program_has_apprenticeship', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addNewPartnerProgramSyllabus($data)
    {
        $this->db->trans_start();
        $this->db->insert('scholarship_partner_university_program_has_syllabus', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }


    function deletePartnerProgramStudyMode($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('scholarship_partner_university_program_has_study_mode');
        return TRUE;
    }

    function deletePartnerProgramApprenticeship($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('scholarship_partner_university_program_has_apprenticeship');
        return TRUE;
    }


    function deletePartnerProgramInternship($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('scholarship_partner_university_program_has_internship');
        return TRUE;
    }


    function deletePartnerProgramSyllabus($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('scholarship_partner_university_program_has_syllabus');
        return TRUE;
    }

    function programListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', $status);
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function moduleTypeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('scholarship_module_type_setup');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function addNewAggrement($data)
    {
        $this->db->trans_start();
        $this->db->insert('scholarship_partner_university_has_aggrement', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function qualificationListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('education_level');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function getProgrammeByEducationLevelId($id_education_level)
    {
        $this->db->select('ihs.*');
        $this->db->from('programme as ihs');
        // $this->db->join('intake as i', 'ihs.id_intake = i.id');
        $this->db->where('ihs.id_education_level', $id_education_level);
        $this->db->where('ihs.status', 1);
        $query = $this->db->get();
        return $query->result();
    }

    function getIntakeListByProgramme($id_programme)
    {
        $this->db->select('DISTINCT(ihp.id_intake) as id_intake, ihp.*, in.name as intake_name, in.year as intake_year');
        $this->db->from('intake_has_programme as ihp');
        $this->db->join('intake as in', 'ihp.id_intake = in.id');
        $this->db->where('ihp.id_programme', $id_programme);
        $query = $this->db->get();
         return $query->result();
    }

    function getProgramSchemeByProgramId($id_programme)
    {
        $this->db->select('DISTINCT(ihs.mode_of_program) as mode_of_program, ihs.*');
        $this->db->from('programme_has_scheme as ihs');
        $this->db->where('ihs.id_program', $id_programme);
        $query = $this->db->get();
        return $query->result();
    }

    function getSchemeByProgramId($id_programme)
    {
        // echo "<Pre>"; print_r($id_programme);exit;
        $this->db->select('DISTINCT(phs.id_scheme) as id_scheme');
        $this->db->from('program_has_scheme as phs');
        $this->db->join('scheme as sch', 'phs.id_scheme = sch.id');
        $this->db->where('phs.id_program', $id_programme);
        $query = $this->db->get();
        $results = $query->result();
            
        // echo "<Pre>"; print_r($results);exit;

        $details = array();

        foreach ($results as $result)
        {

            $id_scheme = $result->id_scheme;

            $scheme = $this->getScheme($id_scheme);
            if($scheme)
            {
                $result = $scheme;
                array_push($details, $result);
            }
        }

        return $details;
    }

    function getScheme($id_scheme)
    {
        $this->db->select('ihs.*');
        $this->db->from('scheme as ihs');
        $this->db->where('ihs.id', $id_scheme);
        $query = $this->db->get();
        return $query->row();
    }

     function getAllCoursesByPartnerUniversity($id) {
         $this->db->select('a.*');
        $this->db->from('programme as a');
        $this->db->where('a.id_partner_university', $id);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }


      function getProgrammeFees($id){
        $this->db->select('d.*');
        $this->db->from('fee_structure_master as d');
        $this->db->where('id_programme', $id);

                $query = $this->db->get();
        $result = $query->row();  
        return $result;
    }

      function getAgreementDetails($idaggreement,$idprogramme) {
         $this->db->select('a.*');
        $this->db->from('agreement_course_details as a');
        $this->db->where('a.id_aggrement', $idaggreement);
        $this->db->where('a.id_programme', $idprogramme);
        $query = $this->db->get();
        $result = $query->row();
        return $result;
    }


     function getPartnerUniversityAggrement($id)
    {
        $this->db->select('phs.*, cs.name as currency_name, cs.code as currency_code');
        $this->db->from('partner_university_has_aggrement as phs');
        $this->db->join('currency_setup as cs', 'phs.id_currency = cs.id');
        $this->db->where('phs.id', $id);
        $query = $this->db->get();
        return $query->row();        
    }
    function addNewFeeStructureMaster($data)
    {
        $this->db->trans_start();
        $this->db->insert('fee_structure_master', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function feeStructureMasterListSearch($data)
    {
        $this->db->select('a.*, in.year as intake_year,in.name as intake, p.name as program, p.code as program_code, el.name as education_level, sch.code as scheme_code, sch.description as scheme_name, lm.mode_of_program, lm.mode_of_study');
        $this->db->from('fee_structure_master as a');
        $this->db->join('intake as in', 'a.id_intake = in.id');
        $this->db->join('programme as p', 'a.id_programme = p.id');
        $this->db->join('education_level as el', 'a.id_education_level = el.id','left');
        $this->db->join('scheme as sch', 'a.id_program_scheme = sch.id','left');
        $this->db->join('programme_has_scheme as lm', 'a.id_learning_mode = lm.id','left');
        // $this->db->join('fee_setup as fs', 'a.id_fee_item = fs.id','left');
        // $this->db->join('currency_setup as cs', 'a.id_currency = cs.id','left');
        // , fs.code as fee_code, fs.name as fee_name, cs.code as currency_code, cs.name as currency_name
        // $this->db->join('programme_has_scheme as lm', 'a.id_learning_mode = lm.id');
        // if($data['name'] != '')
        // {
        //     $likeCriteria = "(a.name  LIKE '%" . $data['name'] . "%')";
        //     $this->db->where($likeCriteria);
        // }
        // if($data['id_education_level'] != '')
        // {
        //     $this->db->where('a.id_education_level', $data['id_education_level']);
        // }
        // if($data['id_programme'] != '')
        // {
        //     $this->db->where('a.id_programme', $data['id_programme']);
        // }
        // if($data['id_intake'] != '')
        // {
        //     $this->db->where('a.id_intake', $data['id_intake']);
        // }
        // if($data['id_learning_mode'] != '')
        // {
        //     $this->db->where('a.id_learning_mode', $data['id_learning_mode']);
        // }
        // if($data['id_program_scheme'] != '')
        // {
        //     $this->db->where('a.id_program_scheme', $data['id_program_scheme']);
        // }

        if($data['id_aggrement'] != '')
        {
            $this->db->where('a.id_aggrement', $data['id_aggrement']);
        }
        if($data['id_partner_university'] != '')
        {
            $this->db->where('a.id_partner_university', $data['id_partner_university']);
        }

         $query = $this->db->get();
         $results = $query->result();


        return $results;

        //  $details = array();

        //  foreach ($results as $value)
        //  {
        //     $is_installment = $value->is_installment;
        //     $id_fee_structure_master = $value->id;


        //     // echo "<Pre>"; print_r($is_installment);exit();  


        //     if($is_installment == 1)
        //     {
        //         $fee_structure_installment_details = $this->getTrainingCenterInstallmentDetailsForTotalCalculation($id_fee_structure_master);

        //         // echo "<Pre>";print_r($fee_structure_installment_details);exit();

        //         $total_instalment_amount = 0;
        //         foreach ($fee_structure_installment_details as $installment_detail)
        //         {
        //             $amount = $installment_detail->amount;
        //             $total_instalment_amount = $total_instalment_amount + $amount;
        //         }

        //         $value->amount = $total_instalment_amount;

        //     }
        //     else
        //     {
        //         $value->amount = $value->amount;
        //     }

        //     // echo "<Pre>"; print_r($is_installment);exit();  
            
        //     array_push($details, $value);
        // }   

        // return $details;
    }


    function getTrainingCenterInstallmentDetailsForTotalCalculation($id_fee_structure)
    {
        $this->db->select('p.*, sem.name as fee_name, sem.code as fee_code, fm.name as frequency_mode');
        $this->db->from('fee_structure_has_training_center as p');
        $this->db->join('fee_setup as sem', 'p.id_fee_item = sem.id');
        $this->db->join('frequency_mode as fm', 'sem.id_frequency_mode = fm.id');
        $this->db->join('fee_structure_triggering_point as fstp', 'p.id_fee_structure_trigger = fstp.id');
        $this->db->where('p.id_fee_structure', $id_fee_structure);
         $query = $this->db->get();
         
         $result = $query->result();  
         return $result;
    }


    function getFeeStructureMaster($id)
    {
        $this->db->select('a.*, in.year as intake_year,in.name as intake, p.name as program, p.code as program_code, el.name as education_level, sch.code as scheme_code, sch.description as scheme_name, lm.mode_of_program, lm.mode_of_study');
        $this->db->from('fee_structure_master as a');
        $this->db->join('intake as in', 'a.id_intake = in.id');
        $this->db->join('programme as p', 'a.id_programme = p.id');
        $this->db->join('education_level as el', 'a.id_education_level = el.id');
        $this->db->join('scheme as sch', 'a.id_program_scheme = sch.id');
        $this->db->join('programme_has_scheme as lm', 'a.id_learning_mode = lm.id');
        $this->db->where('a.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function feeSetupListByStatus($status)
    {
        $this->db->select('fs.*');
        $this->db->from('fee_setup as fs');
        $this->db->where('fs.status', $status);
        $this->db->order_by("fs.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getTrainingCenterFeeStructure($data)
    {
        $this->db->select('p.*, fs.code as currency_code, fs.name as currency');
        $this->db->from('fee_structure as p');
        $this->db->join('currency_setup as fs', 'p.currency = fs.id');
        // $this->db->where('p.id_training_center', $data['id_training_center']);
        // $this->db->where('p.id_program_landscape', $data['id_program_landscape']);
        $this->db->where('p.id', $data['id_fee_structure']);
         $query = $this->db->get();
         
         $result = $query->row();  
         return $result;
    }

    function getFeeStructureTriggerListByStatus($status)
    {
        $this->db->select('p.*');
        $this->db->from('fee_structure_triggering_point as p');
        $this->db->where('p.status', $status);
        $this->db->order_by('p.name', 'ASC');
        $query = $this->db->get();
         
        $result = $query->result();  
        return $result;
    }
    
    function getTrainingCenterInstallmentDetails($data)
    {
        $this->db->select('p.*, sem.name as fee_name, sem.code as fee_code, fm.name as frequency_mode, fstp.name as trigger_name');
        $this->db->from('fee_structure_has_training_center as p');
        $this->db->join('fee_setup as sem', 'p.id_fee_item = sem.id');
        $this->db->join('frequency_mode as fm', 'sem.id_frequency_mode = fm.id');
        $this->db->join('fee_structure_triggering_point as fstp', 'p.id_fee_structure_trigger = fstp.id');
        $this->db->where('p.id_fee_structure', $data['id_fee_structure']);
        $this->db->where('p.id_program_landscape', $data['id_fee_structure_master']);
         $query = $this->db->get();
         
         $result = $query->result();  
         return $result;
    }

    function saveInstallmentData($data)
    {
        $this->db->trans_start();
        $this->db->insert('fee_structure_has_training_center', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function deleteSemesterDataByIdFeeStructureTrainingCenter($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('fee_structure_has_training_center');

       return TRUE;
    }

    function getPartnerUniversityAggrementFeeMaster($id)
    {
        $this->db->select('phs.*');
        $this->db->from('fee_structure_master as phs');
        // , cs.name as currency_name, cs.code as currency_code
        // $this->db->join('currency_setup as cs', 'phs.id_currency = cs.id');
        $this->db->where('phs.id', $id);
        $query = $this->db->get();
        return $query->row();        
    }

    function addNewFeeStructure($data)
    {
        $this->db->trans_start();
        $this->db->insert('fee_structure', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function feeStructureListSearch($data)
    {
        $this->db->select('tfsd.*, fs.name as fee_name, fs.name as fee_code, fm.name as frequency_mode, cs.code as currency_code, cs.name as currency_name, fstp.name as trigger_name');
        $this->db->from('fee_structure as tfsd');
        $this->db->join('fee_setup as fs', 'tfsd.id_fee_item = fs.id','left');
        $this->db->join('frequency_mode as fm', 'tfsd.id_frequency_mode = fm.id','left');
        $this->db->join('currency_setup as cs', 'tfsd.currency = cs.id','left');
        $this->db->join('fee_structure_triggering_point as fstp', 'tfsd.id_fee_structure_trigger = fstp.id','left');
        $this->db->where('tfsd.id_program_landscape', $data['id_fee_structure_master']);
        $this->db->where('tfsd.id_training_center', $data['id_partner_university']);
        $this->db->where('tfsd.id_aggrement', $data['id_aggrement']);
        $query = $this->db->get();
        $results = $query->result();

        // return $results;

         $details = array();

         foreach ($results as $result)
         {
            // echo "<Pre>";print_r($result);exit();

            $result->fee_code = '';
            $result->fee_name = '';
            $id_training_center = $result->id_training_center;
            $id_fee_item = $result->id_fee_item;
            $id_fee_structure = $result->id;

            $is_installment = $result->is_installment;


            $training_center = $this->getPartnerUniversity($id_training_center);
            $fee_item = $this->getFeeItem($id_fee_item);

            
            // echo "<Pre>";print_r($id_training_center);exit();


            if($training_center)
            {

                if($is_installment == 1)
                {
                    $fee_structure_installment_details = $this->getTrainingCenterInstallmentDetailsForTotalCalculation($id_fee_structure);

                    // echo "<Pre>";print_r($fee_structure_installment_details);exit();

                    $total_instalment_amount = 0;
                    foreach ($fee_structure_installment_details as $installment_detail)
                    {
                        $amount = $installment_detail->amount;
                        $total_instalment_amount = $total_instalment_amount + $amount;
                    }

                    $training_center->amount = $total_instalment_amount;

                }
                else
                {
                    $training_center->amount = $result->amount;
                }



                $training_center->trigger_name = $result->trigger_name;
                $training_center->is_installment = $result->is_installment;
                $training_center->installments = $result->installments;
                $training_center->currency = $result->currency;
                $training_center->currency_code = $result->currency_code;
                $training_center->id_fee_structure = $result->id;
                $training_center->fee_code = '';
                $training_center->fee_name = '';

                if($fee_item)
                {
                    $training_center->fee_code = $fee_item->code;
                    $training_center->fee_name = $fee_item->name;
                }
                array_push($details, $training_center);
            }
        }   

        return $details;
    }

    function getFeeItem($id)
    {
        $this->db->select('p.*');
        $this->db->from('fee_setup as p');
        $this->db->where('p.id', $id);
         $query = $this->db->get();
         
         $result = $query->row();  
         return $result;
    }

    function getPartnerUniversityDuplication($data)
    {
        $this->db->select('st.*');
        $this->db->from('partner_university as st');        
        if($data['id_partner'] != '')
        {
            $this->db->where('st.id !=', $data['id_partner']);
        }
        if($data['code'] != '')
        {
            $this->db->where('st.code', $data['code']);
        }
        if($data['name'] != '')
        {
            $this->db->where('st.name', $data['name']);
        }
        if($data['email'] != '')
        {
            $this->db->where('st.email', $data['email']);
        }
        if($data['login_id'] != '')
        {
            $this->db->where('st.login_id', $data['login_id']);
        }
        $query = $this->db->get();
        $result = $query->row();

        return $result;
    }

    function checkPartnerUniversityPassword($data)
    {
        $this->db->select('*');
        $this->db->from('partner_university');
        $this->db->where('id', $data['id_partner_university']);
        $this->db->where('password', $data['password']);
        $query = $this->db->get();
        return $query->row();
    }
}