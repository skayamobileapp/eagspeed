<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>View Partner University Invoice</h3>
            <a href="../list" class="btn btn-link btn-back">‹ Back</a>
        </div>

        


        <form id="form_performa_invoice" action="" method="post">

            <div class="form-container">
                <h4 class="form-group-title">Invoice Details</h4>             
                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Invoice Number <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="invoice_number" name="invoice_number" value="<?php echo $mainInvoice->invoice_number;?>" readonly="readonly">
                        </div>
                    </div>

                    

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Invoice Type</label>
                            <input type="text" class="form-control" id="remarks" name="remarks" value="<?php echo $mainInvoice->type;?>" readonly="readonly">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Remarks</label>
                            <input type="text" class="form-control" id="remarks" name="remarks" value="<?php echo $mainInvoice->remarks;?>" readonly="readonly">
                        </div>
                    </div>

                </div>

                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Invoice Total Amount <span class='error-text'>*</span></label>
                            <input type="amount" class="form-control" id="invoice_total" name="invoice_total" value="<?php echo number_format($mainInvoice->invoice_total, 2, '.', ',');?>" readonly="readonly">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Total Discount Amount <span class='error-text'>*</span></label>
                            <input type="amount" class="form-control" id="total_discount" name="total_discount" value="<?php echo number_format($mainInvoice->total_discount, 2, '.', ',');?>" readonly="readonly">
                        </div>
                    </div>


                     <div class="col-sm-4">
                        <div class="form-group">
                            <label>Total Payable Amount <span class='error-text'>*</span></label>
                            <input type="amount" class="form-control" id="total_amount" name="total_amount" value="<?php echo number_format($mainInvoice->total_amount, 2, '.', ',');?>" readonly="readonly">
                        </div>
                    </div>
                
                </div>

                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Paid Amount <span class='error-text'>*</span></label>
                            <input type="amount" class="form-control" id="total_amount" name="total_amount" value="<?php echo number_format($mainInvoice->paid_amount, 2, '.', ',');?>" readonly="readonly">
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Balance Amount <span class='error-text'>*</span></label>
                            <input type="amount" class="form-control" id="total_amount" name="total_amount" value="<?php echo number_format($mainInvoice->balance_amount, 2, '.', ',');?>" readonly="readonly">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Currency <span class='error-text'>*</span></label>
                            <input type="amount" class="form-control" id="total_amount" name="total_amount" value="<?php echo $mainInvoice->currency; ?>" readonly="readonly">
                        </div>
                    </div>



                </div>

                <div class="row">



                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Status <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="remarks" name="remarks" value="<?php 
                            if($mainInvoice->status == '0')
                            {
                                echo 'Pending';
                            }
                            elseif($mainInvoice->status == '1')
                            {
                                echo 'Approved';
                            }
                            elseif($mainInvoice->status == '2')
                            {
                                echo 'Cancelled';
                            }?>" readonly="readonly">
                        </div>
                    </div>
                </div>



                <div class="row">

                <?php
                if($mainInvoice->status == '2')
                {
                 ?>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Cancel Reason <span class='error-text'>*</span></label>
                            <input type="text" id="reason" name="reason" class="form-control" value="<?php echo $mainInvoice->reason; ?>" readonly>
                        </div>
                    </div>

                <?php
                }
                ?>

                </div>
                
            </div>



            <div class="page-title clearfix">
                <a href="<?php echo '/finance/partnerUniversityInvoice/generateMainInvoice/'.$mainInvoice->id ?>" target="_blank" class="btn btn-link btn-back">
                    Download Invoice >>></a>
            </div>


            

            <?php
                if($mainInvoice->type == 'Sponser')
                {
                 ?>




                 <div class="form-container">
                <h4 class="form-group-title">Student Details</h4>
                <div class='data-list'>
                    <div class='row'> 
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student Name :</dt>
                                <dd><?php echo ucwords($studentDetails->full_name);?></dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd><?php echo $studentDetails->nric ?></dd>
                            </dl>
                            <dl>
                                <dt>Student Email :</dt>
                                <dd><?php echo $studentDetails->email_id; ?></dd>
                            </dl>
                             <dl>
                                <dt>Nationality :</dt>
                                <dd><?php echo $studentDetails->nationality ?></dd>
                            </dl> 
                            <dl>
                                <dt>Program Scheme :</dt>
                                <dd><?php echo $studentDetails->program_scheme ?></dd>
                            </dl>                            
                        </div>        
                        
                        <div class='col-sm-6'>                           
                            <dl>
                                <dt>Intake :</dt>
                                <dd><?php echo $studentDetails->intake_name ?></dd>
                            </dl>
                            <dl>
                                <dt>Program :</dt>
                                <dd><?php echo $studentDetails->programme_name; ?></dd>
                            </dl>
                            <dl>
                                <dt>Qualification Type :</dt>
                                <dd><?php echo $studentDetails->qualification_code . " - " . $studentDetails->qualification_name; ?></dd>
                            </dl>
                            <dl>
                                <dt>Program Scheme :</dt>
                                <dd><?php echo $studentDetails->program_scheme; ?></dd>
                            </dl>
                            <dl>
                                <dt>Academic Advisor :</dt>
                                <dd><?php echo $studentDetails->ic_no . " - " . $studentDetails->advisor; ?></dd>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>


            <br>



                 <?php
             }
             ?>


            <div class="form-container">
                <h4 class="form-group-title"><?php echo $mainInvoice->type; ?> Details For Main Invoice</h4>              

                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label><?php echo $mainInvoice->type; ?> Name <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="student_name" name="student_name" value="<?php echo $invoiceFor->full_name;?>" readonly="readonly">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label><?php echo $mainInvoice->type; if($mainInvoice->type == 'Partner University'){
                                echo ' Code';
                            }else{
                                echo ' NRIC';
                            } ?> <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="student_nric" name="student_nric" value="<?php echo $invoiceFor->nric;?>" readonly="readonly">
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Program <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="programme" name="programme" readonly="readonly" value="<?php echo $mainInvoice->programme_code . ' - ' . $mainInvoice->programme_name;?>" >
                        </div>
                    </div>


                </div>



                  
            </div>





            <div class="form-container">
                    <h4 class="form-group-title"> Invoice Details</h4>          
                    <div class="m-auto text-center">
                        <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
                    </div>
                    <div class="clearfix">
                        <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                            <li role="presentation" class="active" ><a href="#invoice" class="nav-link border rounded text-center"
                                    aria-controls="invoice" aria-selected="true"
                                    role="tab" data-toggle="tab">Student Details</a>
                            </li>

                            <?php
                            if($mainInvoice->applicant_partner_fee == 0)
                            {
                            ?>


                            <li role="presentation" id="view_student_tab"><a href="#tab_two" class="nav-link border rounded text-center" aria-controls="tab_two" role="tab" data-toggle="tab">Fee Item Details</a>
                            </li>

                            <?php
                            }
                            ?>
                        </ul>

                        
                        <div class="tab-content offers-tab-content">

                            <div role="tabpanel" class="tab-pane active" id="invoice">
                                <div class="col-12 mt-4">




                                    <!-- <form id="form_student_data" action="" method="post"> -->


                                    <div class="form-container">
                                        <h4 class="form-group-title">Student Details</h4> 

                                        <div class="custom-table">
                                            <table class="table" id="list-table">
                                                <thead>
                                                <tr>
                                                    <th>Sl. No</th>
                                                    <th>Student Name</th>
                                                    <th>Student NRIC</th>
                                                    <th>Program</th>
                                                    <th>Intake</th>
                                                    <th>Qualification</th>
                                                    <th>Email</th>
                                                    <th>Phone</th>
                                                    <!-- <th style='text-align: center;'>Advisor</th> -->
                                                    <th style='text-align: center;'>Amount</th>
                                                    <th style='text-align: center;'>View</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                    $total_amount = 0;
                                                if (!empty($mainInvoiceStudentDetails)) {
                                                    $i = 1;
                                                    foreach ($mainInvoiceStudentDetails as $record) {
                                                ?>
                                                    <tr>
                                                        <td><?php echo $i ?></td>
                                                        <td><?php echo $record->student_name ?></td>
                                                        <td><?php echo $record->nric ?></td>
                                                        <td><?php echo $record->program_code . " - " . $record->program_name; ?></td>
                                                        <td><?php echo $record->intake_name ?></td>
                                                        <td><?php echo $record->qualification_name ?></td>
                                                        <td><?php echo $record->email_id ?></td>
                                                        <td><?php echo $record->phone ?></td>
                                                        <!-- <td><?php echo $record->ic_no . " - " . $record->advisor_name; ?></td> -->
                                                        <td><?php echo $record->amount ?></td>
                                                        <?php
                                                        if($mainInvoice->applicant_partner_fee > 0)
                                                        {
                                                        ?>

                                                        <td class="text-center">
                                                            <a onclick="viewStudentFeeBulkPartnerDetails(<?php echo $record->id; ?>)" title="View Details">View</a>
                                                        </td>
                                                        <?php
                                                        }
                                                        ?>
                                                    </tr>
                                                <?php
                                                $total_amount = $total_amount + $record->amount;
                                                $i++;
                                                    }
                                                }
                                                $total_amount = number_format($total_amount, 2, '.', ',');
                                                ?>
                                                <tr>
                                                        <td bgcolor="" style="text-align:  right;" colspan="8"><b> Total :  </b></td>
                                                        <td bgcolor=""><b><?php echo $total_amount; ?></b></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>    


                                    <!-- </form> -->



                                </div> 
                            </div>




                            <div role="tabpanel" class="tab-pane" id="tab_two">
                                <div class="col-12 mt-4">





                                <!-- <form id="form_main_invoice_details" action="" method="post"> -->

                                    <?php
                                    if($mainInvoice->type == 'Partner University')
                                    {
                                        ?>

                                            <div class="form-container">
                                                    <h4 class="form-group-title">Main Invoice Details</h4>  

                                                <div class="custom-table">
                                                    <table class="table" id="list-table">
                                                        <thead>
                                                        <tr>
                                                            <th>Sl. No</th>
                                                            <th>Fee Item</th>
                                                            <th>Frequency Mode</th>
                                                            <th>Amount Calculation Mode</th>
                                                            <th>Currency</th>
                                                            <th>Description</th>
                                                            <th>No Of Students</th>
                                                            <th>Price</th>
                                                            <th>Amount</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php
                                                            $total_amount = 0;
                                                        if (!empty($mainInvoiceDetailsList)) {
                                                            $i = 1;
                                                            foreach ($mainInvoiceDetailsList as $record) {
                                                        ?>
                                                            <tr>
                                                                <td><?php echo $i ?></td>
                                                                <td><?php echo $record->fee_setup ?></td>
                                                                <td><?php echo $record->frequency_mode ?></td>
                                                                <td><?php echo $record->amount_calculation_type ?></td>
                                                                <td><?php echo $mainInvoice->currency ?></td>
                                                                <td><?php echo $record->description ?></td>
                                                                <td><?php echo $record->quantity ?></td>
                                                                <td><?php echo $record->price ?></td>
                                                                <td><?php echo $record->amount ?></td>
                                                            </tr>
                                                        <?php
                                                        $total_amount = $total_amount + $record->amount;
                                                        $i++;
                                                            }
                                                        }
                                                        $total_amount = number_format($total_amount, 2, '.', ',');
                                                        ?>
                                                        <tr>
                                                                <td bgcolor="" colspan="7"></td>
                                                                <td bgcolor="" style="text-align:  right;"><b> Total :  </b></td>
                                                                <td bgcolor=""><b><?php echo $total_amount; ?></b></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>


                                        <?php
                                    }
                                    ?>






                                </div>

                            </div>




                        </div>

                    </div>
            
            </div>

        </form>



    <div id="myModal" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <!-- <h4 class="modal-title">Program Landscape</h4> -->
          </div>

          <div class="modal-body">

            <br>



              <div class="form-container">
                <h4 class="form-group-title"> Fee Structure Details</h4>

                
                <div class="row">
                    <div id='view_model'>
                    </div>
                </div>


              </div>
          

          <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>

        </div>
        </div>

      </div>
    </div>


            



        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    function viewStudentFeeBulkPartnerDetails(id_pu_invoice_student_details)
    {
        $.ajax(
            {
               url: '/partner_university/invoice/viewStudentFeeBulkPartnerDetails/'+id_pu_invoice_student_details,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_model").html(result);
                $('#myModal').modal('show');
               }
            });
    }


    $(document).ready(function() {
        $("#form_performa_invoice").validate({
            rules: {
                type_of_invoice: {
                    required: true
                },
                invoice_number: {
                    required: true
                },
                total_amount: {
                    required: true
                },
                id_student: {
                    required: true
                },
                id_application: {
                    required: true
                },
                remarks: {
                    required: true
                },
                status: {
                    required: true
                }
            },
            messages: {
                type_of_invoice: {
                    required: "Select Type Of Invoice",
                },
                invoice_number: {
                    required: "Enter Main Invoice Number",
                },
                total_amount: {
                    required: "Enter Total Amount",
                },
                id_student: {
                    required: "Select Student",
                },
                id_application: {
                    required: "Select Application",
                },
                remarks: {
                    required: "Enter Remarks",
                },
                status: {
                    required: "Status required",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
