
<div class="container-fluid page-wrapper">
   <div class="main-container clearfix">

<form id="form_staff" action="" method="post" enctype="multipart/form-data">

                            <div class="form-container">
                                <h4 class="form-group-title">Faculty Details</h4>

                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Salutation <span class='error-text'>*</span></label>
                                             <select name="salutation" id="salutation" class="form-control">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($salutationList)) {
                                                    foreach ($salutationList as $record) {
                                                ?>
                                                        <option value="<?php echo $record->id;  ?>"
                                                            <?php if($staffDetails->salutation==$record->id)
                                                            {
                                                                echo "selected=selected";
                                                            }
                                                            ?>
                                                            >
                                                            <?php echo $record->name;  ?>        
                                                        </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>First Name <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="first_name" name="first_name" value="<?php echo $staffDetails->first_name;?>">
                                        </div>
                                    </div>


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Last Name <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="last_name" name="last_name" value="<?php echo $staffDetails->last_name;?>">
                                        </div>
                                    </div>

                                      
                                    
                                </div>



                                <div class="row"> 



                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Mobile  Number <span class='error-text'>*</span></label>
                                            <input type="number" class="form-control" id="mobile_number" name="mobile_number" value="<?php echo $staffDetails->mobile_number;?>">
                                        </div>
                                    </div> 
                                   


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Email <span class='error-text'>*</span></label>
                                            <input type="email" class="form-control" id="email" name="email" value="<?php echo $staffDetails->email;?>">
                                        </div>
                                    </div>    

                                  

                                    

                           

                             
                                   <!--  <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Nationality <span class='error-text'>*</span></label>
                                            <select name="nationality" id="nationality" class="form-control">
                                            <option value="">Select</option>
                                            <?php
                                            if (!empty($nationalityList)) {
                                                foreach ($nationalityList as $record) {
                                            ?>
                                                    <option value="<?php echo $record->id;  ?>"
                                                    <?php 
                                                    if($record->id == $staffDetails->nationality)
                                                    {
                                                        echo "selected=selected";
                                                    } ?>
                                                    >
                                                        <?php echo $record->name;  ?>        
                                                    </option>
                                            <?php
                                                }
                                            }
                                            ?>
                                            </select>
                                        </div>
                                    </div> -->



                                    <div class="col-sm-4">
                                      <div class="form-group">
                                         <label>FILE 
                                         <span class='error-text'>*</span>
                                         <?php
                                            if ($staffDetails->image != '')
                                            {
                                            ?>
                                         <a href="<?php echo '/assets/images/' . $staffDetails->image; ?>" target="popup" onclick="window.open(<?php echo '/assets/images/' . $staffDetails->image; ?>)" title="<?php echo $staffDetails->image; ?>"> View </a>
                                         <?php
                                            }
                                            ?>
                                         </label>
                                         <input type="file" name="image" id="image">
                                      </div>
                                    </div>




                                </div>


<div class="row">


                     <div class="col-sm-4">
                        <div class="form-group">
                            <label>Organisation <span class='error-text'>*</span></label>

                             <select name="organisation" id="organisation" class="form-control">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($partnerList)) {
                                                    foreach ($partnerList as $record) {
                                                ?>
                                                        <option value="<?php echo $record->id;  ?>"
                                                            <?php if($staffDetails->organisation==$record->id)
                                                            {
                                                                echo "selected=selected";
                                                            }
                                                            ?>
                                                            >
                                                            <?php echo $record->name;  ?>        
                                                        </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>


                        </div>
                    </div>  

                        <div class="col-sm-4">
                        <div class="form-group">
                            <label>Designation <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="designation" name="designation" value="<?php echo $staffDetails->designation; ?>">
                        </div>
                    </div>  

                    <div class="col-sm-8">
                        <div class="form-group">
                            <label>Education Background <span class='error-text'>*</span></label>

                            <textarea class="form-control z-depth-1" rows="3" placeholder="Short Descripton..." name="education" id="education"><?php echo $staffDetails->education; ?></textarea>

                        </div>
                    </div>  
                </div>
                    
 <div class="row">


                     <div class="col-sm-8">
                        <div class="form-group">
                            <label>About the Facilitatory <span class='error-text'>*</span></label>

                              <textarea class="form-control z-depth-1" rows="3" placeholder="Short Descripton..." name="about_us" id="about_us"><?php echo $staffDetails->about_us; ?></textarea>


                        </div>
                    </div>  

                     
                </div>

                                    
                                </div>


                            </div>

                          



                            <div class="form-container">
                             <h4 class="form-group-title">Other Details</h4>
                             <div class="row">
                                <div class="col-sm-4">
                                  <div class="form-group">
                                      <label>Whatsapp Number <span class='error-text'>*</span></label>
                                      <input type="number" class="form-control" id="whatsapp_number" name="whatsapp_number" value="<?php echo $staffDetails->whatsapp_number; ?>">
                                   </div>
                                </div>

                                 <div class="col-sm-4">
                                  <div class="form-group">
                                      <label>LinkedIn ID/Link: <span class='error-text'></span></label>
                                      <input type="text" class="form-control" id="linked_in" name="linked_in" value="<?php echo $staffDetails->linked_in; ?>">
                                   </div>
                                </div>

                                 <div class="col-sm-4">
                                  <div class="form-group">
                                      <label>Facebook ID/ Link <span class='error-text'></span></label>
                                      <input type="text" class="form-control" id="facebook_id" name="facebook_id" value="<?php echo $staffDetails->facebook_id; ?>">
                                   </div>
                                </div>

                                 <div class="col-sm-4">
                                  <div class="form-group">
                                      <label>Twitter ID/Link: <span class='error-text'></span></label>
                                      <input type="text" class="form-control" id="twitter_id" name="twitter_id" value="<?php echo $staffDetails->twitter_id; ?>">
                                   </div>
                                </div>

                                 <div class="col-sm-4">
                                  <div class="form-group">
                                      <label>Instagram ID/ Link <span class='error-text'></span></label>
                                      <input type="text" class="form-control" id="ig_id" name="ig_id" value="<?php echo $staffDetails->ig_id; ?>">
                                   </div>
                                </div>

                              </div>
                             
                            
                           </div>








                            <div class="button-block clearfix">
                                    <div class="bttn-group">
                                        <button type="submit" class="btn btn-primary btn-lg">Save</button>
                                        <a href="../list" class="btn btn-link">Cancel</a>
                                    </div>
                            </div>


                            




                        </form>


</div>
<footer class="footer-wrapper">
   <p>&copy; 2019 All rights, reserved</p>
</footer>
</div>
</div>


<script src="<?php echo BASE_PATH; ?>assets/ckeditor/ckeditor.js"></script>

<script type="text/javascript">




CKEDITOR.replace('about_us',{
  width: "100%",
  height: "100px"

}); 


CKEDITOR.replace('education',{
  width: "100%",
  height: "100px"

});     
  
  $('select').select2();

   $(function()
   {
       showPartner();
       $(".datepicker").datepicker({
       changeYear: true,
       changeMonth: true,
       });
   });

    getlabel();
    getStateFill();

   function getlabel()
    {
      var labelnric = $("#id_type").val();
      //alert(labelnric);
      $("#labelspanid").html(labelnric);
    }
   
   
   function showPartner(){
       var value = $("#internal_external").val();
       if(value=='Internal') {
            $("#partnerdropdown").hide();
   
       } else if(value=='External') {
            $("#partnerdropdown").show();
   
       }
   }


   function getStateByCountry(id)
    {
     $.get("/af/staff/getStateByCountry/"+id, function(data, status)
     {
        $("#view_state").html(data);
     });
    }
    

    function getStateFill()
    {
        // alert('dasd');

        $.get("/af/staff/getStateByCountry/"+<?php echo $staffDetails->id_country;?>, function(data, status)
        {
            var idstateselected = "<?php echo $staffDetails->id_state;?>";

            $("#view_state").html(data);
            $("#id_state").find('option[value="'+idstateselected+'"]').attr('selected',true);
            $('select').select2();
        });
    }


   $(document).ready(function()
    {

        $("#form_staff").validate(
        {
            rules:
            {
                salutation:
                {
                    required: true
                },
                first_name:
                {
                    required: true
                },
                last_name:
                {
                    required: true
                },
                ic_no:
                {
                    required: true
                },
                gender:
                {
                    required: true
                },
                mobile_number:
                {
                    required: true
                },
                phone_number:
                {
                    required: true
                },
                address:
                {
                    required: true
                },
                address_two:
                {
                    required: true
                },
                id_country:
                {
                    required: true
                },
                id_state:
                {
                    required: true
                },
                zipcode:
                {
                    required: true
                },
                job_type:
                {
                    required: true
                },
                email:
                {
                    required: true
                },
                staff_id:
                {
                    required: true
                },
                dob:
                {
                    required: true
                },
                academic_type:
                {
                    required: true
                },
                id_faculty_program:
                {
                    required: true
                },
                id_education_level:
                {
                    required: true
                },
                id_type:
                {
                    required: true
                },
                status:
                {
                    required: true
                },
                nationality:
                {
                    required: true
                }
            },
            messages:
            {
                salutation:
                {
                    required: "<p class='error-text'>Select Salutation</p>",
                },
                first_name:
                {
                    required: "<p class='error-text'>First Name Required</p>",
                },
                last_name:
                {
                    required: "<p class='error-text'>Last Name Required</p>",
                },
                ic_no:
                {
                    required: "<p class='error-text'>Code Required</p>",
                },
                gender:
                {
                    required: "<p class='error-text'>Select Gender</p>",
                },
                mobile_number:
                {
                    required: "<p class='error-text'>Mobile Number Required</p>",
                },
                phone_number:
                {
                    required: "<p class='error-text'>Phone Number Required</p>",
                },
                address:
                {
                    required: "<p class='error-text'>Faculty Address Required</p>",
                },
                address_two:
                {
                    required: "<p class='error-text'>Address 2 Required</p>",
                },
                id_country:
                {
                    required: "<p class='error-text'>Select Country</p>",
                },
                id_state:
                {
                    required: "<p class='error-text'>Select State</p>",
                },
                zipcode:
                {
                    required: "<p class='error-text'>Enter Zipcode</p>",
                },
                job_type:
                {
                    required: "<p class='error-text'>Select Job Type</p>",
                },
                email:
                {
                    required: "<p class='error-text'>Email Required</p>",
                },
                staff_id:
                {
                    required: "<p class='error-text'>Enter Faculty ID</p>",
                },
                dob:
                {
                    required: "<p class='error-text'>Select Date Of Birth</p>",
                },
                academic_type:
                {
                    required: "<p class='error-text'>Select Academic Type</p>",
                },
                id_faculty_program:
                {
                    required: "<p class='error-text'>Select Faculty Program</p>",
                },
                id_education_level:
                {
                    required: "<p class='error-text'>Select Highest Education Level</p>",
                },
                id_type:
                {
                    required: "<p class='error-text'>Select ID Type</p>",
                },
                status:
                {
                    required: "<p class='error-text'>Select Status</p>",
                },
                nationality:
                {
                    required: "<p class='error-text'>Select Nationality</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
   

</script>

<style type="text/css">
   .shadow-textarea textarea.form-control::placeholder {
   font-weight: 300;
   }
   .shadow-textarea textarea.form-control {
   padding-left: 0.8rem;
   }
</style>
<script src="<?php echo BASE_PATH; ?>assets/ckeditor/ckeditor.js"></script>

