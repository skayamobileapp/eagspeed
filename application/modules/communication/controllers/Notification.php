<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Notification extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('notification_model');
        $this->isLoggedIn();
    }

    function list()
    {

        if ($this->checkAccess('notification.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            // $formData['name'] = '';
            // $formData['type'] = '';
            // $formData['id_template'] = '';


            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['type'] = $this->security->xss_clean($this->input->post('type'));

            $data['searchParam'] = $formData;

            $data['notificationList'] = $this->notification_model->notificationListSearch($formData);
            // echo "<Pre>"; print_r($data);die();
            $this->global['pageTitle'] = 'Campus Management System : Notification List';
            $this->loadViews("notification/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('notification.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $type = $this->security->xss_clean($this->input->post('type'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'name' => $name,
                    'type' => $type,
                    'description' => $description,
                    'start_date' => date('Y-m-d H:i:s', strtotime($start_date)),
                    'end_date' => date('Y-m-d H:i:s', strtotime($end_date)),
                    'status' => $status,
                    'created_by' => $user_id
                );
                $result = $this->notification_model->addNewNotification($data);
                redirect('/communication/notification/list');
            }


            $this->global['pageTitle'] = 'Campus Management System : Add Notification';
            $this->loadViews("notification/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('notification.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/communication/notification/list');
            }
            
            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {

                // echo "<Pre>"; print_r($_POST);exit;
                
                $name = $this->security->xss_clean($this->input->post('name'));
                $type = $this->security->xss_clean($this->input->post('type'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'name' => $name,
                    'type' => $type,
                    'description' => $description,
                    'start_date' => date('Y-m-d H:i:s', strtotime($start_date)),
                    'end_date' => date('Y-m-d H:i:s', strtotime($end_date)),
                    'status' => $status,
                    'updated_by' => $user_id,
                    'updated_dt_tm' => date('Y-m-d H:i:s')
                );

                $result = $this->notification_model->editNotification($data,$id);
                redirect('/communication/notification/list');
            }

            $data['id_notification'] = $id;
            $data['notification'] = $this->notification_model->getNotification($id);

            $this->global['pageTitle'] = 'Campus Management System : Edit Notification';
            $this->loadViews("notification/edit", $this->global, $data, NULL);
        }
    }

    function students($id = NULL)
    {
        if ($this->checkAccess('notification.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/communication/notification/list');
            }
            
            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {
                // echo "<Pre>"; print_r($_POST);exit;

                $id_students = $this->security->xss_clean($this->input->post('id_students'));

                foreach ($id_students as $id_student)
                {  

                    // echo "<Pre>"; print_r($id_student);exit;
                    $data = array(
                        'id_notification' => $id,
                        'id_student' => $id_student,
                        'status' => 1,
                        'created_by' => $user_id
                    );

                    $result = $this->notification_model->addNotificationStudent($data);
                }
                redirect('/communication/notification/students/'.$id);
            }

            $data['id_notification'] = $id;
            $data['notification'] = $this->notification_model->getNotification($id);
            $data['notificationHasStudents'] = $this->notification_model->getNotificationHasStudents($id);
            $data['programmeList'] = $this->notification_model->programmeListByStatus('');
            
            // echo "<Pre>"; print_r($data['notificationHasStudents']);exit;

            $this->global['pageTitle'] = 'Campus Management System : Add Students';
            $this->loadViews("notification/students", $this->global, $data, NULL);
        }
    }

    function searchStudents()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        // $staffList = $this->student_course_registration_model->staffListByStatus('1');
        
        $student_data = $this->notification_model->studentSearch($tempData);

        // echo "<Pre>";print_r($student_data);exit();
        if(!empty($student_data))
        {


         $table = "

         <script type='text/javascript'>
             $('select').select2();
         </script>";


         $table .= "
         <br>
         <table  class='table' id='list-table'>
                  <tr>
                    <th>Sl. No</th>
                    <th>Student Name</th>
                    <th>Student NRIC/PASSPORT</th>
                    <th>Email</th>
                    <th>Programme</th>
                    <th>Phone</th>
                    <th style='text-align: center;'><input type='checkbox' id='checkAll' name='checkAll' onclick='checkAllCheckBoxes()'> Check All</th>
                </tr>";

            for($i=0;$i<count($student_data);$i++)
            {
                $id = $student_data[$i]->id;
                $full_name = $student_data[$i]->full_name;
                $nric = $student_data[$i]->nric;
                $email_id = $student_data[$i]->email_id;
                $phone = $student_data[$i]->phone;
                $programme_code = $student_data[$i]->programme_code;
                $programme_name = $student_data[$i]->programme_name;
                $nationality = $student_data[$i]->nationality;

                $j = $i+1;
                
                $table .= "
                <tr>
                    <td>$j</td>
                    <td>$full_name</td>                        
                    <td>$nric</td>
                    <td>$email_id</td>                      
                    <td>$programme_code - $programme_name</td>
                    <td>$phone</td>           
                    
                    <td class='text-center'>
                        <input type='checkbox' id='id_students[]' name='id_students[]' class='check' value='".$id."'>
                    </td>
               
                </tr>";
            }

         $table.= "</table>";
        }
        else
        {
            $table= "<h4> No Data Found For Your Search</h4>";
        }
        echo $table;exit;
    }

    function deleteNotificationHasStudents($id)
    {
        $student_data = $this->notification_model->deleteNotificationHasStudents($id);
        echo 1;exit;
    }

    function sendMail()
    {
        $data = $this->security->xss_clean($this->input->post('data'));
        // echo "<Pre>";print_r($data);exit();
        $type = $data['type'];
        $id_notification = $data['id_notification'];


        switch ($type)
        {
            case 'Applicant':

                $recepient_list = $this->notification_model->getNotificationRecepientsByApplicantEmail($id_notification);
                break;

            case 'Student':
                $recepient_list = $this->notification_model->getNotificationRecepientsByStudentEmail($id_notification);
                break;

            case 'Staff':
                $recepient_list = $this->notification_model->getNotificationRecepientsByStaffEmail($id_notification);
                break;
            
            default:
                break;
        }


        foreach ($recepient_list as $recepients)
        {
            $to = $recepients->email;
        // echo "<Pre>";print_r($to);exit();
        
         // $to = "vinayp007@yahoo.com";
         $subject = "This is subject";
         
         $message = "<b>This is HTML message.</b>";
         $message .= "<h1>This is headline.</h1>";
         
         $header = "From:vinayp007@yahoo.com \r\n";
         $header .= "Cc:afgh@somedomain.com \r\n";
         $header .= "MIME-Version: 1.0\r\n";
         $header .= "Content-type: text/html\r\n";
         
         $retval = mail ($to,$subject,$message,$header);
         
             if( $retval == true )
             {
                $msz = "Message sent successfully...";
             }
             else
             {
                $msz = "Message could not be sent...";
             }
            
        }

        echo $msz;exit();
        
    }
}
