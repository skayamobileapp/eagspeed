<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Assign Recepients To Group</h3>
        </div>
        <form id="form_sponser" action="" method="post">



            <div class="form-container">
                <h4 class="form-group-title">Group Details</h4>         
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Name <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="name" name="name" value="<?php echo $group->name;?>" readonly>
                            <input type="hidden" class="form-control" id="type" name="type" value="<?php echo $group->type;?>">
                        </div>
                    </div>

     

                   <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" <?php if($group->status=='1') {
                                 echo "checked=checked";
                              };?> disabled><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0" <?php if($group->status=='0') {
                                 echo "checked=checked";
                              };?> disabled>
                              <span class="check-radio"></span> In-Active
                            </label>                              
                        </div>                         
                    </div>

                </div>
            </div>

            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="../recepientList" class="btn btn-link">Back</a>
                </div>
            </div>
        



         <div class="form-container">
            <h4 class="form-group-title"> Add Recepients</h4>          
            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>
            <div class="clearfix">
                <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                    <li role="presentation" class="active" ><a href="#invoice" class="nav-link border rounded text-center"
                            aria-controls="invoice" aria-selected="true"
                            role="tab" data-toggle="tab">Search</a>
                    </li>
                    <li role="presentation"><a href="#receipt" class="nav-link border rounded text-center"
                            aria-controls="receipt" role="tab" data-toggle="tab">Recepient List</a>
                    </li>
                    
                </ul>

                
                <div class="tab-content offers-tab-content">

                    <div role="tabpanel" class="tab-pane active" id="invoice">
                        <div class="col-12 mt-4">




                        <div class="form-container">
                            <h4 class="form-group-title">Search</h4>



                            

                                <div class="row" >

                                        <div class="col-sm-6">
                                           <div class="form-group">
                                                <label class="col-sm-4 control-label">Course Type</label>
                                                <div class="col-sm-8">
                                                   <select name="programme_type_id" id="programme_type_id" class="form-control">
                                                    <option value="">Select</option>
                                                    <?php
                                                    if (!empty($programmeTypeList)) {
                                                      foreach ($programmeTypeList as $record)
                                                      {
                                                        $selected = '';
                                                        if ($record->id == $searchParam['programme_type_id']) {
                                                          $selected = 'selected';
                                                        }
                                                    ?>
                                                        <option value="<?php echo $record->id;  ?>"
                                                          <?php echo $selected;  ?>>
                                                          <?php echo  $record->name;  ?>
                                                          </option>
                                                    <?php
                                                      }
                                                    }
                                                    ?>
                                                  </select>
                                                </div>
                                              </div>
                                        </div>

                                        <div class="col-sm-6">
                                             <div class="form-group">
                    <label class="col-sm-4 control-label">Course Provider</label>
                    <div class="col-sm-8">
                      <select name="partner_university_id" id="partner_university_id" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($organisationList)) {
                          foreach ($organisationList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['partner_university_id']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                    <label class="col-sm-4 control-label">Course Name </label>
                    <div class="col-sm-8">
                      <select name="id_programme" id="id_programme" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($programmeList)) {
                          foreach ($programmeList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['id_programme']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                                        </div>
                                        <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Student Name</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="name" value="">
                    </div>
                  </div>
                </div>


                                </div>

                              
                              


                            <div>
                                <table border="0px" style="width: 100%">
                                    <tr>
                                        <td style="text-align: center;" colspan="6">
                                            <button type="button" id="btn_add_detail" onclick="showstudentlist()" class="btn btn-primary btn-light btn-lg">Search</button>
                                        </td>
                                    </tr>

                                </table>
                                <br>
                            </div>


                            </div> 

                           <form action="" method="post">
                            
                            <div class="form-container" id="view_visible" style="display: none;">
                                <h4 class="form-group-title">Search Result</h4>

                                <div id="view">
                                </div>

                                 <div>
                                <table border="0px" style="width: 100%">
                                    <tr>
                                        <td style="text-align: center;" colspan="6">
                                            <button type="submit" id="btn_add_detail" class="btn btn-primary btn-light btn-lg">Add</button>
                                        </td>
                                    </tr>

                                </table>
                                <br>
                            </div>
                        </div>




                           


                        </div> 
                    </div>










                    <div role="tabpanel" class="tab-pane" id="receipt">
                        <div class="mt-4">

                            <div class="custom-table" id="printReceipt">
                                <table class="table" id="list-table">
                                    <thead>
                                    <tr>
                                        <th>Sl. no</th>
                                        <th><?php echo $group->type; ?> Name</th>
                                        <th>Email</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    if (!empty($groupRecepientsList))
                                    {
                                        $j=1;
                                        foreach ($groupRecepientsList as $record)
                                        {
                                    ?>
                                        <tr>
                                            <td><?php echo $j ?></td>
                                            <td><?php echo $record->nric . " - " . $record->full_name ?></td>
                                            <td><?php echo $record->email_id ?></td>
                                           
                                            <td class="">
                                            <a onclick="deleteStaffRecepient(<?php echo $record->id ?>)">Delete</a>
                                            </td>
                                        </tr>
                                    <?php
                                        $j++;
                                        }
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>






                        </div>
                    </div>
                </div>

            </div>

            


        </div>  

        </form>




        



        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

     function showstudentlist()
    // $("button").click(function()
    {
        getStudentListByType();
    }


    function getStaffList()
    {
        var tempPR = {};
        tempPR['type'] = $("#type").val();
        tempPR['staff_name'] = $("#staff_name").val();
        tempPR['staff_ic_no'] = $("#staff_ic_no").val();

        $.ajax(
        {
           url: '/communication/group/getStaffList',
           type: 'POST',
           data:
           {
            data: tempPR
           },
           error: function()
           {
            alert('Something is wrong');
           },
           success: function(result)
           {
            // alert(result);
             //location.reload();
             $("#view").html(result);
             $("#view_visible").show();
           }
        });
    }



    function getStudentListByType()
    {
        var tempPR = {};
        tempPR['programme_type_id'] = $("#programme_type_id").val();
        tempPR['partner_university_id'] = $("#partner_university_id").val();
        tempPR['id_programme'] = $("#id_programme").val();

        $.ajax(
        {
           url: '/communication/group/getStudentListByType',
           type: 'POST',
           data:
           {
            data: tempPR
           },
           error: function()
           {
            alert('Something is wrong');
           },
           success: function(result)
           {
             //location.reload();
             $("#view").html(result);
             $("#view_visible").show();
           }
        });
    }



     function deleteStaffRecepient(id)
    {

        $.get("/communication/group/deleteStaffRecepient/"+id,
            function(data, status)
            {
                window.location.reload();
            });
    }


    function sendMail()
    {
        var tempPR = {};
        tempPR['id_group'] = <?php echo $group->id ?>;
        tempPR['type'] = $("#type").val();
        // alert(tempPR);
        $.ajax(
        {
           url: '/communication/group/sendMail',
           type: 'POST',
           data:
           {
            data: tempPR
           },
           error: function()
           {
            alert('Something is wrong');
           },
           success: function(result)
           {
             alert(result);
           }
        });
    }





    
    $('select').select2();

    $(document).ready(function() {
        $("#form_sponser").validate({
            rules: {
                name: {
                    required: true
                },
                type: {
                    required: true
                },
                id_template: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                type: {
                    required: "<p class='error-text'>Select Type</p>",
                },
                id_template: {
                    required: "<p class='error-text'>Select Template</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>