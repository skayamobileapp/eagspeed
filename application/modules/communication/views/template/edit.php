<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Template</h3>
        </div>
        <form id="form_sponser" action="" method="post">
            <div class="form-container">
                <h4 class="form-group-title">Template Details</h4>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Name <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="name" name="name" value="<?php echo $template->name;?>">
                        </div>
                    </div>

                  

                 </div>



              


                 <div class="row custom-table">
                    <h3>Table Variables</h3>
                     <div class="col-sm-12 custom-table">
                        <table class="table">
                            <tr>
                                 <th>Text</th>
                                 <th>Variables</th>
                                 <th>Text</th>
                                 <th>Variables</th>
                            </tr>
                            <tr>
                                 <td>Student NAme</td>
                                 <td>@studentname</td>
                                 <td>Program</td>
                                 <td>@program</td>
                            </tr>
                           
                               <tr>
                                 <td>Page Break</td>
                                 <td><?php echo htmlspecialchars(" <pagebreak/> ");?></td>
                            </tr>


                        </table>
                     </div>
                </div>


                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group shadow-textarea">
                            <label for="message">Description <span class='error-text'>*</span></label>
                            <textarea class="form-control z-depth-1" rows="3" placeholder="Write Description..." name="message" id="message"><?php echo $template->message;?></textarea>
                        </div>
                    </div>

                </div>
                
                

                <div class="row">

                   <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" <?php if($template->status=='1') {
                                 echo "checked=checked";
                              };?>><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0" <?php if($template->status=='0') {
                                 echo "checked=checked";
                              };?>>
                              <span class="check-radio"></span> In-Active
                            </label>                              
                        </div>                         
                    </div>

                </div>
            </div>

            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="../list" class="btn btn-link">Cancel</a>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script src="<?php echo BASE_PATH; ?>assets/ckeditor/ckeditor.js"></script>


<script type="text/javascript">

// Initialize CKEditor

CKEDITOR.replace('message',{

  width: "100%",
  height: "200px"

}); 

</script>

<script>

    $('select').select2();

    $(document).ready(function() {
        $("#form_sponser").validate({
            rules: {
                name: {
                    required: true
                },
                subject: {
                    required: true
                },
                message: {
                    required: true
                },
                status: {
                    required: true
                },
                id_university: {
                    required: true
                },
                id_education_level: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                subject: {
                    required: "<p class='error-text'>Subject Required</p>",
                },
                message: {
                    required: "<p class='error-text'>Description Required</p>",
                },
                status: {
                    required: "<p class='error-text'>Select Status</p>",
                },
                id_university: {
                    required: "<p class='error-text'>Select University</p>",
                },
                id_education_level: {
                    required: "<p class='error-text'>Select Education Level</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
