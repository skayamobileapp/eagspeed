<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Event</h3>
        </div>
        <form id="form_sponser" action="" method="post">

            <div class="form-container">
                <h4 class="form-group-title">Event Details</h4>        
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-events">
                            <label>Name <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="name" name="name" value="<?php echo $events->name;?>">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-events">
                            <label>Type <span class='error-text'>*</span></label>
                            <select id="type" name="type" class="form-control" style="width: 405px" disabled>
                                <option value="">SELECT</option>
                                <option value="Applicant" <?php if($events->type=='Applicant'){ echo "selected"; } ?>>Applicant</option>
                                <option value="Student" <?php if($events->type=='Student'){ echo "selected"; } ?>>Student</option>
                                <option value="Staff" <?php if($events->type=='Staff'){ echo "selected"; } ?>>Staff</option>
                            </select>
                        </div>
                    </div>


                </div>


                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group shadow-textarea">
                            <label for="description">Description <span class='error-text'>*</span></label>
                            <textarea class="form-control z-depth-1" rows="3" placeholder="Write Description..." name="description" id="description"><?php echo $events->description;?></textarea>
                        </div>
                    </div>

                </div>



                <div class="row">


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Start Date <span class='error-text'>*</span></label>
                            <input type="text" class="form-control datepicker" id="start_date" name="start_date" value="<?php echo date('d-m-Y', strtotime($events->start_date)); ?>">
                        </div>
                    </div>



                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>End Date <span class='error-text'>*</span></label>
                            <input type="text" class="form-control datepicker" id="end_date" name="end_date" value="<?php echo date('d-m-Y', strtotime($events->start_date)); ?>">
                        </div>
                    </div>



                   <div class="col-sm-4">
                        <div class="form-events">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" <?php if($events->status=='1') {
                                 echo "checked=checked";
                              };?>><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0" <?php if($events->status=='0') {
                                 echo "checked=checked";
                              };?>>
                              <span class="check-radio"></span> In-Active
                            </label>                              
                        </div>                         
                    </div>

                </div>
            </div>

            <div class="button-block clearfix">
                <div class="bttn-events">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="../list" class="btn btn-link">Back</a>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    
    $('select').select2();



    $( function()
    {
        $( ".datepicker" ).datepicker(
        {
            changeYear: true,
            changeMonth: true,
            yearRange: "1960:2001"
        });
    });



     $(document).ready(function() {
        $("#form_sponser").validate({
            rules: {
                name: {
                    required: true
                },
                type: {
                    required: true
                },
                description: {
                    required: true
                },
                start_date: {
                    required: true
                },
                end_date: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                type: {
                    required: "<p class='error-text'>Type Required</p>",
                },
                description: {
                    required: "<p class='error-text'>Description Required</p>",
                },
                start_date: {
                    required: "<p class='error-text'>Select Start Date</p>",
                },
                end_date: {
                    required: "<p class='error-text'>Select End Date</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
