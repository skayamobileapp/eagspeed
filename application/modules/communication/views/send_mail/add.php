<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Send Communication Mail</h3>
        </div>
        <form id="form_sponser_has_students" action="" method="post">
<!-- 
        <div class="page-title clearfix">
            <h4>Sponser Details</h4>
        </div> -->


        <div class="form-container">
            <h4 class="form-group-title">Mail Details</h4>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                      <label>Group </label>
                        <select name="type" id="type" class="form-control">
                            <option value="">Select</option>
                            <option value="Applicant">Applicant</option>
                            <option value="Student">Student</option>
                            <option value="Staff">Staff</option>
                        </select>
                    </div>
                </div>
                  <div class="col-sm-4">
                    <div class="form-group">
                      <label>Subject </label>
                        <input type='text' name='subject' id='subject' class="form-control">
                    </div>
                </div>




                <div class="col-sm-4">
                    <div class="form-group">
                      <label>Template </label>
                        <select name="id_template" id="id_template" class="form-control" onchange="showdescription(this.value)">
                            <option value="">Select</option>
                            <?php
                            if (!empty($templateList))
                            {
                                foreach ($templateList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                 <div class="col-sm-4">
                    <div class="form-group">
                      <label>Schedule Date and Time </label>
                        <input type='text' name='schedule_date_time' id='schedule_date_time' class="form-control">
                    </div>
                </div>

                                <div class="row">


                    <div class="col-sm-12">
                        <div class="form-group shadow-textarea">
                          <label for="message">Description <span class='error-text'>*</span></label>
                          <textarea class="form-control z-depth-1" rows="3" placeholder="Write Description..." name="message" id="message"></textarea>
                        </div>
                    </div>

                </div>

                
            </div>
        </div>


  



        <div class="form-container" id="view_visible" style="display: none;">
            <h4 class="form-group-title">Recepients Details</h4>

            <div id="view">
            </div>

        </div>




     


        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Send Mail</button>
            </div>
        </div>


        </form>

        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>

 <script src="<?php echo BASE_PATH; ?>assets/ckeditor/ckeditor.js"></script>


<script type="text/javascript">

// Initialize CKEditor

CKEDITOR.replace('message',{

  width: "100%",
  height: "200px"

}); 

function showdescription(valuedata){


    $.get("/communication/sendMail/getdescription/"+valuedata, function(data, status){
       
                CKEDITOR.instances['message'].setData(data);

        });



}

</script>
</script>
