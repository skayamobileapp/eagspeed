<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">


        <ul class="page-nav-links">
            <li><a href="/communication/notification/edit/<?php echo $id_notification;?>">Edit Notification</a></li>

            <?php
            if($notification->type == 'Particular Student')
            {
            ?>
                
            <li class="active"><a href="/communication/notification/students/<?php echo $id_notification;?>">Student List</a></li>

            <?php
            }
            ?>
        </ul>


        <form id="form_unit" action="" method="post">


        <div class="form-container">
            <h4 class="form-group-title">Search Student For Programme Registration</h4>
            <h4 >Search Student For Notification Tagging</h4>


            <div class="row">


                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Student Name</label>
                        <input type="text" class="form-control" id="full_name" name="full_name">
                    </div>
                </div>


                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Student Email</label>
                        <input type="text" class="form-control" id="email_id" name="email_id">
                    </div>
                </div>


                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Programme </label>
                        <select name="id_programme" id="id_programme" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($programmeList))
                            {
                                foreach ($programmeList as $record)
                                {?>
                                    <option value="<?php echo $record->id;?>"
                                    ><?php echo $record->code ." - ".$record->name; ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                
            </div>


            <div class="modal-footer">
                <button type="button" class="btn btn-default" onclick="searchStudents()">Search</button>
              </div>

        </div>


        <div class="form-container" style="display: none;" id="view_student_display">
            <h4 class="form-group-title">Notification Tagging For Student</h4>


            <div  id='view_student'>
            </div>

        </div>



        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="../list" class="btn btn-link">Back</a>
            </div>
        </div>

    </form>

    <?php

      if(!empty($notificationHasStudents))
      {
          ?>

          <br>

          <div class="form-container">
                  <h4 class="form-group-title">Student List</h4>

              

                <div class="custom-table">
                  <table class="table">
                      <thead>
                            <tr>
                            <th>Sl. No</th>
                            <th>Student Name</th>
                            <th>Student NRIC/PASSPORT</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <!-- <th>Nationality</th> -->
                           <th style="text-align: center;">Action</th>
                          </tr>
                      </thead>
                      <tbody>
                           <?php
                       $total = 0;
                        for($i=0;$i<count($notificationHasStudents);$i++)
                       { ?>
                          <tr>
                          <td><?php echo $i+1;?></td>
                          <td><?php echo $notificationHasStudents[$i]->full_name;?></td>
                          <td><?php echo $notificationHasStudents[$i]->nric ;?></td>
                          <td><?php echo $notificationHasStudents[$i]->email_id ;?></td>
                          <td><?php echo $notificationHasStudents[$i]->phone ;?></td>
                          <td style="text-align: center;">
                            <a onclick="deleteNotificationHasStudents(<?php echo $notificationHasStudents[$i]->id; ?>)">Delete</a>
                          </td>

                          </tr>
                        <?php 
                    } 
                    ?>
                      </tbody>
                  </table>
                </div>

              </div>


      <?php
      
      }
       ?>





        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    
    $('select').select2();

    $(function()
    {
       $(".datepicker").datepicker({
       changeYear: true,
       changeMonth: true,
       });
    });


    function searchStudents()
    {
        var tempPR = {};
        tempPR['id_notification'] = <?php echo $id_notification; ?>;
        tempPR['id_programme'] = $("#id_programme").val();
        tempPR['full_name'] = $("#full_name").val();
        tempPR['email_id'] = $("#email_id").val();
            $.ajax(
            {
               url: '/communication/notification/searchStudents',
               type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_student_display").show();
                $("#view_student").html(result);
               }
            });
    }



    function deleteNotificationHasStudents(id)
    {
      var cnf= confirm('Do you really want to delete?');
      if(cnf==true)
      {

        $.ajax(
            {
               url: '/communication/notification/deleteNotificationHasStudents/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                  window.location.reload();
               }
            });
      }
    }




    $(document).ready(function() {
        $("#form_sponser").validate({
            rules: {
                name: {
                    required: true
                },
                type: {
                    required: true
                },
                id_template: {
                    required: true
                },
                start_date: {
                    required: true
                },
                end_date: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                type: {
                    required: "<p class='error-text'>Select Type</p>",
                },
                id_template: {
                    required: "<p class='error-text'>Select Template</p>",
                },
                start_date: {
                    required: "<p class='error-text'>Select Start Date</p>",
                },
                end_date: {
                    required: "<p class='error-text'>Select End Date</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
