<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">


        <ul class="page-nav-links">
            <li class="active"><a href="/communication/notification/edit/<?php echo $id_notification;?>">Edit Notification</a></li>

            <?php
            if($notification->type == 'Particular Student')
            {
            ?>
                
            <li><a href="/communication/notification/students/<?php echo $id_notification;?>">Student List</a></li>

            <?php
            }
            ?>
        </ul>


        <form id="form_sponser" action="" method="post">

            <div class="form-container">
                <h4 class="form-group-title">Notification Details</h4>        
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-notification">
                            <label>Name <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="name" name="name" value="<?php echo $notification->name;?>">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-notification">
                            <label>Type <span class='error-text'>*</span></label>
                            <select id="type" name="type" class="form-control">
                                <option value="">SELECT</option>
                                <option value="All Student" <?php if($notification->type=='All Student'){ echo "selected"; } ?>>All Student</option>
                                <option value="Particular Student" <?php if($notification->type=='Particular Student'){ echo "selected"; } ?>>Particular Student</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                          <label>Start Date <span class='error-text'>*</span></label>
                            <input type="text" class="form-control datepicker" id="start_date" name="start_date" value="<?php echo date('d-m-Y', strtotime($notification->start_date)); ?>" autocomplete="off">
                        </div>
                    </div>


                </div>


                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                          <label>End Date <span class='error-text'>*</span></label>
                            <input type="text" class="form-control datepicker" id="end_date" name="end_date" value="<?php echo date('d-m-Y', strtotime($notification->end_date)); ?>" autocomplete="off">
                        </div>
                    </div>


                   <div class="col-sm-4">
                        <div class="form-notification">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" <?php if($notification->status=='1') {
                                 echo "checked=checked";
                              };?>><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0" <?php if($notification->status=='0') {
                                 echo "checked=checked";
                              };?>>
                              <span class="check-radio"></span> In-Active
                            </label>                              
                        </div>                         
                    </div>

                </div>


                <div class="row">
                    
                    <div class="col-sm-12">
                        <div class="form-group shadow-textarea">
                            <label for="description">Description <span class='error-text'>*</span></label>
                            <textarea class="form-control z-depth-1" rows="3" placeholder="Write Description..." name="description" id="description"><?php echo $notification->description;?></textarea>
                        </div>
                    </div>

                </div>

            </div>

            <div class="button-block clearfix">
                <div class="bttn-notification">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="../list" class="btn btn-link">Back</a>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    
    $('select').select2();

    $(function()
    {
       $(".datepicker").datepicker({
       changeYear: true,
       changeMonth: true,
       });
    });

    $(document).ready(function() {
        $("#form_sponser").validate({
            rules: {
                name: {
                    required: true
                },
                type: {
                    required: true
                },
                id_template: {
                    required: true
                },
                start_date: {
                    required: true
                },
                end_date: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                type: {
                    required: "<p class='error-text'>Select Type</p>",
                },
                id_template: {
                    required: "<p class='error-text'>Select Template</p>",
                },
                start_date: {
                    required: "<p class='error-text'>Select Start Date</p>",
                },
                end_date: {
                    required: "<p class='error-text'>Select End Date</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
