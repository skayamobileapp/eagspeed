<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Notification_model extends CI_Model
{
    function notificationList()
    {
        $this->db->select('sp.*');
        $this->db->from('notification as sp');
        $this->db->order_by("sp.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         //echo "<Pre>"; print_r($result);exit;
         return $result;
    }

    function notificationListSearch($data)
    {
        $this->db->select('sp.*');
        $this->db->from('notification as sp');
        if ($data['name'] != '')
        {
            $likeCriteria = "(sp.name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['type'] != '')
        {
            $this->db->where('sp.type', $data['type']);
        }
        $this->db->order_by("sp.name", "ASC");
         $query = $this->db->get();
         $results = $query->result();  
         // echo "<Pre>"; print_r($recepients);exit;
         return $results;
    }

    function getNotification($id)
    {
        $this->db->select('*');
        $this->db->from('notification');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewNotification($data)
    {
        $this->db->trans_start();
        $this->db->insert('notification', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editNotification($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('notification', $data);
        return TRUE;
    }

     function deleteNotification($id)
    { 
        // echo "<Pre>";  print_r($id);exit;
       $this->db->where('id', $id);
       $this->db->delete('notification_recepients');
       return TRUE;
    }

    function deleteNotificationHasStudents($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('notification_has_students');
        return TRUE;
    }

    function studentSearch($data)
    {
        $this->db->select('DISTINCT(shp.id_student) as id, s.*, p.code as programme_code, p.name as programme_name, n.name as nationality');
        $this->db->from('student_has_programme as shp');
        $this->db->join('student as s', 'shp.id_student = s.id');
        $this->db->join('programme as p', 'shp.id_programme = p.id','left');
        $this->db->join('nationality as n', 's.nationality = n.id','left');
        if ($data['full_name'] != '')
        {
            $likeCriteria = "(s.full_name  LIKE '%" . $data['full_name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['email_id'] != '')
        {
            $this->db->where('s.email_id', $data['email_id']);
        }
        if ($data['id_programme'] != '')
        {
            $this->db->where('shp.id_programme', $data['id_programme']);
        }

        $likeCriteria = " s.id NOT IN (SELECT nhs.id_student FROM notification_has_students nhs where nhs.id_notification = " . $data['id_notification'] . ")";
        $this->db->where($likeCriteria); 

        $this->db->where('s.applicant_status !=', 'Graduated');
        $this->db->where('s.id_company', '0');
        $this->db->where('shp.status', '1');
        $query = $this->db->get();
        $result = $query->result(); 

        return $result;
    }

    function addNotificationStudent($data)
    {
        $this->db->trans_start();
        $this->db->insert('notification_has_students', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function programmeListByStatus($status)
    {
        $this->db->select('p.*');
        $this->db->from('programme as p');
        $this->db->where('p.status', $status);
        $query = $this->db->get();
        return $query->result();
    }

    function getNotificationHasStudents($id)
    {
        $this->db->select('DISTINCT(nhs.id_student) as id_student, nhs.id, s.full_name, s.email_id, s.nric, s.phone');
        $this->db->from('notification_has_students as nhs');
        $this->db->join('student as s', 'nhs.id_student = s.id');
        $this->db->where('nhs.id_notification', $id);
        $query = $this->db->get();
        return $query->result();
    }
}