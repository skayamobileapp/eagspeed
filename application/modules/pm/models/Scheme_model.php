<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Scheme_model extends CI_Model
{
    function schemeList()
    {
        $this->db->select('*');
        $this->db->from('scheme');
        $this->db->order_by("description", "DESC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function schemeListSearch($search)
    {
        $this->db->select('*');
        $this->db->from('scheme');
        if (!empty($search))
        {
            $likeCriteria = "(description  LIKE '%" . $search . "%' or description_in_optional_language  LIKE '%" . $search . "%' or code  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("description", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getScheme($id)
    {
        $this->db->select('*');
        $this->db->from('scheme');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewScheme($data)
    {
        $this->db->trans_start();
        $this->db->insert('scheme', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editScheme($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('scheme', $data);
        return TRUE;
    }

    function schemeHasNationality($id_scheme)
    {
        $this->db->select('shn.*, n.name as nationality');
        $this->db->from('scheme_has_nationality as shn');
        $this->db->join('nationality as n','shn.id_nationality = n.id');
        $this->db->where('shn.id_scheme', $id_scheme);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function addSchemeHasNationality($data)
    {
        $this->db->trans_start();
        $this->db->insert('scheme_has_nationality', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function deleteSchemeHasNationality($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('scheme_has_nationality');
        return TRUE;
    }

    function nationalityListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('nationality');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }
}