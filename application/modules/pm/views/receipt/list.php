<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List Partner University Invoice</h3>
      <!-- <a href="add" class="btn btn-primary">+ Add Invoice</a> -->
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">

              <div class="row">

                  <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Invoice Number</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="invoice_number" value="<?php echo $searchParam['invoice_number']; ?>">
                    </div>
                  </div>
                </div>

                  <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Receipt Number</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="invoice_number" value="<?php echo $searchParam['invoice_number']; ?>">
                    </div>
                  </div>
                </div>


              </div>



              </div>

              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href="partnerUniversityInvoiceList" class="btn btn-link" >Clear All Fields</a>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <th>Sl. No</th>
            <th>Invoice Number</th>
            <th>Partner University</th>
            <th>Amount</th>
            <th>Start Date</th>
            <th>End Date</th>
            <th class="text-center">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($mainInvoiceList)) {
            $i=1;
            foreach ($mainInvoiceList as $record)
            {
              
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->invoice_number ?></td>
                <td><?php echo $record->name ?></td>
               
                <td><?php echo $record->amount ?></td>
                <td><?php echo date('d-m-Y',strtotime($record->start_date)) ?></td>
                <td><?php echo date('d-m-Y',strtotime($record->end_date)) ?></td>
                
                <td class="text-center">



                  <a href="<?php echo 'edit/' . $record->id; ?>" title="View">View</a>
                  
                </td>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script type="text/javascript">
  
  $('select').select2();

  function clearSearchForm()
      {
        window.location.reload();
      }
</script>