<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">

        <div class="page-title clearfix">
            <h3>Edit Program </h3>
        </div>





            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>
            <div class="clearfix">

                <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                    <li role="presentation" class="active" ><a href="#program_detail" class="nav-link border rounded text-center"
                            aria-controls="program_detail" aria-selected="true"
                            role="tab" data-toggle="tab">Program Details</a>
                    </li>

                    <li role="presentation"><a href="#program_mode" class="nav-link border rounded text-center"
                            aria-controls="program_mode" role="tab" data-toggle="tab">Learning Mode</a>
                    </li>

                    <li role="presentation"><a href="#program_scheme" class="nav-link border rounded text-center"
                            aria-controls="program_scheme" role="tab" data-toggle="tab">Program Scheme</a>
                    </li>

                    <li role="presentation"><a href="#program_majoring" class="nav-link border rounded text-center"
                            aria-controls="program_majoring" role="tab" data-toggle="tab">Program Majoring</a>
                    </li>

                    <li role="presentation"><a href="#program_minoring" class="nav-link border rounded text-center"
                            aria-controls="program_minoring" role="tab" data-toggle="tab">Program Minoring</a>
                    </li>

                   <!--  <li role="presentation"><a href="#program_concurrent" class="nav-link border rounded text-center"
                            aria-controls="program_concurrent" role="tab" data-toggle="tab">Concurrent Program</a>
                    </li> -->

                    <li role="presentation"><a href="#program_accerdation" class="nav-link border rounded text-center" aria-controls="program_accerdation" role="tab" data-toggle="tab">Programme Accreditation</a>
                    </li>

                    <li role="presentation"><a href="#tab_program_objective" class="nav-link border rounded text-center" aria-controls="tab_program_objective" role="tab" data-toggle="tab">Programme Objective</a>
                    </li>
                    
                </ul>

                
                <div class="tab-content offers-tab-content">

                    <div role="tabpanel" class="tab-pane active" id="program_detail">
                        <div class="col-12 mt-4">




            <form id="form_programme" action="" method="post">

                <div class="form-container">
                <h4 class="form-group-title">Progam Details</h4>

                    <div class="row">

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Code <span class='error-text'>*</span></label>
                                <input type="text" class="form-control" id="code" name="code" value="<?php echo $programmeDetails->code; ?>">
                            </div>
                        </div>


                        
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Name <span class='error-text'>*</span></label>
                                <input type="text" class="form-control" id="name" name="name" value="<?php echo $programmeDetails->name; ?>">
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Name In Other Language </label>
                                <input type="text" class="form-control" id="name_optional_language" name="name_optional_language" value="<?php echo $programmeDetails->name_optional_language; ?>">
                            </div>
                        </div>

                        
                    </div>

                     <div class="row">


                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Select Award <span class='error-text'>*</span></label>
                                <select name="id_award" id="id_award" class="form-control">
                                    <option value="">Select</option>
                                    <?php
                                    if (!empty($awardList))
                                    {
                                        foreach ($awardList as $record)
                                        {?>
                                            <option value="<?php echo $record->id;  ?>"
                                                <?php 
                                                if($record->id == $programmeDetails->id_award)
                                                {
                                                    echo "selected=selected";
                                                } ?>>
                                                <?php echo $record->name;  ?>
                                            </option>
                                    <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>


                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Education Level <span class='error-text'>*</span></label>
                                <select name="id_education_level" id="id_education_level" class="form-control">
                                    <option value="">Select</option>
                                    <?php
                                    if (!empty($educationLevelList))
                                    {
                                        foreach ($educationLevelList as $record)
                                        {?>
                                            <option value="<?php echo $record->id;  ?>"
                                                <?php 
                                                if($record->id == $programmeDetails->id_education_level)
                                                {
                                                    echo "selected=selected";
                                                } ?>>
                                                <?php echo $record->name;  ?>
                                            </option>
                                    <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>


                        <!-- <div class="col-sm-4">
                            <div class="form-group">
                                <label>Select Scheme <span class='error-text'>*</span></label>
                                <select name="id_scheme" id="id_scheme" class="form-control" style="width: 408px">
                                    <option value="">Select</option>
                                    <?php
                                    if (!empty($schemeList))
                                    {
                                        foreach ($schemeList as $record)
                                        {?>
                                            <option value="<?php echo $record->id;  ?>"
                                                <?php 
                                                if($record->id == $programmeDetails->id_scheme)
                                                {
                                                    echo "selected=selected";
                                                } ?>>
                                                <?php echo $record->code . " - " . $record->description;  ?>
                                            </option>
                                    <?php
                                        }
                                    }
                                    ?>

                                </select>
                            </div>
                        </div> -->

                        

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Total Cr. Hours <span class='error-text'>*</span></label>
                                <input type="text" class="form-control" id="total_cr_hrs" name="total_cr_hrs" value="<?php echo $programmeDetails->total_cr_hrs; ?>">
                            </div>
                        </div>


                        


                    </div>

                    <div class="row">


                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Internal / External <span class='error-text'>*</span></label>
                                <select name="internal_external" id="internal_external" class="form-control" onchange="showPartner(this.value)">
                                     <option value="" >Select</option>

                                    <option value="Internal" <?php if($programmeDetails->internal_external=='Internal'){ echo "selected"; } ?>>Internal</option>
                                    <option value="External" <?php if($programmeDetails->internal_external=='External'){ echo "selected"; } ?>>External</option>
                                </select>
                            </div>
                        </div>                        



                        <div class="col-sm-4" id='partnerdropdown' style="display: none;">
                            <div class="form-group">
                                <label>Partner Name <span class='error-text'>*</span></label>
                                <select name="id_partner_university" id="id_partner_university" class="form-control" style="width: 408px">
                                    <option value="">Select</option>
                                    <?php
                                    if (!empty($partnerList))
                                    {
                                        foreach ($partnerList as $record)
                                        {?>
                                            <option value="<?php echo $record->id;  ?>"
                                                <?php 
                                                if($record->id == $programmeDetails->id_partner_university)
                                                {
                                                    echo "selected=selected";
                                                } ?>>
                                                <?php echo $record->name;  ?>
                                            </option>
                                    <?php
                                        }
                                    }
                                    ?>

                                </select>
                            </div>
                        </div>

  


                        <div class="col-sm-4">
                            <div class="form-group">
                                <p>Is APEL A <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                  <input type="radio" name="is_apel" id="is_apel" value="0"
                                   <?php if($programmeDetails->is_apel==0){ echo "checked"; } ?>
                                    ><span class="check-radio"></span> DIRECT ENTRY
                                </label>
                                <label class="radio-inline">
                                  <input type="radio" name="is_apel" id="is_apel" value="1" 
                                  <?php if($programmeDetails->is_apel==1){ echo "checked"; } ?>
                                  ><span class="check-radio"></span> APEL A
                                </label>                              
                            </div>                         
                        </div>




                        <!-- <div class="col-sm-4">
                            <div class="form-group">
                                <p>Mode <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                  <input type="radio" name="mode" id="mode" value="1"
                                   <?php if($programmeDetails->mode==1){ echo "checked"; } ?>
                                    ><span class="check-radio"></span> Single Mode
                                </label>
                                <label class="radio-inline">
                                  <input type="radio" name="mode" id="mode" value="0" 
                                  <?php if($programmeDetails->mode==0){ echo "checked"; } ?>
                                  ><span class="check-radio"></span> Multiple Mode
                                </label>                              
                            </div>                         
                        </div> -->



                        <div class="col-sm-4">
                                <div class="form-group">
                                    <p>Status <span class='error-text'>*</span></p>
                                    <label class="radio-inline">
                                      <input type="radio" name="status" id="status" value="1" <?php if($programmeDetails->status=='1') {
                                         echo "checked=checked";
                                      };?>><span class="check-radio"></span> Active
                                    </label>
                                    <label class="radio-inline">
                                      <input type="radio" name="status" id="status" value="0" <?php if($programmeDetails->status=='0') {
                                         echo "checked=checked";
                                      };?>>
                                      <span class="check-radio"></span> In-Active
                                    </label>                              
                                </div>                         
                        </div>



                    </div>

                    <!-- <div class="row"> 



                        <div class="col-sm-4">
                                <div class="form-group">
                                    <p>Status <span class='error-text'>*</span></p>
                                    <label class="radio-inline">
                                      <input type="radio" name="status" id="status" value="1" <?php if($programmeDetails->status=='1') {
                                         echo "checked=checked";
                                      };?>><span class="check-radio"></span> Active
                                    </label>
                                    <label class="radio-inline">
                                      <input type="radio" name="status" id="status" value="0" <?php if($programmeDetails->status=='0') {
                                         echo "checked=checked";
                                      };?>>
                                      <span class="check-radio"></span> In-Active
                                    </label>                              
                                </div>                         
                        </div>
                    </div> -->

                </div>


                


            </form>


            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="button" class="btn btn-primary btn-lg" onclick="validateDetailsData()">Save</button>
                    <a href="../list" class="btn btn-link">Back</a>
                </div>
            </div>


        <!-- </div> -->





        


                    <div class="form-container">
                        <h4 class="form-group-title"> Program Dean Details</h4>
                        <div class="m-auto text-center">
                            <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
                        </div>
                        <div class="clearfix">
                            <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                                <li role="presentation" class="active" ><a href="#invoice" class="nav-link border rounded text-center"
                                        aria-controls="invoice" aria-selected="true"
                                        role="tab" data-toggle="tab">Head Of Department</a>
                                </li>
                                
                            </ul>

                            
                            <div class="tab-content offers-tab-content">

                                <div role="tabpanel" class="tab-pane active" id="invoice">
                                    <div class="col-12 mt-4">




                                    <form id="form_programme_dean" action="" method="post">

                                        <br>


                                            <div class="row">

                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label>Select Staff <span class='error-text'>*</span></label>
                                                        <select name="id_staff" id="id_staff" class="form-control">
                                                            <option value="">Select</option>
                                                            <?php
                                                            if (!empty($staffList))
                                                            {
                                                                foreach ($staffList as $record)
                                                                {?>
                                                                    <option value="<?php echo $record->id;?>"
                                                                    ><?php echo $record->salutation ." - ".$record->name; ?>
                                                                    </option>
                                                            <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-sm-4">
                                                    <div class="forintake_has_programmem-group">
                                                        <label>Effective Date <span class='error-text'>*</span></label>
                                                        <input type="text" class="form-control datepicker" id="effective_start_date" name="effective_start_date" autocomplete="off">
                                                    </div>
                                                </div>
                                              
                                                <div class="col-sm-4">
                                                    <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="saveData()">Add</button>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div id="view"></div>
                                            </div>



                                    </form>


                            <?php

                            if(!empty($programmeHasDeanList))
                            {
                                ?>

                                <div class="form-container">
                                        <h4 class="form-group-title">Dean Details</h4>

                                    

                                      <div class="custom-table">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                <th>Sl. No</th>
                                                 <th>Dean</th>
                                                 <th>Effective Date</th>
                                                 <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php
                                             $total = 0;
                                              for($i=0;$i<count($programmeHasDeanList);$i++)
                                             { ?>
                                                <tr>
                                                <td><?php echo $i+1;?></td>
                                                <td><?php echo $programmeHasDeanList[$i]->salutation . " - " . $programmeHasDeanList[$i]->staff;?></td>
                                                <td><?php
                                                if($programmeHasDeanList[$i]->effective_start_date)
                                                {
                                                 echo date('d-m-Y', strtotime($programmeHasDeanList[$i]->effective_start_date));
                                                }
                                                 ?></td>
                                                <td>
                                                <a onclick="deleteDeanDetails(<?php echo $programmeHasDeanList[$i]->id; ?>)">Delete</a>
                                                </td>

                                                 </tr>
                                              <?php 
                                          } 
                                          ?>
                                            </tbody>
                                        </table>
                                      </div>

                                    </div>




                            <?php
                            
                            }
                             ?>

                                    



                                    </div> 
                                </div>



                            </div>


                        </div>
                    </div> 

        
        



        

                        



                        </div> 
                    
                    </div>




                    <div role="tabpanel" class="tab-pane" id="program_mode">
                        <div class="mt-4">


                        <form id="form_scheme" action="" method="post">


                            <br>

                            <div class="form-container">
                                <h4 class="form-group-title"> Program Mode Details</h4>

                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Mode Of Program <span class='error-text'>*</span></label>
                                            <select name="mode_of_program" id="mode_of_program" class="form-control" style="width: 398px">
                                                <option value="">Select</option>
                                                <option value="Online">Online</option>
                                                <option value="Face to Face">Face to Face</option>
                                                <option value="Blended">Blended</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Mode Of Study <span class='error-text'>*</span></label>
                                            <select name="mode_of_study" id="mode_of_study" class="form-control" style="width: 398px">
                                                <option value="">Select</option>
                                                <option value="Part Time">Part Time</option>
                                                <option value="Full Time">Full Time</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Program Structure Type <span class='error-text'>*</span></label>
                                            <select name="id_program_type" id="id_program_type" class="form-control" style="width: 398px">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($programTypeList))
                                                {
                                                    foreach ($programTypeList as $record)
                                                    {?>
                                                        <option value="<?php echo $record->id;?>"
                                                        ><?php echo $record->code ." - ".$record->name; ?>
                                                        </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                </div>


                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Min. Duration ( month ) <span class='error-text'>*</span></label>
                                            <input type="number" class="form-control" id="min_duration" name="min_duration">
                                        </div>
                                    </div>


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Max. Duration ( month ) <span class='error-text'>*</span></label>
                                            <input type="number" class="form-control" id="max_duration" name="max_duration">
                                        </div>
                                    </div>


                                </div>

                            </div>


                            <div class="button-block clearfix">
                                <div class="bttn-group">
                                    <button type="button" class="btn btn-primary btn-lg" onclick="saveSchemeData()">Add</button>
                                    <!-- <a href="../list" class="btn btn-link">Back</a> -->
                                </div>
                            </div>

                           <!--  <div class="row">
                                <div id="view_scheme"></div>
                            </div> -->


                            </form>



                            <?php

                            if(!empty($programmeLearningModeList))
                            {
                                ?>
                                <br>

                                <div class="form-container">
                                        <h4 class="form-group-title">Scheme Details</h4>

                                    

                                      <div class="custom-table">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                <th>Sl. No</th>
                                                 <th>Mode Of Program</th>
                                                 <th>Mode Of Study</th>
                                                 <th>Program Type</th>
                                                 <th>Min Duration( Month )</th>
                                                 <th>Max Duration( Month )</th>
                                                 <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php
                                             $total = 0;
                                              for($i=0;$i<count($programmeLearningModeList);$i++)
                                             { ?>
                                                <tr>
                                                <td><?php echo $i+1;?></td>
                                                <td><?php echo $programmeLearningModeList[$i]->mode_of_program;?></td>
                                                <td><?php echo $programmeLearningModeList[$i]->mode_of_study;?></td>
                                                <td><?php echo $programmeLearningModeList[$i]->program_code . " - " . $programmeLearningModeList[$i]->program_name;?></td>
                                                <td><?php echo $programmeLearningModeList[$i]->min_duration;?></td>
                                                <td><?php echo $programmeLearningModeList[$i]->max_duration;?></td>
                                                <td>
                                                <a onclick="deleteSchemeDetails(<?php echo $programmeLearningModeList[$i]->id; ?>)">Delete</a>
                                                </td>

                                                 </tr>
                                              <?php 
                                          } 
                                          ?>
                                            </tbody>
                                        </table>
                                      </div>

                                    </div>




                            <?php
                            
                            }
                             ?>


                        </div>
                    
                    </div>








                    <div role="tabpanel" class="tab-pane" id="program_scheme">
                        <div class="mt-4">


                        <form id="form_program_scheme" action="" method="post">


                            <br>

                            <div class="form-container">
                                <h4 class="form-group-title"> Program Scheme Details</h4>

                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Scheme <span class='error-text'>*</span></label>
                                            <select name="id_scheme" id="id_scheme" class="form-control" style="width: 398px">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($schemeList))
                                                {
                                                    foreach ($schemeList as $record)
                                                    {?>
                                                        <option value="<?php echo $record->id;?>"
                                                        ><?php echo $record->code ." - ".$record->description; ?>
                                                        </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                </div>

                            </div>


                            <div class="button-block clearfix">
                                <div class="bttn-group">
                                    <button type="button" class="btn btn-primary btn-lg" onclick="saveProgramSchemeData()">Add</button>
                                    <!-- <a href="../list" class="btn btn-link">Back</a> -->
                                </div>
                            </div>

                           <!--  <div class="row">
                                <div id="view_scheme"></div>
                            </div> -->


                            </form>



                            <?php

                            if(!empty($programmeSchemeList))
                            {
                                ?>
                                <br>

                                <div class="form-container">
                                        <h4 class="form-group-title">Scheme Details</h4>

                                    

                                      <div class="custom-table">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                <th>Sl. No</th>
                                                 <th>Scheme</th>
                                                 <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php
                                             $total = 0;
                                              for($i=0;$i<count($programmeSchemeList);$i++)
                                             { ?>
                                                <tr>
                                                <td><?php echo $i+1;?></td>
                                                <td><?php echo $programmeSchemeList[$i]->scheme_code . " - " . $programmeSchemeList[$i]->scheme_name;?></td>
                                                <td>
                                                <a onclick="deleteProgramSchemeDetails(<?php echo $programmeSchemeList[$i]->id; ?>)">Delete</a>
                                                </td>

                                                 </tr>
                                              <?php 
                                          } 
                                          ?>
                                            </tbody>
                                        </table>
                                      </div>

                                    </div>




                            <?php
                            
                            }
                             ?>


                        </div>
                    
                    </div>






                    <div role="tabpanel" class="tab-pane" id="program_majoring">
                        <div class="mt-4">


                        <form id="form_majoring" action="" method="post">


                            <br>

                                <div class="form-container">
                                <h4 class="form-group-title"> Majoring Details</h4>


                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Major Code <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="major_code" name="major_code">
                                        </div>
                                    </div>


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Description <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="major_description" name="major_description">
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Description In Other Language</label>
                                            <input type="text" class="form-control" id="major_description_other_language" name="major_description_other_language">
                                        </div>
                                    </div>


                                </div>

                            </div>


                            <div class="button-block clearfix">
                                <div class="bttn-group">
                                    <button type="button" class="btn btn-primary btn-lg" onclick="saveMajorData()">Add</button>
                                    <!-- <a href="../list" class="btn btn-link">Back</a> -->
                                </div>
                            </div>


                            </form>



                            <?php

                            if(!empty($programmeMajoringList))
                            {
                                ?>
                                <br>

                                <div class="form-container">
                                        <h4 class="form-group-title">Majoring Details</h4>

                                    

                                      <div class="custom-table">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                <th>Sl. No</th>
                                                 <th>Code</th>
                                                 <th>Description</th>
                                                 <th>Description In Other Language</th>
                                                 <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php
                                             $total = 0;
                                              for($i=0;$i<count($programmeMajoringList);$i++)
                                             { ?>
                                                <tr>
                                                <td><?php echo $i+1;?></td>
                                                <td><?php echo $programmeMajoringList[$i]->code;?></td>
                                                <td><?php echo $programmeMajoringList[$i]->name;?></td>
                                                <td><?php echo $programmeMajoringList[$i]->name_in_optional_language;?></td>
                                                <td>
                                                <a onclick="deleteMajorDetails(<?php echo $programmeMajoringList[$i]->id; ?>)">Delete</a>
                                                </td>

                                                 </tr>
                                              <?php 
                                          } 
                                          ?>
                                            </tbody>
                                        </table>
                                      </div>

                                    </div>




                            <?php
                            
                            }
                             ?>




                        </div>
                    
                    </div>





                    <div role="tabpanel" class="tab-pane" id="program_minoring">
                        <div class="mt-4">


                        <form id="form_minoring" action="" method="post">


                            <br>

                            <div class="form-container">
                                <h4 class="form-group-title">Program Minoring Details</h4>

                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Major Code <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="minor_code" name="minor_code">
                                        </div>
                                    </div>


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Description <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="minor_description" name="minor_description">
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Description In Other Language</label>
                                            <input type="text" class="form-control" id="minor_description_other_language" name="minor_description_other_language">
                                        </div>
                                    </div>

                                </div>

                            </div>


                            <div class="button-block clearfix">
                                <div class="bttn-group">
                                    <button type="button" class="btn btn-primary btn-lg" onclick="saveMinorData()">Add</button>
                                    <!-- <a href="../list" class="btn btn-link">Back</a> -->
                                </div>
                            </div>

                              


                            </form>



                            <?php

                            if(!empty($programmeMinoringList))
                            {
                                ?>
                                <br>

                                <div class="form-container">
                                        <h4 class="form-group-title">Program Minoring Details</h4>

                                    

                                      <div class="custom-table">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                <th>Sl. No</th>
                                                 <th>Code</th>
                                                 <th>Description</th>
                                                 <th>Description In Other Language</th>
                                                 <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php
                                             $total = 0;
                                              for($i=0;$i<count($programmeMinoringList);$i++)
                                             { ?>
                                                <tr>
                                                <td><?php echo $i+1;?></td>
                                                <td><?php echo $programmeMinoringList[$i]->code;?></td>
                                                <td><?php echo $programmeMinoringList[$i]->name;?></td>
                                                <td><?php echo $programmeMinoringList[$i]->name_in_optional_language;?></td>
                                                <td>
                                                <a onclick="deleteMinorDetails(<?php echo $programmeMinoringList[$i]->id; ?>)">Delete</a>
                                                </td>

                                                 </tr>
                                              <?php 
                                          } 
                                          ?>
                                            </tbody>
                                        </table>
                                      </div>

                                    </div>




                            <?php
                            
                            }
                             ?>





                        </div>
                    
                    </div>







                    <div role="tabpanel" class="tab-pane" id="program_concurrent">
                        <div class="mt-4">


                        <form id="form_concurrent" action="" method="post">


                            <br>

                           <!--  <div class="form-container">
                                <h4 class="form-group-title"> Concurrent Details</h4>


                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Min. Duration (month) <span class='error-text'>*</span></label>
                                            <input type="number" class="form-control" id="min_duration" name="min_duration">
                                        </div>
                                    </div>


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Max. Duration (month) <span class='error-text'>*</span></label>
                                            <input type="number" class="form-control" id="max_duration" name="max_duration">
                                        </div>
                                    </div>


                                </div>

                            </div>


                            <div class="button-block clearfix">
                                <div class="bttn-group">
                                    <button type="button" class="btn btn-primary btn-lg" onclick="saveConcurrentData()">Add</button>
                                </div>
                            </div> -->


                            </form>


                            <?php

                            if(!empty($programmeConcurrentList))
                            {
                                ?>
                                <br>

                                <div class="form-container">
                                        <h4 class="form-group-title">Program Concurrent Details</h4>

                                    

                                      <div class="custom-table">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                <th>Sl. No</th>
                                                 <th>Program</th>
                                                 <th>Award</th>
                                                 <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php
                                             $total = 0;
                                              for($i=0;$i<count($programmeConcurrentList);$i++)
                                             { ?>
                                                <tr>
                                                <td><?php echo $i+1;?></td>
                                                <td><?php echo $programmeConcurrentList[$i]->code . " - " . $programmeConcurrentList[$i]->name;?></td>
                                                <td><?php echo $programmeConcurrentList[$i]->award;?></td>
                                                <td>
                                                <a onclick="deleteCuncurrentDetails(<?php echo $programmeConcurrentList[$i]->id; ?>)">Delete</a>
                                                </td>

                                                 </tr>
                                              <?php 
                                          } 
                                          ?>
                                            </tbody>
                                        </table>
                                      </div>

                                    </div>




                            <?php
                            
                            }
                             ?>




                        </div>
                    
                    </div>






                    <div role="tabpanel" class="tab-pane" id="program_accerdation">
                        <div class="mt-4">


                        <form id="form_acceredation" action="" method="post">


                            <br>

                            <div class="form-container">
                                <h4 class="form-group-title"> Acceredation Details</h4>

                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Acceredation Type <span class='error-text'>*</span></label>
                                            <select name="acceredation_type" id="acceredation_type" class="form-control" style="width: 398px">
                                                <option value="">Select</option>
                                                <option value="Local">Local</option>
                                                <option value="International">International</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Mode Of Program <span class='error-text'>*</span></label>
                                            <select name="acceredation_category" id="acceredation_category" class="form-control" style="width: 398px">
                                                <option value="">Select</option>
                                                <option value="Board">Board</option>
                                                <option value="Kementarian">Kementarian</option>
                                                <option value="MQA">MQA</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Acceredation Date <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control datepicker" id="acceredation_dt" name="acceredation_dt" autocomplete="off">
                                        </div>
                                    </div>

                                </div>


                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Acceredation Number <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="acceredation_number" name="acceredation_number">
                                        </div>
                                    </div>


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Validity From <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control datepicker" id="valid_from" name="valid_from" autocomplete="off">
                                        </div>
                                    </div>


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Validity To <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control datepicker" id="valid_to" name="valid_to" autocomplete="off">
                                        </div>
                                    </div>

                                </div>

                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Approval Date <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control datepicker" id="approval_date" name="approval_date" autocomplete="off">
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Acceredation Reference <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="acceredation_reference" name="acceredation_reference">
                                        </div>
                                    </div>


                                </div>

                            </div>


                            <div class="button-block clearfix">
                                <div class="bttn-group">
                                    <button type="button" class="btn btn-primary btn-lg" onclick="saveAcceredationData()">Add</button>
                                    <!-- <a href="../list" class="btn btn-link">Back</a> -->
                                </div>
                            </div>

                            </form>




                            <?php

                            if(!empty($programmeAcceredationList))
                            {
                                ?>
                                <br>

                                <div class="form-container">
                                        <h4 class="form-group-title">Accrediation Details</h4>

                                    

                                      <div class="custom-table">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                <th>Sl. No</th>
                                                 <th>Type</th>
                                                 <th>Category</th>
                                                 <th>Reference</th>
                                                 <th>Date</th>
                                                 <th>Number</th>
                                                 <th>Valid From</th>
                                                 <th>Valid To</th>
                                                 <th>Approved Date</th>
                                                 <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php
                                             $total = 0;
                                              for($i=0;$i<count($programmeAcceredationList);$i++)
                                             { ?>
                                                <tr>
                                                <td><?php echo $i+1;?></td>
                                                <td><?php echo $programmeAcceredationList[$i]->type;?></td>
                                                <td><?php echo $programmeAcceredationList[$i]->category;?></td>
                                                <td><?php echo $programmeAcceredationList[$i]->acceredation_reference;?></td>
                                                <td><?php echo date('d-m-Y', strtotime($programmeAcceredationList[$i]->acceredation_dt));?></td>
                                                <td><?php echo $programmeAcceredationList[$i]->acceredation_number;?></td>
                                                <td><?php echo date('d-m-Y', strtotime($programmeAcceredationList[$i]->valid_from));?></td>
                                                <td><?php echo date('d-m-Y', strtotime($programmeAcceredationList[$i]->valid_to));?></td>
                                                <td><?php echo date('d-m-Y', strtotime($programmeAcceredationList[$i]->approval_dt));?></td>
                                                <td>
                                                <a onclick="deleteAcceredationDetails(<?php echo $programmeAcceredationList[$i]->id; ?>)">Delete</a>
                                                </td>

                                                 </tr>
                                              <?php 
                                          } 
                                          ?>
                                            </tbody>
                                        </table>
                                      </div>

                                    </div>




                            <?php
                            
                            }
                             ?>





                        </div>
                    
                    </div>










                    <div role="tabpanel" class="tab-pane" id="tab_program_objective">
                        <div class="mt-4">


                        <form id="form_program_objective" action="" method="post">


                            <br>

                            <div class="form-container">
                                <h4 class="form-group-title"> Program Objecticve Details</h4>


                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Objective Name <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="objective_name" name="objective_name">
                                        </div>
                                    </div>


                                </div>

                            </div>


                            <div class="button-block clearfix">
                                <div class="bttn-group">
                                    <button type="button" class="btn btn-primary btn-lg" onclick="saveProgramObjectiveData()">Add</button>
                                    <!-- <a href="../list" class="btn btn-link">Back</a> -->
                                </div>
                            </div>



                            </form>



                            <?php

                            if(!empty($programmeObjectiveList))
                            {
                                ?>
                                <br>

                                <div class="form-container">
                                        <h4 class="form-group-title">Program Objective Details</h4>

                                    

                                      <div class="custom-table">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                <th>Sl. No</th>
                                                 <th>Name</th>
                                                 <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php
                                             $total = 0;
                                              for($i=0;$i<count($programmeObjectiveList);$i++)
                                             { ?>
                                                <tr>
                                                <td><?php echo $i+1;?></td>
                                                <td><?php echo $programmeObjectiveList[$i]->name;?></td>
                                                <td>
                                                <a onclick="deleteProgramObjectiveDetails(<?php echo $programmeObjectiveList[$i]->id; ?>)">Delete</a>
                                                </td>

                                                 </tr>
                                              <?php 
                                          } 
                                          ?>
                                            </tbody>
                                        </table>
                                      </div>

                                    </div>




                            <?php
                            
                            }
                             ?>


                        </div>
                    
                    </div>


                </div>


            </div>












        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>
</div>
<script type="text/javascript">
    $('select').select2();
</script>

<script>
  $( function() {
    showPartner();
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
  } );
</script>

<script>
    function showPartner(){
        var value = $("#internal_external").val();
        if(value=='Internal') {
             $("#partnerdropdown").hide();

        } else if(value=='External') {
             $("#partnerdropdown").show();

        }
    }
    function saveData()
    {
        if($('#form_programme_dean').valid())
        {
        

        var tempPR = {};
        tempPR['id_staff'] = $("#id_staff").val();
        tempPR['effective_start_date'] = $("#effective_start_date").val();
        tempPR['id'] = <?php echo $id_programme;?>;
            $.ajax(
            {
               url: '/setup/programme/directadd',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert(result);
                location.reload();
                // windows().location.reload();

               }
            });
        }
    }

    function saveSchemeData()
    {
        if($('#form_scheme').valid())
        {

        var tempPR = {};
        tempPR['id_program_type'] = $("#id_program_type").val();
        tempPR['mode_of_program'] = $("#mode_of_program").val();
        tempPR['mode_of_study'] = $("#mode_of_study").val();
        tempPR['min_duration'] = $("#min_duration").val();
        tempPR['max_duration'] = $("#max_duration").val();
        tempPR['id_program'] = <?php echo $id_programme;?>;
            $.ajax(
            {
               url: '/setup/programme/directSchemeAdd',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                location.reload();
                // $("#view_scheme").html(result);
                // $('#myModal').modal('hide');
               }
            });
        }
    }

    function saveProgramSchemeData()
    {
        if($('#form_program_scheme').valid())
        {

        var tempPR = {};
        tempPR['id_scheme'] = $("#id_scheme").val();
        tempPR['id_program'] = <?php echo $id_programme;?>;
            $.ajax(
            {
               url: '/setup/programme/saveProgramSchemeData',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                location.reload();
                // $("#view_scheme").html(result);
                // $('#myModal').modal('hide');
               }
            });
        }
    }


    function saveMajorData()
    {
        if($('#form_majoring').valid())
        {

        var tempPR = {};
        tempPR['code'] = $("#major_code").val();
        tempPR['name'] = $("#major_description").val();
        tempPR['name_in_optional_language'] = $("#major_description_other_language").val();
        tempPR['id_program'] = <?php echo $id_programme;?>;
            $.ajax(
            {
               url: '/setup/programme/saveMajorData',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                location.reload();
                // $("#view_scheme").html(result);
                // $('#myModal').modal('hide');
               }
            });
        }
    }

    function saveMinorData()
    {
        if($('#form_minoring').valid())
        {

        var tempPR = {};
        tempPR['code'] = $("#minor_code").val();
        tempPR['name'] = $("#minor_description").val();
        tempPR['name_in_optional_language'] = $("#minor_description_other_language").val();
        tempPR['id_program'] = <?php echo $id_programme;?>;
            $.ajax(
            {
               url: '/setup/programme/saveMinorData',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                location.reload();
                // $("#view_scheme").html(result);
                // $('#myModal').modal('hide');
               }
            });
        }
    }

    function saveConcurrentData()
    {
        if($('#form_concurrent').valid())
        {

        var tempPR = {};
        tempPR['id_concurrent_program'] = $("#id_concurrent_program").val();
        tempPR['id_program'] = <?php echo $id_programme;?>;
            $.ajax(
            {
               url: '/setup/programme/saveConcurrentData',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                location.reload();
                // $("#view_scheme").html(result);
                // $('#myModal').modal('hide');
               }
            });
        }
    }

    function saveAcceredationData()
    {
        if($('#form_acceredation').valid())
        {

        var tempPR = {};
        tempPR['category'] = $("#acceredation_category").val();
        tempPR['type'] = $("#acceredation_type").val();
        tempPR['acceredation_dt'] = $("#acceredation_dt").val();
        tempPR['acceredation_number'] = $("#acceredation_number").val();
        tempPR['valid_from'] = $("#valid_from").val();
        tempPR['valid_to'] = $("#valid_to").val();
        tempPR['approval_dt'] = $("#approval_date").val();
        tempPR['acceredation_reference'] = $("#acceredation_reference").val();
        tempPR['id_program'] = <?php echo $id_programme;?>;
            $.ajax(
            {
               url: '/setup/programme/saveAcceredationData',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                location.reload();
                // $("#view_scheme").html(result);
                // $('#myModal').modal('hide');
               }
            });
        }
    }

    function saveProgramObjectiveData()
    {
        if($('#form_program_objective').valid())
        {

        var tempPR = {};
        tempPR['name'] = $("#objective_name").val();
        tempPR['id_programme'] = <?php echo $id_programme;?>;
            $.ajax(
            {
               url: '/setup/programme/saveProgramObjectiveData',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                location.reload();
               }
            });
        }
    }






    function deleteDeanDetails(id) {
         $.ajax(
            {
               url: '/setup/programme/deleteStaffDetailData/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view").html(result);
                    // alert('Deleted Sessessfully');
                    window.location.reload();
               }
            });
    }

    function deleteSchemeDetails(id)
    {
        $.ajax(
            {
               url: '/setup/programme/deleteProgrammeHasScheme/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view").html(result);
                    // alert('Deleted Sessessfully');
                    location.reload();
               }
            });
    }

    function deleteProgramSchemeDetails(id)
    {
        $.ajax(
            {
               url: '/setup/programme/deleteProgramSchemeDetails/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view").html(result);
                    // alert('Deleted Sessessfully');
                    location.reload();
               }
            });
    }



    function deleteMajorDetails(id)
    {
        $.ajax(
            {
               url: '/setup/programme/deleteMajorDetails/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view").html(result);
                    // alert('Deleted Sessessfully');
                    location.reload();
               }
            });
    }


    function deleteMinorDetails(id)
    {
        $.ajax(
            {
               url: '/setup/programme/deleteMinorDetails/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view").html(result);
                    // alert('Deleted Sessessfully');
                    location.reload();
               }
            });
    }


    function deleteCuncurrentDetails(id)
    {
        $.ajax(
            {
               url: '/setup/programme/deleteCuncurrentDetails/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view").html(result);
                    // alert('Deleted Sessessfully');
                    location.reload();
               }
            });
    }


    function deleteAcceredationDetails(id)
    {
        $.ajax(
            {
               url: '/setup/programme/deleteAcceredationDetails/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view").html(result);
                    // alert('Deleted Sessessfully');
                    location.reload();
               }
            });
    }


    function deleteProgramObjectiveDetails(id)
    {
        $.ajax(
            {
               url: '/setup/programme/deleteProgramObjectiveDetails/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    location.reload();
               }
            });
    }








    function validateDetailsData()
    {
        if($('#form_programme').valid())
        {
            
            $('#form_programme').submit();
        }    
    }





     $(document).ready(function()
     {
        $("#form_programme").validate({
            rules: {
                name: {
                    required: true
                },
                code: {
                    required: true
                },
                graduate_studies: {
                    required: true
                },
                foundation: {
                    required: true
                },
                total_cr_hrs: {
                    required: true
                },
                id_award: {
                    required: true
                },
                status: {
                    required: true
                },
                id_scheme: {
                    required: true
                },
                id_education_level: {
                    required: true
                },
                mode: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Program  Name Required</p>",
                },
                code: {
                    required: "<p class='error-text'>Program  Code Required</p>",
                },
                graduate_studies: {
                    required: "<p class='error-text'>Graduate Studies Required</p>",
                },
                foundation: {
                    required: "<p class='error-text'>Foundation Required</p>",
                },
                total_cr_hrs: {
                    required: "<p class='error-text'>Total Credit Hours required</p>",
                },
                id_award: {
                    required: "<p class='error-text'>Select Award</p>",
                },
                status: {
                    required: "<p class='error-text'>Select status</p>",
                },
                id_scheme: {
                    required: "<p class='error-text'>Select Scheme</p>",
                },
                id_education_level: {
                    required: "<p class='error-text'>Select Education Level</p>",
                },
                mode: {
                    required: "<p class='error-text'>Select Mode</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


     


    $(document).ready(function()
    {
        $("#form_program_scheme").validate({
            rules: {
                id_scheme: {
                    required: true
                }
            },
            messages: {
                id_scheme: {
                    required: "<p class='error-text'>Select Scheme</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });




    $(document).ready(function()
    {
        $("#form_programme_dean").validate({
            rules: {
                id_staff: {
                    required: true
                },
                effective_start_date: {
                    required: true
                }
            },
            messages: {
                id_staff: {
                    required: "<p class='error-text'>Select Staff</p>",
                },
                effective_start_date: {
                    required: "<p class='error-text'>Select Effective Start Date</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });




    $(document).ready(function()
     {
        $("#form_scheme").validate({
            rules: {
                id_program_type: {
                    required: true
                },
                mode_of_program: {
                    required: true
                },
                mode_of_study: {
                    required: true
                },
                min_duration: {
                    required: true
                },
                max_duration: {
                    required: true
                }
            },
            messages: {
                id_program_type: {
                    required: "<p class='error-text'>Select Program Type</p>",
                },
                mode_of_program: {
                    required: "<p class='error-text'>Select Mode Of Program</p>",
                },
                mode_of_study: {
                    required: "<p class='error-text'>Select Mode Of Study</p>",
                },
                min_duration: {
                    required: "<p class='error-text'>Min. Duration Required</p>",
                },
                max_duration: {
                    required: "<p class='error-text'>Max. Duration Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });



    $(document).ready(function()
     {
        $("#form_majoring").validate({
            rules: {
                major_code: {
                    required: true
                },
                major_description: {
                    required: true
                }
            },
            messages: {
                major_code: {
                    required: "<p class='error-text'>Code Required</p>",
                },
                major_description: {
                    required: "<p class='error-text'>Description Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });



    $(document).ready(function()
     {
        $("#form_minoring").validate({
            rules: {
                minor_code: {
                    required: true
                },
                minor_description: {
                    required: true
                }
            },
            messages: {
                minor_code: {
                    required: "<p class='error-text'>Code Required</p>",
                },
                minor_description: {
                    required: "<p class='error-text'>Description Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });



    $(document).ready(function()
     {
        $("#form_acceredation").validate({
            rules: {
                acceredation_category: {
                    required: true
                },
                acceredation_type: {
                    required: true
                },
                acceredation_dt: {
                    required: true
                },
                acceredation_number: {
                    required: true
                },
                valid_from: {
                    required: true
                },
                valid_to: {
                    required: true
                },
                approval_date: {
                    required: true
                },
                acceredation_reference: {
                    required: true
                }
            },
            messages: {
                acceredation_category: {
                    required: "<p class='error-text'>Select Category</p>",
                },
                acceredation_type: {
                    required: "<p class='error-text'>Select Acceredation Type</p>",
                },
                acceredation_dt: {
                    required: "<p class='error-text'>Select Acceredation Date</p>",
                },
                acceredation_number: {
                    required: "<p class='error-text'>Acceredation No. Required</p>",
                },
                valid_from: {
                    required: "<p class='error-text'>Select Validity Start Date</p>",
                },
                valid_to: {
                    required: "<p class='error-text'>Select Validity End Date</p>",
                },
                approval_date: {
                    required: "<p class='error-text'>Select Approval Date</p>",
                },
                acceredation_reference: {
                    required: "<p class='error-text'>Acceredation Reference Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


    $(document).ready(function()
    {
        $("#form_program_objective").validate({
            rules: {
                objective_name: {
                    required: true
                }
            },
            messages: {
                objective_name: {
                    required: "<p class='error-text'>Objective Name Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

</script>