<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>Partner List</h3>
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
               Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">


                <div class="row">

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">From Date</label>
                      <div class="col-sm-8">
                        <input type="date" class="form-control" name="from_date" >
                      </div>
                    </div>
                  </div>

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">To Date</label>
                      <div class="col-sm-8">
                        <input type="date" class="form-control" name="to_date" >
                      </div>
                    </div>
                  </div>

                </div>

               

                 <div class="row">

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Partner</label>
                      <div class="col-sm-8">
                        <select name="id_partner_university" id="id_partner_university" class="form-control">
                          <option value="">Select</option>
                          <?php
                          if (!empty($partnerUniversityList)) {
                            foreach ($partnerUniversityList as $record)
                            {
                              $selected = '';
                              if ($record->id == $searchParam['id_partner_university']) {
                                $selected = 'selected';
                              }
                          ?>
                              <option value="<?php echo $record->id;  ?>"
                                <?php echo $selected;  ?>>
                                <?php echo  $record->name;  ?>
                              </option>
                          <?php
                            }
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                  </div>

                

                </div> 


                
              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Generate</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

     </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script>
    $('select').select2();
  
    function clearSearchForm()
    {
      window.location.reload();
    }
</script>