<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>Add Intake</h3>
            </div>

    <form id="form_programme" action="" method="post">
        <div class="form-container">
            <h4 class="form-group-title">Intake Details</h4>


            <div class="row">
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Year <span class='error-text'>*</span></label>
                        <select class="form-control" id="year" name="year">
                            <option value="">Select</option>
                            

                            <?php
                            if (!empty($yearList))
                            {
                                foreach ($yearList as $record)
                                {?>
                                 <option value="<?php echo $record->name;  ?>">
                                    <?php echo $record->name;?>
                                 </option>
                            <?php
                                }
                            }
                            ?>


                        </select>
                    </div>
                </div>
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name">
                    </div>
                </div>
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name In Other Language</label>
                        <input type="text" class="form-control" id="name_in_malay" name="name_in_malay">
                    </div>
                </div>

            </div>

            <div class="row">

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Semester Sequence <span class='error-text'>*</span></label>
                        <select class="form-control" id="semester_sequence" name="semester_sequence">
                            <option value="">Select</option>
                            <option value='January'>January</option>
                            <option value='February'>February</option>
                            <option value='March'>March</option>
                            <option value='April'>April</option>
                            <option value='May'>May</option>
                            <option value='June'>June</option>
                            <option value='July'>July</option>
                            <option value='August'>August</option>
                            <option value='September'>September</option>
                            <option value='October'>October</option>
                            <option value='November'>November</option>
                            <option value='December'>December</option>
                        </select>
                    </div>
                </div>  

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Application Start Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="start_date" name="start_date" autocomplete="off">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Application End Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control  datepicker" id="end_date" name="end_date" autocomplete="off">
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <p>Is Sibbling Discount Applicable <span class='error-text'>*</span></p>
                        <label class="radio-inline">
                          <input type="radio" name="is_sibbling_discount" id="is_sibbling_discount" value="0" checked="checked"><span class="check-radio"></span> No
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="is_sibbling_discount" id="is_sibbling_discount" value="1"><span class="check-radio"></span> Yes
                        </label>                              
                    </div>                         
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <p>Is Employee Discount Applicable <span class='error-text'>*</span></p>
                        <label class="radio-inline">
                          <input type="radio" name="is_employee_discount" id="is_employee_discount" value="0" checked="checked"><span class="check-radio"></span> No
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="is_employee_discount" id="is_employee_discount" value="1"><span class="check-radio"></span> Yes
                        </label>                              
                    </div>                         
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <p>Is Alumni Discount Applicable <span class='error-text'>*</span></p>
                        <label class="radio-inline">
                          <input type="radio" name="is_alumni_discount" id="is_alumni_discount" value="0" checked="checked"><span class="check-radio"></span> No
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="is_alumni_discount" id="is_alumni_discount" value="1"><span class="check-radio"></span> Yes
                        </label>                              
                    </div>                         
                </div>

            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <p>Generate Temporary Offer Letter <span class='error-text'>*</span></p>
                        <label class="radio-inline">
                          <input type="radio" name="is_temp_offer_letter" id="is_temp_offer_letter" value="1" checked="checked"><span class="check-radio"></span> Yes
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="is_temp_offer_letter" id="is_temp_offer_letter" value="0"><span class="check-radio"></span> Yes
                        </label>                              
                    </div>                         
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <p>Status <span class='error-text'>*</span></p>
                        <label class="radio-inline">
                          <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                        </label>                              
                    </div>                         
                </div>

            </div>

        </div>
    </form>

    <div class="button-block clearfix">
        <div class="bttn-group">
            <button type="button" class="btn btn-primary btn-lg" onclick="validateProgram()">Save</button>
            <a href="list" class="btn btn-link">Cancel</a>
        </div>
    </div>











    <div class="form-container">
            <h4 class="form-group-title"> Intake Programme Details</h4>          
            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>
            <div class="clearfix">
                <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                    <li role="presentation" class="active" ><a href="#invoice" class="nav-link border rounded text-center"
                            aria-controls="invoice" aria-selected="true"
                            role="tab" data-toggle="tab">Program Details</a>
                    </li>
                </ul>

                
                <div class="tab-content offers-tab-content">

                    <div role="tabpanel" class="tab-pane active" id="invoice">
                        <div class="col-12 mt-4">




                        <form id="form_programme_intake" action="" method="post">
                            <div class="form-container">
                            <h4 class="form-group-title">Intake Has Programme Details</h4>

                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Select Program <span class='error-text'>*</span></label>
                                            <select name="id_programme" id="id_programme" class="form-control">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($programmeList))
                                                {
                                                    foreach ($programmeList as $record)
                                                    {?>
                                                        <option value="<?php echo $record->id;?>"
                                                        ><?php echo $record->code . " - " . $record->name; ?>
                                                        </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                  
                                    <div class="col-sm-4">
                                        <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="saveData()">Add</button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div id="view"></div>
                                </div>

                            </div>
                        </form>


                        </div> 
                    </div>







                </div>

            </div>
    

    </div>









           
            
            

    

         
        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Intake Has Programme</h4>
      </div>
      <div class="modal-body">
         <h4></h4>
             <div class="row">
                        <input type="text" class="form-control" id="id" name="id">

                
            </div>


      </div>
      <div class="modal-footer">
                

        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<script>

    function opendialog()
    {
        $("#id_programme").val('');
        $("#id").val('0');                    
        $('#myModal').modal('show');

    }
    
    function saveData()
    {
        if($('#form_programme_intake').valid())
        {

        var tempPR = {};
        tempPR['id_programme'] = $("#id_programme").val();
        tempPR['id'] = $("#id").val();
            $.ajax(
            {
               url: '/setup/intake/tempadd',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view").html(result);
                $('#myModal').modal('hide');
               }
            });
        }
    }

    function deleteTempIntakeHasProgramme(id) {
        // alert(id);
         $.ajax(
            {
               url: '/setup/intake/deleteTempIntakeHasProgramme/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    $("#view").html(result);
               }
            });
    }


    function getTempData(id) {
        $.ajax(
            {
               url: '/setup/intake/tempedit/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(resultfromjson)
               {
                    result = JSON.parse(resultfromjson);
                    $("#dt_fund").val(result['dt_fund']);
                    $("#dt_department").val(result['dt_department']);
                    $("#dt_activity").val(result['dt_activity']);
                    $("#dt_account").val(result['dt_account']);
                    $("#cr_fund").val(result['cr_fund']);
                    $("#cr_department").val(result['cr_department']);
                    $("#cr_activity").val(result['cr_activity']);
                    $("#cr_account").val(result['cr_account']);
                    $("#type").val(result['type']);
                    $("#id_category").val(result['id_category']);
                    $("#id_sub_category").val(result['id_sub_category']);
                    $("#id_item").val(result['id_item']);
                    $("#quantity").val(result['quantity']);
                    $("#price").val(result['price']);
                    $("#id_tax").val(result['id_tax']);
                    $("#tax_price").val(result['tax_price']);
                    $("#total_final").val(result['total_final']);
                    $("#id").val(id);
                    $('#myModal').modal('show');
               }
            });

    }

    

    $(document).ready(function() {
        $("#form_programme_intake").validate({
            rules: {
                id_programme: {
                    required: true
                }
            },
            messages: {
                id_programme: {
                    required: "<p class='error-text'>Select Programme</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    $(document).ready(function() {
        $("#form_programme").validate({
            rules: {
                name: {
                    required: true
                },
                year: {
                    required: true
                },
                semester_sequence: {
                    required: true
                },
                start_date: {
                    required: true
                },
                end_date: {
                    required: true
                },
                status: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                year: {
                    required: "<p class='error-text'>Year Required</p>",
                },
                semester_sequence: {
                    required: "<p class='error-text'>Select Semester Sequence</p>",
                },
                start_date: {
                    required: "<p class='error-text'>Select Start Date</p>",
                },
                end_date: {
                    required: "<p class='error-text'>Select End Date</p>",
                },
                status: {
                    required: "<p class='error-text'>Select Award</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    function validateProgram() {

    if($('#form_programme').valid())
      {
         console.log($("#view").html());
         var addedProgam = $("#view").html();
         if(addedProgam=='') {
            alert("Add program to the intake");
        }else {

         $('#form_programme').submit();
        }
      }     
    }
</script>

<script type="text/javascript">
    $('select').select2();
</script>
 <script>
  $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
  } );
</script>