<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class StudentCourseRegistration extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('student_course_registration_model');
        $this->isLoggedIn();
    }

    function add()
    {
        if ($this->checkAccess('student_course_registration.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                // echo "<Pre>"; print_r($this->input->post());exit;
                
                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                $formData = $this->input->post();
                
                $id_programme = $this->security->xss_clean($this->input->post('id_program'));
                $id_students = $this->security->xss_clean($this->input->post('id_student'));

                


                foreach ($id_students as $id_student)
                {
                    
                    $fee_structure = $this->student_course_registration_model->getFeeStructureByIdProgramme($id_programme);

                    if($fee_structure)
                    {
                        $student_data['id_fee_structure'] = $fee_structure->id;

                        $updated_student = $this->student_course_registration_model->editStudent($student_data, $id_student);
                    }


                    $id_invoice = $this->student_course_registration_model->createNewMainInvoiceForStudent($id_student,$id_programme);


                    if($id_invoice)
                    {
                        $programme = $this->student_course_registration_model->getProgramme($id_programme);

                        if($programme)
                        {

                            // echo "<Pre>"; print_r($programme);exit;
                            
                            $max_duration = $programme->max_duration;
                            $duration_type = $programme->duration_type;

                            $start_date = date('Y-m-d');

                            $data_student_has_programme = array(
                                'id_student' => $id_student,
                                'id_invoice' => $id_invoice,
                                'id_programme' => $id_programme,
                                'start_date' => $start_date,
                                'end_date' => date('Y-m-d', strtotime($start_date . "+" . $max_duration  . " " . $duration_type) ),
                                'status' => 0
                            );
                            // echo "<Pre>"; print_r($data_student_has_programme);exit;

                            $id_student_has_programme = $this->student_course_registration_model->addNewStudentHasProgramme($data_student_has_programme);

                        }
                    }
                }
                
                redirect($_SERVER['HTTP_REFERER']);
            }


            $data['programList'] = $this->student_course_registration_model->programListByStatus('1');

            // echo "<Pre>"; print_r($data);exit;

            $this->global['pageTitle'] = 'Campus Management System : Advisor Taagging';
            $this->loadViews("student_course_registration/add", $this->global, $data, NULL);
        }
    }

    function searchStudents()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        // $staffList = $this->student_course_registration_model->staffListByStatus('1');
        
        $student_data = $this->student_course_registration_model->studentSearch($tempData);

        // echo "<Pre>";print_r($student_data);exit();
        if(!empty($student_data))
        {


         $table = "

         <script type='text/javascript'>
             $('select').select2();
         </script>";


         $table .= "
         <br>
         <h4> Select Students For Programme Registration</h4>
         <table  class='table' id='list-table'>
                  <tr>
                    <th>Sl. No</th>
                    <th>Student Name</th>
                    <th>Student NRIC/PASSPORT</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Nationality</th>
                    <th style='text-align: center;'><input type='checkbox' id='checkAll' name='checkAll' onclick='checkAllCheckBoxes()'> Check All</th>
                </tr>";

            for($i=0;$i<count($student_data);$i++)
            {
                $id = $student_data[$i]->id;
                $full_name = $student_data[$i]->full_name;
                $nric = $student_data[$i]->nric;
                $email_id = $student_data[$i]->email_id;
                $phone = $student_data[$i]->phone;
                $ic_no = $student_data[$i]->ic_no;
                $advisor_name = $student_data[$i]->advisor_name;
                $nationality = $student_data[$i]->nationality;
                $j = $i+1;
                $table .= "
                <tr>
                    <td>$j</td>
                    <td>$full_name</td>                        
                    <td>$nric</td>
                    <td>$email_id</td>                      
                    <td>$phone</td>              
                    <td>$nationality</td>              
                    
                    <td class='text-center'>
                        <input type='checkbox' id='id_student[]' name='id_student[]' class='check' value='".$id."'>
                    </td>
               
                </tr>";
            }

         $table.= "</table>";
        }
        else
        {
            $table= "<h4> No Data Found For Your Search</h4>";
        }
        echo $table;exit;
    }
}