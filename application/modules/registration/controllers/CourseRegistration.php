<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class CourseRegistration extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('course_registration_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('course_registration.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        { 

            $data['intakeList'] = $this->course_registration_model->intakeList();
            $data['programList'] = $this->course_registration_model->programListNotForPostgraduate('POSTGRADUATE');
            // $data['courseList'] = $this->course_registration_model->courseList();

            $formData['first_name'] = $this->security->xss_clean($this->input->post('first_name'));
            $formData['email_id'] = $this->security->xss_clean($this->input->post('email_id'));
            $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
            $formData['id_program'] = $this->security->xss_clean($this->input->post('id_program'));
            $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
            // $formData['id_course'] = $this->security->xss_clean($this->input->post('id_course'));

            $data['searchParam'] = $formData;
            $data['courseRegistrationList'] = $this->course_registration_model->courseRegistrationList($formData);


            // echo "<Pre>";print_r($data['courseRegistrationList']);exit();

            $this->global['pageTitle'] = 'Campus Management System : Course Registration';
            $this->loadViews("course_registration/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('course_registration.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;
            
            if($this->input->post())
            {
                // echo "<Pre>";print_r($this->input->post());exit();

                $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
                $id_education_level = $this->security->xss_clean($this->input->post('id_education_level'));
                $id_semester = $this->security->xss_clean($this->input->post('id_semester'));
                $id_programme = $this->security->xss_clean($this->input->post('id_programme'));
                $id_student = $this->security->xss_clean($this->input->post('id_student'));
                $id_program_landscape = $this->security->xss_clean($this->input->post('id_program_landscape'));
                $courseCheck = $this->security->xss_clean($this->input->post('courseCheck'));
                $id_course_registered_landscape = $this->security->xss_clean($this->input->post('id_course_registered_landscape'));



                $master_data = array(
                   'id_intake' => $id_intake,
                   'id_semester' => $id_semester,
                    'id_programme' => $id_programme,
                    'id_program_landscape' => $id_program_landscape,
                    // 'id_course_registered_landscape' => $id_course_registered_landscape,
                    'id_student' => $id_student,
                    'id_education_level' => $id_education_level,
                    // 'id_course' => $landscape_course_details->id_course,
                    'created_by' => $user_id
                );

                $id_course_register = $this->course_registration_model->addCoureRegister($master_data);


                $details = array();
                for($i=0;$i<count($id_course_registered_landscape);$i++)
                {


                $landscape_course_details = $this->course_registration_model->getCoursesIdByLandscapeIdForDetailsAdd($id_course_registered_landscape[$i]);
                // echo "<Pre>";print_r($landscape_course_details);exit();

                     $data = array(
                    'id_course_register' => $id_course_register,
                    'id_course_registered_landscape' => $id_course_registered_landscape[$i],
                    'id_course' => $landscape_course_details->id_course,
                    'student_current_semester' => $landscape_course_details->id_semester,
                    'id_intake' => $id_intake,
                    'id_semester' => $id_semester,
                    'id_programme' => $id_programme,
                    'id_student' => $id_student,
                    'created_by' => $user_id

                    

                );
                    $insert_id = $this->course_registration_model->addCoureRegistration($data);

                    $detail['id_course'] = $landscape_course_details->id_course;
                    $detail['id_course_registered'] = $insert_id;

                    array_push($details, $detail);

                     // echo "<Pre>";print_r($data);exit();
                }
                    
                if($insert_id)
                {

                    $student = $this->course_registration_model->getStudent($id_student);
                    $id_university = $student->id_university;
                    $partner_university = $this->course_registration_model->getPartnerUniversity($id_university);


                    if($id_university == 1)
                    {
                        $invoice_generated = $this->course_registration_model->generateNewMainInvoiceForCourseRegistration($details,$id_student,$id_semester);
                    }
                    elseif($partner_university->billing_to == 'Student')
                    {
                            // $detail['id_course'] = $landscape_course_details->id_course;
                            // $detail['id_course_registered'] = $insert_id;

                         // echo "<Pre>";print_r($details);exit();
                         
                        // Hided Generate Main Invoice oN Course Registration 
                            $invoice_generated = $this->course_registration_model->generateNewMainInvoiceForCourseRegistration($details,$id_student,$id_semester);
                    }
                }
                
                

                redirect('/registration/courseRegistration/list');
            }
            // $data['programmeList'] = $this->course_registration_model->programmeListByStatus('1');
            // $data['intakeList'] = $this->course_registration_model->intakeListByStatus('1');
            // $data['studentList'] = $this->course_registration_model->studentListByStatus();
            $data['semesterList'] = $this->course_registration_model->semesterListByStatus('1');
            $data['degreeTypeList'] = $this->course_registration_model->qualificationListByStatus('1');

            $this->global['pageTitle'] = 'Campus Management System : Add Course Registration';
            $this->loadViews("course_registration/add", $this->global, $data, NULL);
        }
    }


    function addAttendance()
    {
        if ($this->checkAccess('course_registration.add_attendance') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;
            
            if($this->input->post())
            {
                echo "<Pre>";print_r($this->input->post());exit();

                $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
                $id_education_level = $this->security->xss_clean($this->input->post('id_education_level'));
                $id_semester = $this->security->xss_clean($this->input->post('id_semester'));
                $id_programme = $this->security->xss_clean($this->input->post('id_programme'));
                $id_student = $this->security->xss_clean($this->input->post('id_student'));
                $id_program_landscape = $this->security->xss_clean($this->input->post('id_program_landscape'));
                $courseCheck = $this->security->xss_clean($this->input->post('courseCheck'));
                $id_course_registered_landscape = $this->security->xss_clean($this->input->post('id_course_registered_landscape'));



                $master_data = array(
                   'id_intake' => $id_intake,
                   'id_semester' => $id_semester,
                    'id_programme' => $id_programme,
                    'id_program_landscape' => $id_program_landscape,
                    // 'id_course_registered_landscape' => $id_course_registered_landscape,
                    'id_student' => $id_student,
                    'id_education_level' => $id_education_level,
                    // 'id_course' => $landscape_course_details->id_course,
                    'created_by' => $user_id
                );

                $id_course_register = $this->course_registration_model->addCoureRegister($master_data);


                $details = array();
                for($i=0;$i<count($id_course_registered_landscape);$i++)
                {


                $landscape_course_details = $this->course_registration_model->getCoursesIdByLandscapeIdForDetailsAdd($id_course_registered_landscape[$i]);
                // echo "<Pre>";print_r($landscape_course_details);exit();

                     $data = array(
                    'id_course_register' => $id_course_register,
                    'id_course_registered_landscape' => $id_course_registered_landscape[$i],
                    'id_course' => $landscape_course_details->id_course,
                    'id_intake' => $id_intake,
                    'id_semester' => $id_semester,
                    'id_programme' => $id_programme,
                    'id_student' => $id_student,
                    'created_by' => $user_id

                    

                );
                    $insert_id = $this->course_registration_model->addCoureRegistration($data);

                    $detail['id_course'] = $landscape_course_details->id_course;
                    $detail['id_course_registered'] = $insert_id;

                    array_push($details, $detail);


                     // echo "<Pre>";print_r($data);exit();

                    
                }

                if($insert_id)
                {
                        // $detail['id_course'] = $landscape_course_details->id_course;
                        // $detail['id_course_registered'] = $insert_id;

                     // echo "<Pre>";print_r($details);exit();
                     
                    // Hided Generate Main Invoice oN Course Registration 
                        $invoice_generated = $this->course_registration_model->generateNewMainInvoiceForCourseRegistration($details,$id_student,$id_semester);
                }
                
                

                redirect('/registration/courseRegistration/list');
            }
            $data['semesterList'] = $this->course_registration_model->semesterListByStatus('1');
            $data['degreeTypeList'] = $this->course_registration_model->qualificationListByStatus('1');

            $this->global['pageTitle'] = 'Campus Management System : Add Course Registration';
            $this->loadViews("course_registration/add_attendance", $this->global, $data, NULL);
        }
    }


    function edit($id)
    {
        if ($this->checkAccess('course_registration.view') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/registration/courseRegistration/list');
            }
            if($this->input->post())
            {

                redirect('/registration/courseRegistration/list');
            }
            // $data['id_student'] = $id;

            $data['courseData'] = $this->course_registration_model->getCourseRegistered($id);

            $data['studentData'] = $this->course_registration_model->getStudentByStudentId($data['courseData']->id_student);

            $data['courseList'] = $this->course_registration_model->getCourseRegistrationByMasterId($id);
            $data['degreeTypeList'] = $this->course_registration_model->qualificationListByStatus('1');


            $data['semesterDetails'] = $this->course_registration_model->semesterDetails($data['courseData']->id_semester);
            // echo "<Pre>";print_r($data['courseList']); exit();


            // $data['semesterDetails'] = $this->course_registration_model->semesterDetails($data['courseData']->id_semester);

            // echo "<Pre>";print_r($data['courseList']); exit();
            

            $this->global['pageTitle'] = 'Campus Management System : Edit Course Registration';
            $this->loadViews("course_registration/edit", $this->global, $data, NULL);
        }
    }


    function view($id_student)
    {
        if ($this->checkAccess('course_registration.view') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id_student == null)
            {
                redirect('/registration/courseRegistration/list');
            }
            if($this->input->post())
            {

                redirect('/registration/courseRegistration/list');
            }
            // $data['id_student'] = $id;

            $data['studentDetails'] = $this->course_registration_model->getStudentByStudentId($id_student);
            $data['organisationDetails'] = $this->course_registration_model->getOrganisation();
            $data['courseRegisteredLandscapeFBySemester'] = $this->course_registration_model->getCourseRegisteredLandscapeBySemesterForDisplay($data['studentDetails']->id_program,$data['studentDetails']->id_intake,$data['studentDetails']->id_program_landscape,$id_student);


            // echo "<Pre>";print_r($data['courseRegisteredLandscapeFBySemester']); exit();
            

            $this->global['pageTitle'] = 'Campus Management System : Edit Course Registration';
            $this->loadViews("course_registration/view", $this->global, $data, NULL);
        }
    }

   




    // Register By Selecting Student


    function getCourseLandscapeByByProgNIntake()
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        // $tempData['id_semester'] = '';
        // $tempData['status'] = '';

        $student_list_data = $this->course_registration_model->getCourseLandscapeByByProgNIntake($tempData);

        // echo "<Pre>";print_r($student_list_data);exit;

        $table="

        <script type='text/javascript'>
            $('select').select2();
        </script>

        <select name='id_course_registered_landscape' id='id_course_registered_landscape' class='form-control' onchange='displayStudentData()'>";
        $table.="<option value=''>Select</option> 

        
                ";

        for($i=0;$i<count($student_list_data);$i++)
        {

        // echo "<Pre>";print_r($student_list_data[$i]);exit;
        // $id = $results[$i]->id_procurement_category;
        $id_course_registered_landscape = $student_list_data[$i]->id_course_registered_landscape;
        $code = $student_list_data[$i]->code;
        $name = $student_list_data[$i]->name;
        $table.="<option value=".$id_course_registered_landscape.">".$code. " - " . $name.
                "</option>";

        }
        $table.="</select>";
        
        echo $table;
        exit();
    }

    function displayStudentsByIdCourseRegisteredLandscape()
    {
        
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
         // echo "<Pre>"; print_r($tempData);exit();

        $temp_details = $this->course_registration_model->getStudentListByCourseRegistrationData($tempData);
        $course_details = $this->course_registration_model->getCourseRegisteredLandscape($tempData['id_course_registered_landscape']);

        // echo "<Pre>"; print_r($temp_details);exit();

            $id_course_registered_landscape = $tempData['id_course_registered_landscape'];
            $name = $course_details->name;
            $name = $course_details->name;
            $code = $course_details->code;
            $credit_hours = $course_details->credit_hours;
            $class_recurrence = $course_details->class_recurrence;
            $course_type = $course_details->course_type;




        $table = "

            <h4 class='sub-title'>Course Details</h4>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Course Name :</dt>
                                <dd>$name</dd>
                            </dl>
                            <dl>
                                <dt>Course Code :</dt>
                                <dd>$code</dd>
                            </dl>
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Course Type :</dt>
                                <dd>$course_type</dd>
                            </dl>
                            <dl>
                                <dt>Credit Hours :</dt>
                                <dd>$credit_hours</dd>
                            </dl>
                        </div>
    
                    </div>
                </div>



            <h4 class='sub-title'>Select Student For Course Registration</h4>




        <div class='custom-table'><table class='table' id='list-table'>
                   <thead>
                  <tr>
                    <th>Student Name</th>
                    <th>Email Id</th>
                    <th>NRIC</th>
                    <th>Phone No.</th>
                    <th>Gender</th>
                    <th>Program Scheme</th>
                    <th>DOB</th>
                    <th class='text-center'>Select Student</th>
                </tr></thead><tbody>";
                
        if($temp_details!=NULL)
        {
            

                    for($i=0;$i<count($temp_details);$i++)
                    {

                    $id_student = $temp_details[$i]->id;
                    $full_name = $temp_details[$i]->full_name;
                    $nric = $temp_details[$i]->nric;
                    $phone = $temp_details[$i]->phone;
                    $email_id = $temp_details[$i]->email_id;
                    $gender = $temp_details[$i]->gender;
                    $program_scheme = $temp_details[$i]->program_scheme;
                    $date_of_birth = $temp_details[$i]->date_of_birth;


                    if($date_of_birth)
                    {
                        $date_of_birth = date('d-m-Y', strtotime($date_of_birth));
                    }


                        $table .= "
                        <tr>
                            <td>$full_name</td>
                            <td>$email_id</td>
                            <td>$nric</td>
                            <td>$phone</td>
                            <td>$gender</td>
                            <td>$program_scheme</td>
                            <td>$date_of_birth</td>
                            <td class='text-center'><input type='checkbox' name='id_student[]' id='id_student' value='$id_student'></td>
                        </tr>";
                    }
                $table.= "</tbody></table></div>";

        }
        
        echo $table;
    }

    function displayStudentsForCourseRegistrationAttendanceAttendance()
    {

        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        // echo "<Pre>"; print_r($tempData);exit();

        $temp_details = $this->course_registration_model->displayStudentsForCourseRegistrationAttendance($tempData);

        // echo "<Pre>"; print_r($temp_details);exit();

        $table = "

        <h4 class='sub-title'>Select Student For Course Registration Attendance</h4>

        <div class='custom-table'><table class='table' id='list-table'>
                   <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Student Name</th>
                    <th>Email Id</th>
                    <th>NRIC</th>
                    <th>Phone No.</th>
                    <th>Gender</th>
                    <th>Program Scheme</th>
                    <th>DOB</th>
                    <th class='text-center'>Select Student</th>
                </tr></thead><tbody>";
                
        if($temp_details!=NULL)
        {
            

                    for($i=0;$i<count($temp_details);$i++)
                    {

                    $j = $i+1;

                    $id = $temp_details[$i]->id;
                    $id_course_registration = $temp_details[$i]->id_course_registration;
                    $full_name = $temp_details[$i]->full_name;
                    $nric = $temp_details[$i]->nric;
                    $phone = $temp_details[$i]->phone;
                    $email_id = $temp_details[$i]->email_id;
                    $gender = $temp_details[$i]->gender;
                    $program_scheme = $temp_details[$i]->program_scheme;
                    $date_of_birth = $temp_details[$i]->date_of_birth;


                    if($date_of_birth)
                    {
                        $date_of_birth = date('d-m-Y', strtotime($date_of_birth));
                    }


                        $table .= "
                        <tr>
                            <td>$j</td>
                            <td>$full_name</td>
                            <td>$email_id</td>
                            <td>$nric</td>
                            <td>$phone</td>
                            <td>$gender</td>
                            <td>$program_scheme</td>
                            <td>$date_of_birth</td>
                            <td class='text-center'><input type='checkbox' name='id_course_registration[]' id='id_course_registration' value='$id'></td>
                        </tr>";
                    }
                $table.= "</tbody></table></div>";

        }
        
        echo $table;   
    }






    // Register By Selecting Course

    function getIntakes()
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        // echo "<Pre>";print_r($tempData);exit;
        // $tempData['id_semester'] = '';
        // $tempData['status'] = '';

        $student_list_data = $this->course_registration_model->getIntakeListByProgramme($tempData['id_programme']);
        // echo "<Pre>";print_r($student_list_data);exit;


        $table="

        <script type='text/javascript'>
            $('select').select2();
        </script>

        <select name='id_intake' id='id_intake' class='form-control' onchange='getStudentByProgNIntake()'>";
        $table.="<option value=''>Select</option>";

        for($i=0;$i<count($student_list_data);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $student_list_data[$i]->id_intake;
        $intake_name = $student_list_data[$i]->intake_name;
        $intake_year = $student_list_data[$i]->intake_year;


        $table.="<option value=".$id.">". $intake_year . " - " . $intake_name.
                "</option>";

        }
        $table.="</select>";
        
        echo $table;
        exit();
    }

    function getStudentByProgNIntake()
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        // echo "<Pre>";print_r($tempData);exit;
        // $tempData['id_semester'] = '';
        // $tempData['status'] = '';

        $student_list_data = $this->course_registration_model->getStudentByProgNIntake($tempData);


        $table="

        <script type='text/javascript'>
            $('select').select2();
        </script>

        <select name='id_student' id='id_student' class='form-control' onchange='displaydata()'>";
        $table.="<option value=''>Select</option> 

        
                ";

        for($i=0;$i<count($student_list_data);$i++)
        {

        // echo "<Pre>";print_r($student_list_data[$i]);exit;
        // $id = $results[$i]->id_procurement_category;
        $id = $student_list_data[$i]->id;
        $full_name = $student_list_data[$i]->full_name;
        $nric = $student_list_data[$i]->nric;
        $table.="<option value=".$id.">".$nric. " - " . $full_name.
                "</option>";

        }
        $table.="</select>";
        
        echo $table;
        exit();
    }


    function displaydata()
    {
        
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $id_intake = $tempData['id_intake'];
        $id_programme = $tempData['id_programme'];
        $id_student = $tempData['id_student'];
        $id_learning_mode = $tempData['id_learning_mode'];
        $id_program_scheme = $tempData['id_program_scheme'];
        
        // echo "<Pre>"; print_r($tempData);exit();


        $student_data = $this->course_registration_model->getStudentByStudentId($id_student);

        $tempData['id_program_landscape'] = $student_data->id_program_landscape;
        $tempData['current_semester'] = $student_data->current_semester;
        
        $temp_details = $this->course_registration_model->getPerogramLandscape($tempData);

        // echo "<Pre>"; print_r($tempData);exit();

            $id_program_landscape = 0;

            if(!empty($temp_details))
            {
                $id_program_landscape = $temp_details[0]->id_program_landscape;
            }

            $student_name = $student_data->full_name;
            $student_nric = $student_data->nric;
            $email = $student_data->email_id;
            $nric = $student_data->nric;
            $intake_name = $student_data->intake_name;
            $id_intake = $student_data->id_intake;
            $programme_name = $student_data->programme_name;
            $program_scheme = $student_data->program_scheme;
            $education_level = $student_data->qualification;
            $current_semester = $student_data->current_semester;




        $table = "

            <h4 class='sub-title'>Student Details</h4>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student Name :</dt>
                                <dd>$student_name</dd>
                            </dl>
                            <dl>
                                <dt>Student Email :</dt>
                                <dd>$email</dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd>$nric</dd>
                            </dl>
                            <dl>
                                <dt>Education Level :</dt>
                                <dd>$education_level</dd>
                            </dl>
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Intake :</dt>
                                <dd>

                                    <input type='hidden' name='id_program_landscape' id='id_program_landscape' value='$id_program_landscape' /> $intake_name

                                </dd>
                            </dl>
                            <dl>
                                <dt>Programme :</dt>
                                <dd>$programme_name</dd>
                            </dl>
                            <dl>
                                <dt>Learnning Mode</dt>
                                <dd>$program_scheme</dd>
                            </dl>
                            <dl>
                                <dt>Current Semester</dt>
                                <dd>$current_semester</dd>
                            </dl>
                        </div>
    
                    </div>
                </div>


            <span class='error-text'>Note * :
                <br>
                1. Course With Pending In Previous Semesters and Current Semesters Are Displaying In The List.
            </span>
            

        
        <h4 class='sub-title'>Select Course For Registration</h4>

        <div class='custom-table'><table class='table' id='list-table'>
                   <thead>
                  <tr>
                    <th>Sl No</th>
                    <th>Select Course</th>
                    <th>Registered Semester</th>
                    <th>Course Name</th>
                    <th>Credit Hrs</th>
                    <th>Course Type</th>
                </tr></thead><tbody>";
                
        if($temp_details != NULL)
        {
            

                    for($i=0;$i<count($temp_details);$i++)
                    {
                    $mainId = $temp_details[$i]->id;
                    $courseName = $temp_details[$i]->courseName;
                    $course_type = $temp_details[$i]->course_type;
                    $pre_requisite = $temp_details[$i]->pre_requisite;
                    $id_course_registered_landscape = $temp_details[$i]->id_course_registered_landscape;
                    $credit_hours = $temp_details[$i]->credit_hours;
                    $course_code = $temp_details[$i]->course_code;
                    $id_semester = $temp_details[$i]->id_semester;

                    $j = $i + 1;



                        $table .= "
                        <tr>
                            <td>$j</td>
                            <td>
                            <input type='hidden' name='courseCheck[]' id='courseCheck' value='$mainId'>
                            <input type='checkbox' name='id_course_registered_landscape[]' id='id_course_registered_landscape' value='$id_course_registered_landscape' checked='checked'></td>
                            <td>$id_semester</td>
                            <td>$course_code - $courseName</td>
                            <td>$credit_hours</td>
                            <td>$course_type</td>
                        </tr>";
                    }
                $table.= "</tbody></table></div>";

        }
        
        echo $table;
    }

    function getIntakesForView()
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        // echo "<Pre>";print_r($tempData);exit;
        // $tempData['id_semester'] = '';
        // $tempData['status'] = '';

        $student_list_data = $this->course_registration_model->getIntakeListByProgramme($tempData['id_programme']);
        // echo "<Pre>";print_r($student_list_data);exit;


        $table="

        <script type='text/javascript'>
            $('select').select2();
        </script>

        <select name='id_intake' id='id_intake' class='form-control' onchange='viewData()'>";
        $table.="<option value=''>Select</option> 

        
                ";

        for($i=0;$i<count($student_list_data);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $student_list_data[$i]->id_intake;
        $intake_name = $student_list_data[$i]->intake_name;
        $table.="<option value=".$id.">".$intake_name.
                "</option>";

        }
        $table.="</select>";
        
        echo $table;
        exit();
    }

    function viewData()
    {
        
        $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
        $id_programme = $this->security->xss_clean($this->input->post('id_programme'));
        $id_student = $this->security->xss_clean($this->input->post('id_student'));
         // echo "<Pre>"; print_r($id_student);exit();

        $temp_details = $this->course_registration_model->getCoursesByProgramNIntakeNStudent($id_intake,$id_programme,$id_student);
        $student_data = $this->course_registration_model->getStudentByStudentId($id_student,$id_intake,$id_programme);

        // echo "<Pre>";print_r($temp_details);exit();


            $student_name = $student_data->full_name;
            $student_nric = $student_data->nric;
            $email = $student_data->email_id;
            $nric = $student_data->nric;
            $intake_name = $student_data->intake_name;
            $id_intake = $student_data->id_intake;
            $programme_name = $student_data->programme_name;



        $table = "

            <h4 class='sub-title'>Student Details</h4>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student Name :</dt>
                                <dd>$student_name</dd>
                            </dl>
                            <dl>
                                <dt>Student Email :</dt>
                                <dd>$email</dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd>$nric</dd>
                            </dl>
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Intake :</dt>
                                <dd>

                                    <input type='hidden' name='id_intake' id='id_intake' value='$id_intake' /> $intake_name

                                </dd>
                            </dl>
                            <dl>
                                <dt>Programme :</dt>
                                <dd>$programme_name</dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd></dd>
                            </dl>
                        </div>
    
                    </div>
                </div>



        <h4 class='sub-title'>Course Registration Details</h4>

        <div class='custom-table'>
        <table class='table' id='list-table'>
                   <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Course Name</th>
                    <th>Course Code</th>
                    <th>Pre-Requisite</th>
                    <th>Total Cr. Hours</th>
                </tr></thead><tbody>";
                
        if($temp_details!=NULL) {
            

                    for($i=0;$i<count($temp_details);$i++)
                    {
                    // $mainId = $temp_details[$i]->id;
                    $name = $temp_details[$i]->name;
                    $name_in_malay = $temp_details[$i]->name_in_malay;
                    $code = $temp_details[$i]->code;
                    $pre_requisite = $temp_details[$i]->pre_requisite;
                    $min_total_cr_hrs = $temp_details[$i]->min_total_cr_hrs;
                    $j=$i+1;
                        $table .= "
                        <tr>
                            <td>$j</td>
                            <td>$name</td>
                            <td>$code</td>
                            <td>$pre_requisite</td>
                            <td>$min_total_cr_hrs</td>
                        </tr>";
                    }
                $table.= "</tbody></table></div>";

        }
        
        echo $table;
    }

    function getLearningModeByProgramId()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        
        $id_program = $tempData['id_program'];

        $intake_data = $this->course_registration_model->getProgramSchemeByProgramId($id_program);

            // echo "<Pre>"; print_r($intake_data);exit;
             $table="
            <script type='text/javascript'>

            $('select').select2();
                            
            </script>


            <select name='id_learning_mode' id='id_learning_mode' class='form-control' onchange='getStudentByProgNIntake()'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($intake_data);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $intake_data[$i]->id;
            $mode_of_program = $intake_data[$i]->mode_of_program;
            $mode_of_study = $intake_data[$i]->mode_of_study;


            $table.="<option value=".$id.">". $mode_of_program . " - " .  $mode_of_study .
                    "</option>";



            // $table.="<option value='".$id . "'";
            // if($id_learning_mode == $id)
            // {
            //    $table.="selected";
            // } $table.= ">". $mode_of_program . " - " .  $mode_of_study .
            //         "</option>";

            }
            $table.="</select>";

            echo $table;  
    }


    function getSchemeByProgramId($id_program)
    {
       $intake_data = $this->course_registration_model->getSchemeByProgramId($id_program);

        // echo "<Pre>"; print_r($intake_data);exit;
         $table="
        <script type='text/javascript'>

        $('select').select2();
                        
        </script>


        <select name='id_program_has_scheme' id='id_program_has_scheme' class='form-control'  onchange='getStudentByProgNIntake()'>";
        $table.="<option value=''>Select</option>";

        for($i=0;$i<count($intake_data);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $intake_data[$i]->id;
        $code = $intake_data[$i]->code;
        $name = $intake_data[$i]->description;

        $table.="<option value=".$id.">". $code . " - " .  $name .
                "</option>";

        }
        $table.="</select>";

        echo $table;
    }

    function getProgrammeByEducationLevelId($id_education_level)
    {
            // echo "<Pre>"; print_r($id_education_level);exit;
            $intake_data = $this->course_registration_model->getProgrammeByEducationLevelId($id_education_level);

             $table="
            <script type='text/javascript'>
                $('select').select2();                
            </script>


            <select name='id_programme' id='id_programme' class='form-control' onchange='getIntakes()'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($intake_data);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $intake_data[$i]->id;
            $name = $intake_data[$i]->name;
            $code = $intake_data[$i]->code;

            $table.="<option value=".$id.">".$code . " - " . $name.
                    "</option>";

            }
            $table.="</select>";

            echo $table;       
    }
}