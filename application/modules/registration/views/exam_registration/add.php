<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Exam Registration</h3>
        </div>
        <form id="form_grade" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Exam Regiastration Details</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Semester <span class='error-text'>*</span></label>
                        <select name="id_semester" id="id_semester" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($semesterList))
                            {
                                foreach ($semesterList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Exam Location <span class='error-text'>*</span></label>
                        <select name="id_location" id="id_location" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($examLocationList))
                            {
                                foreach ($examLocationList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name; ?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>



            </div>



            <div class="row">
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Program <span class='error-text'>*</span></label>
                        <select name="id_programme" id="id_programme" class="form-control" onchange="getIntakes()">
                            <option value="">Select</option>
                            <?php
                            if (!empty($programmeList))
                            {
                                foreach ($programmeList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label style="display: none;" id="display_intake">Select Intake <span class='error-text'>*</span></label>
                        <span id='view_intake' ></span>
                    </div>
                </div>
               
                <div class="col-sm-4">
                    <div class="form-group">
                        <label style="display: none;" id="display_student">Select Student <span class='error-text'>*</span></label>
                        <span id='view_student' ></span>
                    </div>
                </div>
            </div>


            


            <!-- <div style="border: 1px solid;border-radius: 12px;">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4">
                        <h4>Student Name : </h4>
                        <h4>Student ID : </h4>
                        <h4>Branch : </h4>
                    </div>
                    <div class="col-sm-4">
                        <h4>Intake : </h4>
                        <h4>Programme : </h4>
                        <h4>Scheme : </h4>
                    </div>
                </div>
            </div>
        </div> -->

        </div>


        <br>

        <div class="form-container" id="course_student_view" style="display: none;">
            <h4 class="form-group-title">Student & Course Details</h4>

            <div class="custom-table">
                  <div id="view"></div>
            </div>

        </div>
            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="list" class="btn btn-link">Cancel</a>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script type="text/javascript">
    $('select').select2();
</script>
<script type="text/javascript">

    function displaydata()
    {

        if($("#id_intake").val() != '' && $("#id_location").val() != '' && $("#id_programme").val() != '' && $("#id_student").val() != '')
        {
                var id_intake = $("#id_intake").val();
                var id_location = $("#id_location").val();
                var id_programme = $("#id_programme").val();
                var id_student = $("#id_student").val();

            $.ajax(
            {
               url: '/registration/examRegistration/displaydata',
                type: 'POST',
               data:
               {
                'id_intake': id_intake,
                'id_location': id_location,
                'id_programme': id_programme,
                'id_student':id_student

               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                if(result != '')
                {
                    $("#course_student_view").show();
                    $("#view").html(result);
                }
               }
            });
        }
    }


    function getIntakes()
    {

     var tempPR = {};
        tempPR['id_programme'] = $("#id_programme").val();
            $.ajax(
            {
               url: '/registration/courseRegistration/getIntakes',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_intake").html(result);
                    $("#display_intake").show();

                // if (tempPR['id_programme'] != '' && tempPR['id_intake'] == '')
                // {
                    
                // }
                // var ita = $("#invoice_total_amount").val();
                // $("#receipt_amount").val(ita);
               }
            });
    }


    function getStudentByProgNIntake()
    {

     var tempPR = {};
        tempPR['id_programme'] = $("#id_programme").val();
        tempPR['id_intake'] = $("#id_intake").val();
        // alert(tempPR['id_programme']);
            $.ajax(
            {
               url: '/registration/courseRegistration/getStudentByProgNIntake',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                if(result != '')
                {
                    $("#display_student").show();
                    $("#view_student").html(result);
                }
                    

                // if (tempPR['id_programme'] != '' && tempPR['id_intake'] == '')
                // {
                    
                // }
                // var ita = $("#invoice_total_amount").val();
                // $("#receipt_amount").val(ita);
               }
            });
    }
    
</script>
<script>

    $(document).ready(function() {
        $("#form_grade").validate({
            rules: {
                 id_programme: {
                    required: true
                },
                 id_intake: {
                    required: true
                },
                id_student: {
                    required: true
                },
                 id_semester: {
                    required: true
                },
                 id_location: {
                    required: true
                }
            },
            messages: {
                id_programme: {
                    required: "<p class='error-text'>Select Program</p>",
                },
                id_intake: {
                    required: "<p class='error-text'>Select Intake</p>",
                },
                id_student: {
                    required: "<p class='error-text'>Select Student</p>",
                },
                id_semester: {
                    required: "<p class='error-text'>Select Semester</p>",
                },
                id_location: {
                    required: "<p class='error-text'>Select Location</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>