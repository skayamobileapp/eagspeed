<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>Report Course Student Count</h3>
      <!-- <a href="add" class="btn btn-primary">+ Add Exam Event</a> -->
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">


                



                <div class="row">

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Intake <span class='error-text'>*</span></label>
                      <div class="col-sm-8">
                        <select name="id_intake" id="id_intake" class="form-control selitemIcon">
                            <option value="">Select</option>
                              <?php
                            if (!empty($intakeList)) {
                              foreach ($intakeList as $record)
                              {
                                
                            ?>
                                <option value="<?php echo $record->id;  ?>"
                                  <?php 
                                  if($record->id == $searchParam['id_intake'])
                                  {
                                    echo 'selected';
                                    }  ?>
                                    >
                                  <?php echo  $record->name;  ?>
                                  </option>
                            <?php
                            }
                            }
                            ?>
                        </select>
                      </div>
                    </div>
                  </div>


                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Program <span class='error-text'>*</span></label>
                      <div class="col-sm-8">
                        <select name="id_program" id="id_program" class="form-control selitemIcon" onchange="getProgramSchemeByProgramId(this.value)">
                            <option value="">Select</option>
                              <?php
                            if (!empty($programList)) {
                              foreach ($programList as $record)
                              {
                                
                            ?>
                                <option value="<?php echo $record->id;  ?>"
                                  <?php 
                                  if($record->id == $searchParam['id_program'])
                                  {
                                    echo 'selected';
                                    }  ?>
                                    >
                                  <?php echo  $record->code . " - " . $record->name;  ?>
                                  </option>
                            <?php
                            }
                            }
                            ?>
                        </select>
                      </div>
                    </div>
                  </div>

                </div>

                <div class="row">


                  <div class="col-sm-6" id="view_by_building">
                   <div class="form-group">
                    <label class="col-sm-4 control-label">Learning Mode <span class='error-text'>*</span></label>
                    <div class="col-sm-8">

                      <div name="view_dummy" id="view_dummy">
                        <select class="form-control selitemIcon">
                          <option value="">Select</option>
                        </select>
                      </div>
                      <span id="view_learning_mode" style="display: none;"></span>
                    </div>
                   </div>
                  </div>


                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Learning Center </label>
                      <div class="col-sm-8">
                        <select name="id_university" id="id_university" class="form-control selitemIcon" onchange="getBranchesByPartnerUniversity(this.value)">
                            <option value="">Select</option>
                              <?php
                            if (!empty($partnerUniversityList)) {
                              foreach ($partnerUniversityList as $record)
                              {
                                $selected = '';
                                if ($record->id == $searchParam['id_university']) {
                                  $selected = 'selected';
                                }
                            ?>
                                <option value="<?php echo $record->id;  ?>"
                                  <?php echo $selected;  ?>>
                                  <?php echo  $record->code . " - " . $record->name;  ?>
                                  </option>
                            <?php
                            }
                            }
                            ?>
                        </select>
                      </div>
                    </div>
                  </div>

                </div>



                <div class="row">


                  <div class="col-sm-6" id="view_by_building">
                   <div class="form-group">
                    <label class="col-sm-4 control-label">Training Center </label>
                    <div class="col-sm-8">
                      <div name="view_dummy_training_center" id="view_dummy_training_center">
                      <select class="form-control selitemIcon">
                      <option value="">Select</option>
                      </select>
                      </div>
                      <span id="view_training_center" style="display: none;"></span>
                    </div>
                   </div>
                  </div>


                </div>

                  <!-- <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Learning Mode <span class='error-text'>*</span></label>
                      <div class="col-sm-8">
                        <select name="id_learning_mode" id="id_learning_mode" class="form-control selitemIcon">
                            <option value="">Select</option>
                              <?php
                            if (!empty($learningModeList)) {
                              foreach ($learningModeList as $record)
                              {
                                
                            ?>
                                <option value="<?php echo $record->id;  ?>"
                                  <?php 
                                    if($record->id == $searchParam['id_learning_mode'])
                                    {
                                        echo 'selected';
                                    }
                                    ?>
                                    >
                                  <?php echo  $record->mode_of_program . " - " . $record->mode_of_study;  ?>
                                  </option>
                            <?php
                            }
                            }
                            ?>
                        </select>
                      </div>
                    </div>
                  </div> -->



                  <!-- <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Learning Center <span class='error-text'>*</span></label>
                      <div class="col-sm-8">
                        <select name="id_branch" id="id_branch" class="form-control selitemIcon">
                            <option value="">Select</option>
                              <?php
                            if (!empty($branchList)) {
                              foreach ($branchList as $record)
                              {
                                $selected = '';
                                if ($record->id == $searchParam['id_branch']) {
                                  $selected = 'selected';
                                }
                            ?>
                                <option value="<?php echo $record->id;  ?>"
                                  <?php echo $selected;  ?>>
                                  <?php echo  $record->name;  ?>
                                  </option>
                            <?php
                            }
                            }
                            ?>
                        </select>
                      </div>
                    </div>
                  </div> -->

                

                  



                  <!-- <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Scheme <span class='error-text'>*</span></label>
                      <div class="col-sm-8">
                        <select name="id_program_scheme" id="id_program_scheme" class="form-control selitemIcon" disabled>
                            <option value="">Select</option>
                              <?php
                            if (!empty($schemeList)) {
                              foreach ($schemeList as $record)
                              {
                                $selected = '';
                                if ($record->id == $searchParam['id_program_scheme']) {
                                  $selected = 'selected';
                                }
                            ?>
                                <option value="<?php echo $record->id;  ?>"
                                  <?php echo $selected;  ?>>
                                  <?php echo  $record->name;  ?>
                                  </option>
                            <?php
                            }
                            }
                            ?>
                        </select>
                      </div>
                    </div>
                  </div> -->



              





              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href="list" class="btn btn-link" >Clear All Fields</a>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
             <th>Sl. No</th>
            <th>Course</th>
            <th>Student Count</th>
            <!-- <th>Blended Part Time</th> -->
            <!-- <th class="text-center">Action</th> -->
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($courseCountList))
          {
            $i=1;
            foreach ($courseCountList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->course_code . "" . $record->course_name ?></td>
                <td><?php echo $record->student_count ?></td>
                <!-- <td><?php echo $record->exam_center ?></td> -->
                <!-- <td><?php if( $record->status == '1')
                {
                  echo "Active";
                }
                else
                {
                  echo "In-Active";
                } 
                ?></td> -->
                <!-- <td class="text-center">
                  <a href="<?php echo 'edit/' . $record->id; ?>" title="Edit">Edit</a>
                  
                </td> -->
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script>

  function getBranchesByPartnerUniversity(id)
   {
      // $.get("/registration/courseCount/getBranchesByPartnerUniversity/"+id, function(data, status)
      // {
      //   $("#view_dummy_training_center").hide();
      //   $("#view_training_center").html(data);
      //   $("#view_training_center").show();
      // });


      var id_branch = <?php if($searchParam['id_branch'] != ''){ echo $searchParam['id_branch']; }else{ echo '0'; } ?>

      if(id_branch == '')
      {
        id_branch = 0;
      }

      var tempPR = {};
        tempPR['id_university'] = id;
        tempPR['id_branch'] = id_branch;

            $.ajax(
            {
               url: '/registration/courseCount/getBranchesByPartnerUniversity',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {

                $("#view_dummy_training_center").hide();
                $("#view_training_center").html(result);
                $("#view_training_center").show();
               }
            });

   }



  function getProgramSchemeByProgramId(id)
  {
    // alert(id);
      // $.get("/registration/courseCount/getProgramSchemeByProgramId/"+id,
      //   function(data, status)
      //   {
      //     $("#view_dummy").hide();
      //     $("#view_learning_mode").html(data);
      //     $("#view_learning_mode").show();
      //   });

      var id_learning_mode = <?php if($searchParam['id_learning_mode'] != ''){ echo $searchParam['id_learning_mode']; }else{ echo '0'; } ?>

      if(id_learning_mode == '')
      {
        id_learning_mode = 0;
      }

      var tempPR = {};
        tempPR['id_program'] = id;
        tempPR['id_learning_mode'] = id_learning_mode;

            $.ajax(
            {
               url: '/registration/courseCount/getProgramSchemeByProgramId',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
               $("#view_dummy").hide();
               $("#view_learning_mode").html(result);
               $("#view_learning_mode").show();
               }
            });


  }


  $(document).ready(function()
    {
        var id_program = "<?php echo $searchParam['id_program'];?>";


        if(id_program.length > 0)
        {
          getProgramSchemeByProgramId(id_program);
        }


        var id_university = "<?php echo $searchParam['id_university'];?>";


        if(id_university.length > 0)
        {
          getBranchesByPartnerUniversity(id_university);
        }




        $("#searchForm").validate(
        {
            rules:
            {
                id_intake:
                {
                    required: true
                },
                id_program:
                {
                    required: true
                },
                id_learning_mode:
                {
                    required: true
                }
            },
            messages:
            {
                id_intake:
                {
                    required: "<p class='error-text'>Select Intake</p>",
                },
                id_program:
                {
                    required: "<p class='error-text'>Select Program</p>",
                },
                id_learning_mode:
                {
                    required: "<p class='error-text'>Select Leaning Mode</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


  
    $('select').select2();
</script>