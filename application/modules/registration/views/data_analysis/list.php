<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>Data Analysis By Learning Center And Semester</h3>
      <!-- <a href="add" class="btn btn-primary">+ Add Exam Event</a> -->
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">


              

                <div class="row">

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Learning Center </label>
                      <div class="col-sm-8">
                        <select name="id_university" id="id_university" class="form-control selitemIcon" onchange="getBranchesByPartnerUniversity(this.value)">
                            <option value="">Select</option>
                              <?php
                            if (!empty($partnerUniversityList)) {
                              foreach ($partnerUniversityList as $record)
                              {
                                $selected = '';
                                if ($record->id == $searchParam['id_university']) {
                                  $selected = 'selected';
                                }
                            ?>
                                <option value="<?php echo $record->id;  ?>"
                                  <?php echo $selected;  ?>>
                                  <?php echo  $record->code . " - " . $record->name;  ?>
                                  </option>
                            <?php
                            }
                            }
                            ?>
                        </select>
                      </div>
                    </div>
                  </div>



                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Semester </label>
                      <div class="col-sm-8">
                        <select name="id_semester" id="id_semester" class="form-control selitemIcon">
                            <option value="">Select</option>
                              <?php
                            if (!empty($semesterList)) {
                              foreach ($semesterList as $record)
                              {
                                $selected = '';
                                if ($record->id == $searchParam['id_semester']) {
                                  $selected = 'selected';
                                }
                            ?>
                                <option value="<?php echo $record->id;  ?>"
                                  <?php echo $selected;  ?>>
                                  <?php echo  $record->code . " - " . $record->name;  ?>
                                  </option>
                            <?php
                            }
                            }
                            ?>
                        </select>
                      </div>
                    </div>
                  </div>

                </div>



              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href="learningCenterNSemester" class="btn btn-link" >Clear All Fields</a>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Programme</th>
            <th class="text-center" colspan="2">2016 - JANUARY 2016</th>
            <th class="text-center" colspan="2">2020 - JANUARY 2020</th>
            <th class="text-center" colspan="2">2021 - JANUARY 2021</th>
            <th class="text-center" colspan="2">2016 - MAY 2020 INTAKE</th>
            <th class="text-center">Action</th>
          </tr>
        </thead>
        <tbody>
          <tr>
                <td></td>
                <td></td>
                <td class="text-center">Full Time</td>
                <td class="text-center">Part Time</td>
                <td class="text-center">Full Time</td>
                <td class="text-center">Part Time</td>
                <td class="text-center">Full Time</td>
                <td class="text-center">Part Time</td>
                <td class="text-center">Full Time</td>
                <td class="text-center">Part Time</td>
                <td></td>
          </tr>
          <tr>
                <td>1</td>
                <td>BBA - Bachelor of Business Administration</td>
                <td class="text-center">2</td>
                <td class="text-center">2</td>
                <td class="text-center">6</td>
                <td class="text-center">8</td>
                <td class="text-center">3</td>
                <td class="text-center">1</td>
                <td class="text-center">4</td>
                <td class="text-center">1</td>
                <td class="text-center">
                  <a href="<?php echo 'view/1'; ?>" title="View">View</a>
                  
                </td>
          </tr>
          <tr>
                <td>2</td>
                <td>English PhD - English PhD</td>
                <td class="text-center">2</td>
                <td class="text-center">2</td>
                <td class="text-center">6</td>
                <td class="text-center">8</td>
                <td class="text-center">3</td>
                <td class="text-center">1</td>
                <td class="text-center">4</td>
                <td class="text-center">1</td>
                <td class="text-center">
                  <a href="<?php echo 'view/1'; ?>" title="View">View</a>
                  
                </td>
          </tr>
          <tr>
                <td>3</td>
                <td>GDT - GRADUATE DIPLOMA IN TEACHING</td>
                <td class="text-center">2</td>
                <td class="text-center">2</td>
                <td class="text-center">6</td>
                <td class="text-center">8</td>
                <td class="text-center">3</td>
                <td class="text-center">1</td>
                <td class="text-center">4</td>
                <td class="text-center">1</td>
                <td class="text-center">
                  <a href="<?php echo 'view/1'; ?>" title="View">View</a>
                  
                </td>
          </tr>
          <tr>
                <td>3</td>
                <td>MBA - Management</td>
                <td class="text-center">2</td>
                <td class="text-center">2</td>
                <td class="text-center">6</td>
                <td class="text-center">8</td>
                <td class="text-center">3</td>
                <td class="text-center">1</td>
                <td class="text-center">4</td>
                <td class="text-center">1</td>
                <td class="text-center">
                  <a href="<?php echo 'view/1'; ?>" title="View">View</a>
                  
                </td>
          </tr>
          <!-- <?php
          if (!empty($courseCountList))
          {
            $i=1;
            foreach ($courseCountList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->course_code . "" . $record->course_name ?></td>
                <td><?php echo $record->student_count ?></td>
              </tr>
          <?php
          $i++;
            }
          }
          ?> -->
        </tbody>
      </table>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script>

  $('select').select2();


  function getBranchesByPartnerUniversity(id)
   {
      // $.get("/registration/courseCount/getBranchesByPartnerUniversity/"+id, function(data, status)
      // {
      //   $("#view_dummy_training_center").hide();
      //   $("#view_training_center").html(data);
      //   $("#view_training_center").show();
      // });


      var id_branch = <?php if($searchParam['id_branch'] != ''){ echo $searchParam['id_branch']; }else{ echo '0'; } ?>

      if(id_branch == '')
      {
        id_branch = 0;
      }

      var tempPR = {};
        tempPR['id_university'] = id;
        tempPR['id_branch'] = id_branch;

            $.ajax(
            {
               url: '/registration/courseCount/getBranchesByPartnerUniversity',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {

                $("#view_dummy_training_center").hide();
                $("#view_training_center").html(result);
                $("#view_training_center").show();
               }
            });

   }



  function getProgramSchemeByProgramId(id)
  {
    // alert(id);
      // $.get("/registration/courseCount/getProgramSchemeByProgramId/"+id,
      //   function(data, status)
      //   {
      //     $("#view_dummy").hide();
      //     $("#view_learning_mode").html(data);
      //     $("#view_learning_mode").show();
      //   });

      var id_learning_mode = <?php if($searchParam['id_learning_mode'] != ''){ echo $searchParam['id_learning_mode']; }else{ echo '0'; } ?>

      if(id_learning_mode == '')
      {
        id_learning_mode = 0;
      }

      var tempPR = {};
        tempPR['id_program'] = id;
        tempPR['id_learning_mode'] = id_learning_mode;

            $.ajax(
            {
               url: '/registration/courseCount/getProgramSchemeByProgramId',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
               $("#view_dummy").hide();
               $("#view_learning_mode").html(result);
               $("#view_learning_mode").show();
               }
            });


  }


  $(document).ready(function()
    {
        var id_program = "<?php echo $searchParam['id_program'];?>";


        if(id_program.length > 0)
        {
          getProgramSchemeByProgramId(id_program);
        }


        var id_university = "<?php echo $searchParam['id_university'];?>";


        if(id_university.length > 0)
        {
          getBranchesByPartnerUniversity(id_university);
        }


        $("#searchForm").validate(
        {
            rules:
            {
                id_university:
                {
                    required: true
                },
                id_semester:
                {
                    required: true
                }
            },
            messages:
            {
                id_university:
                {
                    required: "<p class='error-text'>Select Intake</p>",
                },
                id_semester:
                {
                    required: "<p class='error-text'>Select Program</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

</script>