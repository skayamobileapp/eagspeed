<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Course_count_model extends CI_Model
{
    function courseCountListSearch($data)
    {
// id_university
// id_branch
// id_program_scheme
// id_intake
// id_learning_mode
// id_program


    	if ($data['id_program'] == '' && $data['id_intake'] == '' && $data['id_learning_mode'] == '')
        {
        	return array();
        }
        else
        {
            // echo "<Pre>";print_r($data);exit();


    	// echo "<Pre>";print_r($data);exit();
        $this->db->select('DISTINCT(a.id) as id_course_landscape');
        $this->db->from('add_course_to_program_landscape as a');
        if ($data['id_program'] != '')
        {
            $this->db->where('a.id_program', $data['id_program']);
        }
        if ($data['id_intake'] != '')
        {
            $this->db->where('a.id_intake', $data['id_intake']);
        }
        if ($data['id_learning_mode'] != '')
        {
            $this->db->where('a.id_program_scheme', $data['id_learning_mode']);
        }
         $query = $this->db->get();
         $results = $query->result();  

        // echo "<Pre>";print_r($results);exit();


         $list = array();
         foreach ($results as $value)
         {
         	$id_course_landscape = $value->id_course_landscape;
            $course_data = $this->getCourseByLandscapeId($value->id_course_landscape);
            $student_count = $this->getStudentCountByLandscapeCourseIdList($data,$id_course_landscape);

            if($course_data)
            {

            $value->course_code = $course_data->code;
            $value->course_name = $course_data->name;
            $value->id_course = $course_data->id;
            $value->id_course_landscape = $id_course_landscape;
            $value->student_count = $student_count;
            
            array_push($list, $value);
            }
         }
         return $list;
     	}
    }

    function getCourseByLandscapeId($id_course_landscape)
    {
    	$this->db->select('c.*');
        $this->db->from('course as c');
        $this->db->join('add_course_to_program_landscape as acpl', 'c.id = acpl.id_course');
        $this->db->where('acpl.id', $id_course_landscape);
        $query = $this->db->get();
        return $query->row();
    }

    function getStudentCountByLandscapeCourseIdList($data,$id_course_landscape)
    {
    	$this->db->select('DISTINCT(c.id_student) as id_student');
        $this->db->from('course_registration as c');
        $this->db->where('id_course_registered_landscape', $id_course_landscape);
        $this->db->where('id_intake', $data['id_intake']);
        $this->db->where('id_programme', $data['id_program']);
        $query = $this->db->get();
        $result = $query->num_rows();

        return $result;

    }

    function branchListByStatus($status)
    {
    	$organisation = $this->getOrganisaton();
        $details = array();

        if($organisation)
        {
            array_push($details, $organisation);
        }

        $this->db->select('*');
        $this->db->from('organisation_has_training_center');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $results = $query->result();


         foreach ($results as $result)
         {
         	array_push($details, $result);
         }

         return $details;
    }

    function getOrganisaton()
    {
        $this->db->select('a.*, a.short_name as code');
        $this->db->from('organisation as a');
        $query = $this->db->get();
        $result = $query->row();  
        return $result;
    }


    function getUniversityListByStatus($status)
    {
        $organisation = $this->getOrganisaton();
        $details = array();

        if($organisation)
        {
            array_push($details, $organisation);
        }

        $this->db->select('ahemd.*');
        $this->db->from('partner_university as ahemd');
        $this->db->where('ahemd.status', $status);
        $query = $this->db->get();
        $results = $query->result();

        foreach ($results as $result)
        {
            array_push($details, $result);
        }
        return $details;
    }

    function getBranchesByPartnerUniversity($id_organisation)
    {
        $this->db->select('ahemd.*');
        $this->db->from('organisation_has_training_center as ahemd');
        $this->db->where('ahemd.id_organisation', $id_organisation);
        $query = $this->db->get();
        $results = $query->result();
        return $results;
    }


    function schemeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('scheme');
        $this->db->where('status', $status);
        $this->db->order_by("description", "ASC");
         $query = $this->db->get();
         $results = $query->result();

         return $results;
    }

    function intakeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('intake');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $results = $query->result();

         return $results;
    }

    function learningModeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme_has_scheme');
        $this->db->order_by("mode_of_program", "ASC");
         $query = $this->db->get();
         $results = $query->result();

         return $results;
    }

    function programListByStatus($status)
    {
    	$this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $results = $query->result();

         return $results;
    }


    function getAssignedStudentList($id)
    {
        $this->db->select('*');
        $this->db->from('assign_student_to_exam_center');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getProgramSchemeByProgramId($id_programme)
    {
        $this->db->select('DISTINCT(ihs.mode_of_program) as mode_of_program, ihs.*');
        $this->db->from('programme_has_scheme as ihs');
        $this->db->where('ihs.id_program', $id_programme);
        $query = $this->db->get();
        return $query->result();
    }
}