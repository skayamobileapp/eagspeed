<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class PoEntry extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Po_model');
        $this->load->model('Pr_model');
        $this->isLoggedIn();
    }

    function list()
    {

        if ($this->checkAccess('pr_entry.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['id_vendor'] = $this->security->xss_clean($this->input->post('id_vendor'));
            $formData['id_financial_year'] = $this->security->xss_clean($this->input->post('id_financial_year'));
            $formData['department_code'] = $this->security->xss_clean($this->input->post('department_code'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['status'] = '';
 
            $data['searchParam'] = $formData;

            $data['poList'] = $this->Po_model->getPOListSearch($formData);

            $data['financialYearList'] = $this->Pr_model->financialYearListByStatus('1');
            $data['vendorList'] = $this->Pr_model->vendorListByStatus('Approved');
            $data['departmentList'] = $this->Pr_model->departmentListByStatus('1');
            $data['departmentCodeList'] = $this->Pr_model->getDepartmentCodeList();


            $this->global['pageTitle'] = 'FIMS : List Procurement Category';
            //print_r($subjectDetails);exit;
            $this->loadViews("po_entry/list", $this->global, $data, NULL);
        }
    }

     function add()
    {

        if ($this->checkAccess('pr_entry.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {


                $id = $this->security->xss_clean($this->input->post('type_of_pr'));
                $id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;
                $generated_number = $this->Po_model->generatePONumber();

                $prMaster = $this->Pr_model->getMasterPrDetails($id);

                $prDetails = $this->Pr_model->getPRDetailsByMasterID($id);

                $description = $this->security->xss_clean($this->input->post('description'));

     
                

                $data = array(
                    'id_pr' => $id,
                    'po_number' =>$generated_number,
                    'po_entry_date' => $prMaster->pr_entry_date,
                    'type' => $prMaster->type,
                    'id_financial_year' => $prMaster->id_financial_year,
                    'id_budget_year' => $prMaster->id_budget_year,
                    'department_code' => $prMaster->department_code,
                    'id_department' => $prMaster->id_department,
                    'id_vendor' => $prMaster->id_vendor,
                    'amount' => $prMaster->amount,
                    'description' => $description,
                    'created_by' => $user_id,
                    'status'=>'0'
                );
               
                $inserted_id = $this->Po_model->addPO($data);

                if($inserted_id)
                {
                    $update_data = array('is_po' => $inserted_id);
                    $updated_pr = $this->Pr_model->updatePR($update_data,$id);
                }
                


                // $temp_details = $this->Pr_model->getPRDetailsByMasterID($id);
                     // echo "<Pre>";print_r($temp_details);exit;
                 for($i=0;$i<count($prDetails);$i++)
                 {

                        $dt_fund = $prDetails[$i]->dt_fund;
                        $dt_department = $prDetails[$i]->dt_department;
                        $dt_activity = $prDetails[$i]->dt_activity;
                        $dt_account = $prDetails[$i]->dt_account;
                        $cr_fund = $prDetails[$i]->cr_fund;
                        $cr_department = $prDetails[$i]->cr_department;
                        $cr_activity = $prDetails[$i]->cr_activity;
                        $cr_account = $prDetails[$i]->cr_account;
                        $type = $prDetails[$i]->type;
                        $id_category = $prDetails[$i]->id_category;
                        $id_sub_category = $prDetails[$i]->id_sub_category;
                        $id_item = $prDetails[$i]->id_item;
                        $quantity = $prDetails[$i]->quantity;
                        $price = $prDetails[$i]->price;
                        $id_tax = $prDetails[$i]->id_tax;
                        $tax_price = $prDetails[$i]->tax_price;
                        $total_final = $prDetails[$i]->total_final;
                    

                     $detailsData = array(
                        'id_po_entry'=>$inserted_id,
                        'dt_fund'=>$dt_fund,
                        'dt_department'=>$dt_department,
                        'dt_activity'=>$dt_activity,
                        'dt_account'=>$dt_account,
                        'cr_fund'=>$cr_fund,
                        'cr_department'=>$cr_department,
                        'cr_activity'=>$cr_activity,
                        'cr_account'=>$cr_account,
                        'type'=>$type,
                        'id_category'=>$id_category,
                        'id_sub_category'=>$id_sub_category,
                        'id_item'=>$id_item,
                        'quantity'=>$quantity,
                        'balance_qty'=>$quantity,
                        'received_qty'=>'0',
                        'price'=>$price,
                        'id_tax'=>$id_tax,
                        'tax_price'=>$tax_price,
                        'total_final'=>$total_final

                    );
                     // echo "<Pre>";print_r($detailsData);exit;
                    $result = $this->Po_model->addNewPODetails($detailsData);
                }

                $this->Pr_model->deleteFullSessionData($id_session);
                redirect('/procurement/poEntry/list');
              }
            }

            $data['prEntryList'] = $this->Pr_model->getPRListForProcess('PO');
            $this->global['pageTitle'] = 'FIMS : List Procurement Category';
            // echo "<Pre>";print_r($data['prEntryList']);exit;
            $this->loadViews("po_entry/add", $this->global, $data, NULL);
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('pr_entry.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            if ($id == null)
            {
                redirect('/procurement/pr_entry/list');
            }
            if($this->input->post())
            {
                print_r($this->input->post());exit;
            }
            //  $data['fundCodeList'] = $this->Pr_model->getFundCodeList();
            // $data['departmentCodeList'] = $this->Pr_model->getDepartmentCodeList();
            // $data['accountCodeList'] = $this->Pr_model->getAccountCodeList();
            // $data['activityCodeList'] = $this->Pr_model->getActivityCodeList();
            // $data['prCategoryList'] = $this->Pr_model->prCategoryList();
            // $data['prSubCategoryList'] = $this->Pr_model->prSubCategoryList();
            // $data['prItemList'] = $this->Pr_model->prItemList();
            // $data['departmentList'] = $this->Pr_model->departmentList();
            // $data['vendorList'] = $this->Pr_model->vendorListByStatus('Approved');
            
            $data['poMaster'] = $this->Po_model->getMasterPoDetails($id);
            $data['poDetails'] = $this->Po_model->getPoDetails($id);

            // echo "<Pre>";print_r($data);exit();
            

            $this->global['pageTitle'] = 'FIMS : Edit Procurement Category';
            $this->loadViews("po_entry/edit", $this->global, $data, NULL);
        }
    }

      function view($id = NULL)
    {
        if ($this->checkAccess('pr_entry.view') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            if ($id == null)
            {
                redirect('/procurement/poEntry/list');
            }
            if($this->input->post())
            {
                $status = $this->security->xss_clean($this->input->post('status'));
                $reason = $this->security->xss_clean($this->input->post('reason'));


                $data = array(
                    'status' => $status,
                    'reason' => $reason
                );
                 $result = $this->Po_model->editPOList($data,$id);

                redirect('/procurement/poEntry/approvallist');

                // $check_duplication = $this->budget_amount_model->editBudgetAmount($data,$id);

                // print_r($this->input->post());exit;
            }
            //  $data['fundCodeList'] = $this->Pr_model->getFundCodeList();
            // $data['departmentCodeList'] = $this->Pr_model->getDepartmentCodeList();
            // $data['accountCodeList'] = $this->Pr_model->getAccountCodeList();
            // $data['activityCodeList'] = $this->Pr_model->getActivityCodeList();
            // $data['prCategoryList'] = $this->Pr_model->prCategoryList();
            // $data['prSubCategoryList'] = $this->Pr_model->prSubCategoryList();
            // $data['prItemList'] = $this->Pr_model->prItemList();
            // $data['departmentList'] = $this->Pr_model->departmentList();
            // $data['vendorList'] = $this->Pr_model->vendorListByStatus('Approved');
            
            $data['poMaster'] = $this->Po_model->getMasterPoDetails($id);
            $data['poDetails'] = $this->Po_model->getPoDetails($id);

            // echo "<Pre>";print_r($data);exit();
            

            $this->global['pageTitle'] = 'FIMS : Edit Procurement Category';
            $this->loadViews("po_entry/view", $this->global, $data, NULL);
        }
    }

    function approvallist()
    {
        if ($this->checkAccess('po_entry.approval') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $resultprint = $this->input->post();

           if($resultprint)
            {
             switch ($resultprint['button'])
             {
                case 'approve':

                     for($i=0;$i<count($resultprint['checkvalue']);$i++)
                        {

                         $id = $resultprint['checkvalue'][$i];
                         
                         $result = $this->Po_model->editPOList($id);
                        }
                        redirect($_SERVER['HTTP_REFERER']);
                     break;


                case 'search':
                     
                     break;
                 
                default:
                     break;
             }
                
            }


            
            $formData['id_vendor'] = $this->security->xss_clean($this->input->post('id_vendor'));
            $formData['id_financial_year'] = $this->security->xss_clean($this->input->post('id_financial_year'));
            $formData['department_code'] = $this->security->xss_clean($this->input->post('department_code'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));


            $formData['status'] = '0';

            $data['searchParam'] = $formData;

            $data['poList'] = $this->Po_model->getPOListSearch($formData);

            $data['financialYearList'] = $this->Pr_model->financialYearListByStatus('1');
            $data['vendorList'] = $this->Pr_model->vendorListByStatus('Approved');
            $data['departmentList'] = $this->Pr_model->departmentListByStatus('1');
            $data['departmentCodeList'] = $this->Pr_model->getDepartmentCodeList();
            

            // echo "<Pre>";print_r($formData);exit();
            
                // $array = $this->security->xss_clean($this->input->post('checkvalue'));
                // if (!empty($array))
                // {

                //     $result = $this->Po_model->editPOList($array);
                //     redirect($_SERVER['HTTP_REFERER']);
                // }


            $this->global['pageTitle'] = 'FIMS : Approve Vendor';
            $this->loadViews("po_entry/approval_list", $this->global, $data, NULL);

        }
    }



    function getData($id)
    {
        // echo "<Pre>";print_r($id);exit;

        // $financialYearList = $this->Pr_model->financialYearListByStatus('1');
        $vendorList = $this->Pr_model->vendorListByStatus('Approved');

        $prMaster = $this->Pr_model->getMasterPrDetails($id);
        $prDetails = $this->Pr_model->getPrDetails($id);


        $array = $this->Pr_model->financialYearListByStatus('1');

        // $prDetails = $data['prDetails'];
        $financialYearList = json_decode( json_encode($array), true);

        // $vendorList = json_decode( json_encode($vendorList), true);
        // $financialYearList = json_decode( json_encode($array), true);

        // echo "<Pre>";print_r($prMaster);exit;




        // <div class='container-fluid page-wrapper'>
        $table = "

            <script type='text/javascript'>
                $('select').select2();
            </script>




                <div class='row'>
                    <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>PR Number <span class='error-text'>*</span></label>
                            <input type='text' class='form-control' readonly='readonly' value='$prMaster->pr_number'>
                        </div>
                    </div>

                   <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>Type <span class='error-text'>*</span></label>
                            <input type='text' class='form-control' readonly='readonly' value='$prMaster->type'>
                        </div>
                    </div>

                    <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>PR Entry Date <span class='error-text'>*</span></label>
                            <input type='text' class='form-control datepicker' readonly='readonly' value='$prMaster->pr_entry_date'>
                        </div>
                    </div>

                    
                </div>

                <div class='row'>
                    <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>Financial Year <span class='error-text'>*</span></label>
                            <input type='text' class='form-control'  value='$prMaster->financial_year' readonly='readonly'>
                        </div>
                    </div>

                     <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>Budget Year <span class='error-text'>*</span></label>
                            <input type='text' class='form-control'  value='$prMaster->budget_year' readonly='readonly'>
                        </div>
                    </div>

                     <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>Department <span class='error-text'>*</span></label>
                            <input type='text' class='form-control' value='$prMaster->department_code - $prMaster->department' readonly='readonly'>
                        </div>
                    </div>

                </div>

                <div class='row'>

                  <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>Vendor <span class='error-text'>*</span></label>
                            <input type='text' class='form-control' value='$prMaster->vendor_code - $prMaster->vendor' readonly='readonly'>
                        </div>
                    </div>

                    <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>PR Description <span class='error-text'>*</span></label>
                            <input type='text' readonly='readonly' class='form-control' name='pr_description'  value='$prMaster->description'>
                        </div>
                    </div>

                    <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>PR Total Amount <span class='error-text'>*</span></label>
                            <input type='text' class='form-control'  value='$prMaster->amount' readonly='readonly'>
                        </div>
                    </div>

                 </div>

                <div class='row'>

                   
                    <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>PO Description <span class='error-text'>*</span></label>
                            <input type='text' class='form-control' name='description'>
                        </div>
                    </div>
                </div>

                ";
                // </div>


                $table1="

            

            <select name='id_vendor' id='id_vendor' class='form-control'>";
            $table1.="<option value=''>Select</option>";

            for($i=0;$i<count($vendorList);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $vendorList[$i]->id;
            $code = $vendorList[$i]->code;
            $name = $vendorList[$i]->name;
            $table1.="<option value=".$id.">";

                    if($id == $prMaster->id_vendor)
                    {
                      // echo "selected=selected";
                    }
                    // echo $code . " - " . $name; 

                    $table1.="</option>";

            }
            $table1.="
            </select>
                 </div>
              </div>
        </div>
            ";






        $table.="

        <h3> &nbsp;&nbsp;&nbsp;PR Details </h3>

        <div class='custom-table'>

        <table class='table' width='100%'>
        <thead>
         <tr>
             <th>Sl. No</th>
             <th>Debit GL Code</th>
             <th>Credit GL Code</th>
             <th>Category</th>
             <th>Sub Category</th>
             <th>Item</th>
             <th>Tax</th>
             <th>Qty</th>
             <th>Tax Amount</th>
             <th>Price</th>
             <th>Total Amount</th>
         </tr>
        </thead>
        <tbody>";
            $total_detail = 0;
          for($i=0;$i<count($prDetails);$i++)
          {
          $j=$i+1; 
            $id = $prDetails[$i]->id;
            $type = $prDetails[$i]->type;
            $dt_account = $prDetails[$i]->dt_account;
            $dt_activity = $prDetails[$i]->dt_activity;
            $dt_department = $prDetails[$i]->dt_department;
            $dt_fund = $prDetails[$i]->dt_fund;
            $cr_account = $prDetails[$i]->cr_account;
            $cr_activity = $prDetails[$i]->cr_activity;
            $cr_department = $prDetails[$i]->cr_department;
            $cr_fund = $prDetails[$i]->cr_fund;
            $category_name = $prDetails[$i]->category_name;
            $sub_category_name = $prDetails[$i]->sub_category_name;
            $item_name = $prDetails[$i]->item_name;
            $tax_code = $prDetails[$i]->tax_code;
            $tax_name = $prDetails[$i]->tax_name;
            $quantity = $prDetails[$i]->quantity;
            $price = $prDetails[$i]->price;
            $tax_price = $prDetails[$i]->tax_price;
            $total_final = $prDetails[$i]->total_final;
            $j=$i+1;
            $table .= "
            <tbody>
            <tr>
                <td>$j</td>
                <td>$dt_fund - $dt_department - $dt_activity - $dt_account</td>
                <td>$cr_fund - $cr_department - $cr_activity - $cr_account</td>
                <td>$category_name</td>
                <td>$sub_category_name</td>
                <td>$item_name</td>
                <td>$tax_code - $tax_name</td>
                <td>$quantity</td>
                <td>$tax_price</td>
                <td>$price</td>
                <td>$total_final</td>
            </tr>";
            $total_detail = $total_detail + $total_final;
         }
         $total_detail = number_format($total_detail, 2, '.', ',');

        $table.="
        <tr>
            <td bgcolor='' colspan='9'></td>
            <td bgcolor=''><b> Total : </b></td>
            <td bgcolor=''><b>".$total_detail."</b></td>
        </tr>

        </tbody>
        </table>
        </div>";
       echo $table;

    }
}
