<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ProcurementCategory extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('procurement_category_model');
        $this->isLoggedIn();
    }

    function list()
    {

        if ($this->checkAccess('procurement_category.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;

            $data['procurementCategoryList'] = $this->procurement_category_model->procurementCategoryListSearch($name);
            $this->global['pageTitle'] = 'FIMS : List Procurement Category';
            //print_r($subjectDetails);exit;
            $this->loadViews("procurement_category/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('procurement_category.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'code' => $code,
                    'description' => $description,
                    'status' => $status
                );
            
                $result = $this->procurement_category_model->addNewProcurementCategory($data);
                redirect('/procurement/procurementCategory/list');
            }
            
            $this->global['pageTitle'] = 'FIMS : Add Procurement Category';
            $this->loadViews("procurement_category/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('procurement_category.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/procurement/procurement_category/list');
            }
            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'code' => $code,
                    'description' => $description,
                    'status' => $status
                );
                
                $result = $this->procurement_category_model->editProcurementCategory($data,$id);
                redirect('/procurement/procurementCategory/list');
            }
            $data['procurementCategory'] = $this->procurement_category_model->getProcurementCategoryDetails($id);
            $this->global['pageTitle'] = 'FIMS : Edit Procurement Category';
            $this->loadViews("procurement_category/edit", $this->global, $data, NULL);
        }
    }

   
}
