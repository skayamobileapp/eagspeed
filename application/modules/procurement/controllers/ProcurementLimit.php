<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ProcurementLimit extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('procurement_limit_model');
        $this->isLoggedIn();
    }

    function list()
    {

        if ($this->checkAccess('procurement_limit.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;

            $data['procurementLimitList'] = $this->procurement_limit_model->procurementLimitListSearch($name);
            $this->global['pageTitle'] = 'FIMS : List Procurement Category';
            //print_r($subjectDetails);exit;
            $this->loadViews("procurement_limit/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('procurement_limit.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $from_limit = $this->security->xss_clean($this->input->post('from_limit'));
                $to_limit = $this->security->xss_clean($this->input->post('to_limit'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'name' => $name,
                    'from_limit' => $from_limit,
                    'to_limit' => $to_limit,
                    'status' => $status
                );
            
                $result = $this->procurement_limit_model->addNewProcurementLimit($data);
                redirect('/procurement/procurementLimit/list');
            }
            
            $this->global['pageTitle'] = 'FIMS : Add Procurement Category';
            $this->loadViews("procurement_limit/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('procurement_limit.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/procurement/procurement_limit/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $from_limit = $this->security->xss_clean($this->input->post('from_limit'));
                $to_limit = $this->security->xss_clean($this->input->post('to_limit'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'name' => $name,
                    'from_limit' => $from_limit,
                    'to_limit' => $to_limit,
                    'status' => $status
                );
                // echo "<Pre>";print_r($data);exit;
                
                $result = $this->procurement_limit_model->editProcurementLimit($data,$id);
                redirect('/procurement/procurementLimit/list');
            }
            $data['procurementLimit'] = $this->procurement_limit_model->getProcurementLimit($id);
            $this->global['pageTitle'] = 'FIMS : Edit Procurement Category';
            $this->loadViews("procurement_limit/edit", $this->global, $data, NULL);
        }
    }
}
