<div class="container-fluid page-wrapper">
    

    <form id="form_vendor_approve" action="" method="post">
    <div class="main-container clearfix">
        <div class="page-title clearfix">

        <h3>Block Vendor</h3>
            <!-- <h3>Edit Student</h3> -->
        </div>



  
   <div class="row">
     <div class="m-auto text-center">
       <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
     </div>
     <div class="container" id="tabs">
        <ul class="nav nav-tabs offers-tab text-center" role="tablist" >
            <li role="nav-item" class="active"><a href="#personal" class="nav-link active border rounded text-center"
                    aria-controls="personal" aria-selected="true"
                    role="tab" data-toggle="tab">Personal Details</a>
            </li>
            <li role="nav-item"><a href="#company" class="nav-link company border rounded text-center" aria-selected="false"
                    aria-controls="company" role="tab" data-toggle="tab">Company Details</a>
            </li>
            <li role="nav-item"><a href="#bank" class="nav-link bank border rounded text-center" aria-selected="false"
                    aria-controls="bank" role="tab" data-toggle="tab">Bank Details</a>
            </li>
            <li role="presentation"><a href="#billing" class="nav-link border rounded text-center"
                    aria-controls="billing" role="tab" data-toggle="tab">Billing Person Details</a>
            </li>
        </ul>
        <div class="tab-content offers-tab-content">
          <div role="tabpanel" class="tab-pane active" id="personal">
            <div class="col-12 mt-4">
                <br> 

            <div class="form-container">
            <h4 class="form-group-title">Vendor Details</h4>


                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Vendor Name <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="vname" name="vname" value="<?php echo $vendorRegistration->name; ?>" readonly>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Email <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="vemail" name="vemail" value="<?php echo $vendorRegistration->email; ?>" readonly>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Phone Number <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="vphone" name="vphone" value="<?php echo $vendorRegistration->phone; ?>" readonly>
                        </div>
                    </div>
                </div>

                <div class="row">

                     <div class="col-sm-4">
                        <div class="form-group">
                            <label>Gender <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="vphone" name="vphone" value="<?php echo $vendorRegistration->gender; ?>" readonly>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>NRIC <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="vnric" name="vnric" value="<?php echo $vendorRegistration->nric; ?>" readonly>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Mobile Number <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="vmobile" name="vmobile" value="<?php echo $vendorRegistration->mobile; ?>" readonly>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Address One <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="vaddress_one" name="vaddress_one" value="<?php echo $vendorRegistration->address_one; ?>" readonly>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Address Two <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="vaddress_two" name="vaddress_two" value="<?php echo $vendorRegistration->address_two; ?>" readonly>
                        </div>
                    </div>

                     <div class="col-sm-4">
                        <div class="form-group">
                            <label>Country <span class='error-text'>*</span></label>
                            <select name="ccountry" id="ccountry" class="form-control" style="width: 360px" disabled="disabled">
                                <option value="">Select</option>
                                <?php
                                if (!empty($countryList)) {
                                  foreach ($countryList as $record)
                                  {
                                    $selected = '';
                                    if ($record->id == $vendorRegistration->country) {
                                      $selected = 'selected';
                                    }
                                ?>
                                    <option value="<?php echo $record->id;  ?>"
                                      <?php echo $selected;  ?>>
                                      <?php echo $record->name;  ?>
                                      </option>
                                <?php
                                  }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                </div>


                <div class="row">

                     <div class="col-sm-4">
                        <div class="form-group">
                            <label>State <span class='error-text'>*</span></label>
                            <select name="ccountry" id="ccountry" class="form-control" style="width: 360px" disabled="disabled">
                                <option value="">Select</option>
                                <?php
                                if (!empty($stateList)) {
                                  foreach ($stateList as $record)
                                  {
                                    $selected = '';
                                    if ($record->id == $vendorRegistration->state) {
                                      $selected = 'selected';
                                    }
                                ?>
                                    <option value="<?php echo $record->id;  ?>"
                                      <?php echo $selected;  ?>>
                                      <?php echo $record->name;  ?>
                                      </option>
                                <?php
                                  }
                                }
                                ?>
                            </select>
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>zipcode / Pincode <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="vzipcode" name="vzipcode" value="<?php echo $vendorRegistration->zipcode; ?>" readonly>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Date Of Birth <span class='error-text'>*</span></label>
                            <input type="text" class="form-control datepicker" id="date_of_birth" name="date_of_birth" value="<?php echo date('d-m-Y', strtotime($vendorRegistration->date_of_birth)); ?>" readonly>
                        </div>
                    </div>
                </div>


                <div class="row">

                     <div class="col-sm-4">
                        <div class="form-group">
                            <label>Vendor Code <span class='error-text'>*</span></label>
                            <input type="text" class="form-control datepicker" id="date_of_birth" name="date_of_birth" value="<?php echo $vendorRegistration->code; ?>" readonly>
                        </div>
                    </div>

            <?php
            if($vendorRegistration->vendor_status == 'Approved')
            {
             ?>

                <div class="col-sm-3">
                        <div class="form-group">
                            <label>Expire Date <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="tax_no" name="tax_no" value="<?php echo date('d-m-Y', strtotime($vendorRegistration->expire_date)); ?>" readonly>
                        </div>
                </div>

            <?php
            }
             ?>

                </div>

            </div>

                

            <div class="button-block clearfix">
                <div class="bttn-group pull-right">
                    <button type="button" class="btn btn-primary btn-lg btnNext" >Next</button>
                </div>
            </div>
             
         </div> <!-- END col-12 -->  
        </div>

        <div role="tabpanel" class="tab-pane" id="company">
            <div class="col-12 mt-4">
                <br>
            <div class="form-container">
            <h4 class="form-group-title">Company Details</h4>

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Company Name <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="cname" name="cname" value="<?php echo $vendorCompany->name; ?>" readonly>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Email <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="cemail" name="cemail" value="<?php echo $vendorCompany->email; ?>" readonly>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Phone Number <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="cphone" name="cphone" value="<?php echo $vendorCompany->phone; ?>" readonly>
                        </div>
                    </div>
                </div>

                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Tax Number <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="tax_no" name="tax_no" value="<?php echo $vendorCompany->tax_no; ?>" readonly>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Address One <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="tax_no" name="tax_no" value="<?php echo $vendorCompany->address_one; ?>" readonly>
                        </div>
                    </div>

                   <div class="col-sm-4">
                        <div class="form-group">
                            <label>Address Two <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="tax_no" name="tax_no" value="<?php echo $vendorCompany->address_two; ?>" readonly>
                        </div>
                    </div>

                </div>
                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>State <span class='error-text'>*</span></label>
                            <select name="ccountry" id="ccountry" class="form-control" style="width: 360px" disabled="disabled">
                                <option value="">Select</option>
                                <?php
                                if (!empty($countryList)) {
                                  foreach ($countryList as $record)
                                  {
                                    $selected = '';
                                    if ($record->id == $vendorCompany->country) {
                                      $selected = 'selected';
                                    }
                                ?>
                                    <option value="<?php echo $record->id;  ?>"
                                      <?php echo $selected;  ?>>
                                      <?php echo $record->name;  ?>
                                      </option>
                                <?php
                                  }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                   <div class="col-sm-4">
                        <div class="form-group">
                            <label>Country <span class='error-text'>*</span></label>
                            <select name="ccountry" id="ccountry" class="form-control" style="width: 360px" disabled="disabled">
                                <option value="">Select</option>
                                <?php
                                if (!empty($stateList)) {
                                  foreach ($stateList as $record)
                                  {
                                    $selected = '';
                                    if ($record->id == $vendorCompany->state) {
                                      $selected = 'selected';
                                    }
                                ?>
                                    <option value="<?php echo $record->id;  ?>"
                                      <?php echo $selected;  ?>>
                                      <?php echo $record->name;  ?>
                                      </option>
                                <?php
                                  }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                   <div class="col-sm-4">
                        <div class="form-group">
                            <label>Address Two <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="zipcode" name="zipcode" value="<?php echo $vendorCompany->zipcode; ?>" readonly>
                        </div>
                    </div>
                </div>

            </div>

            <div class="button-block clearfix">
                <div class="bttn-group pull-right">
                    <button type="button" class="btn btn-primary btn-lg btnNext2">Next</button>
                </div>
            </div>  

         </div> <!-- END col-12 -->  
        </div>


        <div role="tabpanel" class="tab-pane" id="bank">
            <div class="col-12 mt-4">
                <br>

        <div class="form-container">
            <h4 class="form-group-title">Bank Details</h4>

            <div class="custom-table">
                <table class="table">
                    <thead>
                        <tr>
                        <th>Sl. No</th>
                         <th>Bank Name</th>
                         <th>Address</th>
                         <th>Country</th>
                         <th>State</th>
                         <th>City</th>
                         <th>Zipcode</th>
                         <th>Branch Number</th>
                         <th>Account Number</th>
                        </tr>
                    </thead>
                    <tbody>
                         <?php
                     $total = 0;
                      for($i=0;$i<count($vendorBankList);$i++)
                     { ?>
                        <tr>
                        <td><?php echo $i+1;?></td>
                        <td><?php echo $vendorBankList[$i]->bank_code . " - " . $vendorBankList[$i]->bank_name;?></td>
                        <td><?php echo $vendorBankList[$i]->address;?></td>
                        <td><?php echo $vendorBankList[$i]->country;?></td>
                        <td><?php echo $vendorBankList[$i]->state;?></td>
                        <td><?php echo $vendorBankList[$i]->city;?></td>
                        <td><?php echo $vendorBankList[$i]->zipcode;?></td>
                        <td><?php echo $vendorBankList[$i]->branch_no;?></td>
                        <td><?php echo $vendorBankList[$i]->acc_no;?></td>

                         </tr>
                      <?php 
                    } 
                  ?>
                    </tbody>
                </table>
            </div>

        </div>



                


        <div class="button-block clearfix">
            <div class="bttn-group pull-right">
                <button type="button" class="btn btn-primary btn-lg btnNext3" >Next</button>
            </div>
        </div>

            <div class="custom-table">
                <div id="view"></div>
            </div>
             
         </div> <!-- END col-12 -->  
        </div>

        <div role="tabpanel" class="tab-pane" id="billing">
            <div class="col-12 mt-4">
                <br>


                

        <div class="form-container">
            <h4 class="form-group-title">Billing Details</h4>


            <div class="custom-table">
                <table class="table">
                    <thead>
                        <tr>
                         <th>Sl. No</th>
                         <th>Name</th>
                         <th>NRIC</th>
                         <th>Email</th>
                         <th>Phone Number</th>
                        </tr>
                    </thead>
                    <tbody>
                         <?php
                     $total = 0;
                      for($i=0;$i<count($vendorBillingList);$i++)
                     { ?>
                        <tr>
                        <td><?php echo $i+1;?></td>
                        <td><?php echo $vendorBillingList[$i]->name;?></td>
                        <td><?php echo $vendorBillingList[$i]->nric;?></td>
                        <td><?php echo $vendorBillingList[$i]->email;?></td>
                        <td><?php echo $vendorBillingList[$i]->phone;?></td>

                         </tr>
                      <?php 
                    } 
                  ?>
                    </tbody>
                </table>
            </div>

        </div>


                <div class="button-block clearfix">
                    <div class="bttn-group pull-right">
                    </div>
                </div>

            <div class="custom-table">
                <div id="view1"></div>
            </div>
             
         </div> <!-- END col-12 -->  
        </div>

      </div>
     </div>

   </div> <!-- END row-->



            <div class="row">

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Status <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="status" name="status" value="<?php  echo $vendorRegistration->vendor_status; ?>" readonly="readonly">
                    </div>
                </div>

             <?php
            if($vendorRegistration->vendor_status == 'Blocked')
            {
             ?>

              <div class="col-sm-3">
                        <div class="form-group">
                            <label>Blocked Reason <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="tax_no" name="tax_no" value="<?php echo $vendorRegistration->reason; ?>" readonly>
                        </div>
                </div>


            <?php
            }
            ?>




        <?php
            if($vendorRegistration->vendor_status == 'Approved')
            {
             ?>




          

                <div class="col-sm-3">
                    <div class="form-group">
                        <p> Block Vendor<span class='error-text'>*</span></p>
                       
                        <label class="radio-inline">
                            <input type="radio" id="ed2" name="status" value="Blocked" onclick="showRejectField()"><span class="check-radio"></span> Block
                        </label>
                    </div>
                </div>

                <div class="col-sm-3" id="view_reject" style="display: none">
                    <div class="form-group">
                        <label>Reason <span class='error-text'>*</span></label>
                        <input type="text" id="reason" name="reason" class="form-control">
                    </div>
                </div>


          <?php
            }
            ?>

            </div>


        </form>


    </div>

   

            <div class="button-block clearfix">
                <div class="bttn-group pull-right pull-right">
             <?php
            if($vendorRegistration->vendor_status == 'Approved')
            {
             ?>
                    <button type="button" class="btn btn-primary btn-lg" onclick="validateData()">Save</button>

             <?php
            }
            ?>
                    <a href="../blockList" class="btn btn-link">Cancel</a>
                </div>
            </div>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script type="text/javascript">

    function validateData()
    {
         if($('#form_vendor_approve').valid())
        {
            $('#form_vendor_approve').submit();
        }
    }

     $(document).ready(function() {
        $("#form_vendor_approve").validate({
            rules: {
                status: {
                    required: true
                },
                 reason: {
                    required: true
                }
            },
            messages: {
                status: {
                    required: "<p class='error-text'>Select Status</p>",
                },
                reason: {
                    required: "<p class='error-text'>Reason Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


     function showRejectField(){
            $("#view_reject").show();
    }

    function hideRejectField(){
            $("#view_reject").hide();
    }



    $(document).ready(function() {
      $('.btnNext').click(function() {
        $('.nav-tabs .active').parent().next('li').find('a').trigger('click');
      });
      $('.btnNext2').click(function() {
        $('.nav-tabs .company').parent().next('li').find('a').trigger('click');
      });
      $('.btnNext3').click(function() {
        $('.nav-tabs .bank').parent().next('li').find('a').trigger('click');
      });
    });

    $('select').select2();

    $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
    } );

</script>