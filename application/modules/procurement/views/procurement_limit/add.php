<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Procurement Limit</h3>
        </div>
        <form id="form_grade" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Procurement Limit Details</h4>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Procurement Name <span class='error-text'>*</span></label>
                        <select name="name" id="name" class="form-control">
                            <option value="">Select</option>
                            <option value="PO">PO</option>
                            <option value="Direct Purchase">Direct Purchase</option>
                            <option value="Warrant">Warrant</option>
                            <option value="Tender">Tender</option>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Min. Limit <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="from_limit" name="from_limit">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Max. Limit <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="to_limit" name="to_limit">
                    </div>
                </div>

            </div>


            <div class="row">

                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                            </label>                              
                        </div>                         
                </div>

            </div>

        </div>
        

        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>
        

        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>
    $(document).ready(function() {
        $("#form_grade").validate({
            rules: {
                name: {
                    required: true
                },
                 from_limit: {
                    required: true
                },
                 to_limit: {
                    required: true
                },
                 status: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                from_limit: {
                    required: "<p class='error-text'>Min. Limit Required</p>",
                },
                to_limit: {
                    required: "<p class='error-text'>Max. Limit Required</p>",
                },
                status: {
                    required: "<p class='error-text'>Status Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script type="text/javascript">
    $('select').select2();
</script>