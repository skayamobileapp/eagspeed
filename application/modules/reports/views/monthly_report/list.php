<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>Monthly Report</h3>
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
           <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">
                


             
               <div class="row">

                  <div class="col-sm-6">

                    <div class="form-group">
                      <label class="col-sm-4 control-label">From Date</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control datepicker" id="from_date" name="from_date" value="<?php echo $searchParam['from_date']; ?>">
                      </div>
                    </div>

                  </div>
                  <div class="col-sm-6">
                      <div class="form-group">
                      <label class="col-sm-4 control-label">To Date</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control datepicker" name="to_date" name="to_date" value="<?php echo $searchParam['to_date']; ?>">
                      </div>
                    </div>

                  </div>

              </div>

               <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Provider </label>
                    <div class="col-sm-8">
                      <select name="partner_university_id" id="partner_university_id" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($organisationList)) {
                          foreach ($organisationList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['partner_university_id']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>                      

              </div> 



              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <button type="reset" class="btn btn-link" onclick="clearSearchForm()">Clear All Fields</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <th>Sl. No</th>
            <th class="text-center">Date</th>
            <th class="text-center">Registraiton</th>
            <th class="text-center">Paid</th>
            <th class="text-center">In-Progress</th>
            <th class="text-center">Completed</th>
          </tr>
        </thead>
        <tbody>
             <?php for($i=0;$i<count($eventDate);$i++){?>
              <tr>
                <td><?php echo $i+1;?></td>
                <td class="text-center"><?php echo date('M-Y', strtotime($eventDate[$i]));?></td>
                <td class="text-center">
                  <a href="/reports/monthlyReport/view/<?php echo date('Y-m-01', strtotime($eventDate[$i]));?>/<?php echo date('Y-m-31', strtotime($eventDate[$i]));?>"><?php echo $regCount[$i];?></a></td>
                <td class="text-center"><a href="/reports/monthlyReport/paid/<?php echo date('Y-m-01', strtotime($eventDate[$i]));?>/<?php echo date('Y-m-31', strtotime($eventDate[$i]));?>"><?php echo $paidCount[$i];?></a></td>
                <td class="text-center"><a href="/reports/monthlyReport/progress/<?php echo date('Y-m-01', strtotime($eventDate[$i]));?>/<?php echo date('Y-m-31', strtotime($eventDate[$i]));?>"><?php echo $inprogressCount[$i];?></a></td>
                <td class="text-center"><a href="/reports/monthlyReport/completed/<?php echo date('Y-m-01', strtotime($eventDate[$i]));?>/<?php echo date('Y-m-31', strtotime($eventDate[$i]));?>"><?php echo $completedCount[$i];?></a></td>

             <?php } ?> 
          
        </tbody>
      </table>
    </div>
        <div id="container"></div>


  </div>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>


  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script type='text/javascript'>

  Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Month Wise Report'
    },
    subtitle: {
        text: ' '
    },
    xAxis: {
        categories: [<?php echo $stringeventDate;?>],
        crosshair: true
    },
    yAxis: {
        title: {
            useHTML: true,
            text: 'Number of Registration'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [{
        name: 'Registration',
        data: [<?php echo $stringregistrationCout;?>]

    },
    {
        name: 'Paid',
        data: [<?php echo $stringpaymentCout;?>]

    },
    {
        name: 'In-Progress',
        data: [<?php echo $stringinprogressCout;?>]

    },
    {
        name: 'Completed',
        data: [<?php echo $stringcompletedCout;?>]

    }]
});


    $('select').select2();
  
    function downloadInvoiceReportCSV()
    {
      var tempPR = {};
      tempPR['from_date'] = $("#from_date").val();
      tempPR['to_date'] = $("#to_date").val();
              
      // alert(tempPR['to_date']);

          $.ajax(
          {
             url: '/reports/invoiceReport/downloadInvoiceReportCSV',
              type: 'POST',
             data:
             {
              tempData: tempPR
             },
             error: function()
             {
              alert('Something is wrong');
             },
             success: function(result)
             {
              // alert(result);
              $("view_data").val(result);
              // alert("Invoice Report Generated Succesfully");
              // window.location.reload();
             }
          });
    } 

    $(function()
    {
        $( ".datepicker" ).datepicker({
          changeMonth: true,
     changeYear: true,
     dateFormat: 'MM yy',


        });
    });

</script>