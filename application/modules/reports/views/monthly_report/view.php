<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>View Student</h3>
            <a href="/reports/dailyReport/list" class="btn btn-link btn-back">‹ Back</a>
        </div>

        


        <form id="form_performa_invoice" action="" method="post">

          

                    <div class="form-container">
                        <h4 class="form-group-title">Student Details</h4>  

                        <div class="custom-table">
                            <table class="table" id="list-table">
                                <thead>
                                <tr>
                                    <th>Sl. No</th>
                                    <th>Student Name</th>
                                    <th>Student Email</th>
                                    <th>Course</th>
                                    <th>Partner</th>
                                    <th>Registered Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                    $total_amount = 0;
                                if (!empty($studentDetails)) {
                                    $i = 1;
                                    foreach ($studentDetails as $record) {
                                ?>
                                    <tr>
                                        <td><?php echo $i ?></td>
                                        <td><?php echo $record->full_name ?></td>
                                        <td><?php echo $record->email_id ?></td>
                                        <td><?php echo $record->programme_name ?></td>
                                        <td><?php echo $record->university_name ?></td>
                                        <td><?php echo date('d-m-Y',strtotime($record->created_dt_tm)) ?></td>
                                    </tr>
                               <?php $i++; } } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>


            
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>
    $(document).ready(function() {
        $("#form_performa_invoice").validate({
            rules: {
                type_of_invoice: {
                    required: true
                },
                invoice_number: {
                    required: true
                },
                total_amount: {
                    required: true
                },
                id_student: {
                    required: true
                },
                id_application: {
                    required: true
                },
                remarks: {
                    required: true
                },
                status: {
                    required: true
                }
            },
            messages: {
                type_of_invoice: {
                    required: "Select Type Of Invoice",
                },
                invoice_number: {
                    required: "Enter Main Invoice Number",
                },
                total_amount: {
                    required: "Enter Total Amount",
                },
                id_student: {
                    required: "Select Student",
                },
                id_application: {
                    required: "Select Application",
                },
                remarks: {
                    required: "Enter Remarks",
                },
                status: {
                    required: "Status required",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
