<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>Statistics Report</h3>
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">
                

                <div class="row">

               

                 <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Course Name </label>
                    <div class="col-sm-8">
                      <select name="id_programme" id="id_programme" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($programmeList)) {
                          foreach ($programmeList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['id_programme']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>                      

               
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Course Provider</label>
                    <div class="col-sm-8">
                      <select name="partner_university_id" id="partner_university_id" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($organisationList)) {
                          foreach ($organisationList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['partner_university_id']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>

                

       
                  <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Transaction Start Date</label>
                    <div class="col-sm-8">
                      <input type="date" class="form-control" name="start_date" id="start_date" value="<?php echo $searchParam['start_date']; ?>">
                    </div>
                  </div>
                </div>

                  <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Transaction End Date</label>
                    <div class="col-sm-8">
                      <input type="date" class="form-control" name="end_date" id="end_date" value="<?php echo $searchParam['end_date']; ?>">
                    </div>
                  </div>
                </div>

              </div>



              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href="/reports/statistics/list" type="reset" class="btn btn-link" onclick="clearSearchForm()">Clear All Fields</a>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

      <div class="custom-table">
        <table class="table" id="list-table">
            <thead>
                 <tr>
                  <th>Sl. No</th>
                  <th>Partner Name</th>

                  <th>Course Name</th>
                  <th class="text-center">Number of Students</th>
                </tr>
            </thead>
           <tbody>
                <?php
                if (!empty($categoryList)) {
                  $i=1;
                  foreach ($categoryList as $record) {

                    if($searchParam['start_date']=='') {
                      $sd = 0;
                    }  else {
                      $sd = date('Y-m-d',$searchParam['start_date']);
                    }
                    if($searchParam['end_date']=='') {
                       $ed = 0;
                    } else {
                      $ed = date('Y-m-d',$searchParam['end_date']);
                    }


                ?>
                    <tr>
                      <td><?php echo $i ?></td>
                    <td><?php echo $record->university_name; ?></td>

                    <td><?php echo $record->name; ?></td>

                    <td  class="text-center"><a href="/reports/statistics/student/<?php echo $sd; ?>/<?php echo $ed; ?>/<?php echo $record->id; ?>"><?php echo $record->totalcount; ?></a></td>

                    
                    </tr>
                <?php
                $i++;
                  }
                }
                ?>
              </tbody>
        </table>
    </div>             
 </div>
</div>

<script>

    $('select').select2();
  
    function clearSearchForm()
    {
      window.location.reload();
    }
</script>