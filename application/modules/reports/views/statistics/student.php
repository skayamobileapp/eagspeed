<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List Student Records</h3>
      <!-- <a href="add" class="btn btn-primary">+ Add Main Invoice</a> -->
    </div>


     <table class="table" border="1">
        <thead>
                      <th>Course Name</th>

            <th>Partner University Name</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td><?php echo $programme[0]->name;?></td>
            <td><?php echo $studentList[0]->university_name;?></td>
          </tr>
        </tbody>
      </table>
    <div class="page-title clearfix">

    <h3>Student Details</h3>
  </div>
    <div class="custom-table">
      <table class="table" border="1">
        <thead>
                      <th>Sl No</th>

            <th>Transaction ID / Date & Time</th>
            <th>Name</th>
            <th>Email</th>
            <th>Purchased Date</th>
            
            <th>Course Amount</th>
            <th>Transaction Discount</th>
            <th>Transaction Amount</th>
            <th>Payment Status</th>
             <?php for($m=0;$m<count($componentList);$m++){ 



                          $this->load->model('statistics_model');
                $marksobtained = $this->statistics_model->getmarksdistributionById($componentList[$m]->id);

                $weightage = $marksobtained->weightage;

            ?> 
              <th style="width:30%;"><?php echo $componentList[$m]->exam_component;?> (<?php echo $weightage;?> %)</th>

            <?php } ?> 
            <th style="width:30%;">Final Marks</th>

          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($studentList)) {
            $j=1;
          
                    $this->load->model('statistics_model');

            foreach ($studentList as $record) {



                           $invoiceAmount = $record->total_amount;
              $invoiceDetails = array();
                $invoiceDetails = $this->statistics_model->getMainInvoiceDetailsById($record->invoiceid);

  $i=1;

                  for($k=0;$k<count($invoiceDetails);$k++) { 
          ?>


              <tr>
                 <?php if($i==1) {?>
                                <td rowspan="<?php echo count($invoiceDetails);?>" style="vertical-align: top;"><?php echo $j++; ?></td>

                <td rowspan="<?php echo count($invoiceDetails);?>" style="vertical-align:top;">

                  <a href="/profile/download/generateMainInvoice/<?php echo $record->invoiceid;?>"><?php echo $record->invoice_number ?></a> / <br/> <?php echo date('d-m-Y H:i',strtotime($record->date_time)) ?></td>
                <td rowspan="<?php echo count($invoiceDetails);?>" style="vertical-align:top;"><?php echo $record->full_name ?></td>
                <td rowspan="<?php echo count($invoiceDetails);?>" style="vertical-align:top;"><?php echo $record->email_id ?></td>
              
              <?php } ?> 
               
                   
                     
                             
                               <td style="width:30%;"><?php echo date('d-m-Y',strtotime($record->date_time));?></td>
                               <td style="width:30%;"><?php echo $invoiceDetails[$k]->amount;?></td>
                        

                     
                                 <?php if($i==1) {?>
                <td rowspan="<?php echo count($invoiceDetails);?>" style="vertical-align:top;"><?php echo $record->total_discount ?></td>
                <td rowspan="<?php echo count($invoiceDetails);?>" style="vertical-align:top;"><?php echo $invoiceAmount;?></td>
                <td rowspan="<?php echo count($invoiceDetails);?>" style="vertical-align:top;"><?php if($record->total_amount>0 && $record->paid_amount>0) {
                       echo "Paid";
                 } else if($record->total_amount>0 && $record->paid_amount==0) {
                       echo "Payment Pending";
                 } else { 
                        echo "Free Course";
                  }?>
               </td>
             <?php } ?> 



              <?php 

                $finalMarks = 0; 
                for($m=0;$m<count($componentList);$m++){


                          $this->load->model('statistics_model');
                $marksobtained = $this->statistics_model->getmarksbycomponentstudent($record->studenthasprogrammeid,$componentList[$m]->id);

            
               



  ?>
             <td>


               <?php echo $marksobtained->marks_obtained;?>

             
            </td>

            <?php } ?> 
            <td><?php echo $record->final_marks;?></td>

               
              </tr>
          <?php
          $i++;
            }
          }
        }
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script type="text/javascript">
    
  $('select').select2();

  function clearSearchForm()
      {
        window.location.reload();
      }
</script>