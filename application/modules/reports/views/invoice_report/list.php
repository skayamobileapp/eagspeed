<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>Payment Success Report</h3>
      <!-- <a href="add" class="btn btn-primary">+ Add Exam Registration</a> -->
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
           <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">
                

                <div class="row">

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Student Name</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="name" value="<?php echo $searchParam['name']; ?>">
                    </div>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Course Type</label>
                    <div class="col-sm-8">
                       <select name="programme_type_id" id="programme_type_id" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($programmeTypeList)) {
                          foreach ($programmeTypeList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['programme_type_id']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>

              </div>

              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Course Provider</label>
                    <div class="col-sm-8">
                      <select name="partner_university_id" id="partner_university_id" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($organisationList)) {
                          foreach ($organisationList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['partner_university_id']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>

                

       
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Student Email</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="email_id" id="email_id" value="<?php echo $searchParam['email_id']; ?>">
                    </div>
                  </div>
                </div>


 </div>

              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Course Name </label>
                    <div class="col-sm-8">
                      <select name="id_programme" id="id_programme" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($programmeList)) {
                          foreach ($programmeList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['id_programme']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>                      

              </div> 
               <div class="row">

                  <div class="col-sm-6">

                    <div class="form-group">
                      <label class="col-sm-4 control-label">From Date</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control datepicker" id="from_date" name="from_date" value="<?php echo $searchParam['from_date']; ?>">
                      </div>
                    </div>

                  </div>
                  <div class="col-sm-6">
                      <div class="form-group">
                      <label class="col-sm-4 control-label">To Date</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control datepicker" name="to_date" name="to_date" value="<?php echo $searchParam['to_date']; ?>">
                      </div>
                    </div>

                  </div>

              </div>

               <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Payment Status </label>
                    <div class="col-sm-8">
                      <select name="payment_status" id="payment_status" class="form-control">
                        <option value="">Select</option>
                        <option value="Paid">Paid</option>
                        <option value="Pending">Pending</option>
                      </select>
                    </div>
                  </div>
                </div>                      

              </div> 



              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <button type="reset" class="btn btn-link" onclick="clearSearchForm()">Clear All Fields</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <th>Sl. No</th>
            <th class="text-center">Invoice Number</th>
            <th class="text-center">Course Provider</th>
            <th class="text-center">Course Type</th>
            <th class="text-center">Course Name</th>
            <th class="text-center">Student Name</th>
            <th class="text-center">Amount</th>
            <th class="text-center">Payment Status </th>
            <th class="text-center">Invoice Date</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($invoiceReportList)) {
            $i=1;
            foreach ($invoiceReportList as $record)
            {
              $amount = number_format($record->amount, 2, '.', ',');
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->invoice_number ?></td>
                <td><?php echo $record->university_name ?></td>
                <td><?php echo $record->categoryname ?></td>

                <td><?php echo $record->programme_name ?></td>
                <td><?php echo $record->full_name ?> / <?php echo $record->email_id ?></td>
                <td><?php echo $amount ?></td>
                <td><?php echo $record->payment_status ?></td>
                <td><?php echo date("d-m-Y", strtotime($record->created_dt_tm)) ?></td>
                
              
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>

  </div>

  <div id="view_data"></div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script type='text/javascript'>

    $('select').select2();
  
    function downloadInvoiceReportCSV()
    {
      var tempPR = {};
      tempPR['from_date'] = $("#from_date").val();
      tempPR['to_date'] = $("#to_date").val();
              
      // alert(tempPR['to_date']);

          $.ajax(
          {
             url: '/reports/invoiceReport/downloadInvoiceReportCSV',
              type: 'POST',
             data:
             {
              tempData: tempPR
             },
             error: function()
             {
              alert('Something is wrong');
             },
             success: function(result)
             {
              // alert(result);
              $("view_data").val(result);
              // alert("Invoice Report Generated Succesfully");
              // window.location.reload();
             }
          });
    } 

    $(function()
    {
        $( ".datepicker" ).datepicker({
            changeYear: true,
            changeMonth: true,
        });
    });

</script>