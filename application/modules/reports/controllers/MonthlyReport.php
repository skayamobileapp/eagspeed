<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class MonthlyReport extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('invoice_report_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('invoice_report.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $from_date = $formData['from_date'] = $this->security->xss_clean($this->input->post('from_date'));
            $to_date = $formData['to_date'] = $this->security->xss_clean($this->input->post('to_date'));

             $date1=date_create(date('Y-m-d',strtotime($from_date)));
        $date2=date_create(date('Y-m-d',strtotime($to_date)));
        $diff=date_diff($date1,$date2);
        $regDateArray = array();
        $eventDateArray = array();
        $registrationCountArray = array();
        $paymentCountArray = array();
        $inprogressCountArray = array();
        $completedCountArray = array();
        $eventDate='';
        $regCountString = '0';
        $regPaymentString='0';
        $completedString='0';
        $inprogressString='0';
           for($i=0;$i<=($diff->m);$i++)
        {



            $startDateofMonth = date('Y-m-01', strtotime($from_date. ' + '.$i.' months'));
            $endDateofMonth = date('Y-m-30', strtotime($from_date. ' + '.$i.' months'));



            $registrationArray = $this->invoice_report_model->registrationCountMonth($startDateofMonth,$endDateofMonth);
            $paymentArray = $this->invoice_report_model->paymentCountMonth($startDateofMonth,$endDateofMonth);
            $inprogressArray = $this->invoice_report_model->inprogressMonth($startDateofMonth,$endDateofMonth);
            $completedArray = $this->invoice_report_model->completedMonth($startDateofMonth,$endDateofMonth);

            if($i==0) 
                 {
            $eventDate = date('m', strtotime($from_date. ' + '.$i.' months'));
           $regCountString = count($registrationArray);
           $regPaymentString = count($paymentArray);
           $inprogressString = count($inprogressArray);
           $completedString = count($completedArray);
       }
            else  {
            $eventDate = $eventDate.','.date('m', strtotime($from_date. ' + '.$i.' months'));
           $regCountString = $regCountString.','.count($registrationArray);
           $regPaymentString = $regPaymentString.','.count($paymentArray);
           $inprogressString = $inprogressString.','.count($inprogressArray);
           $completedString = $completedString.','.count($completedArray);

        }



            array_push($registrationCountArray,count($registrationArray));
            array_push($paymentCountArray,count($paymentArray));
            array_push($inprogressCountArray,count($inprogressArray));
            array_push($completedCountArray,count($completedArray));

            array_push($eventDateArray,date('Y-m-d', strtotime($from_date. ' + '.$i.' months')));
        }


   

            $data['searchParam'] = $formData;
            $data['eventDate'] = $eventDateArray;
            $data['regCount'] = $registrationCountArray;
            $data['paidCount'] = $paymentCountArray;
            $data['inprogressCount'] = $inprogressCountArray;
            $data['completedCount'] = $completedCountArray;
            $data['stringeventDate'] = $eventDate;
            $data['stringregistrationCout'] = $regCountString;
            $data['stringpaymentCout'] = $regPaymentString;
            $data['stringinprogressCout'] = $inprogressString;
            $data['stringcompletedCout'] = $completedString;




            $data['organisationList']= $this->invoice_report_model->organisation();


            $this->global['pageTitle'] = 'Campus Management System : Corporate Company List';
            $this->loadViews("monthly_report/list", $this->global, $data, NULL);
        }
    }

    
   

    function view($fromdate,$todate)
    {
       
             $data['studentDetails'] = $this->invoice_report_model->getRegistrationStudentDataRange($fromdate,$todate);

          

            $this->global['pageTitle'] = 'Campus Management System : View Main Invoice';
            $this->loadViews("monthly_report/view", $this->global, $data, NULL);
        
    }

    function paid($fromdate,$todate)
    {
       
             $data['studentDetails'] = $this->invoice_report_model->getReceiptStudentDataRange($fromdate,$todate);

          

            $this->global['pageTitle'] = 'Campus Management System : View Main Invoice';
            $this->loadViews("monthly_report/paid", $this->global, $data, NULL);
        
    }

    function progress($fromdate,$todate)
    {
       
             $data['studentDetails'] = $this->invoice_report_model->getProgressStudentData($fromdate,$todate);

          

            $this->global['pageTitle'] = 'Campus Management System : View Main Invoice';
            $this->loadViews("monthly_report/progress", $this->global, $data, NULL);
        
    }

    function completed($fromdate,$todate)
    {
       
             $data['studentDetails'] = $this->invoice_report_model->getCompletedStudentDataRange($fromdate,$todate);

          

            $this->global['pageTitle'] = 'Campus Management System : View Main Invoice';
            $this->loadViews("monthly_report/completed", $this->global, $data, NULL);
        
    }

}