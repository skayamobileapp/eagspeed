<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class DailyReport extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('invoice_report_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('invoice_report.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $from_date = $formData['from_date'] = $this->security->xss_clean($this->input->post('from_date'));
            $to_date = $formData['to_date'] = $this->security->xss_clean($this->input->post('to_date'));

             $date1=date_create(date('Y-m-d',strtotime($from_date)));
        $date2=date_create(date('Y-m-d',strtotime($to_date)));
        $diff=date_diff($date1,$date2);
        $eventDateArray = array();
        $registrationCountArray = array();
        $paymentCountArray = array();
        $inprogressCountArray = array();
        $completedCountArray = array();
        $eventDate='';
        $regCountString = '';
        $regPaymentString='';
           for($i=0;$i<=($diff->days);$i++)
        {
            
            $regDate = date('Y-m-d', strtotime($from_date. ' + '.$i.' days'));
            $registrationArray = $this->invoice_report_model->registrationCount($regDate);
            $paymentArray = $this->invoice_report_model->paymentCount($regDate);
            $inprogressArray = $this->invoice_report_model->inprogress($regDate);
            $completedArray = $this->invoice_report_model->completed($regDate);

            if($i==0) 
                 {
            $eventDate = date('j', strtotime($from_date. ' + '.$i.' days'));
           $regCountString = count($registrationArray);
           $regPaymentString = count($paymentArray);
           $inprogressString = count($inprogressArray);
           $completedString = count($completedArray);
       }
            else  {
            $eventDate = $eventDate.','.date('j', strtotime($from_date. ' + '.$i.' days'));
           $regCountString = $regCountString.','.count($registrationArray);
           $regPaymentString = $regPaymentString.','.count($paymentArray);
           $inprogressString = $inprogressString.','.count($inprogressArray);
           $completedString = $completedString.','.count($completedArray);

        }



            array_push($registrationCountArray,count($registrationArray));
            array_push($paymentCountArray,count($paymentArray));
            array_push($inprogressCountArray,count($inprogressArray));
            array_push($completedCountArray,count($completedArray));

            array_push($eventDateArray,date('Y-m-d', strtotime($from_date. ' + '.$i.' days')));
        }


   

            $data['searchParam'] = $formData;
            $data['eventDate'] = $eventDateArray;
            $data['regCount'] = $registrationCountArray;
            $data['paidCount'] = $paymentCountArray;
            $data['inprogressCount'] = $inprogressCountArray;
            $data['completedCount'] = $completedCountArray;
            $data['stringeventDate'] = $eventDate;
            $data['stringregistrationCout'] = $regCountString;
            $data['stringpaymentCout'] = $regPaymentString;
            $data['stringinprogressCout'] = $inprogressString;
            $data['stringcompletedCout'] = $completedString;




            $data['organisationList']= $this->invoice_report_model->organisation();


            $this->global['pageTitle'] = 'Campus Management System : Corporate Company List';
            $this->loadViews("daily_report/list", $this->global, $data, NULL);
        }
    }

    function view($fromdate,$todate)
    {
       
             $data['studentDetails'] = $this->invoice_report_model->getRegistrationStudentData($fromdate,$todate);

          

            $this->global['pageTitle'] = 'Campus Management System : View Main Invoice';
            $this->loadViews("daily_report/view", $this->global, $data, NULL);
        
    }

    function paid($fromdate,$todate)
    {
       
             $data['studentDetails'] = $this->invoice_report_model->getReceiptStudentData($fromdate,$todate);

          

            $this->global['pageTitle'] = 'Campus Management System : View Main Invoice';
            $this->loadViews("daily_report/paid", $this->global, $data, NULL);
        
    }

    function progress($fromdate,$todate)
    {
       
             $data['studentDetails'] = $this->invoice_report_model->getProgressStudentData($fromdate,$todate);

          

            $this->global['pageTitle'] = 'Campus Management System : View Main Invoice';
            $this->loadViews("daily_report/progress", $this->global, $data, NULL);
        
    }

    function completed($fromdate,$todate)
    {
       
             $data['studentDetails'] = $this->invoice_report_model->getCompletedStudentData($fromdate,$todate);

          

            $this->global['pageTitle'] = 'Campus Management System : View Main Invoice';
            $this->loadViews("daily_report/completed", $this->global, $data, NULL);
        
    }

}