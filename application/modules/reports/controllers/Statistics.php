<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Statistics extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        error_reporting(0);
        $this->load->model('statistics_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('statistics.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));
            $formData['partner_university_id'] = $this->security->xss_clean($this->input->post('partner_university_id'));
            $formData['email_id'] = $this->security->xss_clean($this->input->post('email_id'));
            $formData['start_date'] = $this->security->xss_clean($this->input->post('start_date'));
            $formData['end_date'] = $this->security->xss_clean($this->input->post('end_date'));

            $data['searchParam'] = $formData;
            $data['categoryList'] = $this->statistics_model->courseCountData($formData);

            $data['programmeList']= $this->statistics_model->programList();
            $data['organisationList']= $this->statistics_model->organisation();

            $this->global['pageTitle'] = 'Speed Management System : Category List';
            $this->global['pageCode'] = 'statistics.list';

            $this->loadViews("statistics/list", $this->global, $data, NULL);
        }
    }


    function student($startdate = NULL, $enddate = NULL, $course) {
        $data['studentList'] = $this->statistics_model->applicantList($startdate,$enddate,$course);
            $data['programme']= $this->statistics_model->programListbyId($course);
                $data['componentList'] = $this->statistics_model->getMarkDistributionByProgramme($course);



            $this->global['pageTitle'] = 'Speed Management System : Category List';
            $this->global['pageCode'] = 'statistics.list';

            $this->loadViews("statistics/student", $this->global, $data, NULL);
    }

}
