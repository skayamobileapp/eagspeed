<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ReceiptReport extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('receipt_report_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('receipt_report.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $from_date = $this->security->xss_clean($this->input->post('from_date'));
            $to_date = $this->security->xss_clean($this->input->post('to_date'));

            $formData['receipt_number'] = $this->security->xss_clean($this->input->post('receipt_number'));
            $formData['student_name'] = $this->security->xss_clean($this->input->post('student_name'));
            $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));
            $formData['type'] = $this->security->xss_clean($this->input->post('type'));
            $formData['status'] = $this->security->xss_clean($this->input->post('status'));
            $formData['from_date'] = '';
            $formData['to_date'] = '';

            if($from_date != '')
            {
                $formData['from_date'] = date('Y-m-d',strtotime($from_date));
            }
            if($to_date != '')
            {
                $formData['to_date'] = date('Y-m-d',strtotime($to_date));
            }
            
            $data['searchParam'] = $formData;


            if($this->input->post())
            {
                $formSubmit = $this->input->post();
                // echo "<Pre>";print_r($formSubmit);exit;

                $btn_submit = $formSubmit['btn_submit'];

                if($btn_submit == 'download')
                {
                    $this->downloadReceiptReportCSV($formData);
                }
                elseif($btn_submit == 'clear')
                {
                    // $data['searchParam'] = array();
                    $data['receiptReportList'] = array();
                }
                else
                {
                    $data['receiptReportList'] = $this->receipt_report_model->receiptReportListSearch($formData);
                }

                // btn_submit
            }
            else
            {
                $data['receiptReportList'] = array();
            }
            

            $data['programmeList'] = $this->receipt_report_model->programmeListByStatus('1');
            $data['studentList'] = $this->receipt_report_model->studentList();
            
            // echo "<Pre>";print_r($data['studentList']);exit;

            $this->global['pageTitle'] = 'Campus Management System : Corporate Company List';
            $this->loadViews("receipt_report/list", $this->global, $data, NULL);
        }
    }

    function view($id = NULL)
    {
        if ($this->checkAccess('receipt_report.view') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/reports/receiptReport/list');
            }
            if($this->input->post())
            {
                redirect('/reports/receiptReport/list');
            }

            $data['receipt'] = $this->receipt_report_model->getReceipt($id);
            $data['invoiceDetails'] = $this->receipt_report_model->getReceiptDetails($id);
            $data['paymentDetails'] = $this->receipt_report_model->getReceiptPaymentDetails($id);
            
            // echo "<Pre>";  print_r($data['receiptDetailsList']);exit;

            if($data['receipt']->type == 'Student')
            {
                $data['receiptFor'] = $this->receipt_report_model->getStudent($data['receipt']->id_student);
            }
            elseif($data['receipt']->type == 'CORPORATE')
            {
                $data['receiptFor'] = $this->receipt_report_model->getReceiptCorporateData($data['receipt']->id_student);
            }

            // $data['degreeTypeList'] = $this->receipt_report_model->qualificationListByStatus('1');
            // echo "<Pre>";  print_r($data['paymentDetails']);exit;

            $this->global['pageTitle'] = 'Campus Management System : View Main Receipt';
            $this->loadViews("receipt_report/view", $this->global, $data, NULL);
        }
    }

    function downloadReceiptReportCSV($tempData)
    {
        // $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $fileData = $this->receipt_report_model->downloadReceiptReportCSVBtwnDates($tempData);
        // echo "<Pre>";print_r($fileData);exit;


        $fileName = gmdate("YmdHis");

        $this->generateCSVReceiptReport($fileData, $fileName, $tempData);
    }

    public function generateCSVReceiptReport($fileData, $fileName, $data)
    {
        

        $fromDate = $data['from_date'];
        $toDate = $data['to_date'];
        $pay_date   = date("d/m/Y");


        $organisationDetails = $this->receipt_report_model->getOrganisation();

        // echo "<Pre>";print_r($fileData);exit;


        $signature = $_SERVER['DOCUMENT_ROOT']."/assets/images/logo.svg";

        if($organisationDetails->image != '')
        {
            $signature = $_SERVER['DOCUMENT_ROOT']."/assets/images/" . $organisationDetails->image;
        }


        $file_name = $fileName . "_ReceiptReport.csv";

        

        $fp = fopen('php://output', 'w');

        $header = array();

        array_push($header, "Receipt Report");

        header("Content-type: application/csv");
        header("Content-Disposition: attachment; filename=" . $file_name);
        header("Pragma: no-cache");
        header("Expires: 0");

        fputcsv($fp, $header);

        $empty_header = array();

        array_push($empty_header, " ");
        fputcsv($fp, $empty_header);


        $dtheader = array();

        array_push($dtheader, "No");
        array_push($dtheader, "Receipt Type");
        array_push($dtheader, "Receipt Number");
        array_push($dtheader, "Participant Name");
        array_push($dtheader, "IC / Passport No.");
        array_push($dtheader, "COURSE Name");
        array_push($dtheader, "Receipt Amount");
        array_push($dtheader, "Currency");
        array_push($dtheader, "Receipt Date");
        array_push($dtheader, "DESCRIPTION");

        fputcsv($fp, $dtheader);

        $i=1;
        foreach ($fileData as $data)
        {
            $receipt_number = $data->receipt_number;
            $receipt_date = date('d-m-Y H:i:s', strtotime($data->receipt_date));
            $type = $data->type;
            $id_student = $data->id_student;
            $receipt_total = $data->receipt_amount;
            $currency_name = $data->currency_name;
            $remarks = $data->remarks;
            $programme_code = $data->programme_code;
            $programme_name = $data->programme_name;

            $programme = $programme_code  . " - " . $programme_name;
            // $total_currency = $receipt_total . " ( " . $ . " ) ";

            $student_name = '';
            $student_nric = '';

            if($type == 'CORPORATE')
            {
                $company = $this->receipt_report_model->getCompany($id_student);
                if($company)
                {
                    $student_name = $company->name; 
                    $student_nric = $company->registration_number; 
                }
            }
            elseif($type == 'Student')
            {
                $student = $this->receipt_report_model->getStudent($id_student);
                if($student)
                {
                    $student_name = $student->full_name; 
                    $student_nric = $student->nric; 
                }
            }

            $data_array = array();

            array_push($data_array, $i);
            array_push($data_array, $type);
            array_push($data_array, $receipt_number);
            array_push($data_array, $student_name);
            array_push($data_array, $student_nric);
            array_push($data_array, $programme);
            array_push($data_array, $receipt_total);
            array_push($data_array, $currency_name);
            array_push($data_array, $receipt_date);
            array_push($data_array, $remarks);

            fputcsv($fp, $data_array);
            $i++;
        }

        fclose($fp);
        exit;
        // echo $file_name;

    }
}