<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Receipt_report_model extends CI_Model
{
    function programmeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function receiptReportListSearch($data)
    {
        $this->db->select('mi.*, p.code as programme_code, p.name as programme_name, cs.name as currency_name');
        $this->db->from('receipt as mi');
        $this->db->join('programme as p', 'mi.id_program = p.id','left');
        $this->db->join('currency_setup as cs', 'mi.currency = cs.id','left');
        $this->db->join('student as s', 'mi.id_student = s.id','left');
        // $this->db->join('intake as i', 's.id_intake = i.id');
        if ($data['receipt_number'] != '')
        {
            $likeCriteria = "(mi.receipt_number  LIKE '%" . $data['receipt_number'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['student_name'] != '')
        {
            $likeCriteria = "(s.full_name  LIKE '%" . $data['student_name'] . "%')";
            $this->db->where($likeCriteria);
            $this->db->where('mi.type', 'Student');
        }
        if ($data['type'] != '')
        {
            $this->db->where('mi.type', $data['type']);
        }
        if ($data['id_programme'] != '')
        {
            $this->db->where('mi.id_program', $data['id_programme']);
        }
        if ($data['status'] != '')
        {
            $this->db->where('mi.status', $data['status']);
        }
        if ($data['from_date'] != '')
        {
            $this->db->where('mi.receipt_date >=', $data['from_date']);
        }
        if ($data['to_date'] != '')
        {
            $this->db->where('mi.receipt_date <=', $data['to_date']);
        }
        $this->db->where('mi.status', 1);
        $this->db->order_by("mi.id", "DESC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function studentList()
    {
        $this->db->select('*');
        $this->db->from('student');
        $this->db->where('id_company', 0);
        $this->db->order_by("id", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getReceipt($id)
    {
        $this->db->select('mi.*, p.name as programme_name, p.code as programme_code, cs.name as currency_name');
        $this->db->from('receipt as mi');
        $this->db->join('programme as p', 'mi.id_program = p.id','left');
        $this->db->join('currency_setup as cs', 'mi.currency = cs.id','left');
        $this->db->where('mi.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getReceiptDetails($id)
    {
        $this->db->select('mid.*, mi.remarks, cs.name as currency_name, mi.invoice_number, mi.invoice_total, mi.total_amount, mi.total_discount, mi.balance_amount, mi.paid_amount');
        $this->db->from('receipt_details as mid');
        $this->db->join('main_invoice as mi', 'mid.id_main_invoice = mi.id');     
        $this->db->join('currency_setup as cs', 'mi.currency = cs.id','left');
        $this->db->where('mid.id_receipt', $id);
        $query = $this->db->get();
        return $query->result();
    }


    function getReceiptPaymentDetails($id)
    {
        $this->db->select('r.*');
        $this->db->from('receipt_paid_details as r');
        $this->db->where('r.id_receipt', $id);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function downloadReceiptReportCSVBtwnDates($data)
    {
        $this->db->select('mi.*, p.code as programme_code, p.name as programme_name, cs.name as currency_name');
        $this->db->from('receipt as mi');
        $this->db->join('programme as p', 'mi.id_program = p.id','left');
        $this->db->join('currency_setup as cs', 'mi.currency = cs.id','left');
        if ($data['from_date'] != '')
        {
            $this->db->where('mi.receipt_date >=', $data['from_date']);
        }
        if ($data['to_date'] != '')
        {
            $this->db->where('mi.receipt_date <=', $data['to_date']);
        }
        // $this->db->where('mi.status', 1);
        $this->db->order_by("mi.id", "DESC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getOrganisation()
    {
        $this->db->select('*');
        $this->db->from('organisation');
        $this->db->order_by("id", "ASC");
        $query = $this->db->get();
        return $query->row();
    }

    function getCompany($id)
    {
        $this->db->select('*');
        $this->db->from('company');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getStudent($id)
    {
        $this->db->select('*');
        $this->db->from('student');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function getReceiptCorporateData($id)
    {
        $this->db->select('stu.name as full_name, stu.registration_number as nric');
        $this->db->from('company as stu');
        $this->db->where('stu.id', $id);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getMainInvoiceHasStudentList($id)
    {
        $this->db->select('pmhs.*, s.full_name as student_name, s.nric');
        $this->db->from('main_receipt_has_students as pmhs');   
        $this->db->join('student as s', 'pmhs.id_student = s.id');
        $this->db->where('pmhs.id_main_receipt', $id);
        $query = $this->db->get();
        return $query->result();
    }
}