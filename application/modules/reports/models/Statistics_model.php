<?php if(!defined('BASEPATH')) exit('No direct script access allowed');


class Statistics_model extends CI_Model
{
    
    function applicantList($startdate, $enddate, $courseId) {
  $this->db->select('a.*,pu.name as university_name,shp.id as studenthasprogrammeid,shp.final_marks,mi.invoice_number,mi.total_amount,mi.total_discount,mi.id as invoiceid,mi.date_time,mi.paid_amount,mi.date_time');
          $this->db->from('student as a');
        $this->db->join('student_has_programme as shp','a.id=shp.id_student');          
        $this->db->join('programme as p','p.id=shp.id_programme');
        $this->db->join('partner_university as pu','p.id_partner_university=pu.id');
        $this->db->join('main_invoice as mi','mi.id=shp.id_main_invoice');

       

        if ($courseId != '')
        {
            $this->db->where('p.id =', $courseId);
        }

       

        if ($startdate != '0')
        {
            $startdate = date('Y-m-d',strtotime($data['start_date']));
            $this->db->where('date(mi.date_time) >=', $startdate);
        }

        if ($enddate != '0')
        {
            $this->db->where('date(mi.date_time) <=', date('Y-m-d',strtotime($data['end_date'])));
        }

         
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }
    function courseCountData($data)
    {
        $this->db->select('p.name,count(a.id) as totalcount,p.id,pu.name as university_name');
        $this->db->from('student as a');
        $this->db->join('main_invoice as mi','a.id=mi.id_student');
        $this->db->join('main_invoice_details as mid','mid.id_main_invoice=mi.id');
        $this->db->join('programme as p','p.id=mid.id_programme');
        $this->db->join('partner_university as pu','p.id_partner_university=pu.id');

        if ($data['partner_university_id'] != '')
        {
            $this->db->where('p.id_partner_university =', $data['partner_university_id']);
        }


        if ($data['id_programme'] != '')
        {
            $this->db->where('p.id =', $data['id_programme']);
        }

        if ($data['email_id'] != '')
        {
            $this->db->where('a.email_id =', $data['email_id']);
        }


        if ($data['name'] != '')
        {
            $this->db->where('a.full_name  like "%'.$data['name'].'%"');
        }

        if ($data['start_date'] != '')
        {
            $startdate = date('Y-m-d',strtotime($data['start_date']));
            $this->db->where('date(mi.date_time) >=', $startdate);
        }

        if ($data['end_date'] != '')
        {
            $this->db->where('date(mi.date_time) <=', date('Y-m-d',strtotime($data['end_date'])));
        }

        $this->db->group_by("p.id");
        $this->db->order_by("pu.name");
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }
     function programList()
    {
        $this->db->select('*');
        $this->db->from('programme');
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }


     function programListbyId($id)
    {
        $this->db->select('*');
        $this->db->from('programme');
            $this->db->where('id =', $id);
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }



       function organisation()
    {
        $this->db->select('*');
        $this->db->from('partner_university');
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }


       function getMainInvoiceDetailsById($id)
    {
        $this->db->select('mid.*, p.name,shp.*,pu.name as universityname');
        $this->db->from('main_invoice_details as mid');
        $this->db->join('student_has_programme as shp', 'shp.id_main_invoice = mid.id_main_invoice');
        $this->db->join('programme as p', 'shp.id_programme = p.id');        
        $this->db->join('partner_university as pu', 'p.id_partner_university = pu.id');        
      
        $this->db->where('mid.id_main_invoice', $id);
        $query = $this->db->get();
        return $query->result();
    }


      function getMarkDistributionByProgramme($id_programme)
    {
        $this->db->select('md.*, ec.name as exam_component');
        $this->db->from('marks_distribution as md');
        $this->db->join('exam_components as ec', 'md.id_exam_component = ec.id');
        $this->db->where('md.id_programme', $id_programme);
        $query = $this->db->get();
        return $query->result();
    }


     function getmarksdistributionById($id) {
         $this->db->select('md.*');
        $this->db->from('marks_distribution as md');

        $this->db->where('md.id', $id);
        $query = $this->db->get();
        $result = $query->row(); 

        return $result;
    }


     function getmarksbycomponentstudent($shpid,$componetid) {
         $this->db->select('md.*,mds.weightage');
        $this->db->from('student_has_programme_marks as md');
        $this->db->join('marks_distribution as mds', 'mds.id = md.id_marks_distribution');

        $this->db->where('md.id_student_has_programme', $shpid);
        $this->db->where('md.id_marks_distribution', $componetid);
        $this->db->order_by('md.id', 'DESC');
        $query = $this->db->get();
        $result = $query->row(); 

        return $result;
    }



}

  