<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Invoice_report_model extends CI_Model
{
    function programmeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function registrationCount($regdate) {
        $this->db->select('*');
        $this->db->from('student');
        $this->db->where('date(created_dt_tm)', $regdate);
        $query = $this->db->get();
        return $query->result();
    }


    function paymentCount($regdate) {
        $this->db->select('*');
        $this->db->from('receipt');
        $this->db->where('date(receipt_date)', $regdate);
        $query = $this->db->get();
        return $query->result();
    }

    function inprogress($regdate) {
        $this->db->select('s.*');
        $this->db->from('student as s');
        $this->db->join('student_has_programme as shp', 'shp.id_student = s.id');        
        $this->db->where('date(shp.start_date)<=', $regdate);
        $this->db->where('date(shp.end_date)>=', $regdate);
        $query = $this->db->get();
        return $query->result();
    }

    function completed($regdate) {
        $this->db->select('s.*');
        $this->db->from('student as s');
        $this->db->join('student_has_programme as shp', 'shp.id_student = s.id');        
        $this->db->where('date(shp.end_date)', $regdate);
        $query = $this->db->get();
        return $query->result();
    }



    function registrationCountMonth($startdate,$enddate) {
        $this->db->select('*');
        $this->db->from('student');
        $this->db->where('date(created_dt_tm)>=', $startdate);
        $this->db->where('date(created_dt_tm)<=', $enddate);
        $query = $this->db->get();
        return $query->result();
    }


    function paymentCountMonth($startdate,$enddate) {
        $this->db->select('*');
        $this->db->from('student as s');
        $this->db->join('receipt as rcp', 'rcp.id_student = s.id');        
        $this->db->where('date(rcp.receipt_date)>=', $startdate);
        $this->db->where('date(rcp.receipt_date)<=', $enddate);

        $query = $this->db->get();
        return $query->result();
    }

    function inprogressMonth($startdate,$enddate) {
        $this->db->select('*');
        $this->db->from('student_has_programme');
        $this->db->where('date(start_date)<=', $startdate);
        $this->db->where('date(end_date)>=', $enddate);
        $query = $this->db->get();
        return $query->result();
    }

    function completedMonth($startdate,$enddate) {
        $this->db->select('*');
        $this->db->from('student_has_programme');
        $this->db->where('date(end_date)>=', $startdate);
        $this->db->where('date(end_date)<=', $enddate);
        $query = $this->db->get();
        return $query->result();
    }


     function getRegistrationStudentData($startdate,$enddate) {

         $this->db->select('p.code as programme_code, p.name as programme_name,s.full_name,pu.name as university_name,s.created_dt_tm,s.email_id');
        $this->db->from('student as s');
        $this->db->join('student_has_programme as shp', 'shp.id_student = s.id','left');        
        $this->db->join('programme as p', 'shp.id_programme = p.id','left');
        $this->db->join('partner_university as pu', 'p.id_partner_university = pu.id','left'); 
        $this->db->where('date(s.created_dt_tm)=', $startdate);
        $this->db->where('date(s.created_dt_tm)=', $enddate);
        $query = $this->db->get();
        return $query->result();
    }

     function getRegistrationStudentDataRange($startdate,$enddate) {

         $this->db->select('p.code as programme_code, p.name as programme_name,s.full_name,pu.name as university_name,s.created_dt_tm,s.email_id');
        $this->db->from('student as s');
        $this->db->join('student_has_programme as shp', 'shp.id_student = s.id','left');        
        $this->db->join('programme as p', 'shp.id_programme = p.id','left');
        $this->db->join('partner_university as pu', 'p.id_partner_university = pu.id','left'); 
        $this->db->where('date(s.created_dt_tm)>=', $startdate);
        $this->db->where('date(s.created_dt_tm)<=', $enddate);
        $query = $this->db->get();
        return $query->result();
    }



     function getProgressStudentData($startdate,$enddate) {

         $this->db->select('p.code as programme_code, p.name as programme_name,s.full_name,pu.name as university_name,s.created_dt_tm,s.email_id,shp.*');
        $this->db->from('student as s');
        $this->db->join('student_has_programme as shp', 'shp.id_student = s.id','left');        
        $this->db->join('programme as p', 'shp.id_programme = p.id','left');
        $this->db->join('partner_university as pu', 'p.id_partner_university = pu.id','left'); 
        $this->db->where('date(shp.start_date)<=', $startdate);
        $this->db->where('date(shp.end_date)>=', $enddate);
        $query = $this->db->get();
        return $query->result();
    }


  function getCompletedStudentData($startdate,$enddate) {

         $this->db->select('p.code as programme_code, p.name as programme_name,s.full_name,pu.name as university_name,s.created_dt_tm,s.email_id,shp.*');
        $this->db->from('student as s');
        $this->db->join('student_has_programme as shp', 'shp.id_student = s.id','left');        
        $this->db->join('programme as p', 'shp.id_programme = p.id','left');
        $this->db->join('partner_university as pu', 'p.id_partner_university = pu.id','left'); 
        $this->db->where('date(shp.end_date)<=', $startdate);
        $this->db->where('date(shp.end_date)>=', $enddate);
        $query = $this->db->get();
        return $query->result();
    }



    function getReceiptStudentData($startdate,$enddate) {

         $this->db->select('s.full_name,s.created_dt_tm,s.email_id,rcp.*');
        $this->db->from('student as s');
        $this->db->join('receipt as rcp', 'rcp.id_student = s.id');        
        $this->db->where('date(rcp.receipt_date)=', $startdate);
        $this->db->where('date(rcp.receipt_date)=', $enddate);
        //$this->db->group_by('shp.id'); 

        $query = $this->db->get();
        return $query->result();
    }

     function getReceiptStudentDataRange($startdate,$enddate) {

         $this->db->select('s.full_name,s.created_dt_tm,s.email_id,rcp.*');
        $this->db->from('student as s');
        $this->db->join('receipt as rcp', 'rcp.id_student = s.id');        
        $this->db->where('date(rcp.receipt_date)>=', $startdate);
        $this->db->where('date(rcp.receipt_date)<=', $enddate);
        //$this->db->group_by('shp.id'); 

        $query = $this->db->get();
        return $query->result();
    }


      function getCompletedStudentDataRange($startdate,$enddate) {

         $this->db->select('p.code as programme_code, p.name as programme_name,s.full_name,pu.name as university_name,s.created_dt_tm,s.email_id,shp.*');
        $this->db->from('student as s');
        $this->db->join('student_has_programme as shp', 'shp.id_student = s.id','left');        
        $this->db->join('programme as p', 'shp.id_programme = p.id','left');
        $this->db->join('partner_university as pu', 'p.id_partner_university = pu.id','left'); 
        $this->db->where('date(shp.end_date)>=', $startdate);
        $this->db->where('date(shp.end_date)<=', $enddate);
        $query = $this->db->get();
        return $query->result();
    }





     function programList()
    {
        $this->db->select('*');
        $this->db->from('programme');
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

      function programmeType()
    {
        $this->db->select('*');
        $this->db->from('product_type');
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

       function organisation()
    {
        $this->db->select('*');
        $this->db->from('partner_university');
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }


    function invoiceReportListSearch($data)
    {
        $this->db->select('mi.invoice_number,mi.payment_status,mid.*, p.code as programme_code, p.name as programme_name,s.full_name,pu.name as university_name,c.name as categoryname,s.email_id');
        $this->db->from('main_invoice as mi');
        $this->db->join('main_invoice_details as mid', 'mid.id_main_invoice = mi.id','left');        
        $this->db->join('student_has_programme as shp', 'shp.id_main_invoice_details = mid.id','left');        
        $this->db->join('programme as p', 'shp.id_programme = p.id','left');
        $this->db->join('student as s', 'mi.id_student = s.id','left');
        $this->db->join('category as c', 'p.id_category = c.id','left');
        $this->db->join('partner_university as pu', 'p.id_partner_university = pu.id','left');        




        // $this->db->join('intake as i', 's.id_intake = i.id');
        if ($data['invoice_number'] != '')
        {
            $likeCriteria = "(mi.invoice_number  LIKE '%" . $data['invoice_number'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['student_name'] != '')
        {
            $likeCriteria = "(s.full_name  LIKE '%" . $data['student_name'] . "%')";
            $this->db->where($likeCriteria);
            $this->db->where('mi.type', 'Student');
        }
        if ($data['type'] != '')
        {
            $this->db->where('mi.type', $data['type']);
        }
        if ($data['id_programme'] != '')
        {
            $this->db->where('shp.id_programme', $data['id_programme']);
        }
        if ($data['status'] != '')
        {
            $this->db->where('mi.status', $data['status']);
        }
        if ($data['from_date'] != '')
        {
            $this->db->where('mi.date_time >=', $data['from_date']);
        }
        if ($data['to_date'] != '')
        {
            $this->db->where('mi.date_time <=', $data['to_date']);
        }
         if ($data['payment_status'] != '')
        {
            $this->db->where('mi.payment_status =', $data['payment_status']);
        }
        $this->db->order_by("mi.id", "DESC");
        $this->db->group_by("mi.id");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


    function downloadInvoiceReportCSVBtwnDates($data)
    {
        $this->db->select('mi.*, p.code as programme_code, p.name as programme_name, cs.name as currency_name');
        $this->db->from('main_invoice as mi');
        $this->db->join('programme as p', 'mi.id_program = p.id','left');
        $this->db->join('currency_setup as cs', 'mi.currency = cs.id','left');
        if ($data['from_date'] != '')
        {
            $this->db->where('mi.date_time >=', $data['from_date']);
        }
        if ($data['to_date'] != '')
        {
            $this->db->where('mi.date_time <=', $data['to_date']);
        }
        $this->db->where('mi.status', 1);
        $this->db->order_by("mi.id", "DESC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getOrganisation()
    {
        $this->db->select('*');
        $this->db->from('organisation');
        $this->db->order_by("id", "ASC");
        $query = $this->db->get();
        return $query->row();
    }

    function getCompany($id)
    {
        $this->db->select('*');
        $this->db->from('company');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getStudent($id)
    {
        $this->db->select('*');
        $this->db->from('student');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getMainInvoice($id)
    {
        $this->db->select('mi.*,  p.name as programme_name, p.code as programme_code, i.name as intake_name, i.year as intake_year, cs.name as currency_name');
        $this->db->from('main_invoice as mi');
        $this->db->join('programme as p', 'mi.id_program = p.id','left');
        $this->db->join('intake as i', 'mi.id_intake = i.id','left');
        $this->db->join('currency_setup as cs', 'mi.currency = cs.id','left');
        $this->db->where('mi.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getMainInvoiceDetails($id)
    {
        $this->db->select('mid.*, fstp.name as fee_setup');
        $this->db->from('main_invoice_details as mid');
        $this->db->join('fee_setup as fstp', 'mid.id_fee_item = fstp.id');     
        $this->db->where('mid.id_main_invoice', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function getMainInvoiceDiscountDetails($id)
    {
        $this->db->select('*');
        $this->db->from('main_invoice_discount_details');   
        $this->db->where('id_main_invoice', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function getMainInvoiceStudentData($id_student)
    {
        $this->db->select('stu.full_name, stu.nric, stu.id_degree_type');
        $this->db->from('student as stu');
        $this->db->where('stu.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }
    
    function getMainInvoiceCorporateData($id)
    {
        $this->db->select('stu.name as full_name, stu.registration_number as nric');
        $this->db->from('company as stu');
        $this->db->where('stu.id', $id);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function qualificationListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('education_level');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result(); 
         return $result;
    }

    function getMainInvoiceHasStudentList($id)
    {
        $this->db->select('pmhs.*, s.full_name as student_name, s.nric');
        $this->db->from('main_invoice_has_students as pmhs');   
        $this->db->join('student as s', 'pmhs.id_student = s.id');
        $this->db->where('pmhs.id_main_invoice', $id);
        $query = $this->db->get();
        return $query->result();
    }
}