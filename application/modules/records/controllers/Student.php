<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Student extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('student_record_model');
        $this->load->model('student_model');
        
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('student.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['partner_university_id'] = $this->security->xss_clean($this->input->post('partner_university_id'));
            $formData['email_id'] = $this->security->xss_clean($this->input->post('email_id'));
            $formData['end_date'] = $this->security->xss_clean($this->input->post('end_date'));
            $formData['start_date'] = $this->security->xss_clean($this->input->post('start_date'));
            $formData['programme_type'] = $this->security->xss_clean($this->input->post('programme_type'));
            $formData['payment_status'] = $this->security->xss_clean($this->input->post('payment_status'));


            
            $data['searchParam'] = $formData;

            if($this->input->post())
            {           
             
                $data['mainInvoiceList'] = $this->student_model->applicantList($formData);
            }
            else
            {
                $data['mainInvoiceList'] = array();
            }




            $data['programmeTypeList']= $this->student_model->programmeType();
            $data['organisationList']= $this->student_model->organisation();



            $data['programmeList']= $this->student_record_model->programmeListByStatus('');
            $data['intakeList']= $this->student_record_model->intakeListByStatus('1');
            $this->global['pageTitle'] = 'Campus Management System : List Students';
            $this->loadViews("student/list", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('student.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['getStudentData'] = $this->student_record_model->getStudentByStudentId($id);
            $data['studentDetails'] = $data['getStudentData'];

            $data['organisationDetails'] = $this->student_record_model->getOrganisation();
            $data['barrReleaseByStudentId'] = $this->student_record_model->barrReleaseByStudentId($id);
            $data['studentHasProgramme'] = $this->student_record_model->getStudentHasProgrammeByIdStudent($id);
            $data['applyChangeStatusListByStudentId'] = $this->student_record_model->applyChangeStatusListByStudentId($id);
            $data['studentStatus'] = $this->student_record_model->studentStatus($id);
            $data['studentHasInvoice'] = $this->student_record_model->studentHasInvoice($id);
            $data['studentHasReceipt'] = $this->student_record_model->studentHasReceipt($id);



            // $data['courseCompletedDetails'] = $this->student_record_model->courseCompletedDetails($data['studentDetails']->id_program,$data['studentDetails']->id_intake,$id);
            // echo "<Pre>";print_r($data['getStudentData']);exit;

            $this->global['pageTitle'] = 'Campus Management System : View Student Account Statements';
            $this->loadViews("student/edit", $this->global, $data, NULL);
        }
    }

    function delete_exam()
    {
        $id = $this->input->get('id');

       $this->student_model->deleteExamDetails($id);

       redirect($_SERVER['HTTP_REFERER']);
    }

    function delete_english()
    {
        $id = $this->input->get('id');

       $this->student_model->deleteProficiencyDetails($id);

       redirect($_SERVER['HTTP_REFERER']);  
    }

    function delete_employment()
    {
        $id = $this->input->get('id');

       $this->student_model->deleteEmploymentDetails($id);

       redirect($_SERVER['HTTP_REFERER']);  
    }

    function delete_document()
    {
        $id = $this->input->get('id');

       $this->student_model->deleteOtherDocument($id);

       redirect($_SERVER['HTTP_REFERER']);  
    }

    function deleteVisaDetails($id)
    {
        $this->student_model->deleteVisaDetails($id);
        echo "succes";exit;
       // redirect($_SERVER['HTTP_REFERER']);  
    }
}
