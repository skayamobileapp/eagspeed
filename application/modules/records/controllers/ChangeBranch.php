<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ChangeBranch extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('change_branch_model');
        $this->isLoggedIn();
    }

    function list()
    {
       if ($this->checkAccess('change_branch.list') == 0)
       {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['programmeList'] = $this->change_branch_model->programmeList();
            $data['studentList'] = $this->change_branch_model->studentList();
            $data['branchList'] = $this->change_branch_model->branchListByStatus('1');

            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_student'] = $this->security->xss_clean($this->input->post('id_student'));
            $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));
            $formData['id_branch'] = $this->security->xss_clean($this->input->post('id_branch'));
            $formData['id_new_branch'] = $this->security->xss_clean($this->input->post('id_new_branch'));
            $formData['status'] = '';
            
            $data['searchParameters'] = $formData;
            $data['changeBranchList'] = $this->change_branch_model->changeBranchListSearch($formData);
            // echo "<Pre>"; print_r($data['changeBranchList']);exit;

            $this->global['pageTitle'] = 'Campus Management System : Apply Change Scheme List';
            $this->loadViews("change_branch/list", $this->global, $data, NULL);
        }
    }

    function add()
    {
        if ($this->checkAccess('change_branch.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                
            // echo "<Pre>";print_r($this->input->post());exit;
                $id_user = $this->session->userId;

                $id_session = $this->session->my_session_id;

                $id_programme = $this->security->xss_clean($this->input->post('id_programme'));
                $id_new_branch = $this->security->xss_clean($this->input->post('id_new_branch'));
                $id_branch = $this->security->xss_clean($this->input->post('id_branch'));
                $id_student = $this->security->xss_clean($this->input->post('id_student'));
                $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
                $id_new_semester = $this->security->xss_clean($this->input->post('id_new_semester'));
                $fee = $this->security->xss_clean($this->input->post('fee'));
                $reason = $this->security->xss_clean($this->input->post('reason'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $data = array(
                    'id_programme' => $id_programme,
                    'id_new_branch' => $id_new_branch,
                    'id_intake' => $id_intake,
                    'id_student' => $id_student,
                    'id_new_semester' => $id_new_semester,
                    'id_branch' => $id_branch,
                    'reason' => $reason,
                    'status' => '0',
                    'created_by' => $id_user
                );
            // echo "<Pre>";print_r($data);exit;
                $result = $this->change_branch_model->addNewChangeBranch($data);

                $check_apply_status = $this->change_branch_model->getFeeStructureActivityType('CHANGE BRANCH','Application Level',$id_programme);
                if($check_apply_status)
                {
                    $data['add'] = 1;
                    $this->change_branch_model->generateMainInvoice($data,$result);
                }



                redirect('/records/changeBranch/list');
            }
            // $data['programmeList'] = $this->change_branch_model->programmeList();
            $data['studentList'] = $this->change_branch_model->studentList();
            $data['programmeList'] = $this->change_branch_model->programmeListByStatus('1');
            $data['intakeList'] = $this->change_branch_model->intakeListByStatus('1');
            $data['semesterList'] = $this->change_branch_model->semesterListByStatus('1');

            $this->global['pageTitle'] = 'Campus Management System : Add New Apply Change Scheme';
            $this->loadViews("change_branch/add", $this->global, $data, NULL);
        }
    }

    function edit($id)
    {

        if ($this->checkAccess('change_branch.view') == 0)
        {
                      
            $this->loadAccessRestricted();
        
        } else {

            if ($id == null)
            {
                redirect('/records/changeBranch/list');
            }
            if($this->input->post())
            {

                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                $id_student = $this->security->xss_clean($this->input->post('id_student'));
                $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
                $id_programme = $this->security->xss_clean($this->input->post('id_programme'));
                $id_new_programme = $this->security->xss_clean($this->input->post('id_new_programme'));
                $id_new_intake = $this->security->xss_clean($this->input->post('id_new_intake'));
                $id_new_semester = $this->security->xss_clean($this->input->post('id_new_semester'));
                $id_new_program_scheme = $this->security->xss_clean($this->input->post('id_new_program_scheme'));
                $fee = $this->security->xss_clean($this->input->post('fee'));
                $reason = $this->security->xss_clean($this->input->post('reason'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $data = array(
                    'id_student' => $id_student,
                    'id_programme' => $id_programme,
                    'id_intake' => $id_intake,
                    'id_new_programme' => $id_new_programme,
                    'id_new_intake' => $id_new_intake,
                    'id_new_semester' => $id_new_semester,
                    'id_new_program_scheme' => $id_new_program_scheme,
                    'fee' => $fee,
                    'reason' => $reason,
                    'status' => $status,
                    'created_by' => $id_user
                );
                // echo "<Pre>";print_r($data);exit();
                $result = $this->change_branch_model->editChangeBranch($data, $id);
                redirect('/records/changeBranch/list');
            }

            $data['programmeList'] = $this->change_branch_model->programmeList();
            $data['studentList'] = $this->change_branch_model->studentList();
            $data['changeBranch'] = $this->change_branch_model->getChangeBranch($id);
            $data['intakeList'] = $this->change_branch_model->intakeListByStatus('1');
            $data['semesterList'] = $this->change_branch_model->semesterListByStatus('1');


            

            $id_student = $data['changeBranch']->id_student;

            $tempData['id_programme'] = $data['changeBranch']->id_programme;
            $id_branch = $data['changeBranch']->id_branch;
            $id_new_branch = $data['changeBranch']->id_new_branch;
            $tempData['id_intake'] = $data['changeBranch']->id_intake;

            $data['oldBranch'] = $this->change_branch_model->getBranchDetails($id_branch);
            $data['newBranch'] = $this->change_branch_model->getBranchDetails($id_new_branch);

            



            // $data['feeStructure'] = $this->change_branch_model->getFeeByProgrammeNIntake($tempData);

            $data['programScheme'] = $this->change_branch_model->getProgramSchemeByProgramId($tempData['id_programme']);


            $data['studentDetails'] = $this->change_branch_model->getStudentByStudentId($id_student);
            // echo "<Pre>";print_r($data['feeStructure']);exit;
            $this->global['pageTitle'] = 'Campus Management System : View Apply Change Scheme';
            $this->loadViews("change_branch/edit", $this->global, $data, NULL);
        }
    }


    function view($id)
    {

        if ($this->checkAccess('change_branch.approve') == 0)
        {
                      
            $this->loadAccessRestricted();
        
        } else {

            if ($id == null)
            {
                redirect('/records/changeBranch/list');
            }
            if($this->input->post())
            {

                redirect('/records/changeBranch/list');
            }

            $data['programmeList'] = $this->change_branch_model->programmeList();
            $data['studentList'] = $this->change_branch_model->studentList();
            $data['changeBranch'] = $this->change_branch_model->getChangeBranch($id);
            $data['intakeList'] = $this->change_branch_model->intakeListByStatus('1');
            $data['semesterList'] = $this->change_branch_model->semesterListByStatus('1');


            

            $id_student = $data['changeBranch']->id_student;

            $tempData['id_programme'] = $data['changeBranch']->id_programme;
            $id_branch = $data['changeBranch']->id_branch;
            $id_new_branch = $data['changeBranch']->id_new_branch;
            $tempData['id_intake'] = $data['changeBranch']->id_intake;

            $data['oldBranch'] = $this->change_branch_model->getBranchDetails($id_branch);
            $data['newBranch'] = $this->change_branch_model->getBranchDetails($id_new_branch);

            



            // $data['feeStructure'] = $this->change_branch_model->getFeeByProgrammeNIntake($tempData);

            $data['programScheme'] = $this->change_branch_model->getProgramSchemeByProgramId($tempData['id_programme']);


            $data['studentDetails'] = $this->change_branch_model->getStudentByStudentId($id_student);
            
            $this->global['pageTitle'] = 'Campus Management System : View Apply Change Scheme';
            $this->loadViews("change_branch/view", $this->global, $data, NULL);
        }
    }


    function approval_list()
    {
        if ($this->checkAccess('change_branch.approval_list') == 0)
       {
            $this->loadAccessRestricted();
        }
        else
        {

           if($this->input->post())
            {
             $resultprint = $this->input->post();
             // echo "<Pre>"; print_r($resultprint['button']);exit;

             switch ($resultprint['button'])
             {
                 case 'Approve':
                     for($i=0;$i<count($resultprint['approval']);$i++)
                    {

                         $id = $resultprint['approval'][$i];
                         $data = array(
                            'status' => 1,
                        );
                        $result = $this->change_branch_model->editChangeBranch($data, $id);
                        if($result)
                        {
                            $this->change_branch_model->moveChangeBranchToHistory($id);

                            $change_branch = $this->change_branch_model->getChangeBranch($id);

                            $id_program = $change_branch->id_programme;

                            $check_apply_status = $this->change_branch_model->getFeeStructureActivityType('CHANGE BRANCH','Approval Level',$id_program);

                            // echo "<Pre>"; print_r($id_program);exit;
                            if($check_apply_status)
                            {
                                $data['add'] = 0;
                                $data['id_student'] = $change_branch->id_student;
                                $this->change_branch_model->generateMainInvoice($data,$id);
                            }
                        }


                    }
                        redirect('/records/changeBranch/approval_list');
                     break;


                     case 'search':

                    $data['programmeList'] = $this->change_branch_model->programmeList();
            $data['studentList'] = $this->change_branch_model->studentList();
            $data['branchList'] = $this->change_branch_model->branchListByStatus('1');

            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_student'] = $this->security->xss_clean($this->input->post('id_student'));
            $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));
            $formData['id_branch'] = $this->security->xss_clean($this->input->post('id_branch'));
            $formData['id_new_branch'] = $this->security->xss_clean($this->input->post('id_new_branch'));
            $formData['status'] = 0;
            
            $data['searchParameters'] = $formData;
            $data['changeBranchApprovalList'] = $this->change_branch_model->changeBranchListForApprovalSearch($formData);
                     
                     break;
                 
                 default:
                     break;
             }
                
            }
            $data['programmeList'] = $this->change_branch_model->programmeList();
            $data['studentList'] = $this->change_branch_model->studentList();
            $data['branchList'] = $this->change_branch_model->branchListByStatus('1');

            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_student'] = $this->security->xss_clean($this->input->post('id_student'));
            $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));
            $formData['id_branch'] = $this->security->xss_clean($this->input->post('id_branch'));
            $formData['id_new_branch'] = $this->security->xss_clean($this->input->post('id_new_branch'));
            $formData['status'] = 0;
            
            $data['searchParameters'] = $formData;
            $data['changeBranchApprovalList'] = $this->change_branch_model->changeBranchListForApprovalSearch($formData);
            // echo "<Pre>"; print_r($data['applyChangeStatusApprovalList']);exit;

             $this->global['pageTitle'] = 'Campus Management System : Apply Change Scheme Approval';
            $this->loadViews("change_branch/approval_list", $this->global, $data, NULL);


        }

    }

    function getPreviousBranchByProgramId($id_program)
    {
         $intake_data = $this->change_branch_model->getPreviousBranchByProgramId($id_program);

            // echo "<Pre>"; print_r($intake_data);exit;
             $table="
            <script type='text/javascript'>

            $('select').select2();
                            
            </script>


            <select name='id_branch' id='id_branch' class='form-control' onchange='getStudentByData()'>
            <option value=''>Select</option>
            ";

            for($i=0;$i<count($intake_data);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $intake_data[$i]->id;
            $code = $intake_data[$i]->code;
            $name = $intake_data[$i]->name;

            $table.="<option value=".$id.">". $code . " - " .  $name .
                    "</option>";

            }
            $table.="</select>";

            echo $table;  
    }

    function getIntakeByProgramme($id_programme)
    {
            $intake_data = $this->change_branch_model->getIntakeByProgrammeId($id_programme);

            // echo "<Pre>"; print_r($intake_data);exit;

            $table="
            <script type='text/javascript'>
                $('select').select2();
            </script>


            <select name='id_intake' id='id_intake' class='form-control' onchange='getStudentByData()'>
            <option value=''>Select</option>";

            for($i=0;$i<count($intake_data);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $intake_data[$i]->id;
            $name = $intake_data[$i]->name;
            $year = $intake_data[$i]->year;

            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="</select>";

            echo $table;
    }

    function getStudentByData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        // $id_programme = $tempData['id_programme'];
        // $id_program_scheme = $tempData['id_program_scheme'];
        // $id_intake = $tempData['id_intake'];

        $student_data = $this->change_branch_model->getStudentByData($tempData);

        // echo "<Pre>";print_r($student_data);exit();

        $table="
            <script type='text/javascript'>
                $('select').select2();
            </script>


            <select name='id_student' id='id_student' class='form-control' onchange='getStudentByStudentId(this.value)'>
            <option value=''>Select</option>";

            for($i=0;$i<count($student_data);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $student_data[$i]->id;
            $full_name = $student_data[$i]->full_name;
            $nric = $student_data[$i]->nric;

            $table.="<option value=".$id.">" . $nric . " - " . $full_name.
                    "</option>";

            }
            $table.="</select>";

            echo $table;

    }


    function getStudentByStudentId($id)
    {
         // print_r($id);exit;
            $student_data = $this->change_branch_model->getStudentByStudentId($id);
            // echo "<Pre>"; print_r($student_data);exit;

            $student_name = $student_data->full_name;
            $student_nric = $student_data->nric;
            $email = $student_data->email_id;
            $nric = $student_data->nric;
            $intake_name = $student_data->intake_name;
            $id_intake = $student_data->id_intake;
            $programme_name = $student_data->programme_name;
            $branch_code = $student_data->branch_code;
            $branch_name = $student_data->branch_name;


            $table  = "



             <h4 class='sub-title'>Student Details</h4>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student Name :</dt>
                                <dd>$student_name</dd>
                            </dl>
                            <dl>
                                <dt>Student Email :</dt>
                                <dd>$email</dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd>$nric</dd>
                            </dl>
                            <dl>
                                <dt>Branch :</dt>
                                <dd>$branch_code - $branch_name</dd>
                            </dl>
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Intake :</dt>
                                <dd>

                                    <input type='hidden' name='id_intake' id='id_intake' value='$id_intake' /> $intake_name

                                </dd>
                            </dl>
                            <dl>
                                <dt>Programme :</dt>
                                <dd>$programme_name</dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd></dd>
                            </dl>
                        </div>
    
                    </div>
                </div>";

                $table1  ="






            <h4 style='text-align: center;'><b>Student Details</b></h4>
            <table align='center' style='border: 1px solid;border-radius: 12px; width: 90%;'>
                <tr>
                    <td colspan='4'></td>
                </tr>
                <tr>
                    <th style='text-align: center;'>Student Name :</th>
                    <td style='text-align: left;'>$student_name</td>
                    <th style='text-align: center;'>Intake :</th>
                    <td style='text-align: left;'> <input type='hidden' name='id_intake' id='id_intake' value='$id_intake' /> $intake_name</td>
                </tr>
                <tr>
                    <th style='text-align: center;'>Student Email :</th>
                    <td style='text-align: left;'>$email</td>
                    <th style='text-align: center;'>Programme :</th>
                    <td style='text-align: left;'>$programme_name</td>
                </tr>
                <tr>
                    <th style='text-align: center;'>Student NRIC :</th>
                    <td style='text-align: left;'>$nric</td>
                    <th style='text-align: center;'></th>
                    <td style='text-align: center;'></td>
                </tr>

            </table>
            <br>
            <br>
            ";
            echo $table;
            exit;
    }

    function getFeeByProgrammeNIntake()
    {
        $id_session = $this->session->my_session_id;

        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        // echo "<Pre>";print_r($tempData);exit();
        $id_programme = $tempData['id_programme'];
        $id_intake = $tempData['id_intake'];
        $id_program_scheme = $tempData['id_program_scheme'];
        
        $data = $this->change_branch_model->getFeeByProgrammeNIntake($tempData);


        // echo "<Pre>";print_r($data);exit();

         $table = "<table  class='table' id='list-table'>
                  <tr>
                    <th>Sl. No</th>
                    <th>Fee Item</th>
                    <th>Amount</th>
                </tr>";

                    $total_amount = 0;
                    $i=1;
                    foreach ($data as $value)
                    {
                        $id = $value->id;
                        $fee_setup = $value->fee_setup;
                        $fee_code = $value->fee_code;
                        $amount = $value->amount;
                        $fee = $fee_code . " - " . $fee_code;
                        
                    
                    $table .= "
                        <tr>
                            <td>$i</td>
                            <td>$fee</td>
                            <td>$amount</td>
                        </tr>";
                        $total_amount = $total_amount + $value->amount;
                        $i++;
                    }

                    $table .= "
                        <tr>
                            <td></td>
                            <td style='text-align: right'>Total : </td>
                            <td><input type='hidden' id='amount' value='$total_amount' />$total_amount</td>
                        </tr>";

        $table.= "</table>";

        // $table="
        // <input type='number' hidden='hidden' class='form-control' id='fee' name='fee' readonly='readonly' value='$total_amount'>

        // ";


        
        echo $table;
            // exit;      
    }


    function getBranchesListByProgramId($id_program)
    {
         $intake_data = $this->change_branch_model->getBranchesListByProgramId($id_program);

            // echo "<Pre>"; print_r($intake_data);exit;
             $table="
            <script type='text/javascript'>

            $('select').select2();
                            
            </script>


            <select name='id_new_branch' id='id_new_branch' class='form-control' onchange='getFeeByProgrammeNIntake()'>
            <option value=''>Select</option>
            ";

            for($i=0;$i<count($intake_data);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $intake_data[$i]->id;
            $code = $intake_data[$i]->code;
            $name = $intake_data[$i]->name;

            $table.="<option value=".$id.">". $code . " - " .  $name .
                    "</option>";

            }
            $table.="</select>";

            echo $table;  
    }
    
}
