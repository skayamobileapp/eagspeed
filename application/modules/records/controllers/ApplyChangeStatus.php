<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ApplyChangeStatus extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('apply_change_status_model');
        $this->load->model('change_status_model');
        $this->isLoggedIn();
    }

    function list()
    {
       if ($this->checkAccess('apply_change_status.list') == 0)
       {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['changeStatusList'] = $this->change_status_model->changeStatusList();
            $data['semesterList'] = $this->apply_change_status_model->semesterList();
            $data['studentList'] = $this->apply_change_status_model->studentList();

            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_change_status'] = $this->security->xss_clean($this->input->post('id_change_status'));
            $formData['id_student'] = $this->security->xss_clean($this->input->post('id_student'));
            $formData['id_semester'] = $this->security->xss_clean($this->input->post('id_semester'));
            
            $data['searchParameters'] = $formData;
            $data['applyChangeStatusList'] = $this->apply_change_status_model->applyChangeStatusListSearch($formData);
            // echo "<Pre>"; print_r($data['barringList']);exit;

            $this->global['pageTitle'] = 'Campus Management System : Apply Change Status List';
            $this->loadViews("apply_change_status/list", $this->global, $data, NULL);
        }
    }

    function add()
    {
        if ($this->checkAccess('apply_change_status.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                $id_student = $this->security->xss_clean($this->input->post('id_student'));
                $id_change_status = $this->security->xss_clean($this->input->post('id_change_status'));
                $id_program = $this->security->xss_clean($this->input->post('id_programme'));
                $id_semester = $this->security->xss_clean($this->input->post('id_semester'));
                $reason = $this->security->xss_clean($this->input->post('reason'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $data = array(
                    'id_student' => $id_student,
                    'id_change_status' => $id_change_status,
                    'id_semester' => $id_semester,
                    'id_program' => $id_program,
                    'reason' => $reason,
                    'status' => '0',
                    'created_by' => $id_user
                );
                $result = $this->apply_change_status_model->addNewApplyChangeStatus($data);

                // echo "<Pre>"; print_r($check_apply_status);exit;

                $check_apply_status = $this->apply_change_status_model->getFeeStructureActivityType('CHANGE STATUS','Application Level',$id_program);
                if($check_apply_status)
                {
                    $data['add'] = 1;
                    $this->apply_change_status_model->generateMainInvoice($data,$result);
                }
                // echo "<Pre>"; print_r($data);exit;


                redirect('/records/applyChangeStatus/list');
            }
            $data['changeStatusList'] = $this->change_status_model->changeStatusList();
            $data['semesterList'] = $this->apply_change_status_model->semesterList();
            $data['studentList'] = $this->apply_change_status_model->studentList();
            $data['programmeList'] = $this->apply_change_status_model->programmeListByStatus('1');

            $this->global['pageTitle'] = 'Campus Management System : Add New Apply Change Status';
            $this->loadViews("apply_change_status/add", $this->global, $data, NULL);
        }
    }

    function edit($id)
    {

        if ($this->checkAccess('apply_change_status.view') == 0)
        {
                      
            $this->loadAccessRestricted();
        
        } else {

            if ($id == null)
            {
                redirect('/records/applyChangeStatus/add');
            }
            if($this->input->post())
            {

                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                $id_student = $this->security->xss_clean($this->input->post('id_student'));
                $id_change_status = $this->security->xss_clean($this->input->post('id_change_status'));
                $id_semester = $this->security->xss_clean($this->input->post('id_semester'));
                $id_program = $this->security->xss_clean($this->input->post('id_program'));
                $reason = $this->security->xss_clean($this->input->post('reason'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $data = array(
                    'id_student' => $id_student,
                    'id_change_status' => $id_change_status,
                    'id_semester' => $id_semester,
                    'id_program' => $id_program,
                    'reason' => $reason,
                    'status' => $status,
                    'updated_by' => $id_user
                );
                $result = $this->apply_change_status_model->editApplyChangeStatus($data, $id);
                redirect('/records/applyChangeStatus/list');
            }

            $data['changeStatusList'] = $this->change_status_model->changeStatusList();
            $data['semesterList'] = $this->apply_change_status_model->semesterList();
            $data['studentList'] = $this->apply_change_status_model->studentList();
            $data['applyChangeStatus'] = $this->apply_change_status_model->getApplyChangeStatus($id);
            $id_student = $data['applyChangeStatus']->id_student;
            $data['studentDetails'] = $this->apply_change_status_model->getStudentByStudentId($id_student);
            // echo "<Pre>";print_r($data['studentDetails']);exit;
            $this->global['pageTitle'] = 'Campus Management System : Edit Apply Change Status';
            $this->loadViews("apply_change_status/edit", $this->global, $data, NULL);
        }
    }

    function view($id)
    {

        if ($this->checkAccess('apply_change_status.edit') == 0)
        {
                      
            $this->loadAccessRestricted();
        
        } else {

            if ($id == null)
            {
                redirect('/records/applyChangeStatus/add');
            }
            if($this->input->post())
            {

            }

            $data['changeStatusList'] = $this->change_status_model->changeStatusList();
            $data['semesterList'] = $this->apply_change_status_model->semesterList();
            $data['studentList'] = $this->apply_change_status_model->studentList();
            $data['applyChangeStatus'] = $this->apply_change_status_model->getApplyChangeStatus($id);
            $id_student = $data['applyChangeStatus']->id_student;
            $data['studentDetails'] = $this->apply_change_status_model->getStudentByStudentId($id_student);
            // echo "<Pre>";print_r($data['barring']);exit;
            $this->global['pageTitle'] = 'Campus Management System : Edit Apply Change Status';
            $this->loadViews("apply_change_status/view", $this->global, $data, NULL);
        }
    }

    function approval_list()
    {
        if ($this->checkAccess('apply_change_status.approval_list') == 0)
       {
            $this->loadAccessRestricted();
        }
        else
        {

           if($this->input->post())
            {
             $resultprint = $this->input->post();
             // echo "<Pre>"; print_r($resultprint);exit;

             switch ($resultprint['button'])
             {
                 case 'Approve':
                     for($i=0;$i<count($resultprint['approval']);$i++)
                    {

                         $id = $resultprint['approval'][$i];
                         $data = array(
                            'status' => 1,
                        );
                        $result = $this->apply_change_status_model->editApplyChangeStatus($data, $id);

                        if($result)
                        {
                            $change_status = $this->apply_change_status_model->getApplyChangeStatus($id);

                            $id_program = $change_status->id_program;

                            $check_apply_status = $this->apply_change_status_model->getFeeStructureActivityType('CHANGE STATUS','Approval Level',$id_program);

                            // echo "<Pre>"; print_r($id_program);exit;
                            if($check_apply_status)
                            {
                                $data['add'] = 0;
                                $data['id_student'] = $change_status->id_student;
                                $this->apply_change_status_model->generateMainInvoice($data,$id);
                            }
                        }


                    }
                        redirect('/records/applyChangeStatus/approval_list');
                     break;


                case 'search':

                    $data['changeStatusList'] = $this->change_status_model->changeStatusList();
                    $data['semesterList'] = $this->apply_change_status_model->semesterList();
                    $data['studentList'] = $this->apply_change_status_model->studentList();

                    $formData['name'] = $this->security->xss_clean($this->input->post('name'));
                    $formData['id_change_status'] = $this->security->xss_clean($this->input->post('id_change_status'));
                    $formData['id_student'] = $this->security->xss_clean($this->input->post('id_student'));
                    $formData['id_semester'] = $this->security->xss_clean($this->input->post('id_semester'));
                    
                    $data['searchParameters'] = $formData;
                    $data['applyChangeStatusApprovalList'] = $this->apply_change_status_model->applyChangeStatusListForApprovalSearch($formData);
                     
                     break;
                 
                 default:
                     break;
             }
            }
            $data['changeStatusList'] = $this->change_status_model->changeStatusList();
            $data['semesterList'] = $this->apply_change_status_model->semesterList();
            $data['studentList'] = $this->apply_change_status_model->studentList();

            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_change_status'] = $this->security->xss_clean($this->input->post('id_change_status'));
            $formData['id_student'] = $this->security->xss_clean($this->input->post('id_student'));
            $formData['id_semester'] = $this->security->xss_clean($this->input->post('id_semester'));
            
            $data['searchParameters'] = $formData;
            $data['applyChangeStatusApprovalList'] = $this->apply_change_status_model->applyChangeStatusListForApprovalSearch($formData);
            // echo "<Pre>"; print_r($data['applyChangeStatusApprovalList']);exit;

            $this->global['pageTitle'] = 'Campus Management System : ApplyChangeStatus List';
            $this->loadViews("apply_change_status/approval_list", $this->global, $data, NULL);
        }

    }

    function getStudentByProgrammeId($id)
     {       
            // print_r($id);exit;
            $results = $this->apply_change_status_model->getStudentByProgrammeId($id);
            $programme_data = $this->apply_change_status_model->getProgrammeById($id);

            // echo "<Pre>"; print_r($programme_data);exit;
            $programme_name = $programme_data->name;
            $programme_code = $programme_data->code;
            $total_cr_hrs = $programme_data->total_cr_hrs;
            $graduate_studies = $programme_data->graduate_studies;
            $foundation = $programme_data->foundation;

            $table="


            <script type='text/javascript'>
                $('select').select2();
            </script>

            <select name='id_student' id='id_student' class='form-control' onchange='getStudentByStudentId(this.value)'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $full_name = $results[$i]->full_name;
            $nric = $results[$i]->nric;
            $table.="<option value=".$id.">" . $nric . " - " . $full_name.
                    "</option>";

            }
            $table.="</select>";

            $view  = "
            <table border='1px' style='width: 100%'>
                <tr>
                    <td colspan='4'><h5 style='text-align: center;'>Programme Details</h5></td>
                </tr>
                <tr>
                    <th style='text-align: center;'>Programme Name</th>
                    <td style='text-align: center;'>$programme_name</td>
                    <th style='text-align: center;'>Programme Code</th>
                    <td style='text-align: center;'>$programme_code</td>
                </tr>
                <tr>
                    <th style='text-align: center;'>Total Credit Hours</th>
                    <td style='text-align: center;'>$total_cr_hrs</td>
                    <th style='text-align: center;'>Graduate Studies</th>
                    <td style='text-align: center;'>$graduate_studies</td>
                </tr>

            </table>
            <br>
            <br>
            ";

            // $d['table'] = $table;
            // $d['view'] = $view;

            echo $table;
            exit;
    }

    function getStudentByStudentId($id)
    {
         // print_r($id);exit;
            $student_data = $this->apply_change_status_model->getStudentByStudentId($id);
            // echo "<Pre>"; print_r($student_data);exit;

            $student_name = $student_data->full_name;
            $student_nric = $student_data->nric;
            $email = $student_data->email_id;
            $nric = $student_data->nric;
            $nationality = $student_data->nationality;
            $programme_name = $student_data->programme_name;

            $table  = "



             <h4 class='sub-title'>Student Details</h4>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student Name :</dt>
                                <dd>$student_name</dd>
                            </dl>
                            <dl>
                                <dt>Student Email :</dt>
                                <dd>$email</dd>
                            </dl>
                            <dl>
                                <dt>Nationality :</dt>
                                <dd>$nationality</dd>
                            </dl>
                            
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd>$nric</dd>
                            </dl>
                            <dl>
                                <dt>Programme :</dt>
                                <dd>$programme_name</dd>
                            </dl>
                            
                        </div>
    
                    </div>
                </div>
                
                <br>
                
                ";


            
            echo $table;
            exit;
    }
}
