<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Barring extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('barring_model');
        $this->load->model('setup/semester_model');
        $this->load->model('barring_type_model');
        $this->isLoggedIn();
    }

    function list()
    {
       if ($this->checkAccess('barring.list') == 0)
       {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['barringTypeList'] = $this->barring_type_model->barringTypeList();
            $data['programList'] = $this->barring_model->programListByStatus('1');
            $data['studentList'] = $this->barring_model->studentList();

            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_barring_type'] = $this->security->xss_clean($this->input->post('id_barring_type'));
            $formData['id_student'] = $this->security->xss_clean($this->input->post('id_student'));
            $formData['id_program'] = $this->security->xss_clean($this->input->post('id_program'));
            
            $data['searchParameters'] = $formData;
            $data['barringList'] = $this->barring_model->barringListSearch($formData);
            // echo "<Pre>"; print_r($data['barringList']);exit;

            $this->global['pageTitle'] = 'Campus Management System : Barring List';
            $this->loadViews("barring/list", $this->global, $data, NULL);
        }
    }

    function add()
    {
        if ($this->checkAccess('barring.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                $id_student = $this->security->xss_clean($this->input->post('id_student'));
                $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
                $id_program = $this->security->xss_clean($this->input->post('id_programme'));
                $id_barring_type = $this->security->xss_clean($this->input->post('id_barring_type'));
                $id_semester = $this->security->xss_clean($this->input->post('id_semester'));
                $reason = $this->security->xss_clean($this->input->post('reason'));
                $barring_date = $this->security->xss_clean($this->input->post('barring_date'));
                $barring_to_date = $this->security->xss_clean($this->input->post('barring_to_date'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $data = array(
                    'id_program' => $id_program,
                    'id_intake' => $id_intake,
                    'id_student' => $id_student,
                    'id_barring_type' => $id_barring_type,
                    'id_semester' => $id_semester,
                    'reason' => $reason,
                    'barring_date' => date('Y-m-d',strtotime($barring_date)),
                    'barring_to_date' => date('Y-m-d',strtotime($barring_to_date)),
                    'status' => $status,
                    'created_by' => $id_user
                );
                $result = $this->barring_model->addNewBarring($data);
                redirect('/records/barring/list');
            }
            $data['barringTypeList'] = $this->barring_type_model->barringTypeList();
            $data['semesterList'] = $this->semester_model->semesterList();
            $data['studentList'] = $this->barring_model->studentList();
            $data['programList'] = $this->barring_model->programListByStatus('1');

            $this->global['pageTitle'] = 'Campus Management System : Add New Barring';
            $this->loadViews("barring/add", $this->global, $data, NULL);
        }
    }

    function edit($id)
    {

        if ($this->checkAccess('barring.edit') == 0)
        {
                      
            $this->loadAccessRestricted();
        
        } else {

            if ($id == null)
            {
                redirect('/records/barring/list');
            }
            if($this->input->post())
            {

                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                $id_student = $this->security->xss_clean($this->input->post('id_student'));
                $id_barring_type = $this->security->xss_clean($this->input->post('id_barring_type'));
                $id_semester = $this->security->xss_clean($this->input->post('id_semester'));
                $reason = $this->security->xss_clean($this->input->post('reason'));
                $barring_date = $this->security->xss_clean($this->input->post('barring_date'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $data = array(
                    'id_student' => $id_student,
                    'id_barring_type' => $id_barring_type,
                    'id_semester' => $id_semester,
                    'reason' => $reason,
                    'barring_date' => date('Y-m-d',strtotime($barring_date)),
                    'status' => $status,
                    'created_by' => $id_user
                );
                $result = $this->barring_model->editBarring($data, $id);
                redirect('/records/barring/list');
            }

            $data['barringTypeList'] = $this->barring_type_model->barringTypeList();
            $data['semesterList'] = $this->semester_model->semesterList();
            $data['studentList'] = $this->barring_model->studentList();
            $data['programList'] = $this->barring_model->programListByStatus('1');
            $data['barring'] = $this->barring_model->getBarring($id);

            // echo "<Pre>";print_r($data['programList']);exit;
            $this->global['pageTitle'] = 'Campus Management System : Edit Barring';
            $this->loadViews("barring/edit", $this->global, $data, NULL);
        }
    }


    function getIntakes()
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $student_list_data = $this->barring_model->getIntakeListByProgramme($tempData['id_programme']);

        $table="

        <script type='text/javascript'>
            $('select').select2();
        </script>

        <select name='id_intake' id='id_intake' class='form-control' onchange='getStudentByProgNIntake()'>";
        $table.="<option value=''>Select</option> 

        
                ";

        for($i=0;$i<count($student_list_data);$i++)
        {

        $id = $student_list_data[$i]->id_intake;
        $intake_name = $student_list_data[$i]->intake_name;
        $intake_year = $student_list_data[$i]->intake_year;


        $table.="<option value=".$id.">". $intake_year . " - " . $intake_name.
                "</option>";

        }
        $table.="</select>";
        
        echo $table;
        exit();
    }

    function getStudentByProgNIntake()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $student_list_data = $this->barring_model->getStudentByProgNIntake($tempData);


        $table="

        <script type='text/javascript'>
            $('select').select2();
        </script>

        <select name='id_student' id='id_student' class='form-control' onchange='displayStudentData()'>";
        $table.="<option value=''>Select</option> 

        
                ";

        for($i=0;$i<count($student_list_data);$i++)
        {
        $id = $student_list_data[$i]->id;
        $full_name = $student_list_data[$i]->full_name;
        $nric = $student_list_data[$i]->nric;
        $table.="<option value=".$id.">".$nric. " - " . $full_name.
                "</option>";
        }
        $table.="</select>";
        
        echo $table;
        exit();
    }

    function displayStudentData()
    {
        
        // $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
        $id_programme = $this->security->xss_clean($this->input->post('id_programme'));
        $id_student = $this->security->xss_clean($this->input->post('id_student'));
         // echo "<Pre>"; print_r($id_student);exit();

        $student_data = $this->barring_model->getStudentByStudentId($id_student);

        // echo "<Pre>"; print_r($temp_details);exit();


            $student_name = $student_data->full_name;
            $student_nric = $student_data->nric;
            $email = $student_data->email_id;
            $nric = $student_data->nric;
            $intake_name = $student_data->intake_name;
            $id_intake = $student_data->id_intake;
            $programme_name = $student_data->programme_name;
            $programme_code = $student_data->programme_code;
            $nationality = $student_data->nationality;
            // $program_scheme = $student_data->program_scheme;
            // $advisor_name = $student_data->advisor_name;




        $table = "

            <h4 class='sub-title'>Student Details</h4>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student Name :</dt>
                                <dd>$student_name</dd>
                            </dl>
                            <dl>
                                <dt>Student Email :</dt>
                                <dd>$email</dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd>$nric</dd>
                            </dl>
                            
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Programme :</dt>
                                <dd>$programme_code - $programme_name</dd>
                            </dl>
                            <dl>
                                <dt>Nationality :</dt>
                                <dd>$nationality</dd>
                            </dl>
                        </div>
    
                    </div>
                </div>";        
        echo $table;
    }
}
