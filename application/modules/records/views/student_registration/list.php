<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List Student Records</h3>
      <!-- <a href="add" class="btn btn-primary">+ Add Main Invoice</a> -->
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">
                

                <div class="row">

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Student Name</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="name" value="<?php echo $searchParam['name']; ?>">
                    </div>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Student NRIC</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="nric" value="<?php echo $searchParam['nric']; ?>">
                    </div>
                  </div>
                </div>

                

              </div>

              <div class="row">

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Student Email</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="email_id" id="email_id" value="<?php echo $searchParam['email_id']; ?>">
                    </div>
                  </div>
                </div>


                

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Programme </label>
                    <div class="col-sm-8">
                      <select name="id_programme" id="id_programme" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($programmeList)) {
                          foreach ($programmeList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['id_programme']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->code ."-".$record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>                      

              </div> 



              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <button type="reset" class="btn btn-link" onclick="clearSearchForm()">Clear All Fields</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table" border="1">
        <thead>
                      <th>Sl No</th>

            <th>Transaction Date and Time</th>
            <th>Name</th>
            <th>Email</th>
            <th>Transaction Id</th>
            <th style="width: 10%;">Number of Course</th>
            <th>Course</th>
            <th>Amount</th>
            <th>Discount</th>
            <th>Amount Paid</th>
            <th>Payment Status</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($mainInvoiceList)) {
            $i=1;
                    $this->load->model('student_record_model');

            foreach ($mainInvoiceList as $record) {
              $invoiceDetails = array();
                $invoiceDetails = $this->student_record_model->getMainInvoiceDetailsById($record->invoiceid);



          ?>
              <tr>
                                <td><?php echo $i ?></td>

                <td><?php echo date('d-m-Y H:i',strtotime($record->date_time)) ?></td>
                <td><?php echo $record->full_name ?></td>
                <td><?php echo $record->email_id ?></td>
                <td><?php echo $record->invoice_number ?></td>
                <td><?php echo count($invoiceDetails) ?></td>
                <td>
                   
                       <?php for($k=0;$k<count($invoiceDetails);$k++) {?>
                        
                             <?php echo '<b>'.$invoiceDetails[$k]->name.'</b>'.'<br/>'.date('d-m-Y',strtotime($invoiceDetails[$k]->start_date)).' - '.date('d-m-Y',strtotime($invoiceDetails[$k]->name));?>
                        

                       <?php } ?> 
                
                </td>
                <td><?php echo $record->total_amount ?></td>
                <td><?php echo $record->total_discount ?></td>
                <td><?php echo $record->paid_amount ?></td>
                <td><?php if($record->total_amount>0 && $record->paid_amount>0) {
                       echo "Paid";
                 } else if($record->total_amount>0 && $record->paid_amount==0) {
                       echo "Payment Pending";
                 } else { 
                        echo "Free Course";
                  }?>
               </td>
               
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script type="text/javascript">
    
  $('select').select2();

  function clearSearchForm()
      {
        window.location.reload();
      }
</script>