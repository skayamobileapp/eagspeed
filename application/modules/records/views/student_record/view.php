<?php $this->load->helper("form"); ?>
<?php 
   // require('ckeditor/ckeditor.php');
    ?>
<div class="container-fluid page-wrapper">
   <div class="main-container clearfix">
      <div class="page-title clearfix">
         <h3>Student Record</h3>
         <a href="../list" class="btn btn-link btn-back">‹ Back</a>
      </div>



       <div class="form-container">
          <h4 class="form-group-title">Student Details</h4>

              <div class='data-list'>
                  <div class='row'>
  
                      <div class='col-sm-6'>
                          <dl>
                              <dt>Student Name :</dt>
                              <dd><?php echo ucwords($getStudentData->full_name); ?></dd>
                          </dl>
                          <dl>
                              <dt>Company / Organisation :</dt>
                              <dd><?php

                              if($getStudentData->id_university != 1 && $getStudentData->id_university != 0)
                              {
                                  echo $getStudentData->partner_university_code , " - " . $getStudentData->partner_university_name;
                              }
                              elseif($getStudentData->id_company != '0')
                              {
                                  echo ucwords($getStudentData->company_name);
                              }
                              else
                              {
                                  echo $organisationDetails->short_name . " - " . $organisationDetails->name;
                              }
                              
                              ?></dd>
                          </dl>
                      </div>        
                      
                      <div class='col-sm-6'>
                          <dl>
                              <dt>Student NRIC :</dt>
                              <dd><?php echo $getStudentData->nric ?></dd>
                          </dl>
                          <dl>
                              <dt>Student Email :</dt>
                              <dd><?php echo $getStudentData->email_id; ?></dd>
                          </dl>
                          
                      </div>
  
                  </div>
              </div>


       </div>


         



        <div class="form-container">
            <h4 class="form-group-title">Student Records Details</h4>
            <div class="m-auto text-center">
               <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>

            <div class="clearfix">
               <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                  <li role="presentation" class="active" ><a href="#tab_4" class="nav-link border rounded text-center"
                     aria-controls="tab_4" aria-selected="true"
                     role="tab" data-toggle="tab">Program Registration</a>
                  </li>
                  <li role="presentation"><a href="#tab_1" class="nav-link border rounded text-center"
                     aria-controls="tab_1" role="tab" data-toggle="tab">Statement Of Accounts</a>
                  </li>
                
               </ul>


              <div class="tab-content offers-tab-content">


                  <div role="tabpanel" class="tab-pane" id="tab_1">
                     <div class="mt-4">
                        


                            <div class="form-container">
                              <h4 class="form-group-title">Statement Of Accounts</h4>
                              <div class="m-auto text-center">
                                 <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
                              </div>

                              <div class="clearfix">
                                 <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                                    <li role="presentation" class="active" ><a href="#tab_ivvoice1" class="nav-link border rounded text-center"
                                       aria-controls="tab_ivvoice1" aria-selected="true"
                                       role="tab" data-toggle="tab">Invoice</a>
                                    </li>
                                    <li role="presentation"><a href="#tab_receipt1" class="nav-link border rounded text-center"
                                       aria-controls="tab_receipt1" role="tab" data-toggle="tab">Receipt</a>
                                    </li>
                                 </ul>


                                <div class="tab-content offers-tab-content">




                                    <div role="tabpanel" class="tab-pane active" id="tab_ivvoice1">
                                       <div class="mt-4">


                                          




                                        <div class="form-container">
                                          <h4 class="form-group-title">Invoice Details</h4>



                                          <div class="custom-table" id="printReceipt">
                                             <table class="table" id="list-table">
                                                <thead>
                                                   <tr>
                                                      <th>Sl. No</th>
                                                      <th>Invoice Number</th>
                                                      <th>Invoice Date</th>
                                                      <th>Invoice Amount</th>
                                                      <th>Discount Amount</th>
                                                      <th>Total Payable</th>
                                                      <th>Download</th>
                                                   </tr>
                                                </thead>
                                                <tbody>
                                                   <?php
                                                      if (!empty($studentHasInvoice)) {
                                                          $i=1;
                                                          foreach ($studentHasInvoice as $record) {
                                                      ?>
                                                   <tr>
                                                      <td><?php echo $i ?></td>
                                                      <td><?php echo $record->invoice_number; ?></td>
                                                      <td><?php echo date('d-m-Y H:i:s', strtotime($record->created_dt_tm)); ?></td>
                                                      <td><?php echo $record->total_amount; ?></td>
                                                      <td><?php echo $record->total_discount; ?></td>
                                                      <td><?php echo $record->total_amount; ?></td>
                                                      <td><a href="/profile/download/generateMainInvoice/<?php echo $record->id;?>">Download</a></td>
                                                   </tr>
                                                   <?php
                                                      $i++;
                                                          }
                                                      }
                                                      ?>
                                                </tbody>
                                             </table>
                                          </div>


                                            

                                        </div>




                                       </div>
                                    
                                    </div>

                                    <div role="tabpanel" class="tab-pane" id="tab_receipt1">
                                       <div class="mt-4">
                                          

                                          <div class="form-container">
                                          <h4 class="form-group-title">Receipt Details</h4>


                                            <div class="custom-table" id="printReceipt">
                                               <table class="table" id="list-table">
                                                  <thead>
                                                     <tr>
                                                        <th>Sl. No</th>
                                                        <th>Receipt Number</th>
                                                        <!-- <th>Invoices</th> -->
                                                        <th>Currency</th>
                                                        <th>Receipt Date Time</th>
                                                        <th>Receipt Amount</th>
                                                     </tr>
                                                  </thead>
                                                  <tbody>
                                                     <?php
                                                        if (!empty($studentHasReceipt)) {
                                                            $i=1;
                                                            foreach ($studentHasReceipt as $record) {
                                                        ?>
                                                     <tr>
                                                        <td><?php echo $i ?></td>
                                                        <td><?php echo $record->receipt_number; ?></td>
                                                        <td><?php echo $record->currency_name; ?></td>
                                                        <td><?php echo date('d-m-Y H:i:s', strtotime($record->created_dt_tm)); ?></td>
                                                        <td><?php echo $record->receipt_amount; ?></td>
                                                        <td> <a href="/profile/download/generateReceipt/<?php echo $record->id; ?>">Download</a></td>
                                                     </tr>
                                                     <?php
                                                        $i++;
                                                            }
                                                        }
                                                        ?>
                                                  </tbody>
                                               </table>
                                            
                                            </div>

                                          </div>

                                          
                                          
                                          

                                       </div>
                                    
                                    </div>



                          
                                </div>

                              </div>

                          </div>




                        

                        

                     </div>
                  
                  </div>




                  <div role="tabpanel" class="tab-pane" id="tab_2">
                     <div class="mt-4">

                        <div class="custom-table" id="printReceipt">
                           <table class="table" id="list-table">
                              <thead>
                                 <tr>
                                    <th>Sl. No</th>
                                    <th>Type</th>
                                    <th>ID Type</th>
                                    <th>Reason</th>
                                    <th>Date</th>
                                    <th>User</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <?php
                                    if (!empty($barrReleaseByStudentId)) {
                                        $i=1;
                                        foreach ($barrReleaseByStudentId as $record) {
                                    ?>
                                 <tr>
                                    <td><?php echo $i ?></td>
                                    <td><?php echo $record->type ?></td>
                                    <td><?php echo $record->barring_code . " - " . $record->barring_name ?></td>
                                    <td><?php echo $record->reason ?></td>
                                    <td><?php echo date('d-m-Y', strtotime($record->created_dt_tm)); ?></td>
                                    <td><?php echo $record->created_by ?></td>
                                 </tr>
                                 <?php
                                    $i++;
                                        }
                                    }
                                    ?>
                              </tbody>
                           </table>
                        </div>


                     </div>
                  
                  </div>




                  <div role="tabpanel" class="tab-pane" id="tab_3">
                     <div class="mt-4">

                      <div class="form-container">
                           <h4 class="form-group-title">Student Status</h4>
                           <div class="custom-table">
                              <table class="table" id="list-table">
                                 <thead>
                                    <tr>
                                       <th>Sl. No</th>
                                       <th>Date</th>
                                       <th>Reason</th>
                                       <th>By</th>
                                       <th>Status</th>
                                    </tr>
                                 </thead>
                                 <tbody>

                                    <tr>
                                       <td><?php echo '1'; ?></td>
                                       <td><?php 
                                       if($studentStatus->id_applicant > 0)
                                       {
                                          echo date('d-m-Y H:i:s',strtotime($studentStatus->created_dt_tm));
                                       }
                                       else
                                       {
                                          echo date('d-m-Y H:i:s',strtotime($studentStatus->created_dt_tm));
                                       }
                                         ?>     
                                       </td>
                                       <td> </td>
                                       <td><?php
                                       if($studentStatus->added_by_partner_university > 0)
                                       {
                                        echo "Partner University";
                                       }
                                       else
                                       {
                                          echo 'Administrator';
                                       } ?></td>
                                       <td>Active</td>
                                    </tr>
                                    <?php
                                    if(!empty($applyChangeStatusListByStudentId))
                                    {
                                      $j=1;
                                        foreach ($applyChangeStatusListByStudentId as $record)
                                        {
                                    ?>
                                   <tr>
                                      <td><?php echo $j+1 ?></td>
                                      <td><?php echo date('d-m-Y H:i:s',strtotime($record->created_dt_tm)); ?></td>
                                      <td><?php echo $record->change_status ?></td>
                                      <td><?php echo $record->created_by ?></td>
                                      <td>
                                      <?php 
                                         if($record->status == 0)
                                         {
                                             echo "Pending"; 
                                         }if($record->status == 1)
                                         {
                                             echo "Approved"; 
                                         }if($record->status == 2)
                                         {
                                             echo "Rejected"; 
                                         }?>
                                         
                                      </td>
                                   </tr>
                                   <?php
                                      $j++;
                                      }
                                    }
                                    ?>
                                 </tbody>
                              </table>
                           </div>
                        </div>



                     </div>

                  </div>


                  <div role="tabpanel" class="tab-pane active" id="tab_4">
                     <div class="mt-4">


                        




                      <div class="form-container">
                        <h4 class="form-group-title">Program Registration Details</h4>



                        <div class="custom-table" id="printReceipt">
                           <table class="table" id="list-table">
                              <thead>
                                 <tr>
                                    <th>Sl. No</th>
                                    <th>Programme Code</th>
                                    <th>Programme Name</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Registered On</th>
                                    <th>Payment Status</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <?php
                                    if (!empty($studentHasProgramme)) {
                                        $i=1;
                                        foreach ($studentHasProgramme as $record) {
                                    ?>
                                 <tr>
                                    <td><?php echo $i ?></td>
                                    <td><?php echo $record->programme_code; ?></td>
                                    <td><?php echo $record->programme_name; ?></td>
                                    <td><?php echo date('d-m-Y', strtotime($record->start_date)); ?></td>
                                    <td><?php echo date('d-m-Y', strtotime($record->end_date)); ?></td>
                                    <td><?php echo date('d-m-Y', strtotime($record->created_dt_tm)); ?></td>
                                    
                                    <td><?php
                                    if($record->status == '0')
                                    {
                                      echo 'Not Paid';
                                    }
                                    else
                                    {
                                      echo 'Paid';
                                    }
                                    ?></td>
                                   
                                 </tr>
                                 <?php
                                    $i++;
                                        }
                                    }
                                    ?>
                              </tbody>
                           </table>
                        </div>


                          

                      </div>




                     </div>
                  
                  </div>





                  <div role="tabpanel" class="tab-pane" id="tab_5">
                     <div class="mt-4">
                        <div class="custom-table" id="printInvoice">
                           <table class="table" id="list-table">
                              <thead>
                                 <tr>
                                    <th>Sl. No</th>
                                    <th>Advisor Name</th>
                                    <th>Date</th>
                                    <th>Updated By</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <?php
                                    if (!empty($advisorTaggingDetails)) {
                                        $i=1;
                                        foreach ($advisorTaggingDetails as $record) {
                                    ?>
                                 <tr>
                                    <td><?php echo $i ?></td>
                                    <td><?php echo $record->ic_no . " - " . $record->advisor_name  ?></td>
                                    <td><?php echo date('d-m-Y', strtotime($record->created_dt_tm)) ?></td>
                                    <td><?php echo $record->created_by ?></td>
                                 </tr>
                                 <?php
                                    $i++;
                                        }
                                    }
                                    ?>
                              </tbody>
                           </table>
                        </div>
                     </div>
                  
                  </div>

        
              </div>

            </div>

        </div>


      <footer class="footer-wrapper">
        <p>&copy; 2019 All rights, reserved</p>
      </footer>

  </div> 
</div>
<script type="text/javascript">  
   
    $(document).ready(function(){
       $("#form_detail").validate(
       {
           rules:
           {
               note:
               {
                   required: true
               }         
           },
           messages:
           {
               note:
               {
                   required: "<p class='error-text'>Note Required</p>",
               }
           },
           errorElement: "span",
           errorPlacement: function(error, element) {
               error.appendTo(element.parent());
           }
   
       });
   });
   
   
</script>