<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List Student Records</h3>
      <!-- <a href="add" class="btn btn-primary">+ Add Main Invoice</a> -->
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">
                

                <div class="row">

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Student Name</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="name" value="<?php echo $searchParam['name']; ?>">
                    </div>
                  </div>
                </div>



                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Student Email</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="email_id" id="email_id" value="<?php echo $searchParam['email_id']; ?>">
                    </div>
                  </div>
                </div>


                

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Course </label>
                    <div class="col-sm-8">
                      <select name="id_programme" id="id_programme" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($programmeList)) {
                          foreach ($programmeList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['id_programme']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->code ."-".$record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>                      

              </div> 



              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <button type="reset" class="btn btn-link" onclick="clearSearchForm()">Clear All Fields</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
            <th>Sl. No</th>
            <th>Student</th>
            <th>NRIC</th>
            <th>Email</th>
            <th>Current Course Status</th>
            <th style="text-align: center;">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($mainInvoiceList)) {
            $i=1;
                                $this->load->model('student_record_model');


            foreach ($mainInvoiceList as $record) {
              $pastProgramme = array();
                $pastProgramme = $this->student_record_model->getProgram($record->id);

              
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->full_name ?></td>
                <td><?php echo $record->nric ?></td>
                <td><?php echo $record->email_id ?></td>

                <?php if(count($pastProgramme)>0) { ?> 
                  <td>Active</td>

                <?php } else {  ?>
                  <td>Not Active</td>
                <?php } ?> 
               
                <td class="text-center">
                  <a href="<?php echo 'view/' . $record->id; ?>" title="View">View Transactions</a>
                </td>
                <!-- <td class="">
                  <a href="<?php echo '/adminstudentLogin/' . $record->id; ?>" title="Edit" target="_blank">Login</a>
                </td> -->
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script type="text/javascript">
    
  $('select').select2();

  function clearSearchForm()
      {
        window.location.reload();
      }
</script>