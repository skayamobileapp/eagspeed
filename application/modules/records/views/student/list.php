<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List Student Records</h3>
      <!-- <a href="add" class="btn btn-primary">+ Add Main Invoice</a> -->
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">
                

                <div class="row">

               

                 <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Course Name </label>
                    <div class="col-sm-8">
                      <select name="id_programme" id="id_programme" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($programmeList)) {
                          foreach ($programmeList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['id_programme']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>                      

                 <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Student Name</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="name" value="<?php echo $searchParam['name']; ?>">
                    </div>
                  </div>
                </div>

              </div>

              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Course Provider</label>
                    <div class="col-sm-8">
                      <select name="partner_university_id" id="partner_university_id" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($organisationList)) {
                          foreach ($organisationList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['partner_university_id']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Course Type</label>
                    <div class="col-sm-8">
                      <select name="programme_type" id="programme_type" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($programmeTypeList)) {
                          foreach ($programmeTypeList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['programme_type']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>

                

       
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Student Email</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="email_id" id="email_id" value="<?php echo $searchParam['email_id']; ?>">
                    </div>
                  </div>
                </div>


 </div>

              
              <div class="row">
                  <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Transaction Start Date</label>
                    <div class="col-sm-8">
                      <input type="date" class="form-control" name="start_date" id="start_date" value="<?php echo $searchParam['start_date']; ?>">
                    </div>
                  </div>
                </div>

                  <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Transaction End Date</label>
                    <div class="col-sm-8">
                      <input type="date" class="form-control" name="end_date" id="end_date" value="<?php echo $searchParam['end_date']; ?>">
                    </div>
                  </div>
                </div>

            

               <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Payment Status</label>
                    <div class="col-sm-8">
                      <select name="payment_status" id="payment_status" class="form-control">
                        <option value="">Select</option>
                        <option value="Pending">Pending</option>
                        <option value="Paid">Paid</option>
                        <option value="Free Course">Free Course</option>
                      </select>
                    </div>
                  </div>
                </div>

  </div>

              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href="/records/student/list" type="reset" class="btn btn-link" onclick="clearSearchForm()">Clear All Fields</a>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table" border="1">
        <thead>
                      <th>Sl No</th>

            <th>Transaction ID / Date & Time</th>
            <th>Name</th>
            <th>Email</th>
            <th>Course Provider</th>
            <th>Course Name</th>
            
            <th>Purchased Date</th>
            
            <th>Course Amount</th>
            <th>Transaction Discount</th>
            <th>Transaction Amount</th>
            <th>Payment Status</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($mainInvoiceList)) {
            $j=1;
          
                    $this->load->model('student_record_model');

            foreach ($mainInvoiceList as $record) {

                           $invoiceAmount = $record->total_amount;
              $invoiceDetails = array();
                $invoiceDetails = $this->student_record_model->getMainInvoiceDetailsById($record->invoiceid);

  $i=1;

                  if($searchParam['payment_status']=='Pending') {


                    if($record->total_amount>0 && $record->paid_amount==0) {

                    } else {
                      continue;
                    }


                  }


                   else if($searchParam['payment_status']=='Paid') {


                    if($record->total_amount>0 && $record->paid_amount>0) {

                    } else {
                      continue;
                    }


                  }
                  else  if($searchParam['payment_status']=='Free Course') {


                    if($record->total_amount>0 && $record->paid_amount>0) {
                       continue;
                    } else if($record->total_amount>0 && $record->paid_amount==0) {
                                continue;
                    } else {
                     
                    }


                  }
                   else {

                   }

                  for($k=0;$k<count($invoiceDetails);$k++) { 



          ?>


              <tr>
                 <?php if($i==1) {?>
                                <td rowspan="<?php echo count($invoiceDetails);?>" style="vertical-align: top;"><?php echo $j++; ?></td>

                <td rowspan="<?php echo count($invoiceDetails);?>" style="vertical-align:top;">

                  <a href="/profile/download/generateMainInvoice/<?php echo $record->invoiceid;?>"><?php echo $record->invoice_number ?></a> / <br/> <?php echo date('d-m-Y H:i',strtotime($record->date_time)) ?></td>
                <td rowspan="<?php echo count($invoiceDetails);?>" style="vertical-align:top;"><?php echo $record->full_name ?></td>
                <td rowspan="<?php echo count($invoiceDetails);?>" style="vertical-align:top;"><?php echo $record->email_id ?></td>
              <?php } ?> 
                <td style="width:30%;"><?php echo $invoiceDetails[$k]->universityname;?></td>
                <td style="width:30%;"><?php echo $invoiceDetails[$k]->name;?></td>
                <td style="width:30%;"><?php echo date('d-m-Y',strtotime($record->date_time));?></td>
                <td style="width:30%;"><?php echo $invoiceDetails[$k]->amount;?></td>
                        

                     
                                 <?php if($i==1) {?>
                <td rowspan="<?php echo count($invoiceDetails);?>" style="vertical-align:top;"><?php echo $record->total_discount ?></td>
                <td rowspan="<?php echo count($invoiceDetails);?>" style="vertical-align:top;"><?php echo $invoiceAmount;?></td>
                <td rowspan="<?php echo count($invoiceDetails);?>" style="vertical-align:top;"><?php if($record->total_amount>0 && $record->paid_amount>0) {
                       echo "Paid";
                 } else if($record->total_amount>0 && $record->paid_amount==0) {
                       echo "Payment Pending";
                 } else { 
                        echo "Free Course";
                  }?>
               </td>
             <?php } ?> 
               
              </tr>
          <?php
          $i++;
            }
          }
        }
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script type="text/javascript">
    
  $('select').select2();

  function clearSearchForm()
      {
        window.location.reload();
      }
</script>