<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>Approval List Change Branch</h3>
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">
                
                <div class="row">

                  <div class="col-sm-6">

                    <div class="form-group">
                      <label class="col-sm-4 control-label">Reason</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $searchParameters['name']; ?>">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-4 control-label">Student</label>
                      <div class="col-sm-8">
                        <select name="id_student" id="id_student" class="form-control">
                          <option value="">Select</option>
                          <?php
                          if (!empty($studentList)) {
                            foreach ($studentList as $record)
                            {
                              $selected = '';
                              if ($record->id == $searchParameters['id_student'])
                              {
                                $selected = 'selected';
                              }
                          ?>
                              <option value="<?php echo $record->id;  ?>"
                                <?php echo $selected;  ?>>
                                <?php echo $record->nric . " - " . $record->full_name;  ?>
                                  
                                </option>
                          <?php
                            }
                          }
                          ?>
                        </select>
                      </div>
                    </div>


                  </div>

                  <div class="col-sm-6">

                    <div class="form-group">
                      <label class="col-sm-4 control-label">Programme</label>
                      <div class="col-sm-8">
                        <select name="id_programme" id="id_programme" class="form-control">
                          <option value="">Select</option>
                          <?php
                          if (!empty($programmeList)) {
                            foreach ($programmeList as $record) {
                              $selected = '';
                              if ($record->id == $searchParameters['id_programme']) {
                                $selected = 'selected';
                              }
                          ?>
                              <option value="<?php echo $record->id;  ?>" <?php echo $selected;  ?>>
                                <?php echo $record->code . " - " . $record->name;  ?></option>
                          <?php
                            }
                          }
                          ?>
                        </select>
                      </div>
                    </div>

                  </div>

                  <div class="col-sm-6">

                    <div class="form-group">
                      <label class="col-sm-4 control-label">Previous Branch</label>
                      <div class="col-sm-8">
                        <select name="id_branch" id="id_branch" class="form-control">
                          <option value="">Select</option>
                          <?php
                          if (!empty($branchList)) {
                            foreach ($branchList as $record) {
                              $selected = '';
                              if ($record->id == $searchParameters['id_branch']) {
                                $selected = 'selected';
                              }
                          ?>
                              <option value="<?php echo $record->id;  ?>" <?php echo $selected;  ?>>
                                <?php echo $record->code . " - " . $record->name;  ?></option>
                          <?php
                            }
                          }
                          ?>
                        </select>
                      </div>
                    </div>

                  </div>



                  <div class="col-sm-6">

                    <div class="form-group">
                      <label class="col-sm-4 control-label">New Branch</label>
                      <div class="col-sm-8">
                        <select name="id_new_branch" id="id_new_branch" class="form-control">
                          <option value="">Select</option>
                          <?php
                          if (!empty($branchList)) {
                            foreach ($branchList as $record) {
                              $selected = '';
                              if ($record->id == $searchParameters['id_new_branch']) {
                                $selected = 'selected';
                              }
                          ?>
                              <option value="<?php echo $record->id;  ?>" <?php echo $selected;  ?>>
                                <?php echo $record->code . " - " . $record->name;  ?></option>
                          <?php
                            }
                          }
                          ?>
                        </select>
                      </div>
                    </div>

                  </div>


                </div>
                
              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary" name="button" value="search">Search</button>
                <a href='approval_list' class="btn btn-link">Clear All Fields</a>
              </div>
            </div>
          </div>
        
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>

          <tr>
            <th>Sl. No.</th>
            <th>Student</th>
            <th>NRIC</th>
            <th>E-Mail</th>
            <th>Previous Branch</th>
            <th>Changing Branch</th>
            <th>Program</th>
            <th>Intake</th>
            <th>Reason</th>
            <th>Status</th>
            <th style="text-align: center;">Action</th>
            <th style="text-align: center;"><input type="checkbox" id="checkAll" name="checkAll"> Check All</th>
          </tr>
        </thead>
        <tbody>
          <?php
          $i = 1;
            foreach ($changeBranchApprovalList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->student ?></td>
                <td><?php echo $record->nric ?></td>
                <td><?php echo $record->email_id ?></td>
                <td><?php echo $record->branch_code . " - " . $record->branch_name ?></td>
                <td><?php echo $record->new_branch_code . " - " . $record->new_branch_name ?></td>
                <td><?php echo $record->programme_code . " - " . $record->programme ?></td>
                <td><?php echo $record->intake_year . " - " . $record->intake_name ?></td>
                <td><?php echo $record->reason ?></td>
                <td><?php if( $record->status == '1')
                {
                  echo "Approved";
                }
                else if( $record->status == '0')
                {
                  echo "Pending";
                } 
                ?></td>
                <td class="text-center">
                 

                  
                    <a href="<?php echo 'edit/' . $record->id; ?>" title="View">View</a>
                    
                  
                </td>

                <td class="text-center">
                  <!-- <a href="<?php echo 'edit/' . $record->id; ?>" title="Edit">Edit</a> -->
                <input type='checkbox' name='approval[]' value="<?php echo $record->id;?>" />
           
                </td>
              </tr>
          <?php
          $i++;
            }
             
          ?>
               <tr>
                <td><button type="submit" name="button" id="button" class="btn btn-primary" value="Approve">Approve</button></td>
              </tr>
        </tbody>
        <div align="right">
          
        </div>
      </table>
    </form>


    </div>
     
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script>
  $(function () {
      $("#checkAll").click(function () {
          if ($("#checkAll").is(':checked')) {
              $(".check").prop("checked", true);
          } else {
              $(".check").prop("checked", false);
          }
      });
  });

  $('select').select2();
  
  function clearSearchForm()
  {
    window.location.reload();
  }
</script>