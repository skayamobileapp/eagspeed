<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Apply Change Program</h3>
        </div>
        <form id="form_apply_change_programme" action="" method="post">


        <div class="form-container">
            <h4 class="form-group-title">Select Student For Apply Change Program</h4> 
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Current Program <span class='error-text'>*</span></label>
                        <select name="id_programme" id="id_programme" class="form-control" onchange="getStudentByProgramme(this.value)">
                            <option value="">Select</option>
                            <?php
                            if (!empty($programmeList))
                            {
                                foreach ($programmeList as $record)
                                {?>
                            <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                            </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div> 


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Student <span class='error-text'>*</span></label>
                        <span id="student">
                          <select class="form-control" id='id_student' name='id_student'>
                            <option value=''></option>
                          </select>
                        </span>
                    </div>
                </div>

            </div>                

            <div class="row" id="view_programme_details"  style="display: none;">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Program Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="programme_name" name="programme_name" readonly="readonly">
                    </div>
                </div>  

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Program Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="programme_code" name="programme_code" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Total Credit Hrs. <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="total_cr_hrs" name="total_cr_hrs" readonly="readonly">
                    </div>
                </div>

            </div>  


            <!-- <div id="view_programme_details"  style="display: none;">
                <table border="1px" style="width: 100%">
                    <tr>
                        <td colspan="4"><h5 style="text-align: center;">Programme Details</h5></td>
                    </tr>
                    <tr>
                        <th style="text-align: center;">Programme Name</th>
                        <td style="text-align: center;" id="programme_name"></td>
                        <th style="text-align: center;">Programme Code</th>
                        <td style="text-align: center;" id="programme_code"></td>
                    </tr>
                    <tr>
                        <th style="text-align: center;">Total Credit Hours</th>
                        <td style="text-align: center;" id="total_cr_hrs"></td>
                        <th style="text-align: center;">Foundation</th>
                        <td style="text-align: center;" id="foundation"></td>
                    </tr>

                </table>
                <br>
                <br>
            </div> -->

            <div id="view_student_details"  style="display: none;">

                <br>
            </div>

            <div class="row">


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Select New Programme <span class='error-text'>*</span></label>
                            <span id="view_new_programme">
                              <select class="form-control" id='id_new_programme' name='id_new_programme'>
                                <option value=''></option>
                              </select>
                            </span>
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Select New Intake <span class='error-text'>*</span></label>
                            <span id="view_new_intake">
                              <select class="form-control" id='id_new_intake' name='id_new_intake'>
                                <option value=''></option>
                              </select>
                            </span>
                        </div>
                    </div>
  


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Select New Learning Mode <span class='error-text'>*</span></label>
                            <span id="view_new_program_scheme">
                              <select class="form-control" id='id_new_program_scheme' name='id_new_program_scheme'>
                                <option value=''></option>
                              </select>
                            </span>
                        </div>
                    </div>


                 

            </div>


            <div class="row"> 

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select New Semester <span class='error-text'>*</span></label>
                        <select name="id_new_semester" id="id_new_semester" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($semesterList))
                            {
                                foreach ($semesterList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


                <!-- <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select New Programme Scheme <span class='error-text'>*</span></label>
                        <span id="view_new_program_has_scheme">
                          <select class="form-control" id='id_new_program_has_scheme' name='id_new_program_has_scheme'>
                            <option value=''></option>
                          </select>
                        </span>
                    </div>
                </div> -->


                


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Fees <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="fee" name="fee" readonly="readonly" value="0">
                    </div>
                 </div>


                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Reason <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="reason" name="reason">
                    </div>
                 </div> 



            </div>

            
            <div class="row"> 



                  


            </div>
            </div>

            <div class="form-container" style="display: none;">
                <h4 class="form-group-title">Fee Structure Details</h4> 
                <div id='view_fee'>
                </div>
            </div>


            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="list" class="btn btn-link">Cancel</a>
                </div>
            </div>
        
        </form>


        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>
</div>

<script>
    $('select').select2();

    function getStudentByProgramme(id)
    {

     $.get("/records/applyChangeProgramme/getStudentByProgrammeId/"+id, function(data, status)
     {
   
        $("#student").html(data);
        // $("#view_programme_details").show();
        // // $("#view_programme_details").html(data);
        // var programme_name = $("#programme_name").val();
        //  // alert(programme_name);
        // $("#programme_name").val(programme_name);

        //  var programme_code = $("#programme_code").val();
        //  alert(programme_code);
        // $("#programme_code").val(programme_code);

        //  var total_cr_hrs = $("#total_cr_hrs").val();
        // $("#total_cr_hrs").val(total_cr_hrs);

        //  var graduate_studies = $("#graduate_studies").val();
        // $("#graduate_studies").val(graduate_studies);

    });
 }

 function getStudentByStudentId(id)
 {

     $.get("/records/applyChangeProgramme/getStudentByStudentId/"+id, function(data, status){
   
        $("#view_student_details").html(data);
        $("#view_student_details").show();
    });

    $.get("/records/applyChangeProgramme/getProgrammeByEducationLevelIdByStudentId/"+id, function(data, status)
    {
        $("#view_new_programme").html(data);
    });
 }

 function getIntakeByProgramme(id)
 {

    $.get("/records/applyChangeProgramme/getIntakeByProgramme/"+id, function(data, status){
   
        $("#view_new_intake").html(data);
        $("#view_new_intake").show();
    });

    // Programme Scheme Get
    $.get("/records/applyChangeProgramme/getSchemeByProgramId/"+id, function(data, status)
    {
      $("#view_new_program_has_scheme").html(data);
      $("#view_new_program_has_scheme").show();
    });


    // Programme Learning Mode
    $.get("/records/applyChangeProgramme/getProgramSchemeByProgramId/"+id, function(data, status){
   
        $("#view_new_program_scheme").html(data);
        $("#view_new_program_scheme").show();
    });
 }


    function checkFeeStructure()
     {
        var tempPR = {};
        tempPR['id_program'] = $("#id_new_programme").val();
        tempPR['id_intake'] = $("#id_new_intake").val();
        tempPR['id_program_scheme'] = $("#id_new_program_scheme").val();
        // tempPR['id_program_has_scheme'] = $("#id_new_program_has_scheme").val();
        // alert(tempPR['id_program']);

        if(tempPR['id_new_programme'] != '' && tempPR['id_new_intake'] != ''  && tempPR['id_new_program_scheme'] != '')
        {

            $.ajax(
            {
               url: '/records/applyChangeProgramme/checkFeeStructure',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    if(result == '0')
                    {

                        alert('No Programme Landscape Is Defined For The Selected Particulars, Select Another Combination');
                        $(this).data('options', $('#id_intake option').clone());

                        var idstateselected = 0;

                        // $("#id_program").find('option[value="'+idstateselected+'"]').attr('selected',true);
                        // $("#id_intake").find('option[value="'+idstateselected+'"]').attr('selected',true);
                        // $("#id_program_scheme").find('option[value="'+idstateselected+'"]').attr('selected',true);
                        // $("#id_program_has_scheme").find('option[value="'+idstateselected+'"]').attr('selected',true);
                        // $('select').select2();                        
                    }
               }
            });
        }
    }




 function getFeeByProgrammeNIntake()
 {

    var id_programme = $("#id_programme").val();
    var currency = $("#currency").val();
    var id_new_programme = $("#id_new_programme").val();
    var id_intake = $("#id_intake").val();
    var id_new_intake = $("#id_new_intake").val();
    var id_new_program_scheme = $("#id_new_program_scheme").val();
    // alert(id_new_programme);

    // if(id_programme == id_new_programme && id_intake == id_new_intake)
    // {
    //     alert('Same Programme and Intake  Selection For Change Not Allowed Select Anothe Combination');

    //     // $("#id_new_intake").val('');

    // }
    // else
    // {


        var tempPR = {};

        tempPR['id_programme'] = $("#id_new_programme").val();
        tempPR['id_intake'] = $("#id_new_intake").val();
        tempPR['id_program_scheme'] = $("#id_new_program_scheme").val();
        tempPR['currency'] = $("#currency").val();
        
        // alert($("#id_new_programme").val());
            $.ajax(
            {
               url: '/records/applyChangeProgramme/getFeeByProgrammeNIntake',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_fee").html(result);
                var ta = $("#amount").val();
                if(ta == '0')
                {
                    alert('No Fee Structure Defined For Entered Data, Select Anothe Combination');
                    $("#fee").val('');

                }
                else
                {
                    $("#fee").val(ta);
                }
               }
            });

        // }
        
    }




    $(document).ready(function()
    {
        $("#form_apply_change_programme").validate(
        {
            rules:
            {
                id_programme:
                {
                    required: true
                },
                id_student:
                {
                    required: true
                },
                id_intake:
                {
                    required: true
                },
                id_new_programme:
                {
                    required: true
                },
                id_new_intake:
                {
                    required: true
                },
                id_new_semester:
                {
                    required: true
                },
                reason:
                {
                    required: true
                },
                id_new_program_scheme:
                {
                    required: true
                },
                id_new_program_has_scheme:
                {
                    required: true
                }
                // ,
                // fee:
                // {
                //     required: true
                // }
            },
            messages:
            {
                id_programme:
                {
                    required: "<p class='error-text'>Select Current Program</p>",
                },
                id_student:
                {
                    required: "<p class='error-text'>Select Student</p>",
                },
                id_intake:
                {
                    required: "<p class='error-text'>Select Student For Intake</p>",
                },
                id_new_programme:
                {
                    required: "<p class='error-text'>Select Changing Program</p>",
                },
                id_new_intake:
                {
                    required: "<p class='error-text'>Select Changing Intake</p>",
                },
                id_new_semester:
                {
                    required: "<p class='error-text'>Select Changing Semester</p>",
                },
                reason:
                {
                    required: "<p class='error-text'>Enter Reason</p>",
                },
                id_new_program_scheme:
                {
                    required: "<p class='error-text'>Select New Learning Mode</p>",
                },
                id_new_program_has_scheme:
                {
                    required: "<p class='error-text'>Select New Program Scheme</p>",
                }
                // ,
                // fee:
                // {
                //     required: "<p class='error-text'>Select Changing Intake & Program For Fee Structure</p>",
                // }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>