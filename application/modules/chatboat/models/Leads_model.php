<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Leads_model extends CI_Model
{
    function statusListByType($type)
    {
        $this->db->select('sp.*');
        $this->db->from('status_table as sp');
        $this->db->where('type', $type);
        $this->db->order_by("sp.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         //echo "<Pre>"; print_r($result);exit;
         return $result;
    }

    function leadsListSearch($data)
    {
        $this->db->select('sp.*, st.name as status_name, com.name as commented_by_name');
        $this->db->from('leads as sp');
        $this->db->join('status_table as st', 'sp.lead_status = st.id','left');
        $this->db->join('users as com', 'sp.commented_by = com.id','left');
        if ($data['name'] != '')
        {
            $likeCriteria = "(sp.Name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['email'] != '')
        {
            $likeCriteria = "(sp.Email  LIKE '%" . $data['email'] . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("sp.name", "ASC");
         $query = $this->db->get();
         $results = $query->result();  
         // echo "<Pre>"; print_r($recepients);exit;
         return $results;
    }

    function getLead($id)
    {
        $this->db->select('sp.*, st.name as status_name');
        $this->db->from('leads as sp');
        $this->db->join('status_table as st', 'sp.lead_status = st.id','left');
        $this->db->where('sp.LeadId', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function getMessagesByLeadId($id)
    {
        $this->db->select('m.*, p.code as programme_code, p.name as programme_name');
        $this->db->from('messages as m');
        $this->db->join('leads as l', 'm.leadid = l.LeadId');
        $this->db->join('programme as p', 'm.content = p.id','left');
        $this->db->where('m.leadid', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function addNewLeads($data)
    {
        $this->db->trans_start();
        $this->db->insert('leads', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editLeads($data, $id)
    {
        $this->db->where('LeadId', $id);
        $this->db->update('leads', $data);
        return TRUE;
    }

     function deleteLeads($id)
    { 
        // echo "<Pre>";  print_r($id);exit;
       $this->db->where('id', $id);
       $this->db->delete('leads_recepients');
    }
}