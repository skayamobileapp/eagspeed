<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List Leads</h3>
      <!-- <a href="add" class="btn btn-primary">+ Add Event</a> -->
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">


                <div class="row">

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Lead</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="name" id="name" value="<?php echo $searchParam['name']; ?>">
                      </div>
                    </div>
                  </div>


                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Email</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="email" id="email" value="<?php echo $searchParam['email']; ?>">
                      </div>
                    </div>
                  </div>


                  <!-- <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Type </label>
                      <div class="col-sm-8">
                            <select id="type" name="type" class="form-control">
                                <option value="">SELECT</option>
                                <option value="Applicant" <?php if($searchParam['type'] =='Applicant'){ echo "selected"; } ?>>Applicant</option>
                                <option value="Student" <?php if($searchParam['type'] =='Student'){ echo "selected"; } ?>>Student</option>
                                <option value="Staff" <?php if($searchParam['type'] =='Staff'){ echo "selected"; } ?>>Staff</option>
                            </select>
                        </div>
                      </div>
                    </div> -->



                </div>


              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href="list" class="btn btn-link" >Clear All Fields</a>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Lead Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>IP</th>
            <th>Start Time</th>
            <th>Next Action</th>
            <th>Last Commented By</th>
            <th>Last Commented On</th>
            <th>Status</th>
            <th class="text-center">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($leadsList)) {
            $i=1;
            foreach ($leadsList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->Name ?></td>
                <td><?php echo $record->Email ?></td>
                <td><?php echo $record->Phone ?></td>
                <td><?php echo $record->IP ?></td>
                <td><?php echo date('d-m-Y H:i:s', strtotime($record->StartTime)); ?></td>
                <td><?php echo $record->next_action ?></td>
                <td><?php echo $record->commented_by_name; ?></td>
                <td><?php if($record->commented_on)
                {
                  echo date('d-m-Y H:i:s', strtotime($record->commented_on));
                }
                ?></td>
                <td><?php if( $record->lead_status == NULL)
                {
                  echo "In-Active";
                }
                else
                {
                  echo $record->status_name;
                } 
                ?></td>
                <td class="text-center">
                  <a href="<?php echo 'edit/' . $record->LeadId; ?>" title="Edit">Edit</a>
                  <!--  -->
                </td>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script>
    $('select').select2();
    
      function clearSearchForm()
      {
        window.location.reload();
      }
</script>