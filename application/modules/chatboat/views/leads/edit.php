<style>
  /* Chat CSS Starts Here */
.chat-container {
  border: 1px solid #ccc;
  margin-top: 30px;
  margin-bottom: 30px;
  padding: 1rem;
  border-radius: 10px;
}
.chat {
  list-style: none;
  background: none;
  padding: 0;
  margin: 0;
}
.chat li {
  padding: 0.5rem;
  font-size: 1rem;
  overflow: hidden;
  display: flex;
  color: #000000;
}
.chatbot .msg {
  background-color: #f8f8f8;
  color: #333;
}
.chatbot .msg .time {
  background-color: transparent;
  color: #666;
}
/* M E S S A G E S */
#messageArea {
  overflow-y: scroll;
}

.chat {
  list-style: none;
  background: none;
  padding: 0;
  margin: 0;
}

.chat li {
  padding: 8px;
  padding: 0.5rem;
  font-size: 1rem;
  overflow: hidden;
  display: -webkit-box;
  display: -webkit-flex;
  display: -ms-flexbox;
  display: flex;
  color: #000000;
}

.visitor {
  -webkit-box-pack: end;
  -webkit-justify-content: flex-end;
  -ms-flex-pack: end;
  justify-content: flex-end;
  -webkit-box-align: end;
  -webkit-align-items: flex-end;
  -ms-flex-align: end;
  -ms-grid-row-align: flex-end;
  align-items: flex-end;
  margin-top: 10px;
  margin-bottom: 10px;
}

.visitor .msg {
  -webkit-box-ordinal-group: 2;
  -webkit-order: 1;
  -ms-flex-order: 1;
  order: 1;
  border-radius: 20px;
}

.chatbot .msg {
  -webkit-box-ordinal-group: 2;
  -webkit-order: 1;
  -ms-flex-order: 1;
  order: 1;
}

.msg {
  word-wrap: break-word;
  min-width: 50px;
  max-width: 80%;
  padding: 10px 15px;
  border-radius: 20px;
  background: #bd302a;
  color: #fff;
}

.msg p {
  margin: 0 0 0.2rem 0;
}

.msg .time {
  font-size: 0.7rem;
  color: #fff;
  margin-top: 3px;
  float: right;
  cursor: default;
  -webkit-touch-callout: none;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
}

/* I N P U T */

.textarea {
  position: fixed;
  bottom: 0px;
  left: 0;
  right: 0;
  width: 95%;
  height: 55px;
  z-index: 99;
  background-color: #fff;
  border: none;
  outline: none;
  padding-left: 15px;
  padding-right: 15px;
  color: #000000;
  font-weight: 300;
  font-size: 1rem;
  line-height: 1.5;
  background: rgba(250, 250, 250, 0.8);
}

.textarea:focus {
  background: white;
  box-shadow: 0 -6px 12px 0px rgba(235, 235, 235, 0.95);
  transition: 0.4s;
}

a.banner {
  position: fixed;
  bottom: 5px;
  right: 10px;
  height: 12px;
  z-index: 99;
  outline: none;
  color: #777;
  font-size: 10px;
  text-align: right;
  font-weight: 200;
  text-decoration: none;
}

/* Loading Dot Animation */
div.loading-dots {
  position: relative;
}

div.loading-dots .dot {
  display: inline-block;
  width: 8px;
  height: 8px;
  margin-right: 2px;
  border-radius: 50%;
  background: #196eb4;
  animation: blink 1.4s ease-out infinite;
  animation-fill-mode: both;
}

div.loading-dots .dot:nth-child(2) {
  animation-delay: -1.1s;
}

div.loading-dots .dot:nth-child(3) {
  animation-delay: -0.9s;
}

div.loading-dots .dot-grey {
  background: rgb(120, 120, 120);
}

div.loading-dots .dot-sm {
  width: 6px;
  height: 6px;
  margin-right: 2px;
}

div.loading-dots .dot-md {
  width: 12px;
  height: 12px;
  margin-right: 2px;
}

div.loading-dots .dot-lg {
  width: 16px;
  height: 16px;
  margin-right: 3px;
}

@keyframes blink {
  0%,
  100% {
    opacity: 0.2;
  }

  20% {
    opacity: 1;
  }
}

.btn {
  display: block;
  padding: 5px;
  border-radius: 5px;
  margin: 5px;
  min-width: 100px;
  background-color: #17a2b8;
  cursor: pointer;
  color: white;
  text-align: center;
}

@media (max-width: 767px) {
  .textarea {
    bottom: 15px;
  }
}
/* Chat CSS Ends Here */
</style>

<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Approve Lead Messages</h3>
        </div>




        <div class="form-container">
          <h4 class="form-group-title">Lead Details</h4>

              <div class='data-list'>
                  <div class='row'>
  
                      <div class='col-sm-6'>
                          <dl>
                              <dt>Lead Name :</dt>
                              <dd><?php echo ucwords($lead->Name); ?></dd>
                          </dl>
                          <dl>
                              <dt>IP :</dt>
                              <dd><?php echo $lead->IP ?></dd>
                          </dl>                          
                          <dl>
                              <dt>Start Date Time</dt>
                              <dd><?php echo date('d-m-Y H:i:s', strtotime($lead->StartTime)); ?></dd>
                          </dl>
                      </div>        
                      
                      <div class='col-sm-6'>
                          <dl>
                              <dt>Lead Email :</dt>
                              <dd><?php echo $lead->Email ?></dd>
                          </dl>         
                          <dl>
                              <dt>Phone :</dt>
                              <dd><?php echo $lead->Phone; ?></dd>
                          </dl>
                          <dl>
                              <dt>Status </dt>
                              <dd><?php
                              if($lead->lead_status != NULL)
                              {
                               echo $lead->status_name;
                              }
                              else
                              {
                               echo 'In-Active';
                              }
                               ?></dd>
                          </dl>
                          
                      </div>
  
                  </div>
              </div>


       </div>




        



        <?php

      if(!empty($messasesList))
      {
          ?>
          <br>


             <div class="row">
              <div class="col-md-6 col-lg-4">
                <div class="chat-container">
                  <ol class="chat">

                      <?php
                       $total = 0;
                       // print_r($messasesList[0]);exit;
                        for($i=0;$i<count($messasesList);$i++)
                       { ?>

                         <?php if($messasesList[$i]->UserFrom=='0') {?>

                            <li class="chatbot">
                              <div class="msg">
                                <div>
                                  <p>
                                    <?php echo $messasesList[$i]->Content;?>
                                  </p>
                                </div>
                                <div class="time"><?php echo date('d-m-Y H:i:s', strtotime($messasesList[$i]->SentOn))?></div>
                              </div>
                          </li>


                         <?php } else { ?>

                            <li class="visitor">
                              <div class="msg">
                                <div><p><?php echo $messasesList[$i]->Content;?></p></div>
                                <div class="time"><?php echo date('d-m-Y H:i:s', strtotime($messasesList[$i]->SentOn))?></div>
                              </div>
                            </li>


                         <?php } ?> 

                       <?php }?>
                    
                 
                  
                  </ol>
                </div>
              </div>
            </div>


          <div class="form-container">


        <form id="form_main" action="" method="post">

            <div class="form-container">
                <h4 class="form-group-title">Lead Details</h4>        
                

                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Status <span class='error-text'>*</span></label>
                            <select name="lead_status" id="lead_status" class="form-control selitemIcon">
                                <option value="">Select</option>
                                <?php
                                if (!empty($statusList))
                                {
                                    foreach ($statusList as $record)
                                    {?>
                                <option value="<?php echo $record->id;  ?>"
                                    <?php
                                    if($record->id == $lead->lead_status)
                                    {
                                        echo 'selected';
                                    }
                                    ?>>
                                    <?php echo $record->name;?>
                                </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-events">
                            <label>Next Action <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="next_action" name="next_action" value="<?php echo $lead->next_action;?>">
                        </div>
                    </div>

                </div>


                <div class="row">

                    <div class="col-sm-12">
                          <div class="form-group">
                            <label>Comments <span class='error-text'>*</span></label>
                             <textarea type="text" class="form-control" id="comment" name="comment"><?php echo $lead->comment; ?></textarea>
                          </div>
                    </div>

                </div>


         
            <div class="button-block clearfix">
                <div class="bttn-events">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="../list" class="btn btn-link">Back</a>
                </div>
            </div>
               </div>

        </form>

      <?php
      
      }
       ?>




        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script src="<?php echo BASE_PATH; ?>assets/ckeditor/ckeditor.js"></script>


<script type="text/javascript">

    $('select').select2();

    CKEDITOR.replace('comment',{
    width: "100%",
    height: "300px"
    }); 

     $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                lead_status: {
                    required: true
                },
                next_action: {
                    required: true
                },
                comment: {
                    required: true
                }
            },
            messages: {
                lead_status: {
                    required: "<p class='error-text'>Select Lead Status</p>",
                },
                next_action: {
                    required: "<p class='error-text'>Next Action Required</p>",
                },
                comment: {
                    required: "<p class='error-text'>Comment Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
