<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Group extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('group_model');
        $this->isLoggedIn();
    }

    function list()
    {

        if ($this->checkAccess('email_communication_group.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            // $formData['name'] = '';
            // $formData['type'] = '';
            // $formData['id_template'] = '';


            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['type'] = $this->security->xss_clean($this->input->post('type'));
            $formData['id_template'] = $this->security->xss_clean($this->input->post('id_template'));

            $data['searchParam'] = $formData;

            $data['groupList'] = $this->group_model->groupListSearch($formData);
            $data['templateList'] = $this->group_model->templateList();
            //echo "<Pre>"; print_r($data);exit;
            $this->global['pageTitle'] = 'Campus Management System : Communication Group List';
            $this->loadViews("group/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('email_communication_group.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $type = $this->security->xss_clean($this->input->post('type'));
                $id_template = $this->security->xss_clean($this->input->post('id_template'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'name' => $name,
                    'type' => $type,
                    'id_template' => $id_template,
                    'status' => $status,
                    'created_by' => $user_id
                );
                $result = $this->group_model->addNewGroup($data);
                redirect('/communication/group/list');
            }
            $data['templateList'] = $this->group_model->templateListByStatus('1');
            $this->global['pageTitle'] = 'Campus Management System : Add Communication Group';
            $this->loadViews("group/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('email_communication_group.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/communication/group/list');
            }
            
            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                // $type = $this->security->xss_clean($this->input->post('type'));
                $id_template = $this->security->xss_clean($this->input->post('id_template'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'name' => $name,
                    // 'type' => $type,
                    'id_template' => $id_template,
                    'status' => $status,
                    'updated_by' => $user_id
                );                
                $result = $this->group_model->editGroup($data,$id);
                redirect('/communication/group/list');
            }

            $data['group'] = $this->group_model->getGroup($id);
            $data['templateList'] = $this->group_model->templateListByStatus('1');

            $this->global['pageTitle'] = 'Campus Management System : Edit Communication Group';
            $this->loadViews("group/edit", $this->global, $data, NULL);
        }
    }

    function recepientList()
    {

        if ($this->checkAccess('email_communication_group.recepients_list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            // $formData['name'] = '';
            // $formData['type'] = '';
            // $formData['id_template'] = '';
            

            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['type'] = $this->security->xss_clean($this->input->post('type'));
            $formData['id_template'] = $this->security->xss_clean($this->input->post('id_template'));

            $data['searchParam'] = $formData;

            $data['groupList'] = $this->group_model->groupListSearch($formData);
            $data['templateList'] = $this->group_model->templateList();
            //echo "<Pre>"; print_r($data);exit;
            $this->global['pageTitle'] = 'Campus Management System : Communication Recepients Group List';
            $this->loadViews("group/recepient_list", $this->global, $data, NULL);
        }
    }

    function view($id = NULL)
    {
        if ($this->checkAccess('email_communication_group.view') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/communication/group/list');
            }
            
            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $id_template = $this->security->xss_clean($this->input->post('id_template'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'name' => $name,
                    'id_template' => $id_template,
                    'status' => $status,
                    'updated_by' => $user_id
                );                
                $result = $this->group_model->editGroup($data,$id);
                redirect('/communication/group/list');
            }

            $data['group'] = $this->group_model->getGroup($id);
            $data['templateList'] = $this->group_model->templateListByStatus('1');

            $this->global['pageTitle'] = 'Campus Management System : View Communication Group';
            $this->loadViews("group/view", $this->global, $data, NULL);
        }
    }


    function addRecepient($id_group = NULL)
    {

        if ($this->checkAccess('email_communication_group_recepient.add_recepients') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            if ($id_group == null)
            {
                redirect('/communication/group/recepientList');
            }
            
            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {
                $formData = $this->input->post();
                $type = $this->security->xss_clean($this->input->post('type'));

                for($i=0;$i<count($formData['id_recepient']);$i++)
                 {
                    $id_recepient = $formData['id_recepient'][$i];
                // echo "<Pre>";print_r($id_recepient);exit();

                    if($id_recepient > 0)
                    {
                            $detailsData = array(
                                'id_group'=>$id_group,
                                'id_recepient'=>$id_recepient,
                                'type'=>$type,
                                'status'=>'1',
                                'created_by'=>$user_id
                            );
                    // echo "<Pre>"; print_r($detailsData);exit;
                        $inserted_detail_id = $this->group_model->addGroupHasRecepient($detailsData);
                    }
                }
                redirect('/communication/group/recepientList');
            }

            $data['group'] = $this->group_model->getGroup($id_group);
            $data['groupRecepientsList'] = $this->group_model->getGroupRecepientsListByMaster($id_group,$data['group']->type);
            $data['templateList'] = $this->group_model->templateListByStatus('1');
            $data['intakeList'] = $this->group_model->intakeListByStatus('1');
            $data['programmeList'] = $this->group_model->programListByStatus('1');
            // echo "<Pre>"; print_r($data['groupRecepientsList']);exit;

            $this->global['pageTitle'] = 'Campus Management System : Add Recepients';
            $this->loadViews("group/add_recepients", $this->global, $data, NULL);
        }
    }



     function getStudentListByType()
    {
        $data = $this->security->xss_clean($this->input->post('data'));
        $type = $data['type'];
        
        // echo "<Pre>";print_r($data);exit();
        switch ($type)
        {
            case 'Applicant':

                $table = $this->getApplicantList($data);

                break;

            case 'Student':

                $table = $this->getStudentList($data);
                
                break;


            default:
                # code...
                break;
        }
        echo $table;        
    }

    function getApplicantList($data)
    {
        // echo "<Pre>";print_r($data);exit();
        $applicant_data = $this->group_model->getApplicantListByData($data);
        
        // echo "<Pre>";print_r($applicant_data);exit();

        $table = "<table  class='table' id='list-table'>
                  <tr>
                    <th>Sl. No</th>
                    <th>Applicant Name</th>
                    <th>NRIC</th>
                    <th>E Mail</th>
                    <th>Programme</th>
                    <th>Intake</th>
                    <th style='text-align: center;'><input type='checkbox' id='checkAll' name='checkAll'> Check All</th>
                </tr>";
                $total_amount = 0;
                    for($i=0;$i<count($applicant_data);$i++)
                    {
                    $id = $applicant_data[$i]->id;
                    $full_name = $applicant_data[$i]->full_name;
                    $program_code = $applicant_data[$i]->program_code;
                    $program_name = $applicant_data[$i]->program_name;
                    $intake_name = $applicant_data[$i]->intake_name;
                    $intake_year = $applicant_data[$i]->intake_year;
                    $email = $applicant_data[$i]->email_id;
                    $nric = $applicant_data[$i]->nric;
                    $j = $i+1;
                        $table .= "
                        <tr>
                            <td>$j</td>
                            <td>$full_name</td>
                            <td>$nric</td>                           
                            <td>$email</td>                           
                            <td>$program_code - $program_name</td>                           
                            <td>$intake_year - $intake_name</td>                        
                            
                            <td class='text-center'>
                          <input type='checkbox' name='id_recepient[]' class='check' value='".$id."'>
                        </td>
                       
                        </tr>";
                    }


        $table.= "</table>";
        return $table;
    }

    function getStudentList($data)
    {
        // echo "<Pre>";print_r("d");exit();
        $student_data = $this->group_model->getStudentListByData($data);
        
        // echo "<Pre>";print_r($student_data);exit();

        $table = "<table  class='table' id='list-table'>
                  <tr>
                    <th>Sl. No</th>
                    <th>Student Name</th>
                    <th>NRIC</th>
                    <th>E Mail</th>
                    <th>Programme</th>
                    <th>Intake</th>
                    <th style='text-align: center;'><input type='checkbox' id='checkAll' name='checkAll'> Check All</th>
                </tr>";
                $total_amount = 0;
                    for($i=0;$i<count($student_data);$i++)
                    {
                    $id = $student_data[$i]->id;
                    $full_name = $student_data[$i]->full_name;
                    $program_code = $student_data[$i]->program_code;
                    $program_name = $student_data[$i]->program_name;
                    $intake_name = $student_data[$i]->intake_name;
                    $intake_year = $student_data[$i]->intake_year;
                    $email = $student_data[$i]->email_id;
                    $nric = $student_data[$i]->nric;
                    $j = $i+1;
                        $table .= "
                        <tr>
                            <td>$j</td>
                            <td>$full_name</td>
                            <td>$nric</td>                           
                            <td>$email</td>                           
                            <td>$program_code - $program_name</td>                           
                            <td>$intake_year - $intake_name</td>                        
                            
                            <td class='text-center'>
                          <input type='checkbox' name='id_recepient[]' class='check' value='".$id."'>
                        </td>
                       
                        </tr>";
                    }


        $table.= "</table>";
        return $table;
    }

    function getStaffList()
    {
        $data = $this->security->xss_clean($this->input->post('data'));
        // echo "<Pre>";print_r($data);exit();
        $type = $data['type'];
        if($type == 'Staff')
        {

            $staff_data = $this->group_model->getStaffListByData($data);
            // echo "<Pre>";print_r($staff_data);exit();

            $table = "<table  class='table' id='list-table'>
                      <tr>
                        <th>Sl. No</th>
                        <th>Staff Name</th>
                        <th>IC No.</th>
                        <th>E Mail</th>
                        <th>Staff ID</th>
                        <th>Gender</th>
                        <th>Mobile No.</th>
                        <th>DOB</th>
                        <th style='text-align: center;'><input type='checkbox' id='checkAll' name='checkAll'> Check All</th>
                    </tr>";


                $total_amount = 0;
                    for($i=0;$i<count($staff_data);$i++)
                    {

                    $id = $staff_data[$i]->id;
                    $name = $staff_data[$i]->name;
                    $ic_no = $staff_data[$i]->ic_no;
                    $mobile_number = $staff_data[$i]->mobile_number;
                    $email = $staff_data[$i]->email;
                    $staff_id = $staff_data[$i]->staff_id;
                    $gender = $staff_data[$i]->gender;
                    $dob = $staff_data[$i]->dob;
                    $j = $i+1;

                        $table .= "
                        <tr>
                            <td>$j</td>
                            <td>$name</td>
                            <td>$ic_no</td>                           
                            <td>$email</td>                       
                            <td>$staff_id</td>                       
                            <td>$gender</td>                       
                            <td>$mobile_number</td>                      
                            <td>$dob</td>                      
                            
                            <td class='text-center'>
                          <input type='checkbox' name='id_recepient[]' class='check' value='".$id."'>
                        </td>
                       
                        </tr>";
                    }


        $table.= "</table>";
        }
        echo $table;exit();
    }

    function deleteStaffRecepient($recepient_id)
    {
        $staff_data = $this->group_model->deleteStaffRecepient($recepient_id);
        echo "Success";
    }


    function sendMail()
    {
        $data = $this->security->xss_clean($this->input->post('data'));
        // echo "<Pre>";print_r($data);exit();
        $type = $data['type'];
        $id_group = $data['id_group'];


        switch ($type)
        {
            case 'Applicant':

                $recepient_list = $this->group_model->getGroupRecepientsByApplicantEmail($id_group);
                break;

            case 'Student':
                $recepient_list = $this->group_model->getGroupRecepientsByStudentEmail($id_group);
                break;

            case 'Staff':
                $recepient_list = $this->group_model->getGroupRecepientsByStaffEmail($id_group);
                break;
            
            default:
                break;
        }


        foreach ($recepient_list as $recepients)
        {
            $to = $recepients->email;
        // echo "<Pre>";print_r($to);exit();
        
         // $to = "vinayp007@yahoo.com";
         $subject = "This is subject";
         
         $message = "<b>This is HTML message.</b>";
         $message .= "<h1>This is headline.</h1>";
         
         $header = "From:vinayp007@yahoo.com \r\n";
         $header .= "Cc:afgh@somedomain.com \r\n";
         $header .= "MIME-Version: 1.0\r\n";
         $header .= "Content-type: text/html\r\n";
         
         $retval = mail ($to,$subject,$message,$header);
         
             if( $retval == true )
             {
                $msz = "Message sent successfully...";
             }
             else
             {
                $msz = "Message could not be sent...";
             }
            
        }

        echo $msz;exit();
        
    }
}
