<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">

        <div class="page-title clearfix">
            <h3>Register Students For Commitee </h3>
        </div>



            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>
            <div class="clearfix">

                <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                    <li role="presentation" class="active" ><a href="#program_detail" class="nav-link border rounded text-center"
                            aria-controls="program_detail" aria-selected="true"
                            role="tab" data-toggle="tab">Commitee Details</a>
                    </li>

                    <li role="presentation"><a href="#tab_two" class="nav-link border rounded text-center" aria-controls="tab_two" role="tab" data-toggle="tab">New Student Registration</a>
                    </li>

                    <li role="presentation"><a href="#program_scheme" class="nav-link border rounded text-center" aria-controls="program_scheme" role="tab" data-toggle="tab">Student Registration List</a>
                    </li>
                    
                </ul>

                
                <div class="tab-content offers-tab-content">

                    <div role="tabpanel" class="tab-pane active" id="program_detail">
                        <div class="col-12 mt-4">













                        <form id="form_unit" action="" method="post">

                            <!-- <a class="btn btn-link">* Registration End Dated Colloquium Not Editable</a> -->
                            
                            <div class="form-container">
                              <h4 class="form-group-title">Proposal Defense Comitee Details</h4>

                              <div class="row">

                                  <div class="col-sm-4">
                                      <div class="form-group">
                                          <label>Name <span class='error-text'>*</span></label>
                                          <input type="text" class="form-control" id="name" name="name" value="<?php echo $comitee->name; ?>">
                                      </div>
                                  </div>


                                  <div class="col-sm-4">
                                      <div class="form-group">
                                          <label>Year <span class='error-text'>*</span></label>
                                          <input type="text" class="form-control" id="year" name="year" value="<?php echo $comitee->year; ?>">
                                      </div>
                                  </div>


                                  
                                  <div class="col-sm-4">
                                      <div class="form-group">
                                          <label>Chairman <span class='error-text'>*</span></label>
                                          <input type="text" class="form-control" id="chairman" name="chairman" value="<?php echo $comitee->chairman; ?>">
                                      </div>
                                  </div>



                              </div>

                              <div class="row">



                                  <div class="col-sm-4">
                                      <div class="form-group">
                                          <label>Date <span class='error-text'>*</span></label>
                                          <input type="text" class="form-control datepicker" id="date_time" name="date_time" value="<?php echo date("Y-m-d", strtotime($comitee->date_time));?>">
                                      </div>
                                  </div>




                                  <div class="col-sm-4">
                                      <div class="form-group">
                                          <label>Supervisor <span class='error-text'>*</span> </label>
                                          <select name="id_supervisor" id="id_supervisor" class="form-control" >
                                              <option value="">Select</option>
                                              <?php
                                              if (!empty($supervisorList))
                                              {
                                                  foreach ($supervisorList as $record)
                                                  {?>
                                               <option value="<?php echo $record->id;  ?>"
                                                  <?php if($comitee->id_supervisor == $record->id)
                                                  {
                                                      echo 'selected'; 
                                                  }
                                                  ?>
                                                  >
                                                  <?php if($record->type == 0)
                                                  {
                                                      echo 'External';
                                                  }elseif($record->type == 1)
                                                  {
                                                      echo 'Internal';
                                                  }
                                                      echo " - " . $record->full_name;
                                                  ?>
                                               </option>
                                              <?php
                                                  }
                                              }
                                              ?>
                                          </select>
                                      </div>
                                  </div>

                                  <div class="col-sm-4">
                                      <div class="form-group">
                                          <label>Reader 1 <span class='error-text'>*</span> </label>
                                          <select class="form-control" id="reader_one" name="reader_one">
                                              <option value="">Select</option>
                                              <?php
                                              if (!empty($readerList))
                                              {
                                                  foreach ($readerList as $record)
                                                  {?>
                                                   <option value="<?php echo $record->id;  ?>"
                                                      <?php if($comitee->reader_one == $record->id)
                                                      {
                                                          echo 'selected'; 
                                                      }
                                                      ?>>
                                                      <?php echo $record->name . " - " . $record->email;?>
                                                   </option>
                                              <?php
                                                  }
                                              }
                                              ?>
                                          </select>
                                      </div>
                                  </div>


                              </div>

                              <div class="row">

                                  <div class="col-sm-4">
                                      <div class="form-group">
                                          <label>Reader 2 <span class='error-text'>*</span> </label>
                                          <select class="form-control" id="reader_two" name="reader_two">
                                              <option value="">Select</option>
                                              <?php
                                              if (!empty($readerList))
                                              {
                                                  foreach ($readerList as $record)
                                                  {?>
                                                   <option value="<?php echo $record->id;  ?>"
                                                      <?php if($comitee->reader_one == $record->id)
                                                      {
                                                          echo 'selected'; 
                                                      }
                                                      ?>>
                                                      <?php echo $record->name . " - " . $record->email;?>
                                                   </option>
                                              <?php
                                                  }
                                              }
                                              ?>
                                          </select>
                                      </div>
                                  </div>


                                  <div class="col-sm-4">
                                      <div class="form-group">
                                          <p>Status <span class='error-text'>*</span></p>
                                          <label class="radio-inline">
                                            <input type="radio" name="status" id="status" value="1" <?php if($comitee->status=='1') {
                                               echo "checked=checked";
                                            };?>><span class="check-radio"></span> Active
                                          </label>
                                          <label class="radio-inline">
                                            <input type="radio" name="status" id="status" value="0" <?php if($comitee->status=='0') {
                                               echo "checked=checked";
                                            };?>>
                                            <span class="check-radio"></span> In-Active
                                          </label>
                                      </div>
                                  </div>
                              </div>

                          </div>


                            <div class="button-block clearfix">
                                <div class="bttn-group">
                                    <!-- <button type="submit" class="btn btn-primary btn-lg">Save</button> -->
                                    <a href="../list" class="btn btn-link">Back</a>
                                </div>
                            </div>


                            </form>




                            <br>


                            <?php
                            if(!empty($getColloquiumDetailsByColloquiumId))
                            {
                            ?>

                                <div class="form-container">
                                        <h4 class="form-group-title">Committee Details</h4>

                                    

                                      <div class="custom-table">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                <th>Sl. No</th>
                                                 <th>Programme</th>
                                                 <th>Intake</th>
                                                 <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php
                                             $total = 0;
                                              for($i=0;$i<count($getColloquiumDetailsByColloquiumId);$i++)
                                             { ?>
                                                <tr>
                                                <td><?php echo $i+1;?></td>
                                                
                                                <td><?php echo $getColloquiumDetailsByColloquiumId[$i]->programme_code ." - " .  $getColloquiumDetailsByColloquiumId[$i]->programme_name;?></td>

                                                <td><?php echo $getColloquiumDetailsByColloquiumId[$i]->intake_year . " - " .  $getColloquiumDetailsByColloquiumId[$i]->intake_name;?></td>
                                                <td>
                                                <a onclick="deleteColloquiumDetails(<?php echo $getColloquiumDetailsByColloquiumId[$i]->id; ?>)">Delete</a>
                                                </td>

                                                 </tr>
                                              <?php 
                                          } 
                                          ?>
                                            </tbody>
                                        </table>
                                      </div>

                                    </div>

                            <?php
                            }
                            ?>


                        </div> 
                    
                    </div>



                    <div role="tabpanel" class="tab-pane" id="tab_two">
                        <div class="col-12 mt-4">



                      <form id="form_data" action="" method="post">





                          <div class="form-container">

                            <h4 class="form-group-title">Search Students</h4>

                            <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Student Name</label>
                                            <input type="text" class="form-control" id="student_name" name="student_name">
                                        </div>
                                    </div>


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>NRIC</label>
                                            <input type="text" class="form-control" id="nric" name="nric">
                                        </div>
                                    </div>


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input type="text" class="form-control" id="email_id" name="email_id">
                                        </div>
                                    </div>

                                </div>

                                <br>
                                
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" onclick="searchStudentsByData()">Search</button>
                                </div>


                                <div id="view_student">
                                </div>


                                <div class="button-block clearfix">
                                    <div class="bttn-group">
                                        <button type="submit" class="btn btn-primary btn-lg">Save</button>
                                        <a href="../registrationList" class="btn btn-link">Cancel</a>
                                    </div>
                                </div>


                            </div>


                          </form>




                        </div> 
                    
                    </div>




                    <div role="tabpanel" class="tab-pane" id="program_scheme">
                        <div class="mt-4">




                        <?php
                            if(!empty($comiteeStudentList))
                            {
                            ?>

                            <div class="form-container">
                                    <h4 class="form-group-title">Registration Student Details</h4>

                                

                                  <div class="custom-table">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                            <th>Sl. No</th>
                                             <th>Student</th>
                                             <th>Student Email</th>
                                             <th>Programme</th>
                                             <th>Intake</th>
                                             <th>Registered On</th>
                                             <!-- <th>Action</th> -->
                                            </tr>
                                        </thead>
                                        <tbody>
                                             <?php
                                         $total = 0;
                                          for($i=0;$i<count($comiteeStudentList);$i++)
                                         { ?>
                                            <tr>
                                            <td><?php echo $i+1;?></td>
                                            
                                            <td><?php echo $comiteeStudentList[$i]->student_name ." - " .  $comiteeStudentList[$i]->nric;?></td>
                                            <td><?php echo $comiteeStudentList[$i]->email_id;?></td>
                                            <td><?php echo $comiteeStudentList[$i]->programme_code ." - " .  $comiteeStudentList[$i]->programme_name;?></td>

                                            <td><?php echo $comiteeStudentList[$i]->intake_year . " - " .  $comiteeStudentList[$i]->intake_name;?></td>
                                            <td><?php
                                            if($comiteeStudentList[$i]->date_time)
                                            {

                                              echo date('d-m-Y', strtotime($comiteeStudentList[$i]->date_time));
                                            } 
                                            ?>    
                                            </td>
                                            <!-- <td>
                                            <a onclick="deleteColloquiumDetails(<?php echo $comiteeStudentList[$i]->id; ?>)">Delete</a>
                                            </td> -->

                                             </tr>
                                          <?php 
                                      } 
                                      ?>
                                        </tbody>
                                    </table>
                                  </div>

                                </div>

                        <?php
                        }
                        ?>



                        <div class="button-block clearfix">
                            <div class="bttn-group">
                                <!-- <button type="submit" class="btn btn-primary btn-lg">Save</button> -->
                                <a href="../registrationList" class="btn btn-link">Back</a>
                            </div>
                        </div>



                        </div>




                        


                        </div>
                    
                    </div>



                </div>

            </div>



        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>
</div>


<script type="text/javascript">
    $('select').select2();



    function searchStudentsByData()
    {
        var tempPR = {};
        tempPR['id_comitee'] = <?php echo $comitee->id ?>;
        tempPR['name'] = $("#student_name").val();
        tempPR['nric'] = $("#nric").val();
        tempPR['email_id'] = $("#email_id").val();

        // alert(id_student);
            $.ajax(
            {
               url: '/research/comitee/searchStudentsByData',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_student").html(result);
               }
            });   
    }




    $(document).ready(function()
    {
        $("#form_semester").validate(
        {
            rules:
            {
                name:
                {
                    required: true
                },
                code:
                {
                    required: true
                },
                credit_hours:
                {
                    required: true
                },
                id_faculty_program:
                {
                    required: true
                },
                id_course_type:
                {
                    required: true
                },
                class_total_hr:
                {
                    required: true
                },
                class_recurrence:
                {
                    required: true
                },
                class_recurrence_period:
                {
                    required: true
                },
                tutorial_total_hr:
                {
                    required: true
                },
                tutorial_recurrence:
                {
                    required: true
                },
                tutorial_recurrence_period:
                {
                    required: true
                },
                lab_total_hr:
                {
                    required: true
                },
                lab_recurrence:
                {
                    required: true
                },
                lab_recurrence_period:
                {
                    required: true
                },
                exam_total_hr:
                {
                    required: true
                },
                id_course_description:
                {
                    required: true
                }
            },
            messages:
            {
                name:
                {
                    required: "<p class='error-text'>Name Required</p>",
                },
                code:
                {
                    required: "<p class='error-text'>Code Required</p>",
                },
                credit_hours:
                {
                    required: "<p class='error-text'>Credit Hours Required</p>",
                },
                id_faculty_program:
                {
                    required: "<p class='error-text'>Select Faculty Program</p>",
                },
                id_course_type:
                {
                    required: "<p class='error-text'>Select Course Type</p>",
                },
                class_total_hr:
                {
                    required: "<p class='error-text'>Total Cr. Hours Required</p>",
                },
                class_recurrence:
                {
                    required: "<p class='error-text'>Recurrence Required</p>",
                },
                class_recurrence_period:
                {
                    required: "<p class='error-text'>Recurrence Period Required</p>",
                },
                tutorial_total_hr:
                {
                    required: "<p class='error-text'>Total Cr. Hours Required</p>",
                },
                tutorial_recurrence:
                {
                    required: "<p class='error-text'>Recurrence Required</p>",
                },
                tutorial_recurrence_period:
                {
                    required: "<p class='error-text'>Recurrence Period Required</p>",
                },
                lab_total_hr:
                {
                    required: "<p class='error-text'>Total Cr. Hours Required</p>",
                },
                lab_recurrence:
                {
                    required: "<p class='error-text'>Recurrence Required</p>",
                },
                lab_recurrence_period:
                {
                    required: "<p class='error-text'>Recurrence Period Required</p>",
                },
                exam_total_hr:
                {
                    required: "<p class='error-text'>Exam Total Hours Required</p>",
                },
                id_course_description:
                {
                    required: "<p class='error-text'>Select Course Description</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>