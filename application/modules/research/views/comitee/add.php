<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Proposal Defense Comitee</h3>
        </div>
        <form id="form_unit" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Proposal Defense Comitee Details</h4>

            <div class="row">
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Year <span class='error-text'>*</span></label>
                        <br>
                        <input type="number" pattern="\d*" class="form-control"  id="year" name="year" autocomplete="off">
                    </div>
                </div>



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Chairman <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="chairman" name="chairman">
                    </div>
                </div>



            </div>

            <div class="row">




                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="date_time" name="date_time" autocomplete="off">
                    </div>
                </div>
                



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Supervisor <span class='error-text'>*</span> </label>
                        <select name="id_supervisor" id="id_supervisor" class="form-control" >
                            <option value="">Select</option>
                            <?php
                            if (!empty($supervisorList))
                            {
                                foreach ($supervisorList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php if($record->type == 0)
                                {
                                    echo 'External';
                                }elseif($record->type == 1)
                                {
                                    echo 'Internal';
                                }
                                    echo " - " . $record->full_name;
                                ?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Reader 1 <span class='error-text'>*</span> </label>
                        <select class="form-control" id="reader_one" name="reader_one">
                            <option value="">Select</option>
                            <?php
                            if (!empty($readerList))
                            {
                                foreach ($readerList as $record)
                                {?>
                                 <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->name . " - " . $record->email;?>
                                 </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Reader 2 <span class='error-text'>*</span> </label>
                        <select class="form-control" id="reader_two" name="reader_two">
                            <option value="">Select</option>
                            <?php
                            if (!empty($readerList))
                            {
                                foreach ($readerList as $record)
                                {?>
                                 <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->name . " - " . $record->email;?>
                                 </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <p>Status <span class='error-text'>*</span></p>
                        <label class="radio-inline">
                          <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                        </label>                              
                    </div>                         
                </div>
            </div>

        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>


        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    $('select').select2();

    $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
    });

    $(document).ready(function() {
        $("#form_unit").validate({
            rules: {
                chairman: {
                    required: true
                },
                id_supervisor: {
                    required: true
                },
                reader_one: {
                    required: true
                },
                reader_two: {
                    required: true
                },
                name: {
                    required: true
                },
                year: {
                    required: true
                },
                date_time: {
                    required: true
                }
            },
            messages: {
                chairman: {
                    required: "<p class='error-text'>Chairman Required</p>",
                },
                id_supervisor: {
                    required: "<p class='error-text'>Select Supervisor</p>",
                },
                reader_one: {
                    required: "<p class='error-text'>Select Reader One</p>",
                },
                reader_two: {
                    required: "<p class='error-text'>Select Reader Two</p>",
                },
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                year: {
                    required: "<p class='error-text'>Year Required</p>",
                },
                date_time: {
                    required: "<p class='error-text'>Select Date</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
