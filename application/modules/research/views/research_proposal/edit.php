<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Research Proposal</h3>
        </div>



        <h4 class='sub-title'>Student Details</h4>

        <div class='data-list'>
            <div class='row'>

                <div class='col-sm-6'>
                    <dl>
                        <dt>Student Name :</dt>
                        <dd><?php echo $studentDetails->full_name ?></dd>
                    </dl>
                    <dl>
                        <dt>Student Email :</dt>
                        <dd><?php echo $studentDetails->email_id ?></dd>
                    </dl>
                    <dl>
                        <dt>Student NRIC :</dt>
                        <dd><?php echo $studentDetails->nric ?></dd>
                    </dl>
                    
                </div>        
                
                <div class='col-sm-6'>
                    <dl>
                        <dt>Intake :</dt>
                        <dd>
                            <?php echo $studentDetails->intake_name ?>
                        </dd>
                    </dl>
                    <dl>
                        <dt>Programme :</dt>
                        <dd><?php echo $studentDetails->programme_name ?></dd>
                    </dl>
                    <dl>
                        <dt>Nationality :</dt>
                        <dd><?php echo $studentDetails->nationality ?></dd>
                    </dl>
                </div>

            </div>
        </div>















        <form id="form_programme" action="" method="post">
        <div class="form-container">
            <h4 class="form-group-title">Research Proposal Details</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Title <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $researchProposal->name; ?>">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Research Category <span class='error-text'>*</span></label>
                        <select class="form-control" id="id_research_category" name="id_research_category">
                            <option value="">Select</option>
                            <?php
                            if (!empty($researchCategoryList))
                            {
                                foreach ($researchCategoryList as $record)
                                {?>
                                 <option value="<?php echo $record->id;  ?>"
                                    <?php 
                                    if($record->id == $researchProposal->id_research_category)
                                    {
                                        echo 'selected';
                                    }
                                    ?>
                                    >
                                    <?php echo $record->name;?>
                                 </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Research Topic <span class='error-text'>*</span></label>
                        <select class="form-control" id="id_research_topic" name="id_research_topic">
                            <option value="">Select</option>
                            <?php
                            if (!empty($researchTopicList))
                            {
                                foreach ($researchTopicList as $record)
                                {?>
                                 <option value="<?php echo $record->id;  ?>"
                                    <?php 
                                    if($record->id == $researchProposal->id_research_topic)
                                    {
                                        echo 'selected';
                                    }
                                    ?>
                                    >
                                    <?php echo $record->name;?>
                                 </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
                

            </div>

            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Synopsis <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="synopsis" name="synopsis" name="max_candidates" value="<?php echo $researchProposal->synopsis; ?>">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Thesis Committee Meeting <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="meeting_date" name="meeting_date" autocomplete="off" value="<?php if($researchProposal->meeting_date){ echo date('d-m-Y', strtotime($researchProposal->meeting_date)); } ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Upload</label>
                        <input type="file" class="form-control" id="document" name="document">
                    </div>
                </div>

            </div>


            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Completion Date </label>
                        <input type="text" class="form-control datepicker" id="completion_date" name="completion_date" autocomplete="off" value="<?php if($researchProposal->completion_date){ echo date('d-m-Y', strtotime($researchProposal->completion_date)); } ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Semester Start </label>
                        <input type="text" class="form-control" id="semester_start_date" name="semester_start_date" readonly>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Semester End </label>
                        <input type="text" class="form-control" id="semester_end_date" name="semester_end_date" readonly>
                    </div>
                </div>

            </div>

        </div>
    

        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="../list" class="btn btn-link">Back</a>
            </div>
        </div>


    </form>



























    <div class="form-container">
            <h4 class="form-group-title"> Proposal Details</h4>          
            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>
            <div class="clearfix">
                <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                    <li role="presentation" class="active" ><a href="#invoice" class="nav-link border rounded text-center"
                            aria-controls="invoice" aria-selected="true"
                            role="tab" data-toggle="tab">Supervisor Details</a>
                    </li>
                    <li role="presentation"><a href="#tab_two" class="nav-link border rounded text-center"
                            aria-controls="tab_two" role="tab" data-toggle="tab">Examiner Details</a>
                    </li>

                      
                </ul>

                
                <div class="tab-content offers-tab-content">

                    <div role="tabpanel" class="tab-pane active" id="invoice">
                        <div class="col-12 mt-4">




                        <form id="form_supervisor" action="" method="post">

                        <div class="form-container">
                        <h4 class="form-group-title">Supervisor Details</h4>

                            <div class="row">


                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Supervisor <span class='error-text'>*</span></label>
                                        <select class="form-control" id="id_staff" name="id_staff">
                                            <option value="">Select</option>
                                            <?php
                                            if (!empty($staffList))
                                            {
                                                foreach ($staffList as $record)
                                                {?>
                                                 <option value="<?php echo $record->id;  ?>">
                                                    <?php echo $record->ic_no . " - " . $record->name;?>
                                                 </option>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>


                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Supervisor Role <span class='error-text'>*</span></label>
                                        <select class="form-control" id="id_staff_role" name="id_staff_role">
                                            <option value="">Select</option>
                                            <?php
                                            if (!empty($supervisorRoleList))
                                            {
                                                foreach ($supervisorRoleList as $record)
                                                {?>
                                                 <option value="<?php echo $record->id;  ?>">
                                                    <?php echo $record->code;?>
                                                 </option>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <!-- <div class="col-sm-4">
                                    <div class="form-group">
                                        <p>Status <span class='error-text'>*</span></p>
                                        <label class="radio-inline">
                                          <input type="radio" name="supervisor_status" id="supervisor_status" value="1" checked="checked"><span class="check-radio"></span> Active
                                        </label>
                                        <label class="radio-inline">
                                          <input type="radio" name="supervisor_status" id="supervisor_status" value="0"><span class="check-radio"></span> In-Active
                                        </label>                              
                                    </div>                         
                                </div> -->


                            </div>


                            <div class="row">
                              
                                <div class="col-sm-4">
                                    <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="addResearchProposalHasSupervisor()">Add</button>
                                </div>
                            </div>

                        </div>


                    </form>


                    <?php

                    if(!empty($researchProposalHasSupervisor))
                    {
                        ?>

                        <div class="form-container">
                                <h4 class="form-group-title">Supervisor Details</h4>

                            

                              <div class="custom-table">
                                <table class="table">
                                    <thead>
                                        <tr>
                                        <th>Sl. No</th>
                                        <th>Supervisor</th>
                                        <th>Role</th>
                                        <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php
                                     $total = 0;
                                      for($i=0;$i<count($researchProposalHasSupervisor);$i++)
                                     { ?>
                                        <tr>
                                        <td><?php echo $i+1;?></td>
                                        <td><?php echo $researchProposalHasSupervisor[$i]->ic_no . " - " . $researchProposalHasSupervisor[$i]->staff_name;?></td>
                                        <td><?php echo $researchProposalHasSupervisor[$i]->supervisor_role;?></td>
                                        <td>
                                        <a onclick="deleteResearchProposalHasSupervisor(<?php echo $researchProposalHasSupervisor[$i]->id; ?>)">Delete</a>
                                        </td>

                                         </tr>
                                      <?php 
                                  } 
                                  ?>
                                    </tbody>
                                </table>
                              </div>

                            </div>

                    <?php
                    
                    }
                     ?>


                        </div> 
                    </div>





                    <div role="tabpanel" class="tab-pane" id="tab_two">
                        <div class="col-12">




                        <form id="form_examiner" action="" method="post">

                        <div class="form-container">
                        <h4 class="form-group-title">Examiner Details</h4>

                            <div class="row">


                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Examiner <span class='error-text'>*</span></label>
                                        <select class="form-control" id="id_examiner" name="id_examiner">
                                            <option value="">Select</option>
                                            <?php
                                            if (!empty($examinerList))
                                            {
                                                foreach ($examinerList as $record)
                                                {?>
                                                 <option value="<?php echo $record->id;  ?>">
                                                    <?php
                                                    if($record->type == 1)
                                                    {
                                                        echo $record->ic_no . " - " . $record->staff_name;
                                                    }
                                                    else
                                                    {
                                                        echo "External - " . $record->full_name;
                                                    }
                                                    ?>
                                                 </option>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>


                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Examiner Role <span class='error-text'>*</span></label>
                                        <select class="form-control" id="id_examiner_role" name="id_examiner_role">
                                            <option value="">Select</option>
                                            <?php
                                            if (!empty($examinerRoleList))
                                            {
                                                foreach ($examinerRoleList as $record)
                                                {?>
                                                 <option value="<?php echo $record->id;  ?>">
                                                    <?php echo $record->code;?>
                                                 </option>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <!-- <div class="col-sm-4">
                                    <div class="form-group">
                                        <p>Status <span class='error-text'>*</span></p>
                                        <label class="radio-inline">
                                          <input type="radio" name="examiner_status" id="examiner_status" value="1" checked="checked"><span class="check-radio"></span> Active
                                        </label>
                                        <label class="radio-inline">
                                          <input type="radio" name="examiner_status" id="examiner_status" value="0"><span class="check-radio"></span> Inactive
                                        </label>                              
                                    </div>                         
                                </div> -->

                            </div>


                            <div class="row">

                              
                                <div class="col-sm-4">
                                    <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="addResearchProposalHasExaminer()">Add</button>
                                </div>
                            </div>

                        </div>


                    </form>


                    <?php

                    if(!empty($researchProposalHasExaminer))
                    {
                        ?>

                        <div class="form-container">
                                <h4 class="form-group-title">Examiner Details</h4>

                            

                              <div class="custom-table">
                                <table class="table">
                                    <thead>
                                        <tr>
                                        <th>Sl. No</th>
                                        <th>Type</th>
                                        <th>Examiner</th>
                                        <th>Role</th>
                                        <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php
                                     $total = 0;
                                      for($i=0;$i<count($researchProposalHasExaminer);$i++)
                                     { ?>
                                        <tr>
                                        <td><?php echo $i+1;?></td>
                                        <td>
                                        <?php 
                                        if($researchProposalHasExaminer[$i]->type == 1)
                                        {
                                            echo 'Internal';
                                        }
                                        else
                                        {
                                            echo 'External';
                                        }
                                        ?>
                                        </td>

                                        <td>
                                        <?php 
                                        if($researchProposalHasExaminer[$i]->type == 1)
                                        {
                                            echo $researchProposalHasExaminer[$i]->ic_no . " - " . $researchProposalHasExaminer[$i]->staff_name;
                                        }
                                        else
                                        {
                                            echo $researchProposalHasExaminer[$i]->full_name;
                                        }
                                        ?></td>
                                        <td><?php echo $researchProposalHasExaminer[$i]->examiner_role;?></td>
                                        <td>
                                        <a onclick="deleteResearchProposalHasExaminer(<?php echo $researchProposalHasExaminer[$i]->id; ?>)">Delete</a>
                                        </td>

                                         </tr>
                                      <?php 
                                  } 
                                  ?>
                                    </tbody>
                                </table>
                              </div>

                            </div>

                    <?php
                    
                    }
                     ?>



                        </div> 
                    </div>




                </div>

            </div>
        

    </div>











           

      </div>
    </div>

   </div> <!-- END row-->

           
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    $('select').select2();


    function addResearchProposalHasSupervisor()
    {
        if($('#form_supervisor').valid())
        {

        var tempPR = {};
        tempPR['id_staff'] = $("#id_staff").val();
        tempPR['id_staff_role'] = $("#id_staff_role").val();
        // tempPR['status'] = $("#supervisor_status").val();
        tempPR['status'] = 1;
        tempPR['id_proposal'] = <?php echo $researchProposal->id ?>;

            $.ajax(
            {
               url: '/research/proposal/addResearchProposalHasSupervisor',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    window.location.reload();
               }
            });
        }
    }

    function deleteResearchProposalHasSupervisor(id) {
        // alert(id);
         $.ajax(
            {
               url: '/research/proposal/deleteResearchProposalHasSupervisor/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    window.location.reload();
               }
            });
    }



    function addResearchProposalHasExaminer()
    {
        if($('#form_examiner').valid())
        {

        var tempPR = {};
        tempPR['id_examiner'] = $("#id_examiner").val();
        tempPR['id_examiner_role'] = $("#id_examiner_role").val();
        // tempPR['status'] = $("#examiner_status").val();
        tempPR['status'] = 1;
        tempPR['id_proposal'] = <?php echo $researchProposal->id ?>;

            $.ajax(
            {
               url: '/research/proposal/addResearchProposalHasExaminer',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    window.location.reload();
               }
            });
        }
    }

    function deleteResearchProposalHasExaminer(id) {
        // alert(id);
         $.ajax(
            {
               url: '/research/proposal/deleteResearchProposalHasExaminer/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    window.location.reload();
               }
            });
    }
    

    $(document).ready(function() {
        $("#form_supervisor").validate({
            rules: {
                id_staff: {
                    required: true
                },
                id_staff_role: {
                    required: true
                }
            },
            messages: {
                id_staff: {
                    required: "<p class='error-text'>Select Supervisor</p>",
                },
                id_staff_role: {
                    required: "<p class='error-text'>Select Supervisor Role</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


    $(document).ready(function() {
        $("#form_examiner").validate({
            rules: {
                id_examiner: {
                    required: true
                },
                id_examiner_role: {
                    required: true
                }
            },
            messages: {
                id_examiner: {
                    required: "<p class='error-text'>Select Examiner</p>",
                },
                id_examiner_role: {
                    required: "<p class='error-text'>Select Examiner Role</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });



    $(document).ready(function() {
        $("#form_programme").validate({
            rules: {
                id_intake: {
                    required: true
                },
                id_programme: {
                    required: true
                },
                id_student: {
                    required: true
                },
                name: {
                    required: true
                },
                id_research_topic: {
                    required: true
                },
                id_research_category: {
                    required: true
                },
                synopsis: {
                    required: true
                },
                meeting_date: {
                    required: true
                }
            },
            messages: {
                id_intake: {
                    required: "<p class='error-text'>Select Intake</p>",
                },
                id_programme: {
                    required: "<p class='error-text'>Select Program</p>",
                },
                id_student: {
                    required: "<p class='error-text'>Select Student</p>",
                },
                name: {
                    required: "<p class='error-text'>Title Required</p>",
                },
                id_research_topic: {
                    required: "<p class='error-text'>Select Research Topic</p>",
                },
                id_research_category: {
                    required: "<p class='error-text'>Select Research Category</p>",
                },
                synopsis: {
                    required: "<p class='error-text'>Synopsis Required</p>",
                },
                meeting_date: {
                    required: "<p class='error-text'>Meeting Date Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


    $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
        yearRange: "2019:2020"
    });
  } );

</script>