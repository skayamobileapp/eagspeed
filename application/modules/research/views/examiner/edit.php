<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Research-Examiner</h3>
        </div>
        <form id="form_unit" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Research-Examiner Details</h4>

            <div class="row">
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Type <span class='error-text'>*</span></label>
                        <select class="form-control" id="type" name="type" onchange="showType(this.value)">
                            <option value="">Select</option>
                            <option value="1"
                            <?php 
                            if($examiner->type == 1)
                            {
                                echo 'selected';
                            }
                            ?>
                            >Internal</option>
                            <option value="0"
                            <?php 
                            if($examiner->type == 0)
                            {
                                echo 'selected';
                            }
                            ?>
                            >External</option>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Start Date</label>
                        <input type="text" class="form-control datepicker" id="start_date" name="start_date" autocomplete="off" value="<?php if($examiner->end_date){ echo date('d-m-Y', strtotime($examiner->end_date)); } ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>End Date</label>
                        <input type="text" class="form-control datepicker" id="end_date" name="end_date" autocomplete="off" value="<?php if($examiner->start_date){ echo date('d-m-Y', strtotime($examiner->start_date)); } ?>">
                    </div>
                </div>


            </div>

            <div class="row" id="view_external_staff" style="display: none;">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Fullname <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="full_name" name="full_name" value="<?php echo $examiner->full_name ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Email <span class='error-text'>*</span></label>
                        <input type="email" class="form-control" id="email" name="email" value="<?php echo $examiner->email ?>">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Password <span class='error-text'>*</span></label>
                        <input type="password" class="form-control" id="password" name="password" value="<?php echo $examiner->password ?>">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Contact No. <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="contact_no" name="contact_no" value="<?php echo $examiner->password ?>">
                    </div>
                </div>



            </div>


            <div class="row" id="view_internal_staff" style="display: none;">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Staff <span class='error-text'>*</span></label>
                        <select class="form-control" id="id_staff" name="id_staff">
                            <option value="">Select</option>
                            <?php
                            if (!empty($staffList))
                            {
                                foreach ($staffList as $record)
                                {?>
                                 <option value="<?php echo $record->id;  ?>"
                                    <?php 
                                    if($record->id == $examiner->id_staff)
                                    {
                                        echo 'selected';
                                    }
                                    ?>
                                    >
                                    <?php echo $record->ic_no . " - " . $record->name;?>
                                 </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

            </div>

        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="../list" class="btn btn-link">Back</a>
            </div>
        </div>


        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    function showType(type)
    {
        if(type == 0)
        {
            $("#view_external_staff").show();
            $("#view_internal_staff").hide();

        }else
        {
            $("#view_internal_staff").show();
            $("#view_external_staff").hide();
        }
    }

    function checkOnInit()
    {
        var type = <?php echo $examiner->type; ?>;
        showType(type);

    }


    $('select').select2();

    $(document).ready(function() {
        checkOnInit();
        $("#form_unit").validate({
            rules: {
                type: {
                    required: true
                },
                full_name: {
                    required: true
                },
                email: {
                    required: true
                },
                password: {
                    required: true
                },
                contact_no: {
                    required: true
                },
                id_staff: {
                    required: true
                }
            },
            messages: {
                type: {
                    required: "<p class='error-text'>Select Type</p>",
                },
                full_name: {
                    required: "<p class='error-text'>Fullname Required</p>",
                },
                email: {
                    required: "<p class='error-text'>Email Required</p>",
                },
                password: {
                    required: "<p class='error-text'>Password Required</p>",
                },
                contact_no: {
                    required: "<p class='error-text'>Contact No. Required</p>",
                },
                id_staff: {
                    required: "<p class='error-text'>Select Staff</p>",
                }

            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

     $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
        yearRange: "2019:2020"
    });
  } );
</script>
