<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>Overview</h3>
      <!-- <a href="add" class="btn btn-primary">+ Add Stage</a> -->
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">
              
              </div>
              <!-- <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href="list" class="btn btn-link">Clear All Fields</a>
              </div> -->
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
        <?php
          if (!empty($stageOverview))
          {
            $i=1;
            foreach ($stageOverview as $record)
            {
              if($i == 1)
              {
                echo "<th style='text-align: center;'></th>";
              }
          ?>
            <th style='text-align: center;'><?php echo $record->name; ?></th>

        <?php
          $i++;
            }
          }
          ?>
        </tr>
      </thead>

      <tbody>
          <tr>
          <?php
          if (!empty($stageOverview))
          {
            $j=1;
            if($j == 1)
            {
                echo "<th style='text-align: center;'>Part Time</th>";
            }

            foreach ($stageOverview as $record)
            {

              
            
            if($record->type == 'Part Time')
            {
              foreach ($record->semesters as $data)
              {
                // if (!empty($stageOverview->semesters))
                // {
                  

            ?>
                <th style='text-align: center;'><?php echo $data->name ?></th>
          <?php
              $j++;
                }
              }
            }
          }
          ?>
          </tr>

          <tr>
          <?php
          if (!empty($stageOverview))
          {
            $j=1;
            if($j == 1)
            {
                echo "<th style='text-align: center;'>Full Time</th>";
            }
            
            foreach ($stageOverview as $record)
            {

              
            
            if($record->type == 'Full Time')
            {
              foreach ($record->semesters as $data)
              {
                // if (!empty($stageOverview->semesters))
                // {
                  

            ?>
                <th style='text-align: center;'><?php echo $data->name ?></th>
          <?php
              $j++;
                }
              }
            }
          }
          ?>
          </tr>

        </tbody>
      </table>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script type="text/javascript">
  
  $('select').select2();

  function clearSearchForm()
      {
        window.location.reload();
      }
</script>