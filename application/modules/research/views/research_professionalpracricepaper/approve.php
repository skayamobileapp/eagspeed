<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Approve Research Professional Pracrice Paper</h3>
        </div>



        <h4 class='sub-title'>Student Details</h4>

        <div class='data-list'>
            <div class='row'>

                <div class='col-sm-6'>
                    <dl>
                        <dt>Student Name :</dt>
                        <dd><?php echo $studentDetails->full_name ?></dd>
                    </dl>
                    <dl>
                        <dt>Student Email :</dt>
                        <dd><?php echo $studentDetails->email_id ?></dd>
                    </dl>
                    <dl>
                        <dt>Student NRIC :</dt>
                        <dd><?php echo $studentDetails->nric ?></dd>
                    </dl>
                    
                </div>        
                
                <div class='col-sm-6'>
                    <dl>
                        <dt>Intake :</dt>
                        <dd>
                            <?php echo $studentDetails->intake_name ?>
                        </dd>
                    </dl>
                    <dl>
                        <dt>Programme :</dt>
                        <dd><?php echo $studentDetails->programme_name ?></dd>
                    </dl>
                    <dl>
                        <dt>Nationality :</dt>
                        <dd><?php echo $studentDetails->nationality ?></dd>
                    </dl>
                </div>

            </div>
        </div>















        <form id="form_main" action="" method="post">



        <div class="form-container">
            <h4 class="form-group-title">Research Professional Pracrice Paper Details</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Start Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="start_date" name="start_date" autocomplete="off" value="<?php if($researchProfessionalpracricepaper->start_date){ echo date('d-m-Y', strtotime($researchProfessionalpracricepaper->start_date)); } ?>" readonly>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>End Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="end_date" name="end_date" autocomplete="off" value="<?php if($researchProfessionalpracricepaper->end_date){ echo date('d-m-Y', strtotime($researchProfessionalpracricepaper->end_date)); } ?>" readonly>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Proposed Area Of PPP <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="proposed_area_ppp" name="proposed_area_ppp" value="<?php echo $researchProfessionalpracricepaper->proposed_area_ppp; ?>" readonly>
                    </div>
                </div>
                

            </div>

            <div class="row">

                <br>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Reason For Applying <span class='error-text'>*</span></label>
                        <br>
                        <input type="checkbox" id="skill_enhancement" name="skill_enhancement" 
                        <?php if($researchProfessionalpracricepaper->skill_enhancement == 1)
                        {
                            echo "checked='true'";
                        }?>>
                        Skill Enhancement
                        <br>
                        <label>Other Reason</label>
                        <input type="text" class="form-control" id="other_details" name="other_details" >
                    </div>
                </div>



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Status <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="synopsis" name="synopsis" name="max_candidates" value="<?php 
                        if($researchProfessionalpracricepaper->status == 0)
                        {
                            echo 'Pending';
                        }elseif($researchProfessionalpracricepaper->status == 1)
                        {
                            echo 'Approved';
                        }if($researchProfessionalpracricepaper->status == 2)
                        {
                            echo 'Rejected';
                        } ?>" readonly>
                    </div>
                </div>



                <?php 
                    if($researchProfessionalpracricepaper->status == 2)
                    {

                    ?>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Reason <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="reason" name="reason" name="max_candidates" value="
                        <?php echo $researchProfessionalpracricepaper->reason; ?>" readonly>
                    </div>
                </div>

                    <?php

                    }
                   ?>



            </div>





            <?php
            if($researchProfessionalpracricepaper->status == '0')
            {
             ?>


             <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <p> Approval <span class='error-text'>*</span></p>
                        <label class="radio-inline">
                            <input type="radio" id="ed1" name="status" value="1" onclick="hideRejectField()"><span class="check-radio"></span> Approve
                        </label>
                        <label class="radio-inline">
                            <input type="radio" id="ed2" name="status" value="2" onclick="showRejectField()"><span class="check-radio"></span> Reject
                        </label>
                    </div>
                </div>

                 <div class="col-sm-4" id="view_reject" style="display: none">
                    <div class="form-group">
                        <label>Reason <span class='error-text'>*</span></label>
                        <input type="text" id="reason" name="reason" class="form-control">
                    </div>
                </div>

            </div>

          <?php
            }
            ?>






        </div>
    

        <div class="button-block clearfix">
            <div class="bttn-group">

            <?php
            if($researchProfessionalpracricepaper->status == '0')
            {
             ?>
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
            <?php
            }
            ?>
                <a href="../approvalList" class="btn btn-link">Back</a>
            </div>
        </div>


    </form>



























    <div class="form-container">
            <h4 class="form-group-title"> Professional Pracrice Paper Details</h4>          
            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>
            <div class="clearfix">
                <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                    <li role="presentation" class="active" ><a href="#invoice" class="nav-link border rounded text-center"
                            aria-controls="invoice" aria-selected="true"
                            role="tab" data-toggle="tab">Employment Details</a>
                    </li>
                    <li role="presentation"><a href="#tab_two" class="nav-link border rounded text-center"
                            aria-controls="tab_two" role="tab" data-toggle="tab">Supervisor Details</a>
                    </li>

                    <li role="presentation"><a href="#tab_three" class="nav-link border rounded text-center"
                            aria-controls="tab_three" role="tab" data-toggle="tab">Examiner Details</a>
                    </li>

                    

                      
                </ul>

                
                <div class="tab-content offers-tab-content">




                    <div role="tabpanel" class="tab-pane active" id="invoice">
                        <div class="col-12 mt-4">





                    <div class="form-container">
                            <h4 class="form-group-title">Employment Details</h4>

                    <?php

                    if(!empty($researchProfessionalpracricepaperHasEmployment))
                    {
                        ?>

                        

                            

                              <div class="custom-table">
                                <table class="table">
                                    <thead>
                                        <tr>
                                        <th>Sl. No</th>
                                        <th>Company Name</th>
                                        <th>Address</th>
                                        <th>Position</th>
                                        <th>Job Function</th>
                                        <th>Year</th>
                                        <th>Reference Number</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php
                                     $total = 0;
                                      for($i=0;$i<count($researchProfessionalpracricepaperHasEmployment);$i++)
                                     { ?>
                                        <tr>
                                        <td><?php echo $i+1;?></td>
                                        <td><?php echo $researchProfessionalpracricepaperHasEmployment[$i]->company_name;?></td>
                                        <td><?php echo $researchProfessionalpracricepaperHasEmployment[$i]->address;?></td>
                                        <td><?php echo $researchProfessionalpracricepaperHasEmployment[$i]->position;?></td>
                                        <td><?php echo $researchProfessionalpracricepaperHasEmployment[$i]->job_function;?></td>
                                        <td><?php echo $researchProfessionalpracricepaperHasEmployment[$i]->from_year . " - " . $researchProfessionalpracricepaperHasEmployment[$i]->to_year;?></td>
                                        <td><?php echo $researchProfessionalpracricepaperHasEmployment[$i]->reference_number;?></td>

                                         </tr>
                                      <?php 
                                  } 
                                  ?>
                                    </tbody>
                                </table>
                              </div>


                    <?php
                    
                    }
                     ?>

                        


                        </div>


                        </div> 
                    </div>






                    <div role="tabpanel" class="tab-pane" id="tab_two">
                        <div class="col-12 mt-4">



                        <div class="form-container">
                            <h4 class="form-group-title">Supervisor Details</h4>



                    <?php

                    if(!empty($researchProfessionalpracricepaperHasSupervisor))
                    {
                        ?>

                        

                            

                              <div class="custom-table">
                                <table class="table">
                                    <thead>
                                        <tr>
                                        <th>Sl. No</th>
                                        <th>Supervisor</th>
                                        <th>Role</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php
                                     $total = 0;
                                      for($i=0;$i<count($researchProfessionalpracricepaperHasSupervisor);$i++)
                                     { ?>
                                        <tr>
                                        <td><?php echo $i+1;?></td>
                                        <td><?php echo $researchProfessionalpracricepaperHasSupervisor[$i]->ic_no . " - " . $researchProfessionalpracricepaperHasSupervisor[$i]->staff_name;?></td>
                                        <td><?php echo $researchProfessionalpracricepaperHasSupervisor[$i]->supervisor_role;?></td>

                                         </tr>
                                      <?php 
                                  } 
                                  ?>
                                    </tbody>
                                </table>
                              </div>

                            
                    <?php
                    
                    }
                     ?>



                        </div>


                        </div> 
                    </div>





                    <div role="tabpanel" class="tab-pane" id="tab_three">
                        <div class="col-12">



                        <div class="form-container">
                            <h4 class="form-group-title">Examiner Details</h4>



                    <?php

                    if(!empty($researchProfessionalpracricepaperHasExaminer))
                    {
                        ?>

                        

                            

                              <div class="custom-table">
                                <table class="table">
                                    <thead>
                                        <tr>
                                        <th>Sl. No</th>
                                        <th>Type</th>
                                        <th>Examiner</th>
                                        <th>Role</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php
                                     $total = 0;
                                      for($i=0;$i<count($researchProfessionalpracricepaperHasExaminer);$i++)
                                     { ?>
                                        <tr>
                                        <td><?php echo $i+1;?></td>
                                        <td>
                                        <?php 
                                        if($researchProfessionalpracricepaperHasExaminer[$i]->type == 1)
                                        {
                                            echo 'Internal';
                                        }
                                        else
                                        {
                                            echo 'External';
                                        }
                                        ?>
                                        </td>

                                        <td>
                                        <?php 
                                        if($researchProfessionalpracricepaperHasExaminer[$i]->type == 1)
                                        {
                                            echo $researchProfessionalpracricepaperHasExaminer[$i]->ic_no . " - " . $researchProfessionalpracricepaperHasExaminer[$i]->staff_name;
                                        }
                                        else
                                        {
                                            echo $researchProfessionalpracricepaperHasExaminer[$i]->full_name;
                                        }
                                        ?></td>
                                        <td><?php echo $researchProfessionalpracricepaperHasExaminer[$i]->examiner_role;?></td>

                                         </tr>
                                      <?php 
                                  } 
                                  ?>
                                    </tbody>
                                </table>
                              </div>


                    <?php
                    
                    }
                     ?>

                        </div>


                        </div> 
                    </div>




                </div>

            </div>
        

    </div>











           

      </div>
    </div>

   </div> <!-- END row-->

           
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    $('select').select2();


    function addResearchProfessionalpracricepaperHasSupervisor()
    {
        if($('#form_supervisor').valid())
        {

        var tempPR = {};
        tempPR['id_staff'] = $("#id_staff").val();
        tempPR['id_staff_role'] = $("#id_staff_role").val();
        // tempPR['status'] = $("#supervisor_status").val();
        tempPR['status'] = 1;
        tempPR['id_professionalpracricepaper'] = <?php echo $researchProfessionalpracricepaper->id ?>;

            $.ajax(
            {
               url: '/research/professionalpracricepaper/addResearchProfessionalpracricepaperHasSupervisor',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    window.location.reload();
               }
            });
        }
    }

    function deleteResearchProfessionalpracricepaperHasSupervisor(id) {
        // alert(id);
         $.ajax(
            {
               url: '/research/professionalpracricepaper/deleteResearchProfessionalpracricepaperHasSupervisor/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    window.location.reload();
               }
            });
    }



    function addResearchProfessionalpracricepaperHasExaminer()
    {
        if($('#form_examiner').valid())
        {

        var tempPR = {};
        tempPR['id_examiner'] = $("#id_examiner").val();
        tempPR['id_examiner_role'] = $("#id_examiner_role").val();
        // tempPR['status'] = $("#examiner_status").val();
        tempPR['status'] = 1;
        tempPR['id_professionalpracricepaper'] = <?php echo $researchProfessionalpracricepaper->id ?>;

            $.ajax(
            {
               url: '/research/professionalpracricepaper/addResearchProfessionalpracricepaperHasExaminer',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    window.location.reload();
               }
            });
        }
    }

    function deleteResearchProfessionalpracricepaperHasExaminer(id) {
        // alert(id);
         $.ajax(
            {
               url: '/research/professionalpracricepaper/deleteResearchProfessionalpracricepaperHasExaminer/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    window.location.reload();
               }
            });
    }




    function addResearchProfessionalpracricepaperHasEmployment()
    {
        if($('#form_employment').valid())
        {

        var tempPR = {};
        tempPR['company_name'] = $("#company_name").val();
        tempPR['address'] = $("#address").val();
        tempPR['position'] = $("#position").val();
        tempPR['job_function'] = $("#job_function").val();
        tempPR['from_year'] = $("#from_year").val();
        tempPR['to_year'] = $("#to_year").val();
        tempPR['reference_number'] = $("#reference_number").val();
        tempPR['reference_address'] = $("#reference_address").val();
        tempPR['status'] = 1;
        tempPR['id_professionalpracricepaper'] = <?php echo $researchProfessionalpracricepaper->id ?>;

            $.ajax(
            {
               url: '/research/professionalpracricepaper/addResearchProfessionalpracricepaperHasEmployment',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    window.location.reload();
               }
            });
        }
    }

    function deleteResearchProfessionalpracricepaperHasEmployment(id)
    {
         $.ajax(
            {
               url: '/research/professionalpracricepaper/deleteResearchProfessionalpracricepaperHasEmployment/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    window.location.reload();
               }
            });
    }
    

    $(document).ready(function() {
        $("#form_supervisor").validate({
            rules: {
                id_staff: {
                    required: true
                },
                id_staff_role: {
                    required: true
                }
            },
            messages: {
                id_staff: {
                    required: "<p class='error-text'>Select Supervisor</p>",
                },
                id_staff_role: {
                    required: "<p class='error-text'>Select Supervisor Role</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


    $(document).ready(function() {
        $("#form_examiner").validate({
            rules: {
                id_examiner: {
                    required: true
                },
                id_examiner_role: {
                    required: true
                }
            },
            messages: {
                id_examiner: {
                    required: "<p class='error-text'>Select Examiner</p>",
                },
                id_examiner_role: {
                    required: "<p class='error-text'>Select Examiner Role</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });



    $(document).ready(function() {
        $("#form_employment").validate({
            rules: {
                company_name: {
                    required: true
                },
                address: {
                    required: true
                },
                position: {
                    required: true
                },
                job_function: {
                    required: true
                },
                from_year: {
                    required: true
                },
                to_year: {
                    required: true
                },
                reference_number: {
                    required: true
                },
                reference_address: {
                    required: true
                }
            },
            messages: {
                company_name: {
                    required: "<p class='error-text'>Company Name Required</p>",
                },
                address: {
                    required: "<p class='error-text'>Address Required</p>",
                },
                position: {
                    required: "<p class='error-text'>Position Required</p>",
                },
                job_function: {
                    required: "<p class='error-text'>Job Function Required</p>",
                },
                from_year: {
                    required: "<p class='error-text'>From Year Required</p>",
                },
                to_year: {
                    required: "<p class='error-text'>To Year Required</p>",
                },
                reference_number: {
                    required: "<p class='error-text'>Reference No. Required</p>",
                },
                reference_address: {
                    required: "<p class='error-text'>Reference Address Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });




    $(document).ready(function() {
        $("#form_programme").validate({
            rules: {
                id_intake: {
                    required: true
                },
                id_programme: {
                    required: true
                },
                id_student: {
                    required: true
                },
                start_date: {
                    required: true
                },
                end_date: {
                    required: true
                },
                skill_enhancement: {
                    required: true
                },
                proposed_area_ppp: {
                    required: true
                }
            },
            messages: {
                id_intake: {
                    required: "<p class='error-text'>Select Intake</p>",
                },
                id_programme: {
                    required: "<p class='error-text'>Select Program</p>",
                },
                id_student: {
                    required: "<p class='error-text'>Select Student</p>",
                },
                start_date: {
                    required: "<p class='error-text'>Select Start Date</p>",
                },
                end_date: {
                    required: "<p class='error-text'>Select End Date</p>",
                },
                skill_enhancement: {
                    required: "<p class='error-text'>Select Skill Enhancement</p>",
                },
                proposed_area_ppp: {
                    required: "<p class='error-text'>Proposed Area Of PPP Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });






    $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                status: {
                    required: true
                },
                 reason: {
                    required: true
                }
            },
            messages: {
                status: {
                    required: "<p class='error-text'>Select Status",
                },
                reason: {
                    required: "<p class='error-text'>Reason Required",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    function showRejectField(){
            $("#view_reject").show();
    }

    function hideRejectField(){
            $("#view_reject").hide();
    }





    $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
        yearRange: "2019:2020"
    });
  } );

</script>