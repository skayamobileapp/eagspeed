<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Course Registration</h3>
        </div>
        <form id="form_grade" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Course Registration</h4>

            <div class="row">

                <div class="col-sm-4">
                  <div class="form-group">
                     <label>Education Level <span class='error-text'>*</span></label>
                     <select name="id_education_level" id="id_education_level" class="form-control" onchange="getProgrammeByEducationLevelId(this.value)">
                        <option value="">Select</option>
                        <?php
                           if (!empty($degreeTypeList))
                           {
                             foreach ($degreeTypeList as $record)
                             {
                              if($record->name == 'POSTGRADUATE')
                              {
                                    ?>
                        <option value="<?php echo $record->id;  ?>">
                           <?php echo $record->name;  ?>
                        </option>
                        <?php
                              }
                            }
                          }
                           ?>
                     </select>
                  </div>
                </div>


                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Programme <span class='error-text'>*</span></label>
                        <span id="view_programme">
                          <select class="form-control" id='id_programme' name='id_programme'>
                            <option value=''></option>
                          </select>
                        </span>
                    </div>
                </div>
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Intake <span class='error-text'>*</span></label>
                        <span id="view_intake">
                          <select class="form-control" id='id_intake' name='id_intake'>
                            <option value=''></option>
                          </select>
                        </span>
                    </div>
                </div>

            </div>

            <div class="row">

                
               
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Learning Mode <span class='error-text'>*</span></label>
                        <span id="view_learning_mode">
                          <select class="form-control" id='id_learning_mode' name='id_learning_mode'>
                            <option value=''></option>
                          </select>
                        </span>
                    </div>
                </div>

            

                <div class="col-sm-4">
                  <div class="form-group">
                     <label>Program Scheme Mode <span class='error-text'>*</span></label>
                     <span id="view_program_scheme">
                          <select class="form-control" id='id_program_has_scheme' name='id_program_has_scheme'>
                            <option value=''></option>
                          </select>

                     </span>
                  </div>
                </div>



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Student <span class='error-text'>*</span></label>
                        <span id="view_student">
                          <select class="form-control" id='id_student' name='id_student'>
                            <option value=''></option>
                          </select>
                        </span>
                    </div>
                </div>


            </div>

            <div class="row">
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Semester <span class='error-text'>*</span></label>
                        <select name="id_semester" id="id_semester" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($semesterList))
                            {
                                foreach ($semesterList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

            </div>

            <!-- <div style="border: 1px solid;border-radius: 12px;">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4">
                        <h4>Student Name : </h4>
                        <h4>Student ID : </h4>
                        <h4>Branch : </h4>
                    </div>
                    <div class="col-sm-4">
                        <h4>Intake : </h4>
                        <h4>Programme : </h4>
                        <h4>Scheme : </h4>
                    </div>
                </div>
            </div>
        </div> -->
        </div>


        <br>


        <div class="form-container" id="view_search_students" style="display: none">
          <h4 class="form-group-title">Search Students</h4>

          <div class="row">

                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Student Name</label>
                          <input type="text" class="form-control" id="name" name="name">
                      </div>
                  </div>


                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>NRIC</label>
                          <input type="text" class="form-control" id="nric" name="nric">
                      </div>
                  </div>


                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Email</label>
                          <input type="text" class="form-control" id="email_id" name="email_id">
                      </div>
                  </div>

              </div>

              <br>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-default" onclick="displayStudentsByIdCourseRegisteredLandscape()">Search</button>
                  </div>

          </div>


        
        <br>
        <div class="custom-table">
              <div id="view"></div>
        </div>
            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="list" class="btn btn-link">Cancel</a>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script type="text/javascript">
    $('select').select2();
</script>
<script type="text/javascript">

    function getProgrammeByEducationLevelId(id_education_level)
    {
      // alert(id_education_level);
      if(id_education_level != '')
        {

            $.get("/research/courseRegistration/getProgrammeByEducationLevelId/"+id_education_level, function(data, status){
           
                $("#view_programme").html(data);
                $("#view_programme").show();
            });
        }

    }

    function getIntakes()
    {

     var tempPR = {};
        tempPR['id_programme'] = $("#id_programme").val();
            $.ajax(
            {
               url: '/research/courseRegistration/getIntakes',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_intake").html(result);
                $("#dummy_intake").hide();


                // if (tempPR['id_programme'] != '' && tempPR['id_intake'] == '')
                // {
                    
                // }
                // var ita = $("#invoice_total_amount").val();
                // $("#receipt_amount").val(ita);
               }
            });


      var tempPR = {};
        tempPR['id_program'] = $("#id_programme").val();
            $.ajax(
            {
               url: '/research/courseRegistration/getLearningModeByProgramId',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_learning_mode").html(result);
                $("#view_learning_mode").show();
                // $("#dummy_learning_mode").hide();


                // if (tempPR['id_programme'] != '' && tempPR['id_intake'] == '')
                // {
                    
                // }
                // var ita = $("#invoice_total_amount").val();
                // $("#receipt_amount").val(ita);
               }
            });


            var id_program = $("#id_programme").val();

            $.get("/research/courseRegistration/getSchemeByProgramId/"+id_program, function(data, status){
                $("#view_program_scheme").html(data);
                $("#view_program_scheme").show();
            });
    }

    function getCourseLandscapeByByProgNIntake()
    {
        var tempPR = {};
        tempPR['id_programme'] = $("#id_programme").val();
        tempPR['id_intake'] = $("#id_intake").val();
        // alert(tempPR['id_programme']);
            $.ajax(
            {
               url: '/research/courseRegistration/getCourseLandscapeByByProgNIntake',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_student").html(result);
                    $("#view_student").show();

                // if (tempPR['id_programme'] != '' && tempPR['id_intake'] == '')
                // {
                    
                // }
                // var ita = $("#invoice_total_amount").val();
                // $("#receipt_amount").val(ita);
               }
            }); 
    }

    function displayStudentData()
    {
        var id_intake = $("#id_intake").val();
        var id_programme = $("#id_programme").val();
        var id_course_registered_landscape = $("#id_course_registered_landscape").val();
        // alert(id_student);

        if(id_intake != '' && id_programme != '' && id_course_registered_landscape != '')
        {
            $("#view_search_students").show();
        }   
    }


    

    function displayStudentsByIdCourseRegisteredLandscape()
    {
        var tempPR = {};
        tempPR['id_programme'] = $("#id_programme").val();
        tempPR['id_intake'] = $("#id_intake").val();
        tempPR['id_course_registered_landscape'] = $("#id_course_registered_landscape").val();
        tempPR['name'] = $("#name").val();
        tempPR['nric'] = $("#nric").val();
        tempPR['email_id'] = $("#email_id").val();

        // alert(id_student);

        if(id_course_registered_landscape != '')
        {

            $.ajax(
            {
               url: '/research/courseRegistration/displayStudentsByIdCourseRegisteredLandscape',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view").html(result);
               }
            });

        }   
    }



    function getStudentByProgNIntake()
    {

      if($("#id_programme").val() != '' && $("#id_intake").val() != '' && $("#id_learning_mode").val() != '' && $("#id_program_has_scheme").val() != '')
      {

        var tempPR = {};
        tempPR['id_programme'] = $("#id_programme").val();
        tempPR['id_intake'] = $("#id_intake").val();
        tempPR['id_learning_mode'] = $("#id_learning_mode").val();
        tempPR['id_program_has_scheme'] = $("#id_program_has_scheme").val();
        tempPR['id_degree_type'] = $("#id_education_level").val();
        // alert(tempPR['id_programme']);
            $.ajax(
            {
               url: '/research/courseRegistration/getStudentByProgNIntake',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_student").html(result);
                    $("#dummy_student").hide();

                // if (tempPR['id_programme'] != '' && tempPR['id_intake'] == '')
                // {
                    
                // }
                // var ita = $("#invoice_total_amount").val();
                // $("#receipt_amount").val(ita);
               }
            });
      }
    }


    
    function displaydata()
    {

      var tempPR = {};
        tempPR['id_programme'] = $("#id_programme").val();
        tempPR['id_intake'] = $("#id_intake").val();
        tempPR['id_learning_mode'] = $("#id_learning_mode").val();
        tempPR['id_program_scheme'] = $("#id_program_has_scheme").val();
        tempPR['id_student'] = $("#id_student").val();
        // alert(id_student);

        if($("#id_student").val() != '' && $("#id_intake").val() && $("#id_student").val() && $("#id_learning_mode").val() && $("#id_program_has_scheme").val())
        {

            $.ajax(
            {
               url: '/research/courseRegistration/displaydata',
                type: 'POST',
               data:
               {
                tempData : tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                
                    $("#view").html(result);
                
               }
            });

        }        
    }
    
</script>
<script>

    $(document).ready(function() {
        $("#form_grade").validate({
            rules: {
                id_education_level: {
                  required: true
                },
                id_programme: {
                    required: true
                },
                id_intake: {
                    required: true
                },
                 id_semester: {
                    required: true
                },
                 id_student: {
                    required: true
                },
                 id_course_registered_landscape: {
                    required: true
                },
                 id_learning_mode: {
                    required: true
                },
                 id_program_has_scheme: {
                    required: true
                }
            },
            messages: {
                id_education_level: {
                    required: "<p class='error-text'>Select Education Level</p>",
                },
                id_programme: {
                    required: "<p class='error-text'>Select Programme</p>",
                },
                id_intake: {
                    required: "<p class='error-text'>Intake Required</p>",
                },
                id_semester: {
                    required: "<p class='error-text'>Semester Required</p>",
                },
                id_student: {
                    required: "<p class='error-text'>Student Required</p>",
                },
                id_course_registered_landscape: {
                    required: "<p class='error-text'>Select Course</p>",
                },
                id_learning_mode: {
                    required: "<p class='error-text'>Select Learning Mode</p>",
                },
                id_program_has_scheme: {
                    required: "<p class='error-text'>Select Program Scheme</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>