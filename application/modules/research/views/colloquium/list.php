<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List Colloquium Setup</h3>
      <a href="add" class="btn btn-primary">+ Add Colloquium</a>
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">
                <div class="row">

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Name</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="name" value="<?php echo $searchParam['name']; ?>">
                      </div>
                    </div>
                  </div>

                </div>

              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href="list" class="btn btn-link" >Clear All Fields</a>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
             <th>Sl. No</th>
            <th>Name</th>
            <th>Start Date</th>
            <th>End Date</th>
            <th>Mode</th>
            <th>Attendance</th>
            <th>Registration Start Date</th>
            <th>Registration End Date</th>
            <th style="text-align: center;">Count</th>
            <th style="text-align: center;">Status</th>
            <th style="text-align: center;">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($colloquiumList)) {
            $i=1;
            foreach ($colloquiumList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                  <td><?php echo $record->name ?></td>
                  <td><?php
                  if($record->date_time)
                  {
                   echo date('d-m-Y', strtotime($record->date_time));
                  } ?></td>
                  <td><?php
                  if($record->to_date)
                  {
                   echo date('d-m-Y', strtotime($record->to_date));
                  }
                   ?></td>
                  <td><?php if( $record->mode == '1')
                  {
                    echo "Face TO Face";
                  }
                  else
                  {
                    echo "Online";
                  } 
                  ?></td>
                  <td><?php if( $record->attendance == '1')
                  {
                    echo "Compulsary";
                  }
                  else
                  {
                    echo "Optional";
                  } 
                  ?></td>
                  <td><?php
                  if($record->registration_start_date)
                  {
                   echo date('d-m-Y', strtotime($record->registration_start_date));
                  } ?></td>
                  <td><?php
                  if($record->registration_end_date)
                  {
                   echo date('d-m-Y', strtotime($record->registration_end_date));
                  }
                   ?></td>
                  <td><?php echo $record->student_count ?></td>
                  <td style="text-align: center;">
                  <?php if( $record->status == '1')
                  {
                    echo "Active";
                  }
                  else
                  {
                    echo "In-Active";
                  } 
                  ?></td>
                  <td class="text-center">
                    <?php
                    if(date('d-m-Y', strtotime($record->to_date)) < date('d-m-Y'))
                    {
                      ?>  
                      <a href="<?php echo 'view/'.$record->id; ?>" title="View">View</a>
                      <?php
                    }
                    else
                    {
                      ?>
                      <a href="<?php echo 'edit/'.$record->id; ?>" title="Edit">Edit</a>
                    <?php
                    }
                    ?>
                  </td>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script>
    $('select').select2();
</script>