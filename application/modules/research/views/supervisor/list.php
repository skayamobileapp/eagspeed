<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List Research-Supervisor</h3>
      <a href="add" class="btn btn-primary">+ Add Supervisor</a>
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">
                <div class="row">

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Name</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="name" value="<?php echo $searchParam['name']; ?>">
                      </div>
                    </div>
                  </div>


                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Type</label>
                      <div class="col-sm-8">
                        <select name="type" id="type" class="form-control">
                          <option value="">Select</option>
                              <option value="0"
                                <?php
                                if ('0' == $searchParam['type'])
                                {
                                  echo 'selected';
                                } ?>
                                >
                                External
                              </option>

                              <option value="1"
                                <?php
                                if ('1' == $searchParam['type'])
                                {
                                  echo 'selected';
                                } ?>
                                >
                                Internal
                              </option>
                        </select>
                      </div>
                    </div>
                  </div>



                </div>

              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href="list" class="btn btn-link" >Clear All Fields</a>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
             <th>Sl. No</th>
            <th>Name</th>
            <th>Type</th>
            <th>Email</th>
            <!-- <th>Specialisation</th> -->
            <th>Start Date</th>
            <th>End Date</th>
            <th>Active Students</th>
            <th style="text-align: center;">Status</th>
            <th style="text-align: center;">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($supervisorList)) {
            $i=1;
            foreach ($supervisorList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                  <td>
                  <?php
                  if($record->type == 1)
                  {
                    echo $record->ic_no . " - " . $record->staff_name;
                  }else
                  {
                    echo $record->full_name;
                  }
                  ?>
                  </td>
                  <td>
                  <?php
                  if($record->type == 1)
                  {
                    echo "Internal";
                  }else
                  {
                    echo "External";
                  }
                  ?>
                  </td>
                  <td><?php echo $record->email; ?></td>
                  <!-- <td><?php echo $record->specialisation; ?></td> -->
                  <td><?php if($record->start_date){ echo date('d-m-Y', strtotime($record->start_date)); } ?></td>
                  <td><?php if($record->end_date){ echo date('d-m-Y', strtotime($record->end_date)); } ?></td>
                  <td><?php echo $record->count; ?></td>
                  <td style="text-align: center;">
                  <?php if( $record->status == '1')
                  {
                    echo "Active";
                  }
                  else
                  {
                    echo "In-Active";
                  } 
                  ?></td>
                  <td class="text-center">
                      <a href="<?php echo 'edit/'.$record->id; ?>" title="Edit">Edit</a>
                      <?php if( $record->count > 0)
                      {
                        ?>
                        | <a href="<?php echo 'viewStudents/'.$record->id; ?>" title="View Students">Students</a>
                      <?php 
                      }
                        ?>
                  </td>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script>
    $('select').select2();
</script>