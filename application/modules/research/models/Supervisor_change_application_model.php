<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Supervisor_change_application_model extends CI_Model
{

    function supervisorChangeApplicationListSearch($data)
    {


        $this->db->select('st.*, rs.full_name as old_supervisor_name, rs.type as old_supervisor_type, rs1.full_name as new_supervisor_name, rs1.type as new_supervisor_type, u.name as updated_by, stu.full_name as student_name, stu.nric');
        $this->db->from('supervisor_change_application as st');
        $this->db->join('research_supervisor as rs','st.id_supervisor_old = rs.id');
        $this->db->join('student as stu','st.id_student = stu.id');
        $this->db->join('research_supervisor as rs1','st.id_supervisor_new = rs1.id','left');
        $this->db->join('users as u','st.updated_by = u.id','left');
        if ($data['name'] != '')
        {
            $likeCriteria = "(stu.full_name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
            // $likeCriteria = "(stu.full_name  LIKE '%" . $data['name'] . "%' or stu.nric  LIKE '%" . $data['name'] . "%')";
        }
        if ($data['nric'] != '')
        {
            $likeCriteria = "(stu.nric  LIKE '%" . $data['nric'] . "%')";
            $this->db->where($likeCriteria);
        }
        // echo "<Pre>"; print_r($data['status']);exit;
        // if($data['status'] != '')
        // {
            $this->db->where('st.status', $data['status']);
        // }

        // if ($data['name'] != '')
        // {
        //     $likeCriteria = "(rd.topic  LIKE '%" . $data['name'] . "%')";
        //     $this->db->where($likeCriteria);
        // }
        // $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


    function getSupervisor($id)
    {
        $this->db->select('s.*');
        $this->db->from('research_supervisor as s');
        $this->db->where('s.id', $id);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getStudentByStudentId($id_student)
    {
        $this->db->select('s.*, p.name as programme_name, i.name as intake_name, qs.name as qualification_name');
        $this->db->from('student as s');
        $this->db->join('programme as p', 's.id_program = p.id'); 
        $this->db->join('intake as i', 's.id_intake = i.id'); 
        $this->db->join('education_level as qs', 's.id_degree_type = qs.id');
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getStudent($id_student)
    {
        $this->db->select('s.*');
        $this->db->from('student as s');
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function addNewChangeSupervisorApplication($data)
    {
        $this->db->trans_start();
        $this->db->insert('supervisor_change_application', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getSupervisorHistoryListByStudentId($id_student)
    {
        $this->db->select('st.*, rs.full_name as supervisor_name, rs.type as supervisor_type, u.name as created_by');
        $this->db->from('supervisor_tagging as st');
        $this->db->join('research_supervisor as rs','st.id_supervisor = rs.id');
        $this->db->join('users as u','st.created_by = u.id');
        $this->db->where('st.id_student', $id_student);
        // if ($data['name'] != '')
        // {
        //     $likeCriteria = "(rd.topic  LIKE '%" . $data['name'] . "%')";
        //     $this->db->where($likeCriteria);
        // }
        // $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


    function getSupervisorChangeApplicationListByStudentId($id_student)
    {
        $this->db->select('st.*, rs.full_name as old_supervisor_name, rs.type as old_supervisor_type, rs1.full_name as new_supervisor_name, rs1.type as new_supervisor_type, u.name as updated_by');
        $this->db->from('supervisor_change_application as st');
        $this->db->join('research_supervisor as rs','st.id_supervisor_old = rs.id');
        $this->db->join('research_supervisor as rs1','st.id_supervisor_new = rs1.id','left');
        $this->db->join('users as u','st.updated_by = u.id','left');
        $this->db->where('st.id_student', $id_student);
        // if ($data['name'] != '')
        // {
        //     $likeCriteria = "(rd.topic  LIKE '%" . $data['name'] . "%')";
        //     $this->db->where($likeCriteria);
        // }
        // $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function getSupervisorChangeApplication($id)
    {
        $this->db->select('*');
        $this->db->from('supervisor_change_application');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    
    function editSupervisorChangeApplication($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('supervisor_change_application', $data);
        return TRUE;
    }

    function supervisorListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('research_supervisor');
        $this->db->where('status', $status);
        $query = $this->db->get();
        $result = $query->result();
        // echo "<pre>";print_r($query);die;
        return $result;
    }

    function addSupervisorTagging($data)
    {
        $this->db->trans_start();
        $this->db->insert('supervisor_tagging', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function updateStudent($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('student', $data);

        return TRUE;
    }
}