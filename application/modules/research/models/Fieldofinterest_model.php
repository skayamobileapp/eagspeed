<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Fieldofinterest_model extends CI_Model
{
    function fieldOfInterestList()
    {
        $this->db->select('*');
        $this->db->from('research_interest');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function fieldOfInterestListSearch($data)
    {
        $this->db->select('*');
        $this->db->from('research_interest');
        if ($data['name'] != '')
        {
            $likeCriteria = "(name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getFieldOfInterest($id)
    {
        $this->db->select('*');
        $this->db->from('research_interest');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewFieldOfInterest($data)
    {
        $this->db->trans_start();
        $this->db->insert('research_interest', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editFieldOfInterest($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('research_interest', $data);
        return TRUE;
    }
}

