<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Research_category_model extends CI_Model
{
    function researchCategoryList()
    {
        $this->db->select('i.*');
        $this->db->from('research_topic as i');
        $this->db->order_by("i.name", "DESC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function researchCategoryListByStatus($status)
    {
        $this->db->select('i.*');
        $this->db->from('research_topic as i');
        $this->db->where("i.status", $status);
        $this->db->order_by("i.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function programListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where("status", $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function staffListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('staff');
        $this->db->where("status", $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }


    function researchCategoryListSearch($data)
    {
        $this->db->select('i.*, p.code as program_code, p.name as program_name');
        $this->db->from('research_topic_category as i');
        $this->db->join('programme as p', 'i.id_program = p.id');
        if($data['name'] != '')
        {
            $likeCriteria = "(i.name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if($data['id_program'] != '')
        {
            $this->db->where("i.id_program", $data['id_program']);
        }
        $this->db->order_by("i.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getResearchCategory($id)
    {
        $this->db->select('i.*');
        $this->db->from('research_topic_category as i');
        $this->db->where('i.id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewResearchCategory($data)
    {
        $this->db->trans_start();
        $this->db->insert('research_topic_category', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }


    function tempAddResearchCategoryHasSupervisor($data)
    {
        $this->db->trans_start();
        $this->db->insert('temp_research_category_has_supervisors', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getTempResearchCategoryHasSupervisorBySession($id_session)
    {
        $this->db->select('tihp.*, p.ic_no, p.name as staff_name');
        $this->db->from('temp_research_category_has_supervisors as tihp');
        $this->db->join('staff as p', 'tihp.id_staff = p.id');
        $this->db->where('tihp.id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }

    function deleteTempResearchCategoryHasSupervisor($id)
    {
       $this->db->where('id', $id);
       $this->db->delete('temp_research_category_has_supervisors');
       return TRUE;
    }

    function moveTempToDetails($id_topic)
    {
        $id_session = $this->session->my_session_id;

        $temp_details = $this->getTempResearchCategoryHasSupervisor($id_session);

        foreach ($temp_details as $result)
        {
            unset($result->id);
            unset($result->id_session);
            $result->id_category = $id_topic;
            $this->addNewResearchCategoryHasSupervisors($result);
        }

        $deleted = $this->deleteTempResearchCategoryHasSupervisorBySessionId($id_session);
        return $deleted;
    }

    function getTempResearchCategoryHasSupervisor($id_session)
    {
        $this->db->select('tihp.*');
        $this->db->from('temp_research_category_has_supervisors as tihp');    
        $this->db->where('tihp.id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }

    function addNewResearchCategoryHasSupervisors($data)
    {
        $this->db->trans_start();
        $this->db->insert('research_category_has_supervisors', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function deleteTempResearchCategoryHasSupervisorBySessionId($id_session)
    {
       $this->db->where('id_session', $id_session);
       $this->db->delete('temp_research_category_has_supervisors');
       return TRUE;
    }

    function getResearchCategoryHasSupervisor($id)
    {
        $this->db->select('tihp.*, p.name as staff_name, p.ic_no');
        $this->db->from('research_category_has_supervisors as tihp');
        $this->db->join('staff as p', 'tihp.id_staff = p.id');     
        $this->db->where('tihp.id_category', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function editResearchCategoryDetails($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('research_topic_category', $data);
        return TRUE;
    }

     function deleteResearchCategoryHasSupervisor($id)
    {
        $this->db->where('id', $id);
       $this->db->delete('research_category_has_supervisors');
       return TRUE;
    }
}