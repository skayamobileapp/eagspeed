<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Reason_applying_model extends CI_Model
{
    function reasonApplyingList()
    {
        $this->db->select('*');
        $this->db->from('research_reason_applying');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function reasonApplyingListSearch($data)
    {
        $this->db->select('*');
        $this->db->from('research_reason_applying');
        if ($data['name'] != '')
        {
            $likeCriteria = "(name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getReasonApplying($id) {
        $this->db->select('*');
        $this->db->from('research_reason_applying');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewReasonApplying($data)
    {
        $this->db->trans_start();
        $this->db->insert('research_reason_applying', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editReasonApplying($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('research_reason_applying', $data);
        return TRUE;
    }
}

