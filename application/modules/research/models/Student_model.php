<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Student_model extends CI_Model
{

    function applicantListForApproval($data)
    {
        $this->db->select('a.*, in.year as intake_year,in.name as intake, p.name as program, p.code as program_code, el.name as education_level, pt.code as program_structure_code, pt.name as program_structure_name, train.name as training_center_name, train.code as training_center_code');
        $this->db->from('applicant as a');
        $this->db->join('intake as in', 'a.id_intake = in.id');
        $this->db->join('programme as p', 'a.id_program = p.id');
        $this->db->join('education_level as el', 'a.id_degree_type = el.id');
        $this->db->join('program_type as pt', 'a.id_program_structure_type = pt.id');
        $this->db->join('organisation_has_training_center as train', 'a.id_branch = train.id');
        
        if($data['applicant_id'] != '')
        {
            $likeCriteria = "(a.full_name  LIKE '%" . $data['applicant_id'] . "%')";
            $this->db->where($likeCriteria);
        }
        if($data['first_name'] != '')
        {
            $likeCriteria = "(a.full_name  LIKE '%" . $data['first_name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if($data['email_id'] != '')
        {
            $likeCriteria = "(a.email_id  LIKE '%" . $data['email_id'] . "%')";
            $this->db->where($likeCriteria);
        }
        if($data['nric'] != '')
        {
            $likeCriteria = "(a.nric  LIKE '%" . $data['nric'] . "%')";
            $this->db->where($likeCriteria);
        }
        if($data['id_program'] != '')
        {
            $likeCriteria = "(a.id_program  LIKE '%" . $data['id_program'] . "%')";
            $this->db->where($likeCriteria);
        }
        if($data['id_intake'] != '')
        {
            $likeCriteria = "(a.id_intake  LIKE '%" . $data['id_intake'] . "%')";
            $this->db->where($likeCriteria);
        }
        $likeCriteria = "(a.is_sibbling_discount  != '0' and a.is_employee_discount  != '0')";
        $this->db->where('a.applicant_status', 'Approved');
        $this->db->where($likeCriteria);
        $this->db->where('a.is_updated', '1');
        $this->db->where('a.email_verified', '1');
        $this->db->where('a.is_submitted', '1');
        $this->db->where('el.name', 'POSTGRADUATE');
         $query = $this->db->get();
         $result = $query->result();
         // print_r($applicantList);exit();     
         return $result;
    }

   function applicantList($data)
    {
        $this->db->select('a.*, in.code as intake_code,in.name as intake, p.name as program, p.code as program_code');
        $this->db->from('student as a');
        $this->db->join('intake as in', 'a.id_intake = in.id');
        $this->db->join('programme as p', 'a.id_program = p.id');

        if($data['first_name']) {
            $likeCriteria = "(a.first_name  LIKE '%" . $data['first_name'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($data['last_name']) {
            $likeCriteria = "(a.last_name  LIKE '%" . $data['last_name'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($data['email']) {
            $likeCriteria = "(a.email  LIKE '%" . $data['email'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($data['nric']) {
            $likeCriteria = "(a.nric  LIKE '%" . $data['nric'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($data['id_program']) {
            $likeCriteria = "(a.id_program  LIKE '%" . $data['id_program'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($data['id_intake']) {
            $likeCriteria = "(a.id_intake  LIKE '%" . $data['id_intake'] . "%')";
            $this->db->where($likeCriteria);
        }
        if($data['applicant_status']) {
            $likeCriteria = "(a.applicant_status  LIKE '%" . $data['applicant_status'] . "%')";
            $this->db->where($likeCriteria);
        }
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function addExamDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('examination_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addProficiencyDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('english_proficiency_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addEmploymentDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('employment_status', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addProfileDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('profile_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addVisaDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('visa_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addOtherDocuments($data)
    {
        $this->db->trans_start();
        $this->db->insert('other_documents', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getExamDetails($id)
    {
        $this->db->select('*');
        $this->db->from('examination_details');
        $this->db->where('id_student', $id);
        $query = $this->db->get();
         $result = $query->result();
        return $result;
    }

    function getProficiencyDetails($id)
    {
        $this->db->select('*');
        $this->db->from('english_proficiency_details');
        $this->db->where('id_student', $id);
        $query = $this->db->get();
         $result = $query->result();
        return $result;
    }

    function getEmploymentDetails($id)
    {
        $this->db->select('*');
        $this->db->from('employment_status');
        $this->db->where('id_student', $id);
        $query = $this->db->get();
         $result = $query->result();
        return $result;
    }

    function getfeeStructureByProgramIntake($id_intake,$id_program,$id_program_scheme,$currency)
    {
        
        $this->db->select('fs.*, fc.code as fee_code, fc.name as fee_name');
        $this->db->from('fee_structure as fs');
        $this->db->join('fee_setup as fc', 'fs.id_fee_item = fc.id');
        $this->db->where('fs.id_intake', $id_intake);
        $this->db->where('fs.id_programme', $id_program);
        $this->db->where('fs.id_program_scheme', $id_program_scheme);
        $this->db->where('fs.currency', $currency);
        $query = $this->db->get();
         $result = $query->result();
        return $result;
    }

    function getProfileDetails($id)
    {
        $this->db->select('*');
        $this->db->from('profile_details');
        $this->db->where('id_student', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getVisaDetails($id)
    {
        $this->db->select('*');
        $this->db->from('visa_details');
        $this->db->where('id_student', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getOtherDocuments($id)
    {
        $this->db->select('*');
        $this->db->from('other_documents');
        $this->db->where('id_student', $id);
        $this->db->limit('0,1');
        $query = $this->db->get();
         $result = $query->result();
        return $result;
    }

    function intakeList()
    {
        $this->db->select('*');
        $this->db->from('intake');
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function nationalityList()
    {
        $this->db->select('*');
        $this->db->from('nationality');
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function programList()
    {
        $this->db->select('*');
        $this->db->from('programme');
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function studentList()
    {
        $this->db->select('*');
        $this->db->from('student');
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    // function addNewStudent()
    // {
    //     $query = $this->db->get('applicant');
    //     foreach ($query->result() as $row) {
    //           $this->db->insert('student',$row);
    //     }
    // }


    function programStructureTypeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('program_type');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result; 
    }

    function getStudentDetails($id)
    {
        $this->db->select('a.*, in.name as intake, p.name as program, n.name as nation');
        $this->db->from('student as a');
        $this->db->join('intake as in', 'a.id_intake = in.id');
        $this->db->join('programme as p', 'a.id_program = p.id');
        $this->db->join('nationality as n', 'a.id_nationality = n.id');
        $this->db->where('a.id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function deleteExamDetails($id)
    {
        $this->db->where('id', $id);
       $this->db->delete('examination_details');
         return TRUE;
    }

    function deleteProficiencyDetails($id)
    {
        $this->db->where('id', $id);
       $this->db->delete('english_proficiency_details');
         return TRUE;
    }

    function deleteEmploymentDetails($id)
    {
        $this->db->where('id', $id);
       $this->db->delete('employment_status');
         return TRUE;
    }

    function deleteOtherDocument($id)
    {
        $this->db->where('id', $id);
       $this->db->delete('other_documents');
         return TRUE;
    }

    function getStudentByStudentId($id_student)
    {
        $this->db->select('s.*');
        $this->db->from('applicant as s');
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getInvoiceByStudentId($id_applicant)
    {
        // $type = "Student";
        // $this->db->select('mi.*, s.full_name as student, s.email_id, s.nric');
        // $this->db->from('main_invoice as mi');
        // $this->db->join('student as s', 'mi.id_student = s.id');
        // $this->db->where('mi.id_student', $id_student);
        // $this->db->where('mi.type', $type);
        // $query = $this->db->get();
        //  $result = $query->result();

        // return$result;

        $student = "Student";
        $applicant = "Applicant";

        $this->db->select('mi.*');
        $this->db->from('main_invoice as mi');
        // $this->db->join('student as s', 'mi.id_student = s.id');
        // $this->db->where('mi.id_student', $id_student);
        // $this->db->where('mi.type', 'Student');
        $likeCriteria = "(id_student  ='" . $id_applicant . "' and type  ='" . $applicant . "')";
        // echo "<Pre>";print_r($likeCriteria);exit();
        $this->db->where($likeCriteria);


        $query = $this->db->get();
         $result = $query->result();

        return$result;
    }

    function getReceiptByStudentId($id_applicant)
    {
        $student = "Student";
        $applicant = "Applicant";

        // $this->db->select('r.*, s.full_name as student, s.email_id, s.nric');
        // $this->db->from('receipt as r');
        // $this->db->join('student as s', 'r.id_student = s.id');
        // $this->db->where('r.id_student', $id_student);
        // $this->db->where('r.type', $type);
        // $query = $this->db->get();
        //  $result = $query->result();

        // return$result;

        $this->db->select('mi.*');
        $this->db->from('receipt as mi');
        // $this->db->join('student as s', 'mi.id_student = s.id');
        // $this->db->where('mi.id_student', $id_student);
        // $this->db->where('mi.type', 'Student');
        $likeCriteria = "(id_student  ='" . $id_applicant . "' and type  ='" . $applicant . "')";
        // echo "<Pre>";print_r($likeCriteria);exit();
        $this->db->where($likeCriteria);


        $query = $this->db->get();
         $result = $query->result();

        return$result;
    }

    function getProgramLandscapeByIntakeNProgram($data)
    {
        $this->db->select('pl.*, p.name as programme, i.name as intake, phs.mode_of_program, mode_of_study, sch.code as scheme_code, sch.description as scheme_name');
        $this->db->from('programme_landscape as pl');
        $this->db->join('programme as p', 'pl.id_programme = p.id');
        $this->db->join('intake as i', 'pl.id_intake = i.id');
        $this->db->join('programme_has_scheme as phs', 'pl.learning_mode = phs.id','left');
        $this->db->join('scheme as sch', 'pl.program_scheme = sch.id');
        $this->db->where('pl.id_programme', $data['id_program']);
        $this->db->where('pl.id_intake', $data['id_intake']);
        // $this->db->where('pl.learning_mode', $data['id_program_scheme']);
        $this->db->where('pl.program_scheme', $data['id_program_has_scheme']);
        $query = $this->db->get();
        return $query->row();
    }

    function getMainInvoice($id)
    {
        $this->db->select('mi.*, p.name as programme_name, p.code as programme_code, i.name as intake_name');
        $this->db->from('main_invoice as mi');
        // $this->db->join('student as s', 'mi.id_student = s.id');
        $this->db->join('programme as p', 'mi.id_program = p.id');
        $this->db->join('intake as i', 'mi.id_intake = i.id');
        $this->db->where('mi.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getMainInvoiceDetails($id)
    {
        $this->db->select('mid.*, fstp.name as fee_setup');
        $this->db->from('main_invoice_details as mid');
        // $this->db->join('fee_structure as fs', 'mid.id_fee_item = fs.id');        
        $this->db->join('fee_setup as fstp', 'mid.id_fee_item = fstp.id');        
        $this->db->where('mid.id_main_invoice', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function getMainInvoiceDiscountDetails($id)
    {
        $this->db->select('*');
        $this->db->from('main_invoice_discount_details');   
        $this->db->where('id_main_invoice', $id);
        $query = $this->db->get();
        return $query->result();
    }


    function getReceipt($id)
    {
        $this->db->select('re.*, p.name as programme_name, p.code as programme_code, i.name as intake_name');
        $this->db->from('receipt as re');
        // $this->db->join('student as s', 're.id_student = s.id');
        $this->db->join('programme as p', 're.id_program = p.id');
        $this->db->join('intake as i', 're.id_intake = i.id');
        $this->db->where('re.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getReceiptInvoiceDetails($id)
    {
        $this->db->select('r.*,mi.*, r.paid_amount');
        $this->db->from('receipt_details as r');
        $this->db->join('main_invoice as mi', 'mi.id = r.id_main_invoice');
        $this->db->where('r.id_receipt', $id);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getReceiptPaymentDetails($id)
    {
        $this->db->select('r.*');
        $this->db->from('receipt_paid_details as r');
        $this->db->where('r.id_receipt', $id);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


    function countryListByStatus($status)
    {
        $this->db->select('r.*');
        $this->db->from('country as r');
        $this->db->where('r.status', $status);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function stateListByStatus($status)
    {
        $this->db->select('r.*');
        $this->db->from('state as r');
        $this->db->where('r.status', $status);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function raceListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('race_setup');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }


    function religionListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('religion_setup');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function salutationListByStatus($status)
    {
        $this->db->select('a.*');
        $this->db->from('salutation_setup as a');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function qualificationListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('qualification_setup');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function programTypeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('program_type');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function branchListByStatus()
    {
        $organisation = $this->getOrganisaton();
        $details = array();

        if($organisation)
        {
            array_push($details, $organisation);
        }


        $this->db->select('a.*');
        $this->db->from('organisation_has_training_center as a');
        $query = $this->db->get();
        $result = $query->result();  

        foreach ($result as $value)
        {
           array_push($details, $value);
        }
        return $details;
    }

    function getOrganisaton()
    {
        $this->db->select('a.*, a.short_name as code');
        $this->db->from('organisation as a');
        $query = $this->db->get();
        $result = $query->row();  
        return $result;
    }

    function programRequiremntList()
    {
        $this->db->select('ier.*, qs.code as qualification_code, qs.name as qualification_name, ws.code as work_code, ws.name as work_name');
        $this->db->from('scholarship_individual_entry_requirement as ier');
        $this->db->join('education_level as qs','ier.id_education_qualification = qs.id','left');
        $this->db->join('scholarship_work_specialisation as ws','ier.id_work_specialisation = ws.id','left');
        $query = $this->db->get();
        return $query->result();
    }

    function getProgramDetails($id_program)
    {
        $this->db->select('ihs.*');
        $this->db->from('programme as ihs');
        $this->db->where('ihs.id', $id_program);
        $query = $this->db->get();
        return $query->row();
    }

    function programEntryRequirementList($id_program)
    {
        $this->db->select('ier.*, qs.code as qualification_code, qs.name as qualification_name, ws.code as work_code, ws.name as work_name');
        $this->db->from('scholarship_individual_entry_requirement as ier');
        $this->db->join('education_level as qs','ier.id_education_qualification = qs.id','left');
        $this->db->join('scholarship_work_specialisation as ws','ier.id_work_specialisation = ws.id','left');
        $this->db->where('ier.id_program', $id_program);
        $query = $this->db->get();
        return $query->result();
    }

    function getApplicantUploadedFiles($id_applicant)
    {
        $this->db->select('shd.*, d.code as document_code, d.name as document_name');
        $this->db->from('applicant_has_document as shd');
        $this->db->join('documents as d','shd.id_document = d.id');
        $this->db->where('shd.id_applicant', $id_applicant);
        $query = $this->db->get();
        return $query->result();
    }

    function schemeListByStatus($status)
    {
        $this->db->select('ihs.*');
        $this->db->from('scheme as ihs');
        $this->db->where('ihs.status', $status);
        $query = $this->db->get();
        return $query->result();
    }

    function getUniversityListByStatus($status)
    {
        $organisation = $this->getOrganisaton();
        $details = array();

        if($organisation)
        {
            array_push($details, $organisation);
        }

        $this->db->select('ahemd.*');
        $this->db->from('partner_university as ahemd');
        $this->db->where('ahemd.status', $status);
        $query = $this->db->get();
        $results = $query->result();

        foreach ($results as $result)
        {
            array_push($details, $result);
        }
        return $details;
    }

    function getFeeStructureForStudentsForLandscape($id_programme,$id_intake,$id_program_landscape,$currency)
    {
       $this->db->select('p.*, fs.name as fee_structure, fs.code as fee_structure_code, fm.name as frequency_mode');
        $this->db->from('fee_structure as p');
        $this->db->join('fee_setup as fs', 'p.id_fee_item = fs.id'); 
        $this->db->join('frequency_mode as fm', 'fs.id_frequency_mode = fm.id'); 
        $this->db->where('p.id_programme', $id_programme);
        $this->db->where('p.id_intake', $id_intake);
        $this->db->where('p.id_program_landscape', $id_program_landscape);
        $this->db->where('p.currency', $currency);
        // $this->db->where('p.id_program_scheme', $id_program_scheme);
        // $this->db->where('p.id_training_center', 0);
        // $this->db->where('fm.code', 'ONE TIME');
        // $this->db->or_where('fm.code', 'APPLICATION');
        $query = $this->db->get();
        $fee_structure = $query->result();
        // $detail_data = $this->getFeeStructureDetails($fee_structure->id);
        return $fee_structure;
    }

    function getFeestructureByPartnerUniversity($id_program_landscape,$id_university)
    {
        $this->db->select('p.*, fs.name as fee_structure, fs.code as fee_structure_code, fm.name as frequency_mode, cs.name as currency');
        $this->db->from('fee_structure as p');
        $this->db->join('fee_setup as fs', 'p.id_fee_item = fs.id','left');
        $this->db->join('frequency_mode as fm', 'fs.id_frequency_mode = fm.id','left');
        $this->db->join('currency_setup as cs', 'p.currency = cs.id','left');
        $this->db->where('p.id_training_center', $id_university);
        $this->db->where('p.id_program_landscape', $id_program_landscape);
        $query = $this->db->get();
        $fee_structure = $query->row();
        // $detail_data = $this->getFeeStructureDetails($fee_structure->id);
        return $fee_structure;
    }

    function getFeeStructureInstallmentByData($id_programme_landscape,$id_training_center,$id_fee_structure)
    {
        $this->db->select('p.*, cs.name as currency');
        $this->db->from('fee_structure_has_training_center as p');
        $this->db->join('fee_structure as fs', 'p.id_fee_structure = fs.id'); 
        $this->db->join('currency_setup as cs', 'fs.currency = cs.id'); 
        $this->db->where('p.id_program_landscape', $id_programme_landscape);
        $this->db->where('p.id_training_center', $id_training_center);
        $this->db->where('p.id_semester', $current_semester);
        $this->db->order_by('p.id', 'DESC');
        $query = $this->db->get();
        $fee_structure = $query->row();
        return $fee_structure;
    }

    function programListForPostgraduate($name)
    {
        $this->db->select('p.*');
        $this->db->from('programme as p');
        $this->db->join('education_level as el', 'p.id_education_level = el.id');
        $this->db->where('el.name', $name);
        $this->db->where('p.status', '1');
        $this->db->order_by("p.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function getApplicantDetailsById($id)
    {
        $this->db->select('*');
        $this->db->from('applicant');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $sd = $query->row();
        return $sd;
    }

    function getApplicantSibblingDiscountDetails($id_applicant)
    {
        $this->db->select('ahsd.*, usr.name as user_name');
        $this->db->from('applicant_has_sibbling_discount as ahsd');
        $this->db->join('tbl_users as usr', 'ahsd.rejected_by = usr.userId','left'); 
        $this->db->where('id_applicant', $id_applicant);
        $query = $this->db->get();
        return $query->row();
    }

    function getApplicantEmployeeDiscountDetails($id_applicant)
    {
        $this->db->select('ahemd.*, usr.name as user_name');
        $this->db->from('applicant_has_employee_discount as ahemd');
        $this->db->join('tbl_users as usr', 'ahemd.rejected_by = usr.userId','left'); 
        $this->db->where('id_applicant', $id_applicant);
        $query = $this->db->get();
        return $query->row();
    }

    function getApplicantAlumniDiscountDetails($id_applicant)
    {
        $this->db->select('ahemd.*, usr.name as user_name');
        $this->db->from('applicant_has_alumni_discount as ahemd');
        $this->db->join('tbl_users as usr', 'ahemd.rejected_by = usr.userId','left'); 
        $this->db->where('id_applicant', $id_applicant);
        $query = $this->db->get();
        return $query->row();
    }   

    function addNewStudent($id)
    {
        $query = $this->db->select('*')->from('applicant')->where('id',$id)->get();
        foreach ($query->result() as $row)
        {
            // echo "<Pre>";print_r($row);exit();
            unset($row->id);
            unset($row->is_sibbling_discount);
            unset($row->is_employee_discount);
            unset($row->is_alumni_discount);
            unset($row->is_apeal_applied);
            unset($row->id_apeal_status);
            unset($row->approved_by);
            unset($row->email_verified);
            unset($row->is_updated);
            unset($row->is_submitted);
            unset($row->submitted_date);
            unset($row->apel_reject_reason);
            unset($row->created_dt_tm);
            
            
            
            $row->status = '1';
            $row->current_semester = '1';
            $row->applicant_status = 'Approved';
            $row->id_applicant = $id;
            $row->phd_duration = 1;
            $row->current_deliverable = date('M-Y');

            $this->db->insert('student',$row);
            $insert_id = $this->db->insert_id();
        }

        $this->db->trans_complete();
        return $insert_id;
    }

    function editApplicantDetails($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('applicant', $data);
        return TRUE;
    }

    function addStudentProfileDetail($id)
    {
        $data = ['id_student'=>$id];
          $this->db->insert('profile_details', $data);
          // $this->db->insert('visa_details', $data);
    }
}