<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Status_model extends CI_Model
{
    function statusList()
    {
        $this->db->select('*');
        $this->db->from('research_status');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function statusListSearch($data)
    {
        // , p.code as program_code, p.name as program_name, c.code as course_code, c.name as course_name
        $this->db->select('rs.*');
        $this->db->from('research_status as rs');
        // $this->db->join('programme as p', 'rs.id_program = p.id');
        // $this->db->join('course as c', 'rs.id_course = c.id');
        if ($data['name'])
        {
            $likeCriteria = "(rs.code  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        // if ($data['id_program'])
        // {
        //     $this->db->where('rs.id_program', $data['id_program']);
        // }
        // if ($data['id_course'])
        // {
        //     $this->db->where('rs.id_course', $data['id_course']);
        // }
        $this->db->order_by("rs.code", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getStatus($id)
    {
        $this->db->select('*');
        $this->db->from('research_status');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewStatus($data)
    {
        $this->db->trans_start();
        $this->db->insert('research_status', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editStatus($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('research_status', $data);
        return TRUE;
    }

    function programListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', $status);
        $query = $this->db->get();
        return $query->result();
    }

    function courseListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('course');
        $this->db->where('status', $status);
        $query = $this->db->get();
        return $query->result();
    }
}

