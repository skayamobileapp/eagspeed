<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Colloquium_model extends CI_Model
{
    function colloquiumList()
    {
        $this->db->select('*');
        $this->db->from('research_colloquium');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


    function colloquiumListSearch($data)
    {
        $this->db->select('*');
        $this->db->from('research_colloquium');
        if ($data['name'] != '')
        {
            $likeCriteria = "(name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $results = $query->result();
        
        
            $details = array();

            foreach ($results as $result)
            {
                $count = 0;
                $id_colloquium = $result->id;

                $student_count = $this->getColloquiumStudentInterestedCount($id_colloquium);

                if($student_count > 0)
                {
                    $count  = $student_count;
                }

                $result->student_count = $count;


                if($data['attendence'] == 1)
                {
                    $to_date = $result->to_date;

                    if(date('d-m-Y', strtotime($to_date)) < date('d-m-Y'))
                    {
                        array_push($details, $result);
                    }
                }
                else
                {
                    array_push($details, $result);
                }
            }
            return $details;
         // return $results;
    }


    function getColloquium($id)
    {
        $this->db->select('*');
        $this->db->from('research_colloquium');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewColloquium($data)
    {
        $this->db->trans_start();
        $this->db->insert('research_colloquium', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editColloquium($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('research_colloquium', $data);
        return TRUE;
    }


    function programListForPostgraduate($name)
    {
        $this->db->select('p.*');
        $this->db->from('programme as p');
        $this->db->join('education_level as el', 'p.id_education_level = el.id');
        $this->db->where('el.name', $name);
        $this->db->where('p.status', '1');
        $this->db->order_by("p.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function getIntakeListByProgramme($id_programme)
    {
        $this->db->select('DISTINCT(ihp.id_intake) as id_intake, ihp.*, in.name as intake_name, in.year as intake_year');
        $this->db->from('intake_has_programme as ihp');
        $this->db->join('intake as in', 'ihp.id_intake = in.id');
        $this->db->where('ihp.id_programme', $id_programme);
        $query = $this->db->get();
         return $query->result();
    }

    function tempAddColloquiumDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('temp_research_colloquium_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getTempColloquiumDetailsBySession($id_session)
    {
        $this->db->select('trqd.*, i.name as intake_name, i.year as intake_year, p.name as programme_name, p.code as programme_code');
        $this->db->from('temp_research_colloquium_details as trqd');
        $this->db->join('intake as i', 'trqd.id_intake = i.id');
        $this->db->join('programme as p', 'trqd.id_programme = p.id');
        $this->db->where('trqd.id_session', $id_session);
        $query = $this->db->get();
         return $query->result();
    }

    function deleteTempColloquiumDetails($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('temp_research_colloquium_details');
        return TRUE;
    }


    function addColloquiumDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('research_colloquium_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getColloquiumDetailsByColloquiumId($id_colloquium)
    {
        $this->db->select('trqd.*, i.name as intake_name, i.year as intake_year, p.name as programme_name, p.code as programme_code');
        $this->db->from('research_colloquium_details as trqd');
        $this->db->join('intake as i', 'trqd.id_intake = i.id');
        $this->db->join('programme as p', 'trqd.id_programme = p.id');
        $this->db->where('trqd.id_colloquium', $id_colloquium);
        $query = $this->db->get();
         return $query->result();
    }

    function deleteColloquiumDetails($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('research_colloquium_details');
        return TRUE;
    }

    function moveTempToDetails($id_colloquium)
    {
        $id_session = $this->session->my_session_id;

        $temp_details = $this->getTempColloquiumDetailsBySessionToMove($id_session);


        foreach ($temp_details as $detail)
        {
            unset($detail->id_session);
            unset($detail->id);
            
            $detail->id_colloquium = $id_colloquium;

            $added_details_id = $this->addColloquiumDetails($detail);
        }

        $deleted_temp_data = $this->deleteTempColloquiumDetailsBySessionId($id_session);
    }

    function getTempColloquiumDetailsBySessionToMove($id_session)
    {
        $this->db->select('trqd.*');
        $this->db->from('temp_research_colloquium_details as trqd');
        $this->db->join('intake as i', 'trqd.id_intake = i.id');
        $this->db->join('programme as p', 'trqd.id_programme = p.id');
        $this->db->where('trqd.id_session', $id_session);
        $query = $this->db->get();
         return $query->result();
    }

    function deleteTempColloquiumDetailsBySessionId($id_session)
    {
        $this->db->where('id_session', $id_session);
        $this->db->delete('temp_research_colloquium_details');
        return TRUE;
    }

    function getColloquiumInterestedStudents($id_colloquium)
    {
        $this->db->select('trqd.*, s.full_name as student_name, s.nric, s.email_id, p.code as programme_code, p.name as programme_name, i.name as intake_name, i.year as intake_year');
        $this->db->from('research_colloquium_student_interest as trqd');
        $this->db->join('student as s', 'trqd.id_student = s.id');
        $this->db->join('intake as i', 's.id_intake = i.id');
        $this->db->join('programme as p', 's.id_program = p.id');
        $this->db->where('trqd.id_colloquium', $id_colloquium);
        $query = $this->db->get();
         return $query->result();
    }

    function getColloquiumStudentInterestedCount($id_colloquium)
    {
        $this->db->select('*');
        $this->db->from('research_colloquium_student_interest as trqd');
        $this->db->join('student as s', 'trqd.id_student = s.id');
        $this->db->where('trqd.id_colloquium', $id_colloquium);
        $query = $this->db->get();
        $result = $query->num_rows();

        return $result;
    }

    function searchStudentsByData($data)
    {
        $this->db->select('s.*, p.name as program_name, p.code as program_code, i.name as intake_name');
        $this->db->from('student as s');
        $this->db->join('education_level as el', 's.id_degree_type = el.id');
        $this->db->join('intake as i', 's.id_intake = i.id');
        $this->db->join('programme as p', 's.id_program = p.id');
        // $this->db->where('a.id_course_registered_landscape', $data['id_course_registered_landscape']);
        // $this->db->where('a.is_exam_registered !=', 0);
        // $this->db->where('a.is_bulk_withdraw', 0);
        $this->db->where('el.name', 'POSTGRADUATE');

        // if($data['name'])
        // {
        //     $likeCriteria = "(s.full_name  LIKE '%" . $data['name'] . "%')";
        //     $this->db->where($likeCriteria);
        // }
        // if($data['nric'])
        // {
        //     $likeCriteria = "(s.nric  LIKE '%" . $data['nric'] . "%')";
        //     $this->db->where($likeCriteria);
        // }
        // if($data['email_id'])
        // {
        //     $likeCriteria = "(s.email_id  LIKE '%" . $data['email_id'] . "%')";
        //     $this->db->where($likeCriteria);
        // }

        $likeCriteria = "s.id NOT IN (SELECT a.id_student FROM research_colloquium_student_interest as a where a.id_colloquium = " . $data['id_colloquium'] . ")";
        $this->db->where($likeCriteria);  

         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();
         return $result;
    }

    function addColloquiumInterest($data)
    {
        $this->db->trans_start();
        $this->db->insert('research_colloquium_student_interest', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }
}