<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Examiner_role_model extends CI_Model
{
    function examinerRoleList()
    {
        $this->db->select('*');
        $this->db->from('examiner_role');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function examinerRoleListSearch($data)
    {
        $this->db->select('*');
        $this->db->from('examiner_role');
        if ($data['name'])
        {
            $likeCriteria = "(code  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("code", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getExaminerRole($id)
    {
        $this->db->select('*');
        $this->db->from('examiner_role');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewExaminerRole($data)
    {
        $this->db->trans_start();
        $this->db->insert('examiner_role', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editExaminerRole($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('examiner_role', $data);
        return TRUE;
    }
}

