<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Student_semester_model extends CI_Model
{

     function intakeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('intake');
        $this->db->where('status', $status);
        $query = $this->db->get();
        $result = $query->result();
        // echo "<pre>";print_r($query);die;
        return $result;
    }

    function programListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', $status);
        $query = $this->db->get();
        $result = $query->result();
        // echo "<pre>";print_r($query);die;
        return $result;
    }


    function semesterListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('semester');
        $this->db->where('status', $status);
        $query = $this->db->get();
        $result = $query->result();
        // echo "<pre>";print_r($query);die;
        return $result;
    }


    function staffListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('research_supervisor');
        $this->db->where('status', $status);
        $query = $this->db->get();
        $result = $query->result();
        // echo "<pre>";print_r($query);die;
        return $result;
    }

    function qualificationListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('qualification_setup');
        $this->db->where('status', $status);
        $query = $this->db->get();
        $result = $query->result();
        // echo "<pre>";print_r($query);die;
        return $result;
    }

    function studentSearch($data)
    {
        $this->db->select('s.*, p.code as program_code, p.name as program_name, qs.short_name as qualification_code, qs.name as qualification_name, adt.type , adt.full_name as advisor_name, i.year as intake_year, i.name as intake_name');
        $this->db->from('student as s');
        $this->db->join('programme as p', 's.id_program = p.id');
        $this->db->join('education_level as qs', 's.id_degree_type = qs.id');
        $this->db->join('intake as i', 's.id_intake = i.id');
        $this->db->join('research_supervisor as adt', 's.id_supervisor = adt.id','left');
        if ($data['full_name'] != '')
        {
            $likeCriteria = "(s.full_name  LIKE '%" . $data['full_name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['email_id'] != '')
        {
            $this->db->where('s.email_id', $data['email_id']);
        }
        if ($data['current_semester'] != '')
        {
            $this->db->where('s.current_semester', $data['current_semester']);
        }
        if ($data['id_program'] != '')
        {
            $this->db->where('s.id_program', $data['id_program']);
        }
        if ($data['id_intake'] != '')
        {
            $this->db->where('s.id_intake', $data['id_intake']);
        }
        // if ($data['id_qualification'] != '')
        // {
        //     $this->db->where('s.id_degree_type', $data['id_qualification']);
        // }
        // if($data['tagging_status'] != '')
        // {
        //     if($data['tagging_status'] == 1)
        //     {
        //         $this->db->where('s.id_advisor !=','0');
        //     }
        //     elseif($data['tagging_status'] == 0)
        //     {
        //         $this->db->where('s.id_advisor',$data['tagging_status']);
        //     }
        // }
        // if ($data['id_semester'] != '')
        // {
        //     $this->db->where('s.id_semester', $data['id_semester']);
        // }

        $nine = '9';
        // $this->db->where('s.phd_duration <', $nine);
        $this->db->where('s.phd_duration <', '9');
        $this->db->where('s.applicant_status !=', 'Graduated');
        // $likeCriteria = " s.phd_duration  <= '8'";
        // $this->db->where($likeCriteria);
        $this->db->where('qs.name', 'Master');
        $this->db->or_where('qs.name', 'POSTGRADUATE');
        $query = $this->db->get();
        $result = $query->result(); 

        // echo "<Pre>"; print_r($this->db);exit();

        return $result;
    }
    

    function addNewSemesterHistory($data)
    {
        $this->db->trans_start();
        $this->db->insert('student_semester_history', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function updateStudent($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('student', $data);

        return TRUE;
    }

    function getStudent($id)
    {
        $this->db->select('*');
        $this->db->from('student');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $result = $query->row();
        // echo "<pre>";print_r($query);die;
        return $result;
    }

    function getPartnerUniversity($id)
    {
        $this->db->select('*');
        $this->db->from('partner_university');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $result = $query->row();
        // echo "<pre>";print_r($query);die;
        return $result;
    }

    function getFeeStructureMaster($id)
    {
        $this->db->select('*');
        $this->db->from('fee_structure_master');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function generateNewMainInvoiceForCourseRegistration($id_student)
    {

        // echo "<Pre>";print_r($id_semester);exit();
        
        $user_id = $this->session->userId;
        $student_data = $this->getStudent($id_student);

        // echo "<Pre>";print_r($student_data);exit();

        $id_student = $student_data->id;
        $id_program = $student_data->id_program;
        $id_intake = $student_data->id_intake;
        $nationality = $student_data->nationality;
        $id_program_scheme = $student_data->id_program_scheme;
        $id_program_has_scheme = $student_data->id_program_has_scheme;
        $id_university = $student_data->id_university;
        $id_branch = $student_data->id_branch;
        $current_semester = $student_data->current_semester;
        $id_program_landscape = $student_data->id_program_landscape;
        $id_fee_structure = $student_data->id_fee_structure;
        $student_current_semester = $student_data->current_semester;

        // echo "<Pre>";print_r($student_data);exit();

        $get_data['id_intake'] = $id_intake;
        $get_data['id_programme'] = $id_program;
        $get_data['id_programme_scheme'] = $id_program_scheme;
        $get_data['id_program_has_scheme'] = $id_program_has_scheme;

        // $programme_landscape = $this->getProgramLandscapeByData($get_data);

        if($id_fee_structure != 0)
        {

            // echo "<Pre>";print_r($installments);exit();


             // echo "<Pre>";print_r($id_branch);exit();

            $trigger = 'SEMESTER REGISTRATION';
            $is_installment = 0;
            $installments = 0;

            // Hided university == 1 && For Per Semester Based Fee Generation 
                // $id_university == 1 && 
            

            // if($id_university == 1)
            // {

                $fee_structure_master = $this->getFeeStructureMaster($id_fee_structure);
                $id_currency = $fee_structure_master->id_currency;


                $get_data['id_fee_structure'] = $id_fee_structure;
                $get_data['id_training_center'] = 1;
                $get_data['trigger'] = $trigger;


                if($nationality == '1')
                {
                    $currency = 'MYR';
                    $get_data['currency'] = $currency;
                    $invoice_details_data = $this->getFeeStructureByData($get_data);
                }
                elseif($nationality != '')
                {
                    $currency = 'USD';
                    $get_data['currency'] = $currency;
                    $invoice_details_data = $this->getFeeStructureByData($get_data);
                }



                // echo "<Pre>";print_r($fix_amount_data);exit();


                if(isset($invoice_details_data))
                {

                    // echo "<Pre>";print_r($cr_hr_multiflier_amount_data);exit;

                
                    $invoice_number = $this->generateMainInvoiceNumber();

                    $invoice['invoice_number'] = $invoice_number;
                    $invoice['type'] = 'Student';
                    $invoice['remarks'] = 'Student Semester Registration';
                    $invoice['id_application'] = '';
                    $invoice['id_student'] = $id_student;
                    $invoice['id_program'] = $id_program;
                    $invoice['id_intake'] = $id_intake;
                    $invoice['currency'] = $id_currency;
                    $invoice['invoice_total'] = 0;
                    $invoice['total_amount'] = 0;
                    $invoice['balance_amount'] = 0;
                    $invoice['paid_amount'] = 0;
                    $invoice['status'] = '1';
                    $invoice['created_by'] = $user_id;
                    
                    // echo "<Pre>";print_r($detail_data);exit;
                    $inserted_id = $this->addNewMainInvoice($invoice);



                    $total = 0;


                    foreach ($invoice_details_data as $invoice_detail)
                    {
                        $detail_amount = $invoice_detail->amount;
                   
                        $data = array(
                                'id_main_invoice' => $inserted_id,
                                'id_fee_item' => $invoice_detail->id_fee_item,
                                'amount' => $detail_amount,
                                'price' => $detail_amount,
                                'quantity' => 1,
                                'id_reference' => $invoice_detail->id,
                                'description' => 'SEMESTER REGISTRATION Fee',
                                'status' => '1',
                                'created_by' => $user_id
                            );

                        $total = $total + $detail_amount;

                        $this->addNewMainInvoiceDetails($data);
                    }

                    $invoice_update['total_amount'] = $total;
                    $invoice_update['balance_amount'] = $total;
                    $invoice_update['invoice_total'] = $total;
                    $invoice_update['total_discount'] = 0;
                    $invoice_update['paid_amount'] = 0;
                    // $invoice_update['inserted_id'] = $inserted_id;
                    // echo "<Pre>";print_r($invoice_update);exit;
                    $this->editMainInvoice($invoice_update,$inserted_id);

                }


            // }




















            // if($id_university > 1)
            // {
            //     $currency = 'USD';

            //     $fee_structure_data = $this->getFeeStructureByTrainingCenterForInvoiceGeneration($id_program,$id_intake,$id_fee_structure,$id_university,'SEMESTER PROMOTION');

            //     // $fee_structure_training_data = $this->getFeeStructureInstallmentByDataNSemester($id_programme_landscape,$id_university,$current_semester);

            //     // $count_fee_struture = count($fee_structure_data);

            //     // echo "<Pre>";print_r($count_fee_struture);exit();


            //     $count_installment_fee = 0;
            //     $count_per_semester_fee = 0;

            //     foreach ($fee_structure_data as $fee_structure)
            //     {
            //         $currency = $fee_structure->currency;
            //         $is_installment = $fee_structure->is_installment;
            //         $id_training_center = $fee_structure->id_training_center;
            //         $trigger_name = $fee_structure->trigger_name;
            //         $installments = $fee_structure->installments;

            //         $instllment_data['id_fee_structure'] = $fee_structure->id;
            //         $instllment_data['trigger_code'] = 'SEMESTER PROMOTION';
            //         $instllment_data['id_fee_structure_master'] = $fee_structure->id_program_landscape;


            //         if($is_installment == 1)
            //         {
            //             $installment_details = $this->getTrainingCenterInstallmentDetails($instllment_data);

            //             if($installment_details)
            //             {
            //                 foreach ($installment_details as $installment_detail)
            //                 {
                                
            //                     $installment_trigger_name = $installment_detail->trigger_name;
            //                     $trigger_semester = $installment_detail->id_semester;


            //                     if($installment_trigger_name == 'SEMESTER PROMOTION' && $trigger_semester == $student_current_semester)
            //                     {
            //                         $count_installment_fee ++;   
            //                     }
            //                 }
            //             }
            //         }
            //         else
            //         {
            //             if($trigger_name == 'SEMESTER PROMOTION')
            //             {
            //                 $count_per_semester_fee ++;    
            //             }
            //         }
            //     }





            //     // echo "<Pre>";print_r($count_per_semester_fee . '1' . $count_installment_fee);exit();
            //     // echo "<Pre>";print_r($count_fee_struture);exit();

                
            //     if(!empty($fee_structure_data) && ($count_per_semester_fee > 0 || $count_installment_fee > 0))
            //     {
            //         // echo "<Pre>";print_r('1');exit();

            //         $amount = 0;

            //         $invoice_number = $this->generateMainInvoiceNumber();

            //         $invoice['invoice_number'] = $invoice_number;
            //         $invoice['type'] = 'Student';
            //         $invoice['remarks'] = 'Student Course Registration Fee';
            //         $invoice['id_application'] = '';
            //         $invoice['id_student'] = $id_student;
            //         $invoice['id_program'] = $id_program;
            //         $invoice['id_intake'] = $id_intake;
            //         $invoice['currency'] = $currency;
            //         $invoice['invoice_total'] = $amount;
            //         $invoice['total_amount'] = $amount;
            //         $invoice['balance_amount'] = $amount;
            //         $invoice['paid_amount'] = 0;
            //         $invoice['status'] = '1';
            //         $invoice['created_by'] = $user_id;
                    
            //         // echo "<Pre>";print_r($detail_data);exit;
            //         $inserted_id = $this->addNewMainInvoice($invoice);


            //         if($inserted_id)
            //         {
            //             $invoice_total_amount = 0;

            //             foreach ($fee_structure_data as $fee_structure)
            //             {
            //                 $is_installment = $fee_structure->is_installment;
            //                 $id_training_center = $fee_structure->id_training_center;
            //                 $trigger_name = $fee_structure->trigger_name;
            //                 $installments = $fee_structure->installments;


            //                 $instllment_data['id_fee_structure'] = $fee_structure->id;
            //                 $instllment_data['trigger_code'] = 'SEMESTER PROMOTION';
            //                 $instllment_data['id_fee_structure_master'] = $fee_structure->id_program_landscape;



            //                 $total_amount = 0;

            //                 if($is_installment == 1)
            //                 {

            //                     $installment_details = $this->getTrainingCenterInstallmentDetails($instllment_data);

            //                     if($installment_details)
            //                     {
            //                         foreach ($installment_details as $installment_detail)
            //                         {
                                        
            //                             $installment_trigger_name = $installment_detail->trigger_name;
            //                             $trigger_semester = $installment_detail->id_semester;


            //                             if($installment_trigger_name == 'SEMESTER PROMOTION' && $trigger_semester == $student_current_semester)
            //                             {

            //                                 $data = array(
            //                                     'id_main_invoice' => $inserted_id,
            //                                     'id_fee_item' => $installment_detail->id_fee_item,
            //                                     'amount' => $installment_detail->amount,
            //                                     'price' => $installment_detail->amount,
            //                                     'quantity' => 1,
            //                                     'id_reference' => $installment_detail->id,
            //                                     'description' => 'Student Semester Promotion Installment Trigger Fee',
            //                                     'status' => 1,
            //                                     'created_by' => $user_id
            //                                 );

            //                                 $total_amount = $total_amount + $installment_detail->amount;
                            
            //                                 $this->addNewMainInvoiceDetails($data);
            //                             }
            //                         }
            //                     }
            //                 }
            //                 else
            //                 {
            //                     if($trigger_name == 'SEMESTER PROMOTION')
            //                     {
            //                          // && $installments == $student_current_semester
            //                         $data = array(
            //                             'id_main_invoice' => $inserted_id,
            //                             'id_fee_item' => $fee_structure->id_fee_item,
            //                             'amount' => $fee_structure->amount,
            //                             'price' => $fee_structure->amount,
            //                             'quantity' => 1,
            //                             'id_reference' => $fee_structure->id,
            //                             'description' => 'Student Semester Promotion (Per Semester) Trigger Fee',
            //                             'status' => 1,
            //                             'created_by' => $user_id
            //                         );

            //                         $total_amount = $total_amount + $fee_structure->amount;

            //                         $this->addNewMainInvoiceDetails($data);
            //                     }
            //                 }

            //                 $invoice_total_amount = $invoice_total_amount + $total_amount;
            //             }



            //             $invoice_update['total_amount'] = $invoice_total_amount;
            //             $invoice_update['balance_amount'] = $invoice_total_amount;
            //             $invoice_update['invoice_total'] = $invoice_total_amount;
            //             $invoice_update['total_discount'] = 0;
            //             $invoice_update['paid_amount'] = 0;

            //             $updated_invoice = $this->editMainInvoice($invoice_update,$inserted_id);
            //         }

            //     }

            // }

        }
        return TRUE;
    }


    function getFeeStructureByTrainingCenterForInvoiceGeneration($id_programme,$id_intake,$id_fee_structure,$id_training_center,$trigger_code)
    {
        $this->db->select('p.*, fstp.name as trigger_name');
        $this->db->from('fee_structure as p');
        $this->db->join('fee_setup as fs', 'p.id_fee_item = fs.id','left'); 
        $this->db->join('frequency_mode as fm', 'fs.id_frequency_mode = fm.id','left'); 
        $this->db->join('fee_structure_triggering_point as fstp', 'p.id_fee_structure_trigger = fstp.id','left'); 
        // $this->db->where('p.id_programme', $id_programme);
        // $this->db->where('p.id_intake', $id_intake);
        $this->db->where('p.id_program_landscape', $id_fee_structure);
        $this->db->where('p.id_training_center', $id_training_center);
        $query = $this->db->get();
        $fee_structures = $query->result();

        $details = array();

        foreach ($fee_structures as $fee_structure)
        {
            $is_installment = $fee_structure->is_installment;
            $trigger_name = $fee_structure->trigger_name;

            if($is_installment == 0)
            {
                if($trigger_name == $trigger_code)
                {
                    array_push($details, $fee_structure);
                }
            }
            else
            {
                array_push($details, $fee_structure);
            }
        }

        return $details;
    }

    function getTrainingCenterInstallmentDetails($data)
    {
        $this->db->select('p.*, sem.name as fee_name, sem.code as fee_code, fm.name as frequency_mode, fstp.name as trigger_name');
        $this->db->from('fee_structure_has_training_center as p');
        $this->db->join('fee_setup as sem', 'p.id_fee_item = sem.id');
        $this->db->join('frequency_mode as fm', 'sem.id_frequency_mode = fm.id');
        $this->db->join('fee_structure_triggering_point as fstp', 'p.id_fee_structure_trigger = fstp.id');
        $this->db->where('p.id_fee_structure', $data['id_fee_structure']);
        $this->db->where('p.id_program_landscape', $data['id_fee_structure_master']);
        $query = $this->db->get(); 
        $results = $query->result();  
        
        $details = array();

        foreach ($results as $result)
        {
            $trigger_name = $result->trigger_name;

            if($trigger_name == $data['trigger_code'])
            {
                array_push($details, $result);
            }
            
        }
        return $details;
    }

    function generateMainInvoiceNumber()
    {
        $year = date('y');
        $Year = date('Y');
            $this->db->select('j.*');
            $this->db->from('main_invoice as j');
            $this->db->order_by("id", "desc");
            $query = $this->db->get();
            $result = $query->num_rows();

     
            $count= $result + 1;
           $jrnumber = $number = "INV" .(sprintf("%'06d", $count)). "/" . $Year;
           return $jrnumber;        
    }

    function addNewMainInvoice($data)
    {
        $this->db->trans_start();
        $this->db->insert('main_invoice', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addNewMainInvoiceDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('main_invoice_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function editMainInvoice($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('main_invoice', $data);
        return TRUE;
    }

    function getFeeStructure($id_program_landscape,$currency,$code,$id_training_center,$trigger_name)
    {
       $this->db->select('p.*, fstp.name as trigger_name');
        $this->db->from('fee_structure as p');
        $this->db->join('fee_setup as fs', 'p.id_fee_item = fs.id');
        $this->db->join('frequency_mode as fm', 'fs.id_frequency_mode = fm.id'); 
        $this->db->join('amount_calculation_type as amt', 'fs.id_amount_calculation_type = amt.id'); 
        $this->db->join('fee_structure_triggering_point as fstp', 'p.id_fee_structure_trigger = fstp.id'); 
        // $this->db->where('p.id_programme', $id_programme);
        // $this->db->where('p.id_intake', $id_intake);
        // $this->db->where('p.id_program_scheme', $id_program_scheme);
        $this->db->where('p.id_program_landscape', $id_program_landscape);
        $this->db->where('p.currency', $currency);
        $this->db->where('p.id_training_center', $id_training_center);
        $this->db->where('fstp.name', $trigger_name);
        // $this->db->where('fm.code', 'PER SEMESTER');
        $this->db->where('fm.code', $code);
        $this->db->order_by('p.id', 'DESC');
        $query = $this->db->get();
        $fee_structure = $query->row();
        return $fee_structure;
    }

    function getFeeStructureByData($data)
    {
        $this->db->select('fst.*, fs.name as fee_structure, fs.code as fee_structure_code, fm.name as frequency_mode, fstp.name as trigger_name, cs.name as currency_name');
        $this->db->from('fee_structure as fst');
        $this->db->join('fee_setup as fs', 'fst.id_fee_item = fs.id');   
        $this->db->join('frequency_mode as fm', 'fs.id_frequency_mode = fm.id'); 
        $this->db->join('fee_structure_triggering_point as fstp', 'fst.id_fee_structure_trigger = fstp.id','left'); 
        $this->db->join('currency_setup as cs', 'fst.currency = cs.id','left'); 
        $this->db->where('fst.id_training_center', $data['id_training_center']);
        $this->db->where('fst.id_program_landscape', $data['id_fee_structure']);
        $this->db->where('fst.currency', $data['currency']);
        $this->db->where('fstp.name', $data['trigger']);
        $this->db->where('fst.status', '1');
        $query = $this->db->get();

        $result = $query->result();
        // echo "<Pre>";print_r($result);exit();
        return $result;
    }
}