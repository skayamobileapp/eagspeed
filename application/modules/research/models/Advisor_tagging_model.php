<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Advisor_tagging_model extends CI_Model
{

     function intakeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('intake');
        $this->db->where('status', $status);
        $query = $this->db->get();
        $result = $query->result();
        // echo "<pre>";print_r($query);die;
        return $result;
    }

    function programListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', $status);
        $query = $this->db->get();
        $result = $query->result();
        // echo "<pre>";print_r($query);die;
        return $result;
    }


    function semesterListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('semester');
        $this->db->where('status', $status);
        $query = $this->db->get();
        $result = $query->result();
        // echo "<pre>";print_r($query);die;
        return $result;
    }


    function staffListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('research_advisor');
        $this->db->where('status', $status);
        $query = $this->db->get();
        $result = $query->result();
        // echo "<pre>";print_r($query);die;
        return $result;
    }

    function qualificationListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('qualification_setup');
        $this->db->where('status', $status);
        $query = $this->db->get();
        $result = $query->result();
        // echo "<pre>";print_r($query);die;
        return $result;
    }

    function studentSearch($data)
    {
        $this->db->select('s.*, p.code as program_code, p.name as program_name, qs.short_name as qualification_code, qs.name as qualification_name, adt.type , adt.full_name as advisor_name, i.year as intake_year, i.name as intake_name');
        $this->db->from('student as s');
        $this->db->join('programme as p', 's.id_program = p.id');
        $this->db->join('education_level as qs', 's.id_degree_type = qs.id');
        $this->db->join('intake as i', 's.id_intake = i.id');
        $this->db->join('research_advisor as adt', 's.id_advisor = adt.id','left');
        if ($data['full_name'] != '')
        {
            $likeCriteria = "(s.full_name  LIKE '%" . $data['full_name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['email_id'] != '')
        {
            $this->db->where('s.email_id', $data['email_id']);
        }
        if ($data['id_advisor'] != '')
        {
            $this->db->where('s.id_supervisor', $data['id_advisor']);
        }
        if ($data['id_program'] != '')
        {
            $this->db->where('s.id_program', $data['id_program']);
        }
        if ($data['id_intake'] != '')
        {
            $this->db->where('s.id_intake', $data['id_intake']);
        }
        // if ($data['id_qualification'] != '')
        // {
        //     $this->db->where('s.id_degree_type', $data['id_qualification']);
        // }
        // if($data['tagging_status'] != '')
        // {
        //     if($data['tagging_status'] == 1)
        //     {
        //         $this->db->where('s.id_advisor !=','0');
        //     }
        //     elseif($data['tagging_status'] == 0)
        //     {
        //         $this->db->where('s.id_advisor',$data['tagging_status']);
        //     }
        // }
        // if ($data['id_semester'] != '')
        // {
        //     $this->db->where('s.id_semester', $data['id_semester']);
        // }
        $this->db->where('s.applicant_status !=', 'Graduated');
        $this->db->where('qs.name =', 'POSTGRADUATE');
        // $this->db->where('qs.name =', 'Master');
        $query = $this->db->get();
        $result = $query->result(); 
        // echo "<pre>";print_r($query);die;

        return $result;
    }
    

    function addAdvisorTagging($data)
    {
        $this->db->trans_start();
        $this->db->insert('advisor_tagging', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function updateStudent($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('student', $data);

        return TRUE;
    }
}