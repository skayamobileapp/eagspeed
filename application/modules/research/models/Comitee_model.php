<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Comitee_model extends CI_Model
{
    function comiteeList()
    {
        $this->db->select('*');
        $this->db->from('proposal_defense_comitee');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function supervisorListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('research_supervisor');
        $this->db->where('status', $status);
        $this->db->order_by("full_name", "ASC");
        $query = $this->db->get();
        $result = $query->result();  
        
        return $result;
    }

    function readerListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('research_readers');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();  
        
        return $result;
    }

    function comiteeListSearch($data)
    {
        $this->db->select('pdc.*, rs.type, rs.full_name as supervisor, r1.name as reader_one_name, r1.email as reader_one_email, r2.name as reader_two_name, r2.email as reader_two_email');
        $this->db->from('proposal_defense_comitee as pdc');
        $this->db->join('research_supervisor as rs','pdc.id_supervisor = rs.id');
        $this->db->join('research_readers as r1','pdc.reader_one = r1.id');
        $this->db->join('research_readers as r2','pdc.reader_two = r2.id');
        if ($data['name'] != '')
        {
            $likeCriteria = "(chairman  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("chairman", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getComitee($id)
    {
        $this->db->select('*');
        $this->db->from('proposal_defense_comitee');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewComitee($data)
    {
        $this->db->trans_start();
        $this->db->insert('proposal_defense_comitee', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editComitee($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('proposal_defense_comitee', $data);
        return TRUE;
    }

    function getComiteeStudentList($id_comitee)
    {
        $this->db->select('trqd.*, s.full_name as student_name, s.nric, s.email_id, p.code as programme_code, p.name as programme_name, i.name as intake_name, i.year as intake_year, rs.code as research_status_code');
        $this->db->from('research_commitee_student_interest as trqd');
        $this->db->join('student as s', 'trqd.id_student = s.id');
        $this->db->join('intake as i', 's.id_intake = i.id');
        $this->db->join('programme as p', 's.id_program = p.id');
        $this->db->join('research_status as rs', 'trqd.id_research_status = rs.id','left');
        $this->db->where('trqd.id_comitee', $id_comitee);
        $query = $this->db->get();
         return $query->result();
    }

    function searchStudentsByData($data)
    {
        $this->db->select('s.*, p.name as program_name, p.code as program_code, i.name as intake_name');
        $this->db->from('student as s');
        $this->db->join('education_level as el', 's.id_degree_type = el.id');
        $this->db->join('intake as i', 's.id_intake = i.id');
        $this->db->join('programme as p', 's.id_program = p.id');
        // $this->db->where('a.id_course_registered_landscape', $data['id_course_registered_landscape']);
        // $this->db->where('a.is_exam_registered !=', 0);
        // $this->db->where('a.is_bulk_withdraw', 0);
        $this->db->where('el.name', 'POSTGRADUATE');

        // if($data['name'])
        // {
        //     $likeCriteria = "(s.full_name  LIKE '%" . $data['name'] . "%')";
        //     $this->db->where($likeCriteria);
        // }
        // if($data['nric'])
        // {
        //     $likeCriteria = "(s.nric  LIKE '%" . $data['nric'] . "%')";
        //     $this->db->where($likeCriteria);
        // }
        // if($data['email_id'])
        // {
        //     $likeCriteria = "(s.email_id  LIKE '%" . $data['email_id'] . "%')";
        //     $this->db->where($likeCriteria);
        // }

        $likeCriteria = "s.id NOT IN (SELECT a.id_student FROM research_commitee_student_interest as a where a.id_comitee = " . $data['id_comitee'] . ")";
        $this->db->where($likeCriteria);  

         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();
         return $result;
    }

    function addComiteeInterest($data)
    {
        $this->db->trans_start();
        $this->db->insert('research_commitee_student_interest', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function researchStatusListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('research_status');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function updateComiteeInterest($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('research_commitee_student_interest', $data);
        return TRUE;
    }
}

