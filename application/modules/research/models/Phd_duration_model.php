<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Phd_duration_model extends CI_Model
{
    function phdDurationList()
    {
        $this->db->select('*');
        $this->db->from('research_phd_duration');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function phdDurationListSearch($data)
    {
        $this->db->select('*');
        $this->db->from('research_phd_duration');
        if ($data['name'] != '')
        {
            $likeCriteria = "(name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getPhdDuration($id)
    {
        $this->db->select('*');
        $this->db->from('research_phd_duration');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewPhdDuration($data)
    {
        $this->db->trans_start();
        $this->db->insert('research_phd_duration', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editPhdDuration($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('research_phd_duration', $data);
        return TRUE;
    }
}

