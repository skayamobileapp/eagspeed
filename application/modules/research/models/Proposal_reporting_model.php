<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Proposal_reporting_model extends CI_Model
{

    function getProposalReportingLisSearch($data,$stage)
    {
        $this->db->select('rd.*, rdur.topic as deliverable, rs.full_name as student_name, rs.nric, rs.email_id, rc.name as chapter, rss.full_name as supervisor_name, rss.email as supervisor_email');
        $this->db->from('research_proposal_reporting as rd');
        $this->db->join('research_deliverables as rdur','rd.id_deliverable = rdur.id');
        $this->db->join('research_chapter as rc','rd.id_chapter = rc.id');
        // $this->db->join('research_status as rst','rd.status = rst.id','left');
        // $this->db->join('research_phd_duration as rpd','rd.id_phd_duration = rpd.id');
        $this->db->join('student as rs','rd.id_student = rs.id');
        $this->db->join('research_supervisor as rss','rd.id_supervisor = rss.id');
        // $this->db->where('rd.id_supervisor', $id_supervisor);
        if ($data['name'] != '')
        {
            $likeCriteria = "(s.full_name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if($data['id_supervisor'] != '')
        {
            $this->db->where('rd.id_supervisor', $data['id_supervisor']);
        }
        if($data['id_chapter'] != '')
        {
            $this->db->where('rd.id_chapter', $data['id_chapter']);
        }

        if ($stage != '')
        {
            $this->db->where('rd.stage', $stage);
        }
        // $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getSupervisor($id)
    {
        $this->db->select('s.*');
        $this->db->from('research_supervisor as s');
        $this->db->where('s.id', $id);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getStudentByStudentId($id_student)
    {
        $this->db->select('s.*, p.code as programme_code, p.name as programme_name, i.name as intake_name, st.ic_no, st.name as advisor_name, ms.name as mailing_state, mc.name as mailing_country, ps.name as permanent_state, pc.name as permanent_country, rs.name as race, rels.name as religion, brch.code as branch_code, brch.name as branch_name, salut.name as salutation, pu.code as partner_university_code, pu.name as partner_university_name, sch.code as scheme_code, sch.description as scheme_name');
        $this->db->from('student as s');
        $this->db->join('programme as p', 's.id_program = p.id'); 
        $this->db->join('organisation_has_training_center as brch', 's.id_branch = brch.id','left'); 
        $this->db->join('salutation_setup as salut', 's.salutation = salut.id','left'); 
        $this->db->join('partner_university as pu', 's.id_university = pu.id','left'); 
        $this->db->join('scheme as sch', 's.id_program_has_scheme = sch.id','left'); 
        $this->db->join('intake as i', 's.id_intake = i.id'); 
        $this->db->join('state as ms', 's.mailing_state = ms.id'); 
        $this->db->join('country as mc', 's.mailing_country = mc.id');
        $this->db->join('state as ps', 's.permanent_state = ps.id'); 
        $this->db->join('country as pc', 's.permanent_country = pc.id'); 
        $this->db->join('race_setup as rs', 's.id_race = rs.id'); 
        $this->db->join('religion_setup as rels', 's.religion = rels.id','left');
        $this->db->join('staff as st', 's.id_advisor = st.id','left'); 
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getStudent($id_student)
    {
        $this->db->select('s.*');
        $this->db->from('student as s');
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function researchTopicListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('research_topic');
        $this->db->where("status", $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function supervisorListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('research_supervisor');
        $this->db->where('status', $status);
        $this->db->order_by("full_name", "ASC");
        $query = $this->db->get();
        $result = $query->result();  
        
        return $result;
    }

    function deliverablesList()
    {
        $this->db->select('*');
        $this->db->from('research_deliverables_student');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function durationListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('research_phd_duration');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();  
        
        return $result;
    }

    function chapterListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('research_chapter');
        $this->db->where('status', $status);
        // $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();  
        
        return $result;
    }

    function researchStatusListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('research_status');
        $this->db->where('status', $status);
        // $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();  
        
        return $result;
    }

    function deliverableListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('research_deliverables');
        $this->db->where('status', $status);
        // $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();  
        
        return $result;
    }
    
    function getChapterByDuration($id_phd_duration)
    {
        $this->db->select('DISTINCT(rd.id_chapter) as id,rc.*');
        $this->db->from('research_deliverables as rd');
        $this->db->join('research_chapter as rc','rd.id_chapter = rc.id');
        $this->db->where('rd.id_phd_duration', $id_phd_duration);
        // $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getTopicByData($data)
    {
        $this->db->select('rd.*');
        $this->db->from('research_deliverables as rd');
        $this->db->join('research_chapter as rc','rd.id_chapter = rc.id');
        $this->db->where('rd.id_phd_duration', $data['id_phd_duration']);
        $this->db->where('rd.id_chapter', $data['id_chapter']);
        // $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }


    function getOrganisation()
    {
        $this->db->select('fc.*');
        $this->db->from('organisation as fc');
        $this->db->where('fc.status', 1);
        $this->db->order_by("fc.id", "DESC");
        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    function getProposalReport($id)
    {
        $this->db->select('rd.*');
        $this->db->from('research_proposal_reporting as rd');
        // $this->db->join('research_status as rst','rd.status = rst.id','left');
        $this->db->where('rd.id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewProposalReporting($data)
    {
        $this->db->trans_start();
        $this->db->insert('research_proposal_reporting', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editDeliverables($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('research_deliverables_student', $data);
        return TRUE;
    }

    function generateProposalReportingApplicationNumber()
    {
        $year = date('y');
        $Year = date('Y');
            $this->db->select('j.*');
            $this->db->from('research_proposal_reporting as j');
            $this->db->order_by("id", "desc");
            $query = $this->db->get();
            $result = $query->num_rows();

     
            $count= $result + 1;
           $jrnumber = $number = "RPR" .(sprintf("%'06d", $count)). "/" . $Year;
           return $jrnumber;        
    }

    function addProposalReportingComments($data)
    {
        $this->db->trans_start();
        $this->db->insert('proposal_reporting_comments', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;

    }

    function getProposalReportComments($id)
    {
        $this->db->select('rd.*');
        $this->db->from('proposal_reporting_comments as rd');
        $this->db->where('rd.id_proposal_reporting', $id);
        // $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

}