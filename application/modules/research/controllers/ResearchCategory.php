<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ResearchCategory extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('research_category_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('research_category.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_program'] = $this->security->xss_clean($this->input->post('id_program'));

            $data['searchParam'] = $formData;
            $data['researchCategoryList'] = $this->research_category_model->researchCategoryListSearch($formData);
            $data['programList'] = $this->research_category_model->programListByStatus('1');

            // echo "<Pre>";print_r($data['researchCategory']);exit();

            $this->global['pageTitle'] = 'Campus Management System : ResearchCategory List';
            $this->loadViews("research_category/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('research_category.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $id_user = $this->session->userId;


            if($this->input->post())
            {

                $name = $this->security->xss_clean($this->input->post('name'));
                $id_program = $this->security->xss_clean($this->input->post('id_program'));
                $max_candidates = $this->security->xss_clean($this->input->post('max_candidates'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'id_program' => $id_program,
                    'max_candidates' => $max_candidates,
                    'status' => $status,
                    'created_by' => $id_user
                );

                $inserted_id = $this->research_category_model->addNewResearchCategory($data);
                if($inserted_id)
                {
                    $moved = $this->research_category_model->moveTempToDetails($inserted_id);
                }

                redirect('/research/researchCategory/list');
            }
            else
            {
                $this->research_category_model->deleteTempResearchCategoryHasSupervisorBySessionId($id_session);
                
            }
            $data['programList'] = $this->research_category_model->programListByStatus('1');
            $data['staffList'] = $this->research_category_model->staffListByStatus('1');

            $this->global['pageTitle'] = 'Campus Management System : Add ResearchCategory';
            $this->loadViews("research_category/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('research_category.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/research/researchCategory/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $id_program = $this->security->xss_clean($this->input->post('id_program'));
                $max_candidates = $this->security->xss_clean($this->input->post('max_candidates'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'id_program' => $id_program,
                    'max_candidates' => $max_candidates,
                    'status' => $status,
                    'updated_by' => $id_user
                );

                $result = $this->research_category_model->editResearchCategoryDetails($data,$id);
                redirect('/research/researchCategory/list');
            }
            $data['researchCategory'] = $this->research_category_model->getResearchCategory($id);
            $data['researchCategoryHasSupervisor'] = $this->research_category_model->getResearchCategoryHasSupervisor($id);

            $data['staffList'] = $this->research_category_model->staffListByStatus('1');
            $data['programList'] = $this->research_category_model->programListByStatus('1');


            $this->global['pageTitle'] = 'Campus Management System : Edit ResearchCategory';
            // echo "<Pre>";print_r($data);exit;
            $this->loadViews("research_category/edit", $this->global, $data, NULL);
        }
    }

    function tempAddResearchCategoryHasSupervisor()
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $tempData['id_session'] = $id_session;

        // echo "<Pre>";print_r($tempData);exit;

        $inserted_id = $this->research_category_model->tempAddResearchCategoryHasSupervisor($tempData);
        // echo "<Pre>";print_r($inserted_id);exit;

        $data = $this->displaytempdata();
        
        echo $data;        
    }

    function displaytempdata()
    {
        $id_session = $this->session->my_session_id;
        
        $temp_details = $this->research_category_model->getTempResearchCategoryHasSupervisorBySession($id_session); 
        // echo "<Pre>";print_r($temp_details);exit;

        if(!empty($temp_details))
        {

        $table = "
        <div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Supervisor</th>
                    <th>Action</th>
                </tr>
                </thead>";
                    for($i=0;$i<count($temp_details);$i++)
                    {
                    $id = $temp_details[$i]->id;
                    $ic_no = $temp_details[$i]->ic_no;
                    $staff_name = $temp_details[$i]->staff_name;

                    $j = $i+1;
                        $table .= "
                        <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$ic_no - $staff_name</td>                         
                            <td>
                                <a class='btn btn-sm btn-edit' onclick='deleteTempResearchCategoryHasSupervisor($id)'>Delete</a>
                            <td>
                        </tr>";
                    }
            // <span onclick='deleteTempResearchCategoryHasProgramme($id)'>Delete</a>                            
            $table.= "
            </tbody>
            </table>
            </div>";
        }
        else
        {
            $table="";
        }
        return $table;
    }

    function deleteTempResearchCategoryHasSupervisor($id)
    {
        $inserted_id = $this->research_category_model->deleteTempResearchCategoryHasSupervisor($id);
        if($inserted_id)
        {
            $data = $this->displaytempdata();
            echo $data;  
        }
        
        // echo "Success"; 
    }

    function addResearchCategoryHasSupervisor()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $inserted_id = $this->research_category_model->addNewResearchCategoryHasSupervisors($tempData);
        // echo "<Pre>";print_r($tempData);exit();

        echo "success";exit;
    }

    function deleteResearchCategoryHasSupervisor($id)
    {
        $inserted_id = $this->research_category_model->deleteResearchCategoryHasSupervisor($id);
        echo "Success"; 
    }
}
