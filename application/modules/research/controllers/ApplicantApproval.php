<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ApplicantApproval extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('applicant_model');
        $this->load->model('applicant_approval_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('applicant_approval.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['intakeList'] = $this->applicant_model->intakeList();
            $data['programList'] = $this->applicant_model->programList();


                $formData['first_name'] = $this->security->xss_clean($this->input->post('first_name'));
                $formData['email_id'] = $this->security->xss_clean($this->input->post('email_id'));
                $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
                $formData['id_program'] = $this->security->xss_clean($this->input->post('id_program'));
                $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
                // $formData['applicant_status'] = $this->security->xss_clean($this->input->post('applicant_status'));

                $formData['applicant_status'] = 'Draft';
 
            $data['applicantList'] = $this->applicant_approval_model->applicantListForApproval($formData);
            $data['searchParam'] = $formData;



            $this->global['pageTitle'] = 'Campus Management System : Applicant Approval';
            //print_r($subjectDetails);exit;
            $this->loadViews("applicant_approval/list", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('applicant_approval.approve') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/research/applicantApproval/list');
            }
            
            $getApplicantDetails = $this->applicant_model->getApplicantDetailsById($id);

            if($this->input->post())
            {
                // echo "<Pre>";print_r($this->input->post());exit;

                $id_user = $this->session->userId;
                $id_session = $this->session->session_id;

                
                $applicant_status = $this->security->xss_clean($this->input->post('applicant_status'));
                $reason = $this->security->xss_clean($this->input->post('reason'));


                $applicant_data = array(
                    'applicant_status' => $applicant_status,
                    'approved_dt_tm' => date('Y-m-d'),
                    'reason' => $reason,
                    'approved_by' => $id_user
                );
                $result = $this->applicant_approval_model->editApplicantDetails($applicant_data,$id);

                if($applicant_status == "Approved")
                {
                    $id_university = $getApplicantDetails->id_university;
                    $partner_university = $this->applicant_approval_model->getPartnerUniversity($id_university);

                    if($id_university == 1)
                    {
                        // Hided On 13-09-2020 This Hided Because Need To Know Complete Flow Of Invoice Generation
                        // Enabled On 09-11-2020 Because Need To Show Demo Flow Of Invoice Generation For Meeting
                        $this->applicant_approval_model->createNewMainInvoiceForStudent($id);
                        

                        // Hided On 08-08-2020 FOr New Student Registration This Move Shifted To Registration->Student->approvalList
                        // $insert_id =  $this->applicant_approval_model->addNewStudent($id);
                        // if($insert_id)
                        // {
                            // $this->applicant_approval_model->addStudentProfileDetail($insert_id);

                            // echo "<Pre>";print_r($applicant_status);exit;
                            // $this->applicant_approval_model->createNewMainInvoiceForStudent($id,'4');
                        // }
                    }
                    elseif($partner_university->billing_to == 'Student')
                    {
                        $this->applicant_approval_model->createNewMainInvoiceForStudent($id);
                    }
                }
                
                redirect('/research/applicantApproval/list');
            }

            $data['getApplicantDetails'] = $getApplicantDetails;
            $data['raceList'] = $this->applicant_approval_model->raceList();
            $data['religionList'] = $this->applicant_approval_model->religionList();
            $data['salutationList'] = $this->applicant_approval_model->salutationListByStatus('1');
            $data['intakeList'] = $this->applicant_approval_model->intakeList();
            $data['programList'] = $this->applicant_approval_model->programList();
            $data['nationalityList'] = $this->applicant_approval_model->nationalityList();
            $data['countryList'] = $this->applicant_approval_model->countryListByStatus('1');
            $data['stateList'] = $this->applicant_approval_model->stateListByStatus('1');
            $data['degreeTypeList'] = $this->applicant_model->qualificationList();
            $data['branchList'] = $this->applicant_model->branchListByStatus();
            $data['requiremntListList'] = $this->applicant_model->programRequiremntListList();
            $data['partnerUniversityList'] = $this->applicant_model->getUniversityListByStatus('1');
            $data['schemeList'] = $this->applicant_model->schemeListByStatus('1');
            $data['programStructureTypeList'] = $this->applicant_approval_model->programStructureTypeListByStatus('1');
            
            $data['sibblingDiscountDetails'] = $this->applicant_approval_model->getApplicantSibblingDiscountDetails($id);
            $data['employeeDiscountDetails'] = $this->applicant_approval_model->getApplicantEmployeeDiscountDetails($id);
            $data['alumniDiscountDetails'] = $this->applicant_approval_model->getApplicantAlumniDiscountDetails($id);


            $data['feeStructureDetails'] = $this->applicant_approval_model->getfeeStructureMasterByApplicant($id);


            
            $data['sibblingDiscount'] = $this->applicant_approval_model->getSibblingDiscountByApplicantIdCurrency($id);
            $data['employeeDiscount'] = $this->applicant_approval_model->getEmployeeDiscountByApplicantIdCurrency($id);
            $data['alumniDiscount'] = $this->applicant_approval_model->getAlumniDiscountByApplicantIdCurrency($id);


            // echo "<Pre>";print_r($data['feeStructureDetails']);exit;


            $data['programEntryRequirementList'] = $this->applicant_model->programEntryRequirementList($data['getApplicantDetails']->id_program);
            $data['programDetails'] = $this->applicant_model->getProgramDetails($data['getApplicantDetails']->id_program);
            $data['applicantUploadedFiles'] = $this->applicant_model->getApplicantUploadedFiles($id);

            // echo "<Pre>";print_r($data['getApplicantDetails']);exit;
            
            $this->global['pageTitle'] = 'Campus Management System : Edit Applicant Approval';
            $this->loadViews("applicant_approval/edit", $this->global, $data, NULL);
        }
    }
}
