<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ReasonApplying extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('reason_applying_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('research_reason_applying.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $data['searchParam'] = $formData;
            
            $data['reasonApplyingList'] = $this->reason_applying_model->reasonApplyingListSearch($formData);
            $this->global['pageTitle'] = 'College Management System : Research Reason Applying List';
            $this->loadViews("reason_applying/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('research_reason_applying.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'description' => $description,
                    'status' => $status
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->reason_applying_model->addNewReasonApplying($data);
                redirect('/research/reasonApplying/list');
            }
            $this->global['pageTitle'] = 'College Management System : Add Research Reason Applying';
            $this->loadViews("reason_applying/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('research_reason_applying.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/research/reasonApplying/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'description' => $description,
                    'status' => $status
                );

                $result = $this->reason_applying_model->editReasonApplying($data,$id);
                redirect('/research/reasonApplying/list');
            }
            $data['reasonApplying'] = $this->reason_applying_model->getReasonApplying($id);
            $this->global['pageTitle'] = 'College Management System : Edit Research Reason Applying';
            $this->loadViews("reason_applying/edit", $this->global, $data, NULL);
        }
    }
}
