<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Ppt extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('ppt_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if($this->checkAccess('research_ppt.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_supervisor'] = $this->security->xss_clean($this->input->post('id_supervisor'));

            $data['searchParam'] = $formData;

            $data['pptList'] = $this->ppt_model->pptListSearch($formData);

            $data['supervisorList'] = $this->ppt_model->supervisorListByStatus('1');

            // echo "<Pre>";print_r($data['pptList']);exit();

            $this->global['pageTitle'] = 'College Management System : List TOC Reporting';
            $this->loadViews("ppt/list", $this->global, $data, NULL);
        }
    }

    function view($id = NULL)
    {
        if($this->checkAccess('research_ppt.view') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($id == null)
            {
                redirect('/research/pptt/list');
            }
            $data['ppt'] = $this->ppt_model->getPpt($id);
            $data['pptReportingComments'] = $this->ppt_model->pptCommentsDetails($id);

            $data['organisationDetails'] = $this->ppt_model->getOrganisation();
            $data['supervisor'] = $this->ppt_model->getSupervisor($data['ppt']->id_supervisor);
            $data['studentDetails'] = $this->ppt_model->getStudentByStudentId($data['ppt']->id_student);
                
            // echo "<Pre>"; print_r($data['pptCommentsDetails']);exit;

            $this->global['pageTitle'] = 'College Management System : View TOC Reporting';
            $this->loadViews("ppt/edit", $this->global, $data, NULL);
        }
    }
}