<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class SupervisorChangeApplication extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('supervisor_change_application_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('research_supervisor_change_application.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
            $formData['status'] = $this->security->xss_clean($this->input->post('status'));
            if($formData['status'] == '')
            {
            	$formData['status'] = 0;
            }

            $data['searchParam'] = $formData;

            $data['supervisorChangeApplicationList'] = $this->supervisor_change_application_model->supervisorChangeApplicationListSearch($formData);
            
            // echo "<Pre>"; print_r($data['supervisorChangeApplicationList']);exit;
            
            $this->global['pageTitle'] = 'College Management System : Change Supervisor List';
            $this->loadViews("supervisor_change_application/list", $this->global, $data, NULL);
        }
    }

    function edit($id = NULL)
    {
        if ($this->checkAccess('research_supervisor_change_application.approve') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_user = $this->session->userId;


            if ($id == null)
            {
                redirect('/research/supervisor_change_application/list');
            }

            $supervisorChangeApplication = $this->supervisor_change_application_model->getSupervisorChangeApplication($id);

	        $id_student = $supervisorChangeApplication->id_student;
	        $student = $this->supervisor_change_application_model->getStudent($id_student);


            if($this->input->post())
	        {
	            // echo "<Pre>"; print_r($this->input->post());exit;

	            $id_supervisor = $this->security->xss_clean($this->input->post('id_supervisor'));

	            
	            $data = array(
	                'id_supervisor_new' => $id_supervisor,
	                'status' => 1,
                    'updated_by'=>$id_user
	            );

	            $insert_id = $this->supervisor_change_application_model->editSupervisorChangeApplication($data,$id);


	            if($id_student > 0 && $id_supervisor > 0)
                {
                        $data = array(
                            'id_supervisor'=>$id_supervisor,
                            'updated_by'=>$id_user
                        );
                // echo "<Pre>"; print_r($this->input->post());exit;
                // echo "<Pre>"; print_r($detailsData);exit;
                    $updated_student_data = $this->supervisor_change_application_model->updateStudent($data,$id_student);

                        if($updated_student_data)
                        {
                            $data_advisor = array(
                            'id_supervisor'=>$id_supervisor,
                            'id_student'=>$id_student,
                            'is_change_application'=>$id,
                            'status'=> 1,
                            'created_by'=>$id_user
                            );
                            
                            $added_advisor_data = $this->supervisor_change_application_model->addSupervisorTagging($data_advisor);
                        
                        }
                }

                // redirect($_SERVER['HTTP_REFERER']);
	            redirect('/research/supervisorChangeApplication/list');
	        }


	        
	        $data['studentDetails'] = $this->supervisor_change_application_model->getStudentByStudentId($id_student);

	        $supervisor = $this->supervisor_change_application_model->getSupervisor($data['studentDetails']->id_supervisor);

	        if($supervisor)
	        {
	            $data['supervisor'] = $supervisor;
	        }
	        else
	        {
	            $data['supervisor'] = array();
	        }

	        $data['supervisorChangeApplication'] = $supervisorChangeApplication;

	        $data['supervisorList'] = $this->supervisor_change_application_model->supervisorListByStatus('1');
	        $data['supervisorHistoryList'] = $this->supervisor_change_application_model->getSupervisorHistoryListByStudentId($id_student);


	            // echo "<Pre>"; print_r($data['supervisorHistoryList']);exit;
	        
	        $data['getSupervisorChangeApplicationListByStudentId'] = $this->supervisor_change_application_model->getSupervisorChangeApplicationListByStudentId($id_student);


            $this->global['pageTitle'] = 'College Management System : Change Supervisor';
            $this->loadViews("supervisor_change_application/edit", $this->global, $data, NULL);
        }
    }


    function view($id = NULL)
    {
        if ($this->checkAccess('research_supervisor_change_application.view') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_user = $this->session->userId;


            if ($id == null)
            {
                redirect('/research/supervisor_change_application/list');
            }

            $supervisorChangeApplication = $this->supervisor_change_application_model->getSupervisorChangeApplication($id);

	        $id_student = $supervisorChangeApplication->id_student;
	        $student = $this->supervisor_change_application_model->getStudent($id_student);


            if($this->input->post())
	        {
                // redirect($_SERVER['HTTP_REFERER']);
	            redirect('/research/supervisorChangeApplication/list');
	        }


	        
	        $data['studentDetails'] = $this->supervisor_change_application_model->getStudentByStudentId($id_student);

	        $supervisor = $this->supervisor_change_application_model->getSupervisor($data['studentDetails']->id_supervisor);

	        if($supervisor)
	        {
	            $data['supervisor'] = $supervisor;
	        }
	        else
	        {
	            $data['supervisor'] = array();
	        }

	        $data['supervisorChangeApplication'] = $supervisorChangeApplication;

	        $data['supervisorList'] = $this->supervisor_change_application_model->supervisorListByStatus('1');
	        $data['supervisorHistoryList'] = $this->supervisor_change_application_model->getSupervisorHistoryListByStudentId($id_student);


	        
	        $data['getSupervisorChangeApplicationListByStudentId'] = $this->supervisor_change_application_model->getSupervisorChangeApplicationListByStudentId($id_student);


            $this->global['pageTitle'] = 'College Management System : Change Supervisor';
            $this->loadViews("supervisor_change_application/view", $this->global, $data, NULL);
        }
    }
}