<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Bound extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('bound_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if($this->checkAccess('research_bound.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_supervisor'] = $this->security->xss_clean($this->input->post('id_supervisor'));

            $data['searchParam'] = $formData;

            $data['boundList'] = $this->bound_model->boundListSearch($formData);

            $data['supervisorList'] = $this->bound_model->supervisorListByStatus('1');

            // echo "<Pre>";print_r($data['boundList']);exit();

            $this->global['pageTitle'] = 'College Management System : List TOC Reporting';
            $this->loadViews("bound/list", $this->global, $data, NULL);
        }
    }

    function view($id = NULL)
    {
        if($this->checkAccess('research_bound.view') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($id == null)
            {
                redirect('/research/boundt/list');
            }
            $data['bound'] = $this->bound_model->getBound($id);
            $data['boundReportingComments'] = $this->bound_model->boundCommentsDetails($id);

            $data['organisationDetails'] = $this->bound_model->getOrganisation();
            $data['supervisor'] = $this->bound_model->getSupervisor($data['bound']->id_supervisor);
            $data['studentDetails'] = $this->bound_model->getStudentByStudentId($data['bound']->id_student);
                
            // echo "<Pre>"; print_r($data['boundCommentsDetails']);exit;

            $this->global['pageTitle'] = 'College Management System : View TOC Reporting';
            $this->loadViews("bound/edit", $this->global, $data, NULL);
        }
    }
}