<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class PartnerUniversity extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('partner_university_model');
        $this->isPartnerUniversityLoggedIn();
        // echo "Data";exit;
    }

    function edit()
    {
        $id = $this->session->id_partner_university;


        if($this->input->post())
        {

        $resultprint = $this->input->post();

        // echo "<Pre>"; print_r($resultprint);exit();
        
        if($resultprint)
        {
         switch ($resultprint['btn_submit'])
         {

            case '1':

            $id_user = $this->session->userId;
            $id_session = $this->session->my_session_id;

             $formData = $this->input->post();



            $name = $this->security->xss_clean($this->input->post('name'));
            $code = $this->security->xss_clean($this->input->post('code'));
            $short_name = $this->security->xss_clean($this->input->post('short_name'));
            $name_in_malay = $this->security->xss_clean($this->input->post('name_in_malay'));
            $url = $this->security->xss_clean($this->input->post('url'));
            $id_country = $this->security->xss_clean($this->input->post('id_country'));
            $status = $this->security->xss_clean($this->input->post('status'));
            $contact_number = $this->security->xss_clean($this->input->post('contact_number'));
            $address1 = $this->security->xss_clean($this->input->post('address1'));
            $address2 = $this->security->xss_clean($this->input->post('address2'));
            $id_country = $this->security->xss_clean($this->input->post('id_country'));
            $id_state = $this->security->xss_clean($this->input->post('id_state'));
            $city = $this->security->xss_clean($this->input->post('city'));
            $zipcode = $this->security->xss_clean($this->input->post('zipcode'));
            $email = $this->security->xss_clean($this->input->post('email'));
            $id_partner_category = $this->security->xss_clean($this->input->post('id_partner_category'));
            $id_partner_university = $this->security->xss_clean($this->input->post('id_partner_university'));
            $start_date = $this->security->xss_clean($this->input->post('start_date'));
            $end_date = $this->security->xss_clean($this->input->post('end_date'));
            $billing_to = $this->security->xss_clean($this->input->post('billing_to'));
            $old_cert = $this->security->xss_clean($this->input->post('cert'));
            $pay_as_agent = $this->security->xss_clean($this->input->post('pay_as_agent'));
            $id_bank = $this->security->xss_clean($this->input->post('id_bank'));
            $account_number = $this->security->xss_clean($this->input->post('account_number'));
            $swift_code = $this->security->xss_clean($this->input->post('swift_code'));
            $bank_address = $this->security->xss_clean($this->input->post('bank_address'));
            $login_id = $this->security->xss_clean($this->input->post('login_id'));


        
            $data = array(
                'name' => $name,
                'code' => $code,
                'short_name' => $short_name,
                'name_in_malay' => $name_in_malay,
                'url' => $url,
                'login_id' => $login_id,
                'id_country' => $id_country,
                'contact_number' => $contact_number,
                'id_partner_category' => $id_partner_category,
                'id_partner_university' => $id_partner_university,
                'start_date' => date('Y-m-d', strtotime($start_date)),
                'end_date' => date('Y-m-d', strtotime($end_date)),
                // 'certificate' => $certificate,
                'address1' => $address1,
                'address2' => $address2,
                'id_country' => $id_country,
                'id_state' => $id_state,
                'city' => $city,
                'zipcode' => $zipcode,
                'email' => $email,
                'billing_to' => $billing_to,
                'pay_as_agent' => $pay_as_agent,
                'id_bank' => $id_bank,
                'account_number' => $account_number,
                'swift_code' => $swift_code,
                'bank_address' => $bank_address
            );


            if($_FILES['image'])
            {  


            $certificate_name = $_FILES['image']['name'];
            $certificate_size = $_FILES['image']['size'];
            $certificate_tmp =$_FILES['image']['tmp_name'];
            
            // echo "<Pre>"; print_r($certificate_tmp);exit();

            $certificate_ext=explode('.',$certificate_name);
            $certificate_ext=end($certificate_ext);
            $certificate_ext=strtolower($certificate_ext);


            $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'Image File');

            $image_file = $this->uploadFile($certificate_name,$certificate_tmp,'Image File');

            }

            if($image_file != '')
            {
                $data['image'] = $image_file;

                $sessionArray = array(
                    'partner_university_image' => $image_file
                    );

            }

            // echo "<Pre>"; print_r($data);exit;
            
            $result = $this->partner_university_model->editPartnerUniversity($data,$id);

            if($result && $sessionArray)
            {
                $this->session->set_userdata($sessionArray);
            }
            redirect($_SERVER['HTTP_REFERER']);

            break;



            case '4':

            // echo "<Pre>"; print_r($_FILES);exit();

            if($_FILES['moa_file'])
            {  


            $certificate_name = $_FILES['moa_file']['name'];
            $certificate_size = $_FILES['moa_file']['size'];
            $certificate_tmp =$_FILES['moa_file']['tmp_name'];
            
            // echo "<Pre>"; print_r($certificate_tmp);exit();

            $certificate_ext=explode('.',$certificate_name);
            $certificate_ext=end($certificate_ext);
            $certificate_ext=strtolower($certificate_ext);


            $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'MOA File');

            $moa_file = $this->uploadFile($certificate_name,$certificate_tmp,'MOA File');

            }

            // echo "<Pre>"; print_r($moa_file);exit();

            $start_date = $this->security->xss_clean($this->input->post('moa_start_date'));
            $end_date = $this->security->xss_clean($this->input->post('moa_end_date'));
            $name = $this->security->xss_clean($this->input->post('moa_name'));
            $id_currency = $this->security->xss_clean($this->input->post('moa_id_currency'));
            $reminder_months = $this->security->xss_clean($this->input->post('moa_reminder_months'));

            $moa = array(

                    'name' => $name,
                    'id_currency' => $id_currency,
                    'reminder_months' => $reminder_months,
                    'start_date' => date('Y-m-d', strtotime($start_date)),
                    'end_date' => date('Y-m-d', strtotime($end_date)),
                    'id_partner_university' => $id
                );


            if($moa_file != '')
            {
                $moa['file'] = $moa_file;
            }

            // echo "<Pre>"; print_r($moa_file);exit();

            $result = $this->partner_university_model->addNewAggrement($moa);

            
            redirect($_SERVER['HTTP_REFERER']);
                
            break;
            
            }

         }
         
        }

        // echo "<Pre>"; print_r($id);exit();


        $data['id'] = $id;
        $data['countryList'] = $this->partner_university_model->countryListByStatus('1');
        $data['bankList'] = $this->partner_university_model->bankListByStatus('1');
        $data['partnerCategoryList'] = $this->partner_university_model->partnerCategoryListByStatus('1');
        $data['partnerUniversityList'] = $this->partner_university_model->partnerUniversityListByStatus('1');
        // $data['stateList'] = $this->partner_university_model->stateListByStatus('1');
        $data['staffList'] = $this->partner_university_model->staffListByStatus('1');
        $data['programList'] = $this->partner_university_model->programListByStatus('1');
        // $data['moduleTypeList'] = $this->partner_university_model->moduleTypeListByStatus('1');
        $data['currencyList'] = $this->partner_university_model->currencyListByStatus('1');


        $data['partnerUniversity'] = $this->partner_university_model->getPartnerUniversity($id);
        $data['statusListByType'] = $this->partner_university_model->statusListByType('PartnerUniversity');


        // $data['comiteeList'] = $this->partner_university_model->comiteeList($id);
        // $data['trainingCenterList'] = $this->partner_university_model->trainingCenterList($id);
        // $data['getPartnerUniversityAggrementList'] = $this->partner_university_model->getPartnerUniversityAggrementList($id);
        // $data['partnerProgramDetails'] = $this->partner_university_model->partnerProgramDetails($id);

        // if($data['partnerProgramDetails'])
        // {
        //     $data['partnerProgramStudyModeDetails'] = $this->partner_university_model->partnerProgramStudyModeDetails($id,$data['partnerProgramDetails']->id);
        //     $data['partnerProgramApprenticeshipDetails'] = $this->partner_university_model->partnerProgramApprenticeshipDetails($id,$data['partnerProgramDetails']->id);
        //     $data['partnerProgramInternshipDetails'] = $this->partner_university_model->partnerProgramInternshipDetails($id,$data['partnerProgramDetails']->id);
        //     $data['partnerProgramSyllabusDetails'] = $this->partner_university_model->partnerProgramSyllabusDetails($id,$data['partnerProgramDetails']->id);
        // }

        // echo "<Pre>"; print_r($data['partnerUniversity']);exit();


        $this->global['pageTitle'] = 'Campus Management System : Edit Partner University';
        $this->loadViews("partneruniversity/edit_profile", $this->global, $data, NULL);
    }

    function changePassword()
    {
        $id_partner_university = $this->session->id_partner_university;
        
        $resultprint = $this->input->post();

        if($resultprint)
        {
            // echo "<Pre>";print_r($resultprint);exit;
            $new_password = $this->security->xss_clean($this->input->post('new_password'));
            $data_partner_university['password'] = md5($new_password);

            $updated = $this->partner_university_model->editPartnerUniversity($data_partner_university,$id_partner_university);

            redirect('/partner_university/partnerUniversity/edit');
        }

        $data['id_partner_university'] = $id_partner_university;
        
        $data['partnerUniversity'] = $this->partner_university_model->getPartnerUniversity($id_partner_university);
        $data['statusListByType'] = $this->partner_university_model->statusListByType('PartnerUniversity');
        
        // echo "<Pre>";print_r($data['partnerUniversity']);exit;

        $this->global['pageTitle'] = 'Partner University Portal : Change Partner University Password';
        $this->loadViews("partneruniversity/change_password", $this->global, $data, NULL);
    }


    function getPartnerUniversity()
    {
        $id_partner_category = $this->security->xss_clean($this->input->post('id_partner_category'));

        $partner_category = $this->partner_university_model->getPartnerUniversityCategory($id_partner_category);

        $name = $partner_category->name;
        $code = $partner_category->code;
        $value = 0;

        if($name == 'Franchise')
        {
            $value= 1;
        }
        echo $value;exit();
    }

    function getStateByCountry($id_country)
    {
            $results = $this->partner_university_model->getStateByCountryId($id_country);

            // echo "<Pre>"; print_r($programme_data);exit;
            $table="   
                <script type='text/javascript'>
                     $('select').select2();
                 </script>
         ";

            $table.="
            <select name='id_state' id='id_state' class='form-control'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;
    }


    // Copied From Scholarship Partner University


    function getStateByCountryForTraining($id_country)
    {
            $results = $this->partner_university_model->getStateByCountryId($id_country);

            // echo "<Pre>"; print_r($results);exit;
            $table="   
                <script type='text/javascript'>
                     $('select').select2();
                 </script>
         ";

            $table.="
            <select name='id_training_state' id='id_training_state' class='form-control'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;
    }

    function displayTrainingUniversityData($temp_details,$fee_structure_data)
    {
        // echo "<Pre>";print_r($fee_structure_data);exit;

        $table = "";
         if(!empty($fee_structure_data))
        {
            $amount = $fee_structure_data->amount;
            $installments = $fee_structure_data->installments;
            $id_fee_structure = $fee_structure_data->id;
            $id_training_center = $fee_structure_data->id_training_center;
            // $id_program_landscape = 0;
            $id_program_landscape = $fee_structure_data->id_program_landscape;
            $currency = $fee_structure_data->currency_code . " - " . $fee_structure_data->currency;

            $table .= "
        <input type='hidden' id='installment_amount_selected' name='installment_amount_selected' value='$amount'>
        <input type='hidden' id='installment_nos' name='installment_nos' value='$installments'>
        <input type='hidden' id='id_fee_structure' name='id_fee_structure' value='$id_fee_structure'>
        <input type='hidden' id='id_data_training_center' name='id_data_training_center' value='$id_training_center'>
        <input type='hidden' id='id_data_program_landscape' name='id_data_program_landscape' value='$id_program_landscape'>
        <input type='hidden' id='data_currency' name='data_currency' value='$currency'>
        ";
        }

        if(!empty($temp_details))
        {
            

        $table .= "
        <div class='form-container'>
         <h4 class='form-group-title'>Installment List</h4>

        <table  class='table' id='list-table'>
                  <tr>
                    <th>Sl. No</th>
                    <th>Installment Trigger On</th>
                    <th>Fee Item</th>
                    <th>Trgger Point</th>
                    <th>Frequency Mode</th>
                    <th>Amount</th>
                    <th>Action</th>
                </tr>";
                $total_amount = 0;
                    for($i=0;$i<count($temp_details);$i++)
                    {
        // echo "<Pre>";print_r($temp_details);exit;
                    $id = $temp_details[$i]->id;
                    $fee_name = $temp_details[$i]->fee_name;
                    $fee_code = $temp_details[$i]->fee_code;
                    $frequency_mode = $temp_details[$i]->frequency_mode;
                    $id_semester = $temp_details[$i]->id_semester;
                    $fee = $fee_code . " - " . $fee_name;
                    $trigger_name = $temp_details[$i]->trigger_name;
                    $amount = $temp_details[$i]->amount;

                    $j = $i+1;

                        $table .= "
                        <tr>
                            <td>$j</td>
                            <td>$id_semester</td>
                            <td>$fee</td>
                            <td>$trigger_name</td>
                            <td>$frequency_mode</td>
                            <td>$amount</td>
                            <td>
                                <a onclick='deleteSemesterDataByIdFeeStructureTrainingCenter($id)'>Delete</a>
                            <td>
                        </tr>";
                        $total_amount = $total_amount + $amount;
                    }

                    $table .= "
                        <tr>
                            <td colspan='4'></td>
                            <td>Total : </td>
                            <td>$total_amount</td>                           
                            <td>
                            <td>
                        </tr>";
        $table.= "</table>
        </div>
        ";


        }
        
        // $table .= "";
        
        return $table;
    }

    function uploadImage()
    {
        $resultprint = $this->input->post();

        $id = $this->security->xss_clean($this->input->post('id'));

        
        // echo "<Pre>"; print_r($id);exit();

        // echo "<Pre>"; print_r($_FILES['image']);exit;

        if($_FILES['image'])
        {  


        $certificate_name = $_FILES['image']['name'];
        $certificate_size = $_FILES['image']['size'];
        $certificate_tmp =$_FILES['image']['tmp_name'];
        
        // echo "<Pre>"; print_r($certificate_tmp);exit();

        $certificate_ext=explode('.',$certificate_name);
        $certificate_ext=end($certificate_ext);
        $certificate_ext=strtolower($certificate_ext);


        $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'Image File');

        $image_file = $this->uploadFile($certificate_name,$certificate_tmp,'Image File');

        

            if($image_file != '')
            {
                $data['image'] = $image_file;

                $sessionArray = array(
                    'partner_university_image' => $image_file
                    );

            }

            // echo "<Pre>"; print_r($data);exit;
            
            $result = $this->partner_university_model->editPartnerUniversity($data,$id);

            if($result && $sessionArray)
            {
                $this->session->set_userdata($sessionArray);
            }

            redirect($_SERVER['HTTP_REFERER']);

        }
        
        // echo "<Pre>"; print_r($_FILE);exit();
        // $tempData = $this->security->xss_clean($this->input->post('tempData'));
    }

    function checkPartnerUniversityPassword()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        
        // echo "<Pre>"; print_r($tempData);exit();
        
        $tempData['password'] = md5($tempData['old_password']);
        $company_user = $this->partner_university_model->checkPartnerUniversityPassword($tempData);

        if($company_user)
        {
            echo '1';exit;
        }
        else
        {
            echo '0';exit;
        }
    }

    function changeCompanyUserPassword()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        
        // echo "<Pre>"; print_r($tempData);exit();
        
        $tempData['password'] = md5($tempData['new_password']);
        $id_company_user = $tempData['id_company_user'];
        unset($tempData['id_company_user']);

        $updated = $this->partner_university_model->editCompanyUser($tempData,$id_company_user);

        echo $updated;exit;
    }

    function saveCompanyUserDetails()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
            // echo "<Pre>"; print_r($tempData);exit();
        $tempData['password'] = md5($tempData['password']);
        $inserted_id = $this->partner_university_model->saveCompanyUserDetails($tempData);

        if($inserted_id)
        {
            echo $inserted_id;
        }

    }

    function getPartnerUniversityDuplication()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $result = $this->partner_university_model->getPartnerUniversityDuplication($tempData);

        if($result)
        {
            print_r('0');exit;
        }
        else
        {
            print_r('1');exit;
        }       
    }

    function logout()
    {
        $isPartnerUniversityLoggedIn = $this->session->isPartnerUniversityLoggedIn;

        if($isPartnerUniversityLoggedIn == TRUE)
        {

        // echo $isStudentAdminLoggedIn;exit();

            $sessionArray = array(
                        'id_partner_university'=>'',
                        'partner_university_name'=>'',
                        'partner_university_code'=>'',
                        'partner_university_login_id'=> '',
                        'partner_university_last_login' => '',
                        'partner_university_image' => '',
                        'my_partner_university_session_id' => '',
                        'isPartnerUniversityLoggedIn' => FALSE
                    );
            $this->session->set_userdata($sessionArray);
        } 
        
        $this->isPartnerUniversityLoggedIn();
    }
}