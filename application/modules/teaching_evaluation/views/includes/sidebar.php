            <div class="sidebar">
                <div class="user-profile clearfix">
                    <a href="#" class="user-profile-link">
                        <span><img src="<?php echo BASE_PATH; ?>assets/img/user_profile.jpg"></span> <?php echo $name; ?>
                    </a>
                    <div class="dropdown">
                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                            <li><a href="/setup/user/profile">Edit Profile</a></li>
                            <li><a href="/setup/user/logout">Logout</a></li>
                        </ul>
                    </div>
                </div>
                <div class="sidebar-nav">
                    <h4>Setup</h4>
                    <ul>
                        <li><a href="/teaching_evaluation/questionPool/list">Question Pool</a></li>
                        <li><a href="/teaching_evaluation/question/list">Question</a></li>
                    </ul>
                    <h4>Evaluation Form</h4>
                    <ul>
                        <li><a href="/teaching_evaluation/setForm/list">Set Form</a></li>
                        <li><a href="/teaching_evaluation/publish/list">Publish</a></li>
                        
                    </ul>
                    <h4>Report Submission</h4>
                    <ul>
                        <li><a href="/teaching_evaluation/projectReportSubmission/approvalList">Report Submission Approve</a></li>
                        <li><a href="/teaching_evaluation/projectReportSubmission/list">Report Submission Summary</a></li>
                    </ul>
                    
                    <!--
                    <h4>Report</h4>
                    <ul>
                        <li><a href="/teaching_evaluation/welcome">Overall Response By Course</a></li>
                        <li><a href="/teaching_evaluation/assetSubCategory/list">Summary Report</a></li>
                    </ul> -->
                </div>
            </div>