<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Approve Project Report Submission</h3>
        </div>



            <div class="form-container">
                <h4 class="form-group-title">Student Details</h4>
                <div class='data-list'>
                    <div class='row'> 
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student Name :</dt>
                                <dd><?php echo ucwords($studentDetails->full_name);?></dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd><?php echo $studentDetails->nric ?></dd>
                            </dl>
                            <dl>
                                <dt>Student Email :</dt>
                                <dd><?php echo $studentDetails->email_id; ?></dd>
                            </dl>
                             <dl>
                                <dt>Nationality :</dt>
                                <dd><?php echo $studentDetails->nationality ?></dd>
                            </dl>                            
                        </div>        
                        
                        <div class='col-sm-6'>                           
                            <dl>
                                <dt>Intake :</dt>
                                <dd><?php echo $studentDetails->intake_name ?></dd>
                            </dl>
                            <dl>
                                <dt>Program :</dt>
                                <dd><?php echo $studentDetails->programme_name; ?></dd>
                            </dl>
                            <dl>
                                <dt>Qualification Type :</dt>
                                <dd><?php echo $studentDetails->qualification_code . " - " . $studentDetails->qualification_name; ?></dd>
                            </dl>
                            <dl>
                                <dt> :</dt>
                                <dd><?php echo ""; ?></dd>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>


    <form id="form_pr_entry" action="" method="post">


       

        
        <div class="form-container">
            <h4 class="form-group-title">Project Report Submission Details</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" readonly="readonly" value="<?php echo $projectReportSubmission->name;?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Description <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" readonly="readonly" value="<?php echo $projectReportSubmission->description;?>">
                    </div>
                </div>




                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Duration (Months) <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="duration" name="duration" readonly="readonly" value="<?php echo $projectReportSubmission->duration;?>">
                    </div>
                </div>
                

            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Start Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="from_dt" name="from_dt" autocomplete="off" readonly="readonly" value="<?php echo date('d-m-Y',strtotime($projectReportSubmission->from_dt));?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>End Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="from_dt" name="from_dt" autocomplete="off" readonly="readonly" value="<?php echo date('d-m-Y',strtotime($projectReportSubmission->to_dt));?>">
                    </div>
                </div>

               <div class="col-sm-4">
                    <div class="form-group">
                        <label>Status <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="date_time" name="date_time" value="<?php
                        if($projectReportSubmission->status == '0')
                        {
                            echo "Pending";
                        }
                        elseif($projectReportSubmission->status == '1')
                        {
                            echo "Approved";
                        }
                        elseif($projectReportSubmission->status == '2')
                        {
                            echo "Rejected";
                        }
                         ?>" readonly="readonly">
                    </div>
                </div>

            </div>

            <div class="row">


                <?php
            if($projectReportSubmission->status == '2')
            {
             ?>

                <div class="col-sm-4" id="view_reject">
                    <div class="form-group">
                        <label>Reject Reason <span class='error-text'>*</span></label>
                        <input type="text" id="reason" name="reason" class="form-control" value="<?php echo $projectReportSubmission->reason; ?>" readonly>
                    </div>
                </div>

            <?php
            }
            ?>

            </div>

            <?php
            if($projectReportSubmission->status == '0')
            {
             ?>


             <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <p> Approval <span class='error-text'>*</span></p>
                        <label class="radio-inline">
                            <input type="radio" id="ed1" name="status" value="1" onclick="hideRejectField()"><span class="check-radio"></span> Approve
                        </label>
                        <label class="radio-inline">
                            <input type="radio" id="ed2" name="status" value="2" onclick="showRejectField()"><span class="check-radio"></span> Reject
                        </label>
                    </div>
                </div>

            </div>

          <?php
            }
            ?>

            <div class="row">

                <div class="col-sm-4" id="view_reject" style="display: none">
                    <div class="form-group">
                        <label>Reason <span class='error-text'>*</span></label>
                        <input type="text" id="reason" name="reason" class="form-control">
                    </div>
                </div>
            </div>


        </div>


       

        <div class="button-block clearfix">
            <div class="bttn-group">
         <?php
        if($projectReportSubmission->status == '0')
        {
         ?>
                <button type="submit" class="btn btn-primary btn-lg">Save</button>

         <?php
        }
         ?>

                <a href="../approvalList" class="btn btn-link">Back</a>
            </div>
        </div>

        </form>

    </div>

        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
</div>


<script>

    $('select').select2();




  $(document).ready(function() {
        $("#form_pr_entry").validate({
            rules: {
                status: {
                    required: true
                },
                 reason: {
                    required: true
                }
            },
            messages: {
                status: {
                    required: "<p class='error-text'>Select Status",
                },
                reason: {
                    required: "<p class='error-text'>Reason Required",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    function showRejectField(){
            $("#view_reject").show();
    }

    function hideRejectField(){
            $("#view_reject").hide();
    }
</script>