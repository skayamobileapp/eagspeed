<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ProjectReportSubmission extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('project_report_submission_model');
        $this->isLoggedIn();
    }

    function list()
    {       
        if ($this->checkAccess('project_report_submission.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_program'] = $this->security->xss_clean($this->input->post('id_program'));
            $formData['id_qualification'] = $this->security->xss_clean($this->input->post('id_qualification'));
            $formData['status'] = '';
            $data['searchParam'] = $formData;


            
            $data['projectReportList'] = $this->project_report_submission_model->projectReportListSearch($formData);
            $data['programList'] = $this->project_report_submission_model->programList();
            $data['intakeList'] = $this->project_report_submission_model->intakeList();
            $data['qualificationList'] = $this->project_report_submission_model->qualificationList();


            //$data['maxLimit'] = $this->project_report_submission_model->getMaxLimit();
            // $data['check_limit'] = $this->project_report_submission_model->checkStudentProjectReport($check);


                     // echo "<Pre>";print_r($data);exit();

            $this->global['pageTitle'] = 'Campus Management System : List Project Report Submision';
            $this->loadViews("project_report_submission/list", $this->global, $data, NULL);
        }
    }

    function approvalList()
    {       
        if ($this->checkAccess('project_report_submission.approval_list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_program'] = $this->security->xss_clean($this->input->post('id_program'));
            $formData['id_qualification'] = $this->security->xss_clean($this->input->post('id_qualification'));
            $formData['status'] = '0';
            $data['searchParam'] = $formData;


            
            $data['projectReportList'] = $this->project_report_submission_model->projectReportListSearch($formData);
            $data['programList'] = $this->project_report_submission_model->programList();
            $data['intakeList'] = $this->project_report_submission_model->intakeList();
            $data['qualificationList'] = $this->project_report_submission_model->qualificationList();


            //$data['maxLimit'] = $this->project_report_submission_model->getMaxLimit();
            // $data['check_limit'] = $this->project_report_submission_model->checkStudentProjectReport($check);


                     // echo "<Pre>";print_r($data);exit();

            $this->global['pageTitle'] = 'Campus Management System : List Project Report Submision';
            $this->loadViews("project_report_submission/approval_list", $this->global, $data, NULL);
        }
    }


    function view($id = NULL)
    {
        if ($this->checkAccess('project_report_submission.approve') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            if ($id == null)
            {
                redirect('/teaching_evaluation/projectReportSubmission/approvalList');
            }

            if($this->input->post())
            {

                $status = $this->security->xss_clean($this->input->post('status'));
                $reason = $this->security->xss_clean($this->input->post('reason'));


                $data = array(
                    'status' => $status,
                    'reason' => $reason
                );

                 $result = $this->project_report_submission_model->updateProjectReport($data,$id);
                redirect('/teaching_evaluation/projectReportSubmission/approvalList');
            }

            $data['projectReportSubmission'] = $this->project_report_submission_model->getProjectReport($id);
            $data['studentDetails'] = $this->project_report_submission_model->getStudentByStudentId($data['projectReportSubmission']->id_student);

                // echo "<Pre>"; print_r($data);exit;
            $this->global['pageTitle'] = 'Campus Management System : Approve Project Report Submision';
            $this->loadViews("project_report_submission/view", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('project_report_submission.approve') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            if ($id == null)
            {
                redirect('/teaching_evaluation/projectReportSubmission/approvalList');
            }

            if($this->input->post())
            {

                // $status = $this->security->xss_clean($this->input->post('status'));
                // $reason = $this->security->xss_clean($this->input->post('reason'));


                // $data = array(
                //     'status' => $status,
                //     'reason' => $reason
                // );

                //  $result = $this->project_report_submission_model->updateProjectReport($data,$id);
                redirect('/teaching_evaluation/projectReportSubmission/approvalList');
            }

            $data['projectReportSubmission'] = $this->project_report_submission_model->getProjectReport($id);
            $data['studentDetails'] = $this->project_report_submission_model->getStudentByStudentId($data['projectReportSubmission']->id_student);

                // echo "<Pre>"; print_r($data);exit;
            $this->global['pageTitle'] = 'Campus Management System : View Project Report Submision';
            $this->loadViews("project_report_submission/edit", $this->global, $data, NULL);
        }
    }





    function getCompanyByCompanyType($id_company_type)
    {
        $id_student = $this->session->id_student;
        $id_intake = $this->session->id_intake;
        $id_program = $this->session->id_program;

            $results = $this->project_report_submission_model->getCompanyByCompanyTypeId($id_company_type,$id_program);

            // echo "<Pre>"; print_r($programme_data);exit;
            $table="   
                <script type='text/javascript'>
                     $('select').select2();
                 </script>


            <select name='id_company' id='id_company' class='form-control'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $registration_no = $results[$i]->registration_no;
            $table.="<option value=".$id.">".$registration_no  ." - " . $name.
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;
    }





























    function courseWithdraw()
    {
        $id_student = $this->session->id_student;
        $id_intake = $this->session->id_intake;
        $id_programme = $this->session->id_program;
        $user_id = $this->session->userId;

        if($this->input->post())
        {
            // $id_course = $this->security->xss_clean($this->input->post('id_course'));
            $reason = $this->security->xss_clean($this->input->post('reason'));
            $id_exam_register = $this->security->xss_clean($this->input->post('id_exam_register'));
            // $reason = $this->security->xss_clean($this->input->post('reason'));

                $exam_registration_data = $this->project_report_submission_model->getExamRegistration($id_exam_register);
            // echo "<Pre>";print_r($exam_registration_data);exit();

                 $data = array(
                        'id_exam_register' => $id_exam_register,
                        'id_exam_center' => $exam_registration_data->id_exam_center,
                        'id_course' => $exam_registration_data->id_course,
                        'id_semester' => $exam_registration_data->id_semester,
                        'id_student' => $exam_registration_data->id_student,
                        'id_intake' => $exam_registration_data->id_intake,
                        'id_programme' => $exam_registration_data->id_programme,
                        'reason' => $reason,
                        'status' => '0',
                        'created_by' => $user_id
                    );
                     // echo "<Pre>";print_r($data);exit();
                $insert_id = $this->project_report_submission_model->addBulkWithdraw($data);
                if ($insert_id)
                {
                    $update = array(
                        'is_bulk_withdraw' => $insert_id
                    );
                    $updated = $this->project_report_submission_model->updateExamRegistration($update,$id_exam_register);                       
                }
            redirect($_SERVER['HTTP_REFERER']);
        }

        $data['getStudentData'] = $this->edit_profile_model->getStudentData($id_student);
        $data['courseRegistrationList'] = $this->project_report_submission_model->getCourseFromExamRegister($id_intake,$id_programme,$id_student);
        $data['bulkWithdrawList'] = $this->project_report_submission_model->bulkWithdrawList($id_student);
        
                 // echo "<Pre>";print_r($data['courseRegistrationList']);exit();

        $this->global['pageTitle'] = 'Campus Management System : Add Student To Exam Center';
        $this->loadViews("project_report_application/bulk_withdraw", $this->global, $data, NULL);
    }

    function diffDates()
    {
        // // Declare two dates 
        // $start_date = strtotime("2018-06-08"); 
        // $end_date = strtotime("2018-09-01"); 
          
        // // Get the difference and divide into  
        // // total no. seconds 60/60/24 to get  
        // // number of days 
        // echo "Difference between two dates: "
        //     . ($end_date - $start_date)/60/60/24;exit();
    }
}

