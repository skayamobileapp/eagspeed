<?php $this->load->helper("form"); ?>

<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">

        <div class="page-title clearfix">
                <h3>Add Fee Structure </h3>
                <?php
                if($id_fee_details == NULL)
                {
                  ?>
                    <a href="<?php echo '../list' ?>" class="btn btn-link"> < Back</a>
                  <?php
                }
                ?>
        </div>



          <div class="form-container">
                <h4 class="form-group-title">Fee Structure Main Details</h4> 

            <div class="row">
                  

                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Fee Structure Code</label>
                          <input type="text" class="form-control" id="name" name="name" value="<?php echo $getProgrammeLandscapeLocal->code; ?>" readonly="readonly">
                      </div>
                  </div>


                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Fee Structure Name</label>
                          <input type="text" class="form-control" id="name" name="name" value="<?php echo $getProgrammeLandscapeLocal->name; ?>" readonly="readonly">
                      </div>
                  </div>    

                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Name Optional Language</label>
                          <input type="text" class="form-control" id="name_optional_language" name="name_optional_language" value="<?php echo $getProgrammeLandscapeLocal->name_optional_language; ?>" readonly="readonly">
                      </div>
                  </div>     

            </div>



            <div class="row">

                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Program</label>
                          <input type="text" class="form-control" id="year" name="year" value="<?php echo $getProgrammeLandscapeLocal->program_code . " - " . $getProgrammeLandscapeLocal->program; ?>" readonly="readonly">
                      </div>
                  </div>




                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Currency <span class='error-text'>*</span></label>
                          <select name="id_currency" id="id_currency" class="form-control" disabled="true">
                              <option value="">Select</option>
                              <?php
                              if (!empty($currencyList))
                              {
                                  foreach ($currencyList as $record)
                                  {?>
                                      <option value="<?php echo $record->id;?>"
                                        <?php
                                        if($record->id == $getProgrammeLandscapeLocal->id_currency)
                                        {
                                            echo 'selected';
                                        }
                                        ?>
                                      ><?php echo $record->code . " - " . $record->name;?>
                                      </option>
                              <?php
                                  }
                              }
                              ?>
                          </select>
                      </div>
                  </div>

            </div>



          </div>





        <br>






        <div class="form-container">
            <h4 class="form-group-title">Fee Structure Details</h4>          
            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>
            <div class="clearfix">
                <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                    <!-- <li role="presentation" class="active" ><a href="#invoice" class="nav-link border rounded text-center"
                            aria-controls="invoice" aria-selected="true"
                            role="tab" data-toggle="tab">Local Students</a>
                    </li>   -->  
                    
                    <!-- <li role="presentation"><a href="#tab_two" class="nav-link border rounded text-center"
                            aria-controls="tab_two" role="tab" data-toggle="tab">International Students</a>
                    </li>   -->
                    
                    <!-- <li role="presentation"><a href="#tab_three" class="nav-link border rounded text-center"
                            aria-controls="tab_three" role="tab" data-toggle="tab">Partner University</a>
                    </li> -->
                </ul>

                
                <div class="tab-content offers-tab-content">

                    <div role="tabpanel" class="tab-pane active" id="invoice">
                      <div class="col-12 mt-4">



                        <form id="form_one" action="" method="post">

                          <div class="form-container">
                                  <h4 class="form-group-title">Fee Structure Details</h4> 

                              <div class="row">

                                    <div class="col-sm-3">
                                          <div class="form-group">
                                              <label>Fee Item <span class='error-text'>*</span></label>
                                              <select name="one_id_fee_item" id="one_id_fee_item" class="form-control" required>
                                                  <option value="">Select</option>
                                                  <?php
                                                  if (!empty($feeSetupList))
                                                  {
                                                      foreach ($feeSetupList as $record)
                                                      {?>
                                                          <option value="<?php echo $record->id;?>"
                                                            <?php
                                                            if($feeStructureDetail->id_fee_item == $record->id)
                                                            {
                                                              echo 'selected';
                                                            }
                                                            ?>
                                                          ><?php echo $record->code . " - " . $record->name;?>
                                                          </option>
                                                  <?php
                                                      }
                                                  }
                                                  ?>
                                              </select>
                                          </div>
                                      </div>


                                      <!-- <div class="col-sm-3">
                                          <div class="form-group">
                                              <label>Trigger On <span class='error-text'>*</span></label>
                                              <select name="one_id_fee_structure_trigger" id="one_id_fee_structure_trigger" class="form-control">
                                                  <option value="">Select</option>
                                                  <?php
                                                  if (!empty($getFeeStructureTriggerList))
                                                  {
                                                      foreach ($getFeeStructureTriggerList as $record)
                                                      {?>
                                                          <option value="<?php echo $record->id;?>"
                                                          ><?php echo $record->name;?>
                                                          </option>
                                                  <?php
                                                      }
                                                  }
                                                  ?>
                                              </select>
                                          </div>
                                      </div> -->


                                      <div class="col-sm-3">
                                          <div class="form-group">
                                              <label>Amount <span class='error-text'>*</span></label>
                                              <input type="number" class="form-control" id="one_amount" name="one_amount"  value="<?php echo $feeStructureDetail->amount; ?>" required>
                                              <input type="hidden" class="form-control" id="id_programme" name="id_programme"  value="<?php echo $getProgrammeLandscapeLocal->id_programme; ?>">
                                          </div>
                                      </div>


                                      <div class="col-sm-3">
                                          <div class="form-group">
                                              <p>Registration Fee <span class='error-text'>*</span></p>
                                              <label class="radio-inline">
                                              <input type="radio" name="is_registration_fee" id="is_registration_fee" value="1" <?php if($feeStructureDetail->is_registration_fee=='1') {
                                                  echo "checked=checked";
                                              };?>><span class="check-radio"></span> Yes
                                              </label>
                                              <label class="radio-inline">
                                              <input type="radio" name="is_registration_fee" id="is_registration_fee" value="0" <?php if($feeStructureDetail->is_registration_fee=='0') {
                                                  echo "checked=checked";
                                              };?>>
                                              <span class="check-radio"></span> No
                                              </label>
                                          </div>
                                      </div>



                                      <div class="col-sm-3">
                                          <div class="form-group">
                                              <label>Currency <span class='error-text'>*</span></label>
                                              <input type="text" class="form-control" id="one_currency" name="one_currency" value="<?php echo $getProgrammeLandscapeLocal->currency_name; ?>" readonly>
                                          </div>
                                      </div>

                              </div>

                          </div>

                          <div class="button-block clearfix">
                            <div class="bttn-group">
                                  <button type="button" class="btn btn-primary btn-lg" onclick="getProgrammeFeeStructureDuplication()">Add</button>
                                  <?php
                                  if($id_fee_details != NULL)
                                  {
                                    ?>
                                    <a href="<?php echo '../../addFeeStructure/'. $id_program_landscape ?>" class="btn btn-link">Cancel</a>
                                    <?php
                                  }
                                  ?>

                              </div>

                          </div>


                        </form>


                        <!-- <div class="row">

                          <div class="col-sm-4">
                              <div class="form-group">
                                  <label>Total Amount</label>
                                  <input type="text" class="form-control" id="start_date" name="start_date" value="<?php echo $getProgrammeLandscapeLocal->total_amount; ?>" readonly="readonly" >
                              </div>
                          </div>

                        </div> -->


                        <div class="form-container">
                            <h4 class="form-group-title">Fee Structure List</h4> 


                        

                            <div class="custom-table">
                              <table class="table" id="list-table">
                                <thead>
                                  <tr>
                                    <th>Sl. No</th>
                                    <th>Fee Item</th>
                                    <th>Total Amount (RM)</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <?php
                                  if (!empty($feeStructureLocalList))
                                  {
                                    $i = 1;
                                    $total_amount = 0;
                                    foreach ($feeStructureLocalList as $record)
                                    {
                                      $tax_amount = ($record->amount * 0.01) * $tax_percentage;
                                      $tax_amount = number_format($tax_amount, 2, '.', ',');
                                  ?>
                                      <tr>
                                        <td><?php echo $i ?></td>
                                        <td><?php echo $record->fee_structure_code . " - " . $record->fee_structure ?>                                
                                        </td>
                                        
                                        <td><?php echo $record->amount ?></td>
                                        
                                      </tr>
                                  <?php
                                  $total_amount = $total_amount + $record->amount;
                                  $i++;
                                    }
                                     $total_amount = number_format($total_amount, 2, '.', ',');
                                    ?>

                                    <tr >
                                        <td bgcolor=""></td>
                                        <td bgcolor="" style="text-align: center;"><b>Total Amount :</b></td>
                                        <td bgcolor="">
                              <input type="hidden" id="local_amount" name="local_amount" value="<?php echo $total_amount; ?>">

                              <b><?php echo $total_amount . " (RM) ";  ?></b></td>
                                        <!-- <td class="text-center"> -->
                                        <td bgcolor=""></td>
                                      </tr>
                                    <?php
                                  }
                                  ?>
                                </tbody>
                              </table>
                            </div>

                        </div>


                      </div>

                    </div>





            </div>


        </div> 





           
        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>


</script>
