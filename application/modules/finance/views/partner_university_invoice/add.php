<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Partner University Invoice</h3>
        </div>

    <form id="form_main_invoice" action="" method="post">
    
        <div class="form-container">
            <h4 class="form-group-title">Partner University Invoice</h4>
            
            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Type<span class='error-text'>*</span></label>
                        <select name="type" id="type" class="form-control">
                            <option value="Partner University">Partner University</option>
                        </select>
                    </div>
                </div> 


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Program <span class='error-text'>*</span></label>
                        <select name="id_programme" id="id_programme" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($programmeList))
                            {
                                foreach ($programmeList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div> 


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Partner University <span class='error-text'>*</span></label>
                        <select name="id_student" id="id_student" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($partnerUniversityList))
                            {
                                foreach ($partnerUniversityList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div> 


                                  
            </div>

            <div class="row">
                 <div  id='student'>
                </div> 
            </div>
        </div>

        <div class="form-container">
            <div class="row">
                <!-- <div class="col-sm-4">
                    <div class="form-group">
                        <label>Type Of Invoice <span class='error-text'>*</span></label>
                        <select name="type_of_invoice" id="type_of_invoice" class="form-control">
                            <option value="">Select</option>
                            <option value="Student">Student</option>
                            <option value="Applicant">Applicant</option>
                        </select>
                    </div>
                </div> -->

                <div class="col-sm-4">
                    <div class="form-group">
                      <label>Currency <span class='error-text'>*</span></label>
                      <select name="currency" id="currency" class="form-control">
                          <option value="">Select</option>
                          <option value="MYR">MYR</option>
                          <option value="USD">USD</option>
                      </select>
                    </div>
                </div>  

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Total Amount <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="total_amount" name="total_amount" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Date Time <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="date_time" name="date_time" value="<?php echo date('d-m-Y'); ?>" readonly="readonly">
                    </div>
                </div>

            </div>

            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Remarks </label>
                        <input type="text" class="form-control" id="remarks" name="remarks">
                    </div>
                </div>
            </div>
        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg" >Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>
    
    <!-- </form> -->






            <div class="form-container">
                    <h4 class="form-group-title"> Invoice Details</h4>          
                    <div class="m-auto text-center">
                        <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
                    </div>
                    <div class="clearfix">
                        <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                            <li role="presentation" class="active" ><a href="#invoice" class="nav-link border rounded text-center"
                                    aria-controls="invoice" aria-selected="true"
                                    role="tab" data-toggle="tab">Fee Item Details</a>
                            </li>
                            <li role="presentation" id="view_student_tab"><a href="#tab_two" class="nav-link border rounded text-center" aria-controls="tab_two" role="tab" data-toggle="tab">Student Details</a>
                            </li>
                        </ul>

                        
                        <div class="tab-content offers-tab-content">

                            <div role="tabpanel" class="tab-pane active" id="invoice">
                                <div class="col-12 mt-4">




                                <!-- <form id="form_main_invoice_details" action="" method="post"> -->

                                    <div class="form-container">
                                        <h4 class="form-group-title">Fee Item Details</h4>

                                        <div class="row">

                                            <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label>Fee Item <span class='error-text'>*</span></label>
                                                        <select name="id_fee_item" id="id_fee_item" class="form-control">
                                                            <option value="">Select</option>
                                                            <?php
                                                            if (!empty($feeSetupList))
                                                            {
                                                                foreach ($feeSetupList as $record)
                                                                {?>
                                                                    <option value="<?php echo $record->id;?>"
                                                                    ><?php echo $record->name;?>
                                                                    </option>
                                                            <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>


                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label>Amount <span class='error-text'>*</span></label>
                                                        <input type="number"  class="form-control" id="amount" name="amount">
                                                    </div>
                                                </div>
                                          
                                            <div class="col-sm-4">
                                                <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="saveData()">Add</button>
                                            </div>
                                        </div>

                                        <div id="view">
                                            
                                        </div>            
                                    </div>        

                                        

                                    <!-- </form> -->


                                </div> 
                            </div>




                            <div role="tabpanel" class="tab-pane" id="tab_two">
                                <div class="col-12 mt-4">




                                
                                    <!-- <form id="form_student_data" action="" method="post"> -->


                                        <div class="form-container">
                                            <h4 class="form-group-title">Search Student For Invoice Generation</h4>
                                            <h4>Invoice Total Amount = ( Total Amount ) X (No. Of Selected Students)  </h4>
                                            
                                            <h4 >Search Student For Invoice Generation</h4>


                                            <div class="row"> 

                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label>Intake </label>
                                                        <select name="id_intake" id="id_intake" class="form-control" >
                                                            <option value="">-- All --</option>
                                                            <?php
                                                            if (!empty($intakeList))
                                                            {
                                                                foreach ($intakeList as $record)
                                                                {?>
                                                             <option value="<?php echo $record->id;  ?>">
                                                                <?php echo $record->year . " - " . $record->name;?>
                                                             </option>
                                                            <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>  


                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label>Student Name</label>
                                                        <input type="text" class="form-control" id="full_name" name="full_name">
                                                    </div>
                                                </div>

                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label>Student Email</label>
                                                        <input type="text" class="form-control" id="email_id" name="email_id">
                                                    </div>
                                                </div>               
                                                
                                            </div>


                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" onclick="searchStudents()">Search</button>
                                              </div>

                                        </div>


                                        <div class="form-container" style="display: none;" id="view_student_display">
                                            <h4 class="form-group-title">Advisor Tagging For Student</h4>


                                            <div  id='view_student'>
                                            </div>

                                        </div>


                                    <!-- </form> -->





                                </div>

                            </div>







                        </div>

                    </div>
            

            </div>




    </form>





    

        </div>

    </div>

        </div>

        <footer class="footer-wrapper">
                <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>

<script>

    function searchStudents()
    {
        // if($('#form_main_invoice').valid())
        // {

        var tempPR = {};
        tempPR['id_university'] = $("#id_student").val();
        tempPR['id_program'] = $("#id_programme").val();
        tempPR['id_intake'] = $("#id_intake").val();
        tempPR['full_name'] = $("#full_name").val();
        tempPR['email_id'] = $("#email_id").val();
            $.ajax(
            {
               url: '/finance/partnerUniversityInvoice/searchStudents',
               type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_student_display").show();
                $("#view_student").html(result);
               }
            });
        // }
    }




    function validateDetailsData()
    {
        // if($('#form_main_invoice').valid())
        // {
            // console.log($("#view").html());
            // var addedProgam = $("#view").html();
            // if(addedProgam=='')
            // {
            //     alert("Add Invoice Details");
            // }
            // else
            // {
                // document.getElementById("form_student_data").submit();
                // document.getElementById("form_main_invoice").submit();

                $('#form_main_invoice').submit(doubleSubmit);
                // $('#form_main_invoice').submit();
            // }
        // }    
    }

    function doubleSubmit(e1)
    {
        e1.preventDefault();
        e1.stopPropagation();
        var post_form1 = $.post($(this).action, $(this).serialize());

        post_form1.done(function(result)
        {
            // would be nice to show some feedback about the first result here
            $('#form_student_data').submit();
        });
    };




    $('select').select2();

    function getStudentByProgramme()
    {
        var tempPR = {};
        tempPR['id_program'] = $("#id_programme").val();
        tempPR['id_intake'] = $("#id_intake").val();
        tempPR['type'] = $("#type").val();
        // tempPR['id'] = $("#id").val();
        if(tempPR['id_program'] != '' && tempPR['id_intake'] != '' && tempPR['type'] != '')
        {
            $.ajax(
            {
               url: '/finance/partnerUniversityInvoice/getStudentByProgrammeId',
                type: 'POST',
               data:
               {
                formData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#student").html(result);
               }
            });
        }   
    }

    // function enableStudentsForPartnerUniversity(id_partner_university)
    // {
    //     if(id_partner_university != '')
    //     {
    //         $("#view_student_tab").show();
    //     }
    //     else
    //     {
    //         $("#view_student_tab").hide();
    //     }
    // }


 //    function getStudentByProgramme(id)
 //    {

 //     $.get("/finance/mainInvoice/getStudentByProgrammeId/"+id, function(data, status){
   
 //        $("#student").html(data);
 //        // $("#view_programme_details").html(data);
 //        // $("#view_programme_details").show();
 //    });
 // }

     function getStudentByStudentId(id)
     {
        $.get("/finance/partnerUniversityInvoice/getStudentByStudentId/"+id, function(data, status){
       
            $("#view_student_details").html(data);
            $("#view_student_details").show();
        });
     }

     function getApplicantByApplicantId(id)
     {
        $.get("/finance/partnerUniversityInvoice/getApplicantByApplicantId/"+id, function(data, status){
       
            $("#view_student_details").html(data);
            $("#view_student_details").show();
        });
     }

    function opendialog()
    {
        $("#id_fee_item").val('');
        $("#amount").val('');
        // $("#id").val('0');                    
        $('#myModal').modal('show');

    }
    function saveData()
    {
        // if($('#form_main_invoice_details').valid())
        // {
        var tempPR = {};
        tempPR['id_fee_item'] = $("#id_fee_item").val();
        tempPR['amount'] = $("#amount").val();
        // tempPR['id'] = $("#id").val();
            $.ajax(
            {
               url: '/finance/partnerUniversityInvoice/tempadd',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view").html(result);
                var ta = $("#inv-total-amount").val();
                $("#total_amount").val(ta);
                $('#myModal').modal('hide');
               }
            });
        // }
    }

    function deleteTempData(id) {
         $.ajax(
            {
               url: '/finance/partnerUniversityInvoice/tempDelete/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view").html(result);
                var ta = $("#inv-total-amount").val();
                $("#total_amount").val(ta);
               }
            });
    }


    function getTempData(id) {
        $.ajax(
            {
               url: '/finance/partnerUniversityInvoice/tempedit/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(resultfromjson)
               {
                    result = JSON.parse(resultfromjson);
                    $("#dt_fund").val(result['dt_fund']);
                    $("#dt_department").val(result['dt_department']);
                    $("#id").val(id);
                    $('#myModal').modal('show');
               }
            });
    }

    

    $(document).ready(function() {
        $("#form_main_invoice_details").validate({
            rules: {
                id_fee_item: {
                    required: true
                },
                amount: {
                    required: true
                }
            },
            messages: {
                id_fee_item: {
                    required: "<p class='error-text'>Select Fee Item</p>",
                },
                amount: {
                    required: "<p class='error-text'>Amount Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


    $(document).ready(function() {
        $("#form_main_invoice").validate({
            rules: {
                total_amount: {
                    required: true
                },
                id_programme: {
                    required: true
                },
                id_student: {
                    required: true
                },
                date_time: {
                    required: true
                },
                type: {
                    required: true
                },
                currency: {
                    required: true
                }
            },
            messages: {
                total_amount: {
                    required: "<p class='error-text'>Enter Details For Total Amount</p>",
                },
                id_programme: {
                    required: "<p class='error-text'>Select Program</p>",
                },
                id_student: {
                    required: "<p class='error-text'>Select Applicant /Student  </p>",
                },
                date_time: {
                    required: "<p class='error-text'>Select Date </p>",
                },
                type: {
                    required: "<p class='error-text'>Select Type</p>",
                },
                currency: {
                    required: "<p class='error-text'>Select Currency </p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>