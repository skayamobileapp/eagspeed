<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Cororate Invoice</h3>
        </div>

    <form id="form_main_invoice" action="" method="post">
    
        <div class="form-container">
            <h4 class="form-group-title">Invoice</h4>
            
            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Type <span class='error-text'>*</span></label>
                        <select name="type" id="type" class="form-control" onchange="getStudentByProgramme()">
                            <!-- <option value="">Select</option> -->
                            <option value="CORPORATE">CORPORATE</option>
                            <!-- <option value="Student">Student</option> -->
                        </select>
                    </div>
                </div>                


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Fee Type <span class='error-text'>*</span></label>
                        <select name="fee_type" id="fee_type" class="form-control" onchange="showDivByFeeType()">
                            <option value="">Select</option>
                            <option value="Individual">Individual</option>
                            <option value="Registration">Registration</option>
                        </select>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Program <span class='error-text'>*</span></label>
                        <select name="id_programme" id="id_programme" class="form-control" onchange="getRegistrationFeeListByProgramme()">
                            <option value="">Select</option>
                            <?php
                            if (!empty($programmeList))
                            {
                                foreach ($programmeList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div> 



            </div>

            <div class="row">

                <div class="col-sm-4">
                  <div class="form-group">
                     <label><span id='label_span_for_type'></span> <span class='error-text'>*</span></label>
                     <span id="view_student">
                        <select class="form-control" id='id_student' name='id_student'>
                            <option value=''></option>
                          </select>
                     </span>
                  </div>
                </div>

                                  
            </div>

            <div class="row">
                 <div  id='student'>
                </div> 
            </div>
        </div>

        <div id="view_student_details"  style="display: none;">
        </div>

        <div class="form-container">
            <div class="row">
                <!-- <div class="col-sm-4">
                    <div class="form-group">
                        <label>Type Of Invoice <span class='error-text'>*</span></label>
                        <select name="type_of_invoice" id="type_of_invoice" class="form-control">
                            <option value="">Select</option>
                            <option value="Student">Student</option>
                            <option value="Applicant">Applicant</option>
                        </select>
                    </div>
                </div> -->

                <div class="col-sm-4">
                    <div class="form-group">
                      <label>Currency <span class='error-text'>*</span></label>
                      <select name="currency" id="currency" class="form-control">
                          <option value="">Select</option>
                            <?php
                            if (!empty($currencyList))
                            {
                                foreach ($currencyList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                      </select>
                    </div>
                </div>  

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Total Amount <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="total_amount" name="total_amount" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Date Time <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="date_time" name="date_time" value="<?php echo date('d-m-Y'); ?>" readonly="readonly">
                    </div>
                </div>

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Remarks </label>
                        <input type="text" class="form-control" id="remarks" name="remarks">
                    </div>
                </div>
            </div>
        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="button" class="btn btn-primary btn-lg" onclick="validateDetailsData()">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>
    
    







        <div id="view_registration_fee_details">
        </div>





        <div class="form-container" id="employee_search_area" style="display: none">
            <h4 class="form-group-title">Search Employee For Programme Registration</h4>
            <h4 >Search Employee For Programme Registration</h4>


            <div class="row">

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Employee Name</label>
                        <input type="text" class="form-control" id="full_name" name="full_name">
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Employee Email</label>
                        <input type="text" class="form-control" id="email_id" name="email_id">
                    </div>
                </div>               
                
            </div>


            <div class="modal-footer">
                <button type="button" class="btn btn-default" onclick="searchStudents()">Search</button>
            </div>


            <div class="form-container" style="display: none;" id="view_employee_display">
                <h4 class="form-group-title">Employee Programme Registration</h4>


                <div  id='view_employee'>
                </div>

            </div>



        </div>



    </form>



    <form id="form_main_invoice_details" action="" method="post">

        <div class="form-container" id="invoice_details">
            <h4 class="form-group-title">Main Invoice Details</h4>

            <div class="row">

                <div class="col-sm-4">
                        <div class="form-group">
                            <label>Fee Item <span class='error-text'>*</span></label>
                            <select name="id_fee_item" id="id_fee_item" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($feeSetupList))
                                {
                                    foreach ($feeSetupList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;?>"
                                        ><?php echo $record->name;?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Amount <span class='error-text'>*</span></label>
                            <input type="number"  class="form-control" id="amount" name="amount">
                        </div>
                    </div>
              
                <div class="col-sm-4">
                    <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="saveData()">Add</button>
                </div>
            </div>

            <div id="view">
                
            </div>            
        </div>   

            
    </form>

        </div>

    </div>

        </div>

        <footer class="footer-wrapper">
                <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>

<script>

    $('select').select2();

    getStudentByProgramme();

    function validateDetailsData()
    {
        if($('#form_main_invoice').valid())
        {
            console.log($("#view").html());
            var addedProgam = $("#view").html();
            if(addedProgam=='')
            {
                alert("Add Invoice Details");
            }
            else
            {
                $('#form_main_invoice').submit();
            }
        }    
    }


    function showDivByFeeType()
    {
        var fee_type = $("#fee_type").val();
        var id_programme = $("#id_programme").val();

        if(fee_type == 'Individual')
        {            
            $("#invoice_details").show();
            $("#employee_search_area").hide();
        }
        else if(fee_type == 'Registration')
        {
            getRegistrationFeeListByProgramme();
            $("#invoice_details").hide();
            $("#employee_search_area").show();
        }
    }

    function getRegistrationFeeListByProgramme()
    {
        var fee_type = $("#fee_type").val();
        var id_programme = $("#id_programme").val();

        if(fee_type == 'Registration')
        {
            if(id_programme != '')
            {
                var tempPR = {};
                tempPR['id_programme'] = id_programme;

                $.ajax(
                {
                   url: '/finance/mainInvoiceCorporate/getRegistrationFeeListByProgramme',
                    type: 'POST',
                   data:
                   {
                    formData: tempPR
                   },
                   error: function()
                   {
                    alert('Something is wrong');
                   },
                   success: function(result)
                   {
                    $("#view_registration_fee_details").html(result);
                    var registration_fee_total_amount = $("#registration_fee_total_amount").val();
                    var id_fee_master_currency = $("#id_fee_master_currency").val();

                    if(id_fee_master_currency != '')
                    {
                        $("#currency").find('option[value="'+id_fee_master_currency+'"]').attr('selected',true);
                        $('select').select2();
                    }

                    // alert(result);

                    $("#total_amount").val(registration_fee_total_amount);
                    $("#invoice_details").hide();
                    alert('Here Total Amount '+ registration_fee_total_amount + ' Is Multiplied to the No. Of Employees Selected');
                   }
                });
            }
        }
        else
        {
            $("#invoice_details").show();
        }
    }


    function searchStudents()
    {
        var id_student = $("#id_student").val();
        if(id_student != '')
        {

        var tempPR = {};
        tempPR['id_advisor'] = '';
        tempPR['full_name'] = $("#full_name").val();
        tempPR['email_id'] = $("#email_id").val();
        tempPR['id_company'] = id_student;
            $.ajax(
            {
               url: '/finance/mainInvoiceCorporate/searchStudents',
               type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_employee_display").show();
                $("#view_employee").html(result);
               }
            });
        }
        else
        {
            alert('Select CORPORATE Company To Search Employees');
        }
    }


    function getStudentByProgramme()
    {
        var labelnric = $("#type").val();
        $("#label_span_for_type").html(labelnric);

        var tempPR = {};
        tempPR['type'] = $("#type").val();
        // alert(labelnric);
        if(tempPR['type'] != '')
        {
            $.ajax(
            {
               url: '/finance/mainInvoiceCorporate/getStudentByProgram',
                type: 'POST',
               data:
               {
                formData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_student").html(result);
               }
            });
        }   
    }

    function getType(type)
    {
        if(type == 'Partner University')
        {
            $("#view_intake").hide();
            $("#view_partner_university").show();
        }
    }

    function getStudentByStudentId(id)
    {
        $.get("/finance/mainInvoiceCorporate/getStudentByStudentId/"+id, function(data, status){
       
            $("#view_student_details").html(data);
            $("#view_student_details").show();
        });
    }

     function getApplicantByApplicantId(id)
     {
        $.get("/finance/mainInvoiceCorporate/getApplicantByApplicantId/"+id, function(data, status){
       
            $("#view_student_details").html(data);
            $("#view_student_details").show();
        });
     }

    function opendialog()
    {
        $("#id_fee_item").val('');
        $("#amount").val('');
        // $("#id").val('0');                    
        $('#myModal').modal('show');

    }
    function saveData()
    {
        if($('#form_main_invoice_details').valid())
        {
        var tempPR = {};
        tempPR['id_fee_item'] = $("#id_fee_item").val();
        tempPR['amount'] = $("#amount").val();
        // tempPR['id'] = $("#id").val();
            $.ajax(
            {
               url: '/finance/mainInvoiceCorporate/tempadd',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view").html(result);
                var ta = $("#inv-total-amount").val();
                $("#total_amount").val(ta);
                $('#myModal').modal('hide');
               }
            });
        }
    }

    function deleteTempData(id) {
         $.ajax(
            {
               url: '/finance/mainInvoiceCorporate/tempDelete/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view").html(result);
                var ta = $("#inv-total-amount").val();
                $("#total_amount").val(ta);
               }
            });
    }


    function getTempData(id) {
        $.ajax(
            {
               url: '/finance/mainInvoiceCorporate/tempedit/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(resultfromjson)
               {
                    result = JSON.parse(resultfromjson);
                    $("#dt_fund").val(result['dt_fund']);
                    $("#dt_department").val(result['dt_department']);
                    $("#id").val(id);
                    $('#myModal').modal('show');
               }
            });
    }

    

    $(document).ready(function() {
        $("#form_main_invoice_details").validate({
            rules: {
                id_fee_item: {
                    required: true
                },
                amount: {
                    required: true
                }
            },
            messages: {
                id_fee_item: {
                    required: "<p class='error-text'>Select Fee Item</p>",
                },
                amount: {
                    required: "<p class='error-text'>Amount Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


    $(document).ready(function() {
        $("#form_main_invoice").validate({
            rules: {
                total_amount: {
                    required: true
                },
                id_programme: {
                    required: true
                },
                id_student: {
                    required: true
                },
                date_time: {
                    required: true
                },
                type: {
                    required: true
                },
                id_intake: {
                    required: true
                },
                currency: {
                    required: true
                },
                fee_type: {
                    required: true
                }
            },
            messages: {
                total_amount: {
                    required: "<p class='error-text'>Enter Details For Total Amount</p>",
                },
                id_programme: {
                    required: "<p class='error-text'>Select Program</p>",
                },
                id_student: {
                    required: "<p class='error-text'>Select Applicant /Student  </p>",
                },
                date_time: {
                    required: "<p class='error-text'>Select Date </p>",
                },
                type: {
                    required: "<p class='error-text'>Select Type</p>",
                },
                id_intake: {
                    required: "<p class='error-text'>Select Intake </p>",
                },
                currency: {
                    required: "<p class='error-text'>Select Currency </p>",
                },
                fee_type: {
                    required: "<p class='error-text'>Select Fee Type </p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>