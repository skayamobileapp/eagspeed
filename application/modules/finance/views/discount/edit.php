<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Discount</h3>
        </div>
        <form id="form_fee_category" action="" method="post">
            <div class="form-container">
                <h4 class="form-group-title">Discount Details</h4> 

                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Code <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="code" name="code" value="<?php echo $discount->code ?>">
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Discount Type <span class='error-text'>*</span></label>
                            <select name="id_discount_type" id="id_discount_type" class="form-control"  onchange="showdaterange()">
                                <option value="">Select</option>
                                <option value="Fixed" <?php if($discount->id_discount_type=='Fixed') { echo "selected=selected";} ?>>Fixed For Every Month</option>
                                <option value="Dynamic" <?php if($discount->id_discount_type=='Dynamic') { echo "selected=selected";} ?>>Dynamic Range</option>
                               
                            </select>
                        </div>
                    </div>




                </div>


                <div class="row"  id="dynamicdaterange" style="display:none;">


                      <div class="col-sm-4">
                        <div class="form-group">
                            <label>Start Date of Month <span class='error-text'>*</span></label>
                             <select name="start_date_of_month" id="start_date_of_month" class="form-control">
                                <option value="">Select</option>
                                <option value="1st" <?php if($discount->start_date_of_month=='1st') { echo "selected=selected";} ?>>1st of Every Month</option>
                                <option value="2nd" <?php if($discount->start_date_of_month=='2nd') { echo "selected=selected";} ?>>2nd of Every Month</option>
                                <option value="3rd" <?php if($discount->start_date_of_month=='3rd') { echo "selected=selected";} ?>>3rd of Every Month</option>
                                <option value="4th" <?php if($discount->start_date_of_month=='4th') { echo "selected=selected";} ?>>4th of Every Month</option>
                                <option value="5th" <?php if($discount->start_date_of_month=='5th') { echo "selected=selected";} ?>>5th of Every Month</option>
                                <option value="6th" <?php if($discount->start_date_of_month=='6th') { echo "selected=selected";} ?>>6th of Every Month</option>
                                <option value="7th" <?php if($discount->start_date_of_month=='7th') { echo "selected=selected";} ?>>7th of Every Month</option>
                                <option value="8th" <?php if($discount->start_date_of_month=='8th') { echo "selected=selected";} ?>>8th of Every Month</option>
                                <option value="9th" <?php if($discount->start_date_of_month=='9th') { echo "selected=selected";} ?>>9th of Every Month</option>
                                <option value="10th" <?php if($discount->start_date_of_month=='10th') { echo "selected=selected";} ?>>10th of Every Month</option>
                                <option value="11th" <?php if($discount->start_date_of_month=='11th') { echo "selected=selected";} ?>>11th of Every Month</option>
                                <option value="12th" <?php if($discount->start_date_of_month=='12th') { echo "selected=selected";} ?>>12th of Every Month</option>
                                <option value="13th" <?php if($discount->start_date_of_month=='13th') { echo "selected=selected";} ?>>13th of Every Month</option>
                                <option value="14th" <?php if($discount->start_date_of_month=='14th') { echo "selected=selected";} ?>>14th of Every Month</option>
                                <option value="15th" <?php if($discount->start_date_of_month=='15th') { echo "selected=selected";} ?>>15th of Every Month</option>
                                <option value="last_day" <?php if($discount->start_date_of_month=='last_day') { echo "selected=selected";} ?>>last day of Every Month</option>
                               
                            </select>
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>End Date of Month <span class='error-text'>*</span></label>
                            <select name="end_date_of_month" id="end_date_of_month" class="form-control">
                                <option value="">Select</option>
                                <option value="1st" <?php if($discount->end_date_of_month=='1st') { echo "selected=selected";} ?>>1st of Every Month</option>
                                <option value="2nd" <?php if($discount->end_date_of_month=='2nd') { echo "selected=selected";} ?>>2nd of Every Month</option>
                                <option value="3rd" <?php if($discount->end_date_of_month=='3rd') { echo "selected=selected";} ?>>3rd of Every Month</option>
                                <option value="4th" <?php if($discount->end_date_of_month=='4th') { echo "selected=selected";} ?>>4th of Every Month</option>
                                <option value="5th" <?php if($discount->end_date_of_month=='5th') { echo "selected=selected";} ?>>5th of Every Month</option>
                                <option value="6th" <?php if($discount->end_date_of_month=='6th') { echo "selected=selected";} ?>>6th of Every Month</option>
                                <option value="7th" <?php if($discount->end_date_of_month=='7th') { echo "selected=selected";} ?>>7th of Every Month</option>
                                <option value="8th" <?php if($discount->end_date_of_month=='8th') { echo "selected=selected";} ?>>8th of Every Month</option>
                                <option value="9th" <?php if($discount->end_date_of_month=='9th') { echo "selected=selected";} ?>>9th of Every Month</option>
                                <option value="10th" <?php if($discount->end_date_of_month=='10th') { echo "selected=selected";} ?>>10th of Every Month</option>
                                <option value="11th" <?php if($discount->end_date_of_month=='11th') { echo "selected=selected";} ?>>11th of Every Month</option>
                                <option value="12th" <?php if($discount->end_date_of_month=='12th') { echo "selected=selected";} ?>>12th of Every Month</option>
                                <option value="13th" <?php if($discount->end_date_of_month=='13th') { echo "selected=selected";} ?>>13th of Every Month</option>
                                <option value="14th" <?php if($discount->end_date_of_month=='14th') { echo "selected=selected";} ?>>14th of Every Month</option>
                                <option value="15th" <?php if($discount->end_date_of_month=='15th') { echo "selected=selected";} ?>>15th of Every Month</option>
                                <option value="last_day" <?php if($discount->end_date_of_month=='last_day') { echo "selected=selected";} ?>>last day of Every Month</option>
                               
                            </select>
                        </div>
                    </div>


                   
                

                </div>


                <div class="row"  id="staticdaterange" style="display:none;">

                  
                      <div class="col-sm-4">
                        <div class="form-group">
                            <label>Start Date <span class='error-text'>*</span></label>
                            <input type="text" class="form-control datepicker" id="start_date" name="start_date" autocomplete="off" value="<?php echo date('d-m-Y', strtotime($discount->start_date)) ?>">
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>End Date <span class='error-text'>*</span></label>
                            <input type="text" class="form-control datepicker" id="end_date" name="end_date" autocomplete="off" value="<?php echo date('d-m-Y', strtotime($discount->end_date)) ?>">
                        </div>
                    </div>

                   
                </div>



                <div class="row">



                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Amount Type <span class='error-text'>*</span></label>
                            <select name="amount_type" id="amount_type" class="form-control" onchange="percentageAmount(this.value)">
                                <option value="">Select</option>
                                <option value="1"
                                <?php
                                if('1' == $discount->amount_type)
                                {
                                    echo 'selected';
                                }
                                ?>
                                >Amount</option>
                                <option value="2"
                                <?php
                                if('2' == $discount->amount_type)
                                {
                                    echo 'selected';
                                }
                                ?>
                                >Percentage</option>
                            </select>
                        </div>
                    </div>

                    
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label id="view_percentage" style="display: none;">Percentage <span class='error-text'>*</span></label>
                            <label  id="view_amount" style="display: none;">Amount <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="amount" name="amount" value="<?php echo $discount->amount ?>">
                        </div>
                    </div>



                </div>


                <div class="row">

   <div class="col-sm-4">
                        <div class="form-group">
                                                        <label>Minimum Amount <span class='error-text'>*</span></label>

                            <input type="number" class="form-control" id="minimum_amount" name="minimum_amount" value="<?php echo $discount->minimum_amount ?>">
                        </div>
                    </div>








                    <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" <?php if($discount->status=='1') {
                                 echo "checked=checked";
                              };?>><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0" <?php if($discount->status=='0') {
                                 echo "checked=checked";
                              };?>>
                              <span class="check-radio"></span> In-Active
                            </label>                              
                        </div>                         
                    </div>


                </div>
            </div>

            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="../list" class="btn btn-link">Back</a>
                </div>
            </div>
            
        </form>

    </div>

        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
</div>
<script>

    $('select').select2();

    $( function()
    {
        showdaterange();
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
    });


     function showdaterange() {
        var id = $("#id_discount_type").val();
        $("#dynamicdaterange").hide();
        $("#staticdaterange").hide();
        if(id=='Fixed') {
        $("#dynamicdaterange").show();

        } else {
        $("#staticdaterange").show();

        }
    }



    function percentageAmount(value)
    {
        if(value == '1')
        {
                $("#view_amount").show();
                $("#view_percentage").hide();
        }
        else
        if(value == '2')
        {
                $("#view_amount").hide();
                $("#view_percentage").show();
        }
    }


    $(document).ready(function()
    {
        var amount_type = "<?php echo $discount->amount_type ?>";

        if(amount_type)
        {
            percentageAmount(amount_type);
        }

        $("#form_fee_category").validate({
            rules: {
                id_discount_type: {
                    required: true
                },
                start_date: {
                    required: true
                },
                end_date: {
                    required: true
                },
                amount_type: {
                    required: true
                },
                id_currency: {
                    required: true
                },
                amount: {
                    required: true
                },
                code: {
                    required: true
                }
            },
            messages: {
                id_discount_type: {
                    required: "<p class='error-text'>Select Discount Type</p>",
                },
                start_date: {
                    required: "<p class='error-text'>Select Start Date</p>",
                },
                end_date: {
                    required: "<p class='error-text'>Select End Date</p>",
                },
                amount_type: {
                    required: "<p class='error-text'>Select Amount Type</p>",
                },
                id_currency: {
                    required: "<p class='error-text'>Select Currency</p>",
                },
                amount: {
                    required: "<p class='error-text'>Value Required</p>",
                },
                code: {
                    required: "<p class='error-text'>Code Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

</script>
