<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Discount</h3>
        </div>
        <form id="form_fee_category" action="" method="post">
            <div class="form-container">
                <h4 class="form-group-title">Discount Details</h4> 

                <div class="row">

                  

                      <div class="col-sm-4">
                        <div class="form-group">
                            <label>Discount Type <span class='error-text'>*</span></label>
                            <select name="id_discount_type" id="id_discount_type" class="form-control" onchange="showdaterange(this.value)">
                                <option value="">Select</option>
                                <option value="Fixed">Fixed For Every Month</option>
                                <option value="Dynamic">Dynamic Range</option>
                               
                            </select>
                        </div>
                    </div>


                  

                </div>

                   <div class="row"  id="dynamicdaterange" style="display:none;">


                      <div class="col-sm-4">
                        <div class="form-group">
                            <label>Start Date of Month <span class='error-text'>*</span></label>
                            <select name="start_date_of_month" id="start_date_of_month" class="form-control">
                                <option value="">Select</option>
                                <option value="1st">1st of Every Month</option>
                                <option value="2nd">2nd of Every Month</option>
                                <option value="3rd">3rd of Every Month</option>
                                <option value="4th">4th of Every Month</option>
                                <option value="5th">5th of Every Month</option>
                                <option value="6th">6th of Every Month</option>
                                <option value="7th">7th of Every Month</option>
                                <option value="8th">8th of Every Month</option>
                                <option value="9th">9th of Every Month</option>
                                <option value="10th">10th of Every Month</option>
                                <option value="11th">11th of Every Month</option>
                                <option value="12th">12th of Every Month</option>
                                <option value="13th">13th of Every Month</option>
                                <option value="14th">14th of Every Month</option>
                                <option value="15th">15th of Every Month</option>
                                <option value="last_day">last day of Every Month</option>
                               
                            </select>
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>End Date of Month <span class='error-text'>*</span></label>
                            <select name="end_date_of_month" id="end_date_of_month" class="form-control">
                                <option value="">Select</option>
                                <option value="1st">1st of Every Month</option>
                                <option value="2nd">2nd of Every Month</option>
                                <option value="3rd">3rd of Every Month</option>
                                <option value="4th">4th of Every Month</option>
                                <option value="5th">5th of Every Month</option>
                                <option value="6th">6th of Every Month</option>
                                <option value="7th">7th of Every Month</option>
                                <option value="8th">8th of Every Month</option>
                                <option value="9th">9th of Every Month</option>
                                <option value="10th">10th of Every Month</option>
                                <option value="11th">11th of Every Month</option>
                                <option value="12th">12th of Every Month</option>
                                <option value="13th">13th of Every Month</option>
                                <option value="14th">14th of Every Month</option>
                                <option value="15th">15th of Every Month</option>
                                <option value="last_day">last day of Every Month</option>
                               
                            </select>
                        </div>
                    </div>


                   
                

                </div>


                <div class="row"  id="staticdaterange" style="display:none;">

                  
                      <div class="col-sm-4">
                        <div class="form-group">
                            <label>Start Date <span class='error-text'>*</span></label>
                            <input type="text" class="form-control datepicker" id="start_date" name="start_date" autocomplete="off">
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>End Date <span class='error-text'>*</span></label>
                            <input type="text" class="form-control datepicker" id="end_date" name="end_date" autocomplete="off">
                        </div>
                    </div>

                   
                      </div>


                <div class="row">

                     <div class="col-sm-4">
                        <div class="form-group">
                            <label>Code <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="code" name="code">
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Amount Type <span class='error-text'>*</span></label>
                            <select name="amount_type" id="amount_type" class="form-control" onchange="percentageAmount(this.value)">
                                <option value="1">Amount</option>
                                <option value="2">Percentage</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label id="view_amount" style="display: ;">Amount <span class='error-text'>*</span></label>
                            <label id="view_percentage" style="display: none;">Percentage <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="amount" name="amount">
                        </div>
                    </div>



               



                    <div class="col-sm-4">
                        <div class="form-group">
                                                        <label>Minimum Amount <span class='error-text'>*</span></label>

                            <input type="number" class="form-control" id="minimum_amount" name="minimum_amount">
                        </div>
                    </div>



                    




                    <div class="col-sm-4">
                            <div class="form-group">
                                <p>Status <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                                </label>
                                <label class="radio-inline">
                                <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                                </label>                              
                            </div>                         
                    </div>
                </div>
            </div>

            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="list" class="btn btn-link">Cancel</a>
                </div>
            </div>
            
        </form>

    </div>

        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
</div>
<script>

    $('select').select2();

    $( function()
    {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
    });

    function showdaterange(id) {
        $("#dynamicdaterange").hide();
        $("#staticdaterange").hide();
        if(id=='Fixed') {
        $("#dynamicdaterange").show();

        } else {
        $("#staticdaterange").show();

        }
    }

    function percentageAmount(value)
    {
        if(value == '1')
        {
                $("#view_amount").show();
                $("#view_percentage").hide();
        }
        else
        if(value == '2')
        {
                $("#view_amount").hide();
                $("#view_percentage").show();
        }
    }


    $(document).ready(function() {
        $("#form_fee_category").validate({
            rules: {
                id_discount_type: {
                    required: true
                },
                start_date: {
                    required: true
                },
                end_date: {
                    required: true
                },
                amount_type: {
                    required: true
                },
                id_currency: {
                    required: true
                },
                amount: {
                    required: true
                },
                code: {
                    required: true
                }
            },
            messages: {
                id_discount_type: {
                    required: "<p class='error-text'>Select Discount Type</p>",
                },
                start_date: {
                    required: "<p class='error-text'>Select Start Date</p>",
                },
                end_date: {
                    required: "<p class='error-text'>Select End Date</p>",
                },
                amount_type: {
                    required: "<p class='error-text'>Select Amount Type</p>",
                },
                id_currency: {
                    required: "<p class='error-text'>Select Currency</p>",
                },
                amount: {
                    required: "<p class='error-text'>Value Required</p>",
                },
                code: {
                    required: "<p class='error-text'>Code Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

</script>
