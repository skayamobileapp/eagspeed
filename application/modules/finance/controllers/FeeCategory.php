<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class FeeCategory extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('fee_category_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('fee_category.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['feeCategoryList'] = $this->fee_category_model->feeCategoryListSearch();

            $this->global['pageTitle'] = 'Campus Management System : Fee Category List';
            $this->loadViews("fee_category/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('fee_category.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $id_user = $this->session->userId;

            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $name_optional_language = $this->security->xss_clean($this->input->post('name_optional_language'));
                $fee_group = $this->security->xss_clean($this->input->post('fee_group'));
                $sequence = $this->security->xss_clean($this->input->post('sequence'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $data = array(
                    'name' => $name,
                    'name_optional_language' => $name_optional_language,
                    'code' => $code,
					'fee_group' => $fee_group,
					'sequence' => $sequence,
                    'status' => $status,
                    'created_by' => $id_user
                );
                $inserted_id = $this->fee_category_model->addNewFeeCategory($data);
                redirect('/finance/feeCategory/list');
            }
            
            $this->global['pageTitle'] = 'Campus Management System : Add Fee Category';
            $this->loadViews("fee_category/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('fee_category.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/finance/feeCategory/list');
            }
            
            $id_session = $this->session->my_session_id;
            $id_user = $this->session->userId;

            if($this->input->post())
            {
	            $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $name_optional_language = $this->security->xss_clean($this->input->post('name_optional_language'));
                $fee_group = $this->security->xss_clean($this->input->post('fee_group'));
                $sequence = $this->security->xss_clean($this->input->post('sequence'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $data = array(
                    'name' => $name,
                    'name_optional_language' => $name_optional_language,
                    'code' => $code,
					'fee_group' => $fee_group,
					'sequence' => $sequence,
                    'status' => $status,
                    'updated_by' => $id_user,
                    'updated_dt_tm' => date('Y-m-d H:i:s')
                );

                // echo "<Pre>";print_r($data);exit;
                $result = $this->fee_category_model->editFeeCategory($data,$id);
                redirect('/finance/feeCategory/list');
            }
            // $data['studentList'] = $this->fee_category_model->studentList();
            $data['feeCategory'] = $this->fee_category_model->getFeeCategory($id);
            $this->global['pageTitle'] = 'Campus Management System : Edit Fee Category';
            $this->loadViews("fee_category/edit", $this->global, $data, NULL);
        }
    }
}
