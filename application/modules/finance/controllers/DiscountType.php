<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class DiscountType extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('discount_type_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('discount_type.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;
            $data['discountTypeList'] = $this->discount_type_model->discountTypeListSearch($name);
            $this->global['pageTitle'] = 'Campus Management System : Discount Type List';
            $this->loadViews("discount_type/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('discount_type.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $id_user = $this->session->userId; 
            
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'description' => $description,
                    'status' => $status,
                    'created_by' => $id_user
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->discount_type_model->addNewDiscountType($data);
                redirect('/finance/discountType/list');
            }
            $this->global['pageTitle'] = 'Campus Management System : Add Discount Type';
            $this->loadViews("discount_type/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('discount_type.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/finance/discountType/list');
            }
            
            $id_session = $this->session->my_session_id;
            $id_user = $this->session->userId; 
            
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'description' => $description,
                    'status' => $status,
                    'updated_by' => $id_user,
                    'updated_dt_tm' => date('Y-m-d H:i:s')
                );

                $result = $this->discount_type_model->editDiscountType($data,$id);
                redirect('/finance/discountType/list');
            }
            $data['discountType'] = $this->discount_type_model->getDiscountType($id);
            $this->global['pageTitle'] = 'Campus Management System : Edit Discount Type';
            $this->loadViews("discount_type/edit", $this->global, $data, NULL);
        }
    }
}
