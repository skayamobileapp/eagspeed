<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class FeeSetup extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('fee_setup_model');
        $this->load->model('amount_calculation_type_model');
        $this->load->model('frequency_mode_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('fee_setup.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $data['searchParam'] = $formData;

            // echo "<Pre>"; print_r($data);exit;
            $data['feeSetupList'] = $this->fee_setup_model->feeSetupListSearch($formData);
            
            $this->global['pageTitle'] = 'Campus Management System : Sponser List';
            $this->loadViews("fee_setup/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('fee_setup.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $id_session = $this->session->my_session_id;
            $id_user = $this->session->userId; 
            
            if($this->input->post())
            {
                $date = date('dmY_his');
                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $name_optional_language = $this->security->xss_clean($this->input->post('name_optional_language'));
                $id_fee_category = $this->security->xss_clean($this->input->post('id_fee_category'));
                $id_amount_calculation_type = $this->security->xss_clean($this->input->post('id_amount_calculation_type'));
                $id_frequency_mode = $this->security->xss_clean($this->input->post('id_frequency_mode'));
                $account_code = $this->security->xss_clean($this->input->post('account_code'));
                $is_refundable = $this->security->xss_clean($this->input->post('is_refundable'));
                $is_non_invoice = $this->security->xss_clean($this->input->post('is_non_invoice'));
                $is_gst = $this->security->xss_clean($this->input->post('is_gst'));
                $gst_tax = $this->security->xss_clean($this->input->post('gst_tax'));
                $effective_date = $this->security->xss_clean($this->input->post('effective_date'));
                $is_registration_fee = $this->security->xss_clean($this->input->post('is_registration_fee'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'code' => $code,
                    'name' => $name,
                    'name_optional_language' => $name_optional_language,
                    'id_fee_category' => $id_fee_category,
                    'id_amount_calculation_type' => $id_amount_calculation_type,
                    'id_frequency_mode' => $id_frequency_mode,
                    'account_code' => $account_code,
                    'is_refundable' => $is_refundable,
                    'is_non_invoice' => $is_non_invoice,
                    'is_gst' => $is_gst,
                    'is_registration_fee' => $is_registration_fee,
                    'gst_tax' => $gst_tax,
                    'effective_date' => date('Y-m-d',strtotime($effective_date)),
                    'status' => $status,
                    'created_by' => $id_user
                );
                //echo "<Pre>"; print_r($data);exit;

                $inserted_id = $this->fee_setup_model->addNewFeeSetup($data);
                redirect('/finance/feeSetup/list');
            }
            
            $data['accountCodeList'] = $this->fee_setup_model->financialAccountCodeListByStatus('1');
            $data['feeCategoryList'] = $this->fee_setup_model->feeCategoryListByStatus('1');
            // $data['amountCalculationTypeList'] = $this->amount_calculation_type_model->amountCalculationTypeList();
            // $data['frequencyModeList'] = $this->frequency_mode_model->frequencyModeList();
            $this->global['pageTitle'] = 'Campus Management System : Add Sponsor';
            $this->loadViews("fee_setup/add", $this->global, $data, NULL);
        }
    }

    function edit($id = NULL)
    {
        if ($this->checkAccess('fee_setup.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/finance/feeSetup/list');
            }

            $id_session = $this->session->my_session_id;
            $id_user = $this->session->userId; 

            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $name_optional_language = $this->security->xss_clean($this->input->post('name_optional_language'));
                $id_fee_category = $this->security->xss_clean($this->input->post('id_fee_category'));
                $id_amount_calculation_type = $this->security->xss_clean($this->input->post('id_amount_calculation_type'));
                $id_frequency_mode = $this->security->xss_clean($this->input->post('id_frequency_mode'));
                $account_code = $this->security->xss_clean($this->input->post('account_code'));
                $is_refundable = $this->security->xss_clean($this->input->post('is_refundable'));
                $is_non_invoice = $this->security->xss_clean($this->input->post('is_non_invoice'));
                $is_gst = $this->security->xss_clean($this->input->post('is_gst'));
                $gst_tax = $this->security->xss_clean($this->input->post('gst_tax'));
                $effective_date = $this->security->xss_clean($this->input->post('effective_date'));
                $is_registration_fee = $this->security->xss_clean($this->input->post('is_registration_fee'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'code' => $code,
                    'name' => $name,
                    'name_optional_language' => $name_optional_language,
                    'id_fee_category' => $id_fee_category,
                    'id_amount_calculation_type' => $id_amount_calculation_type,
                    'id_frequency_mode' => $id_frequency_mode,
                    'account_code' => $account_code,
                    'is_refundable' => $is_refundable,
                    'is_non_invoice' => $is_non_invoice,
                    'is_gst' => $is_gst,
                    'is_registration_fee' => $is_registration_fee,
                    'gst_tax' => $gst_tax,
                    'effective_date' => date('Y-m-d',strtotime($effective_date)),
                    'status' => $status,
                    'updated_by' => $id_user,
                    'updated_dt_tm' => date('Y-m-d H:i:s')
                );

                //echo "<Pre>"; print_r($data);exit;
                $result = $this->fee_setup_model->editFeeSetup($data,$id);
                redirect('/finance/feeSetup/list');
            }
            $data['accountCodeList'] = $this->fee_setup_model->financialAccountCodeListByStatus('1');
            $data['feeCategoryList'] = $this->fee_setup_model->feeCategoryListByStatus('1');
            $data['feeSetup'] = $this->fee_setup_model->getFeeSetup($id);
            
            //echo "<Pre>"; print_r($data['feeSetup']);exit;
            $this->global['pageTitle'] = 'Campus Management System : Edit Sponsor';
            $this->loadViews("fee_setup/edit", $this->global, $data, NULL);
        }
    }
}
