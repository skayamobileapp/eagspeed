<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

require APPPATH . '/libraries/BaseController.php';

class AmountCalculationType extends BaseController {
	public function __construct() {
		parent::__construct();
		$this->load->model('amount_calculation_type_model');
		$this->isLoggedIn();
	}

	function list() {
		if ($this->checkAccess('amount_calculation_type.list') == 0) {
			$this->loadAccessRestricted();
		} else {
			$data['amountCalculationTypeList'] = $this->amount_calculation_type_model->amountCalculationTypeList();
			$this->global['pageTitle'] = 'Campus Management System : Amount Calculation Type List';
			$this->loadViews("amount_calculation_type/list", $this->global, $data, NULL);
		}
	}

	function add() {
		if ($this->checkAccess('amount_calculation_type.add') == 0) {
			$this->loadAccessRestricted();
		} else {
			if ($this->input->post()) {
				$code = $this->security->xss_clean($this->input->post('code'));
				$name = $this->security->xss_clean($this->input->post('name'));
				$name_optional_language = $this->security->xss_clean($this->input->post('name_optional_language'));
				$status = $this->security->xss_clean($this->input->post('status'));

				$data = array(
					'code' => $code,
					'name' => $name,
					'name_optional_language' => $name_optional_language,
					'status' => $status,
				);
				$inserted_id = $this->amount_calculation_type_model->addNewAmountCalculationType($data);
				redirect('/finance/amountCalculationType/list');
			}

			$this->global['pageTitle'] = 'Campus Management System : Add Amount Calculation Type';
			$this->loadViews("amount_calculation_type/add", $this->global, NULL, NULL);
		}
	}

	function edit($id = NULL) {
		if ($this->checkAccess('amount_calculation_type.edit') == 0) {
			$this->loadAccessRestricted();
		} else {
			if ($id == null) {
				redirect('/finance/amountCalculationType/list');
			}
			if ($this->input->post()) {
				$code = $this->security->xss_clean($this->input->post('code'));
				$name = $this->security->xss_clean($this->input->post('name'));
				$name_optional_language = $this->security->xss_clean($this->input->post('name_optional_language'));
				$status = $this->security->xss_clean($this->input->post('status'));

				$data = array(
					'code' => $code,
					'name' => $name,
					'name_optional_language' => $name_optional_language,
					'status' => $status,
				);
				$result = $this->amount_calculation_type_model->editAmountCalculationType($data, $id);
				redirect('/finance/amountCalculationType/list');
			}
			// $data['studentList'] = $this->amount_calculation_type_model->studentList();
			$data['amountCalculationType'] = $this->amount_calculation_type_model->getAmountCalculationType($id);
			$this->global['pageTitle'] = 'Campus Management System : Edit Amount Calculation Type';
			$this->loadViews("amount_calculation_type/edit", $this->global, $data, NULL);
		}
	}
}
