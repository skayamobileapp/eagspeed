<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class CreditNoteType extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('credit_note_type_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('credit_note_type.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;
            $data['creditNoteTypeList'] = $this->credit_note_type_model->creditNoteTypeListSearch($name);

            $this->global['pageTitle'] = 'Campus Management System : Credit Note Type List';
            $this->loadViews("credit_note_type/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('credit_note_type.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $id_user = $this->session->userId; 

            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'status' => $status,
                    'created_by' => $id_user
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->credit_note_type_model->addNewCreditNoteType($data);
                redirect('/finance/creditNoteType/list');
            }
            $this->global['pageTitle'] = 'Campus Management System : Add CreditNoteType';
            $this->loadViews("credit_note_type/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('credit_note_type.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/finance/creditNoteType/list');
            }

            $id_session = $this->session->my_session_id;
            $id_user = $this->session->userId; 
            
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'status' => $status,
                    'updated_by' => $id_user,
                    'updated_dt_tm' => date('Y-m-d H:i:s')
                );

                $result = $this->credit_note_type_model->editCreditNoteType($data,$id);
                redirect('/finance/creditNoteType/list');
            }
            
            $data['creditNoteType'] = $this->credit_note_type_model->getCreditNoteType($id);

            $this->global['pageTitle'] = 'Campus Management System : Edit CreditNoteType';
            $this->loadViews("credit_note_type/edit", $this->global, $data, NULL);
        }
    }
}