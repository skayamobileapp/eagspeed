ProformaInvoiceDetails
<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class PerformaInvoiceDetails Invoice extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('sponser_model');
        $this->isLoggedIn();
    }

    public function index()
    {
        
        $this->global['pageTitle'] = 'School : Dashboard';        
        $this->loadViews("sponser/dashboard", $this->global, NULL , NULL);
    }

    function pageNotFound()
    {
        $this->global['pageTitle'] = 'Campus Management System : 404 - Page Not Found';
        $this->loadViews("404", $this->global, NULL, NULL);
    }

    function list()
    {
        if ($this->checkAccess('sponser.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['sponserList'] = $this->sponser_model->sponserList();
            $this->global['pageTitle'] = 'Campus Management System : Performa Invoice List';
            $this->loadViews("sponser/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('sponser.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $address = $this->security->xss_clean($this->input->post('address'));
                $mobile_number = $this->security->xss_clean($this->input->post('mobile_number'));
                $id_state = $this->security->xss_clean($this->input->post('id_state'));
                $id_country = $this->security->xss_clean($this->input->post('id_country'));
                $zip_code = $this->security->xss_clean($this->input->post('zip_code'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'name' => $name,
                    'address' => $address,
                    'mobile_number' => $mobile_number,
                    'id_state' => $id_state,
                    'id_country' => $id_country,
                    'zip_code' => $zip_code,
                    'status' => $status
                );
                $result = $this->sponser_model->addNewPerforma Invoice($data);
                redirect('/finance/sponser/list');
            }
            //print_r($data['stateList']);exit;
            $data['stateList'] = $this->sponser_model->stateList();
            $data['countryList'] = $this->sponser_model->countryList();
            $this->global['pageTitle'] = 'Campus Management System : Add Performa Invoice';
            $this->loadViews("sponser/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('sponser.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/finance/sponser/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $address = $this->security->xss_clean($this->input->post('address'));
                $mobile_number = $this->security->xss_clean($this->input->post('mobile_number'));
                $id_state = $this->security->xss_clean($this->input->post('id_state'));
                $id_country = $this->security->xss_clean($this->input->post('id_country'));
                $zip_code = $this->security->xss_clean($this->input->post('zip_code'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'name' => $name,
                    'address' => $address,
                    'mobile_number' => $mobile_number,
                    'id_state' => $id_state,
                    'id_country' => $id_country,
                    'zip_code' => $zip_code,
                    'status' => $status
                );
                
                $result = $this->sponser_model->editPerforma Invoice($data,$id);
                redirect('/finance/sponser/list');
            }
            $data['stateList'] = $this->sponser_model->stateList();
            $data['countryList'] = $this->sponser_model->countryList();
            $data['sponserDetails'] = $this->sponser_model->getPerforma Invoice($id);
            $this->global['pageTitle'] = 'Campus Management System : Edit Performa Invoice';
            $this->loadViews("sponser/edit", $this->global, $data, NULL);
        }
    }
}
