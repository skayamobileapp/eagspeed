<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Voucher extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('discount_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('voucher.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_discount_type'] = $this->security->xss_clean($this->input->post('id_discount_type'));
            $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
            $data['searchParam'] = $formData;

            $formData['type'] ='VOUCHER';

            $data['discountList'] = $this->discount_model->discountListSearch($formData);

            $data['discountTypeList'] = $this->discount_model->discountTypeListByStatus('1');
            $data['intakeList'] = $this->discount_model->intakeListByStatus('1');

            $this->global['pageTitle'] = 'Campus Management System : Discount List';
            $this->loadViews("voucher/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('voucher.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $id_user = $this->session->userId; 

            if($this->input->post())
            {
                $id_discount_type = $this->security->xss_clean($this->input->post('id_discount_type'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));
                $amount_type = $this->security->xss_clean($this->input->post('amount_type'));
                $amount = $this->security->xss_clean($this->input->post('amount'));
                $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
                $id_currency = $this->security->xss_clean($this->input->post('id_currency'));
                $start_date_of_month = $this->security->xss_clean($this->input->post('start_date_of_month'));
                $end_date_of_month = $this->security->xss_clean($this->input->post('end_date_of_month'));
                $minimum_amount = $this->security->xss_clean($this->input->post('minimum_amount'));
            
                $data = array(
                    'id_discount_type' => $id_discount_type,
                    'code' => $code,
                    'start_date_of_month'=>$start_date_of_month,
                    'end_date_of_month'=>$end_date_of_month,
                    'start_date' => date('Y-m-d', strtotime($start_date)),
                    'end_date' => date('Y-m-d', strtotime($end_date)),
                    'amount_type' => $amount_type,
                    'amount' => $amount,
                    'id_intake' => $id_intake,
                    'currency' => $id_currency,
                    'status' => $status,
                    'created_by' => $id_user,
                    'minimum_amount'=>$minimum_amount,
                    'type'=>'VOUCHER'
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->discount_model->addNewDiscount($data);
                redirect('/finance/voucher/list');
            }

            $data['discountTypeList'] = $this->discount_model->discountTypeListByStatus('1');
            $data['intakeList'] = $this->discount_model->intakeListByStatus('1');
            $data['currencyList'] = $this->discount_model->currencyListByStatus('1');

            $this->global['pageTitle'] = 'Campus Management System : Add Discount Type';
            $this->loadViews("voucher/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('voucher.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/finance/voucher/list');
            }

            $id_session = $this->session->my_session_id;
            $id_user = $this->session->userId; 
            
            if($this->input->post())
            {
                // echo "<Pre>"; print_r($this->input->post());exit;


                $id_discount_type = $this->security->xss_clean($this->input->post('id_discount_type'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));
                $amount_type = $this->security->xss_clean($this->input->post('amount_type'));
                $amount = $this->security->xss_clean($this->input->post('amount'));
                $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
                $id_currency = $this->security->xss_clean($this->input->post('id_currency'));
                $start_date_of_month = $this->security->xss_clean($this->input->post('start_date_of_month'));
                $end_date_of_month = $this->security->xss_clean($this->input->post('end_date_of_month'));
                $minimum_amount = $this->security->xss_clean($this->input->post('minimum_amount'));
            
                $data = array(
                    'id_discount_type' => $id_discount_type,
                    'code' => $code,
                    'start_date_of_month'=>$start_date_of_month,
                    'end_date_of_month'=>$end_date_of_month,
                    'start_date' => date('Y-m-d', strtotime($start_date)),
                    'end_date' => date('Y-m-d', strtotime($end_date)),
                    'amount_type' => $amount_type,
                    'amount' => $amount,
                    'id_intake' => $id_intake,
                    'currency' => $id_currency,
                    'status' => $status,
                    'updated_by' => $id_user,
                    'updated_dt_tm' => date('Y-m-d H:i:s'),
                    'minimum_amount'=>$minimum_amount,
                    'type'=>'VOUCHER'
                );

                $result = $this->discount_model->editDiscount($data,$id);
                redirect('/finance/voucher/list');
            }
            
            $data['discount'] = $this->discount_model->getDiscount($id);

            $data['discountTypeList'] = $this->discount_model->discountTypeListByStatus('1');
            $data['intakeList'] = $this->discount_model->intakeListByStatus('1');
            $data['currencyList'] = $this->discount_model->currencyListByStatus('1');

            $this->global['pageTitle'] = 'Campus Management System : Edit Discount';
            $this->loadViews("voucher/edit", $this->global, $data, NULL);
        }
    }
}
