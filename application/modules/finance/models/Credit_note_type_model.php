<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Credit_note_type_model extends CI_Model
{
    function creditNoteTypeList()
    {
        $this->db->select('*');
        $this->db->from('credit_note_type');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function creditNoteTypeListSearch($search)
    {
        $this->db->select('cnt.*, cre.name as creater_name, upd.name as updater_name');
        $this->db->from('credit_note_type as cnt');
        $this->db->join('users as cre','cnt.created_by = cre.id','left');
        $this->db->join('users as upd','cnt.updated_by = upd.id','left');
        if (!empty($search))
        {
            $likeCriteria = "(cnt.name  LIKE '%" . $search . "%' or cnt.code  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("cnt.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getCreditNoteType($id)
    {
        $this->db->select('*');
        $this->db->from('credit_note_type');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewCreditNoteType($data)
    {
        $this->db->trans_start();
        $this->db->insert('credit_note_type', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editCreditNoteType($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('credit_note_type', $data);
        return TRUE;
    }
}

