<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Bank_registration_model extends CI_Model
{
    function bankRegistrationList()
    {
        $this->db->select('fc.*, c.name as country, s.name as state');
        $this->db->from('bank_registration as fc');
        $this->db->join('country as c', 'fc.id_country = c.id');
        $this->db->join('state as s', 'fc.id_state = s.id');
        $this->db->order_by("fc.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function bankRegistrationListSearch($data)
    {
        $this->db->select('fc.*, c.name as country, s.name as state, cre.name as creater_name, upd.name as updater_name');
        $this->db->from('bank_registration as fc');
        $this->db->join('country as c', 'fc.id_country = c.id');
        $this->db->join('state as s', 'fc.id_state = s.id');
        $this->db->join('users as cre','fc.created_by = cre.id','left');
        $this->db->join('users as upd','fc.updated_by = upd.id','left');
        if (!empty($search))
        {
            $likeCriteria = "(name  LIKE '%" . $data['name'] . "%' or code  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("fc.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


    function getBankRegistration($id)
    {
        $this->db->select('fc.*, c.name as country, s.name as state');
        $this->db->from('bank_registration as fc');
        $this->db->join('country as c', 'fc.id_country = c.id');
        $this->db->join('state as s', 'fc.id_state = s.id');
        $this->db->where('fc.id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewBankRegistration($data)
    {
        $this->db->trans_start();
        $this->db->insert('bank_registration', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function addNewBankRegistrationDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('bank_registration_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function editBankRegistration($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('bank_registration', $data);
        return TRUE;
    }

    function editBankRegistrationForStatus($data)
    {
        $this->db->where('id !=', 0);
        $this->db->update('bank_registration', $data);
        return TRUE;
    }

    function getStateByCountryId($id_country)
    {
        $this->db->select('*');
        $this->db->from('state');
        $this->db->where('id_country', $id_country);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function countryListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('country');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function stateListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('state');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getAccountCodeList()
    {
        $this->db->select('*');
        $this->db->from('account_code');
        $this->db->where('level', '3');
        $this->db->where('status', '1');
        $this->db->order_by("code", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getActivityCodeList()
    {
        $this->db->select('*');
        $this->db->from('activity_code');
        $this->db->where('level', '3');
        $this->db->where('status', '1');
        $this->db->order_by("code", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getDepartmentCodeList()
    {
        $this->db->select('*');
        $this->db->from('department_code');
         $this->db->where('status', '1');
        $this->db->order_by("code", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getFundCodeList()
    {
        $this->db->select('*');
        $this->db->from('fund_code');
        $this->db->where('status', '1');
        $this->db->order_by("code", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

}

