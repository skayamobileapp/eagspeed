<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Fee_structure_model extends CI_Model
{
    function getFinanceConfiguration()
    {
        $this->db->select('*');
        $this->db->from('finance_configuration');
        $this->db->order_by("id", "ASC");
        $query = $this->db->get();
        return $query->row();
    }
    
    function programmeListSearch($search)
    {
        $this->db->select('p.*');
        $this->db->from('programme as p');
        if (!empty($search))
        {
            $likeCriteria = "(p.name  LIKE '%" . $search . "%' or p.code  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function intakeListByProgrammeId($id, $search)
    {
       $this->db->select('i.*, p.name as programme_name, p.code as programme_code');
        $this->db->from('intake as i');
        $this->db->join('intake_has_programme as ihp', 'ihp.id_intake = i.id');
        $this->db->join('programme as p', 'ihp.id_programme = p.id');
        if (!empty($search))
        {
            $likeCriteria = "(p.name  LIKE '%" . $search . "%' or i.name  LIKE '%" . $search . "%' or p.code  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('ihp.id_programme', $id);
        $this->db->order_by("i.name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getProgrammeById($id)
    {
       $this->db->select('p.*');
        $this->db->from('programme as p');
        $this->db->where('p.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function programSchemeList($id_programme)
    {
       $this->db->select('p.*');
        $this->db->from('programme_has_scheme as p');
        $this->db->where('p.id_program', $id_programme);
        $query = $this->db->get();
        return $query->result();
    }

    function getProgrammeScheme($id_program_scheme)
    {
        $this->db->select('p.*');
        $this->db->from('programme_has_scheme as p');
        $this->db->where('p.id', $id_program_scheme);
        $query = $this->db->get();
        return $query->row();
    }

    function getFeeStructure($id_programme,$id_intake)
    {
       $this->db->select('p.*');
        $this->db->from('fee_structure as p');
        $this->db->where('p.id_programme', $id_programme);
        $this->db->where('p.id_intake', $id_intake);
        $query = $this->db->get();
        return $query->row();
    }

    function getFeeStructureDetails($id_fee_structure)
    {
        $this->db->select('tfsd.*, fs.name as fee_structure, fm.name as frequency_mode');
        $this->db->from('fee_structure_details as tfsd');
        $this->db->join('fee_setup as fs', 'tfsd.id_fee_item = fs.id');   
        $this->db->join('frequency_mode as fm', 'tfsd.id_frequency_mode = fm.id');   
        $this->db->where('tfsd.id_fee_structure', $id_fee_structure);
        $query = $this->db->get();
        return $query->result();
    }

    

    function getIntakeById($id)
    {
       $this->db->select('p.*');
        $this->db->from('intake as p');
        $this->db->where('p.id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addTempDetails($data)
    {
        // echo "<Pre>";  print_r($data);exit;

        $this->db->trans_start();
        $this->db->insert('temp_fee_structure_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addNewFeeStructure($data)
    {
        // echo "<Pre>";  print_r($data);exit;

        $this->db->trans_start();
        $this->db->insert('fee_structure', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function updateFeeStructureDetail($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('fee_structure', $data);
        return TRUE;
    }

    function addNewFeeStructureDetails($data)
    {
        // echo "<Pre>";  print_r($data);exit;

        $this->db->trans_start();
        $this->db->insert('fee_structure_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function updateTempDetails($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('temp_fee_structure_details', $data);
        return TRUE;
    }

    function getTempFeeStructureDetails($id_session)
    {
        $this->db->select('tfsd.*, fs.name as fee_structure, fm.name as frequency_mode');
        $this->db->from('temp_fee_structure_details as tfsd');
        $this->db->join('fee_setup as fs', 'tfsd.id_fee_item = fs.id');   
        $this->db->join('frequency_mode as fm', 'tfsd.id_frequency_mode = fm.id');   
        $this->db->where('tfsd.id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }

    function deleteTempDataBySession($id_session)
    {
        $this->db->where('id_session', $id_session);
       $this->db->delete('temp_fee_structure_details');
        return TRUE;
    }

    function deletefromfeeitem($id) {
        $this->db->where('id', $id);
       $this->db->delete('fee_structure');
        return TRUE;
    }


    function deleteTempData($id)
    {
        $this->db->where('id', $id);
       $this->db->delete('temp_fee_structure_details');
        return TRUE;
    }

    function getFeeStructureDetail($id)
    {
       $this->db->select('p.*');
        $this->db->from('fee_structure as p');
        $this->db->where('p.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getFeeStructureByIdProgrammeNIdIntake($idfeemaster)
    {
       $this->db->select('p.*, fs.code as fee_structure_code, fs.name as fee_structure, fs.gst_tax');
        $this->db->from('fee_structure_details as p');
        $this->db->join('fee_setup as fs', 'p.id_fee_item = fs.id');
        $this->db->where('p.id_fee_structure', $idfeemaster);
        $this->db->order_by("fs.name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getFeeStructureByIdProgrammeNIdIntakeForCopy($id_program_landscape,$currency)
    {
       $this->db->select('p.*');
        $this->db->from('fee_structure as p');
        $this->db->join('fee_setup as fs', 'p.id_fee_item = fs.id');   
        $this->db->join('frequency_mode as fm', 'fs.id_frequency_mode = fm.id');
        $this->db->join('amount_calculation_type as amt', 'fs.id_amount_calculation_type = amt.id');
        $this->db->join('fee_structure_triggering_point as fstp', 'p.id_fee_structure_trigger = fstp.id','left');
        $this->db->where('p.id_program_landscape', $id_program_landscape);
        $this->db->where('p.id_training_center', 1);
        $this->db->where('p.currency', $currency);
        $this->db->order_by("fs.name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function deleteFeeStructureById($id)
    {
        $this->db->where('id', $id);
       $this->db->delete('fee_structure');
       return TRUE;
    }

    function programmeLandscapeByProgrammeId($id_programme)
    {
        $this->db->select('pl.*,p.code as programme_code, p.name as programme, i.year as intake_year, i.name as intake,phs.description as schemename');
        $this->db->from('programme_landscape as pl');
        $this->db->join('programme as p', 'pl.id_programme = p.id');
        $this->db->join('intake as i', 'pl.id_intake = i.id');
        $this->db->join('scheme as phs', 'pl.program_scheme = phs.id');
        // if($formData['id_programme']) {
        //     $likeCriteria = "(pl.id_programme  LIKE '%" . $formData['id_programme'] . "%')";
        //     $this->db->where($likeCriteria);
        // }

        // if($formData['name']) {
        //     $likeCriteria = "(pl.name  LIKE '%" . $formData['name'] . "%')";
        //     $this->db->where($likeCriteria);
        // }

        //  if($formData['id_intake']) {
        //     $likeCriteria = "(pl.id_intake  LIKE '%" . $formData['id_intake'] . "%')";
        //     $this->db->where($likeCriteria);
        // }
        $this->db->where('pl.id_programme', $id_programme);
        $this->db->order_by("pl.name", "ASC");
         $query = $this->db->get();
         $results = $query->result();  
         // echo "<pre>";print_r($result);exit;
         $details = array();
         foreach ($results as $result)
         {
            $amount_local = $this->getFeeStructureByLandscapeAmount($result->id,'MYR');
            $amount_international = $this->getFeeStructureByLandscapeAmount($result->id,'USD');
            // $amount_p_international = $this->getFeeStructureByLandscapeAmount($result->id,'PUSD');
            $result->total_amount_local = $amount_local;
            $result->total_amount_international = $amount_international;
            // $result->total_amount_p_international = $amount_p_international;
            array_push($details, $result);
         }

         return $details;
    }

    function getFeeStructureByLandscapeAmount($id_program_landscape,$currency)
    {
        $this->db->select('*');
        $this->db->from('fee_structure');
        $this->db->where('id_program_landscape', $id_program_landscape);
        $this->db->where('id_training_center', 0);
        $this->db->where('currency', $currency);
        $query = $this->db->get();
        $results = $query->result(); 

        $total = 0;
        foreach ($results as $value)
        {
            $total = $total + $value->amount;
        }
        return$total;
    }

    function intakeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('intake');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function currencyListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('currency_setup');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function partnerUniversityListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('partner_university');
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function getProgramme($id)
    {
        $this->db->select('p.*');
        $this->db->from('programme as p');
        $this->db->where('p.id', $id);
         $query = $this->db->get();
         
         $result = $query->row();  
         return $result;
    }

    function getProgrammeLandscape($id)
    {
        $this->db->select('p.*');
        $this->db->from('programme_landscape as p');
        $this->db->where('p.id', $id);
        $query = $this->db->get();
         
        $result = $query->row();   
        
        return $result;
    }


    function getProgrammeLandscapeLocal($id,$currency)
    {
        $this->db->select('p.*');
        $this->db->from('fee_structure_master as p');
        $this->db->where('p.id', $id);
         $query = $this->db->get();
         
         $result = $query->row();  

         $amount = 0;
         if($result)
         {
            $amount = $this->getFeeStructureByLandscapeAmountCurrency($result->id,$currency);
            $result->total_amount = $amount;
         }
         
         return $result;
    }

    function getFeeStructureByLandscapeAmountCurrency($id_program_landscape,$currency)
    {
        $this->db->select('*');
        $this->db->from('fee_structure');
        $this->db->where('id_program_landscape', $id_program_landscape);
        $this->db->where('currency', $currency);
        $query = $this->db->get();
        $results = $query->result(); 

        $total = 0;
        foreach ($results as $value)
        {
            $total = $total + $value->amount;
        }
        return $total;
    }

    function branchListByProgram($id_program)
    {
        $this->db->select('DISTINCT(id_training_center) as id_training_center');
        $this->db->from('organisation_training_center_has_program');
        $this->db->where('id_program', $id_program);
        $query = $this->db->get();
        $results = $query->result(); 

        $details = array();
        foreach ($results as $value)
        {
            $id_training_center = $value->id_training_center;
            $training_center = $this->getTrainingCenter($id_training_center);
            array_push($details, $training_center);
        }
        return $details;
    }

    function getTrainingCenter($id)
    {
        $this->db->select('p.*');
        $this->db->from('organisation_has_training_center as p');
        $this->db->where('p.id', $id);
         $query = $this->db->get();
         
         $result = $query->row();  
         return $result;
    }


    function getFeeStructureByIdProgrammeForTrainingCenter($id_programme,$id_program_landscape,$currency)
    {
       $this->db->select('DISTINCT(p.id_training_center) as id_training_center, p.id as id_fee_structure, p.is_installment, p.installments,p.currency, p.amount, p.id_fee_item, fs.name as currency, fs.code as currency_code');
        $this->db->from('fee_structure as p');
        $this->db->join('currency_setup as fs', 'p.currency = fs.id');
        $this->db->where('p.id_programme', $id_programme);
        $this->db->where('p.id_program_landscape', $id_program_landscape);
        // $this->db->where('p.currency', $currency);
        $query = $this->db->get();
        $results = $query->result();

        $details = array();


        // echo "<Pre>";print_r($results);exit();


        foreach ($results as $result)
        {
            $result->fee_code = '';
            $result->fee_name = '';
            $id_training_center = $result->id_training_center;
            $id_fee_item = $result->id_fee_item;
            $id_fee_structure = $result->id_fee_structure;

            $is_installment = $result->is_installment;

            


            $training_center = $this->getPartnerUniversity($id_training_center);
            $fee_item = $this->getFeeItem($id_fee_item);

            


            if($training_center)
            {

                if($is_installment == 1)
                {
                    $fee_structure_installment_details = $this->getTrainingCenterInstallmentDetailsForTotalCalculation($id_fee_structure);

                    // echo "<Pre>";print_r($fee_structure_installment_details);exit();

                    $total_instalment_amount = 0;
                    foreach ($fee_structure_installment_details as $installment_detail)
                    {
                        $amount = $installment_detail->amount;
                        $total_instalment_amount = $total_instalment_amount + $amount;
                    }

                    $training_center->amount = $total_instalment_amount;

                }
                else
                {
                    $training_center->amount = $result->amount;
                }



                $training_center->is_installment = $result->is_installment;
                $training_center->installments = $result->installments;
                $training_center->currency = $result->currency;
                $training_center->currency_code = $result->currency_code;
                $training_center->id_fee_structure = $result->id_fee_structure;
                $training_center->fee_code = '';
                $training_center->fee_name = '';

                if($fee_item)
                {
                    $training_center->fee_code = $fee_item->code;
                    $training_center->fee_name = $fee_item->name;
                }
                array_push($details, $training_center);
            }
        }

        return $details;
    }

    function getFeeStructureByIdProgrammeForTrainingCenterForCopy($id_program_landscape)
    {
       $this->db->select('p.*');
        $this->db->from('fee_structure as p');
        $this->db->join('currency_setup as fs', 'p.currency = fs.id');
        // $this->db->where('p.id_programme', $id_programme);
        $this->db->where('p.id_program_landscape', $id_program_landscape);
        // $this->db->where('p.currency', $currency);
        $query = $this->db->get();
        $results = $query->result();

        return $results;

    }

    function getPartnerUniversity($id)
    {
        $this->db->select('p.*');
        $this->db->from('partner_university as p');
        $this->db->where('p.id', $id);
         $query = $this->db->get();
         
         $result = $query->row();  
         return $result;
    }

    function getFeeItem($id)
    {
        $this->db->select('p.*');
        $this->db->from('fee_setup as p');
        $this->db->where('p.id', $id);
         $query = $this->db->get();
         
         $result = $query->row();  
         return $result;
    }

    function checkDuplicationTrainingCenterFeeStructure($data)
    {
        $this->db->select('p.*');
        $this->db->from('fee_structure as p');
        $this->db->where('p.id_training_center', $data['id_training_center']);
        $this->db->where('p.id_program_landscape', $data['id_program_landscape']);
         $query = $this->db->get();
         
         $result = $query->row();  
         return $result;
    }

    function getTrainingCenterFeeStructure($data)
    {
        $this->db->select('p.*, fs.code as currency_code, fs.name as currency');
        $this->db->from('fee_structure as p');
        $this->db->join('currency_setup as fs', 'p.currency = fs.id');
        // $this->db->where('p.id_training_center', $data['id_training_center']);
        // $this->db->where('p.id_program_landscape', $data['id_program_landscape']);
        $this->db->where('p.id', $data['id_fee_structure']);
         $query = $this->db->get();
         
         $result = $query->row();  
         return $result;
    }

    function deleteTrainingCenterByTrainingCenterId($id_training_center)
    {
        $this->db->where('id_training_center', $id_training_center);
        $this->db->delete('fee_structure');

       return TRUE;
    }


    function deleteFeeStructure($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('fee_structure');

       return TRUE;
    }

    function deleteFeeStructureInstallmentByIdFeeStructure($id)
    {
        $this->db->where('id_fee_structure', $id);
        $this->db->delete('fee_structure_has_training_center');

       return TRUE;
    }

    function semesterListByStatus($status)
    {
        $this->db->select('p.*');
        $this->db->from('semester as p');
        $this->db->where('p.status', $status);
        $this->db->order_by('p.name', 'ASC');
         $query = $this->db->get();
         
         $result = $query->result();  
         return $result;
    }

    function getFeeStructureTriggerListByStatus($status)
    {
        $this->db->select('p.*');
        $this->db->from('fee_structure_triggering_point as p');
        $this->db->where('p.status', $status);
        $this->db->order_by('p.name', 'ASC');
        $query = $this->db->get();
         
        $result = $query->result();  
        return $result;
    }

    function getTrainingCenterInstallmentDetailsForTotalCalculation($id_fee_structure)
    {
        $this->db->select('p.*, sem.name as fee_name, sem.code as fee_code, fm.name as frequency_mode');
        $this->db->from('fee_structure_has_training_center as p');
        $this->db->join('fee_setup as sem', 'p.id_fee_item = sem.id');
        $this->db->join('frequency_mode as fm', 'sem.id_frequency_mode = fm.id');
        $this->db->where('p.id_fee_structure', $id_fee_structure);
         $query = $this->db->get();
         
         $result = $query->result();  
         return $result;
    }

    function getTrainingCenterInstallmentDetails($data)
    {
        $this->db->select('p.*, sem.name as fee_name, sem.code as fee_code, fm.name as frequency_mode, fstp.name as trigger_name');
        $this->db->from('fee_structure_has_training_center as p');
        $this->db->join('fee_setup as sem', 'p.id_fee_item = sem.id');
        $this->db->join('frequency_mode as fm', 'sem.id_frequency_mode = fm.id');
        $this->db->join('fee_structure_triggering_point as fstp', 'p.id_fee_structure_trigger = fstp.id');
        $this->db->where('p.id_fee_structure', $data['id_fee_structure']);
        $this->db->where('p.id_program_landscape', $data['id_program_landscape']);
         $query = $this->db->get();
         
         $result = $query->result();  
         return $result;
    }

    function getTrainingCenterInstallmentDetailsForCopy($data)
    {
        $this->db->select('p.*');
        $this->db->from('fee_structure_has_training_center as p');
        $this->db->join('fee_setup as sem', 'p.id_fee_item = sem.id');
        $this->db->join('frequency_mode as fm', 'sem.id_frequency_mode = fm.id');
        $this->db->join('fee_structure_triggering_point as fstp', 'p.id_fee_structure_trigger = fstp.id');
        $this->db->where('p.id_fee_structure', $data['id_fee_structure']);
        $this->db->where('p.id_program_landscape', $data['id_program_landscape']);
         $query = $this->db->get();
         
         $result = $query->result();  
         return $result;
    }

    function deleteSemesterDataByIdFeeStructureTrainingCenter($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('fee_structure_has_training_center');

       return TRUE;
    }

    function saveInstallmentData($data)
    {
        $this->db->trans_start();
        $this->db->insert('fee_structure_has_training_center', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function copyFeeStructureByProgramLandscape($id_old_program_landscape,$id_new_program_landscape)
    {
        $fee_structure_local_list = $this->getFeeStructureByIdProgrammeNIdIntakeForCopy($id_old_program_landscape,'MYR');
        $fee_structure_international_list = $this->getFeeStructureByIdProgrammeNIdIntakeForCopy($id_old_program_landscape,'USD');
        $fee_structure_partner_list = $this->getFeeStructureByIdProgrammeForTrainingCenterForCopy($id_old_program_landscape);


        foreach ($fee_structure_local_list as $fee_local)
        {
            unset($fee_local->id);
            $fee_local->id_program_landscape = $id_new_program_landscape;

            $added_local_fee = $this->addNewFeeStructure($fee_local);
        }


        foreach ($fee_structure_international_list as $fee_international)
        {
            unset($fee_international->id);
            $fee_international->id_program_landscape = $id_new_program_landscape;

            $added_international_fee =$this->addNewFeeStructure($fee_international);
        }


        foreach ($fee_structure_partner_list as $fee_partner)
        {
            $id_partner_fee = $fee_partner->id;
            $is_installment = $fee_partner->is_installment;
            $installments = $fee_partner->installments;
            $id_training_center = $fee_partner->id_training_center;
            if($id_training_center > 0)
            {                
                unset($fee_partner->id);
                $fee_partner->id_program_landscape = $id_new_program_landscape;

                $added_partner_fee =$this->addNewFeeStructure($fee_partner);

                if($is_installment == 1 && $installments > 0)
                {
                    $data['id_fee_structure'] = $id_partner_fee;
                    $data['id_program_landscape'] = $id_old_program_landscape;

                    $fee_partner_installments = $this->getTrainingCenterInstallmentDetailsForCopy($data);

                    foreach ($fee_partner_installments as $fee_partner_installment)
                    {
                        unset($fee_partner_installment->id);
                        $fee_partner_installment->id_fee_structure = $added_partner_fee;
                        $fee_partner_installment->id_program_landscape = $id_new_program_landscape;

                        $added_installment_data = $this->saveInstallmentData($fee_partner_installment);
                    }
                }
            }
        }

        return TRUE;
    }

    function qualificationListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('education_level');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function getIntakeListByProgramme($id_programme)
    {
        $this->db->select('DISTINCT(ihp.id_intake) as id_intake, ihp.*, in.name as intake_name, in.year as intake_year');
        $this->db->from('intake_has_programme as ihp');
        $this->db->join('intake as in', 'ihp.id_intake = in.id');
        $this->db->where('ihp.id_programme', $id_programme);
        $query = $this->db->get();
         return $query->result();
    }

    function getProgramSchemeByProgramId($id_programme)
    {
        $this->db->select('DISTINCT(ihs.mode_of_program) as mode_of_program, ihs.*');
        $this->db->from('programme_has_scheme as ihs');
        $this->db->where('ihs.id_program', $id_programme);
        $query = $this->db->get();
        return $query->result();
    }

    function getSchemeByProgramId($id_programme)
    {
        // echo "<Pre>"; print_r($id_programme);exit;
        $this->db->select('DISTINCT(phs.id_scheme) as id_scheme');
        $this->db->from('program_has_scheme as phs');
        $this->db->join('scheme as sch', 'phs.id_scheme = sch.id');
        $this->db->where('phs.id_program', $id_programme);
        $query = $this->db->get();
        $results = $query->result();
            
        // echo "<Pre>"; print_r($results);exit;

        $details = array();

        foreach ($results as $result)
        {

            $id_scheme = $result->id_scheme;

            $scheme = $this->getScheme($id_scheme);
            if($scheme)
            {
                $result = $scheme;
                array_push($details, $result);
            }
        }

        return $details;
    }

    function getScheme($id_scheme)
    {
        $this->db->select('ihs.*');
        $this->db->from('scheme as ihs');
        $this->db->where('ihs.id', $id_scheme);
        $query = $this->db->get();
        return $query->row();
    }

    function getProgrammeByEducationLevelId()
    {
        $this->db->select('ihs.*');
        $this->db->from('programme as ihs');
        $this->db->where('ihs.status', '1');
        $query = $this->db->get();
        return $query->result();
    }

    function addNewFeeStructureMaster($data)
    {
        $this->db->trans_start();
        $this->db->insert('fee_structure_master', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editFeeStructureMaster($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('fee_structure_master',$data);

       return TRUE;
    }

    function getFeeamountdetails($id,$feeitemid)
    {
        $this->db->select('a.*');
        $this->db->from('fee_structure as a');
        $this->db->where('a.id_program_landscape', $id);
        $this->db->where('a.id_fee_item', $feeitemid);
        $query = $this->db->get();
        
        return $query->row();
    }

    function getAllFeeitemsFromDb() {
         $this->db->select('distinct(p.id) as feeitemid,p.name');
        $this->db->from('fee_structure as a');
        $this->db->join('fee_setup as p', 'a.id_fee_item = p.id');
        $this->db->join('fee_structure_master as f', 'f.id = a.id_program_landscape');

       

         $query = $this->db->get();
         $result = $query->result();
         // print_r($applicantList);exit();     
         return $result;
    }

    function categoryListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('category');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function productTypeSetupListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('product_type');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function feeStructureMasterListSearch($data)
    {
        $this->db->select('a.*, p.name as program, p.code as program_code');
        $this->db->from('fee_structure_master as a');
        $this->db->join('programme as p', 'a.id_programme = p.id');
        if($data['name'] != '')
        {
            $likeCriteria = "(a.name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['id_category'] != '')
        {
            $this->db->where('p.id_category', $data['id_category']);
        }
        if ($data['id_programme_type'] != '')
        {
            $this->db->where('p.id_programme_type', $data['id_programme_type']);
        }

         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function getFeeStructureMaster($id)
    {
        $this->db->select('a.*, p.name as program, p.code as program_code, cs.code as currency_code, cs.name as currency_name');
        $this->db->from('fee_structure_master as a');
        $this->db->join('programme as p', 'a.id_programme = p.id');
        $this->db->join('currency_setup as cs', 'a.id_currency = cs.id','left');
        $this->db->where('a.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getProgrammeFeeStructureDuplication($data)
    {
        $this->db->select('st.*');
        $this->db->from('fee_structure as st');        
        if($data['id'] != '')
        {
            $this->db->where('st.id !=', $data['id']);
        }
        if($data['id_fee_item'] != '')
        {
            $this->db->where('st.id_fee_item', $data['id_fee_item']);
        }
        if($data['id_program_landscape'] != '')
        {
            $this->db->where('st.id_program_landscape', $data['id_program_landscape']);
        }
        $query = $this->db->get();
        $result = $query->row();

        return $result;
    }
}