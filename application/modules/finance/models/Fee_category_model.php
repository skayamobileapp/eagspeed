<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Fee_category_model extends CI_Model
{
    function feeCategoryListSearch()
    {
        $this->db->select('fc.*, cre.name as creater_name, upd.name as updater_name');
        $this->db->from('fee_category as fc');
        $this->db->join('users as cre','fc.created_by = cre.id','left');
        $this->db->join('users as upd','fc.updated_by = upd.id','left');
        // $this->db->join('country as c', 'sp.id_country = c.id');
        $this->db->order_by("fc.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function feeCategoryListByStatus($status)
    {
        $this->db->select('fc.*');
        $this->db->from('fee_category as fc');
        $this->db->where('fc.status', $status);
        $this->db->order_by("fc.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getfeeCategory($id)
    {
        $this->db->select('fc.*');
        $this->db->from('fee_category as fc');
        $this->db->where('fc.id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewfeeCategory($data)
    {
        $this->db->trans_start();
        $this->db->insert('fee_category', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function addNewfeeCategoryDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('fee_category_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function editfeeCategory($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('fee_category', $data);
        return TRUE;
    }
}

