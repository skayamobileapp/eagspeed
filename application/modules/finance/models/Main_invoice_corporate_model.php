<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Main_invoice_corporate_model extends CI_Model
{
    function mainInvoiceList()
    {
        $this->db->select('mi.*, s.full_name as student');
        $this->db->from('main_invoice as mi');
        $this->db->join('student as s', 'mi.id_student = s.id');
        // $this->db->join('country as c', 'sp.id_country = c.id');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getMainInvoiceListByStatus($data)
    {

        $this->db->select('mi.*, p.code as programme_code, p.name as programme_name, cs.name as currency_name');
        $this->db->from('main_invoice as mi');
        $this->db->join('programme as p', 'mi.id_program = p.id','left');
        $this->db->join('currency_setup as cs', 'mi.currency = cs.id','left');
        // $this->db->join('student as s', 'mi.id_student = s.id');
        // $this->db->join('intake as i', 's.id_intake = i.id');

        if ($data['invoice_number'] != '')
        {
            $likeCriteria = "(mi.invoice_number  LIKE '%" . $data['invoice_number'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['type'] != '')
        {
            $this->db->where('mi.type', $data['type']);
        }
        if ($data['id_programme'] != '')
        {
            $this->db->where('mi.id_program', $data['id_programme']);
        }
        if ($data['id_student'] != '')
        {
            $this->db->where('mi.id_student', $data['id_student']);
        }
        if ($data['status'] != '')
        {
            $this->db->where('mi.status', $data['status']);
        }
        $this->db->where("mi.type !=", "Sponsor");
        $this->db->where("mi.type !=", "Partner University");
        $this->db->order_by("mi.id", "DESC");
        // $this->db->join('country as c', 'sp.id_country = c.id');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getMainInvoiceListByStatusForCancellation($data)
    {
        $and ='';

        $this->db->select('mi.*, p.code as programme_code, p.name as programme_name, cs.name as currency_name');
        $this->db->from('main_invoice as mi');
        $this->db->join('programme as p', 'mi.id_program = p.id','left');
        $this->db->join('currency_setup as cs', 'mi.currency = cs.id','left');
        // $this->db->join('student as s', 'mi.id_student = s.id');
        // $this->db->join('intake as i', 's.id_intake = i.id');
        if ($data['name'] != '')
        {
            $likeCriteria = "(s.full_name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
            $and =' and';

        }
        if ($data['nric'] != '')
        {
            $likeCriteria = "(s.nric  LIKE '%" . $data['nric'] . "%')";
            $this->db->where($likeCriteria);
            $and =' and';
        }
        if ($data['invoice_number'] != '')
        {
            $likeCriteria = "(mi.invoice_number  LIKE '%" . $data['invoice_number'] . "%')";
            $this->db->where($likeCriteria);
            $and =' and';
        }
        if ($data['type'] != '')
        {
            $this->db->where('mi.type', $data['type']);
            $and =' and';
        }
        if ($data['id_programme'] != '')
        {
            $this->db->where('mi.id_program', $data['id_programme']);
            $and =' and';
        }
        if ($data['id_intake'] != '')
        {
            $this->db->where('mi.id_intake', $data['id_intake']);
            $and =' and';
        }
        if ($data['status'] != '')
        {
            $this->db->where('mi.status', $data['status']);
            if($data['status'] == 1)
            {
                if($and =='')
                {
                    $and = ' ';
                }
                $this->db->where("mi.paid_amount", 0);

                // $likeCriteria = " mi.id NOT IN (SELECT id_main_invoice FROM receipt_details)";
                // $this->db->where($likeCriteria);                
            }
        }
        $this->db->order_by("mi.id", "DESC");
        // $this->db->join('country as c', 'sp.id_country = c.id');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function editMainInvoiceList($data,$id)
    {
        // $status = ['status'=>'1'];
      $this->db->where_in('id', $id);
      $this->db->update('main_invoice', $data);
    }

    function getMainInvoice($id)
    {
        $this->db->select('mi.*,  p.name as programme_name, p.code as programme_code, i.name as intake_name, i.year as intake_year, cs.name as currency_name');
        $this->db->from('main_invoice as mi');
        // $this->db->join('student as s', 'mi.id_student = s.id');
        $this->db->join('programme as p', 'mi.id_program = p.id','left');
        $this->db->join('intake as i', 'mi.id_intake = i.id','left');
        $this->db->join('currency_setup as cs', 'mi.currency = cs.id','left');
        $this->db->where('mi.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getMainInvoiceDetails($id)
    {
        $this->db->select('mid.*, fstp.name as fee_setup');
        $this->db->from('main_invoice_details as mid');
        $this->db->join('fee_setup as fstp', 'mid.id_fee_item = fstp.id');        
        // $this->db->join('fee_structure as fs', 'mid.id_fee_item = fs.id');        
        // $this->db->join('frequency_mode as fm', 'fstp.id_frequency_mode = fm.id');        
        // $this->db->join('amount_calculation_type as amt', 'fstp.id_amount_calculation_type = amt.id');        
        $this->db->where('mid.id_main_invoice', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function getMainInvoiceDiscountDetails($id)
    {
        $this->db->select('*');
        $this->db->from('main_invoice_discount_details');   
        $this->db->where('id_main_invoice', $id);
        $query = $this->db->get();
        return $query->result();
    }
    
    function addNewMainInvoice($data)
    {
        $this->db->trans_start();
        $this->db->insert('main_invoice', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }


    function addNewMainInvoiceDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('main_invoice_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function editMainInvoice($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('main_invoice', $data);
        return TRUE;
    }

    function studentList()
    {
        $this->db->select('*');
        $this->db->from('student');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function companyList()
    {
        $this->db->select('*');
        $this->db->from('company');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function applicantList()
    {
        $this->db->select('*');
        $this->db->from('applicant');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function addNewTempMainInvoiceDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('temp_main_invoice_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function getTempMainInvoiceDetails($id_session)
    {
        $this->db->select('tmid.*, fs.name as fee_setup');
        $this->db->from('temp_main_invoice_details as tmid');
        $this->db->join('fee_setup as fs', 'tmid.id_fee_item = fs.id');        
        $this->db->where('tmid.id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }

    function deleteTempDataBySession($id_session)
    { 
       $this->db->where('id_session', $id_session);
       $this->db->delete('temp_main_invoice_details');
    }

    function deleteTempData($id)
    { 
        // echo "<Pre>";  print_r($id);exit;
       $this->db->where('id', $id);
       $this->db->delete('temp_main_invoice_details');
    }

    function addTempDetails($data)
    {
        // echo "<Pre>";  print_r($data);exit;

        $this->db->trans_start();
        $this->db->insert('temp_main_invoice_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function updateTempDetails($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('temp_main_invoice_details', $data);
        return TRUE;
    }

    function programmeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function intakeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('intake');
        $this->db->where('status', $status);
        $this->db->order_by("year", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function currencyListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('currency_setup');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function intakeList()
    {
        $this->db->select('*');
        $this->db->from('intake');
        $this->db->order_by("year", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function getStudentByProgrammeId($id_programme)
    {
        $this->db->select('*');
        $this->db->from('student');
        $this->db->where('id_program', $id_programme);
        $this->db->order_by("full_name", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function getApplicantListByData($data)
    {
        $this->db->select('*');
        $this->db->from('applicant');
        $this->db->where('id_program', $data['id_program']);
        // $this->db->where('id_intake', $data['id_intake']);
        $this->db->where('applicant_status', 'Approved');
        $this->db->order_by("full_name", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function getProgrammeById($id_programme)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('id', $id_programme);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getStudentByStudentId($id_student)
    {
        $this->db->select('s.*, p.name as programme_name, i.name as intake_name');
        $this->db->from('student as s');
        $this->db->join('programme as p', 's.id_program = p.id'); 
        $this->db->join('intake as i', 's.id_intake = i.id'); 
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getApplicantByApplicantId($id_applicant)
    {
        $this->db->select('s.*, p.name as programme_name, i.name as intake_name');
        $this->db->from('applicant as s');
        $this->db->join('programme as p', 's.id_program = p.id'); 
        $this->db->join('intake as i', 's.id_intake = i.id'); 
        $this->db->where('s.id', $id_applicant);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function generateMainInvoiceNumber()
    {
        $year = date('y');
        $Year = date('Y');
            $this->db->select('j.*');
            $this->db->from('main_invoice as j');
            $this->db->order_by("id", "desc");
            $query = $this->db->get();
            $result = $query->num_rows();

     
            $count= $result + 1;
           $jrnumber = $number = "INV" .(sprintf("%'06d", $count)). "/" . $Year;
           return $jrnumber;        
    }

    function getMainInvoiceApplicantData($id_applicant)
    {
        $this->db->select('app.full_name, app.nric, app.id_degree_type');
        $this->db->from('applicant as app');
        $this->db->where('app.id', $id_applicant);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getMainInvoiceStudentData($id_student)
    {
        $this->db->select('stu.full_name, stu.nric, stu.id_degree_type');
        $this->db->from('student as stu');
        $this->db->where('stu.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getMainInvoiceSponserData($id_sponser)
    {
        $this->db->select('stu.name as full_name, stu.code as nric');
        $this->db->from('sponser as stu');
        $this->db->where('stu.id', $id_sponser);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getMainInvoiceCorporateData($id)
    {
        $this->db->select('stu.name as full_name, stu.registration_number as nric');
        $this->db->from('company as stu');
        $this->db->where('stu.id', $id);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }


    function getMainInvoicePartnerData($id_sponser)
    {
        $this->db->select('stu.name as full_name, stu.code as nric');
        $this->db->from('partner_university as stu');
        $this->db->where('stu.id', $id_sponser);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }


    function getMainInvoicePartnerUniversityData($id_partner_university)
    {
        $this->db->select('stu.name as full_name, stu.code as nric');
        $this->db->from('partner_university as stu');
        $this->db->where('stu.id', $id_partner_university);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function qualificationListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('education_level');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function feeSetupList()
    {
        $this->db->select('fs.*, fc.name as fee_category, act.name as amount_calculation_type, fm.name as frequency_mode');
        $this->db->from('fee_setup as fs');
        $this->db->join('fee_category as fc', 'fs.id_fee_category = fc.id');
        $this->db->join('amount_calculation_type as act', 'fs.id_amount_calculation_type = act.id');
        $this->db->join('frequency_mode as fm', 'fs.id_frequency_mode = fm.id');
        // $this->db->join('account_code as ac', 'fs.id_account_code = ac.code');
        $this->db->order_by("fs.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function sponserListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('sponser');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function getStudentByStudent($id_student)
    {
        $this->db->select('s.*, p.name as programme_name, i.name as intake_name, qs.name as qualification_name, qs.code as qualification_code, adv.ic_no, adv.name as advisor');
        $this->db->from('student as s');
        $this->db->join('programme as p', 's.id_program = p.id'); 
        $this->db->join('intake as i', 's.id_intake = i.id','left'); 
        $this->db->join('qualification_setup as qs', 's.id_degree_type = qs.id','left');
        $this->db->join('staff as adv', 's.id_advisor = adv.id','left');
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getApplicantInformation($id)
    {
        $this->db->select('a.*, p.code as program_code, p.name as program_name, i.year as intake_year, i.name as intake_name');
        $this->db->from('applicant as a');
        $this->db->join('programme as p', 'a.id_program = p.id');
        $this->db->join('intake as i', 'a.id_intake = i.id');
        $this->db->where('a.id', $id);
        $query = $this->db->get();
        $applicant = $query->row();
        return $applicant;
    }

    function gettemplate($id) {
        $this->db->select('*');
        $this->db->from('communication_template');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $intake = $query->row();
        return $intake;
    }

    function getMainInvoiceDetailsForCourseRegistrationShow($id)
    {
        $this->db->select('mid.*, fstp.name as fee_setup, fm.code as frequency_mode, amt.code as amount_calculation_type');
        $this->db->from('main_invoice_details as mid');
        // $this->db->join('fee_structure as fs', 'mid.id_fee_item = fs.id');        
        $this->db->join('fee_setup as fstp', 'mid.id_fee_item = fstp.id');        
        $this->db->join('frequency_mode as fm', 'fstp.id_frequency_mode = fm.id');      
        $this->db->join('amount_calculation_type as amt', 'fstp.id_amount_calculation_type = amt.id');        
        $this->db->where('mid.id_main_invoice', $id);
        $query = $this->db->get();
        $results = $query->result();
        $details = array();
        foreach ($results as $result)
        {
        // echo "<Pre>";print_r($results);exit;

            if($result->id_reference > 0 &&  $result->description == 'CREDIT HOUR MULTIPLICATION')
            {
               $course_registered = $this->getCourseRegistered($result->id_reference);
               $result->course_code = $course_registered->code;
               $result->course_name = $course_registered->name;

            }

           array_push($details, $result);
        }

        return $details;
    }

    function getCourseRegistered($id)
    {
        $this->db->select('c.*');
        $this->db->from('course_registration as cr');
        $this->db->join('course as c', 'cr.id_course = c.id');      
        $this->db->where('cr.id', $id);
        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    function getSemesterByMainInvoiceDetailsForCourseRegistrationShow($id)
    {
        $this->db->select('s.*');
        $this->db->from('main_invoice_details as mid');      
        $this->db->join('course_registration as cr', 'mid.id_reference = cr.id');        
        $this->db->join('semester as s', 'cr.id_semester = s.id');
        $this->db->where('mid.description', 'CREDIT HOUR MULTIPLICATION');
        $this->db->where('mid.id_main_invoice', $id);
        $query = $this->db->get();
        $result = $query->row();

        return $result;
    }

    function getBankRegistration()
    {
        $this->db->select('fc.*, c.name as country, s.name as state');
        $this->db->from('bank_registration as fc');
        $this->db->join('country as c', 'fc.id_country = c.id');
        $this->db->join('state as s', 'fc.id_state = s.id');
        $this->db->where('fc.status', 1);
        $this->db->order_by("fc.id", "DESC");
        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    function getOrganisation()
    {
        $this->db->select('fc.*');
        $this->db->from('organisation as fc');
        $this->db->where('fc.status', 1);
        $this->db->order_by("fc.id", "DESC");
        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    function partnerUniversityListByStatus($status)
    {
        $this->db->select('fc.*');
        $this->db->from('partner_university as fc');
        $this->db->order_by("fc.status", $status);
        $this->db->order_by("fc.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getMainInvoiceStudentDetails($idinvoice,$stu_or_app)
    {
        $this->db->select('mid.*, s.full_name as student_name, s.nric, s.email_id, s.phone, p.code as program_code, p.name as program_name, qs.short_name as qualification_code, qs.name as qualification_name, i.year as intake_year, i.name as intake_name ');
        $this->db->from('partner_university_invoice_student_details as mid');
        if($stu_or_app > 0)
        {
            $this->db->join('applicant as s', 'mid.id_student = s.id');
        }else
        {
            $this->db->join('student as s', 'mid.id_student = s.id');
        }       
        // $this->db->join('student as s', 'mid.id_student = s.id');
        $this->db->join('programme as p', 's.id_program = p.id');
        $this->db->join('education_level as qs', 's.id_degree_type = qs.id');
        $this->db->join('intake as i', 's.id_intake = i.id');
        // $this->db->join('staff as adt', 's.id_advisor = adt.id','left');
        $this->db->where('mid.id_main_invoice', $idinvoice);
        $query = $this->db->get();
        return $query->result();
    }

    function getStudentListByData()
    {
        $this->db->select('s.*');
        $this->db->from('student as s');
        $this->db->where('s.id_company', '0');
        $this->db->where('s.id_company_user', '0');
        // $this->db->where('s.approval_status !=', 'Graduated');
        $this->db->order_by("s.full_name", "ASC");
        $query = $this->db->get();
        $result = $query->result();

        return $result;
    }

    function getStudent($id)
    {
        $this->db->select('cn.*');
        $this->db->from('student as cn');
        $this->db->where('cn.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getCorporateListByData()
    {
        $this->db->select('s.*');
        $this->db->from('company as s');       
        $this->db->where('s.status', '1');
        $query = $this->db->get();
        $result = $query->result();

        return $result;
    }

    function getCompany($id)
    {
        $this->db->select('cn.*');
        $this->db->from('company as cn');
        $this->db->where('cn.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getFeeStructureMaster($id_programme)
    {
        $this->db->select('*');
        $this->db->from('fee_structure_master');
        $this->db->where('id_programme', $id_programme);
        $this->db->order_by("id", "ASC");
        $query = $this->db->get();
        return $query->row();
    }

    function getFeeStructureByData($data)
    {
        $this->db->select('fst.*, fs.name as fee_structure, fs.code as fee_structure_code, cs.name as currency_name');
        $this->db->from('fee_structure as fst');
        $this->db->join('fee_setup as fs', 'fst.id_fee_item = fs.id','left');
        $this->db->join('currency_setup as cs', 'fst.currency = cs.id','left');
        $this->db->where('fst.id_programme', $data['id_programme']);
        $this->db->where('fst.id_program_landscape', $data['id_fee_structure_master']);
        $this->db->where('fst.currency', $data['currency']);
        // $this->db->where('fstp.name', $data['trigger']);
        $this->db->where('fst.status', '1');
        $query = $this->db->get();

        $result = $query->result();
        // echo "<Pre>";print_r($result);exit();
        return $result;
    }

    function studentSearch($data)
    {
        $this->db->select('s.*, adt.ic_no, adt.name as advisor_name, cm.name as company_name, cm.registration_number');
        $this->db->from('student as s');
        $this->db->join('programme as p', 's.id_program = p.id','left');
        $this->db->join('staff as adt', 's.id_advisor = adt.id','left');
        $this->db->join('company as cm', 's.id_company = cm.id');
        if ($data['full_name'] != '')
        {
            $likeCriteria = "(s.full_name  LIKE '%" . $data['full_name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['email_id'] != '')
        {
            $this->db->where('s.email_id', $data['email_id']);
        }
        if ($data['id_advisor'] != '')
        {
            $this->db->where('s.id_advisor', $data['id_advisor']);
        }
        if ($data['id_company'] != '')
        {
            $this->db->where('s.id_company', $data['id_company']);
        }
        $this->db->where('s.applicant_status !=', 'Graduated');
        $query = $this->db->get();
        $result = $query->result(); 

        return $result;
    }

    function getFinanceConfiguration()
    {
        $this->db->select('*');
        $this->db->from('finance_configuration');
        $this->db->order_by("id", "ASC");
        $query = $this->db->get();
        return $query->row();
    }

    function getFeeSetup($id)
    {
        $this->db->select('fst.*');
        $this->db->from('fee_setup as fst');
        $this->db->where('fst.id', $id);
        $query = $this->db->get();
        $result = $query->row();
        return $result;
    }

    function addNewMainInvoiceHasStudents($id_students,$id_main_invoice,$amount)
    {
        $student_count = count($id_students);
        foreach ($id_students as $id_student)
        {
            // echo "<Pre>";print_r($id_student);exit;

            $details['id_student'] = $id_student;
            $details['id_main_invoice'] = $id_main_invoice;
            $details['amount'] = $amount / $student_count;

            $this->db->trans_start();
            $this->db->insert('main_invoice_has_students', $details);
            $insert_id = $this->db->insert_id();
            $this->db->trans_complete();

        }
        
        return $insert_id;
    }

    function getProgramme($id)
    {
        $this->db->select('ihs.*');
        $this->db->from('programme as ihs');
        $this->db->where('ihs.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function addNewStudentHasProgramme($data)
    {
        $this->db->trans_start();
        $this->db->insert('student_has_programme', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getMainInvoiceHasStudentList($id)
    {
        $this->db->select('pmhs.*, s.full_name as student_name, s.nric');
        $this->db->from('main_invoice_has_students as pmhs');   
        $this->db->join('student as s', 'pmhs.id_student = s.id');
        $this->db->where('pmhs.id_main_invoice', $id);
        $query = $this->db->get();
        return $query->result();
    }
}