<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Currency_model extends CI_Model
{
    function currencyList()
    {
        $this->db->select('*');
        $this->db->from('currency_setup');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function currencyListSearch($search)
    {
        $this->db->select('cs.*, cre.name as creater_name, upd.name as updater_name');
        $this->db->from('currency_setup as cs');
        $this->db->join('users as cre','cs.created_by = cre.id','left');
        $this->db->join('users as upd','cs.updated_by = upd.id','left');
        if (!empty($search))
        {
            $likeCriteria = "(cs.name  LIKE '%" . $search . "%' or cs.code  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("cs.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getCurrencySetup($id)
    {
        $this->db->select('*');
        $this->db->from('currency_setup');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewCurrency($data)
    {
        $this->db->trans_start();
        $this->db->insert('currency_setup', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editCurrency($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('currency_setup', $data);
        return TRUE;
    }

    function editCurrencyNot($data, $id)
    {
        $this->db->where('id !=', $id);
        $this->db->update('currency_setup', $data);
        return TRUE;
    }
}

