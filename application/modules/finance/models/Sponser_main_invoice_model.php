<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Sponser_main_invoice_model extends CI_Model
{
    function mainInvoiceList()
    {
        $this->db->select('mi.*, s.full_name as student');
        $this->db->from('main_invoice as mi');
        $this->db->join('student as s', 'mi.id_student = s.id');
        // $this->db->join('country as c', 'sp.id_country = c.id');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getMainInvoiceListByStatus($data)
    {

        $this->db->select('mi.*, s.full_name as student_name, s.nric as student_nric, spo.name as sponser_name, spo.code as sponser_code');
        $this->db->from('main_invoice as mi');
        $this->db->join('student as s', 'mi.id_student = s.id');
        $this->db->join('sponser as spo', 'mi.id_sponser = spo.id');
        if ($data['invoice_number'] != '')
        {
            $likeCriteria = "(mi.invoice_number  LIKE '%" . $data['invoice_number'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['type'] != '')
        {
            $this->db->where('mi.type', $data['type']);
        }
        if ($data['id_student'] != '')
        {
            $this->db->where('mi.id_student', $data['id_student']);
        }
        if ($data['id_sponser'] != '')
        {
            $this->db->where('mi.id_sponser', $data['id_sponser']);
        }
        if ($data['status'] != '')
        {
            $this->db->where('mi.status', $data['status']);
        }
        // $this->db->order_by("mi.id", "ASC");
        // $this->db->join('country as c', 'sp.id_country = c.id');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }



    function getMainInvoiceListByStatusForCancelation($data)
    {
        $and ='';

        $this->db->select('mi.*, s.full_name as student_name, s.nric as student_nric, spo.name as sponser_name, spo.code as sponser_code');
        $this->db->from('main_invoice as mi');
        $this->db->join('student as s', 'mi.id_student = s.id');
        $this->db->join('sponser as spo', 'mi.id_sponser = spo.id');
        if ($data['invoice_number'] != '')
        {
            $likeCriteria = "(mi.invoice_number  LIKE '%" . $data['invoice_number'] . "%')";
            $this->db->where($likeCriteria);
            $and =' and';
        }
        if ($data['type'] != '')
        {
            $this->db->where('mi.type', $data['type']);
            $and =' and';
        }
        if ($data['id_student'] != '')
        {
            $this->db->where('mi.id_student', $data['id_student']);
            $and =' and';
        }
        if ($data['id_sponser'] != '')
        {
            $this->db->where('mi.id_sponser', $data['id_sponser']);
            $and =' and';
        }
        if ($data['status'] != '')
        {
            $this->db->where('mi.status', $data['status']);
            if($data['status'] == 1)
            {
                if($and =='')
                {
                    $and = ' ';
                }
                $likeCriteria =  " mi.id NOT IN (SELECT id_main_invoice FROM receipt_details)";
                $this->db->where($likeCriteria);                
            }
        }
        // $this->db->order_by("mi.id", "ASC");
        // $this->db->join('country as c', 'sp.id_country = c.id');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function editMainInvoiceList($data,$id)
    {
        // $status = ['status'=>'1'];
      $this->db->where_in('id', $id);
      $this->db->update('main_invoice', $data);
    }

    function getMainInvoice($id)
    {
        $this->db->select('mi.*, s.full_name as student_name, s.nric as student_nric, spo.name as sponser_name, spo.code as sponser_code, i.year as intake_year, i.name as intake_year, p.code as program_code, p.name as program_name');
        $this->db->from('main_invoice as mi');
        $this->db->join('student as s', 'mi.id_student = s.id');
        $this->db->join('sponser as spo', 'mi.id_sponser = spo.id');
        $this->db->join('intake as i', 's.id_intake = i.id');
        $this->db->join('programme as p', 's.id_intake = p.id');
        $this->db->where('mi.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getMainInvoiceDetails($id)
    {
        $this->db->select('mid.*, fstp.name as fee_setup');
        $this->db->from('main_invoice_details as mid');
        // $this->db->join('fee_structure as fs', 'mid.id_fee_item = fs.id');        
        $this->db->join('fee_setup as fstp', 'mid.id_fee_item = fstp.id');        
        $this->db->where('mid.id_main_invoice', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function getMainInvoiceDiscountDetails($id)
    {
        $this->db->select('*');
        $this->db->from('main_invoice_discount_details');   
        $this->db->where('id_main_invoice', $id);
        $query = $this->db->get();
        return $query->result();
    }
    
    function addNewMainInvoice($data)
    {
        $this->db->trans_start();
        $this->db->insert('main_invoice', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }


    function addNewMainInvoiceDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('main_invoice_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function editMainInvoice($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('main_invoice', $data);
        return TRUE;
    }

    function studentList()
    {
        $this->db->select('*');
        $this->db->from('student');
        $this->db->where('applicant_status !=', 'Graduated');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function applicantList()
    {
        $this->db->select('*');
        $this->db->from('applicant');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function addNewTempMainInvoiceDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('temp_main_invoice_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function getTempMainInvoiceDetails($id_session)
    {
        $this->db->select('tmid.*, fs.name as fee_setup');
        $this->db->from('temp_main_invoice_details as tmid');
        $this->db->join('fee_setup as fs', 'tmid.id_fee_item = fs.id');        
        $this->db->where('tmid.id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }

     function deleteTempDataBySession($id_session)
    { 
       $this->db->where('id_session', $id_session);
       $this->db->delete('temp_main_invoice_details');
    }

     function deleteTempData($id)
    { 
        // echo "<Pre>";  print_r($id);exit;
       $this->db->where('id', $id);
       $this->db->delete('temp_main_invoice_details');
    }

    function addTempDetails($data)
    {
        // echo "<Pre>";  print_r($data);exit;

        $this->db->trans_start();
        $this->db->insert('temp_main_invoice_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function updateTempDetails($data,$id) {
        $this->db->where('id', $id);
        $this->db->update('temp_main_invoice_details', $data);
        return TRUE;
    }

    function programmeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function intakeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('intake');
        $this->db->where('status', $status);
        $this->db->order_by("year", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function intakeList()
    {
        $this->db->select('*');
        $this->db->from('intake');
        $this->db->order_by("year", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function getStudentByProgrammeId($id_programme)
    {
        $this->db->select('*');
        $this->db->from('student');
        $this->db->where('id_program', $id_programme);
        $this->db->order_by("full_name", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function getApplicantListByData($data)
    {
        $this->db->select('*');
        $this->db->from('applicant');
        $this->db->where('id_program', $data['id_program']);
        $this->db->where('id_intake', $data['id_intake']);
        $this->db->where('applicant_status', 'Approved');
        $this->db->order_by("full_name", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function getStudentListByData($data)
    {
        $this->db->select('*');
        $this->db->from('student');
        $this->db->where('id_program', $data['id_program']);
        $this->db->where('id_intake', $data['id_intake']);
        $this->db->where('applicant_status', 'Approved');
        $this->db->order_by("full_name", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function getProgrammeById($id_programme)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('id', $id_programme);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getStudentByStudentId($id_student)
    {
        $this->db->select('s.*, p.name as programme_name, i.name as intake_name');
        $this->db->from('student as s');
        $this->db->join('programme as p', 's.id_program = p.id'); 
        $this->db->join('intake as i', 's.id_intake = i.id'); 
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getApplicantByApplicantId($id_applicant)
    {
        $this->db->select('s.*, p.name as programme_name, i.name as intake_name');
        $this->db->from('applicant as s');
        $this->db->join('programme as p', 's.id_program = p.id'); 
        $this->db->join('intake as i', 's.id_intake = i.id'); 
        $this->db->where('s.id', $id_applicant);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function generateMainInvoiceNumber()
    {
        $year = date('y');
        $Year = date('Y');
            $this->db->select('j.*');
            $this->db->from('main_invoice as j');
            $this->db->order_by("id", "desc");
            $query = $this->db->get();
            $result = $query->num_rows();

     
            $count= $result + 1;
           $jrnumber = $number = "INV" .(sprintf("%'06d", $count)). "/" . $Year;
           return $jrnumber;        
    }

    function getMainInvoiceApplicantData($id_applicant)
    {
        $this->db->select('app.full_name, app.nric, app.id_degree_type');
        $this->db->from('applicant as app');
        $this->db->where('app.id', $id_applicant);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getMainInvoiceStudentData($id_student)
    {
        $this->db->select('stu.full_name, stu.nric, stu.id_degree_type');
        $this->db->from('student as stu');
        $this->db->where('stu.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function qualificationList()
    {
        $this->db->select('*');
        $this->db->from('qualification_setup');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function feeSetupList()
    {
        $this->db->select('fs.*, fc.name as fee_category, act.name as amount_calculation_type, fm.name as frequency_mode');
        $this->db->from('fee_setup as fs');
        $this->db->join('fee_category as fc', 'fs.id_fee_category = fc.id');
        $this->db->join('amount_calculation_type as act', 'fs.id_amount_calculation_type = act.id');
        $this->db->join('frequency_mode as fm', 'fs.id_frequency_mode = fm.id');
        // $this->db->join('account_code as ac', 'fs.id_account_code = ac.code');
        $this->db->order_by("fs.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function sponserListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('sponser');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function getStudentBySponser($id_sponser)
    {
        $this->db->select('DISTINCT(id_student) as id_student');
        $this->db->from('sponser_has_students');
        $this->db->where('end_date >', date('Y-m-d'));
        $this->db->where('id_sponser', $id_sponser);
         $query = $this->db->get();
         $results = $query->result();
         // print_r($results);exit();
         $details = array();
         foreach ($results as $key => $result)
         {
            $student_data = $this->getStudentData($result->id_student);

            array_push($details, $student_data);
         }     
         return $details;
    }

    function getStudentData($id_student)
    {
        $this->db->select('*');
        $this->db->from('student');
        $this->db->where('applicant_status !=', 'Graduated');
        $this->db->where('id', $id_student);
         $query = $this->db->get();
         $result = $query->row();
         // print_r($result);exit();     
         return $result;
    }

    function getStudentByStudent($id_student)
    {
        $this->db->select('s.*, p.name as programme_name, i.name as intake_name, qs.name as qualification_name, qs.code as qualification_code, adv.ic_no, adv.name as advisor');
        $this->db->from('student as s');
        $this->db->join('programme as p', 's.id_program = p.id'); 
        $this->db->join('intake as i', 's.id_intake = i.id'); 
        $this->db->join('qualification_setup as qs', 's.id_degree_type = qs.id');
        $this->db->join('staff as adv', 's.id_advisor = adv.id','left');
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }
}

