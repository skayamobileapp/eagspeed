<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Discount_model extends CI_Model
{
    function intakeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('intake');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function discountTypeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('discount_type');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function currencyListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('currency_setup');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function discountListSearch($data)
    {
        $this->db->select('a.*, cs.name as currency_name, cre.name as creater_name, upd.name as updater_name');
        $this->db->from('discount as a');
        $this->db->join('currency_setup as cs', 'a.currency = cs.id','left');
        $this->db->join('users as cre','a.created_by = cre.id','left');
        $this->db->join('users as upd','a.updated_by = upd.id','left');

        if ($data['id_intake'] != '')
        {
            $this->db->where('a.id_intake', $data['id_intake']);
        }
        

        if($data['type'] != '') {
         $this->db->where('a.type', $data['type']);
        }

        $this->db->order_by("a.id", "DESC");
         $query = $this->db->get();
                  $result = $query->result();  
         return $result;
    }

    function getDiscount($id)
    {
        $this->db->select('*');
        $this->db->from('discount');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewDiscount($data)
    {
        $this->db->trans_start();
        $this->db->insert('discount', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editDiscount($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('discount', $data);
        return TRUE;
    }
}

