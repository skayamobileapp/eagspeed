      <div class="pt-lg-8 pb-lg-16 pt-8 pb-12 course-wrapper">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-xl-7 col-lg-7 col-md-12">
            <div  style="color: white;">
              <h1 class="text-white font-weight-semi-bold">
                                <?php echo $programmeList->name;?>

              </h1>
              <p class="text-white mb-6 lead">
                                <?php echo $programmeList->short_description;?>
              </p>
              <div class="d-flex align-items-center">
                <a
                  href="#!"
                  class="text-white text-decoration-none"
                  data-toggle="tooltip"
                  data-placement="top"
                  title=""
                  data-original-title="Add to
                           Bookmarks"
                >
                  <i class="fe fe-clock text-white-50 mr-2"></i>
                  <?php echo $programmeList->max_duration.' '.$programmeList->duration_type;?>
                </a>
                <span class="text-white ml-3"
                  ><i class="fe fe-user text-white-50"></i> 1200 Enrolled
                </span>
                <span class="ml-4 d-flex rating-star">
                  <span class="text-warning mr-1">4.5</span>
                  <span class="mr-1"
                    ><img src="/website/img/star_icon.svg" alt="star"
                  /></span>
                  <span class="text-white">(140)</span>
                </span>
                <span class="text-white ml-4 d-none d-md-block">
                  <svg
                    width="16"
                    height="16"
                    viewBox="0 0 16
                              16"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <rect
                      x="3"
                      y="8"
                      width="2"
                      height="6"
                      rx="1"
                      fill="#DBD8E9"
                    ></rect>
                    <rect
                      x="7"
                      y="5"
                      width="2"
                      height="9"
                      rx="1"
                      fill="#DBD8E9"
                    ></rect>
                    <rect
                      x="11"
                      y="2"
                      width="2"
                      height="12"
                      rx="1"
                      fill="#DBD8E9"
                    ></rect>
                  </svg>
                  <span class="align-middle"> Intermediate </span>
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="pb-10">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-12 col-12 mt-n8 mb-4 mb-lg-0">
            <!-- Card -->
            <div class="card rounded-lg">
              <!-- Card header -->
              <div class="card-header border-bottom-0 p-0">
                <div>
                  <!-- Nav -->
                  <ul class="nav nav-lb-tab" id="tab" role="tablist">
                   
                    <li class="nav-item">
                      <a
                        class="nav-link active"
                        id="description-tab"
                        data-toggle="pill"
                        href="#description"
                        role="tab"
                        aria-controls="description"
                        aria-selected="false"
                        >Aim</a
                      >
                    </li>

                    <li class="nav-item">
                      <a
                        class="nav-link"
                        id="objective-tab"
                        data-toggle="pill"
                        href="#objective"
                        role="tab"
                        aria-controls="objective"
                        aria-selected="false"
                        >Structure</a
                      >
                    </li>



                   <!--  <li class="nav-item">
                      <a
                        class="nav-link"
                        id="faq-tab"
                        data-toggle="pill"
                        href="#faq"
                        role="tab"
                        aria-controls="faq"
                        aria-selected="false"
                        >Award</a
                      >
                    </li> -->


                     <li class="nav-item">
                      <a
                        class="nav-link"
                        id="award-tab"
                        data-toggle="pill"
                        href="#award"
                        role="tab"
                        aria-controls="award"
                        aria-selected="false"
                        >Modules</a
                      >
                    </li>

                 
                  </ul>
                </div>
              </div>
              <!-- Card Body -->
              <div class="card-body">
                <div class="tab-content" id="tabContent">
                  
                  <div
                    class="tab-pane fade  show active"
                    id="description"
                    role="tabpanel"
                    aria-labelledby="description-tab"
                  >
                    <!-- Description -->
                    <div class="mb-4">
                      <?php echo $programmeAimList[0]->aim;?>
                    </div>
                  </div>

                  <div
                    class="tab-pane fade"
                    id="objective"
                    role="tabpanel"
                    aria-labelledby="objective-tab"
                  >
                    <!-- Description -->
                     <div class="mb-4">
                       <div class="mb-4">
                      <?php echo $programmeStructureList[0]->structure;?>
                    </div>
                      </div>
                  </div>


                  <div
                    class="tab-pane fade"
                    id="award"
                    role="tabpanel"
                    aria-labelledby="award-tab"
                  >

                      <div class="row" id="tableDivId">
                

                  <?php for($i=0;$i<count($programmemodulesList);$i++) { ?>

                    <div class="col-lg-6 col-md-6 col-12">
                      <div class="card mb-4 card-hover">
                        <a href="/coursedetails/index/<?php echo $programmemodulesList[$i]->id;?>" class="card-img-top">
                               <img
                            src="<?php echo "/assets/images/".$programmemodulesList[$i]->image;?>"
                            alt
                            class="rounded-top card-img-top"
                            style='min-height: 138px;max-height: 138px;'
                          />
                      </a>
                      <div class="card-body">
                        <h4 class="mb-2 text-truncate-line-2" class="text-inherit">
                           <a href="/coursedetails/index/<?php echo $programmemodulesList[$i]->id;?>" class="text-inherit"
                    ><?php echo $programmemodulesList[$i]->name;?></a
                  >

                        
                        </h4>
                        <!--list-->
                          <ul class="mb-2 list-inline">
                          <li class="list-inline-item">
                            <i class="fe fe-clock mr-1"></i><?php echo $programmemodulesList[$i]->max_duration . " - " . $programmemodulesList[$i]->duration_type ?>
                          </li>
                          <li class="list-inline-item">
                            <i class="fe fe-bar-chart mr-1"></i>
                            <?php echo $programmemodulesList[$i]->studyname;?>

                          </li>
                        </ul>
                        <div class="1h-1">
                          <div class="d-flex">
                            <div class="h5">RM <?php echo number_format($programmemodulesList[$i]->amount,2);?> </div>
                             <div class="ml-auto">
                                          <i class="fe fe-book mr-1"></i>
                                          <?php echo $programmemodulesList[$i]->cattype;?>

                    </div>
                          </div>
                        </div>
                        

                        <div class="d-flex mt-2">
                           <a href="javascript:buynow(<?php echo $programmemodulesList[$i]->id;?>,<?php echo $programmemodulesList[$i]->amount;?>)" class="btn btn-outline-primary btn-sm">Buy Now</a>
                          <!-- <a href="#" class="btn btn-outline-info btn-sm ml-auto"
                            >Add to Cart</a
                          > -->
                        </div>
                      </div>
                      <!--footer card-->
                     <!--  <div class="card-footer">
                        <div class="row align-items-center no-gutters">
                          <div class="col-auto">
                            <?php if($programmemodulesList[$i]->staffimage!='') {?>
                            <img
                              src="website/img/avatar-1.jpg"
                              class="rounded-circle avatar-xs"
                            />
                          <?php } ?> 
                          </div>
                          <div class="col ml-2">
                            <span><?php echo $programmemodulesList[$i]->staffname;?></span>
                          </div>
                         
                        </div>
                      </div> -->
                    </div>
                  </div>
                <?php } ?>

                    
                   
                  </div>
                </div>

                  <!-- Tab pane -->
                  <div
                    class="tab-pane fade"
                    id="faq"
                    role="tabpanel"
                    aria-labelledby="faq-tab"
                    style='display: none;'
                  >
                    <!-- FAQ -->
                    <div>
                     
                       <div class="mb-4">
                      <?php echo $programmeCertificateList[0]->name;?>
                    </div>
                      
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-12 col-12 mt-lg-n22">
            <!-- Card -->
            <!-- <div class="card mb-3 mb-4">
              <div class="p-1">
                <div
                  class="d-flex justify-content-center position-relative rounded py-10 border-white border rounded-lg bg-cover"
                  style="background-image: url(/assets/images/<?php echo $programmeList->image;?>)"
                >
                 
                </div>
              </div>
              <h4 class="text-center mb-0 mt-3">RM  <?php echo $programmeList->amount;?> </h4>
              <div class="card-body">
                <a href="javascript:buynow(<?php echo $programmeList->id;?>,<?php echo $programmeList->amount;?>)" class="btn btn-primary btn-block"> Buy Now</a>
                <a href="javascript:buynow(<?php echo $programmeList->id;?>,<?php echo $programmeList->amount;?>)" class="btn btn-outline-primary btn-block"
                  >Add to Cart</a
                >
              </div>
            </div> -->
            <!-- Card -->
            <div class="card mb-4">
              <div>
                <!-- Card header -->
                <div class="card-header">
                  <h4 class="mb-0 h5">What’s included</h4>
                </div>
                <ul class="list-group list-group-flush">
                  <li class="list-group-item bg-transparent">
                    <i
                      class="fe fe-play-circle align-middle mr-2 text-primary"
                    ></i
                    ><?php echo count($programmemodulesList);?> Modules
                  </li>
                  <li class="list-group-item bg-transparent">
                    <i class="fe fe-award mr-2 align-middle text-success"></i
                    >Certificate
                  </li>
                  <li class="list-group-item bg-transparent border-bottom-0">
                    <i class="fe fe-clock align-middle mr-2 text-warning"></i
                    >Lifetime access
                  </li>
                </ul>
              </div>
            </div>
            <!-- Card -->
            <div class="card">
              <!-- Card body -->
               <div class="card-body instructor-details">
                <div class="d-flex align-items-center">
                  <div class="position-relative">

                    <img
                    src="/website/staff/<?php echo $staffDetails->image;?>"
                      alt=""
                      class="rounded-circle avatar-xl"
                    />
                  </div>
                  <div class="ml-4">
                    <h4 class="mb-0"><?php echo $staffDetails->name;?></h4>
                    <p class="mb-1 font-size-xs">
                      <?php echo $staffDetails->degree_details;?>
                    </p>
                    <span class="font-size-xs"
                      ><span class="text-warning">5.0 </span
                      ><img src="/website/img/star_icon.svg" alt="star" />Instructor
                      Rating</span
                    >
                  </div>
                </div>
                <div
                  class="border-top row mt-3 border-bottom mb-3 no-gutters stats"
                >
                  <div class="col">
                    <div class="pr-1 pl-2 py-3">
                      <h5 class="mb-0">11,604</h5>
                      <span>Students</span>
                    </div>
                  </div>
                  <div class="col border-left">
                    <div class="pr-1 pl-3 py-3">
                      <h5 class="mb-0">32</h5>
                      <span>Courses</span>
                    </div>
                  </div>
                  <div class="col border-left">
                    <div class="pr-1 pl-3 py-3">
                      <h5 class="mb-0">12,230</h5>
                      <span>Reviews</span>
                    </div>
                  </div>
                </div>
                <p>
                  Professor John Arul Phillips previously served the Faculty of Education, University of Malaya (UM) and Open University Malaysia (OUM) engaged in online distance teacher education. Currently, Professor Phillips is Dean, School and Cognitive Science, Asia e University (AeU) involved in preparing educators at the bachelors, masters and doctoral levels through online distance learning.
                </p>
                <!-- <a href="#" class="btn btn-outline-secondary btn-sm"
                  >View Details</a
                > -->
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- COURSE DETAILS STARTS HERE-->

    <!-- FEATURES WRAPPERS STARTS HERE-->
   <div class="bg-white py-4 shadow-sm features-wrapper">
      <div class="container">
        <div class="row align-items-center no-gutters">
          <!-- Features -->
          <div class="col-xl-4 col-lg-4 col-md-6 mb-lg-0 mb-4">
            <div class="d-flex align-items-center">
              <span
                class="icon-sahpe icon-lg bg-light-warning rounded-circle text-center text-dark-warning font-size-md"
              >
                <i class="fe fe-video"> </i
              ></span>
              <div class="ml-3">
                <h4 class="mb-0 font-weight-semi-bold">
                  Variety of topics
                </h4>
                <p class="mb-0">Enjoy a variety of fresh topics</p>
              </div>
            </div>
          </div>
          <!-- Features -->
          <div class="col-xl-4 col-lg-4 col-md-6 mb-lg-0 mb-4">
            <div class="d-flex align-items-center">
              <span
                class="icon-sahpe icon-lg bg-light-warning rounded-circle text-center text-dark-warning font-size-md"
              >
                <i class="fe fe-users"> </i
              ></span>
              <div class="ml-3">
                <h4 class="mb-0 font-weight-semi-bold">Expert instruction</h4>
                <p class="mb-0">Find the right instructor for you</p>
              </div>
            </div>
          </div>
          <!-- Features -->
          <div class="col-xl-4 col-lg-4 col-md-12">
            <div class="d-flex align-items-center">
              <span
                class="icon-sahpe icon-lg bg-light-warning rounded-circle text-center text-dark-warning font-size-md"
              >
                <i class="fe fe-clock"> </i
              ></span>
              <div class="ml-3">
                <h4 class="mb-0 font-weight-semi-bold">Flexible learning</h4>
                <p class="mb-0">Learn anytime anywhere</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- FEATURES WRAPPERS ENDS HERE-->

   

    <div class="footer">
      <div class="container">
        <div class="row align-items-center no-gutters border-top py-2">
          <!-- Desc -->
          <div class="col-md-6 col-12">
            <span>&copy; 2021 Speed. All Rights Reserved.</span>
          </div>
          <!-- Links -->
          <div class="col-12 col-md-6">
            <nav class="nav justify-content-center justify-content-md-end">
              <a class="nav-link active pl-0" href="#!">Privacy</a>
              <a class="nav-link" href="#!">Terms </a>
              <a class="nav-link" href="#!">Feedback</a>
              <a class="nav-link" href="#!">Support</a>
            </nav>
          </div>
        </div>
      </div>
    </div>
     <script
      src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
      integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns"
      crossorigin="anonymous"
    ></script>

    <script src="<?php echo BASE_PATH;?>website/js/jquery-1.12.4.min.js"></script>

 <script>

   function buynow(id,amount)
    {
      $.noConflict();

        jQuery.get("/coursedetails/tempbuynow/"+id+"/"+amount, function(data, status){
             console.log(data);
             parent.location= "<?php echo BASE_PATH;?>/index/checkout";
         });
    }

  </script>



