<div class="footer">

<div class="container">
        <div class="row no-gutters pt-5">
          <!-- Desc -->
          <div class="col-lg-3">
              <div class="footer-logo">
                  <a href="/index"><img src="/website/img/speed_logo.svg" class="img-responsive"/></a>
              </div>
              <ul class="footer-sm-nav">
                  <li><a href="https://www.facebook.com/SPEED-Training-Sdn-Bhd-103421931966004" class="facebook" target="_blank">Facebook</a></li>
                  <li><a href="https://www.instagram.com/speedtraining_sdnbhd/" class="twitter" target="_blank">Instagram</a></li>
                  <li><a href="#" class="linkedin">LinkedIn</a></li>
                  <li><a href="#" class="youtube">Youtube</a></li>
              </ul>
              <p>&copy; <?php echo date('Y');?> Speed. All Rights Reserved.</p>
          </div>
          <div class="col-lg-9">
        <div class="footer-menu-container">
          <div class="footer-menu">
            <h4>About myeduskills</h4>
            <ul>
              <li>
                <a href="/index/about">About Us</a>
              </li>
             <!--  <li>
                <a href="/index/partner">Our Partners</a>
              </li> -->
              <li>
                <a href="/index/becomeourpartner">Become Our Partner</a>
              </li>
            </ul>
          </div>
          <div class="footer-menu">
            <h4>Using myeduskills</h4>
            <ul>
              <li>
                <a href="/index/usingofplatform">Using of Platform</a>
              </li>
              <li>
                <a href="/index/learningguide">Learning Guides</a>
              </li>
              <li>
                <a href="/index/subscription">Subscription</a>
              </li>
              <li>
                <a href="/course/summary/1">Short Courses </a>
              </li>
              <li>
                <a href="/course/summary/2">Micro-Credentials</a>
              </li>
              <li>
                <a href="/course/summary/4">Professional Qualifications</a>
              </li>
            </ul>
          </div>
          <div class="footer-menu">
            <h4>Popular Subjects</h4>
            <ul>
              <li>
                <a href="/course/index/2">Business & Management</a>
              </li>
               <li>
                <a href="/course/index/3">Education & Teaching</a>
              </li>
              <li>
                <a href="/course/index/4">Information and Communication Technologies (ICT)</a>
              </li>
              <li>
                <a href="/course/index/7">Social Sciences and Humanities </a
                >
              </li>
           
            </ul>
          </div>
          <div class="footer-menu">
            <h4>Need any help?</h4>
            <ul>
              <li>
                <a href="/index/faq">FAQ's</a>
              </li>
              <li>
                <a href="/index/helpdesk">Contact Us</a>
              </li>
            </ul>
          </div>
          <div class="footer-menu">
            <h4>Small Print</h4>
            <ul>
              <li>
                <a href="/index/termsandcondition">Terms & Conditions</a>
              </li>
              <li>
                <a href="/index/privacypolicy">Disclaimer</a>
              </li>
              <li>
                <a href="/index/refundpolicy">Standard Terms for the purchase and use of services
</a>
              </li>
            </ul>
          </div>
        </div>
          </div>

        </div>
      </div>
    </div>