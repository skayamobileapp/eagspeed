<!DOCTYPE html>
<html>
<body>
  <div class="login-wrapper">
    <di class="container">
      <div class="login-container" > 
        <div class="text-center">
          <a href="/"><img src="<?php echo BASE_PATH; ?>assets/img/speed_logo.svg" /></a>     
        </div>
        <h3 class="login-title">Company User Reset Password</h3>
        <h4 class="login-title">Company User Email : <?php echo $email; ?> </h4>
        <div>
          <?php
          $this->load->helper('form');
          $error = $this->session->flashdata('error');
          if ($error) {
          ?>
            <div class="alert alert-danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              <?php echo $error; ?>
            </div>
          <?php }
          $success = $this->session->flashdata('success');
          $entered_url = $this->session->flashdata('entered_url');
          // print_r($success);exit();
    
          if ($success)
          {
          ?>
              <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $success; ?>
              </div>
          <?php
          }
          ?>
        </div>
        <div>
          <form id="form_reset_password" method="post">
            <!-- <div class="form-group">
              <label>Email Address</label>
              <input type="email" class="form-control" placeholder="Email" id="email" name="email" required value="<?php echo $email; ?>" readonly>
            </div> -->

            <div class="form-group">
              <label>Password</label>
              <input type="password" class="form-control" placeholder="Password" id="password" name="password" required>
            </div>

            <div class="form-group">
              <label>Confirm Password</label>
              <input type="text" class="form-control" placeholder="Confirm Password" id="confirm_password" name="confirm_password" required>
            </div>
            <button type="button" class="btn btn-primary btn-block" onclick="checkPasswords()">Reset password</button>
            <div class="login-links">
              <p>Have Password <a href="/companyUserLogin/companyUserLogin">Login Here</a></p>
            </div>
          </form>
        </div>
      </div>
    </di>
  </div>

  <script src="<?php echo BASE_PATH; ?>assets/js/jquery-1.12.4.min.js"></script>
  <script src="<?php echo BASE_PATH; ?>assets/js/bootstrap.min.js"></script>
</body>

</html>

<script>
  
  function checkPasswords()
  {
      var password = $("#password").val();
      var confirm_password = $("#confirm_password").val();

      if(password == confirm_password)
      {
        $('#form_reset_password').submit();
      }
      else
      {
        alert("Entered Passwords Doesn't Match");
        window.location.reload();
      }
  }

</script>