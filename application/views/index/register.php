              <!--PAGE HEADER ENDS HERE-->
                 <form action="" method="POST" id="contactUsForm">


    <div class="pt-5">
      <div class="container">
        <div class="row">

          <div class="col-lg-4  ">
              </div>

          <div class="col-lg-4 mb-5">
            <div class="card mb-3">
              <div class="card-body p-6">
             
              <!-- Form -->
                <div id='loginForm'>

                 <div class="mb-4">
                <h3 class="mb-1 font-weight-bold">Sign in</h3>
                
              </div>

                <!-- Username -->
                <div class="form-group">
                  <label for="email" class="form-label">Username or email</label>
                  <input type="email" id="email" class="form-control" name="email" placeholder="Email address here">
                </div>
                <!-- Password -->
                <div class="form-group">
                  <label for="password" class="form-label">Password</label>
                  <input type="password" id="password" class="form-control" name="password" placeholder="**************">
                </div>

                  <button type="submit" class="btn btn-primary btn-block">
                    Login to Checkout
                  </button>
         

                  <div class="text-center mt-2">Don’t have an account?
                  <a href="JavaScript:;" class="ml-1 text-nowrap" onclick="showregisterform()">Click to Register</a>
                  </div>
              
                  <!-- Button -->
                
                </div>

                <div id='registerForm' style="display: none;">
                  <div class="mb-4">
                <h3 class="mb-1 font-weight-bold">Register</h3>
                
              </div>

                <!-- Username -->
                <div class="form-group">
                  <label  class="form-label">Full Name</label>
                  <input type="text" id="full_name" class="form-control" name="full_name" placeholder="Full Name" >
                </div>
                <div class="form-group">
                  <label class="form-label">Email</label>
                  <input type="text" id="user_email" class="form-control" name="user_email" placeholder="Email address here" >
                </div>
              
                <!-- Password -->
                <div class="form-group">
                  <label for="password" class="form-label">Password</label>
                  <input type="password" id="confirm_password" class="form-control" name="confirm_password" placeholder="**************" >
                </div>

                  <button type="submit" class="btn btn-primary btn-block">
                    Register & Checkout
                  </button>
         

                  <div class="text-center mt-2">Existing User?
                  <a href="JavaScript:;" class="ml-1 text-nowrap" onclick="showLoginform()">Click to Login</a>
                  </div>
              
                  <!-- Button -->
                
                </div>

            </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </form>

   <!--CHECKOUT ENDS HERE-->
    <?php $this->load->view('../includes/newfooter');?>

    <script
      src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
      integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns"
      crossorigin="anonymous"
    ></script>

    <script src="/website/js/jquery-1.12.4.min.js"></script>
    <script src="/website/js/datamain.js"></script>


   

  
 <script>

 function cart(){

        jQuery.get("/index/cart", function(data, status){
             $("#tableDivID").html(data);
         });

    }

  function showtextbox(id){
    $("#staffidDiv").hide();
      if(id!=3) {
       $("#staffidDiv").show();
      } 
    }

  function validatenric(value) {
          $("#congPId").hide();
$("#sorryPId").hide();

    $("#discount_code").val('');
    var stafforstudent = 'Staff';//$("#staff_student").val();
     jQuery.get("/index/checkdiscount/"+stafforstudent+"/"+value, function(data, status){

           if(data>0) {
            $("#congPId").show();
           } else { 
            $("#sorryPId").show();
           }
          cart();
         });
  }

   function deletecourse(id)
    {





       var cnf= confirm('Do you really want to delete from cart?');
       if(cnf==true) {
        jQuery.get("/coursedetails/deletecourses/"+id, function(data, status){

          if(data=='0') {
            alert('Your cart is empty,Please continue shopping');
            parent.location = '/';

          } else {
             parent.location= "<?php echo BASE_PATH;?>/index/checkout";
           }
         });
      }
    }


    function showdiscount() {
      $("#congPId").hide();
$("#sorryPId").hide();

      var discount_code = $("#discount_code").val();
      jQuery.get("/index/checkcouponcode/"+discount_code, function(data, status){
           console.log(data);
           if(data>0) {
            $("#congPId").show();
           } else {
            $("#sorryPId").show();
           }
          cart();
         });
    }

  </script>





<script type="text/javascript">
  function showregisterform() {
    $("#loginForm").hide();
    $("#registerForm").show();
  }
   function showLoginform() {
    $("#loginForm").show();
    $("#registerForm").hide();
  }
</script>

<script type='text/JavaScript'>
$(document).ready(function() {
  cart();
 $("#contactUsForm").validate({
                // Specify the validation rules
                rules: {
                    email: "required",  
                    password:"required",
                    full_name:"required",
                    user_email:"required",
                    nric:"required",
                    confirm_password:"required"
                    
                },
                // Specify the validation error messages
                messages: {
                    email: "<span>Please enter a Email</span>",
                    password: "<span>Please enter a Password</span>",
                    full_name: "<span>Please enter a Full Name</span>",
                    user_email: "<span>Please enter a Email</span>",
                    nric: "<span>Please enter a NRIC</span>",
                    confirm_password: "<span>Please enter a Password</span>",
                    
                }
            });
           
 });
  </script>

  </body>
</html>