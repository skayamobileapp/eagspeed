
<style>
#botmanWidgetRoot{
    padding-top: 120px;
}
</style>
    <div class="callout-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <a href="#">
                        <div>
                            <h4>Short Courses</h4>
                            <p>Choose short courses to upskill, reskill and unlock your potential. Accumulate relevant courses and get Professional Certificate. </p>
                        </div>
                        <img src="/website/img/short_course_icon.svg" />
                    </a>
                </div>
                <div class="col-md-4">
                    <a href="#">
                        <div>
                            <h4>Microcredential</h4>
                            <p>A micro-credential is a recognition of achievement in a specific area of study by identifying a competency and submiting evidence that you mastered the competency. </p>
                        </div>
                        <img src="/website/img/microcredentials_course_icon.svg" />
                    </a>
                </div> 
                <div class="col-md-4">
                    <a href="#">
                        <div>
                            <h4>Expert Track</h4>
                            <p>Expert Tracks will help you upskill and reskill in a specialised area alongside with the top universities and industry leaders.</p>
                        </div>
                        <img src="/website/img/expert_track_course_icon.svg" />
                    </a>
                </div>                  
            </div>
        </div>
    </div>
    <!-- BANNER START HERE -->
    <div class="banner-wrapper d-flex align-items-center">
      <img src="/website/img/banner_img.jpg" class="hero-img" />
      <div class="container">
        <!-- Hero Section -->
        <div class="py-5">
          <h1 class="text-white font-weight-bold">
            Transform Yourself
          </h1>
          <p class="text-white mb-5 lead">
            Learn, Unlearn, Upskill and Reskill to Unlock Your Potential
          </p>
          <a href="/course/index" class="btn btn-primary btn-lg">Get Started</a>
        </div>
      </div>
    </div>
    <!-- BANNER ENDS HERE -->

    <!-- FEATURES WRAPPERS STARTS HERE-->
    <div class="bg-brand-secondary py-4 shadow-sm features-wrapper">
      <div class="container">
        <div class="row align-items-center no-gutters">
          <div class="col-lg-10 offset-lg-1">
            <h3 class="text-center">Take the next step towards your personal and professional goals with SPEED.</h3>
          </div>
        </div>
      </div>
    </div>
    <!-- FEATURES WRAPPERS ENDS HERE-->

    <!--content-->
    <div class="pt-lg-12 pb-lg-3 pt-8 pb-6 course-card">
      <div class="container">
        <div class="row d-lg-flex align-items-center mb-4">
          <div class="col">
            <h2 class="mb-0">Short Courses</h2>
          </div>
          <div class="col-auto">
            <a href="/course/index" class="btn btn-outline-primary btn-sm">View All</a>
          </div>
        </div>
        <div class="row">
          <?php 

            for($i=0;$i<2;$i++) { 
          



            ?>


          <div class="col-lg-3 col-md-6 col-12 lightblue-card">
            <div class="card mb-4 card-hover">
              <a href="/programdetails/index/<?php echo $popularprogrammeList[$i]->id;?>" class="card-img-top">
                     <img
                  src="<?php echo "/assets/images/".$popularprogrammeList[$i]->image;?>"
                  alt
                  class="rounded-top card-img-top"
                />
              </a>
              <div class="card-body">
                <h4 class="mb-2 text-truncate-line-2" class="text-inherit">
                  <a href="/programdetails/index/<?php echo $popularprogrammeList[$i]->id;?>" class="text-inherit"
                    ><?php echo $popularprogrammeList[$i]->name;?></a
                  >
                </h4>
                <!--list-->
                  <ul class="mb-2 list-inline">
                  <li class="list-inline-item">
                    <i class="fe fe-clock mr-1"></i><?php echo $popularprogrammeList[$i]->max_duration . " - " . $popularprogrammeList[$i]->duration_type ?>
                  </li>
                  <li class="list-inline-item">
                    <i class="fe fe-bar-chart mr-1"></i>Advance
                  </li>
                </ul>
             <div class="1h-1 amount-block">
                  <!--   
                  <div class="d-flex">
                    <div class="h5">RM <?php echo $popularprogrammeList[$i]->amount;?></div>
                    <div class="ml-auto">
                      <span class="rating-star mr-1">
                        <img src="/website/img/star_icon.svg" alt="star" />
                      </span>
                      <span class="text-warning">4.5</span>
                      <span class="font-size-xs text-muted">(7,700)</span>
                    </div>
                  </div>-->
                </div> 
                

                <div class="d-flex mt-2">
                  <a href="/programdetails/index/<?php echo $popularprogrammeList[$i]->id;?>" class="btn btn-outline-primary btn-sm">View</a>
                  <!-- <a href="javascript:buynow(<?php echo $popularprogrammeList[$i]->id;?>,<?php echo $popularprogrammeList[$i]->amount;?>)" class="btn btn-outline-info btn-sm ml-auto"
                    >Add to Cart</a
                  > -->
                </div>
              </div>
              <!--footer card-->
              <div class="card-footer">
                <div class="row align-items-center no-gutters">
                  <div class="col-auto">
                    <?php if($popularprogrammeList[$i]->staffimage!='') {?>
                    <img
                      src="/website/staff/<?php echo $popularprogrammeList[$i]->staffimage;?>"
                      class="rounded-circle avatar-xs"
                    />
                  <?php } ?> 
                  </div>
                  <div class="col ml-2">
                    <span><?php echo $popularprogrammeList[$i]->staffname;?></span>
                  </div>
                 
                </div>
              </div>
            </div>
          </div>

                  <?php } ?> 


                  <?php 
          // print_r($programmeList);exit;

                    for($i=0;$i<2;$i++) { 
          



            ?>


          <div class="col-lg-3 col-md-6 col-12">
            <div class="card mb-4 card-hover">
              <a href="/coursedetails/index/<?php echo $recomendedprogrammeList[$i]->id;?>" class="card-img-top">
                     <img
                  src="<?php echo "/assets/images/".$recomendedprogrammeList[$i]->image;?>"
                  alt
                  class="rounded-top card-img-top"
                />
              </a>
              <div class="card-body">
                <h4 class="mb-2 text-truncate-line-2" class="text-inherit">
                  <a href="/coursedetails/index/<?php echo $recomendedprogrammeList[$i]->id;?>" class="text-inherit"
                    ><?php echo $recomendedprogrammeList[$i]->name;?></a
                  >
                </h4>
                <!--list-->
                  <ul class="mb-2 list-inline">
                  <li class="list-inline-item">
                    <i class="fe fe-clock mr-1"></i><?php echo $recomendedprogrammeList[$i]->max_duration . " - " . $recomendedprogrammeList[$i]->duration_type ?>
                  </li>
                  <li class="list-inline-item">
                    <i class="fe fe-bar-chart mr-1"></i>Advance
                  </li>
                </ul>
                <div class="1h-1">
                  <div class="d-flex">
                    <div class="h5">RM <?php echo $recomendedprogrammeList[$i]->amount;?></div>
                    <div class="ml-auto">
                                          <i class="fe fe-book mr-1"></i>
                                          <?php echo $recomendedprogrammeList[$i]->cattype;?>

                    </div>
                  </div>
                </div>
                

                <div class="d-flex mt-2">
                  <a href="/coursedetails/index/<?php echo $recomendedprogrammeList[$i]->id;?>" class="btn btn-outline-primary btn-sm">More Info</a>
                 <!--  <a href="#" class="btn btn-outline-info btn-sm ml-auto"
                    >Add to Cart</a
                  > -->
                </div>
              </div>
              <!--footer card-->
              <div class="card-footer">
                <div class="row align-items-center no-gutters">
                  <div class="col-auto">
                    <?php if($recomendedprogrammeList[$i]->staffimage!='') {?>
                    <img
                      src="/website/staff/<?php echo $recomendedprogrammeList[$i]->staffimage;?>"
                      class="rounded-circle avatar-xs"
                    />
                  <?php } ?> 
                  </div>
                  <div class="col ml-2">
                    <span><?php echo $recomendedprogrammeList[$i]->staffname;?></span>
                  </div>
                 
                </div>
              </div>
            </div>
          </div>

                  <?php } ?> 



        </div>
      </div>
    </div>

  <!--   <div class="pt-lg-12 pb-lg-3 pt-5">
      <div class="container">
        <h2 class="h4 mb-4">Top Categories</h2>
        <div class="row align-items-center">
          <div class="col-md-4">
            <a href="#" class="card category-card mb-4">
              <img src="/website/img/category_1.jpg" class="img-fluid" alt="..." />
              <span class="category-title">Management</span>
            </a>
          </div>
          <div class="col-md-4">
            <a href="#" class="card category-card mb-4">
              <img src="/website/img/category_4.jpg" class="img-fluid" alt="..." />
              <span class="category-title">Business</span>
            </a>
          </div>
          <div class="col-md-4">
            <a href="/course/index/2" class="card category-card mb-4">
              <img src="/website/img/category_3.jpg" class="img-fluid" alt="..." />
              <span class="category-title">Education & Teaching</span>
            </a>
          </div>
          <div class="col-md-4">
            <a href="#" class="card category-card mb-4">
              <img src="/website/img/category_2.jpg" class="img-fluid" alt="..." />
              <span class="category-title">Digital Technology</span>
            </a>
          </div>
          <div class="col-md-4">
            <a href="#" class="card category-card mb-4">
              <img src="/website/img/category_1.jpg" class="img-fluid" alt="..." />
              <span class="category-title">Personal Development</span>
            </a>
          </div>
          <div class="col-md-4">
            <a href="#" class="card category-card mb-4">
              <img src="/website/img/category_4.jpg" class="img-fluid" alt="..." />
              <span class="category-title">Health & Well-Being</span>
            </a>
          </div>          
        </div>
      </div>
    </div> -->

    <div class="pb-lg-3 pt-lg-3 course-card">
      <div class="container">
        <div class="row d-lg-flex align-items-center mb-4">
          <div class="col">
            <h2 class="mb-0">Microcredentials</h2>
          </div>
          <div class="col-auto">
            <a href="/course/index" class="btn btn-outline-primary btn-sm">View All</a>
          </div>
        </div>
        <div class="row">
          <?php 
          // print_r($programmeList);exit;

                    for($i=0;$i<4;$i++) { 
          



            ?>


          <div class="col-lg-3 col-md-6 col-12">
            <div class="card mb-4 card-hover">
              <a href="/coursedetails/index/<?php echo $recomendedprogrammeList[$i]->id;?>" class="card-img-top">
                     <img
                  src="<?php echo "/assets/images/".$recomendedprogrammeList[$i]->image;?>"
                  alt
                  class="rounded-top card-img-top"
                />
              </a>
              <div class="card-body">
                <h4 class="mb-2 text-truncate-line-2" class="text-inherit">
                  <a href="/coursedetails/index/<?php echo $recomendedprogrammeList[$i]->id;?>" class="text-inherit"
                    ><?php echo $recomendedprogrammeList[$i]->name;?></a
                  >
                </h4>
                <!--list-->
                  <ul class="mb-2 list-inline">
                  <li class="list-inline-item">
                    <i class="fe fe-clock mr-1"></i><?php echo $recomendedprogrammeList[$i]->max_duration . " - " . $recomendedprogrammeList[$i]->duration_type ?>
                  </li>
                  <li class="list-inline-item">
                    <i class="fe fe-bar-chart mr-1"></i>Advance
                  </li>
                </ul>
                <div class="1h-1">
                  <div class="d-flex">
                    <div class="h5">RM <?php echo $recomendedprogrammeList[$i]->amount;?></div>
                     <div class="ml-auto">
                                          <i class="fe fe-book mr-1"></i>
                                          <?php echo $recomendedprogrammeList[$i]->cattype;?>

                    </div>
                  </div>
                </div>
                

                <div class="d-flex mt-2">
                  <a href="/coursedetails/index/<?php echo $recomendedprogrammeList[$i]->id;?>" class="btn btn-outline-primary btn-sm">More Info</a>
                 <!--  <a href="#" class="btn btn-outline-info btn-sm ml-auto"
                    >Add to Cart</a
                  > -->
                </div>
              </div>
              <!--footer card-->
              <div class="card-footer">
                <div class="row align-items-center no-gutters">
                  <div class="col-auto">
                    <?php if($recomendedprogrammeList[$i]->staffimage!='') {?>
                    <img
                      src="/website/staff/<?php echo $recomendedprogrammeList[$i]->staffimage;?>"
                      class="rounded-circle avatar-xs"
                    />
                  <?php } ?> 
                  </div>
                  <div class="col ml-2">
                    <span><?php echo $recomendedprogrammeList[$i]->staffname;?></span>
                  </div>
                 
                </div>
              </div>
            </div>
          </div>

                  <?php } ?> 


        </div>
      </div>
    </div>
    <div class="pb-lg-3 pt-lg-3 course-card">
      <div class="container">
        <div class="row d-lg-flex align-items-center mb-4">
          <div class="col">
            <h2 class="mb-0">Expert Track</h2>
          </div>
          <div class="col-auto">
            <a href="/course/index" class="btn btn-outline-primary btn-sm">View All</a>
          </div>
        </div>
        <div class="row">
          <?php 
          // print_r($programmeList);exit;

                    for($i=0;$i<4;$i++) { 
          



            ?>


          <div class="col-lg-3 col-md-6 col-12">
            <div class="card mb-4 card-hover">
              <a href="/coursedetails/index/<?php echo $recomendedprogrammeList[$i]->id;?>" class="card-img-top">
                     <img
                  src="<?php echo "/assets/images/".$recomendedprogrammeList[$i]->image;?>"
                  alt
                  class="rounded-top card-img-top"
                />
              </a>
              <div class="card-body">
                <h4 class="mb-2 text-truncate-line-2" class="text-inherit">
                  <a href="/coursedetails/index/<?php echo $recomendedprogrammeList[$i]->id;?>" class="text-inherit"
                    ><?php echo $recomendedprogrammeList[$i]->name;?></a
                  >
                </h4>
                <!--list-->
                  <ul class="mb-2 list-inline">
                  <li class="list-inline-item">
                    <i class="fe fe-clock mr-1"></i><?php echo $recomendedprogrammeList[$i]->max_duration . " - " . $recomendedprogrammeList[$i]->duration_type ?>
                  </li>
                  <li class="list-inline-item">
                    <i class="fe fe-bar-chart mr-1"></i>Advance
                  </li>
                </ul>
                <div class="1h-1">
                  <div class="d-flex">
                    <div class="h5">RM <?php echo $recomendedprogrammeList[$i]->amount;?></div>
                     <div class="ml-auto">
                                          <i class="fe fe-book mr-1"></i>
                                          <?php echo $recomendedprogrammeList[$i]->cattype;?>

                    </div>
                  </div>
                </div>
                

                <div class="d-flex mt-2">
                  <a href="/coursedetails/index/<?php echo $recomendedprogrammeList[$i]->id;?>" class="btn btn-outline-primary btn-sm">More Info</a>
                 <!--  <a href="#" class="btn btn-outline-info btn-sm ml-auto"
                    >Add to Cart</a
                  > -->
                </div>
              </div>
              <!--footer card-->
              <div class="card-footer">
                <div class="row align-items-center no-gutters">
                  <div class="col-auto">
                    <?php if($recomendedprogrammeList[$i]->staffimage!='') {?>
                    <img
                      src="/website/staff/<?php echo $recomendedprogrammeList[$i]->staffimage;?>"
                      class="rounded-circle avatar-xs"
                    />
                  <?php } ?> 
                  </div>
                  <div class="col ml-2">
                    <span><?php echo $recomendedprogrammeList[$i]->staffname;?></span>
                  </div>
                 
                </div>
              </div>
            </div>
          </div>

                  <?php } ?> 


        </div>
      </div>
    </div>    


      <div class="pb-lg-3 pt-lg-3 course-card">
      <div class="container">
        <div class="row d-lg-flex align-items-center mb-4">
          <div class="col">
            <h2 class="mb-0">Browse for Free</h2>
          </div>
          <div class="col-auto">
            <a href="/course/index" class="btn btn-outline-primary btn-sm">View All</a>
          </div>
        </div>
        <div class="row">
          <?php 
          // print_r($programmeList);exit;

                    for($i=0;$i<4;$i++) { 
          



            ?>


          <div class="col-lg-3 col-md-6 col-12">
            <div class="card mb-4 card-hover">
              <a href="/coursedetails/index/<?php echo $latestprogrammeList[$i]->id;?>" class="card-img-top">
                     <img
                  src="<?php echo "/assets/images/".$latestprogrammeList[$i]->image;?>"
                  alt
                  class="rounded-top card-img-top"
                />
              </a>
              <div class="card-body">
                <h4 class="mb-2 text-truncate-line-2" class="text-inherit">
                  <a href="/coursedetails/index/<?php echo $latestprogrammeList[$i]->id;?>" class="text-inherit"
                    ><?php echo $latestprogrammeList[$i]->name;?></a
                  >
                </h4>
                <!--list-->
                  <ul class="mb-2 list-inline">
                  <li class="list-inline-item">
                    <i class="fe fe-clock mr-1"></i><?php echo $latestprogrammeList[$i]->max_duration . " - " . $latestprogrammeList[$i]->duration_type ?>
                  </li>
                  <li class="list-inline-item">
                    <i class="fe fe-bar-chart mr-1"></i>Advance
                  </li>
                </ul>
                <div class="1h-1">
                  <div class="d-flex">
                    <div class="h5"></div>
                    <div class="ml-auto">
                                          <i class="fe fe-book mr-1"></i>
                                          <?php echo $recomendedprogrammeList[$i]->cattype;?>

                    </div>
                  </div>
                </div>
                

                <div class="d-flex mt-2">
                  <a href="/coursedetails/index/<?php echo $latestprogrammeList[$i]->id;?>" class="btn btn-outline-primary btn-sm">More Info</a>
                 <!--  <a href="#" class="btn btn-outline-info btn-sm ml-auto"
                    >Add to Cart</a
                  > -->
                </div>
              </div>
              <!--footer card-->
              <div class="card-footer">
                <div class="row align-items-center no-gutters">
                  <div class="col-auto">
                    <?php if($latestprogrammeList[$i]->staffimage!='') {?>
                    <img
                      src="/website/staff/<?php echo $latestprogrammeList[$i]->staffimage;?>"
                      class="rounded-circle avatar-xs"
                    />
                  <?php } ?> 
                  </div>
                  <div class="col ml-2">
                    <span><?php echo $latestprogrammeList[$i]->staffname;?></span>
                  </div>
                 
                </div>
              </div>
            </div>
          </div>

                  <?php } ?> 


        </div>
      </div>
    </div>



    <!-- TESTIMONIAL STARTS HERE -->
    <div class="testimonial-wrapper mx-lg-10 text-center">
      <div class="container">
        <h1 class="text-white pt-5 pb-4">Testimonials</h1>
        <div
          id="testimonialCarousel"
          class="carousel slide"
          data-ride="carousel"
        >
          <ol class="carousel-indicators">
            <li
              data-target="#testimonialCarousel"
              data-slide-to="0"
              class="active"
            ></li>
            <li data-target="#testimonialCarousel" data-slide-to="1"></li>
            <li data-target="#testimonialCarousel" data-slide-to="2"></li>
          </ol>
          <div class="carousel-inner pb-5">
            <div class="carousel-item active">
              <div class="w-lg-75 mx-lg-auto mb-5">
                <blockquote class="h4 text-white font-weight-normal">
                  "This platform and courses offered was what I needed to push my career into the next level. Everything was really comprehensible and  full of useful content"
                </blockquote>
                <div class="w-lg-50 mx-lg-auto mt-5">
                  <div class="avatar avatar-circle mb-3">
                    <img
                      class="avatar-img"
                      src="/website/testimonial/Syahirah.png"
                      alt="Image Description"
                    />
                  </div>
                  <h4 class="text-white mb-0">Syahirah</h4>
                </div>
              </div>
            </div>
            <div class="carousel-item">
              <div class="w-lg-75 mx-lg-auto mb-5">
                <blockquote class="h4 text-white font-weight-normal">
                  "I am improving my professional and personal development. Thanks for all who made it available online. It’s very excellent"
                </blockquote>
                <div class="w-lg-50 mx-lg-auto mt-5">
                  <div class="avatar avatar-circle mb-3">
                    <img
                      class="avatar-img"
                      src="/website/testimonial/nazmi.jpg"
                      alt="Image Description"
                    />
                  </div>
                  <h4 class="text-white mb-0">Nazmi Nabil</h4>
                </div>
              </div>
            </div>
            <div class="carousel-item">
              <div class="w-lg-75 mx-lg-auto mb-5">
                <blockquote class="h4 text-white font-weight-normal">
                  "This platform is highly practical. Highly recommend - the course provides a diverse range of topics, straightforward and clear"
                </blockquote>
                <div class="w-lg-50 mx-lg-auto mt-5">
                  <div class="avatar avatar-circle mb-3">
                    <img
                      class="avatar-img"
                      src="/website/testimonial/adib.jpg"
                      alt="Image Description"
                    />
                  </div>
                  <h4 class="text-white mb-0">Adib Khusyairi</h4>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- TESTIMONIAL ENDS HERE -->

    <!-- OUR PARTNERS STARTS HERE -->
    <div class="our-partners text-center bg-white mx-lg-10">
      <h1 class="pt-5">Our Partners</h1>
      <div id="ourPartnersCarousel" class="carousel slide" data-ride="carousel">
        <!-- <ol class="carousel-indicators">
          <li
            data-target="#ourPartnersCarousel"
            data-slide-to="0"
            class="active"
          ></li>
          <li data-target="#ourPartnersCarousel" data-slide-to="1"></li>
          <li data-target="#ourPartnersCarousel" data-slide-to="2"></li>
        </ol> -->
        <div class="carousel-inner py-3">
          <div class="carousel-item active">
            <div class="d-flex justify-content-between align-items-center text-center px-5">
                <img src="/website/img/itrain_logo.png" class="img-fluid" />
                <img src="/website/img/mef_academy_logo.jpg" class="img-fluid" />
                <img src="/website/img/zurich_logo.jpg" class="img-fluid" />
                <img src="/website/img/pikom_logo.jpg" class="img-fluid" />
                <img src="/website/img/ewaa_logo.png" class="img-fluid" />
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- OUR PARTNERS STARTS HERE -->

    <div class="footer">
      <div class="container">
        <div class="row no-gutters pt-5">
          <!-- Desc -->
          <div class="col-md-3">
              <div class="footer-logo">
                  <a href="#"><img src="/website/img/speed_logo.svg" class="img-responsive"/></a>
              </div>
              <ul class="footer-sm-nav">
                  <li><a href="#" class="facebook">Facebook</a></li>
                  <li><a href="#" class="twitter">Twitter</a></li>
                  <li><a href="#" class="linkedin">LinkedIn</a></li>
                  <li><a href="#" class="youtube">Youtube</a></li>
              </ul>
              <p>&copy; 2021 Speed. All Rights Reserved.</p>
          </div>
          <div class="col-md-9">
              <div class="row">
                  <div class="col-md-3">
                      <ul class="nav">
                        <li><a class="nav-link" href="#">Short Courses</a></li>
                        <li><a class="nav-link" href="#">Microcredential</a></li>
                        <li><a class="nav-link" href="#">Expert Track</a></li>
                      </ul>
                  </div>
                  <div class="col-md-3">
                      <ul class="nav">
                        <li><a class="nav-link" href="#">Partner</a></li>
                        <li><a class="nav-link" href="#">Corporate Client</a></li>
                        <li><a class="nav-link" href="#">Free Signup</a></li>
                      </ul>
                  </div>
                  <div class="col-md-3">
                      <ul class="nav">
                        <li><a class="nav-link" href="#">About Us</a></li>
                        <li><a class="nav-link" href="/index/terms">Terms of Use</a></li>
                        <li><a class="nav-link" href="/index/refund">Refund Policy</a></li>
                      </ul>
                  </div>
                  <div class="col-md-3">
                      <ul class="app-nav">
                        <li><a href="#"><img src="/website/img/google_app_btn.png" class="img-responsive"/></a></li>
                        <li><a href="#"><img src="/website/img/apple_app_btn.png" class="img-responsive"/></a></li>
                      </ul>
                  </div>                  
              </div>
          </div>

        </div>
      </div>
    </div>


<div class="modal fade" id="signupModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Sign up</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <label class="col-form-label">Full Name</label>
            <input type="text" name="full_name" id="full_name" class="form-control">
          </div>
          <div class="form-group">
            <label class="col-form-label">Email</label>
            <input type="text" name="user_email" id="user_email" class="form-control">
          </div>   
           <div class="form-group">
            <label class="col-form-label">NRIC</label>
            <input type="text" name="nric" id="nric" class="form-control">
          </div>   

          <div class="form-group">
            <label class="col-form-label">Password</label>
            <input type="password" name="confirm_password" id="confirm_password" class="form-control">
          </div>   
          <button type="button" class="btn btn-primary" onclick="validateUser()">Join for Free</button>             
        </form>
      </div>
    </div>
  </div>
</div>   

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script
      src="//code.jquery.com/jquery-3.5.1.slim.min.js"
      integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
      crossorigin="anonymous"
    ></script>
    <script
      src="//cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
      integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns"
      crossorigin="anonymous"
    ></script>
    <script src="<?php echo BASE_PATH;?>website/js/jquery-1.12.4.min.js"></script>

     <script>

   function buynow(id,amount)
    {
      $.noConflict();

        jQuery.get("/coursedetails/tempbuynow/"+id+"/"+amount, function(data, status){
             console.log(data);
             parent.location= "<?php echo BASE_PATH;?>/index/checkout";
         });
    }

  </script>

  <script type="text/javascript">
    function validateUser() {

        $.post("/register/datainsert",
        {
          full_name: $("#full_name").val(),
          user_email: $("#user_email").val(),
          nric: $("#nric").val(),
          confirm_password: $("#confirm_password").val()
        },
        function(data, status){
          parent.location='/profile/dashboard/index';
        });

    }
  </script>
  
  
  <script>
// Feel free to change the settings on your need
    var botmanWidget = {
        frameEndpoint: 'https://chat.camsedu.com/chat.html',
        chatServer: 'https://chat.camsedu.com/chat.php',
         title: 'SPEED',
        introMessage: 'Welcome to SPEED<br/><br/>Hai, I am Sherry. <br><br/>Enter your name',
        placeholderText: 'Say something...',
        mainColor: '#1f3249',
        headerTextColor: '#fff',
        bubbleBackground: '#fff',
        aboutText: '',
        bubbleAvatarUrl: 'http://chat.camsedu.com/assets/portrait-asian-beautiful-smiling-woman-260nw-1214274322.jpg'
    };
        </script>
        <script src='https://chat.camsedu.com/js/widget.js'></script>
</html>


  </body>
</html>

  