   <!--PAGE HEADER STARTS HERE-->

    <div class="course-lists">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-xl-12 col-lg-12 col-md-12 col-12">
            <div>
              <h1 class="mb-0 text-white display-4">About Us</h1>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!--PAGE HEADER ENDS HERE-->

    <!--LIST PAGE  STARTS HERE-->

    <div class="filter-wrapper py-2">
      <div class="container" >
         <div class="row" >
          <div class="col-sm-12" style="text-align: justify;">
            <p><b>SCHOOL OF PROFESSIONAL, EXECUTIVE EDUCATION AND DEVELOPMENT (SPEED)</b><br/> The <b>School of Professional, Executive Education and Development (SPEED)</b> in collaboration with Asia e University (AeU) was established with the goal to upskill, equipped with the latest knowledge for the 21st century executives. <b>SPEED</b> offers these training courses in the form of short courses, micro credentials as well as Pofessional in  various industries with the purpose to spur their career pogression. <b>SPEED</b>  also provides courses in a variety of fields for self-improvement and general interests. In addition <b>SPEED</b>  also  can tailor customised courses for  continuing professional development and specific competencies development. These tailor-made professional courses may be delivered by multi-national, multi-cultural faculty, through SPEED’s strategic alliances with local and international training partners.

</p>
          </div>
        </div>

         <div class="row">
          <div class="col-sm-12" style="text-align: justify;">
            <p><a href="https://aeu.edu.my/discover/the-university/" target="_blank"><b>Asia e University (AeU)</b></a><br/>Asia e University (AeU) is a unique collaborative multinational university initiated by the Asia Cooperation Dialogue (ACD), a body established in 2002 to promote Asian cooperation at a continental level.</p>
            <p>The University is established as a Malaysian initiative and prime mover to champion e-Education, affirmed at the ACD Foreign Ministerial Meetings in Islamabad (2005) and Doha (2006) and supported by 35 ACD member countries. AeU  collaborates with Institutions of Higher Learning (IHLs) and Training partners  in the ACD member countries to offer quality and affordable academic degree and professional training programmes.

</p>
            <p>As a spearheading institution of choice in the new wave digital learning, AeU is the major catalyst in promoting the Asia wide networks for cooperation in digital education today.
</p>
            <p>AeU acts as an facilitator and enabler for IHLs in Asia to collaborate on the development of academic degree and professional programmes thus leveraging on the resources and facilities of the partners
</p>
          </div>
        </div>

         <div class="row">
          <div class="col-sm-12" style="text-align: justify;">
            <p><b>LEADERSHIP SPEED ADVISORY COUNCIL</b> <br/>
              The myeduskills is a digital learning platform developed by the <b>School of Professional, Executive Education and Development (SPEED)</b> in collaboration with Asia e University (AeU) that offers anywhere, anytime access to online courses from <b>SPEED</b>  and its programme partners, including universities, institutions, corporations and industry partners. The short courses, micro credentials and professional courses on this learning platform are instructor-led or self-paced or a combination of both and aligned to close the gaps in various areas such as business, management, communication, education, training, digital technologies, IR 4.0, social sciences, humanities, religion and other critical areas.</p>
          </div>
        </div>

        


    <hr class="mt-5 mb-3" />
    <div class="row">
      <div class="col-md-4 offset-md-4">
        <h4 class="text-center"><b>Chairman</b></h4>
        <div class="team-card">
          <div class="img-container">
            <img src="/website/img/1.png" />
          </div>
          
          <h5>ADJUNCT PROFESSOR DATO’ DR. TAN YEW CHONG</h5>
          <p>D.S.A.P. (Phg), P.S.K. (Kel), A.M.P. (Phg)</p>
        </div>
      </div>
    </div>
    <h4 class="text-center pt-5"><b>Members</b></h4>
    <div class="row">
      <div class="col-md-4 pt-4">
        <div class="team-card h-100">
          <div class="img-container">
            <img src="/website/img/2.png" />
          </div>          
          <h5>ADJUNCT
            PROFESSOR DATO’
            AMIRNUDDIN BIN
            MAZLAN</h5>
        </div>
      </div>
      <div class="col-md-4 pt-4">
        <div class="team-card h-100">
          <div class="img-container">
            <img src="/website/img/3.png" />
          </div>          
          <h5>DATUK DR.
            JOHNSON TEE</h5>
        </div>
      </div>
      <div class="col-md-4 pt-4">
        <div class="team-card h-100">
          <div class="img-container">
            <img src="/website/img/4.png" />
          </div>          
          <h5>SENIOR FELLOW
            DR. ZAKARIA
            TAIB</h5>
        </div>
      </div>            
    </div>
    <div class="row">
      <div class="col-md-4 pt-4">
        <div class="team-card h-100">
          <div class="img-container">
            <img src="/website/img/5.png" />
          </div>
          <h5>DR. S. SRI
            KUMAR</h5>
        </div>
      </div>
      <div class="col-md-4 pt-4">
        <div class="team-card h-100">
          <div class="img-container">
            <img src="/website/img/6.png" />
          </div>          
          <h5>ENCIK MOHD
            FAIRUZ ISMAIL</h5>
        </div>
      </div>
      <div class="col-md-4 pt-4">
        <div class="team-card h-100">
          <div class="img-container">
            <img src="/website/img/7.png" />
          </div>          
          <h5>ENCIK
            LOGANATHAN
            RATHAKRISHNAN</h5>
        </div>
      </div>            
    </div>    

  </div>

    <?php $this->load->view('../includes/newfooter');?>
