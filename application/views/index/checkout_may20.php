    <div class="course-lists">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-xl-12 col-lg-12 col-md-12 col-12">
            <div>
              <h1 class="mb-0 text-white display-4">Cart</h1>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!--PAGE HEADER ENDS HERE-->
                 <form action="" method="POST" id="contactUsForm">


    <div class="pt-5">
      <div class="container">
        <div class="row">

          <?php 
           $this->id_student =  $this->session->userdata['id_student'];
        if(empty($this->id_student)){  ?>

          <div class="col-lg-9 mb-5">
          <?php } else {?>
              <div class="col-lg-1  ">
              </div>
          <div class="col-lg-9 mb-5">

          <?php }?>  
            <div class="card mb-3">
              <div class="card-header bg-light">
                <h4 class="mb-0"><?php echo count($listOfCourses);?> Products in Cart</h4>
              </div>
              <div class="table-responsive">
                 <div id='tableDivID'></div>
              </div>
              <hr />
              <div class="card-body mt-n3">
                   <div class="row">
                    <div class="col-lg-5 offset-lg-7">
                      <p>Are you a staff or Student of AEU?</p>
                      <div class="mb-3">
                        <div class="custom-control custom-radio custom-control-inline">
                          <input type='radio' name='staff_student' id="radioStaff" value='1' class="custom-control-input" onclick="showtextbox(this.value)">
                          <label class="custom-control-label" for="radioStaff">Staff</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                          <input type='radio' name='staff_student' id="radioStudent" value='2' class="custom-control-input" onclick="showtextbox(this.value)">                      
                          <label class="custom-control-label" for="radioStudent">Student</label>
                        </div>   
                        <div class="custom-control custom-radio custom-control-inline">
                          <input type='radio' name='staff_student' id="radioNone" value='3' class="custom-control-input" onclick="showtextbox(this.value)" checked="checked">
                          <label class="custom-control-label" for="radioNone">None of the above</label>
                        </div>                                                                    
                      </div>
                    </div>
                   <div class="col-lg-5 offset-lg-7" style="display: none;" id='staffidDiv'>
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">Enter your ID Number</span>
                      </div>
                      <input type='text' id="idNumber" class="form-control" name='staff_student_number' value='' onblur='validatenric(this.value)'/>
                    </div>                   
                     <p style='display:none;' id='congPId' class="alert alert-success mt-2" role="alert">Congrats you have been eligible for discount</p>

                     <p style='display:none;' id='sorryPId' class="alert alert-danger mt-2">Sorry your ID does not exist in database</p>
                  </div>

                  
                </div>

                <div class="row">
                  <div class="col-lg-5 offset-lg-7">
                    <div class="input-group mb-3">
                      <input
                        type="text"
                        class="form-control"
                        placeholder="Coupon Code"
                      />
                      <div class="input-group-append">
                        <button
                          class="btn btn-outline-secondary"
                          type="button"
                          id="button-addon2"
                        >
                          Apply
                        </button>
                      </div>
                    </div>
                  </div>
                   <div class="col-lg-5 offset-lg-7">

                        <?php 
           $this->id_student =  $this->session->userdata['id_student'];
        if(empty($this->id_student)){  ?>

                     <a href="/index" class="btn btn-primary btn-block">
                    Continue to Buy Course
                    </a>
                  <?php } else {?>
                <button type='submit' class="btn btn-primary btn-block" name='Submit'>
                    Proceed to Checkout 
                    </button>
                  <?php } ?> 
         
                  </div>
                </div>

             
              </div>
            </div>
          </div>

<?php 
           $this->id_student =  $this->session->userdata['id_student'];
        if(empty($this->id_student)){  ?>
              
          <div class="col-lg-3 mb-5">
            <div class="card mb-3">
              <div class="card-body p-6">
             
              <!-- Form -->
                <div id='loginForm'>

                 <div class="mb-4">
                <h3 class="mb-1 font-weight-bold">Sign in</h3>
                
              </div>

                <!-- Username -->
                <div class="form-group">
                  <label for="email" class="form-label">Username or email</label>
                  <input type="email" id="email" class="form-control" name="email" placeholder="Email address here">
                </div>
                <!-- Password -->
                <div class="form-group">
                  <label for="password" class="form-label">Password</label>
                  <input type="password" id="password" class="form-control" name="password" placeholder="**************">
                </div>

                  <button type="submit" class="btn btn-primary btn-block">
                    Login to Checkout
                  </button>
         

                  <div class="text-center mt-2">Don’t have an account?
                  <a href="JavaScript:;" class="ml-1 text-nowrap" onclick="showregisterform()">Click to Register</a>
                  </div>
              
                  <!-- Button -->
                
                </div>

                <div id='registerForm' style="display: none;">
                  <div class="mb-4">
                <h3 class="mb-1 font-weight-bold">Register</h3>
                
              </div>

                <!-- Username -->
                <div class="form-group">
                  <label  class="form-label">Full Name</label>
                  <input type="text" id="full_name" class="form-control" name="full_name" placeholder="Email address here" >
                </div>
                <div class="form-group">
                  <label class="form-label">Email</label>
                  <input type="text" id="user_email" class="form-control" name="user_email" placeholder="Email address here" >
                </div>
                 <div class="form-group">
                  <label  class="form-label">NRIC / Passport</label>
                  <input type="text" id="nric" class="form-control" name="nric" placeholder="Email address here" >
                </div>

                <!-- Password -->
                <div class="form-group">
                  <label for="password" class="form-label">Password</label>
                  <input type="password" id="confirm_password" class="form-control" name="confirm_password" placeholder="**************" >
                </div>

                  <button type="submit" class="btn btn-primary btn-block">
                    Register & Checkout
                  </button>
         

                  <div class="text-center mt-2">Existing User?
                  <a href="JavaScript:;" class="ml-1 text-nowrap" onclick="showLoginform()">Click to Login</a>
                  </div>
              
                  <!-- Button -->
                
                </div>

            </div>
            </div>
          </div>
        <?php } ?>
        </div>
      </div>
    </div>

              </form>

    <!--CHECKOUT ENDS HERE-->

    <div class="footer">
      <div class="container">
        <div class="row align-items-center no-gutters border-top py-2">
          <!-- Desc -->
          <div class="col-md-6 col-12">
            <span>&copy; 2021 Speed. All Rights Reserved.</span>
          </div>
          <!-- Links -->
          <div class="col-12 col-md-6">
            <nav class="nav justify-content-center justify-content-md-end">
              <a class="nav-link active pl-0" href="#!">Privacy</a>
              <a class="nav-link" href="#!">Terms </a>
              <a class="nav-link" href="#!">Feedback</a>
              <a class="nav-link" href="#!">Support</a>
            </nav>
          </div>
        </div>
      </div>
    </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script
      src="//code.jquery.com/jquery-3.5.1.slim.min.js"
      integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
      crossorigin="anonymous"
    ></script>
    <script
      src="//cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
      integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns"
      crossorigin="anonymous"
    ></script>
    <script src="<?php echo BASE_PATH;?>website/js/jquery-1.12.4.min.js"></script>

<script src="/website/js/jquery.validate.min.js"></script>


 <script>

 function cart(){

        jQuery.get("/index/cart", function(data, status){
             $("#tableDivID").html(data);
         });

    }

  function showtextbox(id){
    $("#staffidDiv").hide();
      if(id!=3) {
       $("#staffidDiv").show();
      } 
    }

  function validatenric(value) {
    var stafforstudent = "Staff";//$("#staff_student").val();
     jQuery.get("/index/checkdiscount/"+stafforstudent+"/"+value, function(data, status){

          console.log(data);
         });
  }

   function deletecourse(id)
    {





       var cnf= confirm('Do you really want to delete from cart?');
       if(cnf==true) {
        jQuery.get("/coursedetails/deletecourses/"+id, function(data, status){

          if(data=='0') {
            alert('Your cart is empty,Please continue shopping');
            parent.location = '/';

          } else {
             parent.location= "<?php echo BASE_PATH;?>/index/checkout";
           }
         });
      }
    }

  </script>





<script type="text/javascript">
  function showregisterform() {
    $("#loginForm").hide();
    $("#registerForm").show();
  }
   function showLoginform() {
    $("#loginForm").show();
    $("#registerForm").hide();
  }
</script>

<script type='text/JavaScript'>
$(document).ready(function() {
  cart();
 $("#contactUsForm").validate({
                // Specify the validation rules
                rules: {
                    email: "required",  
                    password:"required",
                    full_name:"required",
                    user_email:"required",
                    nric:"required",
                    confirm_password:"required"
                    
                },
                // Specify the validation error messages
                messages: {
                    email: "<span>Please enter a Email</span>",
                    password: "<span>Please enter a Password</span>",
                    full_name: "<span>Please enter a Full Name</span>",
                    user_email: "<span>Please enter a Email</span>",
                    nric: "<span>Please enter a NRIC</span>",
                    confirm_password: "<span>Please enter a Password</span>",
                    
                }
            });
           
 });
  </script>

  </body>
</html>
