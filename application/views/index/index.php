 <link
      rel="stylesheet"
      type="text/css"
      href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"
    />
<style>
#botmanWidgetRoot{
    padding-top: 120px;
}
</style>
    <div id="homeCarousel" class="carousel slide home-slider" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#homeCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#homeCarousel" data-slide-to="1"></li>
      <li data-target="#homeCarousel" data-slide-to="2"></li>
      <li data-target="#homeCarousel" data-slide-to="3"></li>
    </ol>
    <div class="carousel-inner">
      <div class="carousel-item active">
        <div class="home-banner">
          <div class="container">
            <div class="row align-items-center">
              <div class="col-sm-6">
                <h1 class="mb-4">Transform Yourself
                </h1>
                <p class="mb-4 lead">
                  <strong>Learn, Unlearn and Relearn to Unlock Your Potential</strong>
                </p>
                <p>
                  The courses on this learning platform are instructor-led or self-paced or a combination of both and
                  aligned to close the
                  gaps in various areas such as business, management, communication, education, training, digital
                  technologies, IR 4.0,
                  social sciences, humanities, religion and other critical areas.

                </p>
              </div>
            </div>
          </div>
          <img src="/website/img/home_banner_3.jpg" />
        </div>
      </div>
      <div class="carousel-item">
        <div class="home-banner">
          <div class="container">
            <div class="row align-items-center">
              <div class="col-sm-6">
                <h1 class="mb-4">Certificate and Credential</h1>
                <p>
                  Validated and approved certificate that will boost your skills and you can add to your resume. The
                  certificate will be
                  given after completion of the course activities and assessments.

                </p>
              </div>
            </div>
          </div>
          <img src="/website/img/home_banner_1.jpg" />
        </div>
      </div>
      <div class="carousel-item">
        <div class="home-banner">
          <div class="container">
            <div class="row align-items-center">
              <div class="col-sm-6">
                <h1 class="mb-4">Golden Learning Experience
                </h1>
                <p>
                  Have a rich learning experience from experts and be engaged in a vibrant global learning community.

                </p>
              </div>
            </div>
          </div>
          <img src="/website/img/home_banner_4.jpg" />
        </div>
      </div>
      <div class="carousel-item">
        <div class="home-banner">
          <div class="container">
            <div class="row align-items-center">
              <div class="col-sm-6">
                <h1 class="mb-4">Interactive Learning Materials
                </h1>
                <p>
                  Interactive learning by using multimedia elements and you will keep in touch with your course
                  Instructor.


                </p>
              </div>
            </div>
          </div>
          <img src="/website/img/home_banner_2.jpg" />
        </div>
      </div>
    </div>
    <button class="carousel-control-prev" type="button" data-target="#homeCarousel" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </button>
    <button class="carousel-control-next" type="button" data-target="#homeCarousel" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </button>
  </div>
    <!-- FEATURES WRAPPERS STARTS HERE-->
    <div class="bg-brand-secondary py-4 shadow-sm features-wrapper">
      <div class="container">
        <div class="row align-items-center no-gutters">
          <div class="col-lg-10 offset-lg-1">
           <h3 class="text-center">Take the next step to transform yourself</h3>
          </div>
        </div>
      </div>
    </div>
    <!-- FEATURES WRAPPERS ENDS HERE-->

    <!--content-->

    <?php if(count($shortCourses)>0) {?>
    <div class="pt-lg-12 pb-lg-4 pt-8 pb-6 course-card" style="padding-top:45px;">
      <div class="container">
        <div class="row d-lg-flex align-items-center mb-4">
          <div class="col">
            <h2 class="mb-0">Education and Teaching</h2>
          </div>
          <div class="col-auto">
            <a href="/course/index/3" class="btn btn-outline-primary btn-sm">View All</a>
          </div>
        </div>
        <div class="course-slick-slider">
         

                  <?php 


                    for($i=0;$i<count($shortCourses);$i++) {    

                    $amount = number_format($shortCourses[$i]->amount,2);

                       if($amount>0) {
                      $amount = "RM ".$amount;
                    } else {
                      $amount = "FREE";
                    }



            ?>


          <div class="col-lg-4 col-md-6 col-12">
            <div class="card mb-4 card-hover">
              <a href="/coursedetails/index/<?php echo $shortCourses[$i]->id;?>" class="card-img-top">
                     <img
                  src="<?php echo "/assets/images/".$shortCourses[$i]->course_image;?>"
                  alt
                  class="rounded-top card-img-top"
                />
              </a>
              <div class="card-body">
                <h4 class="mb-2 text-truncate-line-2" class="text-inherit">
                  <a href="/coursedetails/index/<?php echo $shortCourses[$i]->id;?>" class="text-inherit"
                    ><?php echo ucwords($shortCourses[$i]->name);?></a
                  >

                                    <?php if($shortCourses[$i]->course_sub_type=='MQA') {?> 
                                              <img src="/website/img/mqa.png" style="height:40px;float: right;" />
                  <?php } ?> 


                </h4>
                <!--list-->
                  <ul class="mb-2 list-inline">
                  <li class="list-inline-item">
                    <i class="fe fe-clock mr-1"></i><?php echo $shortCourses[$i]->max_duration . " - " . $shortCourses[$i]->duration_type ?>
                  </li>
                  <li class="list-inline-item">
                    <i class="fe fe-bar-chart mr-1"></i>Advance
                  </li>
                </ul>


                 


                <div class="1h-1">
                  <div class="d-flex">
                    <div class="h5"><?php echo $amount;?></div>
                   
                  </div>
                </div>
                  <div class="1h-1">
                  <div class="d-flex">
                    <div class="">
                                          <i class="fe fe-book mr-1"></i>
                                          <?php echo $shortCourses[$i]->producttype;?>

                    </div>
                  </div>
                </div>
                

                <div class="d-flex mt-2">
                  <a href="/coursedetails/index/<?php echo $shortCourses[$i]->id;?>" class="btn btn-outline-primary btn-sm">Enroll</a>
                </div>
              </div>
              <div class="card-footer">
                <div class="row align-items-center no-gutters">
                  <div class="col-auto">
                    <?php if($shortCourses[$i]->staffimage!='') {?>
                    <img
                      src="/assets/images/<?php echo $shortCourses[$i]->staffimage;?>"
                      class="rounded-circle avatar-xs"
                    />
                   <?php } else { ?>
                     <img
                      src="/website/img/default-staff.jpg"
                      class="rounded-circle avatar-xs"
                    />

                  <?php } ?> 
                  </div>
                  <div class="col ml-2" style="font-weight:bold;">
                    <span><?php echo $shortCourses[$i]->staffname;?></span>
                  </div>
                 
                </div>
              </div>
            </div>
          </div>

                  <?php } ?> 



        </div>
      </div>
    </div>
  <?php } ?>


 <?php if(count($microCourse)>0) {?>
    <div class="pb-lg-4 pt-lg-5 course-card alternate-bg">
      <div class="container">
        <div class="row d-lg-flex align-items-center mb-4">
          <div class="col">
            <h2 class="mb-0">Business and Management</h2>
          </div>
          <div class="col-auto">
            <a href="/course/index/2" class="btn btn-outline-primary btn-sm">View All</a>
          </div>
        </div>
        <div class="course-slick-slider">
          <?php 

                    for($i=0;$i<count($microCourse);$i++) { 
          


                    $amount = number_format($microCourse[$i]->amount,2);

                       if($amount>0) {
                      $amount = "RM ".$amount;
                    } else {
                      $amount = "FREE";
                    }


            ?>


          <div class="col-lg-4 col-md-6 col-12">
              <div class="card mb-4 card-hover" 
              data-toggle="popover"
              data-content="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque consectetur consequat eleifend. Donec tincidunt ante eu ipsum aliquam, in congue elit condimentum. Praesent interdum, ligula at euismod tempus, ex nulla lobortis elit, sed ullamcorper risus magna in dolor. Nullam molestie dapibus facilisis. Phasellus vel lorem luctus, facilisis justo nec, cursus nunc. Nullam in viverra nibh. Aenean et sagittis risus."
              data-placement="right"
              data-trigger="hover">

              <a href="/coursedetails/index/<?php echo $microCourse[$i]->id;?>" class="card-img-top">
                     <img
                  src="<?php echo "/assets/images/".$microCourse[$i]->course_image;?>"
                  alt
                  class="rounded-top card-img-top"
                />
              </a>
              <div class="card-body">
                <h4 class="mb-2 text-truncate-line-2" class="text-inherit">
                  <a href="/coursedetails/index/<?php echo $microCourse[$i]->id;?>" class="text-inherit"
                    ><?php echo ucwords($microCourse[$i]->name);?></a
                  >

                  <?php if($microCourse[$i]->course_sub_type=='MQA') {?> 
                                              <img src="/website/img/mqa.png" style="height:40px;float: right;" />
                  <?php } ?> 
                </h4>
                <!--list-->
                  <ul class="mb-2 list-inline">
                  <li class="list-inline-item">
                    <i class="fe fe-clock mr-1"></i><?php echo $microCourse[$i]->max_duration . " - " . $microCourse[$i]->duration_type ?>
                  </li>
                  <li class="list-inline-item">
                    <i class="fe fe-bar-chart mr-1"></i>Advance
                  </li>
                </ul>
                <div class="1h-1">
                  <div class="d-flex">
                    <div class="h5"><?php echo $amount;?></div>
                   
                  </div>
                </div>
                 <div class="1h-1">
                  <div class="d-flex">
                     <div class="">
                                          <i class="fe fe-book mr-1"></i>
                                          <?php echo $microCourse[$i]->producttype;?>

                    </div>
                  </div>
                </div>
                

                <div class="d-flex mt-2">
                  <a href="/coursedetails/index/<?php echo $microCourse[$i]->id;?>" class="btn btn-outline-primary btn-sm">Enroll</a>
                 <!--  <a href="#" class="btn btn-outline-info btn-sm ml-auto"
                    >Add to Cart</a
                  > -->
                </div>
              </div>
              <!--footer card-->
              <div class="card-footer">
                <div class="row align-items-center no-gutters">
                  <div class="col-auto">
                    <?php if($microCourse[$i]->staffimage!='') {?>
                    <img
                      src="/assets/images/<?php echo $microCourse[$i]->staffimage;?>"
                      class="rounded-circle avatar-xs"
                    />
                   <?php } else { ?>
                     <img
                      src="/website/img/default-staff.jpg"
                      class="rounded-circle avatar-xs"
                    />

                  <?php } ?> 
                  </div>
                  <div class="col ml-2" style="font-weight:bold;">
                    <span><?php echo $microCourse[$i]->staffname;?></span>
                  </div>               
                </div>
              </div>
            </div>
          </div>

                  <?php } ?> 


        </div>
      </div>
    </div>
      <?php } ?>

     <?php if(count($academicCourse)>0) {?>
    <div class="pb-lg-4 pt-lg-5 course-card">
      <div class="container">
        <div class="row d-lg-flex align-items-center mb-4">
          <div class="col">
            <h2 class="mb-0">Information and Communication Technologies (ICT)</h2>
          </div>
          <div class="col-auto">
            <a href="/course/index/4" class="btn btn-outline-primary btn-sm">View All</a>
          </div>
        </div>
        <div class="course-slick-slider">
          <?php 

                    for($i=0;$i<count($academicCourse);$i++) { 
          
                        $amount = number_format($academicCourse[$i]->amount,2);

                         if($amount>0) {
                        $amount = "RM ".$amount;
                      } else {
                        $amount = "FREE";
                      }



            ?>


          <div class="col-lg-4 col-md-6 col-12">
            <div class="card mb-4 card-hover">
              <a href="/coursedetails/index/<?php echo $academicCourse[$i]->id;?>" class="card-img-top">
                     <img
                  src="<?php echo "/assets/images/".$academicCourse[$i]->course_image;?>"
                  alt
                  class="rounded-top card-img-top"
                />
              </a>
              <div class="card-body">
                <h4 class="mb-2 text-truncate-line-2" class="text-inherit">
                  <a href="/coursedetails/index/<?php echo $academicCourse[$i]->id;?>" class="text-inherit"
                    ><?php echo ucwords($academicCourse[$i]->name);?></a
                  >
                   <?php if($academicCourse[$i]->course_sub_type=='MQA') {?> 
                                              <img src="/website/img/mqa.png" style="height:40px;float: right;" />
                  <?php } ?> 
                </h4>
                <!--list-->
                  <ul class="mb-2 list-inline">
                  <li class="list-inline-item">
                    <i class="fe fe-clock mr-1"></i><?php echo $academicCourse[$i]->max_duration . " - " . $academicCourse[$i]->duration_type ?>
                  </li>
                  <li class="list-inline-item">
                    <i class="fe fe-bar-chart mr-1"></i>Advance
                  </li>
                </ul>
                <div class="1h-1">
                  <div class="d-flex">
                    <div class="h5"><?php echo $amount;?></div>
                    
                  </div>
                </div>

                  <div class="1h-1">
                  <div class="d-flex">
                     <div class="">
                                          <i class="fe fe-book mr-1"></i>
                                          <?php echo $academicCourse[$i]->producttype;?>

                    </div>
                  </div>
                </div>
                
                

                <div class="d-flex mt-2">
                  <a href="/coursedetails/index/<?php echo $academicCourse[$i]->id;?>" class="btn btn-outline-primary btn-sm">Enroll</a>
                 <!--  <a href="#" class="btn btn-outline-info btn-sm ml-auto"
                    >Add to Cart</a
                  > -->
                </div>
              </div>
              <!--footer card-->
              <div class="card-footer">
                <div class="row align-items-center no-gutters">
                  <div class="col-auto">
                    <?php if($academicCourse[$i]->staffimage!='') {?>
                    <img
                      src="/assets/images/<?php echo $academicCourse[$i]->staffimage;?>"
                      class="rounded-circle avatar-xs"
                    />
                  <?php } else { ?>
                     <img
                      src="/website/img/default-staff.jpg"
                      class="rounded-circle avatar-xs"
                    />

                  <?php } ?>  
                  </div>
                  <div class="col ml-2" style="font-weight:bold;">
                    <span><?php echo $academicCourse[$i]->staffname;?></span>
                  </div>
                 
                </div>
              </div>
            </div>
          </div>

                  <?php } ?> 


        </div>
      </div>
    </div>   
      <?php } ?>


      
   
    <?php if(count($socialCourse)>0) {?>
    <div class="pb-lg-4 pt-lg-5 course-card  alternate-bg">
      <div class="container">
        <div class="row d-lg-flex align-items-center mb-4">
          <div class="col">
            <h2 class="mb-0">Social Science and Humanities</h2>
          </div>
          <div class="col-auto">
            <a href="/course/index/7" class="btn btn-outline-primary btn-sm">View All</a>
          </div>
        </div>
        <div class="course-slick-slider">
          <?php 

                    for($i=0;$i<count($socialCourse);$i++) { 
          
                        $amount = number_format($socialCourse[$i]->amount,2);

                         if($amount>0) {
                        $amount = "RM ".$amount;
                      } else {
                        $amount = "FREE";
                      }



            ?>


          <div class="col-lg-4 col-md-6 col-12">
            <div class="card mb-4 card-hover">
              <a href="/coursedetails/index/<?php echo $socialCourse[$i]->id;?>" class="card-img-top">
                     <img
                  src="<?php echo "/assets/images/".$socialCourse[$i]->course_image;?>"
                  alt
                  class="rounded-top card-img-top"
                />
              </a>
              <div class="card-body">
                <h4 class="mb-2 text-truncate-line-2" class="text-inherit">
                  <a href="/coursedetails/index/<?php echo $socialCourse[$i]->id;?>" class="text-inherit"
                    ><?php echo ucwords($socialCourse[$i]->name);?></a
                  >
                  <?php if($socialCourse[$i]->course_sub_type=='MQA') {?> 
                                              <img src="/website/img/mqa.png" style="height:40px;float: right;" />
                  <?php } ?> 

                </h4>
                <!--list-->
                  <ul class="mb-2 list-inline">
                  <li class="list-inline-item">
                    <i class="fe fe-clock mr-1"></i><?php echo $socialCourse[$i]->max_duration . " - " . $socialCourse[$i]->duration_type ?>
                  </li>
                  <li class="list-inline-item">
                    <i class="fe fe-bar-chart mr-1"></i>Advance
                  </li>
                </ul>
                <div class="1h-1">
                  <div class="d-flex">
                    <div class="h5"><?php echo $amount;?></div>
                    
                  </div>
                </div>

                  <div class="1h-1">
                  <div class="d-flex">
                     <div class="">
                                          <i class="fe fe-book mr-1"></i>
                                          <?php echo $socialCourse[$i]->producttype;?>

                    </div>
                  </div>
                </div>
                
                

                <div class="d-flex mt-2">
                  <a href="/coursedetails/index/<?php echo $socialCourse[$i]->id;?>" class="btn btn-outline-primary btn-sm">Enroll</a>
                 <!--  <a href="#" class="btn btn-outline-info btn-sm ml-auto"
                    >Add to Cart</a
                  > -->
                </div>
              </div>
              <!--footer card-->
              <div class="card-footer">
                <div class="row align-items-center no-gutters">
                  <div class="col-auto">
                    <?php if($socialCourse[$i]->staffimage!='') {?>
                    <img
                      src="/assets/images/<?php echo $socialCourse[$i]->staffimage;?>"
                      class="rounded-circle avatar-xs"
                    />
                  <?php } else { ?>
                     <img
                      src="/website/img/default-staff.jpg"
                      class="rounded-circle avatar-xs"
                    />

                  <?php } ?>  
                  </div>
                  <div class="col ml-2" style="font-weight:bold;">
                    <span><?php echo $socialCourse[$i]->staffname;?></span>
                  </div>
                 
                </div>
              </div>
            </div>
          </div>

                  <?php } ?> 


        </div>
      </div>
    </div>   
      <?php } ?>


    <div class="pb-4 pt-lg-5 course-card blue-wrapper new-courses">
        <div class="container">
                  <h2 class="text-center">What’s new on Myeduskills</h2>
                  <h2 class="text-center" style="font-size: 1.5rem;padding-bottom: 20px;">Discover these newly launched courses</h2>

          <div class="row align-items-center">


       <?php  for($i=0;$i<count($myeduskills);$i++) { 
          
                        $amount = number_format($myeduskills[$i]->amount,2);

                         if($amount>0) {
                        $amount = "RM ".$amount;
                      } else {
                        $amount = "FREE";
                      }
            ?>

            <div class="col-lg-4 col-md-6 col-12">
            <div class="card mb-4 card-hover">
              <a href="/coursedetails/index/<?php echo $myeduskills[$i]->id;?>" class="card-img-top">
                     <img
                  src="<?php echo "/assets/images/".$myeduskills[$i]->course_image;?>"
                  alt
                  class="rounded-top card-img-top"
                />
              </a>
              <div class="card-body">
                <h4 class="mb-2 text-truncate-line-2" class="text-inherit">
                  <a href="/coursedetails/index/<?php echo $myeduskills[$i]->id;?>" class="text-inherit"
                    ><?php echo ucwords($myeduskills[$i]->name);?></a
                  >

                   <?php if($myeduskills[$i]->course_sub_type=='MQA') {?> 
                                              <img src="/website/img/mqa.png" style="height:40px;float: right;" />
                  <?php } ?> 

                </h4>
                <!--list-->
                  <ul class="mb-2 list-inline">
                  <li class="list-inline-item">
                    <i class="fe fe-clock mr-1"></i><?php echo $myeduskills[$i]->max_duration . " - " . $myeduskills[$i]->duration_type ?>
                  </li>
                  <li class="list-inline-item">
                    <i class="fe fe-bar-chart mr-1"></i>Advance
                  </li>
                </ul>
                <div class="1h-1">
                  <div class="d-flex">
                    <div class="h5"><?php echo $amount;?></div>
                    
                  </div>
                </div>

                  <div class="1h-1">
                  <div class="d-flex">
                     <div class="">
                                          <i class="fe fe-book mr-1"></i>
                                          <?php echo $myeduskills[$i]->producttype;?>

                    </div>
                  </div>
                </div>
                
                

                <div class="d-flex mt-2">
                  <a href="/coursedetails/index/<?php echo $myeduskills[$i]->id;?>" class="btn btn-outline-primary btn-sm">Enroll</a>
                 <!--  <a href="#" class="btn btn-outline-info btn-sm ml-auto"
                    >Add to Cart</a
                  > -->
                </div>
              </div>
              <!--footer card-->
              <div class="card-footer">
                <div class="row align-items-center no-gutters">
                  <div class="col-auto">
                    <?php if($myeduskills[$i]->staffimage!='') {?>
                    <img
                      src="/assets/images/<?php echo $myeduskills[$i]->staffimage;?>"
                      class="rounded-circle avatar-xs"
                    />
                  <?php } else { ?>
                     <img
                      src="/website/img/default-staff.jpg"
                      class="rounded-circle avatar-xs"
                    />

                  <?php } ?>  
                  </div>
                  <div class="col ml-2" style="font-weight:bold;">
                    <span><?php echo $myeduskills[$i]->staffname;?></span>
                  </div>
                 
                </div>
              </div>
            </div>
          </div>

        <?php } ?> 

          </div>
        </div>
      </div> 



    <div class="pb-4 pt-5 bg-white course-tabs">
      <div class="container">
        <h2 class="pb-4 text-center">Let’s Start Transforming Yourself</h2>
        <ul class="nav nav-tabs" id="myTab" role="tablist">
          <li class="nav-item" role="presentation">
            <button
              class="nav-link"
              id="short-courses-tab"
              data-toggle="tab"
              data-target="#shortCourses"
              type="button"
              onclick="scrolldownshortcourses()"
              role="tab"
              aria-controls="shortCourses"
              aria-selected="true"
            >
              Short Courses
            </button>
          </li>
          <li class="nav-item" role="presentation">
            <button
              class="nav-link"
              id="micro-tab"
              data-toggle="tab"
              data-target="#microCredentials"
              type="button"
              onclick="scrolldownmicroCredentials()"              
              role="tab"
              aria-controls="microCredentials"
              aria-selected="false"
            >
              Micro-Credentials
            </button>
          </li>
          <li class="nav-item" role="presentation">
            <button
              class="nav-link"
              id="professional-qualifications-tab"
              data-toggle="tab"
              data-target="#professionalQualifications"
              type="button"
              role="tab"
              aria-controls="professionalQualifications"
              aria-selected="false"
            >
              Professional Courses
            </button>
          </li>
        </ul>

        <div class="tab-content">
          <div
            class="tab-pane active"
            id="shortCourses"
            role="tabpanel"
            aria-labelledby="short-courses-tab"
          >
            <div class="row">
              <div class="col-md-4">
                <h4><div class="course-count">1</div>
                <?php echo $shortstep->step_one_title;?></h4>
                <p>
                  <?php echo $shortstep->step_one_description;?>
                </p>
              </div>
               <div class="col-md-4">
                <h4><div class="course-count">2</div>
                <?php echo $shortstep->step_two_title;?></h4>
                <p>
                  <?php echo $shortstep->step_two_description;?>
                </p>
              </div>
               <div class="col-md-4">
                <h4><div class="course-count">3</div>
                <?php echo $shortstep->step_three_title;?></h4>
                <p>
                  <?php echo $shortstep->step_three_description;?>
                </p>
              </div>
               
            </div>
          </div>
          <div
            class="tab-pane"
            id="microCredentials"
            role="tabpanel"
            aria-labelledby="micro-tab"
          >
             <div class="row">
              <div class="col-md-4">
                <h4><div class="course-count">1</div>
                <?php echo $shortstep->step_one_title;?></h4>
                <p>
                  <?php echo $shortstep->step_one_description;?>
                </p>
              </div>
               <div class="col-md-4">
                <h4><div class="course-count">2</div>
                <?php echo $shortstep->step_two_title;?></h4>
                <p>
                  <?php echo $shortstep->step_two_description;?>
                </p>
              </div>
               <div class="col-md-4">
                <h4><div class="course-count">3</div>
                <?php echo $shortstep->step_three_title;?></h4>
                <p>
                  <?php echo $shortstep->step_three_description;?>
                </p>
              </div>
               
            </div>
          </div>
          <div
            class="tab-pane"
            id="professionalQualifications"
            role="tabpanel"
            aria-labelledby="professional-qualifications-tab"
          >
                        <div class="row">
              <div class="col-md-4">
                <h4><div class="course-count">1</div>
                <?php echo $shortstep->step_one_title;?></h4>
                <p>
                  <?php echo $shortstep->step_one_description;?>
                </p>
              </div>
               <div class="col-md-4">
                <h4><div class="course-count">2</div>
                <?php echo $shortstep->step_two_title;?></h4>
                <p>
                  <?php echo $shortstep->step_two_description;?>
                </p>
              </div>
               <div class="col-md-4">
                <h4><div class="course-count">3</div>
                <?php echo $shortstep->step_three_title;?></h4>
                <p>
                  <?php echo $shortstep->step_three_description;?>
                </p>
              </div>
               
            </div>
          </div>
        </div>
      </div>
    </div>
    

    <?php if(count($prorammetypeone)>0) {?>
    <div class="pb-lg-4 pt-lg-5 course-card" id="shortCoursesDiv">
      <div class="container">
        <div class="row d-lg-flex align-items-center mb-4">
          <div class="col">
            <h2 class="mb-0">Short Courses</h2>
          </div>
          <div class="col-auto">
            <a href="/course/summary/1" class="btn btn-outline-primary btn-sm">View All</a>
          </div>
        </div>
        <div class="course-slick-slider">
          <?php 

                    for($i=0;$i<count($prorammetypeone);$i++) { 
          


                    $amount = number_format($prorammetypeone[$i]->amount,2);

                       if($amount>0) {
                      $amount = "RM ".$amount;
                    } else {
                      $amount = "FREE";
                    }


            ?>


          <div class="col-lg-4 col-md-6 col-12">
              <div class="card mb-4 card-hover" 
              data-toggle="popover"
              data-content="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque consectetur consequat eleifend. Donec tincidunt ante eu ipsum aliquam, in congue elit condimentum. Praesent interdum, ligula at euismod tempus, ex nulla lobortis elit, sed ullamcorper risus magna in dolor. Nullam molestie dapibus facilisis. Phasellus vel lorem luctus, facilisis justo nec, cursus nunc. Nullam in viverra nibh. Aenean et sagittis risus."
              data-placement="right"
              data-trigger="hover">

              <a href="/coursedetails/index/<?php echo $prorammetypeone[$i]->id;?>" class="card-img-top">
                     <img
                  src="<?php echo "/assets/images/".$prorammetypeone[$i]->course_image;?>"
                  alt
                  class="rounded-top card-img-top"
                />
              </a>
              <div class="card-body">
                <h4 class="mb-2 text-truncate-line-2" class="text-inherit">
                  <a href="/coursedetails/index/<?php echo $prorammetypeone[$i]->id;?>" class="text-inherit"
                    ><?php echo ucwords($prorammetypeone[$i]->name);?></a
                  >
                </h4>
                <!--list-->
                  <ul class="mb-2 list-inline">
                  <li class="list-inline-item">
                    <i class="fe fe-clock mr-1"></i><?php echo $prorammetypeone[$i]->max_duration . " - " . $prorammetypeone[$i]->duration_type ?>
                  </li>
                  <li class="list-inline-item">
                    <i class="fe fe-bar-chart mr-1"></i>Advance
                  </li>
                </ul>
                <div class="1h-1">
                  <div class="d-flex">
                    <div class="h5"><?php echo $amount;?></div>
                   
                  </div>
                </div>
                 <div class="1h-1">
                  <div class="d-flex">
                     <div class="">
                                          <i class="fe fe-book mr-1"></i>
                                          <?php echo $prorammetypeone[$i]->producttype;?>

                    </div>
                  </div>
                </div>
                

                <div class="d-flex mt-2">
                  <a href="/coursedetails/index/<?php echo $prorammetypeone[$i]->id;?>" class="btn btn-outline-primary btn-sm">Enroll</a>
                 <!--  <a href="#" class="btn btn-outline-info btn-sm ml-auto"
                    >Add to Cart</a
                  > -->
                </div>
              </div>
              <!--footer card-->
              <div class="card-footer">
                <div class="row align-items-center no-gutters">
                  <div class="col-auto">
                    <?php if($prorammetypeone[$i]->staffimage!='') {?>
                    <img
                      src="/assets/images/<?php echo $prorammetypeone[$i]->staffimage;?>"
                      class="rounded-circle avatar-xs"
                    />
                   <?php } else { ?>
                     <img
                      src="/website/img/default-staff.jpg"
                      class="rounded-circle avatar-xs"
                    />

                  <?php } ?> 
                  </div>
                  <div class="col ml-2" style="font-weight:bold;">
                    <span><?php echo $prorammetypeone[$i]->staffname;?></span>
                  </div>               
                </div>
              </div>
            </div>
          </div>

                  <?php } ?> 


        </div>
      </div>
    </div>
      <?php } ?>
      <?php if(count($prorammetypetwo)>0) {?>
    <div class="pb-lg-4 pt-lg-5 course-card alternate-bg" id="microCredentialsDiv">
      <div class="container">
        <div class="row d-lg-flex align-items-center mb-4">
          <div class="col">
            <h2 class="mb-0">Micro - Credentials</h2>
          </div>
          <div class="col-auto">
            <a href="/course/summary/2" class="btn btn-outline-primary btn-sm">View All</a>
          </div>
        </div>
        <div class="course-slick-slider">
          <?php 

                    for($i=0;$i<count($prorammetypetwo);$i++) { 
          


                    $amount = number_format($prorammetypetwo[$i]->amount,2);

                       if($amount>0) {
                      $amount = "RM ".$amount;
                    } else {
                      $amount = "FREE";
                    }


            ?>


          <div class="col-lg-4 col-md-6 col-12">
              <div class="card mb-4 card-hover" 
              data-toggle="popover"
              data-content="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque consectetur consequat eleifend. Donec tincidunt ante eu ipsum aliquam, in congue elit condimentum. Praesent interdum, ligula at euismod tempus, ex nulla lobortis elit, sed ullamcorper risus magna in dolor. Nullam molestie dapibus facilisis. Phasellus vel lorem luctus, facilisis justo nec, cursus nunc. Nullam in viverra nibh. Aenean et sagittis risus."
              data-placement="right"
              data-trigger="hover">

              <a href="/coursedetails/index/<?php echo $prorammetypetwo[$i]->id;?>" class="card-img-top">
                     <img
                  src="<?php echo "/assets/images/".$prorammetypetwo[$i]->course_image;?>"
                  alt
                  class="rounded-top card-img-top"
                />
              </a>
              <div class="card-body">
                <h4 class="mb-2 text-truncate-line-2" class="text-inherit">
                  <a href="/coursedetails/index/<?php echo $prorammetypetwo[$i]->id;?>" class="text-inherit"
                    ><?php echo ucwords($prorammetypetwo[$i]->name);?></a
                  >

                  <?php if($prorammetypetwo[$i]->course_sub_type=='MQA') {?> 
                                              <img src="/website/img/mqa.png" style="height:40px;float: right;" />
                  <?php } ?> 

                </h4>
                <!--list-->
                  <ul class="mb-2 list-inline">
                  <li class="list-inline-item">
                    <i class="fe fe-clock mr-1"></i><?php echo $prorammetypetwo[$i]->max_duration . " - " . $prorammetypetwo[$i]->duration_type ?>
                  </li>
                  <li class="list-inline-item">
                    <i class="fe fe-bar-chart mr-1"></i>Advance
                  </li>
                </ul>
                <div class="1h-1">
                  <div class="d-flex">
                    <div class="h5"><?php echo $amount;?></div>
                   
                  </div>
                </div>
                 <div class="1h-1">
                  <div class="d-flex">
                     <div class="">
                                          <i class="fe fe-book mr-1"></i>
                                          <?php echo $prorammetypetwo[$i]->producttype;?>

                    </div>
                  </div>
                </div>
                

                <div class="d-flex mt-2">
                  <a href="/coursedetails/index/<?php echo $prorammetypetwo[$i]->id;?>" class="btn btn-outline-primary btn-sm">Enroll</a>
                 <!--  <a href="#" class="btn btn-outline-info btn-sm ml-auto"
                    >Add to Cart</a
                  > -->
                </div>
              </div>
              <!--footer card-->
              <div class="card-footer">
                <div class="row align-items-center no-gutters">
                  <div class="col-auto">
                    <?php if($prorammetypetwo[$i]->staffimage!='') {?>
                    <img
                      src="/assets/images/<?php echo $prorammetypetwo[$i]->staffimage;?>"
                      class="rounded-circle avatar-xs"
                    />
                   <?php } else { ?>
                     <img
                      src="/website/img/default-staff.jpg"
                      class="rounded-circle avatar-xs"
                    />

                  <?php } ?> 
                  </div>
                  <div class="col ml-2" style="font-weight:bold;">
                    <span><?php echo $prorammetypetwo[$i]->staffname;?></span>
                  </div>               
                </div>
              </div>
            </div>
          </div>

                  <?php } ?> 


        </div>
      </div>
    </div>
      <?php } ?>

       <?php if(count($prorammetypethree)>0 && count($prorammetypethree)==3) {?>
    <div class="pb-lg-4 pt-lg-5 course-card">
      <div class="container">
        <div class="row d-lg-flex align-items-center mb-4">
          <div class="col">
            <h2 class="mb-0">Professional Courses</h2>
          </div>
          <div class="col-auto">
            <a href="/course/summary/3" class="btn btn-outline-primary btn-sm">View All</a>
          </div>
        </div>
        <div class="course-slick-slider">
          <?php 

                    for($i=0;$i<count($prorammetypethree);$i++) { 
          


                    $amount = number_format($prorammetypethree[$i]->amount,2);

                       if($amount>0) {
                      $amount = "RM ".$amount;
                    } else {
                      $amount = "FREE";
                    }


            ?>


          <div class="col-lg-4 col-md-6 col-12">
              <div class="card mb-4 card-hover" 
              data-toggle="popover"
              data-placement="right"
              data-trigger="hover">

              <a href="/coursedetails/index/<?php echo $prorammetypethree[$i]->id;?>" class="card-img-top">
                     <img
                  src="<?php echo "/assets/images/".$prorammetypethree[$i]->course_image;?>"
                  alt
                  class="rounded-top card-img-top"
                />
              </a>
              <div class="card-body">
                <h4 class="mb-2 text-truncate-line-2" class="text-inherit">
                  <a href="/coursedetails/index/<?php echo $prorammetypethree[$i]->id;?>" class="text-inherit"
                    ><?php echo ucwords($prorammetypethree[$i]->name);?></a
                  >
                </h4>
                <!--list-->
                  <ul class="mb-2 list-inline">
                  <li class="list-inline-item">
                    <i class="fe fe-clock mr-1"></i><?php echo $prorammetypethree[$i]->max_duration . " - " . $prorammetypethree[$i]->duration_type ?>
                  </li>
                  <li class="list-inline-item">
                    <i class="fe fe-bar-chart mr-1"></i>Advance
                  </li>
                </ul>
                <div class="1h-1">
                  <div class="d-flex">
                    <div class="h5"><?php echo $amount;?></div>
                   
                  </div>
                </div>
                 <div class="1h-1">
                  <div class="d-flex">
                     <div class="">
                                          <i class="fe fe-book mr-1"></i>
                                          <?php echo $prorammetypethree[$i]->producttype;?>

                    </div>
                  </div>
                </div>
                

                <div class="d-flex mt-2">
                  <a href="/coursedetails/index/<?php echo $prorammetypethree[$i]->id;?>" class="btn btn-outline-primary btn-sm">Enroll</a>
                 <!--  <a href="#" class="btn btn-outline-info btn-sm ml-auto"
                    >Add to Cart</a
                  > -->
                </div>
              </div>
              <!--footer card-->
              <div class="card-footer">
                <div class="row align-items-center no-gutters">
                  <div class="col-auto">
                    <?php if($prorammetypethree[$i]->staffimage!='') {?>
                    <img
                      src="/assets/images/<?php echo $prorammetypethree[$i]->staffimage;?>"
                      class="rounded-circle avatar-xs"
                    />
                   <?php } else { ?>
                     <img
                      src="/website/img/default-staff.jpg"
                      class="rounded-circle avatar-xs"
                    />

                  <?php } ?> 
                  </div>
                  <div class="col ml-2" style="font-weight:bold;">
                    <span><?php echo $prorammetypethree[$i]->staffname;?></span>
                  </div>               
                </div>
              </div>
            </div>
          </div>

                  <?php } ?> 


        </div>
      </div>
    </div>
      <?php } ?>

        <div class="bg-brand-secondary pb-4 pt-5">
      <div class="container">
        <h2 class="text-center pb-4">Why myeduskills</h2>
        <div>
          <img src="/website/img/why_myeduskills.jpg" class="img-fluid" />
        </div>
      </div>
    </div>

     <div class="pb-lg-4 pt-lg-5 course-card alternate-bg">
      <div class="container">
        <div class="row d-lg-flex align-items-center mb-4">
          <div class="col">
            <h2 class="mb-0">How our Learners Feel</h2>
          </div>
          
        </div>
        <div class="course-slick-slider">
         
            <div class="col-lg-4 col-md-6 col-12">
            <div class="testimonial-col">
              <img
                class="avatar-img"
                src="/website/testimonial/student.png"
                alt="Image Description"
                style="margin-left: auto;
    margin-right: auto;"
              />
              <div class="title">Syahirah</div>
              <p class="text">This platform and courses offered was what I needed to push my career into the next level. Everything was really comprehensible and full of useful content.

              </p>
            </div>
            </div>

             <div class="col-lg-4 col-md-6 col-12">
            <div class="testimonial-col">
              <img
                class="avatar-img"
                src="/website/testimonial/student.png"
                alt="Image Description"
                style="margin-left: auto;
    margin-right: auto;"
              />
              <div class="title">Nazmi Nabil</div>
              <p class="text">I am improving my professional and personal development. Thanks for all who made it available online. It’s very excellent.

              </p>
            </div>
            </div>

             <div class="col-lg-4 col-md-6 col-12">
            <div class="testimonial-col">
              <img
                class="avatar-img"
                src="/website/testimonial/student.png"
                alt="Image Description"
                style="margin-left: auto;
    margin-right: auto;"
              />
              <div class="title">Adib Khusyairi</div>
              <p class="text">
                This platform is highly practical. Highly recommend - the course provides a diverse range of topics, straightforward and clear
              </p>
            </div>
            </div>

             <div class="col-lg-4 col-md-6 col-12">
            <div class="testimonial-col">
              <img
                class="avatar-img"
                src="/website/testimonial/student.png"
                alt="Image Description"
                style="margin-left: auto;
    margin-right: auto;"
              />
              <div class="title">Michael Leong Swee Hau</div>
              <p class="text">
               This form of modular courses not only provide flexible learning experience but also an enabler and effective way to fast track your academic journey.
              </p>
            </div>
            </div>

             <div class="col-lg-4 col-md-6 col-12">
            <div class="testimonial-col">
              <img
                class="avatar-img"
                src="/website/testimonial/student.png"
                alt="Image Description"
                style="margin-left: auto;
    margin-right: auto;"
              />
              <div class="title">Adib Khusyairi</div>
              <p class="text">
                This platform and the courses it provided were exactly what I needed to take my career to the next level. Everything was very clear and full of useful information.
              </p>
            </div>
            </div>


             <div class="col-lg-4 col-md-6 col-12">
            <div class="testimonial-col">
              <img
                class="avatar-img"
                src="/website/testimonial/student.png"
                alt="Image Description"
                style="margin-left: auto;
    margin-right: auto;"
              />
              <div class="title">Delores A/L Tanggapan</div>
              <p class="text">
               Micro-Credentials designed with a specific learning path and involved studying a programme at my own pace in a mainly online environment. I manage to pursue Micro-Credentials because it offers the flexibility for me to study at my own pace allowing me to complete a programme in my own time.
              </p>
            </div>
            </div>
          </div>

                 


        </div>
      </div>


        <div class="new-testimonial pb-lg-4 pt-lg-4">
     
    </div>
<?php $this->load->view('../includes/newfooter');?>
   


<div class="modal fade" id="signupModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Sign up</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <label class="col-form-label">Full Name <span class="error">*</span></label>
            <input type="text" name="full_name" id="full_name" class="form-control">
          </div>
          <div class="form-group">
            <label class="col-form-label">Email <span class="error">*</span></label>
            <input type="text" name="user_email" id="user_email" class="form-control">
          </div>   

          <div class="form-group">
            <label class="col-form-label">Password <span class="error">*</span></label>
            <input type="password" name="confirm_password" id="confirm_password" class="form-control">
          </div>   
          <button type="button" class="btn btn-primary" onclick="validateUser()">Join for Free</button>             
        </form>
      </div>
    </div>
  </div>
</div>   

    <!-- Optional JavaScript; choose one of the two! -->
 <script src="<?php echo BASE_PATH;?>website/js/jquery-1.12.4.min.js"></script>
    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script
      src="//code.jquery.com/jquery-3.5.1.slim.min.js"
      integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
      crossorigin="anonymous"
    ></script>

    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js" integrity="sha384-Xe+8cL9oJa6tN/veChSP7q+mnSPaj5Bcu9mPX5F5xIGE0DVittaqT5lorf0EI7Vk" crossorigin="anonymous"></script>    


    <script
      src="//cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
      integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns"
      crossorigin="anonymous"
    ></script>
    <script src="<?php echo BASE_PATH;?>website/js/jquery-1.12.4.min.js"></script>


     <script>

 // $(function () {
 //        $('[data-toggle="popover"]').popover()
 //      });
      


   function buynow(id,amount)
    {
      $.noConflict();

        jQuery.get("/coursedetails/tempbuynow/"+id+"/"+amount, function(data, status){
             console.log(data);
             parent.location= "<?php echo BASE_PATH;?>/index/checkout";
         });
    }

  </script>

  <script type="text/javascript">
    function validateUser() {
      var fn = $("#full_name").val();
      if(fn=='') {
        alert('Enter first name');
        return false;
      }

      var ue = $("#user_email").val();
      if(ue=='') {
        alert('Enter Email');
        return false;
      }

      var pwd = $("#confirm_password").val();
      if(pwd=='') {
        alert('Enter Password');
        return false;
      }

        $.post("/register/datainsert",
        {
          full_name: $("#full_name").val(),
          user_email: $("#user_email").val(),
          confirm_password: $("#confirm_password").val()
        },
        function(data, status){
          parent.location='/profile/dashboard/index';
        });

    }
  </script>
  
  
  <script>
// Feel free to change the settings on your need
    var botmanWidget = {
        frameEndpoint: 'https://chat.camsedu.com/chat.html',
        chatServer: 'https://chat.camsedu.com/chat.php',
         title: 'myeduskills',
        introMessage: 'Welcome to myeduskills<br/><br/>Hai, I am Sherry please let me know how should i address you <br><br/>Enter your name',
        placeholderText: 'Say something...',
        mainColor: '#1f3249',
        headerTextColor: '#fff',
        bubbleBackground: '#fff',
        aboutText: '',
        bubbleAvatarUrl: 'http://chat.camsedu.com/assets/portrait-asian-beautiful-smiling-woman-260nw-1214274322.jpg'
    };
        </script>
        <script src='https://chat.camsedu.com/js/widget.js'></script>
        
    <script
      type="text/javascript"
      src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"
    ></script>
    <script>
      $(document).ready(function () {
        $('.course-slick-slider').slick({
          slidesToShow: 3,
          slidesToScroll: 1,
          responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 3,
              },
            },
            {
              breakpoint: 600,
              settings: {
                slidesToShow: 2,
              },
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
              },
            },
          ],
        })
      })

  function scrolldownshortcourses(){

     $('html, body').animate({
              scrollTop: $("#shortCoursesDiv").offset().top
        }, 1500);  

        }  

  function scrolldownmicroCredentials(){
     $('html, body').animate({
              scrollTop: $("#microCredentialsDiv").offset().top
        }, 1500);  

        }  


  </script>
</html>


  </body>
</html>

  