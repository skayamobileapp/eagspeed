   <!--PAGE HEADER STARTS HERE-->

    <div class="course-lists">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-xl-12 col-lg-12 col-md-12 col-12">
            <div>
              
              <h1 class="mb-0 text-white display-4"><?php echo $cattype;?></h1>

              
            </div>
          </div>
        </div>
      </div>
    </div>

 
      <!--content-->
    <div class="pt-lg-12 pb-lg-3 pt-8 pb-6 course-card">
      <div class="container">
        <div class="row d-lg-flex align-items-center mb-4">
         
        </div>
        <div class="row">
          
                  <?php 
          // print_r($programmeList);exit;

                    for($i=0;$i<count($shortCourses);$i++) { 
          



            ?>


          <div class="col-lg-3 col-md-6 col-12">
            <div class="card mb-4 card-hover">
              <a href="/coursedetails/index/<?php echo $shortCourses[$i]->id;?>" class="card-img-top">
                     <img
                  src="<?php echo "/assets/images/".$shortCourses[$i]->course_image;?>"
                  alt
                  class="rounded-top card-img-top"
                />
              </a>
              <div class="card-body">
                <h4 class="mb-2 text-truncate-line-2" class="text-inherit">
                  <a href="/coursedetails/index/<?php echo $shortCourses[$i]->id;?>" class="text-inherit"
                    ><?php echo ucwords($shortCourses[$i]->name);?></a
                  >
                </h4>
                <!--list-->
                  <ul class="mb-2 list-inline">
                  <li class="list-inline-item">
                    <i class="fe fe-clock mr-1"></i><?php echo $shortCourses[$i]->max_duration . " - " . $shortCourses[$i]->duration_type ?>
                  </li>
                  <li class="list-inline-item">
                    <i class="fe fe-bar-chart mr-1"></i>Advance
                  </li>
                </ul>

                <?php if($shortCourses[$i]->amount>0) {?>
                <div class="1h-1">
                  <div class="d-flex">
                    <div class="h5">RM <?php echo $shortCourses[$i]->amount;?></div>
                    
                  </div>
                </div>
                <?php } else { ?>
                  <div class="1h-1">
                  <div class="d-flex">
                    <div class="h5">FREE</div>
                    
                  </div>
                </div>

                <?php } ?> 
                  <div class="d-flex">
                    <div class="">
                      <i class="fe fe-book mr-1"></i>
                                          <?php echo $shortCourses[$i]->cattype;?>

                                         </div>
                    
                  </div>
                

                <div class="d-flex mt-2">
                  <a href="/coursedetails/index/<?php echo $shortCourses[$i]->id;?>" class="btn btn-outline-primary btn-sm">More Info</a>
                 <!--  <a href="#" class="btn btn-outline-info btn-sm ml-auto"
                    >Add to Cart</a
                  > -->
                </div>
              </div>
             
            </div>
          </div>

                  <?php } ?> 



        </div>
      </div>
    </div>


  <?php $this->load->view('../includes/newfooter');?>


 <script
      src="//code.jquery.com/jquery-3.5.1.slim.min.js"
      integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
      crossorigin="anonymous"
    ></script>
    <script
      src="//cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
      integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns"
      crossorigin="anonymous"
    ></script>
   
<script src="//ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>


   <script>

    $( document ).ready(function() {


     
      var cn = '<?php echo $coursetype;?>';
        var productIds = cn;
        var courseIds;
         $.post('/coursecat/getprogramme', {productyType:productIds, courseType:courseIds}, function(response){ 
          $("#loadingicon").hide();
           $("#appendTableDiv").html(response);


        });



       
      
  });

   function buynow(id,amount)
    {
      $.noConflict();

        jQuery.get("/coursedetails/tempbuynow/"+id+"/"+amount, function(data, status){
             console.log(data);
             parent.location= "<?php echo BASE_PATH;?>/index/checkout";
         });
    }

     function getProducts()
    {

      $("#loadingicon").show();
      

         var courseIds = '0';
        $.each($("input[name='courseType[]']:checked"), function() {
          courseIds = courseIds+","+$(this).val();
        });


         var studyIds = '0';
        $.each($("input[name='studyType[]']:checked"), function() {
          studyIds = studyIds+","+$(this).val();
        });



       $.post('/course/getprogramme', {productyType:productIds, courseType:courseIds,studyType:studyIds}, function(response){ 
          $("#loadingicon").hide();
           $("#appendTableDiv").html(response);
        });
       
    }

  </script>
  


  </body>
</html>
