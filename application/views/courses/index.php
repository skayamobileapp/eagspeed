   <!--PAGE HEADER STARTS HERE-->

    <div class="course-lists">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-xl-12 col-lg-12 col-md-12 col-12">
            <div>
              <h1 class="mb-0 text-white display-4"><?php echo $productType->name;?></h1>              
            </div>
          </div>
        </div>
      </div>
    </div>


    <div class="filter-wrapper py-2">
      <div class="container">
        <div class="row align-items-center justify-content-center">
          <div class="col-md-auto filter-text">Filter By:</div>
          <div class="col-md-auto">
            <div class="btn-group filter-dropdown" id="productTypeFilter">
              <button
                type="button"
                class="btn btn-outline-secondary dropdown-toggle"
                data-toggle="dropdown"
                data-display="static"
                aria-haspopup="true"
                aria-expanded="false"
              >
                Course Type <span class="value badge badge-primary"></span>
              </button>
              <div class="dropdown-menu">
                    <?php for($i=0;$i<count($productTypeList);$i++) { ?> 

                <div class="custom-control custom-checkbox">
                  <input
                    type="checkbox"
                    class="custom-control-input"
                    value="<?php echo $productTypeList[$i]->id;?>"                    
                    id="productType<?php echo $productTypeList[$i]->id;?>"
                    name="productType[]"

                  />
                  <label class="custom-control-label" for="productType<?php echo $productTypeList[$i]->id;?>"
                    ><?php echo $productTypeList[$i]->name;?></label
                  >
                </div>
                   <?php }?> 
              
                <div class="dropdown-divider"></div>
                <button class="dropdown-item btn btn-link clear-btn">
                  Clear
                </button>
              </div>
            </div>
          </div>



           <div class="col-md-auto">
            <div class="btn-group filter-dropdown" id="categoryFilter">
              <button
                type="button"
                class="btn btn-outline-secondary dropdown-toggle"
                data-toggle="dropdown"
                data-display="static"
                aria-haspopup="true"
                aria-expanded="false"
              >
                Discipline <span class="value badge badge-primary"></span>
              </button>
              <div class="dropdown-menu">
                    <?php for($i=0;$i<count($categoryList);$i++) { ?> 

                <div class="custom-control custom-checkbox">
                  <input
                    type="checkbox"
                    class="custom-control-input"
                    value="<?php echo $categoryList[$i]->id;?>"                    
                    id="courseType<?php echo $categoryList[$i]->id;?>"
                    name="courseType[]"

                  />
                  <label class="custom-control-label" for="courseType<?php echo $categoryList[$i]->id;?>"
                    ><?php echo $categoryList[$i]->name;?></label
                  >
                </div>
                   <?php }?> 
              
                <div class="dropdown-divider"></div>
                <button class="dropdown-item btn btn-link clear-btn">
                  Clear
                </button>
              </div>
            </div>
          </div>


          <div class="col-md-auto">
            <div class="btn-group filter-dropdown" id="durationFilter">
              <button
                type="button"
                class="btn btn-outline-secondary dropdown-toggle"
                data-toggle="dropdown"
                data-display="static"
                aria-haspopup="true"
                aria-expanded="false"
              >
                Duration <span class="value badge badge-primary"></span>
              </button>
              <div class="dropdown-menu">

                <div class="custom-control custom-checkbox">
                  <input
                    type="checkbox"
                    class="custom-control-input"
                    value="0 - 1"                    
                    id="week0"
                    name="week[]"

                  />
                  <label class="custom-control-label" for="week0"
                    > Less than 1 week</label
                  >
                </div>
                 <div class="custom-control custom-checkbox">
                  <input
                    type="checkbox"
                    class="custom-control-input"
                    value="1 - 4"                    
                    id="week1"
                    name="week[]"

                  />
                  <label class="custom-control-label" for="week1"
                    > 1 - 4 Week</label
                  >
                </div>

                 <div class="custom-control custom-checkbox">
                  <input
                    type="checkbox"
                    class="custom-control-input"
                    value="4 - 12"                    
                    id="week2"
                    name="week[]"

                  />
                  <label class="custom-control-label" for="week2"
                    > 4 - 12 Week</label
                  >
                </div>

                <div class="custom-control custom-checkbox">
                  <input
                    type="checkbox"
                    class="custom-control-input"
                    value="12 - 50"                    
                    id="week3"
                    name="week[]"

                  />
                  <label class="custom-control-label" for="week3"
                    > More than 4 Week</label
                  >
                </div>

            
                <div class="dropdown-divider"></div>
                <button class="dropdown-item btn btn-link clear-btn">
                  Clear
                </button>
              </div>
              
            </div>
          </div>
        

        
          <div class="col-md-auto">
            <div class="btn-group filter-dropdown" id="studyLevelFilter">
              <button
                type="button"
                class="btn btn-outline-secondary dropdown-toggle"
                data-toggle="dropdown"
                data-display="static"
                aria-haspopup="true"
                aria-expanded="false"
              >
                Skill Level <span class="value badge badge-primary"></span>
              </button>
              <div class="dropdown-menu">
                <?php for($i=0;$i<count($studyLevelList);$i++) { ?> 

                <div class="custom-control custom-checkbox">
                  <input
                    type="checkbox"
                    class="custom-control-input"
                    value="<?php echo $studyLevelList[$i]->id;?>"                    
                    id="studyType<?php echo $studyLevelList[$i]->id;?>"
                    name="studyType[]"

                  />
                  <label class="custom-control-label" for="studyType<?php echo $studyLevelList[$i]->id;?>"
                    ><?php echo $studyLevelList[$i]->name;?></label
                  >
                </div>
                   <?php }?> 


                <div class="dropdown-divider"></div>
                <button class="dropdown-item btn btn-link clear-btn">
                  Clear
                </button>
              </div>
            </div>
          </div>


           <div class="col-md-auto">
            <div class="btn-group filter-dropdown" id="skillLevelFilter">
              <button
                type="button"
                class="btn btn-outline-secondary dropdown-toggle"
                data-toggle="dropdown"
                data-display="static"
                aria-haspopup="true"
                aria-expanded="false"
              >
                Price <span class="value badge badge-primary"></span>
              </button>
              <div class="dropdown-menu">


                <div class="custom-control custom-checkbox">
                  <input
                    type="checkbox"
                    class="custom-control-input"
                    value="0"                    
                    id="price1"
                    name="price[]"

                  />
                  <label class="custom-control-label" for="price1"
                    >Free</label
                  >
                </div>



                <div class="custom-control custom-checkbox">
                  <input
                    type="checkbox"
                    class="custom-control-input"
                    value="0 - 300"                    
                    id="price2"
                    name="price[]"

                  />
                  <label class="custom-control-label" for="price2"
                    >0 - 300</label
                  >
                </div>


                <div class="custom-control custom-checkbox">
                  <input
                    type="checkbox"
                    class="custom-control-input"
                    value="300 - 500"                    
                    id="price3"
                    name="price[]"

                  />
                  <label class="custom-control-label" for="price3"
                    >300 - 500</label
                  >
                </div>

                <div class="custom-control custom-checkbox">
                  <input
                    type="checkbox"
                    class="custom-control-input"
                    value="500 - 1000"                    
                    id="price4"
                    name="price[]"

                  />
                  <label class="custom-control-label" for="price4"
                    >500 - 1000</label
                  >
                </div>
                <div class="custom-control custom-checkbox">
                  <input
                    type="checkbox"
                    class="custom-control-input"
                    value="1001 - 2000"                    
                    id="price5"
                    name="price[]"

                  />
                  <label class="custom-control-label" for="price5"
                    >1001 - 2000</label
                  >
                </div>
                <div class="custom-control custom-checkbox">
                  <input
                    type="checkbox"
                    class="custom-control-input"
                    value="2001 - 5000"                    
                    id="price6"
                    name="price[]"

                  />
                  <label class="custom-control-label" for="price6"
                    >2001 - 5000</label
                  >
                </div>
                <div class="custom-control custom-checkbox">
                  <input
                    type="checkbox"
                    class="custom-control-input"
                    value="5001 - 10000"                    
                    id="price7"
                    name="price[]"

                  />
                  <label class="custom-control-label" for="price7"
                    >More than 5000 RM</label
                  >
                </div>


                <div class="dropdown-divider"></div>
                <button class="dropdown-item btn btn-link clear-btn">
                  Clear
                </button>
              </div>
            </div>
          </div>



          <div class="col-md-auto">
            <div class="btn-group filter-dropdown">
              <button
                type="button"
                class="btn btn-primary"
                onclick="getProducts()"
              >
                Apply Now
              </button>
            </div>
          </div>


        </div>
      </div>
    </div>
  <div class="py-6 course-lists-container">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="tab-content">
              <div class="tab-pane fade pb-4 active show" id="tabPaneGrid" role="tabpanel" aria-labelledby="tabPaneGrid">
                <div class="row pb-lg-3 pt-lg-3 course-card" id="appendTableDiv">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="pt-5" id="loadingicon" style="display: none;">
      <div class="container">
        <div class="row">
          <div class="col-lg-10 offset-lg-1 mb-5">
            <div class="card mb-3">
              <div class="card-body text-center">
                <span class="loader-icon">
                  <i class="fe fe-loader"></i>
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!--LIST ENDS  STARTS HERE-->

  <?php $this->load->view('../includes/newfooter');?>


    <script
      src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
      integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns"
      crossorigin="anonymous"
    ></script>

    <script src="/website/js/jquery-1.12.4.min.js"></script>

    <script src="/website/js/datamain.js"></script>

   


   <script>

    $( document ).ready(function() {


     
      var cn = "<?php echo $coursetype;?>";
        var productIds = cn;
        var courseIds;


        if(cn==0) {
            productIds = 0;
            courseIds = 0;
            studyIds = 0;
            amountIds = 0 - 0;
            weekIds =0;




           $.post('/course/getprogrammesummary', {productyType:productIds, courseType:courseIds,studyType:studyIds,amount:amountIds,week:weekIds}, function(response){ 
          $("#loadingicon").hide();
           $("#appendTableDiv").html(response);


            var searchname = "<?php echo $searchname;?>";
                 if(searchname!=0) {
                   $.post('/course/getprogramme', {searchname:searchname}, function(response){ 
                      $("#loadingicon").hide();
                       $("#appendTableDiv").html(response);

                         
                    });
                 }


        });


        } else {
                  alert("asdfs");

             $.post('/course/getprogramme', {productyType:productIds, courseType:courseIds}, function(response){ 
              $("#loadingicon").hide();
               $("#appendTableDiv").html(response);


                 var searchname = "<?php echo $searchname;?>";
                 alert(searchname);
                 if(searchname!=0) {
                   $.post('/course/getprogramme', {searchname:searchname}, function(response){ 
                      $("#loadingicon").hide();
                       $("#appendTableDiv").html(response);

                         
                    });
                 }


            });
       }



       
      
  });

   function buynow(id,amount)
    {
      $.noConflict();

        jQuery.get("/coursedetails/tempbuynow/"+id+"/"+amount, function(data, status){
             console.log(data);
             parent.location= "<?php echo BASE_PATH;?>/index/checkout";
         });
    }

     function getProducts()
    {


      var cn = "<?php echo $coursetype;?>";
        var productIds = cn;

      $("#loadingicon").show();
      

         var courseIds = '0';
        $.each($("input[name='courseType[]']:checked"), function() {
          courseIds = courseIds+","+$(this).val();
        });


         var productTypeIds = '0';
        $.each($("input[name='productType[]']:checked"), function() {
          productTypeIds = productTypeIds+","+$(this).val();
        });

        if(productTypeIds!='0'){
          productIds = productTypeIds;
        }


         var studyIds = '0';
        $.each($("input[name='studyType[]']:checked"), function() {
          studyIds = studyIds+","+$(this).val();
        });


          var weekIds = '-';
        $.each($("input[name='week[]']:checked"), function() {
          weekIds = weekIds+","+$(this).val();
        });



         var amountIds = '-';
        $.each($("input[name='price[]']:checked"), function() {
          amountIds = amountIds+","+$(this).val();
        });

     
       $.post('/course/getprogrammesummary', {productyType:productIds, courseType:courseIds,studyType:studyIds,amount:amountIds,week:weekIds}, function(response){ 
          $("#loadingicon").hide();
           $("#appendTableDiv").html(response);
        });
       
    }


  </script>
  


  </body>
</html>
