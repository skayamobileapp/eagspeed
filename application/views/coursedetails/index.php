<?php
$shareurl = BASE_PATH.$_SERVER['REQUEST_URI'];

?>
 <div class="pt-lg-8 pb-lg-16 pt-8 pb-12 course-wrapper">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-xl-8">

          </div>
          <div class="col-xl-8 col-lg-8 col-md-12">
            <div>
              <h1 class="text-white font-weight-semi-bold">
                                <?php echo $programmeList->name;?>

                                                  <?php if($programmeList->course_sub_type=='MQA') {?> 

                            <img src="/website/img/mqa.jpeg" style="height:80px;float: right;" />
                          <?php } ?> 

              </h1>
              <div class="text-white mb-6">
              </div>
               <div class="d-flex align-items-center mb-2">
                 <span class="text-white ml-3"
                  >
                  <i class="fe fe-clock text-white-50"></i>
                  <?php echo $programmeList->max_duration.' '.$programmeList->duration_type;?>
                </span>
                <span class="text-white ml-3"
                  >
                  <i class="fe fe-book text-white-50"></i> <?php echo substr($programmeList->pcategory, 0, 32);?>
                </span>

                  <?php if($programmeList->credit!=''){?>

                <span class="text-white ml-3"
                  >
                  <i class="fe fe-book text-white-50"></i> <?php echo 'Course Credit : '.$programmeList->credit;?>
                </span>
                <?php } ?> 
                
              </div>
              <div class="d-flex align-items-center">
            
                 
                  <span class="text-white ml-3"
                  ><i class="fe fe-book-open text-white-50"></i> <?php echo  str_pad($programmeList->progtype, 90);?>
                </span>

                  <span class="text-white ml-3"
                  ><i class="fe fe-book-open text-white-50"></i> Language of Delivery : <?php echo  $programmeList->language_of_delivery;?>
                </span>

                 
              <div class="d-flex ml-auto align-items-center">
                  <span class="text-white"
                  >Share on &nbsp;&nbsp;</span>
                 
                 <div class="fb-share-button" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button" data-size="large"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Share</a></div>


                 <a href="http://twitter.com/share?text=myeduskills&url=<?php echo $shareurl;?>" target="_blank"><button class="btn btn-light btn-sm ml-2" ><i class="fe fe-twitter"></i></button> </a>
                 <button class="btn btn-light btn-sm ml-2"><i class="fe fe-linkedin"></i></button>                  
              </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v15.0" nonce="iQZzkKhE"></script>

    <div class="pb-10">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-12 col-12 mt-n8 mb-4 mb-lg-0">
            <!-- Card -->
            <div class="card rounded-lg">
              <!-- Card header -->
              <div class="card-header border-bottom-0 p-0">
                <div>
                  <!-- Nav -->
                  <ul class="nav nav-lb-tab" id="tab" role="tablist">
                   
                    <li class="nav-item">
                      <a
                        class="nav-link active"
                        id="description-tab"
                        data-toggle="pill"
                        href="#description"
                        role="tab"
                        aria-controls="description"
                        aria-selected="false"
                        >Overview</a
                      >
                    </li>

                    

                    <li class="nav-item">
                      <a
                        class="nav-link"
                        id="objective-tab"
                        data-toggle="pill"
                        href="#objective"
                        role="tab"
                        aria-controls="objective"
                        aria-selected="false"
                        >Learning Outcomes</a
                      >
                    </li>

                     <li class="nav-item">
                      <a
                        class="nav-link"
                        id="table-tab"
                        data-toggle="pill"
                        href="#table"
                        role="tab"
                        aria-controls="table"
                        aria-selected="false"
                        >Topic</a
                      >
                    </li>
                     <li class="nav-item">
                      <a
                        class="nav-link"
                        id="faculty-tab"
                        data-toggle="pill"
                        href="#faculty"
                        role="tab"
                        aria-controls="table"
                        aria-selected="false"
                        >Instructor</a
                      >
                    </li>


<?php if($programmeList->entry_qualification!='') { ?>
                     <li class="nav-item">
                      <a
                        class="nav-link"
                        id="qualification-tab"
                        data-toggle="pill"
                        href="#qualification"
                        role="tab"
                        aria-controls="table"
                        aria-selected="false"
                        >Entry Qualification</a
                      >
                    </li>
                  <?php } ?>

                     <li class="nav-item">
                      <a
                        class="nav-link"
                        id="certificate-tab"
                        data-toggle="pill"
                        href="#certificate"
                        role="tab"
                        aria-controls="table"
                        aria-selected="false"
                        >Certificate</a
                      >
                    </li>
                 

                  </ul>
                </div>
              </div>
              <!-- Card Body -->
              <div class="card-body">
                <div class="tab-content" id="tabContent">
                  <div
                    class="tab-pane fade"
                    id="table"
                    role="tabpanel"
                    aria-labelledby="table-tab">
                    <!-- Card -->
                    <div
                      class="accordion course-accordion"
                      id="courseAccordion"
                    >
                      <div>
                        <!-- List group -->
                        <ul class="list-group list-group-flush">

                          <?php for($i=0;$i<count($topicList);$i++) {
                            $j = $i+1; ?>
                          <li class="list-group-item px-0 pt-2">
                            <!-- Toggle -->
                            <a
                              class="h4 mb-0 d-flex align-items-center text-inherit text-decoration-none"
                              data-toggle="collapse show"
                              href="#courseTwo<?php echo $topicList[$i]->id;?>"
                              aria-expanded="true"
                              aria-controls="courseTwo<?php echo $topicList[$i]->id;?>"
                            >
                              <div class="mr-auto">
                                <?php echo $j." - ".$topicList[$i]->topic_name;?>
                              </div>
                             <span class="chevron-arrow ml-4">
                                <i class="fe fe-chevron-down font-size-md"></i>
                              </span>
                            </a>
                            <!-- Row -->
                            <!-- Collapse -->
                            <div
                              class="collapse show"
                              id="courseTwo<?php echo $topicList[$i]->id;?>"
                              data-parent="#courseAccordion"
                            >
                            
                                                               


                              <div class="pt-3 pb-2">
                               
                               
                                  <div>
                                                                 <?php echo $topicList[$i]->topic_overview;?>

                                    
                                  </div>
                                      <div>
                                        <br/>
                                        <p><b>Topic Outcomes</b></p>
                                                                 <?php echo $topicList[$i]->topic_outcome;?>

                                    
                                  </div>
                               
                               
                                
                              </div>


                            </div>
                          </li>
                          <?php } ?> 
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div
                    class="tab-pane fade  show active"
                    id="description"
                    role="tabpanel"
                    aria-labelledby="description-tab">
                    <!-- Description -->
                    <div class="mb-4">
                      <?php echo $programmeList->introduction;?>
                    </div>
                    <div class="mb-4">
                      <?php echo $programmeList->synopsys;?>
                    </div>
                  </div>

                  <div
                    class="tab-pane fade"
                    id="objective"
                    role="tabpanel"
                    aria-labelledby="objective-tab">
                    <!-- Description -->
                     <div class="mb-4">
                        <table class="table">
                          <?php for($m=0;$m<count($syllabusList);$m++) {
                             $j = $m+1; ?> 
                           <tr>
                            <td><?php echo  $j." - ".ucfirst($syllabusList[$m]->clo_name);?></td>
                           
                           </tr>
                         <?php } ?> 
                        </table>
                      </div>
                  </div>

                   <div class="tab-pane fade"
                    id="faculty"
                    role="tabpanel"
                    aria-labelledby="faculty-tab">

                                  <?php for($i=0;$i<count($facultyList);$i++){?>

                      <div class="card-body instructor-details">
                      <div class="d-flex align-items-center">

                        <?php if($facultyList[$i]->image!=''){?>
                        <div class="position-relative">
                          <img
                            src="/assets/images/<?php echo $facultyList[$i]->image;?>"
                            alt=""
                            class="rounded-circle avatar-xl"
                          />
                        </div>
                      <?php } ?> 
                        <div class="ml-4">
                          <h4 class="mb-0"><b><?php echo $facultyList[$i]->salutationname;?> <?php echo $facultyList[$i]->first_name.' '.$facultyList[$i]->last_name;?></b></h4>
                           <h4 class="mb-0"><b>Designation :</b> <?php echo $facultyList[$i]->designation;?></h4>
                           

                        </div>
                      </div>

                      <br/>
                                              <h4><b>Education</b></h4>

                      <p>
                          <?php echo $facultyList[$i]->education;?>

                      </p>
                                              <h4><b>About the Instructor</b></h4>

                      <p>
                          <?php echo $facultyList[$i]->about_us;?>

                      </p>
                      
                  </div>
                   <?php } ?>
                     
                   </div>


                   <div class="tab-pane fade"
                    id="qualification"
                    role="tabpanel"
                    aria-labelledby="qualification-tab">

                                <div class="row">
                                  <div class="col-sm-12">
                                    <?php echo $programmeList->entry_qualification;?>
                                  </div>
                                </div>
                   </div>

                      <div class="tab-pane fade"
                    id="certificate"
                    role="tabpanel"
                    aria-labelledby="certificate-tab">
                                  <div class="row">
                                    <div class="col-sm-12">
                                     You are required to complete all the learning activities provided in this course. Assessments are given to evaluate your achievement of the Course Learning Outcomes. You are able to download a Certificate of Achievement/Certificate of Completion upon completion of all requisite learning materials and passing the prescribed assessments.
                                    </div>
                                  </div>

                                <div class="row">

                                   <div class="col-sm-12">

                                      <!-- List group -->
                        <ul class="list-group list-group-flush">

                          <?php for($i=0;$i<count($certificateList);$i++) {
                            $j = $i+1; ?>
                          <li class="list-group-item px-0 pt-2">
<?php echo  $j." - ".ucfirst($certificateList[$i]->name);?>
                                    <br/>
                                    <?php echo $certificateList[$i]->description;?>
                            </li>
                          <?php } ?> 
                        </ul>
                       
                      </div>
                                </div>
                   </div>

                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-12 col-12 mt-lg-n22">
            <!-- Card -->
            <div class="card mb-3 mb-4">
              <div class="p-1">
                <div
                  class="d-flex justify-content-center position-relative rounded py-10 border-white border rounded-lg bg-cover"
                  style="background-image: url(/assets/images/<?php echo $programmeList->course_image;?>)"
                >
                 
                </div>
              </div>

            </div>

         

            <div class="card mb-4">
              <div class="card-body">
                <div class="text-center">
                  <h4>Join Now</h4>

                  <?php if($feeStructureList->amount>0) { ?>
                  <h3 class="pt-2">RM <?php echo number_format((float)$feeStructureList->amount, 2, '.', '');
;?></h3>
 <?php } else { ?> 

    <h3 class="pt-2">FREE</h3>

  <?php } ?> 
                </div>
                <div class="d-flex">
                 <a href="javascript:buynow(<?php echo $feeStructureList->id_programme;?>,'<?php echo $feeStructureList->id;?>',<?php echo $feeStructureList->amount;?>)" class="btn btn-primary btn-block">Register Now</a>

                 <a href="javascript:buynow(<?php echo $feeStructureList->id_programme;?>,'<?php echo $feeStructureList->id;?>',<?php echo $feeStructureList->amount;?>)" class="btn btn-primary btn-block  ml-4 mt-0">Buy as Gift</a>

                 </div>
              </div>
            </div>

             <div class="card mb-4">
              <div>
                <!-- Card header -->
                <div class="card-header">
                  <h4 class="mb-0 h5">What’s included</h4>
                </div>
                <ul class="list-group list-group-flush">
                  <li class="list-group-item bg-transparent">
                    <i
                      class="fe fe-play-circle align-middle mr-2 text-primary"
                    ></i
                    ><?php echo count($topicList);?> Topics
                  </li>
                  <li class="list-group-item bg-transparent">
                    <i class="fe fe-award mr-2 align-middle text-success"></i
                    >Certificate
                  </li>                

                  <li class="list-group-item bg-transparent">
                    <i class="fe fe-calendar align-middle mr-2 text-info"></i>
                    <?php echo count($syllabusList);?> 
                    Learning Outcomes
                  </li>
                  <li class="list-group-item bg-transparent">
                    <i class="fe fe-video align-middle mr-2 text-secondary"></i
                    ><?php echo $programmeList->learningmodename;?>
                  </li>
                  <li class="list-group-item bg-transparent">
                    <i class="fe fe-clock align-middle mr-2 text-secondary"></i
                    ><?php echo $programmeList->max_duration.' '.$programmeList->duration_type;?> Duration
                  </li>


                  
                  
                </ul>
              </div>
            </div>


           
          </div>
        </div>
      </div>
    </div>
    <!-- COURSE DETAILS STARTS HERE-->

   

   
  <?php $this->load->view('../includes/newfooter');?>


  <script
      src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
      integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns"
      crossorigin="anonymous"
    ></script>

    <script src="/website/js/jquery-1.12.4.min.js"></script>


 <script>

   function buynow(id,feestructure,amount)
    {
      $.noConflict();

       jQuery.post('/coursedetails/tempbuynow', {id_programme:id, id_feestructure:feestructure,amount:amount}, function(response){ 
             parent.location= "<?php echo BASE_PATH;?>/index/checkout";
        });

    }


   

  </script>



