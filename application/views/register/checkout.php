<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    
    <!-- Title-->
    <title>Complete education theme for School, College, University, E-learning</title>
    
    <!-- SEO Meta-->
    <meta name="description" content="Education theme by EchoTheme">
    <meta name="keywords" content="HTML5 Education theme, responsive HTML5 theme, bootstrap 4, Clean Theme">
    <meta name="author" content="education">
    
    <!-- viewport scale-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    
            
    <!-- Favicon and Apple Icons-->
    <link rel="icon" type="image/x-icon" href="<?php echo BASE_PATH;?>website/img/favicon/favicon.ico">
    <link rel="shortcut icon" href="<?php echo BASE_PATH;?>website/img/favicon/114x114.png">
    <link rel="apple-touch-icon-precomposed" href="<?php echo BASE_PATH;?>website/img/favicon/96x96.png">
    
    
    <!--Google fonts-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Maven+Pro:400,500,700%7CWork+Sans:400,500">
    
    
    <!-- Icon fonts -->
    <link rel="stylesheet" href="<?php echo BASE_PATH;?>website/fonts/fontawesome/css/all.css">
    <link rel="stylesheet" href="<?php echo BASE_PATH;?>website/fonts/themify-icons/css/themify-icons.css">
    
    
    <!-- stylesheet-->    
    <link rel="stylesheet" href="<?php echo BASE_PATH;?>website/css/vendors.bundle.css">
    <link rel="stylesheet" href="<?php echo BASE_PATH;?>website/css/style.css">
    
  </head>
  
  <body>
   
  <header class="site-header bg-dark text-white-0_5">        
    <div class="container">
      <div class="row align-items-center justify-content-between mx-0"  style="color:white;">
        <ul class="list-inline d-none d-lg-block mb-0">
          <li class="list-inline-item mr-3">
           <div class="d-flex align-items-center">
            <i class="ti-email mr-2"></i>
            <a href="mailto:support@educati.com">support@educati.com</a>
           </div>
          </li>
          <li class="list-inline-item mr-3">
           <div class="d-flex align-items-center">
            <i class="ti-headphone mr-2"></i>
            <a href="tel:+8801740411513">+8801740411513</a>
           </div>
          </li>
        </ul>
        <ul class="list-inline mb-0">
          <li class="list-inline-item mr-0 p-3 border-right border-left border-white-0_1">
            <a href="#"><i class="ti-facebook"></i></a>
          </li>
          <li class="list-inline-item mr-0 p-3 border-right border-white-0_1">
            <a href="#"><i class="ti-twitter"></i></a>
          </li>
          <li class="list-inline-item mr-0 p-3 border-right border-white-0_1">
            <a href="#"><i class="ti-vimeo"></i></a>
          </li>
          <li class="list-inline-item mr-0 p-3 border-right border-white-0_1">
            <a href="#"><i class="ti-linkedin"></i></a>
          </li>
        </ul>
        <ul class="list-inline mb-0">
          <li class="list-inline-item mr-0 p-md-3 p-2 border-right border-left border-white-0_1">
            <a href="page-login.html">Login</a>
          </li>
          <li class="list-inline-item mr-0 p-md-3 p-2 border-right border-white-0_1">
            <a href="page-signup.html">Register</a>
          </li>
        </ul>
      </div> <!-- END END row-->
    </div> <!-- END container-->
  </header><!-- END site header-->
  
  

  <nav class="ec-nav sticky-top bg-white">
  <div class="container">
    <div class="navbar p-0 navbar-expand-lg">
     <div class="navbar-brand">
        <a class="logo-default" href="index.html"><img alt="" src="<?php echo BASE_PATH;?>website/images/SPEED.svg"></a>
      </div>
      <span aria-expanded="false" class="navbar-toggler ml-auto collapsed" data-target="#ec-nav__collapsible" data-toggle="collapse">
        <div class="hamburger hamburger--spin js-hamburger">
          <div class="hamburger-box">
            <div class="hamburger-inner"></div>
          </div>
        </div>
      </span>
       <div class="collapse navbar-collapse when-collapsed" id="ec-nav__collapsible">
        <ul class="nav navbar-nav ec-nav__navbar ml-auto">

            <li class="nav-item nav-item__has-megamenu megamenu-col-2">
              <a class="nav-link" href="#" data-toggle="dropdown">Home</a>
            </li>

             <li class="nav-item nav-item__has-megamenu megamenu-col-2">
              <a class="nav-link" href="#" data-toggle="dropdown">What are Microcredentials</a>
            </li>

                  <li class="nav-item nav-item__has-dropdown">
                <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">Courses Offered</a>
                <ul class="dropdown-menu">
                  <li><a href="page-sp-all-courcess.html" class="nav-link__list">Management and Leadership</a></li>
                  <li><a href="page-sp-all-courcess-list.html" class="nav-link__list">Early Childhood Education</a></li>
                  <li><a href="page-sp-all-courcess-with-sidebar.html" class="nav-link__list">              Kursus Asuhan PERMATA
</a></li>
                 
                </ul>
            </li>


            <li class="nav-item nav-item__has-megamenu megamenu-col-2">
              <a class="nav-link" href="#" data-toggle="dropdown">What is SPEED</a>
            </li>

            <li class="nav-item nav-item__has-megamenu megamenu-col-2">
              <a class="nav-link" href="#" data-toggle="dropdown">Contact Us</a>
            </li>

        </ul>
      </div>    
    </div>
  </div> <!-- END container-->    
  </nav> <!-- END ec-nav -->    

<div class="py-5 bg-light-v2">
  <div class="container">
   <div class="row align-items-center">
     <div class="col-md-6">
       <h2>Checkout </h2>
     </div>
     <div class="col-md-6">
      <ol class="breadcrumb justify-content-md-end bg-transparent">  
        <li class="breadcrumb-item">
          <a href="#">Home</a>
        </li> 
        <li class="breadcrumb-item">
          <a href="#"> Checkout </a>
        </li>
      </ol>
     </div>
   </div>
  </div> 
</div>


  <section class="padding-y-10">
  <div class="container">
   <div class="row">
    
     <div class="col-12">
       <div class="table-responsive">
        <table class="table table-bordered">
        <thead>
          <tr>
            <th scope="col">Product Name</th>
            <th scope="col">Price</th>
            <th scope="col">Quantity</th>
            <th scope="col">Subtotal</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>

          <tr>
            <td class="p-4">
            <span class="d-inline-block width-7rem border p-3 mr-3">
             <img src="assets/img/shop/products/1.jpg" alt="">
            </span>
              <a href="#">Bootstrap Referance Guide</a>
            </td>
            <td>$250.00</td>
            <td class="text-center">
            <div class="input-group ec-touchspin width-7rem mx-auto">
              <div class="ec-touchspin__minus input-group-prepend">
                <span class="input-group-text ti ti-minus bg-white p-2"></span>
              </div>
              <input class="ec-touchspin__result form-control bg-white text-center p-1" type="text" value="1">
              <div class="input-group-append">
                <span class="ec-touchspin__plus input-group-text ti-plus bg-white p-2"></span>
              </div>
            </div>
            </td>
            <td>$250.00</td>
            <td class="text-center">
              <a href="#"><i class="ti-close"></i></a>
            </td>
          </tr>


          <tr>
          <td colspan="3" class="p-4">
            <form class="form-inline">
              <input type="text" class="form-control" placeholder="Promocode" required>
              <button type="submit" class="btn btn-primary ml-2">Submit</button>
            </form>
          </td>
          <td colspan="3">
            Total: <span class="font-weight-semiBold font-size-18">$500.00</span>
          </td>
          </tr>
        </tbody>
      </table>
      </div>
     </div> <!-- END col-12 -->
     
     <div class="col-md-6 mt-4">
       <a href="shop.html" class="btn btn-outline-light btn-icon"> <i class="ti-angle-double-left mr-2"></i> Back to shopping</a>
     </div> <!-- END col-md-6 -->
     <div class="col-md-6 mt-4 text-right">
       <button href="shop.html" class="btn btn-outline-light">Update cart</button>
       <button href="shop.html" class="btn btn-primary ml-3">Checkout</button>
     </div> <!-- END col-md-6 -->
   </div> <!-- END row-->  
  </div> <!-- END container-->
</section>
