
   
 <div class="container d-flex flex-column">
            <div
        class="row align-items-center justify-content-center no-gutters" style="min-height: 69vh!important;"
      >
        <div class="col-lg-5 col-md-8 py-8 py-xl-0">
          <!-- Card -->
        <form action="#" method="POST" class="px-lg-4">

          <div class="card shadow">
            <!-- Card body -->
            <div class="card-body p-6">
              <div class="mb-4">
             
                <h2 class="mb-1 font-weight-bold" style="text-align:center;">Register</h2>
                
              </div>
              <!-- Form -->
              <form>
               <div class="form-group">
                  <label  class="form-label">Full Name</label>
                  <input type="text" id="full_name" class="form-control" name="full_name" placeholder="Enter Full Name" >
                </div>
                <div class="form-group">
                  <label class="form-label">Email</label>
                  <input type="text" id="user_email" class="form-control" name="user_email" placeholder="Enter Email Address" >
                </div>
              
                <!-- Password -->
                <div class="form-group">
                  <label for="password" class="form-label">Password</label>
                  <input type="password" id="confirm_password" class="form-control" name="confirm_password" placeholder="**************" >
                </div>
                <div class="custom-control custom-checkbox">
                    <input
                      type="checkbox"
                      class="custom-control-input"
                      id="rememberme"
                    />
                    <label class="custom-control-label" for="rememberme"
                      >I agree to the Terms of Service and Privacy Policy</label
                    >
                  </div>

                  <button type="submit" class="btn btn-primary btn-block" >
                    Register
                  </button>
         

                  <div class="text-center mt-2">Existing User?
                  <a href="/login/index" class="ml-1 text-nowrap" onclick="showLoginform()">Click to Login</a>
                  </div>
              
                  <!-- Button -->
                
                </div>
                <div>
                 
                </div>
              </form>
            </div>
          </div>
        </form>
        </div>
      </div>
    </div>
<?php $this->load->view('../includes/newfooter');?>
