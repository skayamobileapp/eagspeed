
   
 <div class="container d-flex flex-column">
      <div
        class="row align-items-center justify-content-center no-gutters" style="min-height: 69vh!important;"
      >
        <div class="col-lg-5 col-md-8 py-8 py-xl-0">
          <!-- Card -->
        <form action="#" method="POST" class="px-lg-4">

          <div class="card shadow">
            <!-- Card body -->
            <div class="card-body p-6">
              <div class="mb-4">
                <h2 class="mb-1 font-weight-bold" style="text-align: center;">Forgot Password</h2>
                
              </div>
              <!-- Form -->
              <form>
                <!-- Username -->
                <div class="form-group">
                  <label for="email" class="form-label"
                    >Username or email</label
                  >
                  <input
                    type="email"
                    id="email"
                    class="form-control"
                    name="email"
                    placeholder="Email address here"
                    required=""
                  />
                </div>
              
                <!-- Checkbox -->
                <div
                  class="d-lg-flex justify-content-between align-items-center mb-4"
                >
                  
                  <div>
                    <a href="/login">Click to Login</a><br/>
                  
                  </div>
                </div>
                <div>
                  <!-- Button -->
                  <button type="submit" class="btn btn-primary btn-block">
                    Sign in
                  </button>
                </div>
              </form>
            </div>
          </div>
        </form>
        </div>
      </div>
    </div>
<?php $this->load->view('../includes/newfooter');?>
