<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class : Login (LoginController)
 * Login class to control to authenticate user credentials and starts user's session.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Index extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        error_reporting(0);

        $this->load->model('register_model');
        $this->load->model('login_model');

        parent::__construct();
    }

    /**
     * Index Page for this controller.
     */
    public function index()
    {
           $data = array();

     $data['productList'] = $this->register_model->productActiveTypeList();

     $data['shortstep'] = $this->register_model->productActiveTypeListById(1);
     $data['microstep'] = $this->register_model->productActiveTypeListById(3);
     $data['professionalstep'] = $this->register_model->productActiveTypeListById(4);


          $data['freeprogrammeList'] = $this->register_model->programmefree();


     $data['shortCourses'] = $this->register_model->programmeByCategory(3);

     $data['academicCourse'] = $this->register_model->programmeByCategory(4);
     $data['socialCourse'] = $this->register_model->programmeByCategory(7);
     $data['microCourse'] = $this->register_model->programmeByCategory(2);

     $data['prorammetypeone'] = $this->register_model->programmeByType(1);
     $data['prorammetypetwo'] = $this->register_model->programmeByType(2);
     $data['prorammetypethree'] = $this->register_model->programmeByType(3);


     $data['about'] = $this->register_model->about();
     $data['myeduskills'] = $this->register_model->eduskills();
     $data['professionalCourse'] = array();

        $this->loadViews('index/index',$this->global,$data,NULL);
        
    }


    public function popup(){


        $full_name = strtolower($this->security->xss_clean($this->input->post('full_name')));
            $user_email = strtolower($this->security->xss_clean($this->input->post('user_email')));
            $nric = strtolower($this->security->xss_clean($this->input->post('nric')));
            $rawpassword = $this->security->xss_clean($this->input->post('confirm_password'));

            $passworddec = md5($rawpassword);
            $student = array(
                'full_name' =>$full_name,
                'email_id' =>$user_email,
                'nric' =>$nric,
                'password' =>$passworddec,
            );
            $this->login_model->insertStudent($student);
            $result = $this->login_model->loginMe($user_email, $rawpassword);

            $functionName = 'core_user_create_users';
                $rand = rand(00000000,999999999);

                $user1 = new stdClass();
                $user1->username = $nric;
                $user1->password = 'Abc12345678*';
                $user1->firstname = $full_name;
                $user1->lastname = $full_name;
                $user1->email = $user_email;


                $users = array($user1);
                $params = array('users' => $users);

                /// REST CALL
                $restformat = "json";
                $serverurl = 'https://lms.myeduskills.com/webservice/rest/server.php?wstoken='.TOKEN.'&wsfunction=' . $functionName. '&moodlewsrestformat=' . $restformat;
                require_once ('curl.php');
                $curl = new curl();


                $resp = $curl->post($serverurl, $params);


                ///echo '</br>************************** Server Response    createUser()**************************</br></br>';
                ///echo $serverurl . '</br></br>';

                $responseArray = json_decode($resp);
                print_R($responseArray);exit;
                //print_R($responseArray[0]->id);
                 if($responseArray->errorcode=='') {
                    $studentdata = array();
                    $studentdata['moodle_id'] = $responseArray[0]->id;
                    $this->login_model->editStudent($studentdata, $result->id);
                }


                $sessionArray = array(
                    'id_student'=>$result->id,
                    'student_name'=>$result->full_name,
                    'student_first_name'=>$result->first_name,
                    'student_last_name'=>$result->last_name,
                    'student_email'=>$result->email_id,
                    'studentLoggedIn' => TRUE
                    );

                $this->session->set_userdata($sessionArray);

                unset($sessionArray['id_student'], $sessionArray['isStudentLoggedIn'], $sessionArray['student_last_login']);

                $loginInfo = array("id_student"=>$result->id, "session_data" => json_encode($sessionArray), "machine_ip"=>$_SERVER['REMOTE_ADDR'], "user_agent"=>getBrowserAgent(), "agent_string"=>$this->agent->agent_string(), "platform"=>$this->agent->platform());


                $uniqueId = rand(0000000000,9999999999);
                $this->session->set_userdata("student_session_id", md5($uniqueId));

                $this->login_model->addStudentLastLogin($loginInfo);

                                redirect('/profile/dashboard/index');



    }
    // public function refund()
    // {
    //     $this->loadViews('index/refund',NULL,NULL,NULL);
    // }


    // public function terms()
    // {
    //     $this->loadViews('index/terms',NULL,NULL,NULL);
    // }

       public function about()
    {
        $this->loadViews('index/about',NULL,NULL,NULL);
    }


     public function login()
    {
        $this->load->view('index/login');
    }

    public function logout()
    {

        $sessionArray = array(
            'id_student'=> '',
            'student_name'=> '',
            'student_first_name'=> '',
            'student_last_name'=> '',
            'student_email'=> '',
            'studentLoggedIn' => FALSE
            );
        
        $this->session->set_userdata($sessionArray);
        
        redirect('index');
    }


    public function cart() {
       $id_session = session_id();

  $listOfCourses = $this->register_model->gerProgrammeFromSession($id_session);


  $staffDiscount = $this->register_model->getDiscountFromStaff($id_session);


        $table.="<table class='table mb-0'>
                    <tr>";
                   $totalAmount=0;
                for($l=0;$l<count($listOfCourses);$l++) {
                $totalAmount =  $totalAmount + $listOfCourses[$l]->amount;
                $idprogramme = $listOfCourses[$l]->id;
                $indAmount = number_format($listOfCourses[$l]->amount,2);
                                    $durationofprog = $programmeList[$i]->max_duration.' '.$programmeList[$i]->duration_type;

                $programmeName = $listOfCourses[$l]->name;
                $image = $listOfCourses[$l]->course_image;
                $tempid = $listOfCourses[$l]->tempid;
                if($indAmount>0) {
                   $indAmountDisplay=" RM $indAmount";
                } else {
                    $indAmountDisplay = 'FREE';
                }
                $table.="<tr>
                  <td>
                    <a href='/coursedetails/index/$idprogramme' class='card-img-top'>
                       
                    <img
                      src='/assets/images/$image'
                      width='200'
                      class='img-responsive'
                    />
                  </a>
                  </td>
                  <td><a href='/coursedetails/index/$idprogramme' class='card-img-top'>
<p>$programmeName</p> <br/> $durationofprog </a>
                  </td>
                  <td>
                    <a href='javascript:deletecourse($tempid)'><i class='fe fe-trash'></i></a>
                  </td>
                  <td>
                    <p class='h5'>$indAmountDisplay</p>
                  </td>
                </tr>";
              } 

              if($staffDiscount[0]->amount>0) { 
                $discountAmount = number_format($staffDiscount[0]->amount,2);

                $table.="<tr>
                     <td colspan='3' style='text-align: right;'>Discount</td>
                     <td><p class='h5'>RM $discountAmount </p></td>
                 </tr>";
             }

                $totalAmount = $totalAmount- $discountAmount;
                $totalAmount = number_format($totalAmount,2);

                if($totalAmount>0) {
                    $totalAmountDisplay = "RM $totalAmount";
                } else {
                    $totalAmountDisplay = "FREE";

                }
                $table.="<tr>
                     <td colspan='3' style='text-align: right;'>Total</td>
                     <td><p class='h5'>$totalAmountDisplay</p></td>
                 </tr>
               
                </table>";
        echo $table;
    }


    public function checkdiscount($type,$value) {
       $id_session = session_id();


       $this->register_model->deletealldiscount($id_session);

        $listOfCourses = $this->register_model->gerProgrammeFromSession($id_session);
          $idprogramme = $listOfCourses[0]->id;



        if($type=='Staff') {
           $codeDetails = $this->register_model->checkemployer($value);

          if($codeDetails) {
            $getDiscountDetails = $this->register_model->getDiscountAmountByProgrammeStaff($idprogramme,$type);
                        $data['amount'] = $getDiscountDetails->staff_value;
                    }

        } else {
            $codeDetails = $this->register_model->checkstudentaeu($value);
             if($codeDetails) {
          $getDiscountDetails = $this->register_model->getDiscountAmountByProgrammeStudent($idprogramme,$type);
                      $data['amount'] = $getDiscountDetails->student_value;
                    }


        }

    


        if($data['amount']>0) {

            //fee_structure_main
            $data['id_session'] = session_id();
            //check if the id already exist 
            $idpresent = $this->register_model->checkiddiscount($data['id_session']);
            if($idpresent) {
            } else {
            $this->register_model->addtotempdiscount($data);
            }


        }

             $discountDetails = $this->register_model->getDataFromSession($id_session);
          echo $discountDetails->amount;
          exit;


    }


    public function checkcouponcode($code){
  //fee_structure_main
            $data['id_session'] = $id_session = session_id();

             $this->register_model->deletealldiscount($id_session);


            $data['id_session'] = session_id();
            //check if the id already exist 
            $codeDetails = $this->register_model->checkvalidcode($code);

            if($codeDetails->id) {
                  $idpresent = $this->register_model->checkdiscountcodeadded($codeDetails->id,$data['id_session']);
                if($idpresent) {
                } else {
                  $data['amount'] = $codeDetails->amount;
                  $data['id_discount'] = $codeDetails->id;

                  $this->register_model->addtotempdiscount($data);
                }

            }

            $discountDetails = $this->register_model->getDataFromSession($id_session);
          echo $discountDetails->amount;
          exit;

            
    }

     public function checkout() {

       $id_session = session_id();


        $data['listOfCourses'] = $this->register_model->gerProgrammeFromSession($id_session);
  $id_session = session_id();

             $this->register_model->deletealldiscount($id_session);

        $this->loadViews('index/checkout',$this->global,$data,NULL);

    }


    public function register() {


             $this->id_student =  $this->session->userdata['id_student'];

            if(!empty($this->id_student)){
                    redirect('/profile/dashboard/paymentgateway');

            } 
            

           $id_session = session_id();

           if($_POST) {


             $this->id_student =  $this->session->userdata['id_student'];

            if(!empty($this->id_student)){
                    redirect('/profile/dashboard/paymentgateway');

            } 

            if($_POST['full_name']) {
                $full_name = strtolower($this->security->xss_clean($this->input->post('full_name')));
                $user_email = strtolower($this->security->xss_clean($this->input->post('user_email')));
                $nric = strtolower($this->security->xss_clean($this->input->post('nric')));
                $rawpassword = $this->security->xss_clean($this->input->post('confirm_password'));

                $passworddec = md5($rawpassword);



                $student = array(
                    'full_name' =>$full_name,
                    'email_id' =>$user_email,
                    'nric' =>$nric,
                    'password' =>$passworddec,
                );
                $this->login_model->insertStudent($student);
                $result = $this->login_model->loginMe($user_email, $rawpassword);

                $functionName = 'core_user_create_users';
                    $rand = rand(00000000,999999999);

                    $user1 = new stdClass();
                    $user1->username = $user_email;
                    $user1->password = 'Abc12345678*';
                    $user1->firstname = $full_name;
                    $user1->lastname = $full_name;
                    $user1->email = $user_email;


                    $users = array($user1);
                    $params = array('users' => $users);


                    /// REST CALL
                    $restformat = "json";
                   $serverurl = 'https://lms.myeduskills.com/webservice/rest/server.php?wstoken='.TOKEN.'&wsfunction=' . $functionName. '&moodlewsrestformat=' . $restformat;
                    require_once ('curl.php');
                    $curl = new curl();


                    $resp = $curl->post($serverurl, $params);


                // echo '</br>************************** Server Response    createUser()**************************</br></br>';
                // echo $serverurl . '</br></br>';

                    $responseArray = json_decode($resp);
                 // print_R($responseArray);exit;
                    //print_R($responseArray[0]->id);
                     if($responseArray->errorcode=='') {
                        $studentdata = array();
                        $studentdata['moodle_id'] = $responseArray[0]->id;
                        $this->login_model->editStudent($studentdata, $result->id);
                    }



            } else {


                $email = strtolower($this->security->xss_clean($this->input->post('email')));
                $password = $this->input->post('password');



                $result = $this->login_model->loginMe($email, $password);
            }



                if($result)
                {
                    $sessionArray = array(
                        'id_student'=>$result->id,
                        'student_name'=>$result->full_name,
                        'student_first_name'=>$result->first_name,
                        'student_last_name'=>$result->last_name,
                        'student_email'=>$result->email_id,
                        'studentLoggedIn' => TRUE
                        );

                    $this->session->set_userdata($sessionArray);

                    unset($sessionArray['id_student'], $sessionArray['isStudentLoggedIn'], $sessionArray['student_last_login']);

                    $loginInfo = array("id_student"=>$result->id, "session_data" => json_encode($sessionArray), "machine_ip"=>$_SERVER['REMOTE_ADDR'], "user_agent"=>getBrowserAgent(), "agent_string"=>$this->agent->agent_string(), "platform"=>$this->agent->platform());


                    $uniqueId = rand(0000000000,9999999999);
                    $this->session->set_userdata("student_session_id", md5($uniqueId));

                    $this->login_model->addStudentLastLogin($loginInfo);

                    redirect('/profile/dashboard/paymentgateway');

               }
           }

            $data['listOfCourses'] = $this->register_model->gerProgrammeFromSession($id_session);

                 $this->register_model->deletealldiscount($id_session);

            $this->loadViews('index/register',$this->global,$data,NULL);

    }


      public function registergift() {

           $id_session = session_id();

           

           if($_POST) {


                $email = strtolower($this->security->xss_clean($this->input->post('email')));
                $name = strtolower($this->security->xss_clean($this->input->post('name')));
                $nric = strtolower($this->security->xss_clean($this->input->post('nric')));


                $student = array(
                    'email' =>$email,
                    'name' =>$name,
                    'nric' =>$nric,
                    'session_id' =>$id_session,
                );
               
                    $this->login_model->addgiftDatatemp($student);

                                   redirect('/index/register');

           }

                 $this->register_model->deletealltempregister($id_session);

            $this->loadViews('index/registergift',$this->global,$data,NULL);

    }


    public function values() {
        $data['awardDetails'] = $this->register_model->getCms(1,'value');
            $this->loadViews('index/values',$this->global,$data,NULL);

    }

    public function privacypolicy() {
        $data['awardDetails'] = $this->register_model->getCms(1,'privacy');
            $this->loadViews('index/privacypolicy',$this->global,$data,NULL);

    }

    public function termsandcondition() {
        $data['awardDetails'] = $this->register_model->getCms(1,'terms_and_condition');
            $this->loadViews('index/termsandcondition',$this->global,$data,NULL);

    }

    public function refundpolicy() {
        $data['awardDetails'] = $this->register_model->getCms(1,'refund');
            $this->loadViews('index/refundpolicy',$this->global,$data,NULL);

    }

    public function faq() {
        $data['awardDetails'] = $this->register_model->getCms(1,'faq');
            $this->loadViews('index/faq',$this->global,$data,NULL);

    }

    public function support() {
        $data['awardDetails'] = $this->register_model->getCms(1,'support');
            $this->loadViews('index/support',$this->global,$data,NULL);

    }

    public function helpdesk() {
        $data['awardDetails'] = $this->register_model->getCms(1,'helpdesk');
            $this->loadViews('index/helpdesk',$this->global,$data,NULL);

    }

    public function learningguide() {
        $data['awardDetails'] = $this->register_model->getCms(1,'learning_guides');
            $this->loadViews('index/learning_guides',$this->global,$data,NULL);

    }

    public function becomeourpartner() {
        $data['awardDetails'] = $this->register_model->getCms(1,'become_our_partner');
            $this->loadViews('index/becomeourpartner',$this->global,$data,NULL);

    }

    public function usingofplatform() {
        $data['awardDetails'] = $this->register_model->getCms(1,'using_of_platform');
            $this->loadViews('index/usingofplatform',$this->global,$data,NULL);

    }

    public function partner() {
        $data['awardDetails'] = $this->register_model->getCms(1,'partner');
            $this->loadViews('index/partner',$this->global,$data,NULL);

    }

 public function subscription() {
        $data['awardDetails'] = $this->register_model->getCms(1,'subscription');
            $this->loadViews('index/subscription',$this->global,$data,NULL);

    }

    
    
}