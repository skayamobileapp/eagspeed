<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class : Login (LoginController)
 * Login class to control to authenticate user credentials and starts user's session.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Coursecat extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        $this->load->model('register_model');
        error_reporting(0);
        parent::__construct();
    }

    /**
     * Index Page for this controller.
     */
    public function index($id=null,$type=null)
    {
       

  $data['shortCourses'] = $this->register_model->programmecat($id);


   $data['cattype'] = $data['shortCourses'][0]->cattype;

        $data['categoryList'] = $this->register_model->getCategoryList();

        $data['studyLevelList'] = $this->register_model->getStudyLevel();

        $data['coursetype'] = $id;
       
      $this->loadViews('coursecat/index',$this->global,$data,NULL);
    }


    public function category()
    {

    }

    public function downloadcsv()
    {
        $programmeList = $this->register_model->programmeForCourse();

     // echo "<Pre>";print_r($programmeList);exit();

        $filename = "auditResult.csv";
        $fp = fopen('php://output', 'w');
        $header = array();


        array_push($header, 'Name');
        array_push($header, 'Short Description');
        array_push($header, 'Staff');

        header('Content-type: application/csv');
        header('Content-Disposition: attachment; filename=' . $filename);
        fputcsv($fp, $header);


        for($i=0;$i<count($programmeList);$i++)
        {
          $newarray = array();
          array_push($newarray, $programmeList[$i]->name);
          array_push($newarray, $programmeList[$i]->short_description);
          array_push($newarray, $programmeList[$i]->staffname);
          fputcsv($fp, $newarray);
        }

      exit;
    }

    public function courseList() {


        
    }

    public function getprogramme() {

        $programmeType = $_POST['productyType'];
        $courseType = $_POST['courseType'];
        $studyType = $_POST['studyType'];

        




        if($_POST['productname']) {

          if(is_numeric($_POST['productname'])) {
            $programmeList = $this->register_model->getAllByCategories($_POST['productname']);

          } else {
            $programmeList = $this->register_model->programmeForCourseByFilterByName($_POST['productname']);

          }
        } else {
            $programmeList = $this->register_model->programmeForCourseByFilter($programmeType,$courseType,$studyType);
        }

        if($_POST['searchname']!='') {
                      $programmeList = $this->register_model->programmeForCourseByFilterByName($_POST['searchname']);

        }


        $table="";
                

                  for($i=0;$i<count($programmeList);$i++) {

                    $idprogramme  = $programmeList[$i]->id;
                    $programmeName = $programmeList[$i]->name;
                    $programmeimage = $programmeList[$i]->course_image;
                    $durationofprog = $programmeList[$i]->max_duration.' '.$programmeList[$i]->duration_type;
                    $amount = number_format($programmeList[$i]->amount,2);
                    $staffimage = $programmeList[$i]->staffimage;
                    $staffname = $programmeList[$i]->staffname;
                    $categoryname = $programmeList[$i]->categoryname;
                    $cattype = $programmeList[$i]->cattype;
                    $studylevel = $programmeList[$i]->studylevel;
                    if($amount>0) {
                      $amount = "RM ".$amount;
                    } else {
                      $amount = "FREE";
                    }
                    
                  
                  $table.="<div class='col-lg-3 col-md-6 col-12 ' style='padding-top:25px;'>
                  <div class='card mb-4 card-hover'>
                    <a href='/coursedetails/index/$idprogramme' class='text-inherit'>
                           <img
                        src='/assets/images/$programmeimage'
                        alt
                        class='rounded-top card-img-top'
                      />
                  </a>
                  <div class='card-body'>
                    <h4 class='mb-2 text-truncate-line-2' class='text-inherit'>
                    <a href='/coursedetails/index/$idprogramme' class='text-inherit'>
                        $programmeName</a
                      >
                    </h4>
                   
                    <ul class='mb-2 list-inline'>
                  <li class='list-inline-item'>
                    <i class='fe fe-clock mr-1'></i>$durationofprog
                  </li>
                  <li class='list-inline-item'>
                    <i class='fe fe-bar-chart mr-1'></i>$studylevel
                  </li>
                </ul>

                 <div class='1h-1'>
                  <div class='d-flex'>
                    <div class='h5'>$amount</div>
                    
                  </div>
                </div>
                  <div class='d-flex'>
                    <div class=''>
                      <i class='fe fe-book mr-1'></i>
                                          $categoryname

                                         </div>
                    
                  </div>


                  ";
                     if($id_category=='2') {
                       $table.="<div class='d-flex mt-2'>
                        <a href='/programdetails/index/$idprogramme' class='btn btn-outline-primary btn-sm'>View Details</a>
                     
                    </div>";
                     }
                     else {
                       $table.="<div class='d-flex mt-2'>
                        <a href='/coursedetails/index/$idprogramme' class='btn btn-outline-primary btn-sm'>
                        More Info</a
                      >
                     
                    </div>";

                     }
                    

                    
                  $table.="</div>
                  <!--footer card-->
                 
                </div>
              </div>";             
                 }
                
        echo $table;
        exit;
    }

    
}