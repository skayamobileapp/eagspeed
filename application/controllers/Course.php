<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class : Login (LoginController)
 * Login class to control to authenticate user credentials and starts user's session.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Course extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        $this->load->model('register_model');
        error_reporting(0);
        parent::__construct();
    }

    /**
     * Index Page for this controller.
     */
    public function index($id=null,$type=null)
    {
        // $this->downloadcsv();



        $data = array();
        $data = array();

        if($_POST['search']!='') {
          $data['searchname'] = $_POST['search'];
        } else {
           $data['searchname'] = 0 ;
        }




        $data['productType'] = $this->register_model->getProductTypeById($id);


        $data['categoryList'] = $this->register_model->getCategoryList();

        $data['productTypeList'] = $this->register_model->getProductType();

        $data['studyLevelList'] = $this->register_model->getStudyLevel();

        $data['coursetype'] = $id;
       
      $this->loadViews('courses/index',$this->global,$data,NULL);
    }


     /**
     * Index Page for this controller.
     */
    public function summary($id=null,$type=null)
    {
        // $this->downloadcsv();

        $data = array();
        $data = array();

        if($_POST['search']!='') {
          $data['searchname'] = $_POST['search'];
        } else {
           $data['searchname'] = 0 ;
        }


        $data['productType'] = $this->register_model->getProductTypeByIdNew($id);


        $data['categoryList'] = $this->register_model->getCategoryList();

        $data['productTypeList'] = $this->register_model->getProductType();

        $data['studyLevelList'] = $this->register_model->getStudyLevel();

        $data['coursetype'] = $id;
       
      $this->loadViews('courses/summary',$this->global,$data,NULL);
    }



    public function category()
    {

    }

    public function downloadcsv()
    {
        $programmeList = $this->register_model->programmeForCourse();

     // echo "<Pre>";print_r($programmeList);exit();

        $filename = "auditResult.csv";
        $fp = fopen('php://output', 'w');
        $header = array();


        array_push($header, 'Name');
        array_push($header, 'Short Description');
        array_push($header, 'Staff');

        header('Content-type: application/csv');
        header('Content-Disposition: attachment; filename=' . $filename);
        fputcsv($fp, $header);


        for($i=0;$i<count($programmeList);$i++)
        {
          $newarray = array();
          array_push($newarray, $programmeList[$i]->name);
          array_push($newarray, $programmeList[$i]->short_description);
          array_push($newarray, $programmeList[$i]->staffname);
          fputcsv($fp, $newarray);
        }

      exit;
    }

    public function courseList() {


        
    }

    public function getprogramme() {

        $programmeType = $_POST['productyType'];
        $courseType = $_POST['courseType'];
        $studyType = $_POST['studyType'];

         $amountArray = explode(',',$_POST['amount']);


          $amountdetails = array();
          $minamount = 99999;
          $maxamount = 0;

        for($i=0;$i<count($amountArray);$i++) {

           $indamountArray = explode('-',$amountArray[$i]);


           if($indamountArray[0]=='') {
             continue;
           }

           if(count($indamountArray)==1) {
             if($indamountArray[0]==0) {
               $minamount = 0;
             }
           }

           if(count($indamountArray)>1) {

            if((int)$minamount>(int)$indamountArray[0]) {
               $minamount = $indamountArray[0];
            }
            if((int)$minamount>(int)$indamountArray[1]) {
               $minamount = $indamountArray[1];
            }

            if((int)$maxamount<(int)$indamountArray[0]) {
               $maxamount = $indamountArray[0];
            }


            if( (int) $maxamount< (int) $indamountArray[1]) {
               $maxamount = $indamountArray[1];
            }


           } else {


            if((int)$minamount>(int)$amountArray[$i]) {
               $minamount = $amountArray[$i];
            }

            if((int)$maxamount<(int)$amountArray[$i]) {
               $maxamount = $amountArray[$i];
            }


           }




        }
    







        if($_POST['productname']) {

          if(is_numeric($_POST['productname'])) {
            $programmeList = $this->register_model->getAllByCategories($_POST['productname']);

          } else {
            $programmeList = $this->register_model->programmeForCourseByFilterByName($_POST['productname']);

          }
        } else {
            $programmeList = $this->register_model->programmeForCourseByFilter($programmeType,$courseType,$studyType,$minamount,$maxamount);
        }

        if($_POST['searchname']!='') {
                      $programmeList = $this->register_model->programmeForCourseByFilterByName($_POST['searchname']);
        }


        if(count($programmeList)>0) {
          $table="";
        } else {
        $table="No Coures for the selected Filters";
      }
                

                  for($i=0;$i<count($programmeList);$i++) {

                    $idprogramme  = $programmeList[$i]->id;
                    $programmeName = ucwords($programmeList[$i]->name);
                    $programmeimage = $programmeList[$i]->course_image;
                    $durationofprog = $programmeList[$i]->max_duration.' '.$programmeList[$i]->duration_type;
                    $amount = number_format($programmeList[$i]->amount,2);
                    $staffimage = $programmeList[$i]->staffimage;
                    $staffname = $programmeList[$i]->staffname;
                    $categoryname = $programmeList[$i]->categoryname;
                    $producttype = $programmeList[$i]->producttype;
                    $cattype = ucwords($programmeList[$i]->cattype);
                    $studylevel = $programmeList[$i]->studylevel;
                    if($amount>0) {
                      $amount = "RM ".$amount;
                    } else {
                      $amount = "FREE";
                    }
                    
                   $mqa = $programmeList[$i]->course_sub_type;

                  $table.="<div class='col-lg-4 col-md-6 col-12 ' style='padding-top:25px;'>
                  <div class='card mb-4 card-hover'>
                    <a href='/coursedetails/index/$idprogramme' class='text-inherit'>
                           <img
                        src='/assets/images/$programmeimage'
                        alt
                        class='rounded-top card-img-top'
                      />
                  </a>
                  <div class='card-body'>
                    <h4 class='mb-2 text-truncate-line-2' class='text-inherit'>
                    <a href='/coursedetails/index/$idprogramme' class='text-inherit'>
                        $programmeName</a
                      >";

                      if($mqa) {
                        $table.="<img src='/website/img/mqa.png' style='height:40px;float: right;'/>";
                      }


                    $table.="</h4>
                   
                    <ul class='mb-2 list-inline'>
                  <li class='list-inline-item'>
                    <i class='fe fe-clock mr-1'></i>$durationofprog
                  </li>
                  <li class='list-inline-item'>
                    <i class='fe fe-bar-chart mr-1'></i>$studylevel
                  </li>
                </ul>

                   <div class='1h-1'>
                  <div class='d-flex'>
                    <div class='h5'>$amount</div>
                   
                  </div>
                </div>
                <div class='1h-1'>
                  <div class='d-flex'>
                     <div class=''>
                                          <i class='fe fe-book mr-1'></i>
                                          $producttype

                    </div>
                    
                  </div>
                </div>";
                     if($id_category=='2') {
                       $table.="<div class='d-flex mt-2'>
                        <a href='/programdetails/index/$idprogramme' class='btn btn-outline-primary btn-sm'>Enroll</a>
                     
                    </div>";
                     }
                     else {
                       $table.="<div class='d-flex mt-2'>
                        <a href='/coursedetails/index/$idprogramme' class='btn btn-outline-primary btn-sm'>
                        Enroll</a
                      >
                     
                    </div>";

                     }
                    

                    
                  $table.="</div>
                   <div class='card-footer'>
                <div class='row align-items-center no-gutters'>
                  <div class='col-auto'>";
                    if($staffimage!='') { 
                    $table.="<img
                      src='/assets/images/$staffimage'
                      class='rounded-circle avatar-xs'
                    />";
                    } else { 
                     $table.="<img
                      src='/website/img/default-staff.jpg'
                      class='rounded-circle avatar-xs'
                    />";

                  } 
                  $table.="</div>
                  <div class='col ml-2' style='font-weight:bold;''>
                    <span>$staffname</span>
                  </div>
                 
                </div>
              </div>
                 
                </div>
              </div>";             
                 }
                
        echo $table;
        exit;
    }


    public function getprogrammesummary(){
        $programmeType = $_POST['productyType'];
        $courseType = $_POST['courseType'];
        $studyType = $_POST['studyType'];
        $amountArray = explode(',',$_POST['amount']);
        $weekArray = explode(',',$_POST['week']);


          $amountdetails = array();
          $minamount = 99999;
          $maxamount = 0;
        for($i=0;$i<count($amountArray);$i++) {

           $indamountArray = explode('-',$amountArray[$i]);


           if($indamountArray[0]=='') {
             continue;
           }

           if(count($indamountArray)>1) {

            if((int)$minamount>(int)$indamountArray[0]) {
               $minamount = (int)$indamountArray[0];
            }
            if((int)$minamount>(int)$indamountArray[1]) {
               $minamount = (int)$indamountArray[1];
            }

            if((int)$maxamount<(int)$indamountArray[0]) {
               $maxamount = (int)$indamountArray[0];
            }


            if( (int) $maxamount< (int) $indamountArray[1]) {
               $maxamount = (int)$indamountArray[1];
            }


           } else {


            if((int)$minamount>(int)$amountArray[$i]) {
               $minamount = (int)$amountArray[$i];
            }

            if((int)$maxamount<(int)$amountArray[$i]) {
               $maxamount = (int)$amountArray[$i];
            }


           }




        }


        $weekdetails = array();
          $minweek = 99999;
          $maxweek = 0;
        for($i=0;$i<count($weekArray);$i++) {

           $indweekArray = explode('-',$weekArray[$i]);


           if($indweekArray[0]=='') {
             continue;
           }

           if(count($indweekArray)>1) {

            if((int)$minweek>(int)$indweekArray[0]) {
               $minweek = (int)$indweekArray[0];
            }
            if((int)$minweek>(int)$indweekArray[1]) {
               $minweek = (int)$indweekArray[1];
            }

            if((int)$maxweek<(int)$indweekArray[0]) {
               $maxweek = (int)$indweekArray[0];
            }


            if( (int) $maxweek< (int) $indweekArray[1]) {
               $maxweek = (int)$indweekArray[1];
            }


           } else {


            if((int)$minweek>(int)$indweekArray[$i]) {
               $minweek = (int)$indweekArray[$i];
            }

            if((int)$maxweek<(int)$indweekArray[$i]) {
               $maxweek = (int)$indweekArray[$i];
            }


           }




        }
    
    

        if($_POST['productname']) {

          if(is_numeric($_POST['productname'])) {
            $programmeList = $this->register_model->getAllByCategoriesSummary($_POST['productname']);

          } else {
            $programmeList = $this->register_model->programmeForCourseByFilterByNameSummary($_POST['productname']);

          }
        } else {

            $programmeList = $this->register_model->programmeForCourseByFilterSummary($programmeType,$courseType,$studyType,$minamount,$maxamount,$minweek,$maxweek);
        }

        if($_POST['searchname']!='') {
                      $programmeList = $this->register_model->programmeForCourseByFilterByNameSummary($_POST['searchname']);

        }


        if(count($programmeList)>0) {
          $table="";
        } else {
        $table="No Coures for the selected Filters";
      }
                

                  for($i=0;$i<count($programmeList);$i++) {

                    $idprogramme  = $programmeList[$i]->id;
                    $programmeName = ucwords($programmeList[$i]->name);
                    $programmeimage = $programmeList[$i]->course_image;
                    $durationofprog = $programmeList[$i]->max_duration.' '.$programmeList[$i]->duration_type;
                    $amount = number_format($programmeList[$i]->amount,2);
                    $staffimage = $programmeList[$i]->staffimage;
                    $staffname = $programmeList[$i]->staffname;
                    $categoryname = $programmeList[$i]->categoryname;
                    $producttype = $programmeList[$i]->producttype;
                    $cattype = ucwords($programmeList[$i]->cattype);
                    $studylevel = $programmeList[$i]->studylevel;
                    if($amount>0) {
                      $amount = "RM ".$amount;
                    } else {
                      $amount = "FREE";
                    }
                                       $mqa = $programmeList[$i]->course_sub_type;

                  
                  $table.="<div class='col-lg-4 col-md-6 col-12 ' style='padding-top:25px;'>
                  <div class='card mb-4 card-hover'>
                    <a href='/coursedetails/index/$idprogramme' class='text-inherit'>
                           <img
                        src='/assets/images/$programmeimage'
                        alt
                        class='rounded-top card-img-top'
                      />
                  </a>
                  <div class='card-body'>
                    <h4 class='mb-2 text-truncate-line-2' class='text-inherit'>
                    <a href='/coursedetails/index/$idprogramme' class='text-inherit'>
                        $programmeName</a
                      >";
                       if($mqa) {
                        $table.="<img src='/website/img/mqa.png' style='height:40px;float: right;'/>";
                      }

                    $table.="</h4>
                   
                    <ul class='mb-2 list-inline'>
                  <li class='list-inline-item'>
                    <i class='fe fe-clock mr-1'></i>$durationofprog
                  </li>
                  <li class='list-inline-item'>
                    <i class='fe fe-bar-chart mr-1'></i>$studylevel
                  </li>
                </ul>

                 <div class='1h-1'>
                  <div class='d-flex'>
                    <div class='h5'>$amount</div>
                   
                  </div>
                </div>
                <div class='1h-1'>
                  <div class='d-flex'>
                     <div class=''>
                                          <i class='fe fe-book mr-1'></i>
                                          $categoryname

                    </div>
                    
                  </div>
                </div>
                ";
                     if($id_category=='2') {
                       $table.="<div class='d-flex mt-2'>
                        <a href='/programdetails/index/$idprogramme' class='btn btn-outline-primary btn-sm'>Enroll</a>
                     
                    </div>";
                     }
                     else {
                       $table.="<div class='d-flex mt-2'>
                        <a href='/coursedetails/index/$idprogramme' class='btn btn-outline-primary btn-sm'>
                        Enroll</a
                      >
                     
                    </div>";

                     }
                    

                    
                  $table.="</div>
                   <div class='card-footer'>
                <div class='row align-items-center no-gutters'>
                  <div class='col-auto'>";
                    if($staffimage!='') { 
                    $table.="<img
                      src='/assets/images/$staffimage'
                      class='rounded-circle avatar-xs'
                    />";
                    } else { 
                     $table.="<img
                      src='/website/img/default-staff.jpg'
                      class='rounded-circle avatar-xs'
                    />";

                  } 
                  $table.="</div>
                  <div class='col ml-2' style='font-weight:bold;''>
                    <span>$staffname</span>
                  </div>
                 
                </div>
              </div>
                 
                </div>
              </div>";             
                 }
                
        echo $table;
        exit;
    }

    
}