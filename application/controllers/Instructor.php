<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Instructor extends BaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('instructor_login_model');

    }

    /**
     * Index Page for this controller.
     */
    public function index()
    {
        $this->checkInstructor();

    }
    

    function checkInstructor()
    {

        $isInstructorLogin = $this->session->userdata('isInstructorLogin');
        
        $this->load->view('instructor_login');
    }


      function logout()
    {
        // echo "DATA";exit;

        $sessionArray = array('id_instructor'=> '',                    
                    'instructor_name'=> '',
                    'isInstructorLogin' => FALSE

            );
     $this->session->set_userdata($sessionArray);
     $this->isLoggedIn();
    }
    


    public function instructorLogin()
    {
        $formData = $this->input->post();
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('email', 'Email', 'required|max_length[32]');
        $this->form_validation->set_rules('password', 'Password', 'required|max_length[32]');
        
        if($this->form_validation->run() == FALSE)
        {
            $this->index();
        }
        else
        {
            $email = strtolower($this->security->xss_clean($this->input->post('email')));
            $password = $this->input->post('password');



            $result = $this->instructor_login_model->InstructorLogin($email, $password);
            if(!empty($result))
            {
                if($lastLogin == '')
                {
                    $partner_university_login = date('Y-m-d h:i:s');
                }
                else
                {
                    $partner_university_login = $lastLogin->created_dt_tm;
                }


                $image = $result->image;
                    
                if($image == '')
                {
                    $image = 'user_profile.jpg  ';
                }


                $sessionArray = array('id_instructor'=>$result->id,
                                        'instructor_name'=>$result->name,
                                        'isInstructorLogin' => TRUE
                                );
                
                $this->session->set_userdata($sessionArray);

                
                redirect('/instructor/student/list');
                }
            else
            {
                $this->session->set_flashdata('error', 'Login ID or Password Mismatch');
                $this->index();
            }
        }
    }

}

?>