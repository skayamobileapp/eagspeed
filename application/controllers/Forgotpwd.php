<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class : Login (LoginController)
 * Login class to control to authenticate user credentials and starts user's session.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Forgotpwd extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {

/*session is started if you don't write this line can't use $_Session  global variable*/
        error_reporting(0);
        parent::__construct();
        $this->load->model('login_model');
        $this->id_student =  $this->session->userdata['id_student'];

        if(empty($this->id_student))
        {
            // redirect('/index');
        }
        else
        {
            redirect('profile/dashboard/index');
        }


    }

    /**
     * Index Page for this controller.
     */
    public function index()
    {
      
        $this->load->model('register_model');

    
        if($this->input->post())
        {
            $email = $this->input->post('email');
            $registerDetails = $this->register_model->checkemail($email);
            if(empty($registerDetails)) {
                 echo "<script>alert('The email has not been registered')</script>";
                 echo "<script>parent.location='/forgotpwd'</script>";
                 exit;
            } else {
                $password = md5("Abc123");
                $studentId = $registerDetails->id;
                $studentdata['password'] = $password;
                $this->login_model->editStudent($studentdata, $studentId);
                  $params = [
                'to' => 'askiran123@gmail.com',
                'subject' => 'SPEED RESET PASSWORD',
                'message' => 'The New Password for your login is Abc123',
                ];
                $serverurl = "https://jobs.industry-mitra.com/mail.php";
                require_once ('curl.php');
                $curl = new curl();
                $resp = $curl->post($serverurl, $params);
                echo "<script>alert('The New password has been sent to your email, please check the inbox or spam folder once')</script>";
                echo "<script>parent.location='/login'</script>";
                exit;

            }
        }
        $this->loadViews('forgot/index',$this->global,$data,NULL);
    }
}
?>