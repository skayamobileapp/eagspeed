


ALTER TABLE `student`  ADD `country_code` VARCHAR(50) NULL AFTER `id_type`;


ALTER TABLE `fee_structure_master` ADD `description` VARCHAR(2048) NULL DEFAULT '' AFTER `code`;

ALTER TABLE `scholarship_individual_entry_requirement` ADD `entry_type` VARCHAR(50) NULL AFTER `updated_dt_tm`;

UPDATE `scholarship_individual_entry_requirement` SET `entry_type` = 'ENTRY'

ALTER TABLE `applicant` ADD `profile_pic` VARCHAR(1024) NULL DEFAULT 'default_profile.jpg' AFTER `ig_id`;

ALTER TABLE `student` ADD `profile_pic` VARCHAR(1024) NULL DEFAULT 'default_profile.jpg' AFTER `ig_id`;


ALTER TABLE `file_type` CHANGE `file_type_name` `name` VARCHAR(512) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;

ALTER TABLE `file_type` ADD `status` INT(2) NULL DEFAULT '0' AFTER `name`, ADD `created_by` INT(20) NULL AFTER `status`, ADD `created_dt_tm` DATETIME NULL DEFAULT CURRENT_TIMESTAMP AFTER `created_by`, ADD `updated_by` INT(20) NULL AFTER `created_dt_tm`, ADD `updated_dt_tm` DATETIME NULL AFTER `updated_by`;

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`) VALUES (NULL, 'fileType', 'Setup', 'Document', '1', 'fileType', 'list', '5');

ALTER TABLE `discount_type` ADD `status` INT(2) NULL DEFAULT '0' AFTER `description`, ADD `created_by` INT(20) NULL AFTER `status`, ADD `created_dt_tm` DATETIME NULL DEFAULT CURRENT_TIMESTAMP AFTER `created_by`, ADD `updated_by` INT(20) NULL AFTER `created_dt_tm`, ADD `updated_dt_tm` DATETIME NULL AFTER `updated_by`;

ALTER TABLE `discount` ADD `status` INT(2) NULL DEFAULT '0' AFTER `currency`, ADD `created_by` INT(20) NULL AFTER `status`, ADD `created_dt_tm` DATETIME NULL DEFAULT CURRENT_TIMESTAMP AFTER `created_by`, ADD `updated_by` INT(20) NULL AFTER `created_dt_tm`, ADD `updated_dt_tm` DATETIME NULL AFTER `updated_by`;

CREATE TABLE `program_landscape_learning_mode` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_program_landscape` bigint(20) NOT NULL DEFAULT 0,
  `id_programme` bigint(20) NOT NULL DEFAULT 0,
  `code` varchar(1024) DEFAULT '',
  `name` varchar(2048) DEFAULT '',
  `mode_of_program` varchar(2048) DEFAULT '',
  `mode_of_study` varchar(2048) DEFAULT '',
  `id_program_type` bigint(20) NOT NULL DEFAULT 0,
  `total_semester` bigint(20) NOT NULL DEFAULT 0,
  `min_cr_hrs` bigint(20) NOT NULL DEFAULT 0,
  `max_cr_hrs` bigint(20) NOT NULL DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `notification` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `type` varchar(1024) DEFAULT '',
  `name` varchar(2048) DEFAULT '',
  `description` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `event` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `type` varchar(1024) DEFAULT '',
  `name` varchar(2048) DEFAULT '',
  `description` varchar(2048) DEFAULT '',
  `start_date` varchar(2048) DEFAULT '',
  `end_date` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`) VALUES (NULL, 'Notification', 'Communication', 'Alerts', '1', 'notification', 'list', '3');

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`) VALUES (NULL, 'Events', 'Communication', 'Alerts', '2', 'events', 'list', '3');

ALTER TABLE `fee_structure_activity` ADD `id_currency` INT(20) NULL DEFAULT '0' AFTER `amount_international`;

ALTER TABLE `apply_change_programme` ADD `by_student` INT(20) NULL DEFAULT '0' AFTER `id_new_program_has_scheme`;

ALTER TABLE `apply_change_scheme` ADD `by_student` INT(20) NULL DEFAULT '0' AFTER `id_new_program_scheme`;

ALTER TABLE `communication_template` ADD `id_university` INT(20) NULL DEFAULT '0' AFTER `message`, ADD `id_education_level` INT(20) NULL DEFAULT '0' AFTER `id_university`;

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`) VALUES (NULL, 'New Student Enrollment', 'Setup', 'Student Enrollment', '1', 'enrollment', 'list', '7');

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`) VALUES (NULL, 'Checklist For StudentVisa', 'Setup', 'Student Enrollment', '2', 'checklistVisa', 'list', '7');

CREATE TABLE `enrollment_setup` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_university` int(20) DEFAULT 0,
  `id_education_level` int(20) DEFAULT 0,
  `description` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `checklist_visa_setup` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_university` int(20) DEFAULT 0,
  `id_education_level` int(20) DEFAULT 0,
  `description` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `communication_template_message` ADD `id_university` INT(20) NULL DEFAULT '0' AFTER `message`, ADD `id_education_level` INT(20) NULL DEFAULT '0' AFTER `id_university`

CREATE TABLE `temp_test_booking_details` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_session` varchar(2048) DEFAULT '',
  `booking_type` varchar(2048) DEFAULT '',
  `id_test` int(20) DEFAULT 0,
  `id_package` int(20) DEFAULT 0,
  `amount` float(20,2) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `test_booking_details` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_test_booking` varchar(2048) DEFAULT '',
  `booking_type` varchar(2048) DEFAULT '',
  `id_test` int(20) DEFAULT 0,
  `id_package` int(20) DEFAULT 0,
  `amount` float(20,2) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `temp_sample_booking_details` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_session` varchar(2048) DEFAULT '',
  `id_sample` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `sample_booking_details` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_test_booking` varchar(2048) DEFAULT '',
  `id_sample` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `test_booking` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_patient` int(20) DEFAULT 0,
  `id_staff` int(20) DEFAULT 0,
  `time_slot` varchar(200) DEFAULT '',
  `appointment_date` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



ALTER TABLE `users` ADD `id_role` INT(20) NULL DEFAULT '0' AFTER `role_id`;


CREATE TABLE `user_last_login` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_user` bigint(20) NOT NULL,
  `session_data` varchar(2048) NOT NULL,
  `machine_ip` varchar(1024) NOT NULL,
  `user_agent` varchar(128) NOT NULL,
  `agent_string` varchar(1024) NOT NULL,
  `platform` varchar(128) NOT NULL,
  `created_dt_tm` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_last_login`
--

INSERT INTO `user_last_login` (`id`, `id_user`, `session_data`, `machine_ip`, `user_agent`, `agent_string`, `platform`, `created_dt_tm`) VALUES
(1, 1, '{"role":"1","roleText":"Examination Administrator","name":"admin"}', '127.0.0.1', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux','2020-08-17 12:30:52'),
(2, 1, '{"role":"1","roleText":"Examination Administrator","name":"admin"}', '127.0.0.1', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux','2020-08-17 12:30:52'),
(3, 1, '{"role":"1","roleText":"Examination Administrator","name":"admin"}', '157.45.19.112', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux','2020-08-17 12:30:52'),
(4, 1, '{"role":"1","roleText":"Examination Administrator","name":"admin"}', '157.45.19.112', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux','2020-08-17 12:30:52'),
(5, 1, '{"role":"1","roleText":"Examination Administrator","name":"admin"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux','2020-08-17 12:30:52'),
(6, 1, '{"role":"1","roleText":"Examination Administrator","name":"admin"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux','2020-08-17 12:30:52'),
(7, 1, '{"role":"1","roleText":"Examination Administrator","name":"admin"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux','2020-08-17 12:30:52'),
(8, 1, '{"role":"1","roleText":"Examination Administrator","name":"admin"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux','2020-08-17 12:30:52'),
(9, 1, '{"role":"1","roleText":"Examination Administrator","name":"admin"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux','2020-08-17 12:30:52'),
(10, 1, '{"role":"1","roleText":"Examination Administrator","name":"admin"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux','2020-08-17 12:30:52'),
(11, 1, '{"role":"2","roleText":"Ex","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux','2020-08-17 12:30:52'),
(12, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux','2020-08-17 12:30:52'),
(13, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux','2020-08-17 12:30:52'),
(14, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux','2020-08-17 12:30:52'),
(15, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux','2020-08-17 12:30:52'),
(16, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux','2020-08-17 12:30:52'),
(17, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux','2020-08-17 12:30:52'),
(18, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux','2020-08-17 12:30:52'),
(19, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux','2020-08-17 12:30:52'),
(20, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '117.230.180.35', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux','2020-08-17 12:30:52'),
(21, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux','2020-08-17 12:30:52'),
(22, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux','2020-08-17 12:30:52'),
(23, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux','2020-08-17 12:30:52'),
(24, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux','2020-08-17 12:30:52'),
(25, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux','2020-08-17 12:30:52'),
(26, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux','2020-08-17 12:30:52'),
(27, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux','2020-08-17 12:30:52'),
(28, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux','2020-08-17 12:30:52'),
(29, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux','2020-08-17 12:30:52'),
(30, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux','2020-08-17 12:30:52'),
(31, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux','2020-08-17 12:30:52'),
(32, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux','2020-08-17 12:30:52'),
(33, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux','2020-08-17 12:30:52'),
(34, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux','2020-08-17 12:30:52'),
(35, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux','2020-08-17 12:30:52'),
(36, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux','2020-08-17 12:30:52'),
(37, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux','2020-08-17 12:30:52'),
(38, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '157.49.159.118', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux','2020-08-17 12:30:52'),
(39, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux','2020-08-17 12:30:52'),
(40, 1, '{"role":"1","roleText":null,"name":"Mr. Virat Kohli"}', '157.49.104.95', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux','2020-08-17 12:30:52'),
(41, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '157.49.104.95', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux','2020-08-17 12:30:52'),
(42, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '157.49.98.97', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux','2020-08-17 12:30:52'),
(43, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux','2020-08-17 12:30:52'),
(44, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux','2020-08-17 12:30:52'),
(45, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux','2020-08-17 12:30:52'),
(46, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux','2020-08-17 12:30:52'),
(47, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux','2020-08-17 12:30:52'),
(48, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux','2020-08-17 12:30:52'),
(49, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux','2020-08-17 12:30:52'),
(50, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux','2020-08-17 12:30:52'),
(51, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux','2020-08-17 12:30:52'),
(52, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux','2020-08-17 12:30:52'),
(53, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux','2020-08-17 12:30:52'),
(54, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux','2020-08-17 12:30:52'),
(55, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux','2020-08-17 12:30:52'),
(56, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux','2020-08-17 12:30:52'),
(57, 1, '{"role":"1","roleText":"Administrator","name":"Mr. Virat Kohli"}', '127.0.0.1', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux','2020-08-17 12:30:52');

UPDATE `users` SET `id_role` = '1' WHERE `users`.`id` = 1;


CREATE TABLE `category` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(1024) DEFAULT '',
  `code` varchar(200) DEFAULT '',
  `description` varchar(580) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `product_type` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(1024) DEFAULT '',
  `code` varchar(200) DEFAULT '',
  `description` varchar(580) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `student` ADD `is_invoice_generated` INT(20) NULL DEFAULT '0' AFTER `submitted_date`;

CREATE TABLE `campaign` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(1024) DEFAULT '',
  `code` varchar(1024) DEFAULT '',
  `description` varchar(580) DEFAULT '',
  `start_date` varchar(200) DEFAULT '',
  `end_date` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `programme` ADD `id_category` INT(20) NULL DEFAULT '0' AFTER `graduate_studies`, ADD `id_category_setup` INT(20) NULL DEFAULT '0' AFTER `id_category`, ADD `id_programme_type` INT(20) NULL DEFAULT '0' AFTER `id_category_setup`

ALTER TABLE `category_type` CHANGE `category_name` `name` VARCHAR(120) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '';

CREATE TABLE `campaign_student_tagging` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_campaign` varchar(1024) DEFAULT '',
  `id_student` varchar(1024) DEFAULT '',
  `pooling_date_time` varchar(200) DEFAULT '',
  `pooling` varchar(580) DEFAULT '0',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `campaign_student_tagging` ADD `random_number` VARCHAR(512) NULL DEFAULT '' AFTER `pooling`;

ALTER TABLE `programme` ADD `max_duration` INT(20) NULL DEFAULT '0' AFTER `foundation`;


CREATE TABLE `student_has_programme` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_student` int(20) DEFAULT 0,
  `id_invoice` int(20) DEFAULT 0,
  `id_programme` int(20) DEFAULT 0,
  `start_date` varchar(200) DEFAULT '',
  `end_date` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`, `status`) VALUES (NULL, 'Student Programme Registration', 'Registration', 'Registration', '5', 'studentCourseRegistration', 'add', '1', '1');


CREATE TABLE `program_course_overview` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_programme` int(20) DEFAULT 0,
  `name` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `program_delivery_mode` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_programme` int(20) DEFAULT 0,
  `name` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `program_assesment` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_programme` int(20) DEFAULT 0,
  `name` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `program_syllabus` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_programme` int(20) DEFAULT 0,
  `name` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `programme` ADD `image` VARCHAR(512) NULL DEFAULT '' AFTER `certificate`;

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`, `status`) VALUES (NULL, 'Company', 'Corporate', 'Company', '1', 'company', 'list', '1', '1');

CREATE TABLE `company` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `level` varchar(200) DEFAULT '',
  `id_parent_company` INT(20) NULL DEFAULT '0',
  `registration_number` varchar(200) DEFAULT '',
  `name` varchar(2048) DEFAULT '',
  `joined_date` varchar(200) DEFAULT '',
  `website` varchar(200) DEFAULT '',
  `institution_type` varchar(512) DEFAULT '',
  `chairman` varchar(512) DEFAULT '',
  `ceo` varchar(512) DEFAULT '',
  `address1` varchar(1024) DEFAULT '',
  `address2` varchar(1024) DEFAULT '',
  `id_country` int(20) DEFAULT 0,
  `id_state` int(20) DEFAULT 0,
  `city` varchar(1024) DEFAULT '',
  `zipcode` varchar(200) DEFAULT '',
  `country_code` varchar(50) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `corporate_membership_status` varchar(200) DEFAULT '',
  `staff_strength` int(20) DEFAULT NULL,
  `debtor_code` varchar(512) DEFAULT '',
  `credit_term` varchar(512) DEFAULT '',
  `control_account` varchar(512) DEFAULT '',
  `credit_term_code` varchar(512) DEFAULT '',
  `primary_contact_name` varchar(1024) DEFAULT '',
  `primary_contact_designation` varchar(1024) DEFAULT '',
  `primary_contact_email` varchar(1024) DEFAULT '',
  `primary_country_code` varchar(50) DEFAULT '',
  `primary_phone` varchar(50) DEFAULT '',
  `second_contact_name` varchar(1024) DEFAULT '',
  `second_contact_designation` varchar(1024) DEFAULT '',
  `second_contact_email` varchar(1024) DEFAULT '',
  `second_country_code` varchar(50) DEFAULT '',
  `second_phone` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `company` ADD `password` VARCHAR(512) NULL DEFAULT '' AFTER `registration_number`;

ALTER TABLE `programme` ADD `duration_type` VARCHAR(200) NULL DEFAULT '' AFTER `max_duration`;

ALTER TABLE `programme` ADD `trending` VARCHAR(200) NULL DEFAULT '' AFTER `duration_type`;

CREATE TABLE `company_user_role` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(2048) DEFAULT '',
  `code` varchar(200) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `company_user_role` (`id`, `name`, `code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES (NULL, 'l&D User', 'l&D User', '1', NULL, CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP);

INSERT INTO `company_user_role` (`id`, `name`, `code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES (NULL, 'l&D Admin', 'l&D Admin', '1', NULL, CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP);

CREATE TABLE `company_users` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_company` int(20) DEFAULT 0,
  `name` varchar(2048) DEFAULT '',
  `phone` varchar(1024) DEFAULT '',
  `email` varchar(1024) DEFAULT '',
  `designation` varchar(1024) DEFAULT '',
  `user_name` varchar(1024) DEFAULT '',
  `password` varchar(1024) DEFAULT '',
  `id_user_role` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `company_user_last_login` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_company_user` bigint(20) NOT NULL,
  `session_data` varchar(2048) NOT NULL,
  `machine_ip` varchar(1024) NOT NULL,
  `user_agent` varchar(128) NOT NULL,
  `agent_string` varchar(1024) NOT NULL,
  `platform` varchar(128) NOT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `student` ADD `id_company_user` INT(20) NULL DEFAULT '0' AFTER `id_type`, ADD `id_company` INT(20) NULL DEFAULT '0' AFTER `id_company_user`;

CREATE TABLE `performa_main_invoice` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `type` varchar(50) DEFAULT '',
  `id_program` int(20) DEFAULT 0,
  `id_intake` int(20) DEFAULT 0,
  `invoice_number` varchar(50) DEFAULT '',
  `type_of_invoice` varchar(50) DEFAULT '',
  `date_time` datetime DEFAULT current_timestamp(),
  `remarks` varchar(520) DEFAULT '',
  `id_application` int(10) DEFAULT NULL,
  `id_sponser` int(20) DEFAULT 0,
  `id_student` int(10) DEFAULT NULL,
  `no_count` int(20) DEFAULT 0,
  `applicant_partner_fee` int(20) DEFAULT 0,
  `currency` varchar(200) DEFAULT '',
  `total_amount` float(20,2) DEFAULT 0.00,
  `invoice_total` float(20,2) DEFAULT 0.00,
  `total_discount` float(20,2) DEFAULT 0.00,
  `balance_amount` float(20,2) DEFAULT 0.00,
  `paid_amount` float(20,2) DEFAULT 0.00,
  `is_migrate_applicant` int(20) DEFAULT 0,
  `reason` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `performa_main_invoice_has_students` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_main_invoice` int(20) DEFAULT 0,
  `id_student` int(20) DEFAULT 0,
  `amount` float(20,2) DEFAULT 0.00,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `performa_main_invoice_discount_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_main_invoice` int(20) DEFAULT 0,
  `name` varchar(500) DEFAULT '',
  `amount` float(20,2) DEFAULT 0.00,
  `id_student` int(20) DEFAULT 0,
  `id_reference` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `performa_main_invoice_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_main_invoice` int(10) DEFAULT NULL,
  `id_fee_item` int(10) DEFAULT NULL,
  `amount` float(20,2) DEFAULT NULL,
  `quantity` int(20) DEFAULT 0,
  `price` int(20) DEFAULT 0,
  `id_reference` int(20) DEFAULT 0,
  `description` varchar(1024) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `performa_main_invoice` ADD `id_main_invoice` INT(20) NULL DEFAULT '0' AFTER `id_intake`;

ALTER TABLE `company` ADD `staff_credit` INT(20) NULL DEFAULT '0' AFTER `second_phone`;

CREATE TABLE `main_invoice_has_students` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_main_invoice` int(20) DEFAULT 0,
  `id_student` int(20) DEFAULT 0,
  `amount` float(20,2) DEFAULT 0.00,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `receipt_paid_details` ADD `date_time` VARCHAR(200) NOT NULL DEFAULT '' AFTER `payment_reference_number`;
ALTER TABLE `receipt_paid_details` ADD `image` VARCHAR(512) NULL DEFAULT '' AFTER `date_time`;

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`, `status`) VALUES (NULL, 'Finance Configuration', 'Finance', 'Fee Structure', '1', 'financeConfiguration', 'edit', '1', '1');

CREATE TABLE `finance_configuration` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `tax_sst` float(20,2) DEFAULT 0.00,
  `sst_registration_number` varchar(512) DEFAULT '',
  `tax_code` varchar(512) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `finance_configuration` (`id`, `tax_sst`, `sst_registration_number`, `tax_code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES (NULL, '6.00', 'WA-214213131', 'SA-88', '1', NULL, CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP);

ALTER TABLE `main_invoice_details` ADD `amount_before_gst` FLOAT(20,2) NOT NULL DEFAULT '0' AFTER `amount`, ADD `gst_amount` FLOAT(20,2) NOT NULL DEFAULT '0' AFTER `amount_before_gst`, ADD `gst_percentage` FLOAT(20,2) NOT NULL DEFAULT '0' AFTER `gst_amount`;

ALTER TABLE `main_invoice` ADD `amount_before_gst` FLOAT(20,2) NULL DEFAULT '0' AFTER `paid_amount`, ADD `gst_amount` FLOAT(20,2) NULL DEFAULT '0' AFTER `amount_before_gst`, ADD `gst_percentage` FLOAT(20,2) NULL DEFAULT '0' AFTER `gst_amount`;

ALTER TABLE `performa_main_invoice_details` ADD `amount_before_gst` FLOAT(20,2) NOT NULL DEFAULT '0' AFTER `amount`, ADD `gst_amount` FLOAT(20,2) NOT NULL DEFAULT '0' AFTER `amount_before_gst`, ADD `gst_percentage` FLOAT(20,2) NOT NULL DEFAULT '0' AFTER `gst_amount`;

ALTER TABLE `performa_main_invoice` ADD `amount_before_gst` FLOAT(20,2) NULL DEFAULT '0' AFTER `paid_amount`, ADD `gst_amount` FLOAT(20,2) NULL DEFAULT '0' AFTER `amount_before_gst`, ADD `gst_percentage` FLOAT(20,2) NULL DEFAULT '0' AFTER `gst_amount`;

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`, `status`) VALUES (NULL, 'Credit Note Approval', 'Finance', 'Credit Note', '2', 'creditNote', 'approvalList', '6', '1'), (NULL, 'Debit Note Approval', 'Finance', 'Debit Note', '2', 'debitNote', 'approvalList', '7', '1');

UPDATE `menu` SET `status` = '0' WHERE `menu`.`id` = 121;


INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`, `status`) VALUES (NULL, 'Edit Profile', 'company_user', 'Profile', '1', 'editProfile', 'edit', '1', '1');

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`, `status`) VALUES (NULL, 'Register Employee', 'company_user', 'Registration', '1', 'applicant', 'list', '2', '1');

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`, `status`) VALUES (NULL, 'Employee Programme Registration', 'company_user', 'Registration', '2', 'studentCourseRegistration', 'add', '2', '1');

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`, `status`) VALUES (NULL, 'Proforma Invoice', 'company_user_finance', 'Proforma Invoice', '1', 'performaMainInvoice', 'list', '1', '1');

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`, `status`) VALUES (NULL, 'Approve Proforma Invoice', 'company_user_finance', 'Proforma Invoice', '2', 'performaMainInvoice', 'approvalList', '1', '1');

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`, `status`) VALUES (NULL, 'Main Invoice', 'company_user_finance', 'Main Invoice', '1', 'mainInvoice', 'list', '2', '1');

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`, `status`) VALUES (NULL, 'Pay Main Invoice', 'company_user_finance', 'Main Invoice', '2', 'mainInvoice', 'approvalList', '2', '1');

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`, `status`) VALUES (NULL, 'Receipt', 'company_user_finance', 'Receipt', '1', 'receipt', 'list', '3', '1');

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`, `status`) VALUES (NULL, 'Student Profile', 'company_user_records', 'Records Info', '1', 'studentRecord', 'list', '1', '1');

CREATE TABLE `programme_condition` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(1024) DEFAULT '',
  `code` varchar(1024) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `programme_condition` (`id`, `name`, `code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES (NULL, 'Name', 'Code', '1', NULL, CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP);

CREATE TABLE `programme_has_award` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(1024) DEFAULT '',
  `id_program` int(20) DEFAULT 0,
  `id_award` int(20) DEFAULT 0,
  `id_program_condition` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


UPDATE `menu` SET `status` = '0' WHERE `menu`.`id` = 213;
UPDATE `menu` SET `status` = '0' WHERE `menu`.`id` = 214;
UPDATE `menu` SET `status` = '0' WHERE `menu`.`id` = 215;
UPDATE `menu` SET `status` = '0' WHERE `menu`.`id` = 216;
UPDATE `menu` SET `status` = '0' WHERE `menu`.`id` = 218;
UPDATE `menu` SET `status` = '0' WHERE `menu`.`id` = 221;
UPDATE `menu` SET `status` = '0' WHERE `menu`.`id` = 221;
UPDATE `menu` SET `status` = '0' WHERE `menu`.`id` = 223;

ALTER TABLE `staff` ADD `id_type` VARCHAR(512) NULL DEFAULT '' AFTER `last_name`;

ALTER TABLE `staff`  ADD `nationality` VARCHAR(200) NULL DEFAULT ''  AFTER `id_type`;

ALTER TABLE `staff` ADD `whatsapp_number` VARCHAR(512) NULL DEFAULT '' AFTER `id_education_level`;
ALTER TABLE `staff` ADD `linked_in` VARCHAR(512) NULL DEFAULT '' AFTER `whatsapp_number`;
ALTER TABLE `staff` ADD `facebook_id` VARCHAR(512) NULL DEFAULT '' AFTER `linked_in`;
ALTER TABLE `staff` ADD `twitter_id` VARCHAR(512) NULL DEFAULT '' AFTER `facebook_id`;
ALTER TABLE `staff` ADD `ig_id` VARCHAR(512) NULL DEFAULT '' AFTER `twitter_id`;

ALTER TABLE `staff` ADD `image` VARCHAR(512) NULL DEFAULT '' AFTER `ig_id`;

ALTER TABLE `staff` ADD `joined_date` VARCHAR(200) NULL DEFAULT '' AFTER `dob`;

CREATE TABLE `staff_education_qualification` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_staff` int(20) DEFAULT 0,
  `level` varchar(1024) DEFAULT '',
  `name` varchar(1024) DEFAULT '',
  `awarding_institute` varchar(1024) DEFAULT '',
  `year` varchar(1024) DEFAULT '',
  `country` int(20) DEFAULT 0,
  `certificate` varchar(1024) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `staff_work_experience` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_staff` int(20) DEFAULT 0,
  `organisation_name` varchar(1024) DEFAULT '',
  `designation` varchar(1024) DEFAULT '',
  `level` varchar(1024) DEFAULT '',
  `start_date` varchar(512) DEFAULT '',
  `end_date` varchar(512) DEFAULT '',
  `employment_letter` varchar(512) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `staff_specialization` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_staff` int(20) DEFAULT 0,
  `id_specialization` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `staff_has_programme` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_staff` int(20) DEFAULT 0,
  `id_programme` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`, `status`) VALUES (NULL, 'Profile', 'partner_university', 'Profile', '1', 'profile', 'edit', '1', '1');

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`, `status`) VALUES (NULL, 'Instructor', 'partner_university', 'Profile', '2', 'instructor', 'list', '1', '1');

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`, `status`) VALUES (NULL, 'Product', 'partner_university_prdtm', 'programme', '1', 'product', 'list', '1', '1');

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`, `status`) VALUES (NULL, 'Product Partner Approval', 'prdtm', 'Product Management', '5', 'programmeApproval', 'list', '1', '1');

ALTER TABLE `programme` ADD `percentage` INT(20) NULL DEFAULT '0' AFTER `image`;

ALTER TABLE `programme` ADD `sold_separately` INT(20) NULL DEFAULT '0' AFTER `mode`;



CREATE TABLE `programme_structure` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_programme` int(20) DEFAULT 0,
  `structure` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `programme_aim` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_programme` int(20) DEFAULT 0,
  `aim` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `programme_module_courses` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_programme` int(20) DEFAULT 0,
  `modules` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



DROP Table programme_structure;
DROP Table programme_aim;
DROP Table programme_module_courses;

CREATE TABLE `programme_has_aim` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `aim` text,
  `id_programme` int(20) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



DROP Table programme_has_ref_program;

CREATE TABLE `programme_has_ref_program` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_programme` int(20) DEFAULT '0',
  `id_child_programme` int(20) DEFAULT '0',
  `status` int(20) DEFAULT '1',
  `created_by` int(20) DEFAULT '0',
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT '0',
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `programme_has_structure` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_programme` int(20) DEFAULT NULL,
  `structure` text,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
------------------------------------

DROP TABLE staff;

CREATE TABLE `staff` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `salutation` varchar(20) DEFAULT '',
  `name` varchar(520) DEFAULT '',
  `first_name` varchar(2048) DEFAULT '',
  `last_name` varchar(2048) DEFAULT '',
  `id_type` varchar(512) DEFAULT '',
  `id_university` int(20) DEFAULT '0',
  `nationality` varchar(200) DEFAULT '',
  `ic_no` varchar(50) DEFAULT '',
  `dob` date DEFAULT NULL,
  `joined_date` varchar(200) DEFAULT '',
  `mobile_number` varchar(20) DEFAULT '',
  `phone_number` varchar(20) DEFAULT '',
  `id_country` int(20) DEFAULT NULL,
  `id_state` int(20) DEFAULT NULL,
  `zipcode` int(10) DEFAULT NULL,
  `gender` varchar(20) DEFAULT '',
  `staff_id` varchar(50) DEFAULT '',
  `email` varchar(180) DEFAULT '',
  `address` varchar(520) DEFAULT '',
  `address_two` varchar(520) DEFAULT '',
  `job_type` varchar(20) DEFAULT '',
  `academic_type` varchar(20) DEFAULT '',
  `id_department` int(10) DEFAULT NULL,
  `id_faculty_program` int(20) DEFAULT '0' COMMENT 'as similar to department',
  `id_education_level` int(20) DEFAULT '0',
  `whatsapp_number` varchar(512) DEFAULT '',
  `linked_in` varchar(512) DEFAULT '',
  `facebook_id` varchar(512) DEFAULT '',
  `twitter_id` varchar(512) DEFAULT '',
  `ig_id` varchar(512) DEFAULT '',
  `image` varchar(512) DEFAULT '',
  `status` int(2) DEFAULT '1',
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `degree_details` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`id`, `salutation`, `name`, `first_name`, `last_name`, `id_type`, `id_university`, `nationality`, `ic_no`, `dob`, `joined_date`, `mobile_number`, `phone_number`, `id_country`, `id_state`, `zipcode`, `gender`, `staff_id`, `email`, `address`, `address_two`, `job_type`, `academic_type`, `id_department`, `id_faculty_program`, `id_education_level`, `whatsapp_number`, `linked_in`, `facebook_id`, `twitter_id`, `ig_id`, `image`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`, `degree_details`) VALUES
(1, '4', 'Dr. Darshan Brungee', 'Darshan', 'Brungee', 'NRIC', 9, '1', 'NDASDFi', '2021-03-31', '2021-03-25', '85394892', '88882323183', 79, 95, 123, 'Male', '8881', 'db11@espeed.com', 'KL', 'KL', '0', '1', 0, 0, 0, '879989987997', '', '', '', '', '41633a79ad9ac7f0b1a781db7009a1fa.jpeg', 1, NULL, '2021-03-12 22:46:52', NULL, '2021-03-12 22:46:52', NULL),
(2, '1', 'AG. Muhammad Hakim', 'Muhammad', 'Hakim', '', 0, '', '7612948373', '2020-07-11', '', '93873737373', '637363636', 1, 1, 573663, 'Male', '1234', 'mhd@g.co.my', 'Address one ', 'Address two', '1', '1', 1, 1, 0, '', '', '', '', '', '', 1, NULL, '2020-07-10 23:42:07', NULL, '2020-07-10 23:42:07', NULL),
(3, '1', 'Mr. Asadullah Bag', 'Asadullah', 'Bag', 'NRIC', 0, '1', 'IC88909', '1980-08-01', '2021-02-11', '89786756', '123', 1, 1, 23432, 'Male', 'F22312', 'asad@cms.com', 'KL', 'KL', '1', '1', 0, 0, 0, '88888888', '', '', '', '', '', 1, NULL, '2020-08-06 09:26:31', NULL, '2020-08-06 09:26:31', NULL),
(4, '16', 'Professor John Arul Phillips', 'John Arul Phillips', '', '', 0, '', 'MF01', '1989-01-01', '', '12345', '12345', 110, 9, 75000, 'Male', '005', '11@mail.com', 'Malacca', 'Malacca', '1', '1', 2, 1, 0, '', '', '', '', '', 'education_john.jpg', 7, NULL, '2020-11-14 11:56:38', 1, '2020-11-14 11:56:38', 'B.A , Dip.Ed. M.Ed. M.S, PhD'),
(5, '16', 'DR. ABDULSALAM KAED', 'ABDULSALAM', 'KAED', '', 0, '', 'NA', '2020-12-01', '', '912', '0123995943', 110, 5, 68000, 'Male', '1', 'hazzna@gmail.com', '57 JALAN SWEI INTAN 3', 'TAMAN SERI INTAN', '1', '1', 1, 7, 0, '', '', '', '', '', '', 1, NULL, '2020-12-22 22:54:38', NULL, '2020-12-22 22:54:38', NULL);



INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`, `status`) VALUES (NULL, 'Student Profile', 'partner_university_records', 'Records Info', '1', 'studentRecord', 'list', '1', '1');

INSERT INTO `status_table` (`id`, `name`, `type`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES (NULL, 'Active', 'Company', '1', '1', '2021-03-15 00:11:22', NULL, '2021-03-15 00:11:22');

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`, `status`) VALUES (NULL, 'Company Approval', 'Corporate', 'Company', '1', 'companyApproval', 'list', '1', '1');

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`, `status`) VALUES (NULL, 'Partner Approval', 'pm', 'Partner Management', '2', 'partnerUniversityApproval', 'list', '1', '1');

CREATE TABLE `status_table` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(2048) DEFAULT '',
  `type` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

UPDATE `country` SET `phone_code` = '+' WHERE `country`.`phone_code` = 'NULL';

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`, `status`) VALUES (NULL, 'Create Batch', 'Setup', 'Batch', '1', 'batch', 'list', '7', '1');


CREATE TABLE `batch` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `code` varchar(2048) DEFAULT '',
  `name` varchar(2048) DEFAULT '',
  `description` varchar(2048) DEFAULT '',
  `start_date` varchar(200) DEFAULT '',
  `end_date` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `batch_has_programme` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_batch` int(20) DEFAULT 0,
  `id_programme` int(20) DEFAULT 0,
  `start_date` varchar(200) DEFAULT '',
  `end_date` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `fee_structure_master` ADD `international_amount` FLOAT(20,2) NULL DEFAULT '0' AFTER `amount`;

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`, `status`) VALUES (NULL, 'Skills', 'Setup', 'General Setup', '6', 'skill', 'list', '1', '1');

CREATE TABLE `skill` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `code` varchar(2048) DEFAULT '',
  `name` varchar(2048) DEFAULT '',
  `description` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `programme_has_skills` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_programme` int(20) DEFAULT 0,
  `id_skill` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`, `status`) VALUES (NULL, 'Collection Report', 'reports', 'Reports', '1', 'collectionReport', 'list', '1', '1');
INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`, `status`) VALUES (NULL, 'Invoice Report', 'reports', 'Reports', '2', 'invoiceReport', 'list', '1', '1');
INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`, `status`) VALUES (NULL, 'Credit Note Report', 'reports', 'Reports', '3', 'creditNoteReport', 'list', '1', '1');
INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`, `status`) VALUES (NULL, 'Debit Note Report', 'reports', 'Reports', '4', 'debitNoteReport', 'list', '1', '1');
INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`, `status`) VALUES (NULL, 'Invoice V/S Payment Report', 'reports', 'Reports', '5', 'invoicePaymentReport', 'list', '1', '1');

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`, `status`) VALUES (NULL, 'Fee Structure', 'partner_university_finance', 'Fee Structure', '1', 'feeStructure', 'list', '1', '1');

ALTER TABLE `student_has_programme` ADD `id_performa_invoice` INT(20) NULL DEFAULT '0' AFTER `id_invoice`;

ALTER TABLE `programme` ADD `send_for_approval` INT(20) NULL DEFAULT '1' AFTER `sold_separately`;

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`, `status`) VALUES (NULL, 'Collection Report', 'company_user_reports', 'Reports', '1', 'collectionReport', 'list', '1', '1'), (NULL, 'Invoice Report', 'company_user_reports', 'Reports', '2', 'invoiceReport', 'list', '1', '1'), (NULL, 'Credit Note Report', 'company_user_reports', 'Reports', '3', 'creditNoteReport', 'list', '1', '1'), (NULL, 'Debit Note Report', 'company_user_reports', 'Reports', '4', 'debitNoteReport', 'list', '1', '1'), (NULL, 'Invoice V/S Payment Report', 'company_user_reports', 'Reports', '5', 'invoicePaymentReport', 'list', '1', '1');

UPDATE `menu` SET `status` = '0' WHERE `menu`.`id` = 117;

UPDATE `menu` SET `order` = '3' WHERE `menu`.`id` = 118;

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`, `status`) VALUES (NULL, 'Student Invoice Generation', 'Finance', 'Invoice', '1', 'mainInvoiceStudent', 'list', '4', '1');

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`, `status`) VALUES (NULL, 'Corporate Invoice Generation', 'Finance', 'Invoice', '2', 'mainInvoiceCorporate', 'list', '4', '1');

ALTER TABLE `main_invoice` ADD `fee_type` VARCHAR(200) NULL DEFAULT '' AFTER `applicant_partner_fee`;

ALTER TABLE `performa_main_invoice` ADD `fee_type` VARCHAR(200) NULL DEFAULT '' AFTER `applicant_partner_fee`;

UPDATE `menu` SET `menu_name` = 'Add Employee' WHERE `menu`.`id` = 230;

ALTER TABLE `performa_main_invoice_details` CHANGE `price` `price` FLOAT(20,2) NULL DEFAULT '0';

ALTER TABLE `main_invoice_details` CHANGE `price` `price` FLOAT(20,2) NULL DEFAULT '0';

ALTER TABLE `performa_main_invoice_details` ADD `price_before_gst` FLOAT(20,2) NULL DEFAULT '0' AFTER `price`;

ALTER TABLE `main_invoice_details` ADD `price_before_gst` FLOAT(20,2) NULL DEFAULT '0' AFTER `price`;

ALTER TABLE `performa_main_invoice_details` ADD `total_gst_amount` FLOAT(20,2) NULL DEFAULT '0' AFTER `gst_amount`;

ALTER TABLE `main_invoice_details` ADD `total_gst_amount` FLOAT(20,2) NULL DEFAULT '0' AFTER `gst_amount`;

ALTER TABLE `temp_main_invoice_details` CHANGE `amount` `amount` FLOAT(20,2) NULL DEFAULT '0';

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`, `status`) VALUES (NULL, 'Change Password', 'company_user', 'Profile', '2', 'company', 'changePassword', '1', '1');

CREATE TABLE `reset_user_password` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `email` varchar(512) NOT NULL DEFAULT '',
  `password` varchar(1024) NOT NULL DEFAULT '',
  `type` varchar(128) NOT NULL DEFAULT '',
  `activation_id` varchar(512) NOT NULL DEFAULT '',
  `agent` varchar(512) NOT NULL DEFAULT '',
  `agent_string` varchar(512) NOT NULL DEFAULT '',
  `platform` varchar(512) NOT NULL DEFAULT '',
  `client_ip` varchar(32) NOT NULL DEFAULT '',
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  `status` int(20) NOT NULL DEFAULT '0',
  `created_by` bigint(20) NOT NULL DEFAULT '0',
  `created_dt_tm` datetime NOT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `updated_dtm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`, `status`) VALUES (NULL, 'Award Setup', 'Examination', 'Setup', '6', 'award', 'list', '1', '1');

CREATE TABLE `award_condition` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_award` int(20) DEFAULT '0',
  `id_condition` int(20) DEFAULT '0',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`, `status`) VALUES (NULL, 'Grade', 'Examination', 'Setup', '4', 'grade', 'list', '1', '1');

ALTER TABLE `grade` ADD `id_programme` INT(20) NULL DEFAULT '0' AFTER `description`, ADD `min_range` INT(20) NULL DEFAULT '0' AFTER `id_programme`, ADD `max_range` INT(20) NULL DEFAULT '0' AFTER `min_range`, ADD `grade_level_up` VARCHAR(512) NULL DEFAULT '' AFTER `max_range`;

ALTER TABLE `company` ADD `reason` VARCHAR(1048) NULL DEFAULT '' AFTER `staff_credit`;

CREATE TABLE `marks_distribution_setup` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_programme` int(20) DEFAULT 0,
  `min` int(20) DEFAULT 0,
  `max` int(20) DEFAULT 0,
  `is_fail` int(20) DEFAULT 0,
  `id_grade` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

UPDATE `menu` SET `status` = '0' WHERE `menu`.`id` = 262;

ALTER TABLE `grade` DROP `id_programme`, DROP `min_range`, DROP `max_range`, DROP `grade_level_up`;

UPDATE `menu` SET `menu_name` = 'Mark Distribution Setup' WHERE `menu`.`id` = 262;

UPDATE `menu` SET `controller` = 'markDistribution' WHERE `menu`.`id` = 262;

UPDATE `menu` SET `status` = '1' WHERE `menu`.`id` = 262;

ALTER TABLE `student_has_programme` ADD `id_marks_entry` INT(20) NULL DEFAULT '0' AFTER `end_date`, ADD `id_marks_adjustment` INT(20) NULL DEFAULT '0' AFTER `id_marks_entry`, ADD `marks` INT(20) NULL DEFAULT '0' AFTER `id_marks_adjustment`;

ALTER TABLE `student_marks_entry` CHANGE `id_program` `id_programme` INT(20) NULL DEFAULT '0';

ALTER TABLE `student_marks_entry` CHANGE `id_course_registered_landscape` `id_student_has_programme` INT(20) NULL DEFAULT '0';

ALTER TABLE `student_marks_entry` CHANGE `id_intake` `marks_obtained` INT(20) NULL DEFAULT '0';

ALTER TABLE `student_marks_entry` DROP `id_course_registration`, DROP `id_mark_distribution`, DROP `total_course_marks`, DROP `total_obtained_marks`, DROP `total_min_pass_marks`, DROP `grade`, DROP `is_remarking`;

UPDATE `menu` SET `status` = '0' WHERE `menu`.`id` = 89;

UPDATE `menu` SET `controller` = 'studentMarksEntryApproval' WHERE `menu`.`id` = 88;

UPDATE `menu` SET `parent_name` = 'Mark Adjustment' WHERE `menu`.`id` = 91;

UPDATE `menu` SET `parent_name` = 'Mark Adjustment' WHERE `menu`.`id` = 90;
UPDATE `menu` SET `status` = '0' WHERE `menu`.`id` = 92;

UPDATE `menu` SET `status` = '0' WHERE `menu`.`id` = 93;

CREATE TABLE `student_marks_adjustment` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_student` int(20) DEFAULT '0',
  `id_programme` int(20) DEFAULT '0',
  `old_marks` int(20) DEFAULT '0',
  `marks_obtained` int(20) DEFAULT '0',
  `id_student_has_programme` int(20) DEFAULT '0',
  `result` varchar(500) DEFAULT '',
  `status` int(2) DEFAULT '0',
  `reason` varchar(2048) DEFAULT '',
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `student_marks_entry` ADD `id_student_marks_adjustment` INT(20) NULL DEFAULT '0' AFTER `result`;

CREATE TABLE `programme_discount` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_programme` int(20) DEFAULT '0',
  `is_staff_discount` int(20) DEFAULT '0',
  `staff_discount_type` varchar(200) DEFAULT '',
  `staff_value` int(20) DEFAULT '0',
  `is_student_discount` int(20) DEFAULT '0',
  `student_discount_type` varchar(200) DEFAULT '',
  `student_value` varchar(500) DEFAULT '',
  `status` int(2) DEFAULT '0',
  `reason` varchar(2048) DEFAULT '',
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE `account_code`, `activity_code`, `applicant_has_alumni_discount`, `applicant_has_discount`, `applicant_has_document`, `applicant_has_employee_discount`, `applicant_has_sibbling_discount`, `asset_disposal`, `asset_disposal_detail`, `asset_item`, `asset_order`, `asset_pre_registration`, `asset_registration`, `asset_sub_category`, `courses_from_programme_landscape`, `course_withdraw`, `credit_hour_program_landscape_details`, `grn`, `hostel_inventory_registration`, `hostel_item_registration`, `hostel_registration`, `hostel_room`, `hostel_room_type`, `intake_has_branch`, `intake_has_programme`, `intake_has_scheme`, `internship_application`, `internship_company_has_program`, `internship_company_registration`, `internship_company_type`, `internship_student_limit`, `inventory_allotment`, `investment_application`, `investment_application_details`, `investment_institution`, `investment_registration`, `investment_registration_withdraw`, `investment_type`, `is_scholarship_applicant_examination_details`, `is_scholarship_applicant_personel_details`, `is_scholar_applicant_employment_status`, `is_scholar_applicant_family_details`, `journal`, `journal_details`, `nonpo_detail`, `nonpo_entry`, `po_detail`, `po_entry`,`pr_entry`, `pr_entry_details`;;

-- For Research & Scholar

DROP TABLE `research_articleship`, `research_articleship_has_examiner`, `research_articleship_has_supervisors`, `research_bound`, `research_category_has_supervisors`, `research_chapter`, `research_colloquium`, `research_colloquium_details`, `research_colloquium_student_interest`, `research_commitee_student_interest`, `research_deliverables`, `research_deliverables_student`, `research_examiner`, `research_interest`, `research_phd_duration`, `research_ppt`, `research_professionalpracricepaper`, `research_professionalpracricepaper_has_employment`, `research_professionalpracricepaper_has_examiner`, `research_professionalpracricepaper_has_supervisors`, `research_proposal`, `research_proposal_has_examiner`, `research_proposal_has_supervisors`, `research_proposal_reporting`, `research_readers`, `research_reason_applying`, `research_sco`, `research_status`, `research_supervisor`, `research_toc`, `research_topic`, `research_topic_category`, `scholarship_accreditation_category`, `scholarship_accreditation_type`, `scholarship_amount_calculation_type`, `scholarship_applicant`, `scholarship_application`, `scholarship_assesment_method_setup`, `scholarship_award_setup`, `scholarship_bank_registration`, `scholarship_country`, `scholarship_department`, `scholarship_department_has_staff`, `scholarship_documents`, `scholarship_documents_for_applicant`, `scholarship_documents_program`, `scholarship_documents_program_details`, `scholarship_education_level`, `scholarship_examination_details`, `scholarship_frequency_mode`, `scholarship_individual_entry_requirement`, `scholarship_module_type_setup`, `scholarship_organisation_has_department`, `scholarship_organisation_setup`, `scholarship_organisation_setup_comitee`, `scholarship_partner_university`, `scholarship_partner_university_has_aggrement`, `scholarship_partner_university_has_program`, `scholarship_partner_university_has_training_center`, `scholarship_partner_university_program_has_apprenticeship`, `scholarship_partner_university_program_has_internship`, `scholarship_partner_university_program_has_study_mode`, `scholarship_partner_university_program_has_syllabus`, `scholarship_payment_type`, `scholarship_programme`, `scholarship_program_accreditation`, `scholarship_program_partner`, `scholarship_program_syllabus`, `scholarship_program_syllabus_has_module`, `scholarship_race_setup`, `scholarship_religion_setup`, `scholarship_salutation_setup`, `scholarship_scheme`, `scholarship_selection_type`, `scholarship_state`, `scholarship_sub_thrust`, `scholarship_sub_thrust_has_entry_requirement`, `scholarship_sub_thrust_has_program`, `scholarship_sub_thrust_has_requirement`, `scholarship_sub_thrust_requirement_has_age`, `scholarship_sub_thrust_requirement_has_education`, `scholarship_sub_thrust_requirement_has_other`, `scholarship_sub_thrust_requirement_has_work_experience`, `scholarship_thrust`, `scholarship_users`, `scholarship_work_specialisation`, `scholar_applicant`, `scholar_applicant_employment_status`, `scholar_applicant_family_details`, `scholar_applicant_last_login`, `scholar_last_login`, `scholar_permissions`, `scholar_roles`, `scholar_role_permissions`, `sibbling_discount`, `sponser_coordinator_details`, `sponser_credit_note`, `sponser_credit_note_details`, `sponser_fee_info_details`, `sponser_main_invoice`, `sponser_main_invoice_details`, `sponser_receipt`, `sponser_receipt_details`, `sponser_receipt_paid_details`, `vendor_details`;


CREATE TABLE `programme_status_change` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_programme` int(20) DEFAULT '0',
  `reason` varchar(2048) DEFAULT '',
  `percentage` int(20) DEFAULT 0,
  `status` int(20) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`, `id_module`, `status`) VALUES (NULL, 'Change Password', 'partner_university', 'Profile', '3', 'partnerUniversity', 'changePassword', '1', NULL, '1');

ALTER TABLE `partner_university` ADD `image` VARCHAR(512) NULL DEFAULT 'user_profile.jpg' AFTER `bank_address`;

ALTER TABLE `company` ADD `image` VARCHAR(512) NULL DEFAULT 'user_profile.jpg' AFTER `staff_credit`;

CREATE TABLE `product_type_has_tabs` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_product` int(20) DEFAULT '0',
  `name` varchar(512) DEFAULT '',
  `tab` varchar(512) DEFAULT '',
  `status` int(20) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `product_tabs` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(512) DEFAULT '',
  `title` varchar(512) DEFAULT '',
  `status` int(20) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


INSERT INTO `product_tabs` (`id`, `name`, `title`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES (NULL, 'skill', 'Skills', '1', NULL, CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP);

INSERT INTO `product_tabs` (`id`, `name`, `title`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES (NULL, 'overview', 'Description', '1', NULL, CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP),(NULL, 'syllabus', 'Learning Outcomes', '1', NULL, CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP),(NULL, 'topic', 'Topic', '1', NULL, CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP),(NULL, 'faculty', 'Facilitator', '1', NULL, CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP),(NULL, 'assessment', 'Assessment', '1', NULL, CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP),(NULL, 'accreditation', 'Accreditation', '1', NULL, CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP),(NULL, 'award', 'Award', '1', NULL, CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP),(NULL, 'discount', 'Discounts', '1', NULL, CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP),(NULL, 'structure', 'Programme Structure', '1', NULL, CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP),(NULL, 'aim', 'Aim Of The Programme', '1', NULL, CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP),(NULL, 'modules', 'Modules to Courses', '1', NULL, CURRENT_TIMESTAMP, NULL, CURRENT_TIMESTAMP);

ALTER TABLE `programme` ADD `language` INT(20) NULL DEFAULT '0' AFTER `mode`;

ALTER TABLE `programme` ADD `is_free_course` INT(20) NULL DEFAULT '0' AFTER `language`;

ALTER TABLE `study_level` ADD `short_name` VARCHAR(1024) NULL DEFAULT '' AFTER `name`;

ALTER TABLE `study_level` ADD `code` VARCHAR(512) NULL DEFAULT '' AFTER `id`;

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`, `id_module`, `status`) VALUES (NULL, 'Competency Level', 'Setup', 'General Setup', '6', 'studyLevel', 'list', '1', '1', '1');

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`, `id_module`, `status`) VALUES (NULL, 'Specialisation', 'Setup', 'General Setup', '7', 'specialisation', 'list', '1', '1', '1');

ALTER TABLE `fee_setup` ADD `is_registration_fee` INT(20) NULL DEFAULT '0' AFTER `is_non_invoice`;

ALTER TABLE `fee_structure` ADD `is_registration_fee` INT(20) NULL DEFAULT '0' AFTER `is_installment`;

ALTER TABLE `product_tabs` ADD `description` VARCHAR(1024) NULL DEFAULT '' AFTER `title`;

ALTER TABLE `users` ADD `image` VARCHAR(512) NULL DEFAULT '' AFTER `is_deleted`;
ALTER TABLE `product_type_has_fields` ADD `value_no` INT(21) NULL DEFAULT NULL AFTER `order_no`;

CREATE TABLE `programme_has_resources` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_programme` int(20) DEFAULT 0,
  `id_resource` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT '' 
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `resources` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(512) DEFAULT '',
  `code` varchar(512) DEFAULT '',
  `status` int(20) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `resources` (`id`, `name`, `code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES (NULL, 'Online E-Book', 'Ebook', '1', NULL, CURRENT_TIMESTAMP, NULL, '');

ALTER TABLE `programme_has_assessment` ADD `id_assesment` INT(20) NULL DEFAULT '0' AFTER `id_programme`;

CREATE TABLE `competency` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(512) DEFAULT '',
  `code` varchar(512) DEFAULT '',
  `status` int(20) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `programme_how_to_pass` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_programme` int(20) DEFAULT 0,
  `description` varchar(2048) DEFAULT '',
  `status` int(20) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `exam_components` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(512) DEFAULT '',
  `code` varchar(512) DEFAULT '',
  `status` int(20) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
INSERT INTO `exam_components` (`id`, `name`, `code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES (NULL, 'Exam', '', '1', NULL, CURRENT_TIMESTAMP, NULL, '');

INSERT INTO `exam_components` (`id`, `name`, `code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES (NULL, 'Workshop', '', '1', NULL, CURRENT_TIMESTAMP, NULL, '');

INSERT INTO `exam_components` (`id`, `name`, `code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES (NULL, 'Assessment', '', '1', NULL, CURRENT_TIMESTAMP, NULL, '');

UPDATE `menu` SET `controller` = 'gradeSetup' WHERE `menu`.`id` = 262;
-- UPDATE `menu` SET `status` = '0' WHERE `menu`.`id` = 87;
UPDATE `menu` SET `status` = '1' WHERE `menu`.`id` = 86;

CREATE TABLE `marks_distribution` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_programme` int(20) DEFAULT 0,
  `id_exam_component` int(20) DEFAULT 0,
  `max` int(20) DEFAULT 0,
  `pass_marks` int(20) DEFAULT 0,
  `is_attendance` int(20) DEFAULT 0,
  `is_pass` int(20) DEFAULT 0,
  `status` int(20) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`, `id_module`, `status`) VALUES (NULL, 'Chat', 'chatboat', 'Chat', '1', 'leads', 'list', '1', '12', '1');

INSERT INTO `status_table` (`id`, `name`, `type`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES (NULL, 'Active', 'Chat', '1', '1', '2021-03-15 00:11:22', NULL, '2021-03-15 00:11:22');

INSERT INTO `status_table` (`id`, `name`, `type`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES (NULL, 'Draft', 'Chat', '1', '1', '2021-03-15 00:11:22', NULL, '2021-03-15 00:11:22');



ALTER TABLE main_invoice_details
MODIFY id INT(20) NOT NULL AUTO_INCREMENT;
ALTER TABLE main_invoice_discount_details
MODIFY id INT(20) NOT NULL AUTO_INCREMENT;
ALTER TABLE student_has_programme
MODIFY id INT(20) NOT NULL AUTO_INCREMENT;
ALTER TABLE receipt
MODIFY id INT(20) NOT NULL AUTO_INCREMENT;


INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`, `id_module`, `status`) VALUES (NULL, 'Cohert', 'Setup', 'Cohert', '1', 'batch', 'list', '7', '1', '1');

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`, `id_module`, `status`) VALUES (NULL, 'Receipt Report', 'reports', 'Reports', '6', 'receiptReport', 'list', '1', NULL, '1');



CREATE TABLE `author` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(1024) DEFAULT '',
  `status` int(20) DEFAULT 1,
  `created_by` int(20) DEFAULT 0,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `notification` ADD `start_date` VARCHAR(256) NULL DEFAULT '' AFTER `description`, ADD `end_date` VARCHAR(256) NULL DEFAULT '' AFTER `start_date`;

CREATE TABLE `notification_has_students` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_notification` int(20) DEFAULT 0,
  `id_student` int(20) DEFAULT 0,
  `status` int(20) DEFAULT 1,
  `created_by` int(20) DEFAULT 0,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
------ Update CMS From Here ------

ALTER TABLE `discount` ADD `code` VARCHAR(512) NULL DEFAULT '' AFTER `end_date`;























TRUNCATE table `main_invoice`;
TRUNCATE table `main_invoice_details`;
TRUNCATE table `main_invoice_discount_details`;
TRUNCATE table `main_invoice_has_students`;
TRUNCATE table `performa_main_invoice`;
TRUNCATE table `performa_main_invoice_details`;
TRUNCATE table `performa_main_invoice_discount_details`;
TRUNCATE table `performa_main_invoice_has_students`;
TRUNCATE table `receipt`;
TRUNCATE table `receipt_details`;
TRUNCATE table `receipt_paid_details`;
TRUNCATE table `temp_receipt_details`;
TRUNCATE table `add_temp_receipt_paid_amount`;
TRUNCATE table `student_has_programme`;



https://www.youtube.com/watch?v=k2qgadSvNyU
https://www.youtube.com/watch?v=jzD_yyEcp0M
https://www.youtube.com/watch?v=QYh6mYIJG2Y
https://www.youtube.com/watch?v=apFfnzkgEII
https://www.youtube.com/watch?v=j9V78UbdzWI


























































