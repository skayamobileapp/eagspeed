-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 08, 2021 at 08:10 AM
-- Server version: 10.2.37-MariaDB-log
-- PHP Version: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `camsedu_espeed`
--

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(20) NOT NULL,
  `menu_name` varchar(200) DEFAULT NULL,
  `module_name` varchar(200) DEFAULT NULL,
  `parent_name` varchar(200) DEFAULT NULL,
  `order` int(20) DEFAULT NULL,
  `controller` varchar(200) DEFAULT NULL,
  `action` varchar(200) DEFAULT NULL,
  `parent_order` int(20) DEFAULT NULL,
  `status` int(20) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`, `status`) VALUES
(1, 'User', 'Setup', 'General Setup', 1, 'user', 'list', 1, 1),
(2, 'Role', 'Setup', 'General Setup', 2, 'role', 'list', 1, 1),
(3, 'Permission', 'Setup', 'General Setup', 3, 'permission', 'list', 1, 1),
(4, 'Salutation', 'Setup', 'General Setup', NULL, 'salutation', 'list', 1, 1),
(5, 'Country', 'Setup', 'country', NULL, 'country', 'list', NULL, 1),
(6, 'State', 'Setup', 'state', NULL, 'state', 'list', NULL, 1),
(7, 'Education Level', 'Setup', 'General Setup', 4, 'educationLevel', 'list', 1, 1),
(8, 'Academic Year', 'Setup', 'Operation Setup', 2, 'academicYear', 'list', 6, 1),
(9, 'Award', 'Setup', 'General Setup', NULL, 'award', 'list', NULL, 1),
(10, 'Race', 'Setup', 'Demographic', 1, 'race', 'list', 2, 1),
(11, 'Religion', 'Setup', 'Demographic', 2, 'religion', 'list', 2, 1),
(12, 'Nationality', 'Setup', 'Demographic', 3, 'nationality', 'list', 2, 1),
(13, 'Department', 'Setup', 'Organisation Setup', 3, 'department', 'list', 3, 1),
(14, 'Organisation', 'Setup', 'Organisation Setup', 1, 'organisation', 'edit', 3, 1),
(15, 'Partner Category', 'pm', 'Setup', 1, 'partnerCategory', 'list', 1, 0),
(16, 'Partner University', 'pm', 'Setup', 2, 'partnerUniversity', 'list', 1, 0),
(17, 'Faculty Program', 'Setup', 'Organisation Setup', 2, 'facultyProgram', 'list', 3, 1),
(19, 'Course Type', 'cm', 'Course Setup', 1, 'courseType', 'list', 1, 1),
(21, 'Course Main', 'cm', 'Course Setup', 2, 'course', 'list', 1, 1),
(22, 'Course Requisite', 'cm', 'Course Setup', 3, 'courseMaster', 'list', 1, 1),
(23, 'Course Equivalent', 'cm', 'Course Setup', 4, 'equivalentCourse', 'list', 1, 1),
(24, 'Scheme', 'Setup', 'Operation Setup', 1, 'scheme', 'list', 6, 1),
(29, 'Program Type', 'cm', 'Program Setup', 1, 'programType', 'list', 2, 1),
(30, 'Program Main', 'cm', 'Program Setup', 2, 'programme', 'list', 2, 1),
(31, 'Program Landscape', 'cm', 'Program Setup', 4, 'programmeLandscape', 'list', 2, 1),
(32, 'Program Entry Requirement', 'cm', 'Program Setup', 3, 'individualEntryRequirement', 'list', 2, 1),
(33, 'Documents Required', 'Setup', 'Document', 2, 'documents', 'list', 5, 1),
(40, 'Group Setup', 'Admission', NULL, NULL, NULL, 'list', NULL, 1),
(41, 'Specialization', 'Admission', NULL, NULL, NULL, 'list', NULL, 1),
(42, 'Application List', 'Admission', 'Online Application and Applicant Portal', NULL, 'applicant', 'list', NULL, 1),
(43, 'APEL Approval', 'Admission', 'Online Application and Applicant Portal', NULL, 'apelApproval', 'list', NULL, 1),
(44, 'Sibbling Discount Approval', 'Admission', 'Online Application and Applicant Portal', NULL, 'sibblingDiscountApproval', NULL, NULL, 1),
(45, 'Employee Discount Approval', 'Admission', 'Online Application and Applicant Portal\n', NULL, 'employeeDiscountApproval', NULL, NULL, 1),
(46, 'Alumni Discount Approval', 'Admission', 'Online Application and Applicant Portal\n', NULL, 'alumniDiscountApproval', 'list', NULL, 1),
(47, 'Application Approval', 'Admission', 'Online Application and Applicant Portal\n', 1, 'applicantApproval', 'list', 1, 1),
(48, 'Student Registration', 'Registration', 'Registration', 1, 'student', 'approvalList', 1, 0),
(49, 'Academic Advisor Tagging', 'Registration', 'Registration', 3, 'advisorTagging', 'add', 1, 1),
(50, 'Subject Registration', 'Registration', 'Registration', 3, 'courseRegistration', 'list', 1, 0),
(51, 'Individual and bulk Withdrawal', 'Registration', 'Registration', 4, 'bulkWithdraw', 'list', 1, 0),
(52, 'Credit Transfer / Excemption Application', 'Registration', 'Credit Transfer / Excemption', 1, 'creditTransfer', 'list', 2, 0),
(53, 'Credit Transfer / Excemption Approval', 'Registration', 'Credit Transfer / Excemption', 2, 'creditTransfer', 'approvalList', 2, 0),
(54, 'Attendence Setup', 'Registration', NULL, NULL, NULL, 'list', NULL, 0),
(55, 'Report Course Count', 'Registration', NULL, NULL, NULL, 'list', NULL, 0),
(56, 'Barring Type', 'Records', 'Barring', 1, 'barringType', 'list', 2, 1),
(57, 'Barring', 'Records', 'Barring', 2, 'barring', 'list', 2, 1),
(58, 'Release', 'Records', 'Barring', 3, 'release', 'list', 2, 1),
(59, 'Student Profile', 'Records', 'Records Info', 2, 'studentRecord', 'list', 1, 1),
(60, 'Student Records', 'Records', 'Records Info', 1, 'student', 'list', 1, 0),
(61, 'Change Status Setup', 'Records', 'Change Status', 1, 'changeStatus', 'list', 3, 1),
(62, 'Apply Change Status', 'Records', 'Change Status', 2, 'applyChangeStatus', 'list', 3, 1),
(63, 'Apply Change Status Approval', 'Records', 'Change Status', 3, 'applyChangeStatus', 'approval_list', 3, 1),
(64, 'Visa Details', 'Records', 'Visa Management', 1, 'visaDetails', 'list', 7, 1),
(65, 'Apply Change Program', 'Records', 'Change Programme', 1, 'applyChangeProgramme', 'list', 4, 1),
(66, 'Apply Change Program Approval', 'Records', 'Change Programme', 2, 'applyChangeProgramme', 'approval_list', 4, 1),
(67, 'Apply Change Scheme', 'Records', NULL, NULL, NULL, 'list', NULL, 1),
(68, 'Apply Change Scheme Approval', 'Records', NULL, NULL, NULL, 'list', NULL, 1),
(71, 'Exam Configuration', 'Examination', 'Setup', 1, 'examConfiguration', 'edit', 1, 1),
(72, 'Exam Calender', 'Examination', NULL, 1, NULL, 'list', NULL, 1),
(73, 'GPA/CGPA Setup', 'Examination', 'Setup', 2, 'gpaCgpaSetup', 'list', 1, 1),
(74, 'Assesment Type', 'Examination', 'Setup', 3, 'assesmentType', 'list', 1, 1),
(75, 'Grade Setup', 'Examination', 'Setup', 4, 'gradeSetup', 'list', 1, 1),
(76, 'Course Grade', 'Examination', 'Setup', NULL, NULL, 'list', NULL, 1),
(77, 'Publish Exam Result', 'Examination', NULL, NULL, NULL, 'list', NULL, 1),
(78, 'Exam Locaction Setup', 'Examination', NULL, NULL, NULL, 'list', NULL, 1),
(79, 'Exam Center Setup', 'Examination', NULL, NULL, NULL, 'list', NULL, 1),
(80, 'Exam Event', 'Examination', NULL, NULL, NULL, 'list', NULL, 1),
(81, 'Exam Registration List', 'Examination', 'Exam Registration', 1, 'examRegistration', 'list', 2, 1),
(82, 'Tag Student To Exam Registration', 'Examination', 'Exam Registration', 5, 'examRegistration', 'tagList', 2, 1),
(83, 'Release Exam Slip', 'Examination', NULL, NULL, NULL, 'list', NULL, 1),
(84, 'Publish Exam Result Schedule', 'Examination', NULL, NULL, NULL, 'list', NULL, 1),
(85, 'Publish Assesment Result Schedule', 'Examination', NULL, NULL, NULL, 'list', NULL, 1),
(86, 'Mark Distribution', 'Examination', 'Mark Distribution Setup', 1, 'markDistribution', 'list', 3, 1),
(87, 'Mark Distribution Approval', 'Examination', 'Mark Distribution Setup', 2, 'markDistribution', 'approvalList', 3, 1),
(88, 'Student Marks Entry Approval', 'Examination', 'Marks Entry', 2, 'studentMarksEntry', 'marksEntryApprovalList', 4, 1),
(89, 'Student Marks Entry Summary', 'Examination', 'Marks Entry', 3, 'studentMarksEntry', 'summaryList', 4, 1),
(90, 'Remarking Application', 'Examination', 'Remarking', 1, 'remarkingApplication', 'remarkingList', 5, 1),
(91, 'Remarking Application Approval', 'Examination', 'Remarking', 2, 'remarkingApplication', 'marksEntryApprovalList', 5, 1),
(92, 'Remarking List', 'Examination', 'Remarking', 3, 'remarkingApplication', 'summaryList', 5, 1),
(93, 'Transcript', 'Examination', 'Remarking', 4, 'transcript', 'list', 5, 1),
(94, 'Result Slip', 'Examination', NULL, 5, NULL, 'list', NULL, 1),
(95, 'Award Level', 'Graduation', 'Setup', 1, 'award', 'list', 1, 1),
(96, 'Convocation Setup', 'Graduation', 'Graduation', 1, 'convocation', 'list', 2, 1),
(97, 'Guest', 'Graduation', 'Graduation', 2, 'guest', 'list', 2, 1),
(98, 'Graduation List', 'Graduation', 'Graduation', 3, 'graduationList', 'list', NULL, 1),
(99, 'Approve Graduation Students', 'Graduation', 'Graduation', 4, 'graduationList', 'approvalList', NULL, 1),
(100, 'Currency Setup', 'Finance', 'Finance Setup', 1, 'currency', 'list', 1, 1),
(101, 'Currency Rate Setup', 'Finance', 'Finance Setup', 2, 'currencyRateSetup', 'list', 1, 1),
(102, 'Credit Note Type', 'Finance', 'Finance Setup', 3, 'creditNoteType', 'list', 1, 1),
(103, 'Fee Structure Activity', 'Finance', 'Fee Structure', 3, 'feeStructureActivity', 'list', 2, 1),
(104, 'Account Code', 'Finance', 'Finance Setup', 5, 'accountCode', 'list', 1, 1),
(105, 'Bank Registration', 'Finance', 'Finance Setup', 4, 'bankRegistration', 'list', 1, 1),
(106, 'Amount Calculation Type', 'Finance', 'Finance Setup', 7, 'amountCalculationType', 'list', 1, 0),
(107, 'Frequency Mode', 'Finance', 'Finance Setup', 6, 'frequencyMode', 'list', 1, 0),
(108, 'Payment Type', 'Finance', 'Finance Setup', 9, 'paymentType', 'list', 1, 1),
(109, 'Fee Category', 'Finance', 'Finance Setup', 8, 'feeCategory', 'list', 1, 1),
(110, 'Fee Setup', 'Finance', 'Fee Structure', 1, 'feeSetup', 'list', 2, 1),
(111, 'Fee Structure', 'Finance', 'Fee Structure', 2, 'feeStructure', 'list', 2, 1),
(112, 'Invoicing', 'pm', 'Finance', 1, 'partnerUniversityInvoice', 'list', 2, 0),
(113, 'Payment & Receipt', 'pm', 'Finance', 2, 'receipt', 'list', 2, 0),
(115, 'Discount', 'Finance', 'Discount Setup', 2, 'discount', 'list', 3, 1),
(116, 'Statement Of Accounts', 'Finance', 'Refund', 1, 'StatementOfAccount', 'list', 8, 1),
(117, 'Invoice Generation', 'Finance', 'Invoice', 1, 'mainInvoice', 'list', 4, 0),
(118, 'Wrong Invoice Generated', 'Finance', 'Invoice', 3, 'mainInvoice', 'approvalList', 4, 1),
(119, 'Partner Invoice Generation', 'Finance', NULL, NULL, NULL, 'list', NULL, 1),
(120, 'Receipt', 'Finance', 'Receivables', 1, 'receipt', 'list', 5, 1),
(121, 'Receipt Approval', 'Finance', 'Receivables', 2, 'receipt', 'approvalList', 5, 0),
(122, 'Sponsor Invoice Generation', 'Finance', NULL, NULL, NULL, 'list', NULL, 1),
(123, 'Sponsor Invoice Cancellation', 'Finance', NULL, NULL, NULL, 'list', NULL, 1),
(124, 'Advance Payment', 'Finance', NULL, NULL, NULL, 'list', NULL, 1),
(125, 'Credit Note Entry', 'Finance', 'Credit Note', 1, 'creditNote', 'list', 6, 1),
(126, 'Debit Note Entry', 'Finance', 'Debit Note', 1, 'debitNote', 'list', 7, 1),
(127, 'Refund Entry', 'Finance', NULL, NULL, NULL, 'list', NULL, 1),
(128, 'Sponsor', 'Sponsor', NULL, NULL, NULL, 'list', NULL, 1),
(129, 'Sponsor Has Students', 'Sponsor', NULL, NULL, NULL, 'list', NULL, 1),
(130, 'Room Type', 'Hostel', NULL, NULL, NULL, 'list', NULL, 1),
(131, 'Inventory', 'Hostel', NULL, NULL, NULL, 'list', NULL, 1),
(132, 'Hostel Registration', 'Hostel', NULL, NULL, NULL, 'list', NULL, 1),
(133, 'Apartment Setup', 'Hostel', NULL, NULL, NULL, 'list', NULL, 1),
(134, 'Unit Setup', 'Hostel', NULL, NULL, NULL, 'list', NULL, 1),
(135, 'Room Setup', 'Hostel', NULL, NULL, NULL, 'list', NULL, 1),
(136, 'Room Allotment', 'Hostel', NULL, NULL, NULL, 'list', NULL, 1),
(137, 'Room Allotment Summary', 'Hostel', NULL, NULL, NULL, 'list', NULL, 1),
(138, 'Room Vacancy', 'Hostel', NULL, NULL, NULL, 'list', NULL, 1),
(139, 'Equipments', 'Facility', NULL, NULL, NULL, 'list', NULL, 1),
(140, 'Room Type', 'Facility', NULL, NULL, NULL, 'list', NULL, 1),
(141, 'Building', 'Facility', NULL, NULL, NULL, 'list', NULL, 1),
(142, 'Room Setup', 'Facility', NULL, NULL, NULL, 'list', NULL, 1),
(143, 'Apply For Booking', 'Facility', NULL, NULL, NULL, 'list', NULL, 1),
(144, 'Booking Summary', 'Facility', NULL, NULL, NULL, 'list', NULL, 1),
(145, 'Max. Internship Limit', 'Internship', NULL, NULL, NULL, 'list', NULL, 1),
(146, 'Company Type', 'Internship', NULL, NULL, NULL, 'list', NULL, 1),
(147, 'Company Registration', 'Internship', NULL, NULL, NULL, 'list', NULL, 1),
(148, 'Application', 'Internship', NULL, NULL, NULL, 'list', NULL, 1),
(149, 'Application Approval', 'Internship', NULL, NULL, NULL, 'list', NULL, 1),
(150, 'Reporting', 'Internship', NULL, NULL, NULL, 'list', NULL, 1),
(151, 'Placement Form', 'Internship', NULL, NULL, NULL, 'list', NULL, 1),
(152, 'Alumni List', 'Alumni', NULL, NULL, NULL, 'list', NULL, 1),
(153, 'Templates', 'Communication', 'Communication', 1, 'template', 'list', 1, 1),
(154, 'Group', 'Communication', 'Communication', 2, 'group', 'list', 1, 1),
(155, 'Group Recepients', 'Communication', 'Communication', 3, 'group', 'recepientList', 1, 1),
(156, 'Messaging Templates', 'Communication', 'Messaging', 1, 'templateMessage', 'list', 2, 1),
(157, 'Messaging Group', 'Communication', 'Messaging', 2, 'groupMessage', 'list', 2, 1),
(158, 'Group Recepients (Messaging)', 'Communication', 'Messaging', 3, 'groupMessage', 'recepientList', 2, 1),
(159, 'IPS Topics', 'IPS', NULL, NULL, NULL, 'list', NULL, 1),
(160, 'Research Status', 'IPS', NULL, NULL, NULL, 'list', NULL, 1),
(161, 'Field Of Interest', 'IPS', NULL, NULL, NULL, 'list', NULL, 1),
(162, 'Reason For Applying', 'IPS', NULL, NULL, NULL, 'list', NULL, 1),
(163, 'Research Category', 'IPS', NULL, NULL, NULL, 'list', NULL, 1),
(164, 'Colloquium Setup', 'IPS', NULL, NULL, NULL, 'list', NULL, 1),
(165, 'Advisory', 'IPS', NULL, NULL, NULL, 'list', NULL, 1),
(166, 'Readers', 'IPS', NULL, NULL, NULL, 'list', NULL, 1),
(167, 'Proposal Defense Comitee', 'IPS', NULL, NULL, NULL, 'list', NULL, 1),
(168, 'Chapter', 'IPS', NULL, NULL, NULL, 'list', NULL, 1),
(169, 'Deliverables', 'IPS', NULL, NULL, NULL, 'list', NULL, 1),
(170, 'Submitted Deliverables', 'IPS', NULL, NULL, NULL, 'list', NULL, 1),
(171, 'Phd Duration Tagging', 'IPS', NULL, NULL, NULL, 'list', NULL, 1),
(172, 'Stage', 'IPS', NULL, NULL, NULL, 'list', NULL, 1),
(173, 'Mile Stone', 'IPS', NULL, NULL, NULL, 'list', NULL, 1),
(174, 'Tag Mile Stone To Semester', 'IPS', NULL, NULL, NULL, 'list', NULL, 1),
(175, 'Overview', 'IPS', NULL, NULL, NULL, 'list', NULL, 1),
(176, 'Supervisor Role', 'IPS', NULL, NULL, NULL, 'list', NULL, 1),
(177, 'Supervisor', 'IPS', NULL, NULL, NULL, 'list', NULL, 1),
(178, 'Supervisor Tagging', 'IPS', NULL, NULL, NULL, 'list', NULL, 1),
(179, 'Examiner Role', 'IPS', NULL, NULL, NULL, 'list', NULL, 1),
(180, 'Examiner', 'IPS', NULL, NULL, NULL, 'list', NULL, 1),
(181, 'Proposal', 'IPS', NULL, NULL, NULL, 'list', NULL, 1),
(182, 'Articleship', 'IPS', NULL, NULL, NULL, 'list', NULL, 1),
(183, 'Professional Practice Paper', 'IPS', NULL, NULL, NULL, 'list', NULL, 1),
(184, 'Proposal Approval', 'IPS', NULL, NULL, NULL, 'list', NULL, 1),
(185, 'Articleship Approval', 'IPS', NULL, NULL, NULL, 'list', NULL, 1),
(186, 'PPP Approval', 'IPS', NULL, NULL, NULL, 'list', NULL, 1),
(187, 'Student Marks Entry', 'Examination', 'Marks Entry', 1, 'studentMarksEntry', 'studentList', 4, 1),
(188, 'Supervisor Change Application', 'IPS', NULL, NULL, NULL, 'list', NULL, 1),
(189, 'Student IPS Progress', 'Examination', NULL, NULL, NULL, 'list', NULL, 1),
(190, 'Payment Rate', 'af', 'Setup', 1, 'paymentRate', 'list', 1, 1),
(191, 'Academic Facilitator profile', 'af', 'Setup', 2, 'staff', 'list', 1, 1),
(192, 'Change Of Status', 'af', 'Records Management', 1, 'staff', 'changeStatusList', 2, 1),
(193, 'Online claim', 'af', 'Records Management', 2, 'onlineClaim', 'list', 2, 1),
(194, 'Statement of account', 'af', 'Records Management', 3, 'statementOfAccount', 'list', 2, 1),
(195, 'Discount Type', 'Finance', 'Discount Setup', 1, 'discountType', 'list', 3, 1),
(196, 'Change Learning Mode Application', 'Records', 'Change Learning Mode', 1, 'applyChangeLearningMode', 'list', 5, 1),
(197, 'Change Learning Mode Approval', 'Records', 'Change Learning Mode', 2, 'applyChangeLearningMode', 'approval_list', 5, 1),
(198, 'Schedule Publish Final Exam Result', 'Examination', 'Setup', 5, 'publishExamResult', 'list', 1, 1),
(199, 'Student IP List', 'Examination', 'Exam Registration', 2, 'ipsProgress', 'list', 2, 1),
(200, 'Exam Registration Slip', 'Examination', 'Exam Registration', 3, 'examAttendence', 'comingSoon', 2, 1),
(201, 'Student Semester Promotion', 'Registration', 'Registration', 5, 'studentSemester', 'add', 1, 0),
(202, 'File Type', 'Setup', 'Document', 1, 'fileType', 'list', 5, 1),
(203, 'Notification', 'Communication', 'Alerts', 1, 'notification', 'list', 3, 1),
(204, 'Events', 'Communication', 'Alerts', 2, 'events', 'list', 3, 1),
(207, 'Category Type Set-Up', 'prdtm', 'Product Management', 2, 'categoryType', 'list', 1, 0),
(208, 'Product Type', 'prdtm', 'Product Management', 1, 'category', 'list', 1, 1),
(209, 'Cateogory Type', 'prdtm', 'Product Management', 3, 'productType', 'list', 1, 1),
(210, 'Product', 'prdtm', 'Product Management', 4, 'programme', 'list', 1, 1),
(211, 'Create campaign', 'mrktngm', 'Marketing Management', 1, 'campaign', 'list', 1, 1),
(212, 'Archived campaign', 'mrktngm', 'Marketing Management', 2, 'archievedCampaign', 'list', 1, 1),
(213, 'Registration record', 'mrktngm', 'Marketing Management', 3, 'welcome', 'list', 1, 0),
(214, 'Create contact', 'mrktngm', 'Marketing Management', 4, 'welcome', 'list', 1, 0),
(215, 'Reports', 'mrktngm', 'Marketing Management', 5, 'welcome', 'list', 1, 0),
(216, 'Partner type set-up', 'pm', 'Partner Management', 1, 'welcome', 'list', 1, 0),
(217, 'Add partner', 'pm', 'Partner Management', 2, 'partnerUniversity', 'list', 1, 1),
(218, 'Partner SOA', 'pm', 'Partner Management', 3, 'welcome', 'list', 1, 0),
(219, 'Invoicing', 'pm', 'Partner Management', 4, 'partnerUniversityInvoice', 'list', 1, 1),
(220, 'Payments', 'pm', 'Partner Management', 5, 'receipt', 'list', 1, 1),
(221, 'Register bulk students', 'registration', 'Registration', 1, 'welcome', 'list', 1, 0),
(222, 'Registration list', 'registration', 'Registration', 2, 'applicant', 'list', 1, 1),
(223, 'Defer Registration', 'registration', 'Registration', 4, 'welcome', 'list', 1, 0),
(224, 'Student Programme Registration', 'Registration', 'Registration', 5, 'studentCourseRegistration', 'add', 1, 1),
(225, 'Company', 'Corporate', 'Company', 1, 'company', 'list', 1, 1),
(226, 'Finance Configuration', 'Finance', 'Fee Structure', 1, 'financeConfiguration', 'edit', 1, 1),
(227, 'Credit Note Approval', 'Finance', 'Credit Note', 2, 'creditNote', 'approvalList', 6, 1),
(228, 'Debit Note Approval', 'Finance', 'Debit Note', 2, 'debitNote', 'approvalList', 7, 1),
(229, 'Edit Profile', 'company_user', 'Profile', 1, 'company', 'edit', 1, 1),
(230, 'Add Employee', 'company_user', 'Registration', 1, 'applicant', 'list', 2, 1),
(231, 'Employee Programme Registration', 'company_user', 'Registration', 2, 'studentCourseRegistration', 'add', 2, 1),
(232, 'Proforma Invoice', 'company_user_finance', 'Proforma Invoice', 1, 'performaMainInvoice', 'list', 1, 1),
(233, 'Approve Proforma Invoice', 'company_user_finance', 'Proforma Invoice', 2, 'performaMainInvoice', 'approvalList', 1, 1),
(234, 'Main Invoice', 'company_user_finance', 'Main Invoice', 1, 'mainInvoice', 'list', 2, 1),
(235, 'Pay Main Invoice', 'company_user_finance', 'Main Invoice', 2, 'mainInvoice', 'approvalList', 2, 1),
(236, 'Receipt', 'company_user_finance', 'Receipt', 1, 'receipt', 'list', 3, 1),
(237, 'Employee Profile', 'company_user_records', 'Records Info', 1, 'studentRecord', 'list', 1, 1),
(238, 'Profile', 'partner_university', 'Profile', 1, 'partnerUniversity', 'edit', 1, 1),
(239, 'Academic Facilitator', 'partner_university', 'Profile', 2, 'staff', 'list', 1, 1),
(240, 'Product', 'partner_university_prdtm', 'Product', 1, 'programme', 'list', 1, 1),
(241, 'Product Partner Approval', 'prdtm', 'Product Management', 5, 'programmeApproval', 'list', 1, 1),
(242, 'Student Profile', 'partner_university_records', 'Records Info', 1, 'studentRecord', 'list', 1, 1),
(243, 'Company Approval', 'Corporate', 'Company', 1, 'companyApproval', 'list', 1, 1),
(244, 'Partner Approval', 'pm', 'Partner Management', 2, 'partnerUniversityApproval', 'list', 1, 1),
(246, 'Skills', 'Setup', 'General Setup', 6, 'skill', 'list', 1, 1),
(247, 'Collection Report', 'reports', 'Reports', 1, 'collectionReport', 'list', 1, 0),
(248, 'Invoice Report', 'reports', 'Reports', 2, 'invoiceReport', 'list', 1, 1),
(249, 'Credit Note Report', 'reports', 'Reports', 3, 'creditNoteReport', 'list', 1, 1),
(250, 'Debit Note Report', 'reports', 'Reports', 4, 'debitNoteReport', 'list', 1, 1),
(251, 'Invoice V/S Payment Report', 'reports', 'Reports', 5, 'invoicePaymentReport', 'list', 1, 0),
(252, 'Fee Structure', 'partner_university_finance', 'Fee Structure', 1, 'feeStructure', 'list', 1, 1),
(253, 'Collection Report', 'company_user_reports', 'Reports', 1, 'collectionReport', 'list', 1, 0),
(254, 'Invoice Report', 'company_user_reports', 'Reports', 2, 'invoiceReport', 'list', 1, 1),
(255, 'Credit Note Report', 'company_user_reports', 'Reports', 3, 'creditNoteReport', 'list', 1, 1),
(256, 'Debit Note Report', 'company_user_reports', 'Reports', 4, 'debitNoteReport', 'list', 1, 1),
(257, 'Invoice V/S Payment Report', 'company_user_reports', 'Reports', 5, 'invoicePaymentReport', 'list', 1, 0),
(258, 'Student Invoice Generation', 'Finance', 'Invoice', 1, 'mainInvoiceStudent', 'list', 4, 1),
(259, 'Corporate Invoice Generation', 'Finance', 'Invoice', 2, 'mainInvoiceCorporate', 'list', 4, 1),
(260, 'Change Password', 'company_user', 'Profile', 2, 'company', 'changePassword', 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=261;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
